.class public final Lcom/b/d/a;
.super Ljava/lang/Object;
.source "AndroidThreeTen.java"


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    sput-object v0, Lcom/b/d/a;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static a(Landroid/app/Application;)V
    .locals 0

    .line 13
    invoke-static {p0}, Lcom/b/d/a;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .line 17
    sget-object v0, Lcom/b/d/a;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/b/d/b;

    invoke-direct {v0, p0}, Lcom/b/d/b;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lorg/threeten/bp/zone/ZoneRulesInitializer;->setInitializer(Lorg/threeten/bp/zone/ZoneRulesInitializer;)V

    :cond_0
    return-void
.end method
