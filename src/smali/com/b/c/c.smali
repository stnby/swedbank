.class public final Lcom/b/c/c;
.super Lcom/b/c/d;
.source "PublishRelay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/c/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/b/c/d<",
        "TT;>;"
    }
.end annotation


# static fields
.field static final a:[Lcom/b/c/c$a;


# instance fields
.field final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "[",
            "Lcom/b/c/c$a<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    .line 48
    new-array v0, v0, [Lcom/b/c/c$a;

    sput-object v0, Lcom/b/c/c;->a:[Lcom/b/c/c$a;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .line 66
    invoke-direct {p0}, Lcom/b/c/d;-><init>()V

    .line 67
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/b/c/c;->a:[Lcom/b/c/c$a;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/b/c/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public static a()Lcom/b/c/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/b/c/c<",
            "TT;>;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/b/c/c;

    invoke-direct {v0}, Lcom/b/c/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method a(Lcom/b/c/c$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/c/c$a<",
            "TT;>;)V"
        }
    .end annotation

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/b/c/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/c/c$a;

    .line 89
    array-length v1, v0

    add-int/lit8 v2, v1, 0x1

    .line 91
    new-array v2, v2, [Lcom/b/c/c$a;

    const/4 v3, 0x0

    .line 92
    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    aput-object p1, v2, v1

    .line 95
    iget-object v1, p0, Lcom/b/c/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method protected a(Lio/reactivex/u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 72
    new-instance v0, Lcom/b/c/c$a;

    invoke-direct {v0, p1, p0}, Lcom/b/c/c$a;-><init>(Lio/reactivex/u;Lcom/b/c/c;)V

    .line 73
    invoke-interface {p1, v0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 74
    invoke-virtual {p0, v0}, Lcom/b/c/c;->a(Lcom/b/c/c$a;)V

    .line 77
    invoke-virtual {v0}, Lcom/b/c/c$a;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 78
    invoke-virtual {p0, v0}, Lcom/b/c/c;->b(Lcom/b/c/c$a;)V

    :cond_0
    return-void
.end method

.method b(Lcom/b/c/c$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/c/c$a<",
            "TT;>;)V"
        }
    .end annotation

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/b/c/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/c/c$a;

    .line 109
    sget-object v1, Lcom/b/c/c;->a:[Lcom/b/c/c$a;

    if-ne v0, v1, :cond_1

    return-void

    .line 113
    :cond_1
    array-length v1, v0

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_3

    .line 116
    aget-object v5, v0, v4

    if-ne v5, p1, :cond_2

    move v2, v4

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    if-gez v2, :cond_4

    return-void

    :cond_4
    const/4 v4, 0x1

    if-ne v1, v4, :cond_5

    .line 129
    sget-object v1, Lcom/b/c/c;->a:[Lcom/b/c/c$a;

    goto :goto_2

    :cond_5
    add-int/lit8 v5, v1, -0x1

    .line 131
    new-array v5, v5, [Lcom/b/c/c$a;

    .line 132
    invoke-static {v0, v3, v5, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v2, 0x1

    sub-int/2addr v1, v2

    sub-int/2addr v1, v4

    .line 133
    invoke-static {v0, v3, v5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v1, v5

    .line 135
    :goto_2
    iget-object v2, p0, Lcom/b/c/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 144
    iget-object v0, p0, Lcom/b/c/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/c/c$a;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 145
    invoke-virtual {v3, p1}, Lcom/b/c/c$a;->a(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 143
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "value == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()Z
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/b/c/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/c/c$a;

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
