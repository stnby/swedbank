.class final Lcom/b/c/b$a;
.super Ljava/lang/Object;
.source "BehaviorRelay.java"

# interfaces
.implements Lcom/b/c/a$a;
.implements Lio/reactivex/b/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/c/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/b/c/a$a<",
        "TT;>;",
        "Lio/reactivex/b/c;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final b:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "TT;>;"
        }
    .end annotation
.end field

.field c:Z

.field d:Z

.field e:Lcom/b/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field f:Z

.field volatile g:Z

.field h:J


# direct methods
.method constructor <init>(Lio/reactivex/u;Lcom/b/c/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;",
            "Lcom/b/c/b<",
            "TT;>;)V"
        }
    .end annotation

    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iput-object p1, p0, Lcom/b/c/b$a;->a:Lio/reactivex/u;

    .line 284
    iput-object p2, p0, Lcom/b/c/b$a;->b:Lcom/b/c/b;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 289
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 290
    iput-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    .line 292
    iget-object v0, p0, Lcom/b/c/b$a;->b:Lcom/b/c/b;

    invoke-virtual {v0, p0}, Lcom/b/c/b;->b(Lcom/b/c/b$a;)V

    :cond_0
    return-void
.end method

.method a(Ljava/lang/Object;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)V"
        }
    .end annotation

    .line 333
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    if-eqz v0, :cond_0

    return-void

    .line 336
    :cond_0
    iget-boolean v0, p0, Lcom/b/c/b$a;->f:Z

    if-nez v0, :cond_5

    .line 337
    monitor-enter p0

    .line 338
    :try_start_0
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    if-eqz v0, :cond_1

    .line 339
    monitor-exit p0

    return-void

    .line 341
    :cond_1
    iget-wide v0, p0, Lcom/b/c/b$a;->h:J

    cmp-long p2, v0, p2

    if-nez p2, :cond_2

    .line 342
    monitor-exit p0

    return-void

    .line 344
    :cond_2
    iget-boolean p2, p0, Lcom/b/c/b$a;->d:Z

    if-eqz p2, :cond_4

    .line 345
    iget-object p2, p0, Lcom/b/c/b$a;->e:Lcom/b/c/a;

    if-nez p2, :cond_3

    .line 347
    new-instance p2, Lcom/b/c/a;

    const/4 p3, 0x4

    invoke-direct {p2, p3}, Lcom/b/c/a;-><init>(I)V

    .line 348
    iput-object p2, p0, Lcom/b/c/b$a;->e:Lcom/b/c/a;

    .line 350
    :cond_3
    invoke-virtual {p2, p1}, Lcom/b/c/a;->a(Ljava/lang/Object;)V

    .line 351
    monitor-exit p0

    return-void

    :cond_4
    const/4 p2, 0x1

    .line 353
    iput-boolean p2, p0, Lcom/b/c/b$a;->c:Z

    .line 354
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    iput-boolean p2, p0, Lcom/b/c/b$a;->f:Z

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 354
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 358
    :cond_5
    :goto_0
    invoke-virtual {p0, p1}, Lcom/b/c/b$a;->a(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 363
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    if-nez v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/b/c/b$a;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public b()Z
    .locals 1

    .line 298
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    return v0
.end method

.method c()V
    .locals 4

    .line 302
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    if-eqz v0, :cond_0

    return-void

    .line 306
    :cond_0
    monitor-enter p0

    .line 307
    :try_start_0
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    if-eqz v0, :cond_1

    .line 308
    monitor-exit p0

    return-void

    .line 310
    :cond_1
    iget-boolean v0, p0, Lcom/b/c/b$a;->c:Z

    if-eqz v0, :cond_2

    .line 311
    monitor-exit p0

    return-void

    .line 314
    :cond_2
    iget-object v0, p0, Lcom/b/c/b$a;->b:Lcom/b/c/b;

    .line 315
    iget-object v1, v0, Lcom/b/c/b;->d:Ljava/util/concurrent/locks/Lock;

    .line 317
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 318
    iget-wide v2, v0, Lcom/b/c/b;->f:J

    iput-wide v2, p0, Lcom/b/c/b$a;->h:J

    .line 319
    iget-object v0, v0, Lcom/b/c/b;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 320
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    .line 322
    :goto_0
    iput-boolean v2, p0, Lcom/b/c/b$a;->d:Z

    .line 323
    iput-boolean v1, p0, Lcom/b/c/b$a;->c:Z

    .line 324
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    .line 327
    invoke-virtual {p0, v0}, Lcom/b/c/b$a;->a(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual {p0}, Lcom/b/c/b$a;->d()V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    .line 324
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method d()V
    .locals 2

    .line 371
    :goto_0
    iget-boolean v0, p0, Lcom/b/c/b$a;->g:Z

    if-eqz v0, :cond_0

    return-void

    .line 375
    :cond_0
    monitor-enter p0

    .line 376
    :try_start_0
    iget-object v0, p0, Lcom/b/c/b$a;->e:Lcom/b/c/a;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 378
    iput-boolean v0, p0, Lcom/b/c/b$a;->d:Z

    .line 379
    monitor-exit p0

    return-void

    :cond_1
    const/4 v1, 0x0

    .line 381
    iput-object v1, p0, Lcom/b/c/b$a;->e:Lcom/b/c/a;

    .line 382
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    invoke-virtual {v0, p0}, Lcom/b/c/a;->a(Lcom/b/c/a$a;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 382
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
