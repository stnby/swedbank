.class final synthetic Lcom/b/b/d/f;
.super Ljava/lang/Object;
.source "ViewLongClickObservable.kt"


# direct methods
.method public static final a(Landroid/view/View;Lkotlin/e/a/a;)Lio/reactivex/o;
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/e/a/a<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/b/b/d/h;

    invoke-direct {v0, p0, p1}, Lcom/b/b/d/h;-><init>(Landroid/view/View;Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public static bridge synthetic a(Landroid/view/View;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 32
    sget-object p1, Lcom/b/b/b/a;->a:Lcom/b/b/b/a;

    check-cast p1, Lkotlin/e/a/a;

    :cond_0
    invoke-static {p0, p1}, Lcom/b/b/d/d;->a(Landroid/view/View;Lkotlin/e/a/a;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method
