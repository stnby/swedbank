.class public final Lcom/b/b/d/d;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Landroid/view/View;)Lcom/b/b/a;
    .locals 0
    .param p0    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lcom/b/b/a<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0}, Lcom/b/b/d/e;->a(Landroid/view/View;)Lcom/b/b/a;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Landroid/view/View;Lkotlin/e/a/a;)Lio/reactivex/o;
    .locals 0
    .param p0    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/e/a/a<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0, p1}, Lcom/b/b/d/f;->a(Landroid/view/View;Lkotlin/e/a/a;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic a(Landroid/view/View;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/b/b/d/f;->a(Landroid/view/View;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method
