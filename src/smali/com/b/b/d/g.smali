.class final Lcom/b/b/d/g;
.super Lcom/b/b/a;
.source "ViewFocusChangeObservable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/b/d/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/b/b/a<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/b/b/a;-><init>()V

    iput-object p1, p0, Lcom/b/b/d/g;->a:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/b/b/d/g;->c()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lio/reactivex/u;)V
    .locals 2
    .param p1    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "observer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/b/b/d/g$a;

    iget-object v1, p0, Lcom/b/b/d/g;->a:Landroid/view/View;

    invoke-direct {v0, v1, p1}, Lcom/b/b/d/g$a;-><init>(Landroid/view/View;Lio/reactivex/u;)V

    .line 38
    move-object v1, v0

    check-cast v1, Lio/reactivex/b/c;

    invoke-interface {p1, v1}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 39
    iget-object p1, p0, Lcom/b/b/d/g;->a:Landroid/view/View;

    check-cast v0, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method protected c()Ljava/lang/Boolean;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/b/b/d/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
