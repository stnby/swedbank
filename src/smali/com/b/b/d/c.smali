.class final synthetic Lcom/b/b/d/c;
.super Ljava/lang/Object;
.source "MenuItemClickObservable.kt"


# direct methods
.method public static final a(Landroid/view/MenuItem;Lkotlin/e/a/b;)Lio/reactivex/o;
    .locals 1
    .param p0    # Landroid/view/MenuItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MenuItem;",
            "Lkotlin/e/a/b<",
            "-",
            "Landroid/view/MenuItem;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/b/b/d/a;

    invoke-direct {v0, p0, p1}, Lcom/b/b/d/a;-><init>(Landroid/view/MenuItem;Lkotlin/e/a/b;)V

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public static bridge synthetic a(Landroid/view/MenuItem;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 31
    sget-object p1, Lcom/b/b/b/a;->a:Lcom/b/b/b/a;

    check-cast p1, Lkotlin/e/a/b;

    :cond_0
    invoke-static {p0, p1}, Lcom/b/b/d/b;->a(Landroid/view/MenuItem;Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method
