.class final Lcom/b/b/d/h;
.super Lio/reactivex/o;
.source "ViewLongClickObservable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/b/d/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/o<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lkotlin/e/a/a;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/e/a/a<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handled"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    iput-object p1, p0, Lcom/b/b/d/h;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/b/b/d/h;->b:Lkotlin/e/a/a;

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 3
    .param p1    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "observer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {p1}, Lcom/b/b/b/b;->a(Lio/reactivex/u;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 45
    :cond_0
    new-instance v0, Lcom/b/b/d/h$a;

    iget-object v1, p0, Lcom/b/b/d/h;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/b/b/d/h;->b:Lkotlin/e/a/a;

    invoke-direct {v0, v1, v2, p1}, Lcom/b/b/d/h$a;-><init>(Landroid/view/View;Lkotlin/e/a/a;Lio/reactivex/u;)V

    .line 46
    move-object v1, v0

    check-cast v1, Lio/reactivex/b/c;

    invoke-interface {p1, v1}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 47
    iget-object p1, p0, Lcom/b/b/d/h;->a:Landroid/view/View;

    check-cast v0, Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method
