.class public final Lcom/b/b/e/a;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Landroid/widget/TextView;)Lcom/b/b/a;
    .locals 0
    .param p0    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lcom/b/b/a<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0}, Lcom/b/b/e/c;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Landroid/widget/TextView;Lkotlin/e/a/b;)Lio/reactivex/o;
    .locals 0
    .param p0    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/b/b/e/d;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/b/b/e/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0, p1}, Lcom/b/b/e/b;->a(Landroid/widget/TextView;Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic a(Landroid/widget/TextView;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/b/b/e/b;->a(Landroid/widget/TextView;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method
