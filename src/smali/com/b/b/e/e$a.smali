.class final Lcom/b/b/e/e$a;
.super Lio/reactivex/a/a;
.source "TextViewEditorActionEventObservable.kt"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/b/e/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-",
            "Lcom/b/b/e/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/b/b/e/d;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Lio/reactivex/u;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/u<",
            "-",
            "Lcom/b/b/e/d;",
            ">;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/b/b/e/d;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handled"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Lio/reactivex/a/a;-><init>()V

    iput-object p1, p0, Lcom/b/b/e/e$a;->a:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/b/b/e/e$a;->b:Lio/reactivex/u;

    iput-object p3, p0, Lcom/b/b/e/e$a;->c:Lkotlin/e/a/b;

    return-void
.end method


# virtual methods
.method protected c_()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/b/b/e/e$a;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/view/KeyEvent;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "textView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance p1, Lcom/b/b/e/d;

    iget-object v0, p0, Lcom/b/b/e/e$a;->a:Landroid/widget/TextView;

    invoke-direct {p1, v0, p2, p3}, Lcom/b/b/e/d;-><init>(Landroid/widget/TextView;ILandroid/view/KeyEvent;)V

    .line 67
    :try_start_0
    invoke-virtual {p0}, Lcom/b/b/e/e$a;->b()Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/b/b/e/e$a;->c:Lkotlin/e/a/b;

    invoke-interface {p2, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 68
    iget-object p2, p0, Lcom/b/b/e/e$a;->b:Lio/reactivex/u;

    invoke-interface {p2, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    .line 72
    iget-object p2, p0, Lcom/b/b/e/e$a;->b:Lio/reactivex/u;

    check-cast p1, Ljava/lang/Throwable;

    invoke-interface {p2, p1}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V

    .line 73
    invoke-virtual {p0}, Lcom/b/b/e/e$a;->a()V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
