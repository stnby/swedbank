.class final Lcom/b/b/e/e;
.super Lio/reactivex/o;
.source "TextViewEditorActionEventObservable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/b/e/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/o<",
        "Lcom/b/b/e/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/b/b/e/d;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/b/b/e/d;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handled"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    iput-object p1, p0, Lcom/b/b/e/e;->a:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/b/b/e/e;->b:Lkotlin/e/a/b;

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 3
    .param p1    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Lcom/b/b/e/d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "observer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {p1}, Lcom/b/b/b/b;->a(Lio/reactivex/u;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 53
    :cond_0
    new-instance v0, Lcom/b/b/e/e$a;

    iget-object v1, p0, Lcom/b/b/e/e;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/b/b/e/e;->b:Lkotlin/e/a/b;

    invoke-direct {v0, v1, p1, v2}, Lcom/b/b/e/e$a;-><init>(Landroid/widget/TextView;Lio/reactivex/u;Lkotlin/e/a/b;)V

    .line 54
    move-object v1, v0

    check-cast v1, Lio/reactivex/b/c;

    invoke-interface {p1, v1}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 55
    iget-object p1, p0, Lcom/b/b/e/e;->a:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method
