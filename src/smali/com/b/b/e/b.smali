.class final synthetic Lcom/b/b/e/b;
.super Ljava/lang/Object;
.source "TextViewEditorActionEventObservable.kt"


# direct methods
.method public static final a(Landroid/widget/TextView;Lkotlin/e/a/b;)Lio/reactivex/o;
    .locals 1
    .param p0    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/b/b/e/d;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/b/b/e/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/b/b/e/e;

    invoke-direct {v0, p0, p1}, Lcom/b/b/e/e;-><init>(Landroid/widget/TextView;Lkotlin/e/a/b;)V

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public static bridge synthetic a(Landroid/widget/TextView;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 32
    sget-object p1, Lcom/b/b/b/a;->a:Lcom/b/b/b/a;

    check-cast p1, Lkotlin/e/a/b;

    :cond_0
    invoke-static {p0, p1}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method
