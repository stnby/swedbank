.class final Lcom/b/b/a/e;
.super Lio/reactivex/o;
.source "ToolbarNavigationClickObservable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/b/a/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/o<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroidx/appcompat/widget/Toolbar;


# direct methods
.method public constructor <init>(Landroidx/appcompat/widget/Toolbar;)V
    .locals 1
    .param p1    # Landroidx/appcompat/widget/Toolbar;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    iput-object p1, p0, Lcom/b/b/a/e;->a:Landroidx/appcompat/widget/Toolbar;

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 2
    .param p1    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "observer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p1}, Lcom/b/b/b/b;->a(Lio/reactivex/u;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 37
    :cond_0
    new-instance v0, Lcom/b/b/a/e$a;

    iget-object v1, p0, Lcom/b/b/a/e;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-direct {v0, v1, p1}, Lcom/b/b/a/e$a;-><init>(Landroidx/appcompat/widget/Toolbar;Lio/reactivex/u;)V

    .line 38
    move-object v1, v0

    check-cast v1, Lio/reactivex/b/c;

    invoke-interface {p1, v1}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 39
    iget-object p1, p0, Lcom/b/b/a/e;->a:Landroidx/appcompat/widget/Toolbar;

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
