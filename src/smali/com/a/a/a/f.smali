.class public final Lcom/a/a/a/f;
.super Ljava/lang/Object;
.source "RxSharedPreferences.java"


# static fields
.field private static final a:Ljava/lang/Float;

.field private static final b:Ljava/lang/Integer;

.field private static final c:Ljava/lang/Boolean;

.field private static final d:Ljava/lang/Long;


# instance fields
.field private final e:Landroid/content/SharedPreferences;

.field private final f:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    .line 21
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/f;->a:Ljava/lang/Float;

    const/4 v0, 0x0

    .line 22
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/a/a/a/f;->b:Ljava/lang/Integer;

    .line 23
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/f;->c:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 24
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/f;->d:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/a/a/a/f;->e:Landroid/content/SharedPreferences;

    .line 39
    new-instance v0, Lcom/a/a/a/f$1;

    invoke-direct {v0, p0, p1}, Lcom/a/a/a/f$1;-><init>(Lcom/a/a/a/f;Landroid/content/SharedPreferences;)V

    invoke-static {v0}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/a/f;->f:Lio/reactivex/o;

    return-void
.end method

.method public static a(Landroid/content/SharedPreferences;)Lcom/a/a/a/f;
    .locals 1

    const-string v0, "preferences == null"

    .line 30
    invoke-static {p0, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/a/a/a/f;

    invoke-direct {v0, p0}, Lcom/a/a/a/f;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/a/a/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 62
    sget-object v0, Lcom/a/a/a/f;->c:Ljava/lang/Boolean;

    invoke-virtual {p0, p1, v0}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 68
    invoke-static {p1, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 69
    invoke-static {p2, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance v0, Lcom/a/a/a/e;

    iget-object v2, p0, Lcom/a/a/a/f;->e:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/a/a/a/a;->a:Lcom/a/a/a/a;

    iget-object v6, p0, Lcom/a/a/a/f;->f:Lio/reactivex/o;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/a/a/a/e;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/a/a/a/e$a;Lio/reactivex/o;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/a/a/a/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/a/a/a/d<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 107
    invoke-static {p1, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 108
    invoke-static {p2, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/a/a/a/e;

    iget-object v2, p0, Lcom/a/a/a/f;->e:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/a/a/a/b;->a:Lcom/a/a/a/b;

    iget-object v6, p0, Lcom/a/a/a/f;->f:Lio/reactivex/o;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/a/a/a/e;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/a/a/a/e$a;Lio/reactivex/o;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 148
    invoke-static {p1, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 149
    invoke-static {p2, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v0, Lcom/a/a/a/e;

    iget-object v2, p0, Lcom/a/a/a/f;->e:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    iget-object v6, p0, Lcom/a/a/a/f;->f:Lio/reactivex/o;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/a/a/a/e;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/a/a/a/e$a;Lio/reactivex/o;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/a/a/a/f;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
