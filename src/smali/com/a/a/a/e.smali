.class final Lcom/a/a/a/e;
.super Ljava/lang/Object;
.source "RealPreference.java"

# interfaces
.implements Lcom/a/a/a/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/a/a/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/a/a/a/d<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final d:Lcom/a/a/a/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/e$a<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final e:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/a/a/a/e$a;Lio/reactivex/o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/a/a/a/e$a<",
            "TT;>;",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/a/a/a/e;->a:Landroid/content/SharedPreferences;

    .line 37
    iput-object p2, p0, Lcom/a/a/a/e;->b:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/a/a/a/e;->c:Ljava/lang/Object;

    .line 39
    iput-object p4, p0, Lcom/a/a/a/e;->d:Lcom/a/a/a/e$a;

    .line 40
    new-instance p1, Lcom/a/a/a/e$2;

    invoke-direct {p1, p0, p2}, Lcom/a/a/a/e$2;-><init>(Lcom/a/a/a/e;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p5, p1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "<init>"

    .line 46
    invoke-virtual {p1, p2}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    new-instance p2, Lcom/a/a/a/e$1;

    invoke-direct {p2, p0}, Lcom/a/a/a/e$1;-><init>(Lcom/a/a/a/e;)V

    .line 47
    invoke-virtual {p1, p2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/a/e;->e:Lio/reactivex/o;

    return-void
.end method


# virtual methods
.method public declared-synchronized a()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    monitor-enter p0

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/a/a/a/e;->a:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/a/a/a/e;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/a/a/a/e;->c:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 66
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/a/a/a/e;->d:Lcom/a/a/a/e$a;

    iget-object v1, p0, Lcom/a/a/a/e;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/a/a/a/e;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1, v2}, Lcom/a/a/a/e$a;->b(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Object;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 62
    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "value == null"

    .line 70
    invoke-static {p1, v0}, Lcom/a/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/a/a/a/e;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/a/a/a/e;->d:Lcom/a/a/a/e$a;

    iget-object v2, p0, Lcom/a/a/a/e;->b:Ljava/lang/String;

    invoke-interface {v1, v2, p1, v0}, Lcom/a/a/a/e$a;->a(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)V

    .line 73
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/a/a/a/e;->e:Lio/reactivex/o;

    return-object v0
.end method
