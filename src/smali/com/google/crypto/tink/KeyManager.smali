.class public interface abstract Lcom/google/crypto/tink/KeyManager;
.super Ljava/lang/Object;
.source "KeyManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract doesSupport(Ljava/lang/String;)Z
.end method

.method public abstract getKeyType()Ljava/lang/String;
.end method

.method public abstract getPrimitive(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/ByteString;",
            ")TP;"
        }
    .end annotation
.end method

.method public abstract getPrimitive(Lcom/google/protobuf/MessageLite;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/MessageLite;",
            ")TP;"
        }
    .end annotation
.end method

.method public abstract getVersion()I
.end method

.method public abstract newKey(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/MessageLite;
.end method

.method public abstract newKey(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/MessageLite;
.end method

.method public abstract newKeyData(Lcom/google/protobuf/ByteString;)Lcom/google/crypto/tink/proto/KeyData;
.end method
