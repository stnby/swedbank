.class public final Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;
.super Ljava/lang/Object;
.source "WebPushHybridDecrypt.java"

# interfaces
.implements Lcom/google/crypto/tink/HybridDecrypt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
    }
.end annotation


# instance fields
.field private final authSecret:[B

.field private final recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

.field private final recipientPublicKey:[B

.field private final recordSize:I


# direct methods
.method private constructor <init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)V
    .locals 5

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$000(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)Ljava/security/interfaces/ECPrivateKey;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 95
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$000(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)Ljava/security/interfaces/ECPrivateKey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

    .line 97
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$100(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    .line 98
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$100(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B

    move-result-object v0

    array-length v0, v0

    const/16 v1, 0x41

    if-ne v0, v1, :cond_3

    .line 102
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$100(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->recipientPublicKey:[B

    .line 104
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    .line 107
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B

    move-result-object v0

    array-length v0, v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 111
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->authSecret:[B

    .line 113
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)I

    move-result v0

    const/16 v1, 0x1000

    const/16 v2, 0x67

    if-lt v0, v2, :cond_0

    .line 114
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 122
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)I

    move-result p1

    iput p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->recordSize:I

    return-void

    .line 115
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 118
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v4

    const/4 p1, 0x1

    .line 119
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, p1

    const/4 p1, 0x2

    .line 120
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, p1

    const-string p1, "invalid record size (%s); must be a number between [%s, %s]"

    .line 116
    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "auth secret must have 16 bytes"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 105
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set auth secret with Builder.withAuthSecret"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 99
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "recipient public key must have 65 bytes"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 92
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set recipient\'s private key with Builder.withRecipientPrivateKey"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic constructor <init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$1;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;-><init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)V

    return-void
.end method

.method private decrypt([B[B[B)[B
    .locals 3

    .line 257
    sget-object v0, Lcom/google/crypto/tink/subtle/EngineFactory;->CIPHER:Lcom/google/crypto/tink/subtle/EngineFactory;

    const-string v1, "AES/GCM/NoPadding"

    invoke-virtual {v0, v1}, Lcom/google/crypto/tink/subtle/EngineFactory;->getInstance(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/crypto/Cipher;

    .line 258
    new-instance v1, Ljavax/crypto/spec/GCMParameterSpec;

    const/16 v2, 0x80

    invoke-direct {v1, v2, p2}, Ljavax/crypto/spec/GCMParameterSpec;-><init>(I[B)V

    .line 259
    new-instance p2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {p2, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 p1, 0x2

    invoke-virtual {v0, p1, p2, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 260
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p2

    .line 261
    array-length p3, p2

    add-int/lit8 p3, p3, -0x1

    aget-byte p3, p2, p3

    if-ne p3, p1, :cond_0

    const/4 p1, 0x0

    .line 264
    array-length p3, p2

    add-int/lit8 p3, p3, -0x1

    invoke-static {p2, p1, p3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    return-object p1

    .line 262
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "decryption failed"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public decrypt([B[B)[B
    .locals 4

    if-nez p2, :cond_4

    .line 208
    array-length p2, p1

    const/16 v0, 0x67

    if-lt p2, v0, :cond_3

    .line 214
    array-length p2, p1

    const/16 v0, 0x1000

    if-gt p2, v0, :cond_2

    .line 219
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p2

    const/16 v1, 0x10

    .line 220
    new-array v1, v1, [B

    .line 221
    invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 223
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 224
    iget v3, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->recordSize:I

    if-ne v2, v3, :cond_1

    array-length v3, p1

    if-lt v2, v3, :cond_1

    if-gt v2, v0, :cond_1

    .line 230
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    const/16 v2, 0x41

    if-ne v0, v2, :cond_0

    .line 235
    new-array v0, v2, [B

    .line 236
    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 237
    sget-object v2, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    sget-object v3, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->UNCOMPRESSED_POINT_FORMAT:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    .line 238
    invoke-static {v2, v3, v0}, Lcom/google/crypto/tink/subtle/EllipticCurves;->pointDecode(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;[B)Ljava/security/spec/ECPoint;

    move-result-object v2

    .line 243
    array-length p1, p1

    add-int/lit8 p1, p1, -0x56

    new-array p1, p1, [B

    .line 244
    invoke-virtual {p2, p1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 247
    iget-object p2, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

    invoke-static {p2, v2}, Lcom/google/crypto/tink/subtle/EllipticCurves;->computeSharedSecret(Ljava/security/interfaces/ECPrivateKey;Ljava/security/spec/ECPoint;)[B

    move-result-object p2

    .line 248
    iget-object v2, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->authSecret:[B

    iget-object v3, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->recipientPublicKey:[B

    invoke-static {p2, v2, v3, v0}, Lcom/google/crypto/tink/apps/webpush/WebPushUtil;->computeIkm([B[B[B[B)[B

    move-result-object p2

    .line 249
    invoke-static {p2, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushUtil;->computeCek([B[B)[B

    move-result-object v0

    .line 250
    invoke-static {p2, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushUtil;->computeNonce([B[B)[B

    move-result-object p2

    .line 252
    invoke-direct {p0, v0, p2, p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;->decrypt([B[B[B)[B

    move-result-object p1

    return-object p1

    .line 232
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "invalid ephemeral public key size: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 227
    :cond_1
    new-instance p1, Ljava/security/GeneralSecurityException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "invalid record size: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 215
    :cond_2
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "ciphertext too long"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 209
    :cond_3
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "ciphertext too short"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 205
    :cond_4
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "contextInfo must be null because it is unused"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
