.class Lcom/google/crypto/tink/apps/webpush/WebPushConstants;
.super Ljava/lang/Object;
.source "WebPushConstants.java"


# static fields
.field public static final AUTH_SECRET_SIZE:I = 0x10

.field public static final CEK_INFO:[B

.field public static final CEK_KEY_SIZE:I = 0x10

.field public static final CIPHERTEXT_OVERHEAD:I = 0x67

.field public static final CONTENT_CODING_HEADER_SIZE:I = 0x56

.field public static final HMAC_SHA256:Ljava/lang/String; = "HMACSHA256"

.field public static final IKM_INFO:[B

.field public static final IKM_SIZE:I = 0x20

.field public static final MAX_CIPHERTEXT_SIZE:I = 0x1000

.field public static final NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

.field public static final NONCE_INFO:[B

.field public static final NONCE_SIZE:I = 0xc

.field public static final PADDING_DELIMITER_BYTE:B = 0x2t

.field public static final PADDING_SIZE:I = 0x1

.field public static final PUBLIC_KEY_SIZE:I = 0x41

.field public static final PUBLIC_KEY_SIZE_LEN:I = 0x1

.field public static final RECORD_SIZE_LEN:I = 0x4

.field public static final SALT_SIZE:I = 0x10

.field public static final TAG_SIZE:I = 0x10

.field public static final UNCOMPRESSED_POINT_FORMAT:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

.field public static final UTF_8:Ljava/nio/charset/Charset;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "UTF-8"

    .line 24
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->UTF_8:Ljava/nio/charset/Charset;

    const/16 v0, 0xe

    .line 29
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->IKM_INFO:[B

    const/16 v0, 0x1c

    .line 31
    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->CEK_INFO:[B

    const/16 v0, 0x18

    .line 36
    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NONCE_INFO:[B

    .line 67
    sget-object v0, Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;->UNCOMPRESSED:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    sput-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->UNCOMPRESSED_POINT_FORMAT:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    .line 69
    sget-object v0, Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;->NIST_P256:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    sput-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    return-void

    :array_0
    .array-data 1
        0x57t
        0x65t
        0x62t
        0x50t
        0x75t
        0x73t
        0x68t
        0x3at
        0x20t
        0x69t
        0x6et
        0x66t
        0x6ft
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x43t
        0x6ft
        0x6et
        0x74t
        0x65t
        0x6et
        0x74t
        0x2dt
        0x45t
        0x6et
        0x63t
        0x6ft
        0x64t
        0x69t
        0x6et
        0x67t
        0x3at
        0x20t
        0x61t
        0x65t
        0x73t
        0x31t
        0x32t
        0x38t
        0x67t
        0x63t
        0x6dt
        0x0t
    .end array-data

    :array_2
    .array-data 1
        0x43t
        0x6ft
        0x6et
        0x74t
        0x65t
        0x6et
        0x74t
        0x2dt
        0x45t
        0x6et
        0x63t
        0x6ft
        0x64t
        0x69t
        0x6et
        0x67t
        0x3at
        0x20t
        0x6et
        0x6ft
        0x6et
        0x63t
        0x65t
        0x0t
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
