.class public final Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
.super Ljava/lang/Object;
.source "WebPushHybridDecrypt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private authSecret:[B

.field private recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

.field private recipientPublicKey:[B

.field private recordSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 131
    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

    .line 132
    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPublicKey:[B

    .line 133
    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->authSecret:[B

    const/16 v0, 0x1000

    .line 134
    iput v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recordSize:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)Ljava/security/interfaces/ECPrivateKey;
    .locals 0

    .line 130
    iget-object p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

    return-object p0
.end method

.method static synthetic access$100(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B
    .locals 0

    .line 130
    iget-object p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPublicKey:[B

    return-object p0
.end method

.method static synthetic access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)[B
    .locals 0

    .line 130
    iget-object p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->authSecret:[B

    return-object p0
.end method

.method static synthetic access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;)I
    .locals 0

    .line 130
    iget p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recordSize:I

    return p0
.end method


# virtual methods
.method public build()Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;
    .locals 2

    .line 197
    new-instance v0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;-><init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$1;)V

    return-object v0
.end method

.method public withAuthSecret([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
    .locals 0

    .line 153
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->authSecret:[B

    return-object p0
.end method

.method public withRecipientPrivateKey(Ljava/security/interfaces/ECPrivateKey;)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

    return-object p0
.end method

.method public withRecipientPrivateKey([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
    .locals 1

    .line 191
    sget-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    .line 192
    invoke-static {v0, p1}, Lcom/google/crypto/tink/subtle/EllipticCurves;->getEcPrivateKey(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;[B)Ljava/security/interfaces/ECPrivateKey;

    move-result-object p1

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPrivateKey:Ljava/security/interfaces/ECPrivateKey;

    return-object p0
.end method

.method public withRecipientPublicKey(Ljava/security/interfaces/ECPublicKey;)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
    .locals 2

    .line 159
    sget-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    sget-object v1, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->UNCOMPRESSED_POINT_FORMAT:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    .line 163
    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object p1

    .line 160
    invoke-static {v0, v1, p1}, Lcom/google/crypto/tink/subtle/EllipticCurves;->pointEncode(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;Ljava/security/spec/ECPoint;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPublicKey:[B

    return-object p0
.end method

.method public withRecipientPublicKey([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
    .locals 0

    .line 174
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recipientPublicKey:[B

    return-object p0
.end method

.method public withRecordSize(I)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;
    .locals 0

    .line 147
    iput p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->recordSize:I

    return-object p0
.end method
