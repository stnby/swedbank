.class public final Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;
.super Ljava/lang/Object;
.source "WebPushHybridEncrypt.java"

# interfaces
.implements Lcom/google/crypto/tink/HybridEncrypt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;
    }
.end annotation


# instance fields
.field private final authSecret:[B

.field private final recipientPublicKey:[B

.field private final recipientPublicPoint:Ljava/security/spec/ECPoint;

.field private final recordSize:I


# direct methods
.method private constructor <init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)V
    .locals 5

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$000(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$100(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)Ljava/security/spec/ECPoint;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 96
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$000(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->recipientPublicKey:[B

    .line 97
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$100(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)Ljava/security/spec/ECPoint;

    move-result-object v0

    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->recipientPublicPoint:Ljava/security/spec/ECPoint;

    .line 99
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    .line 102
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)[B

    move-result-object v0

    array-length v0, v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 106
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->authSecret:[B

    .line 108
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)I

    move-result v0

    const/16 v1, 0x1000

    const/16 v2, 0x67

    if-lt v0, v2, :cond_0

    .line 109
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 117
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)I

    move-result p1

    iput p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->recordSize:I

    return-void

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 113
    invoke-static {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v4

    const/4 p1, 0x1

    .line 114
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, p1

    const/4 p1, 0x2

    .line 115
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, p1

    const-string p1, "invalid record size (%s); must be a number between [%s, %s]"

    .line 111
    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "auth secret must have 16 bytes"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 100
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set auth secret with Builder.withAuthSecret"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 93
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set recipient\'s public key with Builder.withRecipientPublicKey"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic constructor <init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$1;)V
    .locals 0

    .line 85
    invoke-direct {p0, p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;-><init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)V

    return-void
.end method

.method private encrypt([B[B[B)[B
    .locals 3

    .line 223
    sget-object v0, Lcom/google/crypto/tink/subtle/EngineFactory;->CIPHER:Lcom/google/crypto/tink/subtle/EngineFactory;

    const-string v1, "AES/GCM/NoPadding"

    invoke-virtual {v0, v1}, Lcom/google/crypto/tink/subtle/EngineFactory;->getInstance(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/crypto/Cipher;

    .line 224
    new-instance v1, Ljavax/crypto/spec/GCMParameterSpec;

    const/16 v2, 0x80

    invoke-direct {v1, v2, p2}, Ljavax/crypto/spec/GCMParameterSpec;-><init>(I[B)V

    .line 225
    new-instance p2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {p2, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 226
    array-length p2, p3

    add-int/2addr p2, p1

    new-array p2, p2, [B

    .line 227
    array-length v1, p2

    sub-int/2addr v1, p1

    const/4 p1, 0x2

    aput-byte p1, p2, v1

    .line 228
    array-length p1, p3

    const/4 v1, 0x0

    invoke-static {p3, v1, p2, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 229
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public encrypt([B[B)[B
    .locals 4

    if-nez p2, :cond_1

    .line 192
    array-length p2, p1

    iget v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->recordSize:I

    add-int/lit8 v0, v0, -0x67

    if-gt p2, v0, :cond_0

    .line 197
    sget-object p2, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    invoke-static {p2}, Lcom/google/crypto/tink/subtle/EllipticCurves;->generateKeyPair(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;)Ljava/security/KeyPair;

    move-result-object p2

    .line 198
    invoke-virtual {p2}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v0

    check-cast v0, Ljava/security/interfaces/ECPrivateKey;

    .line 199
    invoke-virtual {p2}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object p2

    check-cast p2, Ljava/security/interfaces/ECPublicKey;

    .line 200
    iget-object v1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->recipientPublicPoint:Ljava/security/spec/ECPoint;

    .line 201
    invoke-static {v0, v1}, Lcom/google/crypto/tink/subtle/EllipticCurves;->computeSharedSecret(Ljava/security/interfaces/ECPrivateKey;Ljava/security/spec/ECPoint;)[B

    move-result-object v0

    .line 202
    sget-object v1, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    sget-object v2, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->UNCOMPRESSED_POINT_FORMAT:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    .line 206
    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object p2

    .line 203
    invoke-static {v1, v2, p2}, Lcom/google/crypto/tink/subtle/EllipticCurves;->pointEncode(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;Ljava/security/spec/ECPoint;)[B

    move-result-object p2

    .line 207
    iget-object v1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->authSecret:[B

    iget-object v2, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->recipientPublicKey:[B

    .line 208
    invoke-static {v0, v1, v2, p2}, Lcom/google/crypto/tink/apps/webpush/WebPushUtil;->computeIkm([B[B[B[B)[B

    move-result-object v0

    const/16 v1, 0x10

    .line 209
    invoke-static {v1}, Lcom/google/crypto/tink/subtle/Random;->randBytes(I)[B

    move-result-object v1

    .line 210
    invoke-static {v0, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushUtil;->computeCek([B[B)[B

    move-result-object v2

    .line 211
    invoke-static {v0, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushUtil;->computeNonce([B[B)[B

    move-result-object v0

    .line 212
    array-length v3, p1

    add-int/lit8 v3, v3, 0x67

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 213
    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->recordSize:I

    .line 214
    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/16 v3, 0x41

    .line 215
    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 216
    invoke-virtual {v1, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object p2

    .line 217
    invoke-direct {p0, v2, v0, p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;->encrypt([B[B[B)[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 218
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p1

    return-object p1

    .line 193
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "plaintext too long"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 189
    :cond_1
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "contextInfo must be null because it is unused"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
