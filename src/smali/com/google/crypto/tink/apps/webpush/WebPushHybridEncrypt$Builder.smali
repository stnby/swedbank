.class public final Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;
.super Ljava/lang/Object;
.source "WebPushHybridEncrypt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private authSecret:[B

.field private recipientPublicKey:[B

.field private recipientPublicPoint:Ljava/security/spec/ECPoint;

.field private recordSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 126
    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicKey:[B

    .line 127
    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicPoint:Ljava/security/spec/ECPoint;

    .line 128
    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->authSecret:[B

    const/16 v0, 0x1000

    .line 129
    iput v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recordSize:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)[B
    .locals 0

    .line 125
    iget-object p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicKey:[B

    return-object p0
.end method

.method static synthetic access$100(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)Ljava/security/spec/ECPoint;
    .locals 0

    .line 125
    iget-object p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicPoint:Ljava/security/spec/ECPoint;

    return-object p0
.end method

.method static synthetic access$200(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)[B
    .locals 0

    .line 125
    iget-object p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->authSecret:[B

    return-object p0
.end method

.method static synthetic access$300(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;)I
    .locals 0

    .line 125
    iget p0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recordSize:I

    return p0
.end method


# virtual methods
.method public build()Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;
    .locals 2

    .line 181
    new-instance v0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;-><init>(Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$1;)V

    return-object v0
.end method

.method public withAuthSecret([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;
    .locals 0

    .line 148
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->authSecret:[B

    return-object p0
.end method

.method public withRecipientPublicKey(Ljava/security/interfaces/ECPublicKey;)Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;
    .locals 2

    .line 154
    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v0

    iput-object v0, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicPoint:Ljava/security/spec/ECPoint;

    .line 155
    sget-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    sget-object v1, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->UNCOMPRESSED_POINT_FORMAT:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    .line 159
    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object p1

    .line 156
    invoke-static {v0, v1, p1}, Lcom/google/crypto/tink/subtle/EllipticCurves;->pointEncode(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;Ljava/security/spec/ECPoint;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicKey:[B

    return-object p0
.end method

.method public withRecipientPublicKey([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;
    .locals 2

    .line 171
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicKey:[B

    .line 172
    sget-object p1, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NIST_P256_CURVE_TYPE:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    sget-object v0, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->UNCOMPRESSED_POINT_FORMAT:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    iget-object v1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicKey:[B

    .line 173
    invoke-static {p1, v0, v1}, Lcom/google/crypto/tink/subtle/EllipticCurves;->pointDecode(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;[B)Ljava/security/spec/ECPoint;

    move-result-object p1

    iput-object p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recipientPublicPoint:Ljava/security/spec/ECPoint;

    return-object p0
.end method

.method public withRecordSize(I)Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;
    .locals 0

    .line 142
    iput p1, p0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->recordSize:I

    return-object p0
.end method
