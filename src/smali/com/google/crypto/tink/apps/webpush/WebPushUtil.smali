.class Lcom/google/crypto/tink/apps/webpush/WebPushUtil;
.super Ljava/lang/Object;
.source "WebPushUtil.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computeCek([B[B)[B
    .locals 3

    const-string v0, "HMACSHA256"

    .line 42
    sget-object v1, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->CEK_INFO:[B

    const/16 v2, 0x10

    invoke-static {v0, p0, p1, v1, v2}, Lcom/google/crypto/tink/subtle/Hkdf;->computeHkdf(Ljava/lang/String;[B[B[BI)[B

    move-result-object p0

    return-object p0
.end method

.method public static computeIkm([B[B[B[B)[B
    .locals 3

    const/4 v0, 0x3

    .line 31
    new-array v0, v0, [[B

    sget-object v1, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->IKM_INFO:[B

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 p2, 0x2

    aput-object p3, v0, p2

    invoke-static {v0}, Lcom/google/crypto/tink/subtle/Bytes;->concat([[B)[B

    move-result-object p2

    const-string p3, "HMACSHA256"

    const/16 v0, 0x20

    .line 32
    invoke-static {p3, p0, p1, p2, v0}, Lcom/google/crypto/tink/subtle/Hkdf;->computeHkdf(Ljava/lang/String;[B[B[BI)[B

    move-result-object p0

    return-object p0
.end method

.method public static computeNonce([B[B)[B
    .locals 3

    const-string v0, "HMACSHA256"

    .line 52
    sget-object v1, Lcom/google/crypto/tink/apps/webpush/WebPushConstants;->NONCE_INFO:[B

    const/16 v2, 0xc

    invoke-static {v0, p0, p1, v1, v2}, Lcom/google/crypto/tink/subtle/Hkdf;->computeHkdf(Ljava/lang/String;[B[B[BI)[B

    move-result-object p0

    return-object p0
.end method
