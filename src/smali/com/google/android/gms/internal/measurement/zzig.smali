.class public enum Lcom/google/android/gms/internal/measurement/zzig;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/gms/internal/measurement/zzig;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum zzanb:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanc:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzand:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzane:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanf:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzang:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanh:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzani:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanj:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzank:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanl:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanm:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzann:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzano:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanp:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanq:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzanr:Lcom/google/android/gms/internal/measurement/zzig;

.field public static final enum zzans:Lcom/google/android/gms/internal/measurement/zzig;

.field private static final synthetic zzanv:[Lcom/google/android/gms/internal/measurement/zzig;


# instance fields
.field private final zzant:Lcom/google/android/gms/internal/measurement/zzij;

.field private final zzanu:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "DOUBLE"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanz:Lcom/google/android/gms/internal/measurement/zzij;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanb:Lcom/google/android/gms/internal/measurement/zzig;

    .line 10
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "FLOAT"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzany:Lcom/google/android/gms/internal/measurement/zzij;

    const/4 v5, 0x5

    invoke-direct {v0, v1, v3, v2, v5}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanc:Lcom/google/android/gms/internal/measurement/zzig;

    .line 11
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "INT64"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanx:Lcom/google/android/gms/internal/measurement/zzij;

    const/4 v6, 0x2

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzand:Lcom/google/android/gms/internal/measurement/zzig;

    .line 12
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "UINT64"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanx:Lcom/google/android/gms/internal/measurement/zzij;

    const/4 v7, 0x3

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzane:Lcom/google/android/gms/internal/measurement/zzig;

    .line 13
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "INT32"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanw:Lcom/google/android/gms/internal/measurement/zzij;

    const/4 v8, 0x4

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanf:Lcom/google/android/gms/internal/measurement/zzig;

    .line 14
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "FIXED64"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanx:Lcom/google/android/gms/internal/measurement/zzij;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzang:Lcom/google/android/gms/internal/measurement/zzig;

    .line 15
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "FIXED32"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanw:Lcom/google/android/gms/internal/measurement/zzij;

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2, v5}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanh:Lcom/google/android/gms/internal/measurement/zzig;

    .line 16
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "BOOL"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzaoa:Lcom/google/android/gms/internal/measurement/zzij;

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzani:Lcom/google/android/gms/internal/measurement/zzig;

    .line 17
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzif;

    const-string v1, "STRING"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzaob:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2, v6}, Lcom/google/android/gms/internal/measurement/zzif;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanj:Lcom/google/android/gms/internal/measurement/zzig;

    .line 18
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzii;

    const-string v1, "GROUP"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzaoe:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2, v7}, Lcom/google/android/gms/internal/measurement/zzii;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzank:Lcom/google/android/gms/internal/measurement/zzig;

    .line 19
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzih;

    const-string v1, "MESSAGE"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzaoe:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2, v6}, Lcom/google/android/gms/internal/measurement/zzih;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanl:Lcom/google/android/gms/internal/measurement/zzig;

    .line 20
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzik;

    const-string v1, "BYTES"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzaoc:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2, v6}, Lcom/google/android/gms/internal/measurement/zzik;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanm:Lcom/google/android/gms/internal/measurement/zzig;

    .line 21
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "UINT32"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanw:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzann:Lcom/google/android/gms/internal/measurement/zzig;

    .line 22
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "ENUM"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzaod:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzano:Lcom/google/android/gms/internal/measurement/zzig;

    .line 23
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "SFIXED32"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanw:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2, v5}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanp:Lcom/google/android/gms/internal/measurement/zzig;

    .line 24
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "SFIXED64"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanx:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2, v3}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanq:Lcom/google/android/gms/internal/measurement/zzig;

    .line 25
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "SINT32"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanw:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanr:Lcom/google/android/gms/internal/measurement/zzig;

    .line 26
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzig;

    const-string v1, "SINT64"

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzij;->zzanx:Lcom/google/android/gms/internal/measurement/zzij;

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzans:Lcom/google/android/gms/internal/measurement/zzig;

    const/16 v0, 0x12

    .line 27
    new-array v0, v0, [Lcom/google/android/gms/internal/measurement/zzig;

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanb:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanc:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzand:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzane:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanf:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzang:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanh:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzani:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanj:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzank:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanl:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v13

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanm:Lcom/google/android/gms/internal/measurement/zzig;

    aput-object v1, v0, v14

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzann:Lcom/google/android/gms/internal/measurement/zzig;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzano:Lcom/google/android/gms/internal/measurement/zzig;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanp:Lcom/google/android/gms/internal/measurement/zzig;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanq:Lcom/google/android/gms/internal/measurement/zzig;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzanr:Lcom/google/android/gms/internal/measurement/zzig;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzig;->zzans:Lcom/google/android/gms/internal/measurement/zzig;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanv:[Lcom/google/android/gms/internal/measurement/zzig;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/measurement/zzij;",
            "I)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/zzig;->zzant:Lcom/google/android/gms/internal/measurement/zzij;

    .line 4
    iput p4, p0, Lcom/google/android/gms/internal/measurement/zzig;->zzanu:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;ILcom/google/android/gms/internal/measurement/zzid;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/measurement/zzig;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/zzij;I)V

    return-void
.end method

.method public static values()[Lcom/google/android/gms/internal/measurement/zzig;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzig;->zzanv:[Lcom/google/android/gms/internal/measurement/zzig;

    invoke-virtual {v0}, [Lcom/google/android/gms/internal/measurement/zzig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/measurement/zzig;

    return-object v0
.end method


# virtual methods
.method public final zzwz()Lcom/google/android/gms/internal/measurement/zzij;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzig;->zzant:Lcom/google/android/gms/internal/measurement/zzij;

    return-object v0
.end method

.method public final zzxa()I
    .locals 1

    .line 7
    iget v0, p0, Lcom/google/android/gms/internal/measurement/zzig;->zzanu:I

    return v0
.end method
