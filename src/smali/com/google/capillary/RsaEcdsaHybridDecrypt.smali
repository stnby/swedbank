.class public final Lcom/google/capillary/RsaEcdsaHybridDecrypt;
.super Ljava/lang/Object;
.source "RsaEcdsaHybridDecrypt.java"

# interfaces
.implements Lcom/google/crypto/tink/HybridDecrypt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;
    }
.end annotation


# instance fields
.field private final oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

.field private final padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

.field private final recipientPrivateKey:Ljava/security/PrivateKey;

.field private final senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;


# direct methods
.method private constructor <init>(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)V
    .locals 2

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$100(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Lcom/google/crypto/tink/PublicKeyVerify;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 176
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$100(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Lcom/google/crypto/tink/PublicKeyVerify;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;

    .line 178
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$200(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Ljava/security/PrivateKey;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 182
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$200(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Ljava/security/PrivateKey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->recipientPrivateKey:Ljava/security/PrivateKey;

    .line 184
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$300(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 188
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$300(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    .line 190
    iget-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    sget-object v1, Lcom/google/capillary/RsaEcdsaConstants$Padding;->OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$400(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Ljavax/crypto/spec/OAEPParameterSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 191
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set OAEP parameter spec with Builder.withOaepParameterSpec"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 194
    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->access$400(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Ljavax/crypto/spec/OAEPParameterSpec;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-void

    .line 185
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set padding with Builder.withPadding"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 179
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set recipient\'s private key with Builder.withRecipientPrivateKey"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 173
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set sender\'s verifier with Builder.withSenderVerificationKey"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic constructor <init>(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;Lcom/google/capillary/RsaEcdsaHybridDecrypt$1;)V
    .locals 0

    .line 91
    invoke-direct {p0, p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt;-><init>(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)V

    return-void
.end method

.method private deserializeAndVerify([B)[B
    .locals 6

    .line 215
    array-length v0, p1

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    .line 220
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v2, 0x0

    .line 222
    invoke-virtual {v0, p1, v2, v1}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 223
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 224
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    if-ltz v0, :cond_0

    .line 226
    array-length v3, p1

    sub-int/2addr v3, v1

    if-gt v0, v3, :cond_0

    .line 232
    new-array v3, v0, [B

    .line 233
    invoke-static {p1, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 240
    array-length v4, p1

    sub-int/2addr v4, v0

    sub-int/2addr v4, v1

    .line 243
    new-array v5, v4, [B

    add-int/2addr v0, v1

    .line 244
    invoke-static {p1, v0, v5, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    iget-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;

    invoke-interface {p1, v3, v5}, Lcom/google/crypto/tink/PublicKeyVerify;->verify([B[B)V

    return-object v5

    .line 229
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "invalid signature length"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 216
    :cond_1
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "invalid signed payload"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public decrypt([B[B)[B
    .locals 2

    if-nez p2, :cond_0

    .line 204
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->deserializeAndVerify([B)[B

    move-result-object p1

    .line 205
    iget-object p2, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->recipientPrivateKey:Ljava/security/PrivateKey;

    iget-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    iget-object v1, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    invoke-static {p1, p2, v0, v1}, Lcom/google/capillary/HybridRsaUtils;->decrypt([BLjava/security/PrivateKey;Lcom/google/capillary/RsaEcdsaConstants$Padding;Ljavax/crypto/spec/OAEPParameterSpec;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 208
    new-instance p2, Ljava/security/GeneralSecurityException;

    const-string v0, "decryption failed"

    invoke-direct {p2, v0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    .line 200
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "contextInfo must be null because it is unused"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
