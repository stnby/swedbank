.class public final Lcom/google/capillary/internal/CapillaryCiphertext;
.super Lcom/google/protobuf/GeneratedMessageV3;
.source "CapillaryCiphertext.java"

# interfaces
.implements Lcom/google/capillary/internal/CapillaryCiphertextOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/capillary/internal/CapillaryCiphertext$Builder;
    }
.end annotation


# static fields
.field public static final CIPHERTEXT_FIELD_NUMBER:I = 0x4

.field private static final DEFAULT_INSTANCE:Lcom/google/capillary/internal/CapillaryCiphertext;

.field public static final IS_AUTH_KEY_FIELD_NUMBER:I = 0x3

.field public static final KEYCHAIN_UNIQUE_ID_FIELD_NUMBER:I = 0x1

.field public static final KEY_SERIAL_NUMBER_FIELD_NUMBER:I = 0x2

.field private static final PARSER:Lcom/google/protobuf/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/CapillaryCiphertext;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field private ciphertext_:Lcom/google/protobuf/ByteString;

.field private isAuthKey_:Z

.field private keySerialNumber_:I

.field private volatile keychainUniqueId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 649
    new-instance v0, Lcom/google/capillary/internal/CapillaryCiphertext;

    invoke-direct {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/CapillaryCiphertext;

    .line 657
    new-instance v0, Lcom/google/capillary/internal/CapillaryCiphertext$1;

    invoke-direct {v0}, Lcom/google/capillary/internal/CapillaryCiphertext$1;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageV3;-><init>()V

    const/4 v0, -0x1

    .line 158
    iput-byte v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedIsInitialized:B

    const-string v0, ""

    .line 22
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    .line 24
    iput-boolean v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    .line 25
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 3

    .line 37
    invoke-direct {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;-><init>()V

    const/4 p2, 0x0

    :cond_0
    :goto_0
    if-nez p2, :cond_6

    .line 42
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_5

    const/16 v2, 0xa

    if-eq v0, v2, :cond_4

    const/16 v2, 0x10

    if-eq v0, v2, :cond_3

    const/16 v2, 0x18

    if-eq v0, v2, :cond_2

    const/16 v2, 0x22

    if-eq v0, v2, :cond_1

    .line 48
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 71
    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    .line 66
    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    goto :goto_0

    .line 61
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    goto :goto_0

    .line 54
    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readStringRequireUtf8()Ljava/lang/String;

    move-result-object v0

    .line 56
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_5
    :goto_1
    const/4 p2, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 79
    :try_start_1
    new-instance p2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-direct {p2, p1}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/io/IOException;)V

    .line 80
    invoke-virtual {p2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    .line 77
    invoke-virtual {p1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    :goto_2
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->makeExtensionsImmutable()V

    throw p1

    :cond_6
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->makeExtensionsImmutable()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/capillary/internal/CapillaryCiphertext$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryCiphertext;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/GeneratedMessageV3$Builder<",
            "*>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    const/4 p1, -0x1

    .line 158
    iput-byte p1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedIsInitialized:B

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;Lcom/google/capillary/internal/CapillaryCiphertext$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/google/capillary/internal/CapillaryCiphertext;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .line 13
    sget-boolean v0, Lcom/google/capillary/internal/CapillaryCiphertext;->alwaysUseFieldBuilders:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/capillary/internal/CapillaryCiphertext;)Ljava/lang/Object;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic access$402(Lcom/google/capillary/internal/CapillaryCiphertext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/capillary/internal/CapillaryCiphertext;I)I
    .locals 0

    .line 13
    iput p1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/capillary/internal/CapillaryCiphertext;Z)Z
    .locals 0

    .line 13
    iput-boolean p1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/capillary/internal/CapillaryCiphertext;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$800()Lcom/google/protobuf/Parser;
    .locals 1

    .line 13
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 13
    invoke-static {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->checkByteStringIsUtf8(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method public static getDefaultInstance()Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 653
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object v0
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 87
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;
    .locals 1

    .line 313
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/CapillaryCiphertext;

    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->toBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/capillary/internal/CapillaryCiphertext;)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;
    .locals 1

    .line 316
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/CapillaryCiphertext;

    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->toBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;->mergeFrom(Lcom/google/capillary/internal/CapillaryCiphertext;)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 287
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    .line 288
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 294
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    .line 295
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 255
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 261
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 300
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    .line 301
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 307
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    .line 308
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 275
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    .line 276
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 282
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    .line 283
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom([B)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 265
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 271
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object p0
.end method

.method public static parser()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/CapillaryCiphertext;",
            ">;"
        }
    .end annotation

    .line 667
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 214
    :cond_0
    instance-of v1, p1, Lcom/google/capillary/internal/CapillaryCiphertext;

    if-nez v1, :cond_1

    .line 215
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 217
    :cond_1
    check-cast p1, Lcom/google/capillary/internal/CapillaryCiphertext;

    .line 220
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v1

    .line 221
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    .line 222
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeySerialNumber()I

    move-result v1

    .line 223
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeySerialNumber()I

    move-result v3

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    .line 224
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getIsAuthKey()Z

    move-result v1

    .line 225
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->getIsAuthKey()Z

    move-result v3

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_5

    .line 226
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v1

    .line 227
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->getCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_3
    return v0
.end method

.method public getCiphertext()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/capillary/internal/CapillaryCiphertext;
    .locals 1

    .line 676
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/CapillaryCiphertext;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getDefaultInstanceForType()Lcom/google/capillary/internal/CapillaryCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getDefaultInstanceForType()Lcom/google/capillary/internal/CapillaryCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public getIsAuthKey()Z
    .locals 1

    .line 146
    iget-boolean v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    return v0
.end method

.method public getKeySerialNumber()I
    .locals 1

    .line 137
    iget v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    return v0
.end method

.method public getKeychainUniqueId()Ljava/lang/String;
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    .line 104
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 105
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 107
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 109
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 110
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    return-object v0
.end method

.method public getKeychainUniqueIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    .line 120
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 121
    check-cast v0, Ljava/lang/String;

    .line 122
    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 124
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    return-object v0

    .line 127
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/CapillaryCiphertext;",
            ">;"
        }
    .end annotation

    .line 672
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .line 185
    iget v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 189
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeychainUniqueIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 190
    iget-object v2, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/protobuf/GeneratedMessageV3;->computeStringSize(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_1
    iget v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    .line 193
    iget v2, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    .line 194
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_2
    iget-boolean v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    .line 197
    iget-boolean v2, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    .line 198
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_3
    iget-object v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x4

    .line 201
    iget-object v2, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    .line 202
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_4
    iput v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedSize:I

    return v0
.end method

.method public final getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;
    .locals 1

    .line 31
    invoke-static {}, Lcom/google/protobuf/UnknownFieldSet;->getDefaultInstance()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 233
    iget v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedHashCode:I

    if-eqz v0, :cond_0

    .line 234
    iget v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedHashCode:I

    return v0

    :cond_0
    const/16 v0, 0x30b

    .line 237
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x35

    .line 239
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x35

    .line 241
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeySerialNumber()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x35

    .line 244
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getIsAuthKey()Z

    move-result v1

    .line 243
    invoke-static {v1}, Lcom/google/protobuf/Internal;->hashBoolean(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x4

    mul-int/lit8 v0, v0, 0x35

    .line 246
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1d

    .line 247
    iget-object v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->unknownFields:Lcom/google/protobuf/UnknownFieldSet;

    invoke-virtual {v1}, Lcom/google/protobuf/UnknownFieldSet;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    iput v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedHashCode:I

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;
    .locals 3

    .line 92
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryCiphertext_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    const-class v1, Lcom/google/capillary/internal/CapillaryCiphertext;

    const-class v2, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    .line 93
    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;->ensureFieldAccessorsInitialized(Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 2

    .line 160
    iget-byte v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedIsInitialized:B

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    return v0

    .line 164
    :cond_1
    iput-byte v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->memoizedIsInitialized:B

    return v1
.end method

.method public newBuilderForType()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;
    .locals 1

    .line 311
    invoke-static {}, Lcom/google/capillary/internal/CapillaryCiphertext;->newBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;
    .locals 2

    .line 326
    new-instance v0, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;Lcom/google/capillary/internal/CapillaryCiphertext$1;)V

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->newBuilderForType()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->newBuilderForType()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;
    .locals 2

    .line 319
    sget-object v0, Lcom/google/capillary/internal/CapillaryCiphertext;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/CapillaryCiphertext;

    const/4 v1, 0x0

    if-ne p0, v0, :cond_0

    new-instance v0, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;-><init>(Lcom/google/capillary/internal/CapillaryCiphertext$1;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;-><init>(Lcom/google/capillary/internal/CapillaryCiphertext$1;)V

    .line 320
    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;->mergeFrom(Lcom/google/capillary/internal/CapillaryCiphertext;)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->toBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->toBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    .line 170
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeychainUniqueIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 171
    iget-object v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keychainUniqueId_:Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lcom/google/protobuf/GeneratedMessageV3;->writeString(Lcom/google/protobuf/CodedOutputStream;ILjava/lang/Object;)V

    .line 173
    :cond_0
    iget v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    .line 174
    iget v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->keySerialNumber_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 176
    :cond_1
    iget-boolean v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    .line 177
    iget-boolean v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->isAuthKey_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    .line 180
    iget-object v1, p0, Lcom/google/capillary/internal/CapillaryCiphertext;->ciphertext_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    return-void
.end method
