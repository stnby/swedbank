.class public final Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
.super Lcom/google/protobuf/GeneratedMessageV3$Builder;
.source "WrappedRsaEcdsaPublicKey.java"

# interfaces
.implements Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKeyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageV3$Builder<",
        "Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;",
        ">;",
        "Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKeyOrBuilder;"
    }
.end annotation


# instance fields
.field private keyBytes_:Lcom/google/protobuf/ByteString;

.field private padding_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 300
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;-><init>()V

    const-string v0, ""

    .line 418
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 487
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 301
    invoke-direct {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V
    .locals 0

    .line 283
    invoke-direct {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V
    .locals 0

    .line 306
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V

    const-string p1, ""

    .line 418
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 487
    sget-object p1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 307
    invoke-direct {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V
    .locals 0

    .line 283
    invoke-direct {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V

    return-void
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 289
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .line 311
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->access$200()Z

    return-void
.end method


# virtual methods
.method public addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    .line 372
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public build()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 2

    .line 333
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->buildPartial()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    .line 334
    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 335
    :cond_0
    invoke-static {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->newUninitializedMessageException(Lcom/google/protobuf/Message;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/Message;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->build()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->build()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 2

    .line 341
    new-instance v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V

    .line 342
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->access$402(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->access$502(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 344
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onBuilt()V

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/Message;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->buildPartial()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->buildPartial()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 315
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clear()Lcom/google/protobuf/GeneratedMessageV3$Builder;

    const-string v0, ""

    .line 316
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 318
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clear()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clear()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clear()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clear()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    .line 358
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public clearKeyBytes()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 511
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 512
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    .line 362
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public clearPadding()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 468
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPadding()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 469
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public clone()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 349
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clone()Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clone()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clone()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clone()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clone()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clone()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->clone()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 329
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 283
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 325
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method public getKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 492
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getPadding()Ljava/lang/String;
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 424
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 425
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 427
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 428
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    return-object v0

    .line 431
    :cond_0
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPaddingBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .line 439
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 440
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 441
    check-cast v0, Ljava/lang/String;

    .line 442
    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 444
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    return-object v0

    .line 447
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;
    .locals 3

    .line 294
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    const-class v1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    const-class v2, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    .line 295
    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;->ensureFieldAccessorsInitialized(Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 2

    .line 384
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    .line 385
    :cond_0
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPadding()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 386
    invoke-static {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->access$400(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 387
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onChanged()V

    .line 389
    :cond_1
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    if-eq v0, v1, :cond_2

    .line 390
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    .line 392
    :cond_2
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 2

    const/4 v0, 0x0

    .line 406
    :try_start_0
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->access$600()Lcom/google/protobuf/Parser;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    .line 412
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    :cond_0
    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 408
    :try_start_1
    invoke-virtual {p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object p2

    check-cast p2, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409
    :try_start_2
    invoke-virtual {p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->unwrapIOException()Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    move-object v0, p2

    :goto_0
    if-eqz v0, :cond_1

    .line 412
    invoke-virtual {p0, v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    :cond_1
    throw p1
.end method

.method public mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 375
    instance-of v0, p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    if-eqz v0, :cond_0

    .line 376
    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1

    .line 378
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    .line 354
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 502
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 503
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onChanged()V

    return-object p0

    .line 499
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public setPadding(Ljava/lang/String;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 459
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 460
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onChanged()V

    return-object p0

    .line 456
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public setPaddingBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 480
    invoke-static {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->access$700(Lcom/google/protobuf/ByteString;)V

    .line 482
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->padding_:Ljava/lang/Object;

    .line 483
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->onChanged()V

    return-object p0

    .line 478
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    .line 367
    invoke-super {p0, p1, p2, p3}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method
