.class public final Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
.super Lcom/google/protobuf/GeneratedMessageV3;
.source "WrappedRsaEcdsaPublicKey.java"

# interfaces
.implements Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKeyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

.field public static final KEY_BYTES_FIELD_NUMBER:I = 0x2

.field public static final PADDING_FIELD_NUMBER:I = 0x1

.field private static final PARSER:Lcom/google/protobuf/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field private keyBytes_:Lcom/google/protobuf/ByteString;

.field private memoizedIsInitialized:B

.field private volatile padding_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 532
    new-instance v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    invoke-direct {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    .line 540
    new-instance v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;

    invoke-direct {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageV3;-><init>()V

    const/4 v0, -0x1

    .line 128
    iput-byte v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedIsInitialized:B

    const-string v0, ""

    .line 22
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 3

    .line 35
    invoke-direct {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;-><init>()V

    const/4 p2, 0x0

    :cond_0
    :goto_0
    if-nez p2, :cond_4

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12

    if-eq v0, v2, :cond_1

    .line 46
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 59
    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    .line 52
    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readStringRequireUtf8()Ljava/lang/String;

    move-result-object v0

    .line 54
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_3
    :goto_1
    const/4 p2, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 67
    :try_start_1
    new-instance p2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-direct {p2, p1}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/io/IOException;)V

    .line 68
    invoke-virtual {p2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    .line 65
    invoke-virtual {p1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    :goto_2
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->makeExtensionsImmutable()V

    throw p1

    :cond_4
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->makeExtensionsImmutable()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/GeneratedMessageV3$Builder<",
            "*>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    const/4 p1, -0x1

    .line 128
    iput-byte p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedIsInitialized:B

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .line 13
    sget-boolean v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->alwaysUseFieldBuilders:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Ljava/lang/Object;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic access$402(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$600()Lcom/google/protobuf/Parser;
    .locals 1

    .line 13
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 13
    invoke-static {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->checkByteStringIsUtf8(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method public static getDefaultInstance()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 536
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object v0
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 75
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 260
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 263
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 234
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 235
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 241
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 242
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 202
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 208
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 247
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 248
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 254
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 255
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 222
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 223
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 229
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 230
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom([B)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 212
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 218
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object p0
.end method

.method public static parser()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;",
            ">;"
        }
    .end annotation

    .line 550
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 170
    :cond_0
    instance-of v1, p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    if-nez v1, :cond_1

    .line 171
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 173
    :cond_1
    check-cast p1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    .line 176
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPadding()Ljava/lang/String;

    move-result-object v1

    .line 177
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPadding()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    .line 178
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    .line 179
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;
    .locals 1

    .line 559
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public getKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getPadding()Ljava/lang/String;
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    .line 92
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 93
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 95
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 97
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 98
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    return-object v0
.end method

.method public getPaddingBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    .line 108
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 109
    check-cast v0, Ljava/lang/String;

    .line 110
    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 112
    iput-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    return-object v0

    .line 115
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;",
            ">;"
        }
    .end annotation

    .line 555
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .line 149
    iget v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 153
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPaddingBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 154
    iget-object v2, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/protobuf/GeneratedMessageV3;->computeStringSize(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x2

    .line 157
    iget-object v2, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 158
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_2
    iput v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedSize:I

    return v0
.end method

.method public final getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;
    .locals 1

    .line 29
    invoke-static {}, Lcom/google/protobuf/UnknownFieldSet;->getDefaultInstance()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 185
    iget v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedHashCode:I

    if-eqz v0, :cond_0

    .line 186
    iget v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedHashCode:I

    return v0

    :cond_0
    const/16 v0, 0x30b

    .line 189
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x35

    .line 191
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPadding()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x35

    .line 193
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1d

    .line 194
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->unknownFields:Lcom/google/protobuf/UnknownFieldSet;

    invoke-virtual {v1}, Lcom/google/protobuf/UnknownFieldSet;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    iput v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedHashCode:I

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;
    .locals 3

    .line 80
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    const-class v1, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    const-class v2, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    .line 81
    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;->ensureFieldAccessorsInitialized(Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 2

    .line 130
    iget-byte v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedIsInitialized:B

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    return v0

    .line 134
    :cond_1
    iput-byte v1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->memoizedIsInitialized:B

    return v1
.end method

.method public newBuilderForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 1

    .line 258
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->newBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 2

    .line 273
    new-instance v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->newBuilderForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->newBuilderForType()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;
    .locals 2

    .line 266
    sget-object v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    const/4 v1, 0x0

    if-ne p0, v0, :cond_0

    new-instance v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;-><init>(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;-><init>(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$1;)V

    .line 267
    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    .line 140
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPaddingBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 141
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->padding_:Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lcom/google/protobuf/GeneratedMessageV3;->writeString(Lcom/google/protobuf/CodedOutputStream;ILjava/lang/Object;)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    .line 144
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    return-void
.end method
