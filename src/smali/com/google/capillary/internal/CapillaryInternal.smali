.class public final Lcom/google/capillary/internal/CapillaryInternal;
.super Ljava/lang/Object;
.source "CapillaryInternal.java"


# static fields
.field private static descriptor:Lcom/google/protobuf/Descriptors$FileDescriptor;

.field static final internal_static_capillary_internal_CapillaryCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

.field static final internal_static_capillary_internal_CapillaryCiphertext_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

.field static final internal_static_capillary_internal_CapillaryPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

.field static final internal_static_capillary_internal_CapillaryPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

.field static final internal_static_capillary_internal_HybridRsaCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

.field static final internal_static_capillary_internal_HybridRsaCiphertext_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

.field static final internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

.field static final internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

.field static final internal_static_capillary_internal_WrappedWebPushPrivateKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

.field static final internal_static_capillary_internal_WrappedWebPushPrivateKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

.field static final internal_static_capillary_internal_WrappedWebPushPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

.field static final internal_static_capillary_internal_WrappedWebPushPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\n\u0018capillary_internal.proto\u0012\u0012capillary.internal\"S\n\u0013HybridRsaCiphertext\u0012 \n\u0018symmetric_key_ciphertext\u0018\u0001 \u0001(\u000c\u0012\u001a\n\u0012payload_ciphertext\u0018\u0002 \u0001(\u000c\">\n\u0018WrappedRsaEcdsaPublicKey\u0012\u000f\n\u0007padding\u0018\u0001 \u0001(\t\u0012\u0011\n\tkey_bytes\u0018\u0002 \u0001(\u000c\"A\n\u0017WrappedWebPushPublicKey\u0012\u0013\n\u000bauth_secret\u0018\u0001 \u0001(\u000c\u0012\u0011\n\tkey_bytes\u0018\u0002 \u0001(\u000c\"d\n\u0018WrappedWebPushPrivateKey\u0012\u0013\n\u000bauth_secret\u0018\u0001 \u0001(\u000c\u0012\u0018\n\u0010public_key_bytes\u0018\u0002 \u0001(\u000c\u0012\u0019\n\u0011private_key_bytes\u0018\u0003 \u0001(\u000c\"k\n\u0012CapillaryPublicKey\u0012\u001a\n\u0012keychain_u"

    const-string v1, "nique_id\u0018\u0001 \u0001(\t\u0012\u0015\n\rserial_number\u0018\u0002 \u0001(\u0005\u0012\u000f\n\u0007is_auth\u0018\u0003 \u0001(\u0008\u0012\u0011\n\tkey_bytes\u0018\u0004 \u0001(\u000c\"u\n\u0013CapillaryCiphertext\u0012\u001a\n\u0012keychain_unique_id\u0018\u0001 \u0001(\t\u0012\u0019\n\u0011key_serial_number\u0018\u0002 \u0001(\u0005\u0012\u0013\n\u000bis_auth_key\u0018\u0003 \u0001(\u0008\u0012\u0012\n\nciphertext\u0018\u0004 \u0001(\u000cB!\n\u001dcom.google.capillary.internalP\u0001b\u0006proto3"

    .line 55
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/google/capillary/internal/CapillaryInternal$1;

    invoke-direct {v1}, Lcom/google/capillary/internal/CapillaryInternal$1;-><init>()V

    const/4 v2, 0x0

    .line 81
    new-array v3, v2, [Lcom/google/protobuf/Descriptors$FileDescriptor;

    .line 82
    invoke-static {v0, v3, v1}, Lcom/google/protobuf/Descriptors$FileDescriptor;->internalBuildGeneratedFileFrom([Ljava/lang/String;[Lcom/google/protobuf/Descriptors$FileDescriptor;Lcom/google/protobuf/Descriptors$FileDescriptor$InternalDescriptorAssigner;)V

    .line 86
    invoke-static {}, Lcom/google/capillary/internal/CapillaryInternal;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_HybridRsaCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    .line 87
    new-instance v0, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    sget-object v1, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_HybridRsaCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    const-string v2, "SymmetricKeyCiphertext"

    const-string v3, "PayloadCiphertext"

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_HybridRsaCiphertext_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    .line 92
    invoke-static {}, Lcom/google/capillary/internal/CapillaryInternal;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    .line 93
    new-instance v0, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    sget-object v1, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    const-string v2, "Padding"

    const-string v3, "KeyBytes"

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedRsaEcdsaPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    .line 98
    invoke-static {}, Lcom/google/capillary/internal/CapillaryInternal;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    .line 99
    new-instance v0, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    sget-object v1, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    const-string v2, "AuthSecret"

    const-string v3, "KeyBytes"

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    .line 104
    invoke-static {}, Lcom/google/capillary/internal/CapillaryInternal;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPrivateKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    .line 105
    new-instance v0, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    sget-object v1, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPrivateKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    const-string v2, "AuthSecret"

    const-string v3, "PublicKeyBytes"

    const-string v4, "PrivateKeyBytes"

    filled-new-array {v2, v3, v4}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPrivateKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    .line 110
    invoke-static {}, Lcom/google/capillary/internal/CapillaryInternal;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    .line 111
    new-instance v0, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    sget-object v1, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    const-string v2, "KeychainUniqueId"

    const-string v3, "SerialNumber"

    const-string v4, "IsAuth"

    const-string v5, "KeyBytes"

    filled-new-array {v2, v3, v4, v5}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    .line 116
    invoke-static {}, Lcom/google/capillary/internal/CapillaryInternal;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    .line 117
    new-instance v0, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    sget-object v1, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    const-string v2, "KeychainUniqueId"

    const-string v3, "KeySerialNumber"

    const-string v4, "IsAuthKey"

    const-string v5, "Ciphertext"

    filled-new-array {v2, v3, v4, v5}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryCiphertext_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/protobuf/Descriptors$FileDescriptor;)Lcom/google/protobuf/Descriptors$FileDescriptor;
    .locals 0

    .line 6
    sput-object p0, Lcom/google/capillary/internal/CapillaryInternal;->descriptor:Lcom/google/protobuf/Descriptors$FileDescriptor;

    return-object p0
.end method

.method public static getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;
    .locals 1

    .line 50
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->descriptor:Lcom/google/protobuf/Descriptors$FileDescriptor;

    return-object v0
.end method

.method public static registerAllExtensions(Lcom/google/protobuf/ExtensionRegistry;)V
    .locals 0

    .line 14
    invoke-static {p0}, Lcom/google/capillary/internal/CapillaryInternal;->registerAllExtensions(Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method public static registerAllExtensions(Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 0

    return-void
.end method
