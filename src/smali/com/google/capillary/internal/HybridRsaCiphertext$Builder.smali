.class public final Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
.super Lcom/google/protobuf/GeneratedMessageV3$Builder;
.source "HybridRsaCiphertext.java"

# interfaces
.implements Lcom/google/capillary/internal/HybridRsaCiphertextOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/capillary/internal/HybridRsaCiphertext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageV3$Builder<",
        "Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;",
        ">;",
        "Lcom/google/capillary/internal/HybridRsaCiphertextOrBuilder;"
    }
.end annotation


# instance fields
.field private payloadCiphertext_:Lcom/google/protobuf/ByteString;

.field private symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 275
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;-><init>()V

    .line 392
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;

    .line 421
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->payloadCiphertext_:Lcom/google/protobuf/ByteString;

    .line 276
    invoke-direct {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/capillary/internal/HybridRsaCiphertext$1;)V
    .locals 0

    .line 258
    invoke-direct {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V
    .locals 0

    .line 281
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V

    .line 392
    sget-object p1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;

    .line 421
    sget-object p1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->payloadCiphertext_:Lcom/google/protobuf/ByteString;

    .line 282
    invoke-direct {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;Lcom/google/capillary/internal/HybridRsaCiphertext$1;)V
    .locals 0

    .line 258
    invoke-direct {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V

    return-void
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 264
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_HybridRsaCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .line 286
    invoke-static {}, Lcom/google/capillary/internal/HybridRsaCiphertext;->access$200()Z

    return-void
.end method


# virtual methods
.method public addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    .line 347
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    return-object p1
.end method

.method public bridge synthetic addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public build()Lcom/google/capillary/internal/HybridRsaCiphertext;
    .locals 2

    .line 308
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->buildPartial()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lcom/google/capillary/internal/HybridRsaCiphertext;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 310
    :cond_0
    invoke-static {v0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->newUninitializedMessageException(Lcom/google/protobuf/Message;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/Message;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->build()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->build()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/capillary/internal/HybridRsaCiphertext;
    .locals 2

    .line 316
    new-instance v0, Lcom/google/capillary/internal/HybridRsaCiphertext;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/capillary/internal/HybridRsaCiphertext;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;Lcom/google/capillary/internal/HybridRsaCiphertext$1;)V

    .line 317
    iget-object v1, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/capillary/internal/HybridRsaCiphertext;->access$402(Lcom/google/capillary/internal/HybridRsaCiphertext;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 318
    iget-object v1, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->payloadCiphertext_:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/capillary/internal/HybridRsaCiphertext;->access$502(Lcom/google/capillary/internal/HybridRsaCiphertext;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 319
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->onBuilt()V

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/Message;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->buildPartial()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->buildPartial()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 1

    .line 290
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clear()Lcom/google/protobuf/GeneratedMessageV3$Builder;

    .line 291
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;

    .line 293
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->payloadCiphertext_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clear()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clear()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clear()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clear()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    .line 333
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    return-object p1
.end method

.method public bridge synthetic clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    .line 337
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public clearPayloadCiphertext()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 1

    .line 445
    invoke-static {}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getDefaultInstance()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getPayloadCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->payloadCiphertext_:Lcom/google/protobuf/ByteString;

    .line 446
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->onChanged()V

    return-object p0
.end method

.method public clearSymmetricKeyCiphertext()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 1

    .line 416
    invoke-static {}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getDefaultInstance()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getSymmetricKeyCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;

    .line 417
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->onChanged()V

    return-object p0
.end method

.method public clone()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 1

    .line 324
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clone()Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clone()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clone()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clone()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clone()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clone()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->clone()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/capillary/internal/HybridRsaCiphertext;
    .locals 1

    .line 304
    invoke-static {}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getDefaultInstance()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->getDefaultInstanceForType()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->getDefaultInstanceForType()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 300
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_HybridRsaCiphertext_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method public getPayloadCiphertext()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 426
    iget-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->payloadCiphertext_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getSymmetricKeyCiphertext()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 397
    iget-object v0, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;
    .locals 3

    .line 269
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_HybridRsaCiphertext_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    const-class v1, Lcom/google/capillary/internal/HybridRsaCiphertext;

    const-class v2, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    .line 270
    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;->ensureFieldAccessorsInitialized(Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/capillary/internal/HybridRsaCiphertext;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 2

    .line 359
    invoke-static {}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getDefaultInstance()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    .line 360
    :cond_0
    invoke-virtual {p1}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getSymmetricKeyCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    if-eq v0, v1, :cond_1

    .line 361
    invoke-virtual {p1}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getSymmetricKeyCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setSymmetricKeyCiphertext(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    .line 363
    :cond_1
    invoke-virtual {p1}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getPayloadCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    if-eq v0, v1, :cond_2

    .line 364
    invoke-virtual {p1}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getPayloadCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setPayloadCiphertext(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    .line 366
    :cond_2
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->onChanged()V

    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 2

    const/4 v0, 0x0

    .line 380
    :try_start_0
    invoke-static {}, Lcom/google/capillary/internal/HybridRsaCiphertext;->access$600()Lcom/google/protobuf/Parser;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/HybridRsaCiphertext;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    .line 386
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/capillary/internal/HybridRsaCiphertext;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    :cond_0
    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 382
    :try_start_1
    invoke-virtual {p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object p2

    check-cast p2, Lcom/google/capillary/internal/HybridRsaCiphertext;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383
    :try_start_2
    invoke-virtual {p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->unwrapIOException()Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    move-object v0, p2

    :goto_0
    if-eqz v0, :cond_1

    .line 386
    invoke-virtual {p0, v0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/capillary/internal/HybridRsaCiphertext;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    :cond_1
    throw p1
.end method

.method public mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 1

    .line 350
    instance-of v0, p1, Lcom/google/capillary/internal/HybridRsaCiphertext;

    if-eqz v0, :cond_0

    .line 351
    check-cast p1, Lcom/google/capillary/internal/HybridRsaCiphertext;

    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/capillary/internal/HybridRsaCiphertext;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1

    .line 353
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    .line 329
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    return-object p1
.end method

.method public bridge synthetic setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setPayloadCiphertext(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 436
    iput-object p1, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->payloadCiphertext_:Lcom/google/protobuf/ByteString;

    .line 437
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->onChanged()V

    return-object p0

    .line 433
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    .line 342
    invoke-super {p0, p1, p2, p3}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    return-object p1
.end method

.method public bridge synthetic setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setSymmetricKeyCiphertext(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 407
    iput-object p1, p0, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->symmetricKeyCiphertext_:Lcom/google/protobuf/ByteString;

    .line 408
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->onChanged()V

    return-object p0

    .line 404
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public final setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 258
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    return-object p1
.end method
