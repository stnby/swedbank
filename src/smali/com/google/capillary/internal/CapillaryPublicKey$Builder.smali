.class public final Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
.super Lcom/google/protobuf/GeneratedMessageV3$Builder;
.source "CapillaryPublicKey.java"

# interfaces
.implements Lcom/google/capillary/internal/CapillaryPublicKeyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/capillary/internal/CapillaryPublicKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageV3$Builder<",
        "Lcom/google/capillary/internal/CapillaryPublicKey$Builder;",
        ">;",
        "Lcom/google/capillary/internal/CapillaryPublicKeyOrBuilder;"
    }
.end annotation


# instance fields
.field private isAuth_:Z

.field private keyBytes_:Lcom/google/protobuf/ByteString;

.field private keychainUniqueId_:Ljava/lang/Object;

.field private serialNumber_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 353
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;-><init>()V

    const-string v0, ""

    .line 483
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 604
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 354
    invoke-direct {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/capillary/internal/CapillaryPublicKey$1;)V
    .locals 0

    .line 336
    invoke-direct {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V
    .locals 0

    .line 359
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V

    const-string p1, ""

    .line 483
    iput-object p1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 604
    sget-object p1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 360
    invoke-direct {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;Lcom/google/capillary/internal/CapillaryPublicKey$1;)V
    .locals 0

    .line 336
    invoke-direct {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)V

    return-void
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 342
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .line 364
    invoke-static {}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$200()Z

    return-void
.end method


# virtual methods
.method public addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    .line 431
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->addRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public build()Lcom/google/capillary/internal/CapillaryPublicKey;
    .locals 2

    .line 390
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->buildPartial()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryPublicKey;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 392
    :cond_0
    invoke-static {v0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->newUninitializedMessageException(Lcom/google/protobuf/Message;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/Message;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->build()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->build()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/capillary/internal/CapillaryPublicKey;
    .locals 2

    .line 398
    new-instance v0, Lcom/google/capillary/internal/CapillaryPublicKey;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/capillary/internal/CapillaryPublicKey;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;Lcom/google/capillary/internal/CapillaryPublicKey$1;)V

    .line 399
    iget-object v1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$402(Lcom/google/capillary/internal/CapillaryPublicKey;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget v1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->serialNumber_:I

    invoke-static {v0, v1}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$502(Lcom/google/capillary/internal/CapillaryPublicKey;I)I

    .line 401
    iget-boolean v1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->isAuth_:Z

    invoke-static {v0, v1}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$602(Lcom/google/capillary/internal/CapillaryPublicKey;Z)Z

    .line 402
    iget-object v1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$702(Lcom/google/capillary/internal/CapillaryPublicKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 403
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onBuilt()V

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/Message;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->buildPartial()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->buildPartial()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 1

    .line 368
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clear()Lcom/google/protobuf/GeneratedMessageV3$Builder;

    const-string v0, ""

    .line 369
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 371
    iput v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->serialNumber_:I

    .line 373
    iput-boolean v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->isAuth_:Z

    .line 375
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clear()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clear()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clear()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clear()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    .line 417
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clearField(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public clearIsAuth()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 599
    iput-boolean v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->isAuth_:Z

    .line 600
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public clearKeyBytes()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 1

    .line 628
    invoke-static {}, Lcom/google/capillary/internal/CapillaryPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 629
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public clearKeychainUniqueId()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 1

    .line 533
    invoke-static {}, Lcom/google/capillary/internal/CapillaryPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryPublicKey;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 534
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    .line 421
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clearOneof(Lcom/google/protobuf/Descriptors$OneofDescriptor;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public clearSerialNumber()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 573
    iput v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->serialNumber_:I

    .line 574
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public clone()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 1

    .line 408
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->clone()Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clone()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clone()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clone()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clone()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clone()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->clone()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/capillary/internal/CapillaryPublicKey;
    .locals 1

    .line 386
    invoke-static {}, Lcom/google/capillary/internal/CapillaryPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->getDefaultInstanceForType()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 336
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->getDefaultInstanceForType()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 382
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method public getIsAuth()Z
    .locals 1

    .line 583
    iget-boolean v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->isAuth_:Z

    return v0
.end method

.method public getKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 609
    iget-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getKeychainUniqueId()Ljava/lang/String;
    .locals 2

    .line 488
    iget-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 489
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 490
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 492
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 493
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    return-object v0

    .line 496
    :cond_0
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getKeychainUniqueIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .line 504
    iget-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 505
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 506
    check-cast v0, Ljava/lang/String;

    .line 507
    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 509
    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    return-object v0

    .line 512
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getSerialNumber()I
    .locals 1

    .line 557
    iget v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->serialNumber_:I

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;
    .locals 3

    .line 347
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_CapillaryPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    const-class v1, Lcom/google/capillary/internal/CapillaryPublicKey;

    const-class v2, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    .line 348
    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;->ensureFieldAccessorsInitialized(Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/capillary/internal/CapillaryPublicKey;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 2

    .line 443
    invoke-static {}, Lcom/google/capillary/internal/CapillaryPublicKey;->getDefaultInstance()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    .line 444
    :cond_0
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 445
    invoke-static {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$400(Lcom/google/capillary/internal/CapillaryPublicKey;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 446
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    .line 448
    :cond_1
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getSerialNumber()I

    move-result v0

    if-eqz v0, :cond_2

    .line 449
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getSerialNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setSerialNumber(I)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    .line 451
    :cond_2
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getIsAuth()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 452
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getIsAuth()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setIsAuth(Z)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    .line 454
    :cond_3
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    if-eq v0, v1, :cond_4

    .line 455
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    .line 457
    :cond_4
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 2

    const/4 v0, 0x0

    .line 471
    :try_start_0
    invoke-static {}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$800()Lcom/google/protobuf/Parser;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/CapillaryPublicKey;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    .line 477
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/CapillaryPublicKey;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    :cond_0
    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 473
    :try_start_1
    invoke-virtual {p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object p2

    check-cast p2, Lcom/google/capillary/internal/CapillaryPublicKey;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474
    :try_start_2
    invoke-virtual {p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->unwrapIOException()Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    move-object v0, p2

    :goto_0
    if-eqz v0, :cond_1

    .line 477
    invoke-virtual {p0, v0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/CapillaryPublicKey;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    :cond_1
    throw p1
.end method

.method public mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 1

    .line 434
    instance-of v0, p1, Lcom/google/capillary/internal/CapillaryPublicKey;

    if-eqz v0, :cond_0

    .line 435
    check-cast p1, Lcom/google/capillary/internal/CapillaryPublicKey;

    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/CapillaryPublicKey;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1

    .line 437
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    .line 413
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setField(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setIsAuth(Z)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    .line 590
    iput-boolean p1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->isAuth_:Z

    .line 591
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public setKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 619
    iput-object p1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 620
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0

    .line 616
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public setKeychainUniqueId(Ljava/lang/String;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 524
    iput-object p1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 525
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0

    .line 521
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public setKeychainUniqueIdBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 545
    invoke-static {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->access$900(Lcom/google/protobuf/ByteString;)V

    .line 547
    iput-object p1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->keychainUniqueId_:Ljava/lang/Object;

    .line 548
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0

    .line 543
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    .line 426
    invoke-super {p0, p1, p2, p3}, Lcom/google/protobuf/GeneratedMessageV3$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;

    move-result-object p1

    check-cast p1, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    return-object p1
.end method

.method public bridge synthetic setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setRepeatedField(Lcom/google/protobuf/Descriptors$FieldDescriptor;ILjava/lang/Object;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setSerialNumber(I)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    .line 564
    iput p1, p0, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->serialNumber_:I

    .line 565
    invoke-virtual {p0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->onChanged()V

    return-object p0
.end method

.method public final setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessageV3$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 336
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method
