.class public final Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
.super Lcom/google/protobuf/GeneratedMessageV3;
.source "WrappedWebPushPrivateKey.java"

# interfaces
.implements Lcom/google/capillary/internal/WrappedWebPushPrivateKeyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;
    }
.end annotation


# static fields
.field public static final AUTH_SECRET_FIELD_NUMBER:I = 0x1

.field private static final DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

.field private static final PARSER:Lcom/google/protobuf/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedWebPushPrivateKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final PRIVATE_KEY_BYTES_FIELD_NUMBER:I = 0x3

.field public static final PUBLIC_KEY_BYTES_FIELD_NUMBER:I = 0x2

.field private static final serialVersionUID:J


# instance fields
.field private authSecret_:Lcom/google/protobuf/ByteString;

.field private memoizedIsInitialized:B

.field private privateKeyBytes_:Lcom/google/protobuf/ByteString;

.field private publicKeyBytes_:Lcom/google/protobuf/ByteString;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 527
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    invoke-direct {v0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    .line 535
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$1;

    invoke-direct {v0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$1;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageV3;-><init>()V

    const/4 v0, -0x1

    .line 117
    iput-byte v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedIsInitialized:B

    .line 22
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;

    .line 23
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    .line 24
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 3

    .line 36
    invoke-direct {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;-><init>()V

    const/4 p2, 0x0

    :cond_0
    :goto_0
    if-nez p2, :cond_5

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    const/16 v2, 0xa

    if-eq v0, v2, :cond_3

    const/16 v2, 0x12

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1a

    if-eq v0, v2, :cond_1

    .line 47
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    .line 54
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_4
    :goto_1
    const/4 p2, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 72
    :try_start_1
    new-instance p2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-direct {p2, p1}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/io/IOException;)V

    .line 73
    invoke-virtual {p2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    .line 70
    invoke-virtual {p1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :goto_2
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->makeExtensionsImmutable()V

    throw p1

    :cond_5
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->makeExtensionsImmutable()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/capillary/internal/WrappedWebPushPrivateKey$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/GeneratedMessageV3$Builder<",
            "*>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    const/4 p1, -0x1

    .line 117
    iput-byte p1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedIsInitialized:B

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;Lcom/google/capillary/internal/WrappedWebPushPrivateKey$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .line 13
    sget-boolean v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->alwaysUseFieldBuilders:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/capillary/internal/WrappedWebPushPrivateKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/capillary/internal/WrappedWebPushPrivateKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/capillary/internal/WrappedWebPushPrivateKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$700()Lcom/google/protobuf/Parser;
    .locals 1

    .line 13
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public static getDefaultInstance()Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 531
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object v0
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 80
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPrivateKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;
    .locals 1

    .line 261
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/capillary/internal/WrappedWebPushPrivateKey;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;
    .locals 1

    .line 264
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedWebPushPrivateKey;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 235
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 236
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 242
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 243
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 203
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 209
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 248
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 249
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 255
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 256
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 223
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 224
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 230
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 231
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom([B)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 213
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 219
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object p0
.end method

.method public static parser()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedWebPushPrivateKey;",
            ">;"
        }
    .end annotation

    .line 545
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 167
    :cond_0
    instance-of v1, p1, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    if-nez v1, :cond_1

    .line 168
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 170
    :cond_1
    check-cast p1, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    .line 173
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v1

    .line 174
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    .line 175
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPublicKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    .line 176
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPublicKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    .line 177
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPrivateKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    .line 178
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPrivateKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    return v0
.end method

.method public getAuthSecret()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedWebPushPrivateKey;
    .locals 1

    .line 554
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    move-result-object v0

    return-object v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedWebPushPrivateKey;",
            ">;"
        }
    .end annotation

    .line 550
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getPrivateKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getPublicKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .line 141
    iget v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 145
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 146
    iget-object v2, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;

    .line 147
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x2

    .line 150
    iget-object v2, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    .line 151
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x3

    .line 154
    iget-object v2, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    .line 155
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_3
    iput v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedSize:I

    return v0
.end method

.method public final getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;
    .locals 1

    .line 30
    invoke-static {}, Lcom/google/protobuf/UnknownFieldSet;->getDefaultInstance()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 184
    iget v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedHashCode:I

    if-eqz v0, :cond_0

    .line 185
    iget v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedHashCode:I

    return v0

    :cond_0
    const/16 v0, 0x30b

    .line 188
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x35

    .line 190
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x35

    .line 192
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPublicKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x35

    .line 194
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPrivateKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1d

    .line 195
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->unknownFields:Lcom/google/protobuf/UnknownFieldSet;

    invoke-virtual {v1}, Lcom/google/protobuf/UnknownFieldSet;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    iput v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedHashCode:I

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;
    .locals 3

    .line 85
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPrivateKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    const-class v1, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    const-class v2, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;->ensureFieldAccessorsInitialized(Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 2

    .line 119
    iget-byte v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedIsInitialized:B

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    return v0

    .line 123
    :cond_1
    iput-byte v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->memoizedIsInitialized:B

    return v1
.end method

.method public newBuilderForType()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;
    .locals 1

    .line 259
    invoke-static {}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->newBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;
    .locals 2

    .line 274
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;Lcom/google/capillary/internal/WrappedWebPushPrivateKey$1;)V

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->newBuilderForType()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->newBuilderForType()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;
    .locals 2

    .line 267
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    const/4 v1, 0x0

    if-ne p0, v0, :cond_0

    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;-><init>(Lcom/google/capillary/internal/WrappedWebPushPrivateKey$1;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;-><init>(Lcom/google/capillary/internal/WrappedWebPushPrivateKey$1;)V

    .line 268
    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedWebPushPrivateKey;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 130
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->authSecret_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    .line 133
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->publicKeyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    .line 136
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->privateKeyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    return-void
.end method
