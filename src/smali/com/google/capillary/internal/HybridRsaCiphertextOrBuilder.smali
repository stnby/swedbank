.class public interface abstract Lcom/google/capillary/internal/HybridRsaCiphertextOrBuilder;
.super Ljava/lang/Object;
.source "HybridRsaCiphertextOrBuilder.java"

# interfaces
.implements Lcom/google/protobuf/MessageOrBuilder;


# virtual methods
.method public abstract getPayloadCiphertext()Lcom/google/protobuf/ByteString;
.end method

.method public abstract getSymmetricKeyCiphertext()Lcom/google/protobuf/ByteString;
.end method
