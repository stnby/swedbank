.class public interface abstract Lcom/google/capillary/internal/WrappedWebPushPrivateKeyOrBuilder;
.super Ljava/lang/Object;
.source "WrappedWebPushPrivateKeyOrBuilder.java"

# interfaces
.implements Lcom/google/protobuf/MessageOrBuilder;


# virtual methods
.method public abstract getAuthSecret()Lcom/google/protobuf/ByteString;
.end method

.method public abstract getPrivateKeyBytes()Lcom/google/protobuf/ByteString;
.end method

.method public abstract getPublicKeyBytes()Lcom/google/protobuf/ByteString;
.end method
