.class public interface abstract Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKeyOrBuilder;
.super Ljava/lang/Object;
.source "WrappedRsaEcdsaPublicKeyOrBuilder.java"

# interfaces
.implements Lcom/google/protobuf/MessageOrBuilder;


# virtual methods
.method public abstract getKeyBytes()Lcom/google/protobuf/ByteString;
.end method

.method public abstract getPadding()Ljava/lang/String;
.end method

.method public abstract getPaddingBytes()Lcom/google/protobuf/ByteString;
.end method
