.class public interface abstract Lcom/google/capillary/internal/CapillaryPublicKeyOrBuilder;
.super Ljava/lang/Object;
.source "CapillaryPublicKeyOrBuilder.java"

# interfaces
.implements Lcom/google/protobuf/MessageOrBuilder;


# virtual methods
.method public abstract getIsAuth()Z
.end method

.method public abstract getKeyBytes()Lcom/google/protobuf/ByteString;
.end method

.method public abstract getKeychainUniqueId()Ljava/lang/String;
.end method

.method public abstract getKeychainUniqueIdBytes()Lcom/google/protobuf/ByteString;
.end method

.method public abstract getSerialNumber()I
.end method
