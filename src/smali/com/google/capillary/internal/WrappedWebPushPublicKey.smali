.class public final Lcom/google/capillary/internal/WrappedWebPushPublicKey;
.super Lcom/google/protobuf/GeneratedMessageV3;
.source "WrappedWebPushPublicKey.java"

# interfaces
.implements Lcom/google/capillary/internal/WrappedWebPushPublicKeyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;
    }
.end annotation


# static fields
.field public static final AUTH_SECRET_FIELD_NUMBER:I = 0x1

.field private static final DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPublicKey;

.field public static final KEY_BYTES_FIELD_NUMBER:I = 0x2

.field private static final PARSER:Lcom/google/protobuf/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedWebPushPublicKey;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field private authSecret_:Lcom/google/protobuf/ByteString;

.field private keyBytes_:Lcom/google/protobuf/ByteString;

.field private memoizedIsInitialized:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 466
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    invoke-direct {v0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    .line 474
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey$1;

    invoke-direct {v0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$1;-><init>()V

    sput-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageV3;-><init>()V

    const/4 v0, -0x1

    .line 102
    iput-byte v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedIsInitialized:B

    .line 22
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;

    .line 23
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 3

    .line 35
    invoke-direct {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;-><init>()V

    const/4 p2, 0x0

    :cond_0
    :goto_0
    if-nez p2, :cond_4

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12

    if-eq v0, v2, :cond_1

    .line 46
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 58
    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_3
    :goto_1
    const/4 p2, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 66
    :try_start_1
    new-instance p2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-direct {p2, p1}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/io/IOException;)V

    .line 67
    invoke-virtual {p2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    .line 64
    invoke-virtual {p1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :goto_2
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->makeExtensionsImmutable()V

    throw p1

    :cond_4
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->makeExtensionsImmutable()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/capillary/internal/WrappedWebPushPublicKey$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/GeneratedMessageV3$Builder<",
            "*>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    const/4 p1, -0x1

    .line 102
    iput-byte p1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedIsInitialized:B

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;Lcom/google/capillary/internal/WrappedWebPushPublicKey$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;-><init>(Lcom/google/protobuf/GeneratedMessageV3$Builder;)V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .line 13
    sget-boolean v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->alwaysUseFieldBuilders:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/capillary/internal/WrappedWebPushPublicKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/capillary/internal/WrappedWebPushPublicKey;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$600()Lcom/google/protobuf/Parser;
    .locals 1

    .line 13
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public static getDefaultInstance()Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 470
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object v0
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    .line 74
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPublicKey_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;
    .locals 1

    .line 235
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/capillary/internal/WrappedWebPushPublicKey;)Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;
    .locals 1

    .line 238
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedWebPushPublicKey;)Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 209
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 210
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 216
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 217
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseDelimitedWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 177
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 183
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 222
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 223
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 229
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 230
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 197
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 198
    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 204
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    .line 205
    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->parseWithIOException(Lcom/google/protobuf/Parser;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom([B)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 187
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 193
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object p0
.end method

.method public static parser()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedWebPushPublicKey;",
            ">;"
        }
    .end annotation

    .line 484
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 145
    :cond_0
    instance-of v1, p1, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    if-nez v1, :cond_1

    .line 146
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessageV3;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 148
    :cond_1
    check-cast p1, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    .line 151
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v1

    .line 152
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    .line 153
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    .line 154
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public getAuthSecret()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedWebPushPublicKey;
    .locals 1

    .line 493
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getDefaultInstanceForType()Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public getKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/google/capillary/internal/WrappedWebPushPublicKey;",
            ">;"
        }
    .end annotation

    .line 489
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .line 123
    iget v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 128
    iget-object v2, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;

    .line 129
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x2

    .line 132
    iget-object v2, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    .line 133
    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_2
    iput v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedSize:I

    return v0
.end method

.method public final getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;
    .locals 1

    .line 29
    invoke-static {}, Lcom/google/protobuf/UnknownFieldSet;->getDefaultInstance()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 160
    iget v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedHashCode:I

    if-eqz v0, :cond_0

    .line 161
    iget v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedHashCode:I

    return v0

    :cond_0
    const/16 v0, 0x30b

    .line 164
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x35

    .line 166
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    add-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x35

    .line 168
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1d

    .line 169
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->unknownFields:Lcom/google/protobuf/UnknownFieldSet;

    invoke-virtual {v1}, Lcom/google/protobuf/UnknownFieldSet;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    iput v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedHashCode:I

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;
    .locals 3

    .line 79
    sget-object v0, Lcom/google/capillary/internal/CapillaryInternal;->internal_static_capillary_internal_WrappedWebPushPublicKey_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    const-class v1, Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    const-class v2, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    .line 80
    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;->ensureFieldAccessorsInitialized(Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/GeneratedMessageV3$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 2

    .line 104
    iget-byte v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedIsInitialized:B

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    return v0

    .line 108
    :cond_1
    iput-byte v1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->memoizedIsInitialized:B

    return v1
.end method

.method public newBuilderForType()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;
    .locals 1

    .line 233
    invoke-static {}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->newBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;
    .locals 2

    .line 248
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;Lcom/google/capillary/internal/WrappedWebPushPublicKey$1;)V

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->newBuilderForType()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/protobuf/Message$Builder;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->newBuilderForType(Lcom/google/protobuf/GeneratedMessageV3$BuilderParent;)Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->newBuilderForType()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;
    .locals 2

    .line 241
    sget-object v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->DEFAULT_INSTANCE:Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    const/4 v1, 0x0

    if-ne p0, v0, :cond_0

    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;-><init>(Lcom/google/capillary/internal/WrappedWebPushPublicKey$1;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    invoke-direct {v0, v1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;-><init>(Lcom/google/capillary/internal/WrappedWebPushPublicKey$1;)V

    .line 242
    invoke-virtual {v0, p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;->mergeFrom(Lcom/google/capillary/internal/WrappedWebPushPublicKey;)Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/Message$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->toBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 115
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->authSecret_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    .line 118
    iget-object v1, p0, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->keyBytes_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    return-void
.end method
