.class public interface abstract Lcom/google/capillary/internal/CapillaryCiphertextOrBuilder;
.super Ljava/lang/Object;
.source "CapillaryCiphertextOrBuilder.java"

# interfaces
.implements Lcom/google/protobuf/MessageOrBuilder;


# virtual methods
.method public abstract getCiphertext()Lcom/google/protobuf/ByteString;
.end method

.method public abstract getIsAuthKey()Z
.end method

.method public abstract getKeySerialNumber()I
.end method

.method public abstract getKeychainUniqueId()Ljava/lang/String;
.end method

.method public abstract getKeychainUniqueIdBytes()Lcom/google/protobuf/ByteString;
.end method
