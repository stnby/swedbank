.class public final Lcom/google/capillary/HybridRsaUtils;
.super Ljava/lang/Object;
.source "HybridRsaUtils.java"


# static fields
.field private static final SYMMETRIC_KEY_TEMPLATE:Lcom/google/crypto/tink/proto/KeyTemplate;

.field private static final emptyEad:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    sget-object v0, Lcom/google/crypto/tink/aead/AeadKeyTemplates;->AES128_GCM:Lcom/google/crypto/tink/proto/KeyTemplate;

    sput-object v0, Lcom/google/capillary/HybridRsaUtils;->SYMMETRIC_KEY_TEMPLATE:Lcom/google/crypto/tink/proto/KeyTemplate;

    const/4 v0, 0x0

    .line 42
    new-array v0, v0, [B

    sput-object v0, Lcom/google/capillary/HybridRsaUtils;->emptyEad:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decrypt([BLjava/security/PrivateKey;Lcom/google/capillary/RsaEcdsaConstants$Padding;Ljavax/crypto/spec/OAEPParameterSpec;)[B
    .locals 3

    .line 103
    :try_start_0
    invoke-static {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext;->parseFrom([B)Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object p0
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1

    .line 109
    invoke-virtual {p2}, Lcom/google/capillary/RsaEcdsaConstants$Padding;->getTransformation()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 110
    sget-object v1, Lcom/google/capillary/RsaEcdsaConstants$Padding;->OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    const/4 v2, 0x2

    if-ne p2, v1, :cond_0

    .line 111
    invoke-virtual {v0, v2, p1, p3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    goto :goto_0

    .line 113
    :cond_0
    invoke-virtual {v0, v2, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 117
    :goto_0
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getSymmetricKeyCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object p1

    .line 118
    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1

    .line 122
    :try_start_1
    invoke-static {p1}, Lcom/google/crypto/tink/BinaryKeysetReader;->withBytes([B)Lcom/google/crypto/tink/KeysetReader;

    move-result-object p1

    invoke-static {p1}, Lcom/google/crypto/tink/CleartextKeysetHandle;->read(Lcom/google/crypto/tink/KeysetReader;)Lcom/google/crypto/tink/KeysetHandle;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 128
    invoke-static {p1}, Lcom/google/crypto/tink/aead/AeadFactory;->getPrimitive(Lcom/google/crypto/tink/KeysetHandle;)Lcom/google/crypto/tink/Aead;

    move-result-object p1

    .line 129
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext;->getPayloadCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object p0

    invoke-virtual {p0}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object p0

    .line 130
    sget-object p2, Lcom/google/capillary/HybridRsaUtils;->emptyEad:[B

    invoke-interface {p1, p0, p2}, Lcom/google/crypto/tink/Aead;->decrypt([B[B)[B

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    .line 124
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "hybrid rsa decryption failed: "

    invoke-direct {p1, p2, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    move-exception p0

    .line 105
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "hybrid rsa decryption failed: "

    invoke-direct {p1, p2, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static encrypt([BLjava/security/PublicKey;Lcom/google/capillary/RsaEcdsaConstants$Padding;Ljavax/crypto/spec/OAEPParameterSpec;)[B
    .locals 3

    .line 58
    invoke-virtual {p2}, Lcom/google/capillary/RsaEcdsaConstants$Padding;->getTransformation()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 59
    sget-object v1, Lcom/google/capillary/RsaEcdsaConstants$Padding;->OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    const/4 v2, 0x1

    if-ne p2, v1, :cond_0

    .line 60
    invoke-virtual {v0, v2, p1, p3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {v0, v2, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 66
    :goto_0
    sget-object p1, Lcom/google/capillary/HybridRsaUtils;->SYMMETRIC_KEY_TEMPLATE:Lcom/google/crypto/tink/proto/KeyTemplate;

    invoke-static {p1}, Lcom/google/crypto/tink/KeysetHandle;->generateNew(Lcom/google/crypto/tink/proto/KeyTemplate;)Lcom/google/crypto/tink/KeysetHandle;

    move-result-object p1

    .line 67
    new-instance p2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 70
    :try_start_0
    invoke-static {p2}, Lcom/google/crypto/tink/BinaryKeysetWriter;->withOutputStream(Ljava/io/OutputStream;)Lcom/google/crypto/tink/KeysetWriter;

    move-result-object p3

    .line 69
    invoke-static {p1, p3}, Lcom/google/crypto/tink/CleartextKeysetHandle;->write(Lcom/google/crypto/tink/KeysetHandle;Lcom/google/crypto/tink/KeysetWriter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p2

    .line 75
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p2

    .line 78
    invoke-static {p1}, Lcom/google/crypto/tink/aead/AeadFactory;->getPrimitive(Lcom/google/crypto/tink/KeysetHandle;)Lcom/google/crypto/tink/Aead;

    move-result-object p1

    .line 79
    sget-object p3, Lcom/google/capillary/HybridRsaUtils;->emptyEad:[B

    invoke-interface {p1, p0, p3}, Lcom/google/crypto/tink/Aead;->encrypt([B[B)[B

    move-result-object p0

    .line 81
    invoke-static {}, Lcom/google/capillary/internal/HybridRsaCiphertext;->newBuilder()Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    .line 82
    invoke-static {p2}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setSymmetricKeyCiphertext(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p1

    .line 83
    invoke-static {p0}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->setPayloadCiphertext(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;

    move-result-object p0

    .line 84
    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext$Builder;->build()Lcom/google/capillary/internal/HybridRsaCiphertext;

    move-result-object p0

    invoke-virtual {p0}, Lcom/google/capillary/internal/HybridRsaCiphertext;->toByteArray()[B

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    .line 72
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "hybrid rsa encryption failed: "

    invoke-direct {p1, p2, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method
