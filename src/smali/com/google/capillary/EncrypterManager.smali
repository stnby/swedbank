.class public abstract Lcom/google/capillary/EncrypterManager;
.super Ljava/lang/Object;
.source "EncrypterManager.java"


# instance fields
.field private capillaryPublicKey:Lcom/google/capillary/internal/CapillaryPublicKey;

.field private encrypter:Lcom/google/crypto/tink/HybridEncrypt;

.field private isLoaded:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized clearPublicKey()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 88
    :try_start_0
    iput-boolean v0, p0, Lcom/google/capillary/EncrypterManager;->isLoaded:Z

    const/4 v0, 0x0

    .line 89
    iput-object v0, p0, Lcom/google/capillary/EncrypterManager;->capillaryPublicKey:Lcom/google/capillary/internal/CapillaryPublicKey;

    .line 90
    iput-object v0, p0, Lcom/google/capillary/EncrypterManager;->encrypter:Lcom/google/crypto/tink/HybridEncrypt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 87
    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized encrypt([B)[B
    .locals 2

    monitor-enter p0

    .line 72
    :try_start_0
    iget-boolean v0, p0, Lcom/google/capillary/EncrypterManager;->isLoaded:Z

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/capillary/EncrypterManager;->encrypter:Lcom/google/crypto/tink/HybridEncrypt;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/crypto/tink/HybridEncrypt;->encrypt([B[B)[B

    move-result-object p1

    .line 76
    invoke-static {}, Lcom/google/capillary/internal/CapillaryCiphertext;->newBuilder()Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/capillary/EncrypterManager;->capillaryPublicKey:Lcom/google/capillary/internal/CapillaryPublicKey;

    .line 77
    invoke-virtual {v1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;->setKeychainUniqueId(Ljava/lang/String;)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/capillary/EncrypterManager;->capillaryPublicKey:Lcom/google/capillary/internal/CapillaryPublicKey;

    .line 78
    invoke-virtual {v1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getSerialNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;->setKeySerialNumber(I)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/capillary/EncrypterManager;->capillaryPublicKey:Lcom/google/capillary/internal/CapillaryPublicKey;

    .line 79
    invoke-virtual {v1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getIsAuth()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;->setIsAuthKey(Z)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object v0

    .line 80
    invoke-static {p1}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;->setCiphertext(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/CapillaryCiphertext$Builder;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext$Builder;->build()Lcom/google/capillary/internal/CapillaryCiphertext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->toByteArray()[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    monitor-exit p0

    return-object p1

    .line 73
    :cond_0
    :try_start_1
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "public key is not loaded"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    .line 71
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized loadPublicKey([B)V
    .locals 2

    monitor-enter p0

    .line 51
    :try_start_0
    invoke-static {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->parseFrom([B)Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/EncrypterManager;->capillaryPublicKey:Lcom/google/capillary/internal/CapillaryPublicKey;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :try_start_1
    iget-object p1, p0, Lcom/google/capillary/EncrypterManager;->capillaryPublicKey:Lcom/google/capillary/internal/CapillaryPublicKey;

    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/capillary/EncrypterManager;->rawLoadPublicKey([B)Lcom/google/crypto/tink/HybridEncrypt;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/EncrypterManager;->encrypter:Lcom/google/crypto/tink/HybridEncrypt;

    const/4 p1, 0x1

    .line 56
    iput-boolean p1, p0, Lcom/google/capillary/EncrypterManager;->isLoaded:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 53
    :try_start_2
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "unable to parse public key"

    invoke-direct {v0, v1, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 50
    :goto_0
    monitor-exit p0

    throw p1
.end method

.method abstract rawLoadPublicKey([B)Lcom/google/crypto/tink/HybridEncrypt;
.end method
