.class public final enum Lcom/google/capillary/RsaEcdsaConstants$Padding;
.super Ljava/lang/Enum;
.source "RsaEcdsaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/capillary/RsaEcdsaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Padding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/capillary/RsaEcdsaConstants$Padding;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/capillary/RsaEcdsaConstants$Padding;

.field public static final enum OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

.field public static final enum PKCS1:Lcom/google/capillary/RsaEcdsaConstants$Padding;

.field private static final PREFIX:Ljava/lang/String; = "RSA/ECB/"


# instance fields
.field private final padding:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 36
    new-instance v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;

    const-string v1, "OAEP"

    const-string v2, "OAEPPadding"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/google/capillary/RsaEcdsaConstants$Padding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    .line 37
    new-instance v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;

    const-string v1, "PKCS1"

    const-string v2, "PKCS1Padding"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/capillary/RsaEcdsaConstants$Padding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->PKCS1:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    const/4 v0, 0x2

    .line 35
    new-array v0, v0, [Lcom/google/capillary/RsaEcdsaConstants$Padding;

    sget-object v1, Lcom/google/capillary/RsaEcdsaConstants$Padding;->OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/capillary/RsaEcdsaConstants$Padding;->PKCS1:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->$VALUES:[Lcom/google/capillary/RsaEcdsaConstants$Padding;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput-object p3, p0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->padding:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/capillary/RsaEcdsaConstants$Padding;
    .locals 1

    .line 35
    const-class v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/RsaEcdsaConstants$Padding;

    return-object p0
.end method

.method public static values()[Lcom/google/capillary/RsaEcdsaConstants$Padding;
    .locals 1

    .line 35
    sget-object v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->$VALUES:[Lcom/google/capillary/RsaEcdsaConstants$Padding;

    invoke-virtual {v0}, [Lcom/google/capillary/RsaEcdsaConstants$Padding;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/capillary/RsaEcdsaConstants$Padding;

    return-object v0
.end method


# virtual methods
.method public getTransformation()Ljava/lang/String;
    .locals 2

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RSA/ECB/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->padding:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
