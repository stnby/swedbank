.class public final Lcom/google/capillary/android/RsaEcdsaKeyManager;
.super Lcom/google/capillary/android/KeyManager;
.source "RsaEcdsaKeyManager.java"


# static fields
.field private static final KEY_CHAIN_ID_PREFIX:Ljava/lang/String; = "rsa_ecdsa_"

.field private static instances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/google/capillary/android/RsaEcdsaKeyManager;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final keyStore:Ljava/security/KeyStore;

.field private senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->instances:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 2

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rsa_ecdsa_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/google/capillary/android/KeyManager;-><init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;)V

    .line 60
    invoke-static {p4}, Lcom/google/crypto/tink/BinaryKeysetReader;->withInputStream(Ljava/io/InputStream;)Lcom/google/crypto/tink/KeysetReader;

    move-result-object p1

    invoke-static {p1}, Lcom/google/crypto/tink/CleartextKeysetHandle;->read(Lcom/google/crypto/tink/KeysetReader;)Lcom/google/crypto/tink/KeysetHandle;

    move-result-object p1

    .line 61
    invoke-static {p1}, Lcom/google/crypto/tink/signature/PublicKeyVerifyFactory;->getPrimitive(Lcom/google/crypto/tink/KeysetHandle;)Lcom/google/crypto/tink/PublicKeyVerify;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;

    .line 62
    invoke-virtual {p2}, Lcom/google/capillary/android/Utils;->loadKeyStore()Ljava/security/KeyStore;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keyStore:Ljava/security/KeyStore;

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Ljava/lang/String;Ljava/io/InputStream;)Lcom/google/capillary/android/RsaEcdsaKeyManager;
    .locals 3

    const-class v0, Lcom/google/capillary/android/RsaEcdsaKeyManager;

    monitor-enter v0

    .line 80
    :try_start_0
    sget-object v1, Lcom/google/capillary/android/RsaEcdsaKeyManager;->instances:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    sget-object p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->instances:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;

    .line 82
    invoke-direct {p0, p2}, Lcom/google/capillary/android/RsaEcdsaKeyManager;->updateSenderVerifier(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit v0

    return-object p0

    .line 85
    :cond_0
    :try_start_1
    new-instance v1, Lcom/google/capillary/android/RsaEcdsaKeyManager;

    .line 86
    invoke-static {}, Lcom/google/capillary/android/Utils;->getInstance()Lcom/google/capillary/android/Utils;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/google/capillary/android/RsaEcdsaKeyManager;-><init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;Ljava/io/InputStream;)V

    .line 87
    sget-object p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->instances:Ljava/util/Map;

    invoke-interface {p0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    .line 79
    monitor-exit v0

    throw p0
.end method

.method private declared-synchronized updateSenderVerifier(Ljava/io/InputStream;)V
    .locals 0

    monitor-enter p0

    .line 94
    :try_start_0
    invoke-static {p1}, Lcom/google/crypto/tink/BinaryKeysetReader;->withInputStream(Ljava/io/InputStream;)Lcom/google/crypto/tink/KeysetReader;

    move-result-object p1

    invoke-static {p1}, Lcom/google/crypto/tink/CleartextKeysetHandle;->read(Lcom/google/crypto/tink/KeysetReader;)Lcom/google/crypto/tink/KeysetHandle;

    move-result-object p1

    .line 95
    invoke-static {p1}, Lcom/google/crypto/tink/signature/PublicKeyVerifyFactory;->getPrimitive(Lcom/google/crypto/tink/KeysetHandle;)Lcom/google/crypto/tink/PublicKeyVerify;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 92
    monitor-exit p0

    throw p1
.end method


# virtual methods
.method declared-synchronized rawDeleteKeyPair(Z)V
    .locals 2

    monitor-enter p0

    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keyStore:Ljava/security/KeyStore;

    iget-object v1, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keychainId:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->deleteKeyPair(Ljava/security/KeyStore;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 129
    monitor-exit p0

    throw p1
.end method

.method declared-synchronized rawGenerateKeyPair(Z)V
    .locals 2

    monitor-enter p0

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keychainId:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->generateKeyPair(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 100
    monitor-exit p0

    throw p1
.end method

.method declared-synchronized rawGetDecrypter(Z)Lcom/google/crypto/tink/HybridDecrypt;
    .locals 2

    monitor-enter p0

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keyStore:Ljava/security/KeyStore;

    iget-object v1, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keychainId:Ljava/lang/String;

    .line 119
    invoke-static {v0, v1, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getPrivateKey(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljava/security/PrivateKey;

    move-result-object p1

    .line 120
    new-instance v0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;

    invoke-direct {v0}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;-><init>()V

    .line 121
    invoke-virtual {v0, p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->withRecipientPrivateKey(Ljava/security/PrivateKey;)Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;

    .line 122
    invoke-virtual {p1, v0}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->withSenderVerifier(Lcom/google/crypto/tink/PublicKeyVerify;)Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;

    move-result-object p1

    .line 123
    invoke-static {}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getCompatibleRsaPadding()Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->withPadding(Lcom/google/capillary/RsaEcdsaConstants$Padding;)Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->build()Lcom/google/capillary/RsaEcdsaHybridDecrypt;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 117
    monitor-exit p0

    throw p1
.end method

.method declared-synchronized rawGetPublicKey(Z)[B
    .locals 2

    monitor-enter p0

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keyStore:Ljava/security/KeyStore;

    iget-object v1, p0, Lcom/google/capillary/android/RsaEcdsaKeyManager;->keychainId:Ljava/lang/String;

    .line 108
    invoke-static {v0, v1, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getPublicKey(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljava/security/PublicKey;

    move-result-object p1

    invoke-interface {p1}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object p1

    .line 109
    invoke-static {}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->newBuilder()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    .line 110
    invoke-static {}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getCompatibleRsaPadding()Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/capillary/RsaEcdsaConstants$Padding;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setPadding(Ljava/lang/String;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object v0

    .line 111
    invoke-static {p1}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->setKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey$Builder;->build()Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->toByteArray()[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 106
    monitor-exit p0

    throw p1
.end method
