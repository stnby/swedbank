.class public abstract Lcom/google/capillary/android/KeyManager;
.super Ljava/lang/Object;
.source "KeyManager.java"


# static fields
.field private static final AUTH_KEY_SERIAL_NUMBER_KEY:Ljava/lang/String; = "current_auth_key_serial_number"

.field private static final KEYCHAIN_UNIQUE_ID_KEY:Ljava/lang/String; = "keychain_unique_id"

.field private static final NO_AUTH_KEY_SERIAL_NUMBER_KEY:Ljava/lang/String; = "current_no_auth_key_serial_number"


# instance fields
.field final context:Landroid/content/Context;

.field private decrypterManager:Lcom/google/capillary/android/DecrypterManager;

.field final keychainId:Ljava/lang/String;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private final utils:Lcom/google/capillary/android/Utils;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;)V
    .locals 3

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/capillary/android/KeyManager;->context:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/google/capillary/android/KeyManager;->utils:Lcom/google/capillary/android/Utils;

    .line 53
    iput-object p3, p0, Lcom/google/capillary/android/KeyManager;->keychainId:Ljava/lang/String;

    .line 54
    invoke-virtual {p2, p1}, Lcom/google/capillary/android/Utils;->getDeviceProtectedStorageContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    const-string p2, "%s_%s_preferences"

    const/4 v0, 0x2

    .line 55
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p3, v0, v1

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 56
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/android/KeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private checkAndGetSerialNumber(Z)I
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/capillary/android/KeyManager;->toSerialNumberPrefKey(Z)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    return v0

    .line 128
    :cond_0
    new-instance v0, Lcom/google/capillary/NoSuchKeyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/capillary/android/KeyManager;->toKeyTypeString(Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " key not initialized"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/google/capillary/NoSuchKeyException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static toKeyTypeString(Z)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    const-string p0, "Auth"

    goto :goto_0

    :cond_0
    const-string p0, "NoAuth"

    :goto_0
    return-object p0
.end method

.method private static toSerialNumberPrefKey(Z)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    const-string p0, "current_auth_key_serial_number"

    goto :goto_0

    :cond_0
    const-string p0, "current_no_auth_key_serial_number"

    :goto_0
    return-object p0
.end method


# virtual methods
.method public deleteKeyPair(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->utils:Lcom/google/capillary/android/Utils;

    iget-object v1, p0, Lcom/google/capillary/android/KeyManager;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/capillary/android/Utils;->checkAuthModeIsAvailable(Landroid/content/Context;)V

    .line 247
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/capillary/android/KeyManager;->checkAndGetSerialNumber(Z)I

    .line 248
    invoke-virtual {p0, p1}, Lcom/google/capillary/android/KeyManager;->rawDeleteKeyPair(Z)V

    return-void
.end method

.method public declared-synchronized generateKeyPair(Z)V
    .locals 3

    monitor-enter p0

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/capillary/android/KeyManager;->toSerialNumberPrefKey(Z)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 100
    invoke-virtual {p0, v0, p1}, Lcom/google/capillary/android/KeyManager;->generateKeyPair(IZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 98
    monitor-exit p0

    throw p1
.end method

.method declared-synchronized generateKeyPair(IZ)Z
    .locals 3

    monitor-enter p0

    if-eqz p2, :cond_0

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->utils:Lcom/google/capillary/android/Utils;

    iget-object v1, p0, Lcom/google/capillary/android/KeyManager;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/capillary/android/Utils;->checkAuthModeIsAvailable(Landroid/content/Context;)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 111
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {p2}, Lcom/google/capillary/android/KeyManager;->toSerialNumberPrefKey(Z)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt p1, v0, :cond_1

    const/4 p1, 0x0

    .line 114
    monitor-exit p0

    return p1

    .line 116
    :cond_1
    :try_start_1
    invoke-virtual {p0, p2}, Lcom/google/capillary/android/KeyManager;->rawGenerateKeyPair(Z)V

    .line 117
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p2}, Lcom/google/capillary/android/KeyManager;->toSerialNumberPrefKey(Z)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x1

    .line 118
    monitor-exit p0

    return p1

    .line 107
    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public generateKeyPairs()V
    .locals 1

    const/4 v0, 0x0

    .line 84
    invoke-virtual {p0, v0}, Lcom/google/capillary/android/KeyManager;->generateKeyPair(Z)V

    const/4 v0, 0x1

    .line 85
    invoke-virtual {p0, v0}, Lcom/google/capillary/android/KeyManager;->generateKeyPair(Z)V

    return-void
.end method

.method declared-synchronized getDecrypter(Ljava/lang/String;IZ)Lcom/google/crypto/tink/HybridDecrypt;
    .locals 2

    monitor-enter p0

    if-eqz p3, :cond_0

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->utils:Lcom/google/capillary/android/Utils;

    iget-object v1, p0, Lcom/google/capillary/android/KeyManager;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/capillary/android/Utils;->checkAuthModeIsAvailable(Landroid/content/Context;)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 208
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/capillary/android/KeyManager;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 211
    invoke-direct {p0, p3}, Lcom/google/capillary/android/KeyManager;->checkAndGetSerialNumber(Z)I

    move-result p1

    if-ne p1, p2, :cond_1

    .line 217
    invoke-virtual {p0, p3}, Lcom/google/capillary/android/KeyManager;->rawGetDecrypter(Z)Lcom/google/crypto/tink/HybridDecrypt;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 215
    :cond_1
    :try_start_1
    new-instance p1, Lcom/google/capillary/NoSuchKeyException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3}, Lcom/google/capillary/android/KeyManager;->toKeyTypeString(Z)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " key serial number invalid"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/google/capillary/NoSuchKeyException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 209
    :cond_2
    new-instance p1, Lcom/google/capillary/NoSuchKeyException;

    const-string p2, "keychain unique ID mismatch"

    invoke-direct {p1, p2}, Lcom/google/capillary/NoSuchKeyException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getDecrypterManager()Lcom/google/capillary/android/DecrypterManager;
    .locals 4

    monitor-enter p0

    .line 65
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->decrypterManager:Lcom/google/capillary/android/DecrypterManager;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/google/capillary/android/CiphertextStorage;

    iget-object v1, p0, Lcom/google/capillary/android/KeyManager;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/capillary/android/KeyManager;->utils:Lcom/google/capillary/android/Utils;

    iget-object v3, p0, Lcom/google/capillary/android/KeyManager;->keychainId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/capillary/android/CiphertextStorage;-><init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;)V

    .line 67
    new-instance v1, Lcom/google/capillary/android/DecrypterManager;

    iget-object v2, p0, Lcom/google/capillary/android/KeyManager;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/capillary/android/KeyManager;->utils:Lcom/google/capillary/android/Utils;

    invoke-direct {v1, v2, p0, v0, v3}, Lcom/google/capillary/android/DecrypterManager;-><init>(Landroid/content/Context;Lcom/google/capillary/android/KeyManager;Lcom/google/capillary/android/CiphertextStorage;Lcom/google/capillary/android/Utils;)V

    iput-object v1, p0, Lcom/google/capillary/android/KeyManager;->decrypterManager:Lcom/google/capillary/android/DecrypterManager;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->decrypterManager:Lcom/google/capillary/android/DecrypterManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 64
    monitor-exit p0

    throw v0
.end method

.method getKeychainUniqueId()Ljava/lang/String;
    .locals 3

    .line 182
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "keychain_unique_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 184
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/google/capillary/android/KeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "keychain_unique_id"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-object v0
.end method

.method public getPublicKey(ZLcom/google/capillary/android/CapillaryHandler;Ljava/lang/Object;)V
    .locals 1

    .line 158
    invoke-virtual {p0, p1}, Lcom/google/capillary/android/KeyManager;->getPublicKey(Z)[B

    move-result-object v0

    invoke-interface {p2, p1, v0, p3}, Lcom/google/capillary/android/CapillaryHandler;->handlePublicKey(Z[BLjava/lang/Object;)V

    return-void
.end method

.method declared-synchronized getPublicKey(Z)[B
    .locals 3

    monitor-enter p0

    if-eqz p1, :cond_0

    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/KeyManager;->utils:Lcom/google/capillary/android/Utils;

    iget-object v1, p0, Lcom/google/capillary/android/KeyManager;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/capillary/android/Utils;->checkAuthModeIsAvailable(Landroid/content/Context;)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 171
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/capillary/android/KeyManager;->checkAndGetSerialNumber(Z)I

    move-result v0

    .line 172
    invoke-static {}, Lcom/google/capillary/internal/CapillaryPublicKey;->newBuilder()Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v1

    .line 173
    invoke-virtual {p0}, Lcom/google/capillary/android/KeyManager;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setKeychainUniqueId(Ljava/lang/String;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v1

    .line 174
    invoke-virtual {v1, v0}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setSerialNumber(I)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    .line 175
    invoke-virtual {v0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setIsAuth(Z)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object v0

    .line 176
    invoke-virtual {p0, p1}, Lcom/google/capillary/android/KeyManager;->rawGetPublicKey(Z)[B

    move-result-object p1

    invoke-static {p1}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->setKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/CapillaryPublicKey$Builder;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey$Builder;->build()Lcom/google/capillary/internal/CapillaryPublicKey;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryPublicKey;->toByteArray()[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    monitor-exit p0

    return-object p1

    .line 167
    :goto_1
    monitor-exit p0

    throw p1
.end method

.method abstract rawDeleteKeyPair(Z)V
.end method

.method abstract rawGenerateKeyPair(Z)V
.end method

.method abstract rawGetDecrypter(Z)Lcom/google/crypto/tink/HybridDecrypt;
.end method

.method abstract rawGetPublicKey(Z)[B
.end method
