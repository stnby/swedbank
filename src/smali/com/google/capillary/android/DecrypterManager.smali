.class public final Lcom/google/capillary/android/DecrypterManager;
.super Ljava/lang/Object;
.source "DecrypterManager.java"


# instance fields
.field private final ciphertextStorage:Lcom/google/capillary/android/CiphertextStorage;

.field private final context:Landroid/content/Context;

.field private final keyManager:Lcom/google/capillary/android/KeyManager;

.field private final utils:Lcom/google/capillary/android/Utils;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/capillary/android/KeyManager;Lcom/google/capillary/android/CiphertextStorage;Lcom/google/capillary/android/Utils;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/capillary/android/DecrypterManager;->context:Landroid/content/Context;

    .line 49
    iput-object p3, p0, Lcom/google/capillary/android/DecrypterManager;->ciphertextStorage:Lcom/google/capillary/android/CiphertextStorage;

    .line 50
    iput-object p2, p0, Lcom/google/capillary/android/DecrypterManager;->keyManager:Lcom/google/capillary/android/KeyManager;

    .line 51
    iput-object p4, p0, Lcom/google/capillary/android/DecrypterManager;->utils:Lcom/google/capillary/android/Utils;

    return-void
.end method

.method private regenerateKeyAndRequestMessage(Lcom/google/capillary/internal/CapillaryCiphertext;Lcom/google/capillary/android/CapillaryHandler;Ljava/lang/Object;)V
    .locals 3

    .line 141
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->getIsAuthKey()Z

    move-result v0

    .line 142
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->toByteArray()[B

    move-result-object v1

    .line 145
    :try_start_0
    iget-object v2, p0, Lcom/google/capillary/android/DecrypterManager;->keyManager:Lcom/google/capillary/android/KeyManager;

    .line 146
    invoke-virtual {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeySerialNumber()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v2, p1, v0}, Lcom/google/capillary/android/KeyManager;->generateKeyPair(IZ)Z

    move-result p1

    if-nez p1, :cond_0

    .line 152
    sget-object p1, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->STALE_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    invoke-interface {p2, p1, v1, p3}, Lcom/google/capillary/android/CapillaryHandler;->error(Lcom/google/capillary/android/CapillaryHandlerErrorCode;[BLjava/lang/Object;)V

    return-void

    .line 156
    :cond_0
    iget-object p1, p0, Lcom/google/capillary/android/DecrypterManager;->keyManager:Lcom/google/capillary/android/KeyManager;

    invoke-virtual {p1, v0}, Lcom/google/capillary/android/KeyManager;->getPublicKey(Z)[B

    move-result-object p1

    invoke-interface {p2, v0, p1, v1, p3}, Lcom/google/capillary/android/CapillaryHandler;->handlePublicKey(Z[B[BLjava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/capillary/AuthModeUnavailableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/capillary/NoSuchKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 159
    :catch_0
    sget-object p1, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->UNKNOWN_ERROR:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    invoke-interface {p2, p1, v1, p3}, Lcom/google/capillary/android/CapillaryHandler;->error(Lcom/google/capillary/android/CapillaryHandlerErrorCode;[BLjava/lang/Object;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public declared-synchronized decrypt([BLcom/google/capillary/android/CapillaryHandler;Ljava/lang/Object;)V
    .locals 6

    monitor-enter p0

    .line 86
    :try_start_0
    invoke-static {p1}, Lcom/google/capillary/internal/CapillaryCiphertext;->parseFrom([B)Lcom/google/capillary/internal/CapillaryCiphertext;

    move-result-object v0
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :try_start_1
    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getIsAuthKey()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/capillary/android/DecrypterManager;->utils:Lcom/google/capillary/android/Utils;

    iget-object v2, p0, Lcom/google/capillary/android/DecrypterManager;->context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/capillary/android/Utils;->isScreenLocked(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/capillary/android/DecrypterManager;->ciphertextStorage:Lcom/google/capillary/android/CiphertextStorage;

    invoke-virtual {v0, p1}, Lcom/google/capillary/android/CiphertextStorage;->save([B)V

    .line 95
    invoke-interface {p2, p1, p3}, Lcom/google/capillary/android/CapillaryHandler;->authCiphertextSavedForLater([BLjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    .line 99
    :cond_0
    :try_start_2
    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getCiphertext()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    :try_start_3
    iget-object v2, p0, Lcom/google/capillary/android/DecrypterManager;->keyManager:Lcom/google/capillary/android/KeyManager;

    .line 105
    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeychainUniqueId()Ljava/lang/String;

    move-result-object v3

    .line 106
    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getKeySerialNumber()I

    move-result v4

    .line 107
    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getIsAuthKey()Z

    move-result v5

    .line 104
    invoke-virtual {v2, v3, v4, v5}, Lcom/google/capillary/android/KeyManager;->getDecrypter(Ljava/lang/String;IZ)Lcom/google/crypto/tink/HybridDecrypt;

    move-result-object v2

    const/4 v3, 0x0

    .line 108
    invoke-interface {v2, v1, v3}, Lcom/google/crypto/tink/HybridDecrypt;->decrypt([B[B)[B

    move-result-object v1
    :try_end_3
    .catch Lcom/google/capillary/AuthModeUnavailableException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    :try_start_4
    invoke-virtual {v0}, Lcom/google/capillary/internal/CapillaryCiphertext;->getIsAuthKey()Z

    move-result p1

    invoke-interface {p2, p1, v1, p3}, Lcom/google/capillary/android/CapillaryHandler;->handleData(Z[BLjava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 137
    monitor-exit p0

    return-void

    :catch_0
    move-exception v1

    .line 116
    :try_start_5
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_1

    instance-of v2, v1, Landroid/security/keystore/UserNotAuthenticatedException;

    if-eqz v2, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/capillary/android/DecrypterManager;->ciphertextStorage:Lcom/google/capillary/android/CiphertextStorage;

    invoke-virtual {v0, p1}, Lcom/google/capillary/android/CiphertextStorage;->save([B)V

    .line 118
    invoke-interface {p2, p1, p3}, Lcom/google/capillary/android/CapillaryHandler;->authCiphertextSavedForLater([BLjava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    .line 122
    :cond_1
    :try_start_6
    instance-of v2, v1, Lcom/google/capillary/NoSuchKeyException;

    if-nez v2, :cond_4

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v3, :cond_2

    instance-of v2, v1, Landroid/security/keystore/KeyPermanentlyInvalidatedException;

    if-nez v2, :cond_4

    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-lt v2, v3, :cond_3

    instance-of v1, v1, Ljava/security/UnrecoverableKeyException;

    if-eqz v1, :cond_3

    goto :goto_0

    .line 131
    :cond_3
    sget-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->UNKNOWN_ERROR:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    invoke-interface {p2, v0, p1, p3}, Lcom/google/capillary/android/CapillaryHandler;->error(Lcom/google/capillary/android/CapillaryHandlerErrorCode;[BLjava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 127
    :cond_4
    :goto_0
    :try_start_7
    invoke-direct {p0, v0, p2, p3}, Lcom/google/capillary/android/DecrypterManager;->regenerateKeyAndRequestMessage(Lcom/google/capillary/internal/CapillaryCiphertext;Lcom/google/capillary/android/CapillaryHandler;Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 128
    monitor-exit p0

    return-void

    .line 110
    :catch_1
    :try_start_8
    sget-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->AUTH_CIPHER_IN_NO_AUTH_DEVICE:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    invoke-interface {p2, v0, p1, p3}, Lcom/google/capillary/android/CapillaryHandler;->error(Lcom/google/capillary/android/CapillaryHandlerErrorCode;[BLjava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 111
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 88
    :catch_2
    :try_start_9
    sget-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->MALFORMED_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    invoke-interface {p2, v0, p1, p3}, Lcom/google/capillary/android/CapillaryHandler;->error(Lcom/google/capillary/android/CapillaryHandlerErrorCode;[BLjava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    .line 85
    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized decryptSaved(Lcom/google/capillary/android/CapillaryHandler;Ljava/lang/Object;)V
    .locals 2

    monitor-enter p0

    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/DecrypterManager;->ciphertextStorage:Lcom/google/capillary/android/CiphertextStorage;

    invoke-virtual {v0}, Lcom/google/capillary/android/CiphertextStorage;->get()Ljava/util/List;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/google/capillary/android/DecrypterManager;->ciphertextStorage:Lcom/google/capillary/android/CiphertextStorage;

    invoke-virtual {v1}, Lcom/google/capillary/android/CiphertextStorage;->clear()V

    .line 63
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 64
    invoke-virtual {p0, v1, p1, p2}, Lcom/google/capillary/android/DecrypterManager;->decrypt([BLcom/google/capillary/android/CapillaryHandler;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 66
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 60
    monitor-exit p0

    throw p1
.end method
