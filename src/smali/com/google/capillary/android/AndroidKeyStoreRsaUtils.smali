.class final Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;
.super Ljava/lang/Object;
.source "AndroidKeyStoreRsaUtils.java"


# static fields
.field private static final AUTH_KEY_ALIAS_SUFFIX:Ljava/lang/String; = "_capillary_rsa_auth"

.field private static final KEYSTORE_ANDROID:Ljava/lang/String; = "AndroidKeyStore"

.field private static final KEY_DURATION_YEARS:I = 0x64

.field private static final KEY_SIZE:I = 0x800

.field private static final NO_AUTH_KEY_ALIAS_SUFFIX:Ljava/lang/String; = "_capillary_rsa_no_auth"

.field private static final UNLOCK_DURATION_SECONDS:I = 0xe10


# direct methods
.method constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkKeyExists(Ljava/security/KeyStore;Ljava/lang/String;)V
    .locals 2

    .line 138
    invoke-virtual {p0, p1}, Ljava/security/KeyStore;->containsAlias(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    return-void

    .line 139
    :cond_0
    new-instance p0, Lcom/google/capillary/NoSuchKeyException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android key store has no rsa key pair with alias "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/capillary/NoSuchKeyException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static checkKeyExists(Ljava/security/KeyStore;Ljava/lang/String;Z)V
    .locals 0

    .line 133
    invoke-static {p1, p2}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->toKeyAlias(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->checkKeyExists(Ljava/security/KeyStore;Ljava/lang/String;)V

    return-void
.end method

.method static deleteKeyPair(Ljava/security/KeyStore;Ljava/lang/String;Z)V
    .locals 0

    .line 121
    invoke-static {p1, p2}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->toKeyAlias(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 122
    invoke-static {p0, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->checkKeyExists(Ljava/security/KeyStore;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0, p1}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    return-void
.end method

.method static generateKeyPair(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 7

    .line 60
    invoke-static {p1, p2}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->toKeyAlias(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 61
    new-instance v0, Ljava/security/spec/RSAKeyGenParameterSpec;

    sget-object v1, Ljava/security/spec/RSAKeyGenParameterSpec;->F4:Ljava/math/BigInteger;

    const/16 v2, 0x800

    invoke-direct {v0, v2, v1}, Ljava/security/spec/RSAKeyGenParameterSpec;-><init>(ILjava/math/BigInteger;)V

    .line 66
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v1, v3, :cond_1

    .line 67
    new-instance p0, Landroid/security/keystore/KeyGenParameterSpec$Builder;

    const/4 v1, 0x2

    invoke-direct {p0, p1, v1}, Landroid/security/keystore/KeyGenParameterSpec$Builder;-><init>(Ljava/lang/String;I)V

    .line 69
    invoke-virtual {p0, v0}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setAlgorithmParameterSpec(Ljava/security/spec/AlgorithmParameterSpec;)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    move-result-object p0

    const-string p1, "SHA-256"

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    .line 70
    invoke-virtual {p0, p1}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setDigests([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    move-result-object p0

    const-string p1, "OAEPPadding"

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    .line 71
    invoke-virtual {p0, p1}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setEncryptionPaddings([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    move-result-object p0

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    .line 73
    invoke-virtual {p0, p1}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setUserAuthenticationRequired(Z)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    const/16 p1, 0xe10

    .line 74
    invoke-virtual {p0, p1}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setUserAuthenticationValidityDurationSeconds(I)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    .line 76
    :cond_0
    invoke-virtual {p0}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->build()Landroid/security/keystore/KeyGenParameterSpec;

    move-result-object p0

    goto :goto_0

    .line 78
    :cond_1
    invoke-static {}, Lorg/joda/time/LocalDate;->now()Lorg/joda/time/LocalDate;

    move-result-object v1

    const/16 v3, 0x64

    .line 79
    invoke-virtual {v1, v3}, Lorg/joda/time/LocalDate;->plusYears(I)Lorg/joda/time/LocalDate;

    move-result-object v3

    .line 80
    new-instance v4, Landroid/security/KeyPairGeneratorSpec$Builder;

    invoke-direct {v4, p0}, Landroid/security/KeyPairGeneratorSpec$Builder;-><init>(Landroid/content/Context;)V

    .line 81
    invoke-virtual {v4, p1}, Landroid/security/KeyPairGeneratorSpec$Builder;->setAlias(Ljava/lang/String;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object p0

    new-instance v4, Ljavax/security/auth/x500/X500Principal;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CN="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, p1}, Ljavax/security/auth/x500/X500Principal;-><init>(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0, v4}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSubject(Ljavax/security/auth/x500/X500Principal;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object p0

    sget-object p1, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    .line 83
    invoke-virtual {p0, p1}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSerialNumber(Ljava/math/BigInteger;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object p0

    .line 84
    invoke-virtual {v1}, Lorg/joda/time/LocalDate;->toDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/security/KeyPairGeneratorSpec$Builder;->setStartDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object p0

    .line 85
    invoke-virtual {v3}, Lorg/joda/time/LocalDate;->toDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/security/KeyPairGeneratorSpec$Builder;->setEndDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object p0

    .line 87
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt p1, v1, :cond_2

    .line 88
    invoke-virtual {p0, v0}, Landroid/security/KeyPairGeneratorSpec$Builder;->setAlgorithmParameterSpec(Ljava/security/spec/AlgorithmParameterSpec;)Landroid/security/KeyPairGeneratorSpec$Builder;

    .line 89
    invoke-virtual {p0, v2}, Landroid/security/KeyPairGeneratorSpec$Builder;->setKeySize(I)Landroid/security/KeyPairGeneratorSpec$Builder;

    :cond_2
    if-eqz p2, :cond_3

    .line 92
    invoke-virtual {p0}, Landroid/security/KeyPairGeneratorSpec$Builder;->setEncryptionRequired()Landroid/security/KeyPairGeneratorSpec$Builder;

    .line 94
    :cond_3
    invoke-virtual {p0}, Landroid/security/KeyPairGeneratorSpec$Builder;->build()Landroid/security/KeyPairGeneratorSpec;

    move-result-object p0

    :goto_0
    const-string p1, "RSA"

    const-string p2, "AndroidKeyStore"

    .line 97
    invoke-static {p1, p2}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object p1

    .line 98
    invoke-virtual {p1, p0}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 99
    invoke-virtual {p1}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    return-void
.end method

.method static getCompatibleRsaPadding()Lcom/google/capillary/RsaEcdsaConstants$Padding;
    .locals 2

    .line 144
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/capillary/RsaEcdsaConstants$Padding;->PKCS1:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    :goto_0
    return-object v0
.end method

.method static getPrivateKey(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljava/security/PrivateKey;
    .locals 0

    .line 114
    invoke-static {p1, p2}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->toKeyAlias(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 115
    invoke-static {p0, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->checkKeyExists(Ljava/security/KeyStore;Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 116
    invoke-virtual {p0, p1, p2}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p0

    check-cast p0, Ljava/security/PrivateKey;

    return-object p0
.end method

.method static getPublicKey(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljava/security/PublicKey;
    .locals 0

    .line 104
    invoke-static {p1, p2}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->toKeyAlias(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 105
    invoke-static {p0, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->checkKeyExists(Ljava/security/KeyStore;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0, p1}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p0

    return-object p0
.end method

.method private static toKeyAlias(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    const-string p1, "_capillary_rsa_auth"

    goto :goto_0

    :cond_0
    const-string p1, "_capillary_rsa_no_auth"

    .line 128
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
