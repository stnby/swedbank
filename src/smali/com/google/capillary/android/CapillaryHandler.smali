.class public interface abstract Lcom/google/capillary/android/CapillaryHandler;
.super Ljava/lang/Object;
.source "CapillaryHandler.java"


# virtual methods
.method public abstract authCiphertextSavedForLater([BLjava/lang/Object;)V
.end method

.method public abstract error(Lcom/google/capillary/android/CapillaryHandlerErrorCode;[BLjava/lang/Object;)V
.end method

.method public abstract handleData(Z[BLjava/lang/Object;)V
.end method

.method public abstract handlePublicKey(Z[BLjava/lang/Object;)V
.end method

.method public abstract handlePublicKey(Z[B[BLjava/lang/Object;)V
.end method
