.class Lcom/google/capillary/android/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final KEYSTORE_ANDROID:Ljava/lang/String; = "AndroidKeyStore"

.field private static instance:Lcom/google/capillary/android/Utils;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static declared-synchronized getInstance()Lcom/google/capillary/android/Utils;
    .locals 2

    const-class v0, Lcom/google/capillary/android/Utils;

    monitor-enter v0

    .line 44
    :try_start_0
    sget-object v1, Lcom/google/capillary/android/Utils;->instance:Lcom/google/capillary/android/Utils;

    if-nez v1, :cond_0

    .line 45
    new-instance v1, Lcom/google/capillary/android/Utils;

    invoke-direct {v1}, Lcom/google/capillary/android/Utils;-><init>()V

    sput-object v1, Lcom/google/capillary/android/Utils;->instance:Lcom/google/capillary/android/Utils;

    .line 47
    :cond_0
    sget-object v1, Lcom/google/capillary/android/Utils;->instance:Lcom/google/capillary/android/Utils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 43
    monitor-exit v0

    throw v1
.end method

.method private isScreenLockEnabled(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "keyguard"

    .line 83
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/KeyguardManager;

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 86
    invoke-virtual {p1}, Landroid/app/KeyguardManager;->isDeviceSecure()Z

    move-result p1

    return p1

    .line 88
    :cond_0
    invoke-virtual {p1}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result p1

    return p1
.end method


# virtual methods
.method checkAuthModeIsAvailable(Landroid/content/Context;)V
    .locals 1

    .line 67
    invoke-direct {p0, p1}, Lcom/google/capillary/android/Utils;->isScreenLockEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0, p1}, Lcom/google/capillary/android/Utils;->isScreenLocked(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-nez v0, :cond_1

    .line 72
    new-instance p1, Lcom/google/capillary/AuthModeUnavailableException;

    const-string v0, "the device is not secured with a PIN, pattern, or password"

    invoke-direct {p1, v0}, Lcom/google/capillary/AuthModeUnavailableException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 75
    :cond_1
    new-instance p1, Lcom/google/capillary/AuthModeUnavailableException;

    const-string v0, "the device is locked"

    invoke-direct {p1, v0}, Lcom/google/capillary/AuthModeUnavailableException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method getDeviceProtectedStorageContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .line 56
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object p1

    return-object p1

    :cond_0
    return-object p1
.end method

.method isScreenLocked(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "keyguard"

    .line 96
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/KeyguardManager;

    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 99
    invoke-virtual {p1}, Landroid/app/KeyguardManager;->isDeviceLocked()Z

    move-result p1

    return p1

    .line 101
    :cond_0
    invoke-virtual {p1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result p1

    return p1
.end method

.method loadKeyStore()Ljava/security/KeyStore;
    .locals 3

    const-string v0, "AndroidKeyStore"

    .line 105
    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    const/4 v1, 0x0

    .line 107
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 109
    new-instance v1, Ljava/security/GeneralSecurityException;

    const-string v2, "unable to load keystore"

    invoke-direct {v1, v2, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
