.class public final enum Lcom/google/capillary/android/CapillaryHandlerErrorCode;
.super Ljava/lang/Enum;
.source "CapillaryHandlerErrorCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/capillary/android/CapillaryHandlerErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/capillary/android/CapillaryHandlerErrorCode;

.field public static final enum AUTH_CIPHER_IN_NO_AUTH_DEVICE:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

.field public static final enum MALFORMED_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

.field public static final enum STALE_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

.field public static final enum UNKNOWN_ERROR:Lcom/google/capillary/android/CapillaryHandlerErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 25
    new-instance v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    const-string v1, "AUTH_CIPHER_IN_NO_AUTH_DEVICE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/capillary/android/CapillaryHandlerErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->AUTH_CIPHER_IN_NO_AUTH_DEVICE:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    .line 27
    new-instance v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    const-string v1, "MALFORMED_CIPHERTEXT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/google/capillary/android/CapillaryHandlerErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->MALFORMED_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    .line 31
    new-instance v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    const-string v1, "STALE_CIPHERTEXT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/google/capillary/android/CapillaryHandlerErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->STALE_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    .line 33
    new-instance v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/google/capillary/android/CapillaryHandlerErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->UNKNOWN_ERROR:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    const/4 v0, 0x4

    .line 22
    new-array v0, v0, [Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    sget-object v1, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->AUTH_CIPHER_IN_NO_AUTH_DEVICE:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->MALFORMED_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->STALE_CIPHERTEXT:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->UNKNOWN_ERROR:Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->$VALUES:[Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/capillary/android/CapillaryHandlerErrorCode;
    .locals 1

    .line 22
    const-class v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/google/capillary/android/CapillaryHandlerErrorCode;
    .locals 1

    .line 22
    sget-object v0, Lcom/google/capillary/android/CapillaryHandlerErrorCode;->$VALUES:[Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    invoke-virtual {v0}, [Lcom/google/capillary/android/CapillaryHandlerErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/capillary/android/CapillaryHandlerErrorCode;

    return-object v0
.end method
