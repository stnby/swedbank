.class public Lcom/google/capillary/android/WebPushKeyManager;
.super Lcom/google/capillary/android/KeyManager;
.source "WebPushKeyManager.java"


# static fields
.field private static final KEY_CHAIN_ID_PREFIX:Ljava/lang/String; = "web_push_"

.field private static final OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

.field private static final PRIVATE_KEY_KEY_SUFFIX:Ljava/lang/String; = "_encrypted_web_push_private_key"

.field private static final PUBLIC_KEY_KEY_SUFFIX:Ljava/lang/String; = "_web_push_public_key"

.field private static instances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/google/capillary/android/WebPushKeyManager;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final keyStore:Ljava/security/KeyStore;

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 57
    new-instance v0, Ljavax/crypto/spec/OAEPParameterSpec;

    const-string v1, "SHA-256"

    const-string v2, "MGF1"

    sget-object v3, Ljava/security/spec/MGF1ParameterSpec;->SHA1:Ljava/security/spec/MGF1ParameterSpec;

    sget-object v4, Ljavax/crypto/spec/PSource$PSpecified;->DEFAULT:Ljavax/crypto/spec/PSource$PSpecified;

    invoke-direct {v0, v1, v2, v3, v4}, Ljavax/crypto/spec/OAEPParameterSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/spec/PSource;)V

    sput-object v0, Lcom/google/capillary/android/WebPushKeyManager;->OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/capillary/android/WebPushKeyManager;->instances:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;)V
    .locals 3

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "web_push_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/capillary/android/KeyManager;-><init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p2}, Lcom/google/capillary/android/Utils;->loadKeyStore()Ljava/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->keyStore:Ljava/security/KeyStore;

    .line 69
    invoke-virtual {p2, p1}, Lcom/google/capillary/android/Utils;->getDeviceProtectedStorageContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    const-string p2, "%s_%s_preferences"

    const/4 v0, 0x2

    .line 70
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p3, v0, v1

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 71
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/android/WebPushKeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private checkKeyExists(Z)V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/google/capillary/android/WebPushKeyManager;->toKeyPrefKey(ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    .line 157
    invoke-static {p1, v1}, Lcom/google/capillary/android/WebPushKeyManager;->toKeyPrefKey(ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 158
    :cond_0
    new-instance v0, Lcom/google/capillary/NoSuchKeyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/capillary/android/WebPushKeyManager;->toKeyTypeString(Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " web push key not initialized"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/google/capillary/NoSuchKeyException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/google/capillary/android/WebPushKeyManager;
    .locals 3

    const-class v0, Lcom/google/capillary/android/WebPushKeyManager;

    monitor-enter v0

    .line 84
    :try_start_0
    sget-object v1, Lcom/google/capillary/android/WebPushKeyManager;->instances:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    sget-object p0, Lcom/google/capillary/android/WebPushKeyManager;->instances:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/capillary/android/WebPushKeyManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    .line 87
    :cond_0
    :try_start_1
    new-instance v1, Lcom/google/capillary/android/WebPushKeyManager;

    invoke-static {}, Lcom/google/capillary/android/Utils;->getInstance()Lcom/google/capillary/android/Utils;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/google/capillary/android/WebPushKeyManager;-><init>(Landroid/content/Context;Lcom/google/capillary/android/Utils;Ljava/lang/String;)V

    .line 88
    sget-object p0, Lcom/google/capillary/android/WebPushKeyManager;->instances:Ljava/util/Map;

    invoke-interface {p0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    .line 83
    monitor-exit v0

    throw p0
.end method

.method private static toKeyPrefKey(ZZ)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    const-string p0, "auth"

    goto :goto_0

    :cond_0
    const-string p0, "no_auth"

    :goto_0
    if-eqz p1, :cond_1

    const-string p1, "_web_push_public_key"

    goto :goto_1

    :cond_1
    const-string p1, "_encrypted_web_push_private_key"

    .line 95
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method declared-synchronized rawDeleteKeyPair(Z)V
    .locals 2

    monitor-enter p0

    .line 202
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/capillary/android/WebPushKeyManager;->checkKeyExists(Z)V

    .line 203
    iget-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->keyStore:Ljava/security/KeyStore;

    iget-object v1, p0, Lcom/google/capillary/android/WebPushKeyManager;->keychainId:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->deleteKeyPair(Ljava/security/KeyStore;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 201
    monitor-exit p0

    throw p1
.end method

.method declared-synchronized rawGenerateKeyPair(Z)V
    .locals 5

    monitor-enter p0

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/capillary/android/WebPushKeyManager;->keychainId:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->generateKeyPair(Landroid/content/Context;Ljava/lang/String;Z)V

    const/16 v0, 0x10

    .line 114
    invoke-static {v0}, Lcom/google/crypto/tink/subtle/Random;->randBytes(I)[B

    move-result-object v0

    .line 115
    sget-object v1, Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;->NIST_P256:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    invoke-static {v1}, Lcom/google/crypto/tink/subtle/EllipticCurves;->generateKeyPair(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;)Ljava/security/KeyPair;

    move-result-object v1

    .line 117
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v2

    check-cast v2, Ljava/security/interfaces/ECPublicKey;

    .line 118
    sget-object v3, Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;->NIST_P256:Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;

    sget-object v4, Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;->UNCOMPRESSED:Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;

    .line 120
    invoke-interface {v2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v2

    .line 119
    invoke-static {v3, v4, v2}, Lcom/google/crypto/tink/subtle/EllipticCurves;->pointEncode(Lcom/google/crypto/tink/subtle/EllipticCurves$CurveType;Lcom/google/crypto/tink/subtle/EllipticCurves$PointFormatType;Ljava/security/spec/ECPoint;)[B

    move-result-object v2

    .line 121
    invoke-static {}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->newBuilder()Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v3

    .line 122
    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;->setAuthSecret(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v3

    .line 123
    invoke-static {v2}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;->setKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/capillary/internal/WrappedWebPushPublicKey$Builder;->build()Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    move-result-object v3

    .line 124
    invoke-virtual {v3}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->toByteArray()[B

    move-result-object v3

    .line 126
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v1

    check-cast v1, Ljava/security/interfaces/ECPrivateKey;

    .line 127
    invoke-interface {v1}, Ljava/security/interfaces/ECPrivateKey;->getS()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    .line 128
    invoke-static {}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->newBuilder()Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v4

    .line 129
    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;->setAuthSecret(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    .line 130
    invoke-static {v2}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;->setPublicKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    .line 131
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;->setPrivateKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey$Builder;->build()Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->toByteArray()[B

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :try_start_1
    iget-object v1, p0, Lcom/google/capillary/android/WebPushKeyManager;->keyStore:Ljava/security/KeyStore;

    iget-object v2, p0, Lcom/google/capillary/android/WebPushKeyManager;->keychainId:Ljava/lang/String;

    invoke-static {v1, v2, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getPublicKey(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljava/security/PublicKey;

    move-result-object v1
    :try_end_1
    .catch Lcom/google/capillary/NoSuchKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    :try_start_2
    invoke-static {}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getCompatibleRsaPadding()Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v2

    sget-object v4, Lcom/google/capillary/android/WebPushKeyManager;->OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

    .line 142
    invoke-static {v0, v1, v2, v4}, Lcom/google/capillary/HybridRsaUtils;->encrypt([BLjava/security/PublicKey;Lcom/google/capillary/RsaEcdsaConstants$Padding;Ljavax/crypto/spec/OAEPParameterSpec;)[B

    move-result-object v0

    .line 149
    iget-object v1, p0, Lcom/google/capillary/android/WebPushKeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const/4 v2, 0x1

    .line 150
    invoke-static {p1, v2}, Lcom/google/capillary/android/WebPushKeyManager;->toKeyPrefKey(ZZ)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Lcom/google/crypto/tink/subtle/Base64;->encode([B)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const/4 v2, 0x0

    .line 151
    invoke-static {p1, v2}, Lcom/google/capillary/android/WebPushKeyManager;->toKeyPrefKey(ZZ)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Lcom/google/crypto/tink/subtle/Base64;->encode([B)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 152
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 153
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    .line 139
    :try_start_3
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "unable to load rsa public key"

    invoke-direct {v0, v1, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    .line 110
    monitor-exit p0

    throw p1
.end method

.method declared-synchronized rawGetDecrypter(Z)Lcom/google/crypto/tink/HybridDecrypt;
    .locals 3

    monitor-enter p0

    .line 173
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/capillary/android/WebPushKeyManager;->checkKeyExists(Z)V

    .line 175
    iget-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    .line 176
    invoke-static {p1, v1}, Lcom/google/capillary/android/WebPushKeyManager;->toKeyPrefKey(ZZ)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/crypto/tink/subtle/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v0

    .line 178
    iget-object v1, p0, Lcom/google/capillary/android/WebPushKeyManager;->keyStore:Ljava/security/KeyStore;

    iget-object v2, p0, Lcom/google/capillary/android/WebPushKeyManager;->keychainId:Ljava/lang/String;

    invoke-static {v1, v2, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getPrivateKey(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljava/security/PrivateKey;

    move-result-object p1

    .line 182
    invoke-static {}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->getCompatibleRsaPadding()Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v1

    sget-object v2, Lcom/google/capillary/android/WebPushKeyManager;->OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

    .line 179
    invoke-static {v0, p1, v1, v2}, Lcom/google/capillary/HybridRsaUtils;->decrypt([BLjava/security/PrivateKey;Lcom/google/capillary/RsaEcdsaConstants$Padding;Ljavax/crypto/spec/OAEPParameterSpec;)[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :try_start_1
    invoke-static {p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->parseFrom([B)Lcom/google/capillary/internal/WrappedWebPushPrivateKey;

    move-result-object p1
    :try_end_1
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    :try_start_2
    new-instance v0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;

    invoke-direct {v0}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;-><init>()V

    .line 193
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->withAuthSecret([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;

    move-result-object v0

    .line 194
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPublicKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->withRecipientPublicKey([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;

    move-result-object v0

    .line 195
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPrivateKey;->getPrivateKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->withRecipientPrivateKey([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;

    move-result-object p1

    .line 196
    invoke-virtual {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt$Builder;->build()Lcom/google/crypto/tink/apps/webpush/WebPushHybridDecrypt;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    .line 189
    :try_start_3
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "unable to load web push private key"

    invoke-direct {v0, v1, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    .line 172
    monitor-exit p0

    throw p1
.end method

.method declared-synchronized rawGetPublicKey(Z)[B
    .locals 2

    monitor-enter p0

    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->keyStore:Ljava/security/KeyStore;

    iget-object v1, p0, Lcom/google/capillary/android/WebPushKeyManager;->keychainId:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/capillary/android/AndroidKeyStoreRsaUtils;->checkKeyExists(Ljava/security/KeyStore;Ljava/lang/String;Z)V

    .line 166
    invoke-direct {p0, p1}, Lcom/google/capillary/android/WebPushKeyManager;->checkKeyExists(Z)V

    .line 167
    iget-object v0, p0, Lcom/google/capillary/android/WebPushKeyManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/google/capillary/android/WebPushKeyManager;->toKeyPrefKey(ZZ)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/google/crypto/tink/subtle/Base64;->decode(Ljava/lang/String;)[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 164
    monitor-exit p0

    throw p1
.end method
