.class public final Lcom/google/capillary/RsaEcdsaConstants;
.super Ljava/lang/Object;
.source "RsaEcdsaConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/capillary/RsaEcdsaConstants$Padding;
    }
.end annotation


# static fields
.field static final OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

.field static final SIGNATURE_LENGTH_BYTES_LENGTH:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 28
    new-instance v0, Ljavax/crypto/spec/OAEPParameterSpec;

    const-string v1, "SHA-256"

    const-string v2, "MGF1"

    sget-object v3, Ljava/security/spec/MGF1ParameterSpec;->SHA1:Ljava/security/spec/MGF1ParameterSpec;

    sget-object v4, Ljavax/crypto/spec/PSource$PSpecified;->DEFAULT:Ljavax/crypto/spec/PSource$PSpecified;

    invoke-direct {v0, v1, v2, v3, v4}, Ljavax/crypto/spec/OAEPParameterSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/spec/PSource;)V

    sput-object v0, Lcom/google/capillary/RsaEcdsaConstants;->OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
