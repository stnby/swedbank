.class public final Lcom/google/capillary/WebPushEncrypterManager;
.super Lcom/google/capillary/EncrypterManager;
.source "WebPushEncrypterManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/google/capillary/EncrypterManager;-><init>()V

    return-void
.end method


# virtual methods
.method rawLoadPublicKey([B)Lcom/google/crypto/tink/HybridEncrypt;
    .locals 2

    .line 34
    :try_start_0
    invoke-static {p1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->parseFrom([B)Lcom/google/capillary/internal/WrappedWebPushPublicKey;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    new-instance v0, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;

    invoke-direct {v0}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;-><init>()V

    .line 39
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getAuthSecret()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->withAuthSecret([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedWebPushPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->withRecipientPublicKey([B)Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt$Builder;->build()Lcom/google/crypto/tink/apps/webpush/WebPushHybridEncrypt;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 36
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "unable to parse public key"

    invoke-direct {v0, v1, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
