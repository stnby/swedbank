.class public final Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;
.super Ljava/lang/Object;
.source "RsaEcdsaHybridEncrypt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/capillary/RsaEcdsaHybridEncrypt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

.field private padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

.field private recipientPublicKey:Ljava/security/PublicKey;

.field private senderSigner:Lcom/google/crypto/tink/PublicKeySign;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 104
    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->senderSigner:Lcom/google/crypto/tink/PublicKeySign;

    .line 105
    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->recipientPublicKey:Ljava/security/PublicKey;

    .line 106
    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    .line 107
    sget-object v0, Lcom/google/capillary/RsaEcdsaConstants;->OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-void
.end method

.method static synthetic access$100(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Lcom/google/crypto/tink/PublicKeySign;
    .locals 0

    .line 102
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->senderSigner:Lcom/google/crypto/tink/PublicKeySign;

    return-object p0
.end method

.method static synthetic access$200(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Ljava/security/PublicKey;
    .locals 0

    .line 102
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->recipientPublicKey:Ljava/security/PublicKey;

    return-object p0
.end method

.method static synthetic access$300(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Lcom/google/capillary/RsaEcdsaConstants$Padding;
    .locals 0

    .line 102
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    return-object p0
.end method

.method static synthetic access$400(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Ljavax/crypto/spec/OAEPParameterSpec;
    .locals 0

    .line 102
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/google/capillary/RsaEcdsaHybridEncrypt;
    .locals 2

    .line 168
    new-instance v0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt;-><init>(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;Lcom/google/capillary/RsaEcdsaHybridEncrypt$1;)V

    return-object v0
.end method

.method public withOaepParameterSpec(Ljavax/crypto/spec/OAEPParameterSpec;)Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-object p0
.end method

.method public withPadding(Lcom/google/capillary/RsaEcdsaConstants$Padding;)Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    return-object p0
.end method

.method public withRecipientPublicKey(Ljava/security/PublicKey;)Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->recipientPublicKey:Ljava/security/PublicKey;

    return-object p0
.end method

.method public withSenderSigner(Lcom/google/crypto/tink/PublicKeySign;)Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->senderSigner:Lcom/google/crypto/tink/PublicKeySign;

    return-object p0
.end method
