.class public final Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;
.super Ljava/lang/Object;
.source "RsaEcdsaHybridDecrypt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/capillary/RsaEcdsaHybridDecrypt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

.field private padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

.field private recipientPrivateKey:Ljava/security/PrivateKey;

.field private senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 103
    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;

    .line 104
    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->recipientPrivateKey:Ljava/security/PrivateKey;

    .line 105
    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    .line 106
    sget-object v0, Lcom/google/capillary/RsaEcdsaConstants;->OAEP_PARAMETER_SPEC:Ljavax/crypto/spec/OAEPParameterSpec;

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-void
.end method

.method static synthetic access$100(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Lcom/google/crypto/tink/PublicKeyVerify;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;

    return-object p0
.end method

.method static synthetic access$200(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Ljava/security/PrivateKey;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->recipientPrivateKey:Ljava/security/PrivateKey;

    return-object p0
.end method

.method static synthetic access$300(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Lcom/google/capillary/RsaEcdsaConstants$Padding;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    return-object p0
.end method

.method static synthetic access$400(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;)Ljavax/crypto/spec/OAEPParameterSpec;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/google/capillary/RsaEcdsaHybridDecrypt;
    .locals 2

    .line 167
    new-instance v0, Lcom/google/capillary/RsaEcdsaHybridDecrypt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/capillary/RsaEcdsaHybridDecrypt;-><init>(Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;Lcom/google/capillary/RsaEcdsaHybridDecrypt$1;)V

    return-object v0
.end method

.method public withOaepParameterSpec(Ljavax/crypto/spec/OAEPParameterSpec;)Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-object p0
.end method

.method public withPadding(Lcom/google/capillary/RsaEcdsaConstants$Padding;)Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    return-object p0
.end method

.method public withRecipientPrivateKey(Ljava/security/PrivateKey;)Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->recipientPrivateKey:Ljava/security/PrivateKey;

    return-object p0
.end method

.method public withSenderVerifier(Lcom/google/crypto/tink/PublicKeyVerify;)Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridDecrypt$Builder;->senderVerifier:Lcom/google/crypto/tink/PublicKeyVerify;

    return-object p0
.end method
