.class public final Lcom/google/capillary/RsaEcdsaHybridEncrypt;
.super Ljava/lang/Object;
.source "RsaEcdsaHybridEncrypt.java"

# interfaces
.implements Lcom/google/crypto/tink/HybridEncrypt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;
    }
.end annotation


# instance fields
.field private final oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

.field private final padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

.field private final recipientPublicKey:Ljava/security/PublicKey;

.field private final senderSigner:Lcom/google/crypto/tink/PublicKeySign;


# direct methods
.method private constructor <init>(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)V
    .locals 2

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$100(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Lcom/google/crypto/tink/PublicKeySign;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 177
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$100(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Lcom/google/crypto/tink/PublicKeySign;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->senderSigner:Lcom/google/crypto/tink/PublicKeySign;

    .line 179
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$200(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Ljava/security/PublicKey;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 183
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$200(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Ljava/security/PublicKey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->recipientPublicKey:Ljava/security/PublicKey;

    .line 185
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$300(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 189
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$300(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object v0

    iput-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    .line 191
    iget-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    sget-object v1, Lcom/google/capillary/RsaEcdsaConstants$Padding;->OAEP:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$400(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Ljavax/crypto/spec/OAEPParameterSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 192
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set OAEP parameter spec with Builder.withOaepParameterSpec"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 195
    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->access$400(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)Ljavax/crypto/spec/OAEPParameterSpec;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    return-void

    .line 186
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set padding with Builder.withPadding"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 180
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set recipient\'s public key with Builder.withRecipientPublicKeyBytes"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 174
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "must set sender\'s signer with Builder.withSenderSigner"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic constructor <init>(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;Lcom/google/capillary/RsaEcdsaHybridEncrypt$1;)V
    .locals 0

    .line 92
    invoke-direct {p0, p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt;-><init>(Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;)V

    return-void
.end method

.method private signAndSerialize([B)[B
    .locals 4

    .line 215
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 217
    iget-object v1, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->senderSigner:Lcom/google/crypto/tink/PublicKeySign;

    invoke-interface {v1, p1}, Lcom/google/crypto/tink/PublicKeySign;->sign([B)[B

    move-result-object v1

    const/4 v2, 0x4

    .line 220
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 221
    array-length v3, v1

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 222
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 224
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 226
    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 228
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public encrypt([B[B)[B
    .locals 2

    if-nez p2, :cond_0

    .line 206
    :try_start_0
    iget-object p2, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->recipientPublicKey:Ljava/security/PublicKey;

    iget-object v0, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->padding:Lcom/google/capillary/RsaEcdsaConstants$Padding;

    iget-object v1, p0, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->oaepParameterSpec:Ljavax/crypto/spec/OAEPParameterSpec;

    .line 207
    invoke-static {p1, p2, v0, v1}, Lcom/google/capillary/HybridRsaUtils;->encrypt([BLjava/security/PublicKey;Lcom/google/capillary/RsaEcdsaConstants$Padding;Ljavax/crypto/spec/OAEPParameterSpec;)[B

    move-result-object p1

    .line 208
    invoke-direct {p0, p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt;->signAndSerialize([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 210
    new-instance p2, Ljava/security/GeneralSecurityException;

    const-string v0, "encryption failed"

    invoke-direct {p2, v0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    .line 202
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "contextInfo must be null because it is unused"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
