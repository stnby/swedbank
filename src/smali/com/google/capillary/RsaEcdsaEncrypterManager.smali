.class public final Lcom/google/capillary/RsaEcdsaEncrypterManager;
.super Lcom/google/capillary/EncrypterManager;
.source "RsaEcdsaEncrypterManager.java"


# instance fields
.field private final senderSigner:Lcom/google/crypto/tink/PublicKeySign;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/google/capillary/EncrypterManager;-><init>()V

    .line 53
    invoke-static {p1}, Lcom/google/crypto/tink/BinaryKeysetReader;->withInputStream(Ljava/io/InputStream;)Lcom/google/crypto/tink/KeysetReader;

    move-result-object p1

    invoke-static {p1}, Lcom/google/crypto/tink/CleartextKeysetHandle;->read(Lcom/google/crypto/tink/KeysetReader;)Lcom/google/crypto/tink/KeysetHandle;

    move-result-object p1

    .line 54
    invoke-static {p1}, Lcom/google/crypto/tink/signature/PublicKeySignFactory;->getPrimitive(Lcom/google/crypto/tink/KeysetHandle;)Lcom/google/crypto/tink/PublicKeySign;

    move-result-object p1

    iput-object p1, p0, Lcom/google/capillary/RsaEcdsaEncrypterManager;->senderSigner:Lcom/google/crypto/tink/PublicKeySign;

    return-void
.end method


# virtual methods
.method rawLoadPublicKey([B)Lcom/google/crypto/tink/HybridEncrypt;
    .locals 3

    .line 61
    :try_start_0
    invoke-static {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->parseFrom([B)Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "RSA"

    .line 65
    invoke-static {v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    new-instance v1, Ljava/security/spec/X509EncodedKeySpec;

    .line 66
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 65
    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;

    invoke-direct {v1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;-><init>()V

    iget-object v2, p0, Lcom/google/capillary/RsaEcdsaEncrypterManager;->senderSigner:Lcom/google/crypto/tink/PublicKeySign;

    .line 68
    invoke-virtual {v1, v2}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->withSenderSigner(Lcom/google/crypto/tink/PublicKeySign;)Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;

    move-result-object v1

    .line 69
    invoke-virtual {v1, v0}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->withRecipientPublicKey(Ljava/security/PublicKey;)Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {p1}, Lcom/google/capillary/internal/WrappedRsaEcdsaPublicKey;->getPadding()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/google/capillary/RsaEcdsaConstants$Padding;->valueOf(Ljava/lang/String;)Lcom/google/capillary/RsaEcdsaConstants$Padding;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->withPadding(Lcom/google/capillary/RsaEcdsaConstants$Padding;)Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/google/capillary/RsaEcdsaHybridEncrypt$Builder;->build()Lcom/google/capillary/RsaEcdsaHybridEncrypt;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 63
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "unable to parse public key"

    invoke-direct {v0, v1, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
