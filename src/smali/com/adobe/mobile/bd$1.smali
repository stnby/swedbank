.class Lcom/adobe/mobile/bd$1;
.super Ljava/lang/Object;
.source "VisitorIDService.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/bd;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/bd;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/bd;)V
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 6

    const/4 v0, 0x0

    .line 128
    :try_start_0
    iget-object v1, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    iget-object v2, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "ADBMOBILE_VISITORID_IDS"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;Ljava/util/List;)V

    .line 129
    iget-object v1, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ADBMOBILE_PERSISTED_MID"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ADBMOBILE_PERSISTED_MID_HINT"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ADBMOBILE_PERSISTED_MID_BLOB"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/adobe/mobile/bd;->d(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 132
    iget-object v1, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ADBMOBILE_VISITORID_TTL"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;J)J

    .line 133
    iget-object v1, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ADBMOBILE_VISITORID_SYNC"

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;J)J
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 135
    iget-object v2, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {v2, v0}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 136
    iget-object v2, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {v2, v0}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 137
    iget-object v2, p0, Lcom/adobe/mobile/bd$1;->a:Lcom/adobe/mobile/bd;

    invoke-static {v2, v0}, Lcom/adobe/mobile/bd;->d(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "Visitor - Unable to check for stored visitor ID due to context error (%s)"

    const/4 v3, 0x1

    .line 139
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/adobe/mobile/bd$1;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
