.class final enum Lcom/adobe/mobile/AbstractDatabaseBacking$a;
.super Ljava/lang/Enum;
.source "AbstractDatabaseBacking.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/AbstractDatabaseBacking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adobe/mobile/AbstractDatabaseBacking$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

.field public static final enum b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

.field private static final synthetic d:[Lcom/adobe/mobile/AbstractDatabaseBacking$a;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 9
    new-instance v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    const-string v1, "OK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/adobe/mobile/AbstractDatabaseBacking$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->a:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    .line 10
    new-instance v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    const-string v1, "FATALERROR"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/adobe/mobile/AbstractDatabaseBacking$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    const/4 v0, 0x2

    .line 8
    new-array v0, v0, [Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    sget-object v1, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->a:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->d:[Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput p3, p0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->c:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/AbstractDatabaseBacking$a;
    .locals 1

    .line 8
    const-class v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    return-object p0
.end method

.method public static values()[Lcom/adobe/mobile/AbstractDatabaseBacking$a;
    .locals 1

    .line 8
    sget-object v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->d:[Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    invoke-virtual {v0}, [Lcom/adobe/mobile/AbstractDatabaseBacking$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    return-object v0
.end method
