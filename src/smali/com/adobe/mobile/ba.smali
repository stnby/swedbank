.class Lcom/adobe/mobile/ba;
.super Lcom/adobe/mobile/a;
.source "ThirdPartyQueue.java"


# static fields
.field private static final l:[Ljava/lang/String;

.field private static m:Lcom/adobe/mobile/ba;

.field private static final n:Ljava/lang/Object;


# instance fields
.field private k:Landroid/database/sqlite/SQLiteStatement;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "ID"

    const-string v1, "URL"

    const-string v2, "POSTBODY"

    const-string v3, "POSTTYPE"

    const-string v4, "TIMESTAMP"

    const-string v5, "TIMEOUT"

    .line 35
    filled-new-array/range {v0 .. v5}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/ba;->l:[Ljava/lang/String;

    const/4 v0, 0x0

    .line 53
    sput-object v0, Lcom/adobe/mobile/ba;->m:Lcom/adobe/mobile/ba;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/ba;->n:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .line 68
    invoke-direct {p0}, Lcom/adobe/mobile/a;-><init>()V

    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    .line 69
    invoke-virtual {p0}, Lcom/adobe/mobile/ba;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/ba;->d:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/adobe/mobile/ba;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    const-string v0, "CREATE TABLE IF NOT EXISTS HITS (ID INTEGER PRIMARY KEY AUTOINCREMENT, URL TEXT, POSTBODY TEXT, POSTTYPE TEXT, TIMESTAMP INTEGER, TIMEOUT INTEGER)"

    .line 71
    iput-object v0, p0, Lcom/adobe/mobile/ba;->h:Ljava/lang/String;

    const-wide/16 v0, 0x0

    .line 72
    iput-wide v0, p0, Lcom/adobe/mobile/ba;->g:J

    .line 74
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/adobe/mobile/ba;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ba;->a(Ljava/io/File;)V

    .line 75
    invoke-virtual {p0}, Lcom/adobe/mobile/ba;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/adobe/mobile/ba;->f:J

    return-void
.end method

.method protected static p()Lcom/adobe/mobile/ba;
    .locals 2

    .line 56
    sget-object v0, Lcom/adobe/mobile/ba;->n:Ljava/lang/Object;

    monitor-enter v0

    .line 57
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/ba;->m:Lcom/adobe/mobile/ba;

    if-nez v1, :cond_0

    .line 58
    new-instance v1, Lcom/adobe/mobile/ba;

    invoke-direct {v1}, Lcom/adobe/mobile/ba;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ba;->m:Lcom/adobe/mobile/ba;

    .line 61
    :cond_0
    sget-object v1, Lcom/adobe/mobile/ba;->m:Lcom/adobe/mobile/ba;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 62
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 5

    .line 82
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    const-string p1, "%s - Cannot send hit, MobileConfig is null (this really shouldn\'t happen)"

    .line 84
    new-array p2, v2, [Ljava/lang/Object;

    iget-object p3, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object p3, p2, v1

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 88
    :cond_0
    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v0

    sget-object v3, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    if-ne v0, v3, :cond_1

    const-string p1, "%s - Ignoring hit due to privacy status being opted out"

    .line 89
    new-array p2, v2, [Ljava/lang/Object;

    iget-object p3, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object p3, p2, v1

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/adobe/mobile/ba;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v3, 0x2

    .line 97
    :try_start_0
    iget-object v4, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4, v2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    if-eqz p2, :cond_2

    .line 100
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 101
    iget-object v4, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4, v3, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object p2, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {p2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    :goto_0
    const/4 p2, 0x3

    if-eqz p3, :cond_3

    .line 107
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 108
    iget-object v4, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_1

    .line 111
    :cond_3
    iget-object p3, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {p3, p2}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 114
    :goto_1
    iget-object p2, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    const/4 p3, 0x4

    invoke-virtual {p2, p3, p4, p5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 116
    iget-object p2, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    const/4 p3, 0x5

    invoke-virtual {p2, p3, p6, p7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 117
    iget-object p2, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 118
    iget-wide p2, p0, Lcom/adobe/mobile/ba;->f:J

    const-wide/16 p4, 0x1

    add-long/2addr p2, p4

    iput-wide p2, p0, Lcom/adobe/mobile/ba;->f:J

    .line 121
    iget-object p2, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception p2

    :try_start_1
    const-string p3, "%s - Unknown error while inserting url (%s)"

    .line 128
    new-array p4, v3, [Ljava/lang/Object;

    iget-object p5, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object p5, p4, v1

    aput-object p1, p4, v2

    invoke-static {p3, p4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    invoke-virtual {p0, p2}, Lcom/adobe/mobile/ba;->a(Ljava/lang/Exception;)V

    goto :goto_2

    :catch_1
    move-exception p2

    const-string p3, "%s - Unable to insert url (%s)"

    .line 124
    new-array p4, v3, [Ljava/lang/Object;

    iget-object p5, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object p5, p4, v1

    aput-object p1, p4, v2

    invoke-static {p3, p4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    invoke-virtual {p0, p2}, Lcom/adobe/mobile/ba;->a(Ljava/lang/Exception;)V

    .line 131
    :goto_2
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    invoke-virtual {p0, v1}, Lcom/adobe/mobile/ba;->a(Z)V

    return-void

    .line 131
    :goto_3
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method protected b()V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 154
    :try_start_0
    iget-object v3, p0, Lcom/adobe/mobile/ba;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "INSERT INTO HITS (URL, POSTBODY, POSTTYPE, TIMESTAMP, TIMEOUT) VALUES (?, ?, ?, ?, ?)"

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/adobe/mobile/ba;->k:Landroid/database/sqlite/SQLiteStatement;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v4, "%s - Unable to create database due to an unexpected error (%s)"

    .line 163
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v3

    const-string v4, "%s - Unable to create database due to a sql error (%s)"

    .line 160
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v3

    const-string v4, "%s - Unable to create database due to an invalid path (%s)"

    .line 157
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected g()Lcom/adobe/mobile/a$a;
    .locals 15

    .line 171
    iget-object v0, p0, Lcom/adobe/mobile/ba;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    .line 176
    :try_start_0
    iget-object v5, p0, Lcom/adobe/mobile/ba;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "HITS"

    sget-object v7, Lcom/adobe/mobile/ba;->l:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v12, "ID ASC"

    const-string v13, "1"

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 180
    new-instance v6, Lcom/adobe/mobile/a$a;

    invoke-direct {v6}, Lcom/adobe/mobile/a$a;-><init>()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 181
    :try_start_2
    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    .line 182
    invoke-interface {v5, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    .line 183
    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/adobe/mobile/a$a;->d:Ljava/lang/String;

    const/4 v1, 0x3

    .line 184
    invoke-interface {v5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/adobe/mobile/a$a;->e:Ljava/lang/String;

    const/4 v1, 0x4

    .line 185
    invoke-interface {v5, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, v6, Lcom/adobe/mobile/a$a;->c:J

    const/4 v1, 0x5

    .line 186
    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v6, Lcom/adobe/mobile/a$a;->f:I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v1, v6

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_3

    :cond_0
    :goto_0
    if-eqz v5, :cond_1

    .line 199
    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_1
    move-object v6, v1

    goto :goto_4

    :catch_2
    move-exception v6

    move-object v14, v6

    move-object v6, v1

    move-object v1, v14

    goto :goto_1

    :catch_3
    move-exception v6

    move-object v14, v6

    move-object v6, v1

    move-object v1, v14

    goto :goto_3

    :catchall_0
    move-exception v2

    move-object v5, v1

    move-object v1, v2

    goto :goto_5

    :catch_4
    move-exception v5

    move-object v6, v1

    move-object v1, v5

    move-object v5, v6

    :goto_1
    :try_start_4
    const-string v7, "%s - Unknown error reading from database (%s)"

    .line 195
    new-array v4, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v8, v4, v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v7, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v5, :cond_2

    .line 199
    :goto_2
    :try_start_5
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_4

    :catch_5
    move-exception v5

    move-object v6, v1

    move-object v1, v5

    move-object v5, v6

    :goto_3
    :try_start_6
    const-string v7, "%s - Unable to read from database (%s)"

    .line 191
    new-array v4, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v8, v4, v3

    invoke-virtual {v1}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v7, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v5, :cond_2

    goto :goto_2

    .line 202
    :cond_2
    :goto_4
    :try_start_7
    monitor-exit v0

    return-object v6

    :catchall_1
    move-exception v1

    :goto_5
    if-eqz v5, :cond_3

    .line 199
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_6

    :catchall_2
    move-exception v1

    goto :goto_7

    :cond_3
    :goto_6
    throw v1

    .line 202
    :goto_7
    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v1
.end method

.method protected h()Ljava/lang/Runnable;
    .locals 2

    .line 215
    invoke-virtual {p0}, Lcom/adobe/mobile/ba;->o()Lcom/adobe/mobile/ba;

    move-result-object v0

    .line 217
    new-instance v1, Lcom/adobe/mobile/ba$1;

    invoke-direct {v1, p0, v0}, Lcom/adobe/mobile/ba$1;-><init>(Lcom/adobe/mobile/ba;Lcom/adobe/mobile/ba;)V

    return-object v1
.end method

.method protected l()Ljava/lang/String;
    .locals 1

    const-string v0, "External Callback"

    return-object v0
.end method

.method protected m()Ljava/lang/String;
    .locals 1

    const-string v0, "ADBMobile3rdPartyDataCache.sqlite"

    return-object v0
.end method

.method protected o()Lcom/adobe/mobile/ba;
    .locals 1

    .line 208
    invoke-static {}, Lcom/adobe/mobile/ba;->p()Lcom/adobe/mobile/ba;

    move-result-object v0

    return-object v0
.end method
