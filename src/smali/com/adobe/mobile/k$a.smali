.class public Lcom/adobe/mobile/k$a;
.super Ljava/lang/Object;
.source "AudienceManagerWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/adobe/mobile/j$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/adobe/mobile/j$a<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/adobe/mobile/j$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/adobe/mobile/j$a<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    iput-object p1, p0, Lcom/adobe/mobile/k$a;->a:Ljava/util/Map;

    .line 205
    iput-object p2, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 212
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 216
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v3

    invoke-virtual {v3}, Lcom/adobe/mobile/ao;->d()Z

    move-result v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    .line 259
    iget-object v1, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v1, :cond_0

    .line 260
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 265
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void

    .line 221
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v3

    invoke-virtual {v3}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v3

    sget-object v4, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    if-ne v3, v4, :cond_3

    const-string v3, "Audience Manager - Privacy status is set to opt out, no signals will be submitted."

    .line 222
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 259
    iget-object v1, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v1, :cond_2

    .line 260
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 265
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_2
    return-void

    .line 226
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/adobe/mobile/k$a;->a:Ljava/util/Map;

    invoke-static {v3}, Lcom/adobe/mobile/k;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    .line 228
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v4, v1, :cond_5

    const-string v3, "Audience Manager - Unable to create URL object"

    .line 229
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 259
    iget-object v1, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v1, :cond_4

    .line 260
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 265
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_4
    return-void

    :cond_5
    :try_start_3
    const-string v4, "Audience Manager - request (%s)"

    .line 233
    new-array v5, v1, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v4, 0x0

    .line 235
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v5

    invoke-virtual {v5}, Lcom/adobe/mobile/ao;->v()I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    const-string v6, "Audience Manager"

    invoke-static {v3, v4, v5, v6}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v3

    const-string v4, ""

    if-eqz v3, :cond_6

    .line 238
    array-length v5, v3

    if-lez v5, :cond_6

    .line 239
    new-instance v4, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-direct {v4, v3, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 243
    :cond_6
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 246
    invoke-static {v3}, Lcom/adobe/mobile/k;->a(Lorg/json/JSONObject;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 259
    iget-object v1, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v1, :cond_7

    .line 260
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_4
    const-string v4, "Audience Manager - Unexpected error parsing result (%s)"

    .line 255
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 259
    iget-object v1, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v1, :cond_7

    .line 260
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    goto :goto_0

    :catch_1
    move-exception v3

    :try_start_5
    const-string v4, "Audience Manager - Unable to parse JSON data (%s)"

    .line 252
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 259
    iget-object v1, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v1, :cond_7

    .line 260
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    goto :goto_0

    :catch_2
    move-exception v3

    :try_start_6
    const-string v4, "Audience Manager - Unable to decode server response (%s)"

    .line 249
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 259
    iget-object v1, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v1, :cond_7

    .line 260
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 265
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_7
    return-void

    .line 259
    :goto_1
    iget-object v2, p0, Lcom/adobe/mobile/k$a;->b:Lcom/adobe/mobile/j$a;

    if-eqz v2, :cond_8

    .line 260
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/adobe/mobile/k$a$1;

    invoke-direct {v3, p0, v0}, Lcom/adobe/mobile/k$a$1;-><init>(Lcom/adobe/mobile/k$a;Ljava/util/HashMap;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 265
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    :cond_8
    throw v1
.end method
