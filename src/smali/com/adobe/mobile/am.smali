.class final Lcom/adobe/mobile/am;
.super Lcom/adobe/mobile/al;
.source "MessageTemplatePii.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/adobe/mobile/al;-><init>()V

    return-void
.end method


# virtual methods
.method protected b(Lorg/json/JSONObject;)Z
    .locals 3

    .line 14
    invoke-super {p0, p1}, Lcom/adobe/mobile/al;->b(Lorg/json/JSONObject;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 19
    :cond_0
    iget-object p1, p0, Lcom/adobe/mobile/am;->k:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v1, 0x1

    if-lez p1, :cond_2

    iget-object p1, p0, Lcom/adobe/mobile/am;->k:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v2, "https"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    :goto_0
    const-string p1, "Data Callback - Unable to create data callback %s, templateurl is empty or does not use https for request"

    .line 20
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/am;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    const-string v0, "PII"

    return-object v0
.end method

.method protected l()Lcom/adobe/mobile/ba;
    .locals 1

    .line 35
    invoke-static {}, Lcom/adobe/mobile/as;->n()Lcom/adobe/mobile/as;

    move-result-object v0

    return-object v0
.end method
