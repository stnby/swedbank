.class Lcom/adobe/mobile/ay$2$1;
.super Ljava/lang/Object;
.source "TargetPreviewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/ay$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/ay$2;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/ay$2;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/adobe/mobile/ay$2$1;->a:Lcom/adobe/mobile/ay$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v0, 0x0

    .line 215
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "Could not download Target Preview UI. Please try again!"

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Could not show error message!(%s) "

    const/4 v3, 0x1

    .line 217
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
