.class final Lcom/adobe/mobile/ao;
.super Ljava/lang/Object;
.source "MobileConfig.java"


# static fields
.field private static E:Lcom/adobe/mobile/ao;

.field private static final F:Ljava/lang/Object;

.field private static final H:Ljava/lang/Object;

.field private static final J:Ljava/lang/Object;

.field private static final L:Ljava/lang/Object;

.field private static final N:Ljava/lang/Object;

.field private static O:Ljava/io/InputStream;

.field private static final P:Ljava/lang/Object;

.field private static final a:Lcom/adobe/mobile/aq;


# instance fields
.field private A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adobe/mobile/p;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Z

.field private G:Ljava/lang/Boolean;

.field private I:Ljava/lang/Boolean;

.field private K:Ljava/lang/Boolean;

.field private M:Ljava/lang/Boolean;

.field private b:Z

.field private c:Lcom/adobe/mobile/l$a;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:Lcom/adobe/mobile/aq;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:I

.field private r:J

.field private s:Ljava/lang/String;

.field private t:Z

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adobe/mobile/p;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adobe/mobile/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 99
    sget-object v0, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    sput-object v0, Lcom/adobe/mobile/ao;->a:Lcom/adobe/mobile/aq;

    const/4 v0, 0x0

    .line 165
    sput-object v0, Lcom/adobe/mobile/ao;->E:Lcom/adobe/mobile/ao;

    .line 166
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ao;->F:Ljava/lang/Object;

    .line 443
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ao;->H:Ljava/lang/Object;

    .line 462
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ao;->J:Ljava/lang/Object;

    .line 473
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ao;->L:Ljava/lang/Object;

    .line 494
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ao;->N:Ljava/lang/Object;

    .line 1026
    sput-object v0, Lcom/adobe/mobile/ao;->O:Ljava/io/InputStream;

    .line 1027
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/ao;->P:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 11

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 108
    iput-boolean v0, p0, Lcom/adobe/mobile/ao;->b:Z

    const/4 v1, 0x0

    .line 109
    iput-object v1, p0, Lcom/adobe/mobile/ao;->c:Lcom/adobe/mobile/l$a;

    .line 114
    iput-object v1, p0, Lcom/adobe/mobile/ao;->d:Ljava/lang/String;

    .line 115
    iput-object v1, p0, Lcom/adobe/mobile/ao;->e:Ljava/lang/String;

    const-string v2, "UTF-8"

    .line 116
    iput-object v2, p0, Lcom/adobe/mobile/ao;->f:Ljava/lang/String;

    const/4 v2, 0x1

    .line 117
    iput-boolean v2, p0, Lcom/adobe/mobile/ao;->g:Z

    .line 118
    iput-boolean v0, p0, Lcom/adobe/mobile/ao;->h:Z

    .line 119
    iput-boolean v2, p0, Lcom/adobe/mobile/ao;->i:Z

    const/16 v3, 0x12c

    .line 120
    iput v3, p0, Lcom/adobe/mobile/ao;->j:I

    .line 121
    iput v0, p0, Lcom/adobe/mobile/ao;->k:I

    .line 122
    iput v0, p0, Lcom/adobe/mobile/ao;->l:I

    .line 123
    sget-object v4, Lcom/adobe/mobile/ao;->a:Lcom/adobe/mobile/aq;

    iput-object v4, p0, Lcom/adobe/mobile/ao;->m:Lcom/adobe/mobile/aq;

    .line 124
    iput-object v1, p0, Lcom/adobe/mobile/ao;->n:Ljava/util/List;

    .line 125
    iput-object v1, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;

    .line 130
    iput-object v1, p0, Lcom/adobe/mobile/ao;->p:Ljava/lang/String;

    const/4 v4, 0x2

    .line 131
    iput v4, p0, Lcom/adobe/mobile/ao;->q:I

    const-wide/16 v5, 0x0

    .line 132
    iput-wide v5, p0, Lcom/adobe/mobile/ao;->r:J

    .line 137
    iput-object v1, p0, Lcom/adobe/mobile/ao;->s:Ljava/lang/String;

    .line 138
    iput-boolean v0, p0, Lcom/adobe/mobile/ao;->t:Z

    .line 139
    iput v4, p0, Lcom/adobe/mobile/ao;->u:I

    .line 144
    iput-object v1, p0, Lcom/adobe/mobile/ao;->v:Ljava/lang/String;

    .line 145
    iput-object v1, p0, Lcom/adobe/mobile/ao;->w:Ljava/lang/String;

    .line 150
    iput-object v1, p0, Lcom/adobe/mobile/ao;->x:Ljava/lang/String;

    .line 151
    iput-object v1, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    .line 152
    iput-object v1, p0, Lcom/adobe/mobile/ao;->z:Ljava/util/ArrayList;

    .line 153
    iput-object v1, p0, Lcom/adobe/mobile/ao;->A:Ljava/util/ArrayList;

    .line 158
    iput-object v1, p0, Lcom/adobe/mobile/ao;->B:Ljava/lang/String;

    .line 159
    iput-object v1, p0, Lcom/adobe/mobile/ao;->C:Ljava/lang/String;

    .line 160
    iput-boolean v0, p0, Lcom/adobe/mobile/ao;->D:Z

    .line 442
    iput-object v1, p0, Lcom/adobe/mobile/ao;->G:Ljava/lang/Boolean;

    .line 461
    iput-object v1, p0, Lcom/adobe/mobile/ao;->I:Ljava/lang/Boolean;

    .line 472
    iput-object v1, p0, Lcom/adobe/mobile/ao;->K:Ljava/lang/Boolean;

    .line 493
    iput-object v1, p0, Lcom/adobe/mobile/ao;->M:Ljava/lang/Boolean;

    .line 183
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->H()V

    .line 186
    invoke-direct {p0}, Lcom/adobe/mobile/ao;->I()Lorg/json/JSONObject;

    move-result-object v7

    if-nez v7, :cond_0

    return-void

    :cond_0
    :try_start_0
    const-string v8, "analytics"

    .line 195
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v8, "Analytics - Not configured."

    .line 197
    new-array v9, v0, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v8, v1

    :goto_0
    if-eqz v8, :cond_3

    :try_start_1
    const-string v9, "server"

    .line 203
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/adobe/mobile/ao;->e:Ljava/lang/String;

    const-string v9, "rsids"

    .line 204
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/adobe/mobile/ao;->d:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 206
    :catch_1
    iput-object v1, p0, Lcom/adobe/mobile/ao;->e:Ljava/lang/String;

    .line 207
    iput-object v1, p0, Lcom/adobe/mobile/ao;->d:Ljava/lang/String;

    const-string v9, "Analytics - Not Configured."

    .line 208
    new-array v10, v0, [Ljava/lang/Object;

    invoke-static {v9, v10}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    :try_start_2
    const-string v9, "charset"

    .line 212
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/adobe/mobile/ao;->f:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    const-string v9, "UTF-8"

    .line 214
    iput-object v9, p0, Lcom/adobe/mobile/ao;->f:Ljava/lang/String;

    :goto_2
    :try_start_3
    const-string v9, "ssl"

    .line 218
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/adobe/mobile/ao;->g:Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    .line 220
    :catch_3
    iput-boolean v2, p0, Lcom/adobe/mobile/ao;->g:Z

    :goto_3
    :try_start_4
    const-string v9, "offlineEnabled"

    .line 224
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/adobe/mobile/ao;->h:Z
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_4

    .line 226
    :catch_4
    iput-boolean v0, p0, Lcom/adobe/mobile/ao;->h:Z

    :goto_4
    :try_start_5
    const-string v9, "backdateSessionInfo"

    .line 230
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/adobe/mobile/ao;->i:Z
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_5

    .line 232
    :catch_5
    iput-boolean v2, p0, Lcom/adobe/mobile/ao;->i:Z

    :goto_5
    :try_start_6
    const-string v9, "lifecycleTimeout"

    .line 236
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lcom/adobe/mobile/ao;->j:I
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_6

    .line 238
    :catch_6
    iput v3, p0, Lcom/adobe/mobile/ao;->j:I

    :goto_6
    :try_start_7
    const-string v3, "referrerTimeout"

    .line 242
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/adobe/mobile/ao;->k:I
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_7

    .line 244
    :catch_7
    iput v0, p0, Lcom/adobe/mobile/ao;->k:I

    :goto_7
    :try_start_8
    const-string v3, "batchLimit"

    .line 248
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/adobe/mobile/ao;->l:I
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_8

    .line 250
    :catch_8
    iput v0, p0, Lcom/adobe/mobile/ao;->l:I

    .line 255
    :goto_8
    :try_start_9
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v9, "PrivacyStatus"

    invoke-interface {v3, v9}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 256
    invoke-static {}, Lcom/adobe/mobile/aq;->values()[Lcom/adobe/mobile/aq;

    move-result-object v3

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v9

    const-string v10, "PrivacyStatus"

    invoke-interface {v9, v10, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    aget-object v3, v3, v9

    iput-object v3, p0, Lcom/adobe/mobile/ao;->m:Lcom/adobe/mobile/aq;
    :try_end_9
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_9 .. :try_end_9} :catch_b

    goto :goto_b

    :cond_1
    :try_start_a
    const-string v3, "privacyDefault"

    .line 261
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_9
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_a .. :try_end_a} :catch_b

    goto :goto_9

    :catch_9
    move-object v3, v1

    :goto_9
    if-eqz v3, :cond_2

    .line 266
    :try_start_b
    invoke-direct {p0, v3}, Lcom/adobe/mobile/ao;->b(Ljava/lang/String;)Lcom/adobe/mobile/aq;

    move-result-object v3

    goto :goto_a

    :cond_2
    sget-object v3, Lcom/adobe/mobile/ao;->a:Lcom/adobe/mobile/aq;

    :goto_a
    iput-object v3, p0, Lcom/adobe/mobile/ao;->m:Lcom/adobe/mobile/aq;
    :try_end_b
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_b .. :try_end_b} :catch_b

    :goto_b
    :try_start_c
    const-string v3, "poi"

    .line 275
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 276
    invoke-direct {p0, v3}, Lcom/adobe/mobile/ao;->a(Lorg/json/JSONArray;)V
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_a

    goto :goto_c

    :catch_a
    move-exception v3

    const-string v8, "Analytics - Malformed POI List(%s)"

    .line 278
    new-array v9, v2, [Ljava/lang/Object;

    invoke-virtual {v3}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v0

    invoke-static {v8, v9}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_c

    :catch_b
    move-exception v1

    const-string v3, "Config - Error pulling privacy from shared preferences. (%s)"

    .line 270
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v3, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_3
    :goto_c
    :try_start_d
    const-string v3, "target"

    .line 286
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3
    :try_end_d
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_c

    goto :goto_d

    :catch_c
    const-string v3, "Target - Not Configured."

    .line 288
    new-array v8, v0, [Ljava/lang/Object;

    invoke-static {v3, v8}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v3, v1

    :goto_d
    if-eqz v3, :cond_4

    :try_start_e
    const-string v8, "clientCode"

    .line 293
    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/adobe/mobile/ao;->p:Ljava/lang/String;
    :try_end_e
    .catch Lorg/json/JSONException; {:try_start_e .. :try_end_e} :catch_d

    goto :goto_e

    .line 295
    :catch_d
    iput-object v1, p0, Lcom/adobe/mobile/ao;->p:Ljava/lang/String;

    const-string v8, "Target - Not Configured."

    .line 296
    new-array v9, v0, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_e
    :try_start_f
    const-string v8, "timeout"

    .line 300
    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/adobe/mobile/ao;->q:I
    :try_end_f
    .catch Lorg/json/JSONException; {:try_start_f .. :try_end_f} :catch_e

    goto :goto_f

    .line 302
    :catch_e
    iput v4, p0, Lcom/adobe/mobile/ao;->q:I

    :goto_f
    :try_start_10
    const-string v8, "environmentId"

    .line 306
    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/adobe/mobile/ao;->r:J
    :try_end_10
    .catch Lorg/json/JSONException; {:try_start_10 .. :try_end_10} :catch_f

    goto :goto_10

    .line 308
    :catch_f
    iput-wide v5, p0, Lcom/adobe/mobile/ao;->r:J

    :cond_4
    :goto_10
    :try_start_11
    const-string v3, "audienceManager"

    .line 316
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3
    :try_end_11
    .catch Lorg/json/JSONException; {:try_start_11 .. :try_end_11} :catch_10

    goto :goto_11

    :catch_10
    const-string v3, "Audience Manager - Not Configured."

    .line 318
    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v3, v1

    :goto_11
    if-eqz v3, :cond_6

    :try_start_12
    const-string v5, "server"

    .line 323
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/adobe/mobile/ao;->s:Ljava/lang/String;
    :try_end_12
    .catch Lorg/json/JSONException; {:try_start_12 .. :try_end_12} :catch_11

    goto :goto_12

    .line 325
    :catch_11
    iput-object v1, p0, Lcom/adobe/mobile/ao;->s:Ljava/lang/String;

    const-string v5, "Audience Manager - Not Configured."

    .line 326
    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_12
    :try_start_13
    const-string v5, "analyticsForwardingEnabled"

    .line 330
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/adobe/mobile/ao;->t:Z
    :try_end_13
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_13} :catch_12

    goto :goto_13

    .line 332
    :catch_12
    iput-boolean v0, p0, Lcom/adobe/mobile/ao;->t:Z

    .line 335
    :goto_13
    iget-boolean v5, p0, Lcom/adobe/mobile/ao;->t:Z

    if-eqz v5, :cond_5

    const-string v5, "Audience Manager - Analytics Server-Side Forwarding Is Enabled."

    .line 336
    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    :try_start_14
    const-string v5, "timeout"

    .line 340
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/adobe/mobile/ao;->u:I
    :try_end_14
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_14} :catch_13

    goto :goto_14

    .line 342
    :catch_13
    iput v4, p0, Lcom/adobe/mobile/ao;->u:I

    :cond_6
    :goto_14
    :try_start_15
    const-string v3, "acquisition"

    .line 350
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3
    :try_end_15
    .catch Lorg/json/JSONException; {:try_start_15 .. :try_end_15} :catch_14

    goto :goto_15

    :catch_14
    const-string v3, "Acquisition - Not Configured."

    .line 352
    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v3, v1

    :goto_15
    if-eqz v3, :cond_7

    :try_start_16
    const-string v4, "appid"

    .line 357
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/adobe/mobile/ao;->w:Ljava/lang/String;

    const-string v4, "server"

    .line 358
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/adobe/mobile/ao;->v:Ljava/lang/String;
    :try_end_16
    .catch Lorg/json/JSONException; {:try_start_16 .. :try_end_16} :catch_15

    goto :goto_16

    .line 361
    :catch_15
    iput-object v1, p0, Lcom/adobe/mobile/ao;->w:Ljava/lang/String;

    .line 362
    iput-object v1, p0, Lcom/adobe/mobile/ao;->v:Ljava/lang/String;

    const-string v3, "Acquisition - Not configured correctly (missing server or app identifier)."

    .line 363
    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    :goto_16
    :try_start_17
    const-string v3, "remotes"

    .line 370
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3
    :try_end_17
    .catch Lorg/json/JSONException; {:try_start_17 .. :try_end_17} :catch_16

    goto :goto_17

    :catch_16
    const-string v3, "Remotes - Not Configured."

    .line 373
    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v3, v1

    :goto_17
    if-eqz v3, :cond_8

    :try_start_18
    const-string v4, "messages"

    .line 378
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/adobe/mobile/ao;->x:Ljava/lang/String;
    :try_end_18
    .catch Lorg/json/JSONException; {:try_start_18 .. :try_end_18} :catch_17

    goto :goto_18

    :catch_17
    move-exception v4

    const-string v5, "Config - No in-app messages remote url loaded (%s)"

    .line 380
    new-array v6, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v0

    invoke-static {v5, v6}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_18
    :try_start_19
    const-string v4, "analytics.poi"

    .line 384
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;
    :try_end_19
    .catch Lorg/json/JSONException; {:try_start_19 .. :try_end_19} :catch_18

    goto :goto_19

    :catch_18
    move-exception v3

    const-string v4, "Config - No points of interest remote url loaded (%s)"

    .line 386
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    :goto_19
    :try_start_1a
    const-string v2, "messages"

    .line 393
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2
    :try_end_1a
    .catch Lorg/json/JSONException; {:try_start_1a .. :try_end_1a} :catch_19

    goto :goto_1a

    :catch_19
    const-string v2, "Messages - Not configured locally."

    .line 395
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v2, v1

    :goto_1a
    if-eqz v2, :cond_9

    .line 398
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_9

    .line 399
    invoke-direct {p0, v2}, Lcom/adobe/mobile/ao;->b(Lorg/json/JSONArray;)V

    :cond_9
    :try_start_1b
    const-string v2, "marketingCloud"

    .line 405
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2
    :try_end_1b
    .catch Lorg/json/JSONException; {:try_start_1b .. :try_end_1b} :catch_1a

    goto :goto_1b

    :catch_1a
    const-string v2, "Marketing Cloud - Not configured locally."

    .line 407
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v2, v1

    :goto_1b
    if-eqz v2, :cond_a

    :try_start_1c
    const-string v3, "org"

    .line 412
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/adobe/mobile/ao;->B:Ljava/lang/String;
    :try_end_1c
    .catch Lorg/json/JSONException; {:try_start_1c .. :try_end_1c} :catch_1b

    goto :goto_1c

    .line 414
    :catch_1b
    iput-object v1, p0, Lcom/adobe/mobile/ao;->B:Ljava/lang/String;

    const-string v3, "Visitor - ID Service Not Configured."

    .line 415
    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1c
    :try_start_1d
    const-string v3, "server"

    .line 421
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/adobe/mobile/ao;->C:Ljava/lang/String;
    :try_end_1d
    .catch Lorg/json/JSONException; {:try_start_1d .. :try_end_1d} :catch_1c

    goto :goto_1d

    .line 423
    :catch_1c
    iput-object v1, p0, Lcom/adobe/mobile/ao;->C:Ljava/lang/String;

    const-string v1, "Visitor ID Service - Custom endpoint not found in configuration, using default endpoint."

    .line 424
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1d
    :try_start_1e
    const-string v1, "coopUnsafe"

    .line 428
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/adobe/mobile/ao;->D:Z
    :try_end_1e
    .catch Lorg/json/JSONException; {:try_start_1e .. :try_end_1e} :catch_1d

    goto :goto_1e

    :catch_1d
    const-string v1, "Visitor - Coop Unsafe Not Configured."

    .line 430
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 436
    :cond_a
    :goto_1e
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->F()V

    .line 439
    invoke-direct {p0}, Lcom/adobe/mobile/ao;->J()V

    return-void
.end method

.method private I()Lorg/json/JSONObject;
    .locals 6

    .line 916
    sget-object v0, Lcom/adobe/mobile/ao;->P:Ljava/lang/Object;

    monitor-enter v0

    .line 917
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/ao;->O:Ljava/io/InputStream;

    .line 918
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    :try_start_1
    const-string v4, "Config - Attempting to load config file from override stream"

    .line 923
    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 924
    invoke-direct {p0, v1}, Lcom/adobe/mobile/ao;->a(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v2, v4

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v5, "Config - Error parsing user defined config (%s)"

    .line 931
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v4

    const-string v5, "Config - Error loading user defined config (%s)"

    .line 928
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    if-nez v2, :cond_2

    if-eqz v1, :cond_1

    const-string v1, "Config - Failed attempting to load custom config, will fall back to standard config location."

    .line 937
    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    const-string v1, "Config - Attempting to load config file from default location"

    .line 940
    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "ADBMobileConfig.json"

    .line 941
    invoke-direct {p0, v1}, Lcom/adobe/mobile/ao;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v1, "Config - Could not find config file at expected location.  Attempting to load from www folder"

    .line 946
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 947
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "www"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "ADBMobileConfig.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/adobe/mobile/ao;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    :cond_2
    return-object v2

    :catchall_0
    move-exception v1

    .line 918
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private J()V
    .locals 5

    .line 1150
    iget-object v0, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1151
    iget-object v0, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adobe/mobile/p;

    .line 1152
    invoke-virtual {v1}, Lcom/adobe/mobile/p;->d()Ljava/util/HashMap;

    move-result-object v2

    .line 1155
    invoke-virtual {v1}, Lcom/adobe/mobile/p;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    invoke-virtual {v3}, Lcom/adobe/mobile/an$a;->a()I

    move-result v3

    iget-object v4, v1, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v3, v2, :cond_0

    .line 1156
    invoke-virtual {v1}, Lcom/adobe/mobile/p;->b()V

    goto :goto_0

    .line 1161
    :cond_1
    iget-object v0, p0, Lcom/adobe/mobile/ao;->z:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1162
    iget-object v0, p0, Lcom/adobe/mobile/ao;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adobe/mobile/p;

    .line 1163
    invoke-virtual {v1}, Lcom/adobe/mobile/p;->d()Ljava/util/HashMap;

    move-result-object v2

    .line 1166
    invoke-virtual {v1}, Lcom/adobe/mobile/p;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    invoke-virtual {v3}, Lcom/adobe/mobile/an$a;->a()I

    move-result v3

    iget-object v4, v1, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v3, v2, :cond_2

    .line 1167
    invoke-virtual {v1}, Lcom/adobe/mobile/p;->b()V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private K()V
    .locals 2

    .line 1175
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->u()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ao$3;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/ao$3;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected static a()Lcom/adobe/mobile/ao;
    .locals 2

    .line 168
    sget-object v0, Lcom/adobe/mobile/ao;->F:Ljava/lang/Object;

    monitor-enter v0

    .line 169
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/ao;->E:Lcom/adobe/mobile/ao;

    if-nez v1, :cond_0

    .line 170
    new-instance v1, Lcom/adobe/mobile/ao;

    invoke-direct {v1}, Lcom/adobe/mobile/ao;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ao;->E:Lcom/adobe/mobile/ao;

    .line 173
    :cond_0
    sget-object v1, Lcom/adobe/mobile/ao;->E:Lcom/adobe/mobile/ao;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 174
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method static synthetic a(Lcom/adobe/mobile/ao;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/adobe/mobile/ao;->x:Ljava/lang/String;

    return-object p0
.end method

.method private a(Ljava/io/InputStream;)Lorg/json/JSONObject;
    .locals 5

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1002
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2

    new-array v2, v2, [B

    .line 1003
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    .line 1006
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v2, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 1007
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1017
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v3, "Config - Unable to close stream (%s)"

    .line 1019
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v2

    :catchall_0
    move-exception v2

    goto :goto_3

    :catch_1
    move-exception v2

    :try_start_2
    const-string v3, "Config - Stream closed when attempting to load config (%s)"

    .line 1013
    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1017
    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    :catch_2
    move-exception p1

    const-string v2, "Config - Unable to close stream (%s)"

    .line 1019
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    goto :goto_1

    :catch_3
    move-exception v2

    :try_start_4
    const-string v3, "Config - Exception when reading config (%s)"

    .line 1010
    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1017
    :try_start_5
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_2

    :catch_4
    move-exception p1

    const-string v2, "Config - Unable to close stream (%s)"

    .line 1019
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    :goto_1
    invoke-static {v2, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1023
    :goto_2
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    return-object p1

    .line 1017
    :goto_3
    :try_start_6
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_4

    :catch_5
    move-exception p1

    .line 1019
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    const-string p1, "Config - Unable to close stream (%s)"

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1020
    :goto_4
    throw v2
.end method

.method private a(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 957
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-nez v3, :cond_0

    return-object v2

    .line 962
    :cond_0
    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    if-nez v3, :cond_1

    return-object v2

    .line 967
    :cond_1
    invoke-virtual {v3, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/adobe/mobile/ao;->a(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v3, "Config - Null context when attempting to read config file (%s)"

    .line 976
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p1

    const-string v3, "Config - Exception parsing config file (%s)"

    .line 973
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception p1

    const-string v3, "Config - Exception loading config file (%s)"

    .line 970
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    move-object p1, v2

    :goto_1
    return-object p1
.end method

.method private a(Lorg/json/JSONArray;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1060
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/adobe/mobile/ao;->n:Ljava/util/List;

    .line 1061
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 1062
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v4

    .line 1063
    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x4

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1065
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1066
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x2

    .line 1067
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x3

    .line 1068
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1070
    iget-object v4, p0, Lcom/adobe/mobile/ao;->n:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v2, "Messages - Unable to parse remote points of interest JSON (%s)"

    .line 1074
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    invoke-static {v2, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string p1, "Messages - Remote messages config was null, falling back to bundled messages"

    .line 1084
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string p1, "messageImages"

    .line 1085
    invoke-static {p1}, Lcom/adobe/mobile/au;->c(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "messages"

    .line 1092
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, "Messages - Remote messages not configured, falling back to bundled messages"

    .line 1095
    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object p1, v1

    :goto_0
    const-string v2, "Messages - Using remote definition for messages"

    .line 1098
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    .line 1101
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_1

    .line 1109
    :cond_1
    invoke-direct {p0, p1}, Lcom/adobe/mobile/ao;->b(Lorg/json/JSONArray;)V

    return-void

    :cond_2
    :goto_1
    const-string p1, "messageImages"

    .line 1102
    invoke-static {p1}, Lcom/adobe/mobile/au;->c(Ljava/lang/String;)V

    .line 1103
    iput-object v1, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    .line 1104
    iput-object v1, p0, Lcom/adobe/mobile/ao;->z:Ljava/util/ArrayList;

    .line 1105
    iput-object v1, p0, Lcom/adobe/mobile/ao;->A:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lcom/adobe/mobile/ao;Z)Z
    .locals 0

    .line 48
    iput-boolean p1, p0, Lcom/adobe/mobile/ao;->b:Z

    return p1
.end method

.method private b(Ljava/lang/String;)Lcom/adobe/mobile/aq;
    .locals 1

    if-eqz p1, :cond_2

    const-string v0, "optedin"

    .line 1038
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1039
    sget-object p1, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    return-object p1

    :cond_0
    const-string v0, "optedout"

    .line 1041
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1042
    sget-object p1, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    return-object p1

    :cond_1
    const-string v0, "optunknown"

    .line 1044
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1045
    sget-object p1, Lcom/adobe/mobile/aq;->c:Lcom/adobe/mobile/aq;

    return-object p1

    .line 1049
    :cond_2
    sget-object p1, Lcom/adobe/mobile/ao;->a:Lcom/adobe/mobile/aq;

    return-object p1
.end method

.method static synthetic b(Lcom/adobe/mobile/ao;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/adobe/mobile/ao;->K()V

    return-void
.end method

.method private b(Lorg/json/JSONArray;)V
    .locals 11

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1115
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1116
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1117
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1119
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v5

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_4

    .line 1122
    invoke-virtual {p1, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 1124
    invoke-static {v7}, Lcom/adobe/mobile/p;->a(Lorg/json/JSONObject;)Lcom/adobe/mobile/p;

    move-result-object v7

    if-eqz v7, :cond_3

    const-string v8, "Messages - loaded message - %s"

    .line 1127
    new-array v9, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/adobe/mobile/p;->i()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v0

    invoke-static {v8, v9}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1130
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-class v9, Lcom/adobe/mobile/am;

    if-ne v8, v9, :cond_0

    .line 1131
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1132
    :cond_0
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-class v9, Lcom/adobe/mobile/al;

    if-eq v8, v9, :cond_2

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-class v9, Lcom/adobe/mobile/aj;

    if-ne v8, v9, :cond_1

    goto :goto_1

    .line 1135
    :cond_1
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1133
    :cond_2
    :goto_1
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1140
    :cond_4
    iput-object v2, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    .line 1141
    iput-object v3, p0, Lcom/adobe/mobile/ao;->z:Ljava/util/ArrayList;

    .line 1142
    iput-object v4, p0, Lcom/adobe/mobile/ao;->A:Ljava/util/ArrayList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    const-string v2, "Messages - Unable to parse messages JSON (%s)"

    .line 1145
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    invoke-static {v2, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    return-void
.end method

.method static synthetic c(Lcom/adobe/mobile/ao;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/adobe/mobile/ao;->J()V

    return-void
.end method

.method static synthetic d(Lcom/adobe/mobile/ao;)Ljava/util/ArrayList;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic e(Lcom/adobe/mobile/ao;)Z
    .locals 0

    .line 48
    iget-boolean p0, p0, Lcom/adobe/mobile/ao;->b:Z

    return p0
.end method


# virtual methods
.method protected A()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/adobe/mobile/p;",
            ">;"
        }
    .end annotation

    .line 889
    iget-object v0, p0, Lcom/adobe/mobile/ao;->z:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected B()Ljava/lang/String;
    .locals 1

    .line 897
    iget-object v0, p0, Lcom/adobe/mobile/ao;->B:Ljava/lang/String;

    return-object v0
.end method

.method protected C()Ljava/lang/String;
    .locals 1

    .line 900
    iget-object v0, p0, Lcom/adobe/mobile/ao;->C:Ljava/lang/String;

    return-object v0
.end method

.method protected D()Z
    .locals 1

    .line 902
    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->D:Z

    return v0
.end method

.method protected E()Z
    .locals 1

    .line 905
    iget-object v0, p0, Lcom/adobe/mobile/ao;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/ao;->B:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected F()V
    .locals 1

    .line 983
    iget-object v0, p0, Lcom/adobe/mobile/ao;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/ao;->x:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 984
    iget-object v0, p0, Lcom/adobe/mobile/ao;->x:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/au;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ao;->a(Ljava/io/File;)V

    .line 987
    :cond_0
    iget-object v0, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 988
    iget-object v0, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/au;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ao;->b(Ljava/io/File;)V

    :cond_1
    return-void
.end method

.method protected G()Z
    .locals 1

    .line 1224
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->b:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method protected H()V
    .locals 5

    .line 1230
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1234
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Analytics - Error registering network receiver (%s)"

    const/4 v3, 0x1

    .line 1236
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_0

    return-void

    .line 1241
    :cond_0
    new-instance v2, Lcom/adobe/mobile/ao$4;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ao$4;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected a(Lcom/adobe/mobile/aq;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 579
    :cond_0
    sget-object v0, Lcom/adobe/mobile/aq;->c:Lcom/adobe/mobile/aq;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->h:Z

    if-nez v0, :cond_1

    const-string p1, "Analytics - Cannot set privacy status to unknown when offline tracking is disabled"

    .line 580
    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 585
    :cond_1
    sget-object v0, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    if-ne p1, v0, :cond_2

    .line 586
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v2, Lcom/adobe/mobile/ao$1;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ao$1;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 593
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->t()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v2, Lcom/adobe/mobile/ao$5;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ao$5;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 600
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->w()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v2, Lcom/adobe/mobile/ao$6;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ao$6;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 609
    :cond_2
    sget-object v0, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    if-ne p1, v0, :cond_3

    .line 610
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v2, Lcom/adobe/mobile/ao$7;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ao$7;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 617
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->t()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v2, Lcom/adobe/mobile/ao$8;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ao$8;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 624
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->w()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v2, Lcom/adobe/mobile/ao$9;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ao$9;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 631
    invoke-static {}, Lcom/adobe/mobile/ap;->a()V

    .line 635
    :cond_3
    iput-object p1, p0, Lcom/adobe/mobile/ao;->m:Lcom/adobe/mobile/aq;

    .line 637
    invoke-virtual {p1}, Lcom/adobe/mobile/aq;->a()I

    move-result v0

    invoke-static {v0}, Lcom/adobe/mobile/be;->a(I)V

    .line 639
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "PrivacyStatus"

    .line 640
    invoke-virtual {p1}, Lcom/adobe/mobile/aq;->a()I

    move-result p1

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 641
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "Config - Error persisting privacy status (%s)."

    const/4 v2, 0x1

    .line 643
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v1

    invoke-static {v0, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected a(Lcom/adobe/mobile/l$c;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/adobe/mobile/l$c;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 524
    iget-object v0, p0, Lcom/adobe/mobile/ao;->c:Lcom/adobe/mobile/l$a;

    if-nez v0, :cond_0

    const-string p1, "Config - A callback has not been registered for Adobe events."

    const/4 p2, 0x0

    .line 525
    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    if-eqz p2, :cond_1

    .line 530
    iget-object v0, p0, Lcom/adobe/mobile/ao;->c:Lcom/adobe/mobile/l$a;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, p1, v1}, Lcom/adobe/mobile/l$a;->a(Lcom/adobe/mobile/l$c;Ljava/util/Map;)V

    goto :goto_0

    .line 532
    :cond_1
    iget-object p2, p0, Lcom/adobe/mobile/ao;->c:Lcom/adobe/mobile/l$a;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Lcom/adobe/mobile/l$a;->a(Lcom/adobe/mobile/l$c;Ljava/util/Map;)V

    :goto_0
    return-void
.end method

.method protected a(Ljava/io/File;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 790
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 791
    :try_start_1
    invoke-direct {p0, v3}, Lcom/adobe/mobile/ao;->a(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object p1

    .line 792
    invoke-direct {p0, p1}, Lcom/adobe/mobile/ao;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 803
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    const-string v2, "Messages - Unable to close file stream (%s)"

    .line 807
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    :goto_0
    invoke-static {v2, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :catchall_0
    move-exception p1

    move-object v2, v3

    goto :goto_4

    :catch_1
    move-exception p1

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception p1

    move-object v2, v3

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_4

    :catch_3
    move-exception p1

    :goto_1
    :try_start_3
    const-string v3, "Messages - Unable to open messages config file, falling back to bundled messages (%s)"

    .line 798
    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v0

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_1

    .line 803
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_3

    :catch_4
    move-exception p1

    const-string v2, "Messages - Unable to close file stream (%s)"

    .line 807
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    goto :goto_0

    :catch_5
    move-exception p1

    :goto_2
    :try_start_5
    const-string v3, "Messages - Unable to read messages remote config file, falling back to bundled messages (%s)"

    .line 795
    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v0

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v2, :cond_1

    .line 803
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_3

    :catch_6
    move-exception p1

    const-string v2, "Messages - Unable to close file stream (%s)"

    .line 807
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    goto :goto_0

    :cond_1
    :goto_3
    return-void

    :goto_4
    if-eqz v2, :cond_2

    .line 803
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_5

    :catch_7
    move-exception v2

    .line 807
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const-string v0, "Messages - Unable to close file stream (%s)"

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 808
    :cond_2
    :goto_5
    throw p1
.end method

.method protected a(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x1

    if-eqz p1, :cond_3

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "connectivity"

    .line 1268
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    if-eqz p1, :cond_2

    .line 1272
    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1276
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_0
    :try_start_1
    const-string p1, "Analytics - Unable to determine connectivity status due to there being no default network currently active"

    .line 1281
    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move v2, v0

    goto :goto_5

    :catch_0
    move-exception p1

    const/4 v2, 0x0

    goto :goto_1

    :catch_1
    move-exception p1

    const/4 v2, 0x0

    goto :goto_2

    :catch_2
    move-exception p1

    const/4 v2, 0x0

    goto :goto_3

    :cond_2
    :try_start_2
    const-string p1, "Analytics - Unable to determine connectivity status due to the system service requested being unrecognized"

    .line 1285
    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_4

    :catch_3
    move-exception p1

    const/4 v2, 0x1

    :goto_1
    const-string v3, "Analytics - Unable to access connectivity status due to an unexpected error (%s)"

    .line 1296
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    invoke-static {v3, v0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :catch_4
    move-exception p1

    const/4 v2, 0x1

    :goto_2
    const-string v3, "Analytics - Unable to access connectivity status due to a security error (%s)"

    .line 1293
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/SecurityException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    invoke-static {v3, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :catch_5
    move-exception p1

    const/4 v2, 0x1

    :goto_3
    const-string v3, "Analytics - Unable to determine connectivity status due to an unexpected error (%s)"

    .line 1290
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    invoke-static {v3, v0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_3
    :goto_4
    const/4 v2, 0x1

    :goto_5
    return v2
.end method

.method protected b(Ljava/io/File;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 855
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 856
    :try_start_1
    invoke-direct {p0, v3}, Lcom/adobe/mobile/ao;->a(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v2, "analytics"

    .line 858
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v2, "poi"

    .line 859
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    .line 860
    invoke-direct {p0, p1}, Lcom/adobe/mobile/ao;->a(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 872
    :cond_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    const-string v2, "Config - Unable to close file stream (%s)"

    .line 876
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    :goto_0
    invoke-static {v2, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :catchall_0
    move-exception p1

    move-object v2, v3

    goto :goto_4

    :catch_1
    move-exception p1

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception p1

    move-object v2, v3

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_4

    :catch_3
    move-exception p1

    :goto_1
    :try_start_3
    const-string v3, "Config - Unable to open points of interest config file, falling back to bundled poi (%s)"

    .line 867
    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v0

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_2

    .line 872
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_3

    :catch_4
    move-exception p1

    const-string v2, "Config - Unable to close file stream (%s)"

    .line 876
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    goto :goto_0

    :catch_5
    move-exception p1

    :goto_2
    :try_start_5
    const-string v3, "Config - Unable to read points of interest remote config file, falling back to bundled poi (%s)"

    .line 864
    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v0

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v2, :cond_2

    .line 872
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_3

    :catch_6
    move-exception p1

    const-string v2, "Config - Unable to close file stream (%s)"

    .line 876
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    goto :goto_0

    :cond_2
    :goto_3
    return-void

    :goto_4
    if-eqz v2, :cond_3

    .line 872
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_5

    :catch_7
    move-exception v2

    .line 876
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const-string v0, "Config - Unable to close file stream (%s)"

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 877
    :cond_3
    :goto_5
    throw p1
.end method

.method protected b()Z
    .locals 3

    .line 445
    sget-object v0, Lcom/adobe/mobile/ao;->H:Ljava/lang/Object;

    monitor-enter v0

    .line 446
    :try_start_0
    iget-object v1, p0, Lcom/adobe/mobile/ao;->G:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 448
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->g()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 449
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 450
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 451
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 448
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/adobe/mobile/ao;->G:Ljava/lang/Boolean;

    .line 453
    iget-object v1, p0, Lcom/adobe/mobile/ao;->G:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Analytics - Your config file is not set up to use Analytics(missing report suite id(s) or tracking server information)"

    .line 454
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    :cond_1
    iget-object v1, p0, Lcom/adobe/mobile/ao;->G:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 458
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected c()Z
    .locals 2

    .line 464
    sget-object v0, Lcom/adobe/mobile/ao;->J:Ljava/lang/Object;

    monitor-enter v0

    .line 465
    :try_start_0
    iget-object v1, p0, Lcom/adobe/mobile/ao;->I:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 466
    invoke-static {}, Lcom/adobe/mobile/be;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/adobe/mobile/ao;->I:Ljava/lang/Boolean;

    .line 468
    :cond_0
    iget-object v1, p0, Lcom/adobe/mobile/ao;->I:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 469
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected d()Z
    .locals 3

    .line 475
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 478
    :cond_0
    sget-object v0, Lcom/adobe/mobile/ao;->L:Ljava/lang/Object;

    monitor-enter v0

    .line 479
    :try_start_0
    iget-object v2, p0, Lcom/adobe/mobile/ao;->K:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 481
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->t()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 482
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 481
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/ao;->K:Ljava/lang/Boolean;

    .line 484
    iget-object v2, p0, Lcom/adobe/mobile/ao;->K:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "Audience Manager - Your config file is not set up to use Audience Manager(missing audience manager server information)"

    .line 485
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    :cond_2
    iget-object v1, p0, Lcom/adobe/mobile/ao;->K:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 490
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected e()Z
    .locals 3

    .line 496
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 499
    :cond_0
    sget-object v0, Lcom/adobe/mobile/ao;->N:Ljava/lang/Object;

    monitor-enter v0

    .line 500
    :try_start_0
    iget-object v2, p0, Lcom/adobe/mobile/ao;->M:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 502
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->r()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 503
    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 502
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/ao;->M:Ljava/lang/Boolean;

    .line 505
    iget-object v2, p0, Lcom/adobe/mobile/ao;->M:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "Target Worker - Your config file is not set up to use Target(missing client code information)"

    .line 506
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 510
    :cond_2
    iget-object v1, p0, Lcom/adobe/mobile/ao;->M:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 511
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected f()Z
    .locals 1

    .line 515
    iget-object v0, p0, Lcom/adobe/mobile/ao;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/ao;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/ao;->v:Ljava/lang/String;

    .line 516
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/ao;->w:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected g()Ljava/lang/String;
    .locals 1

    .line 543
    iget-object v0, p0, Lcom/adobe/mobile/ao;->d:Ljava/lang/String;

    return-object v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .line 547
    iget-object v0, p0, Lcom/adobe/mobile/ao;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .line 551
    iget-object v0, p0, Lcom/adobe/mobile/ao;->f:Ljava/lang/String;

    return-object v0
.end method

.method protected j()Z
    .locals 1

    .line 555
    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->g:Z

    return v0
.end method

.method protected k()Z
    .locals 1

    .line 559
    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->h:Z

    return v0
.end method

.method protected l()Z
    .locals 1

    .line 562
    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->i:Z

    return v0
.end method

.method protected m()I
    .locals 1

    .line 565
    iget v0, p0, Lcom/adobe/mobile/ao;->j:I

    return v0
.end method

.method protected n()I
    .locals 1

    .line 569
    iget v0, p0, Lcom/adobe/mobile/ao;->l:I

    return v0
.end method

.method protected o()Lcom/adobe/mobile/aq;
    .locals 1

    .line 648
    iget-object v0, p0, Lcom/adobe/mobile/ao;->m:Lcom/adobe/mobile/aq;

    return-object v0
.end method

.method protected p()I
    .locals 1

    .line 657
    iget v0, p0, Lcom/adobe/mobile/ao;->k:I

    mul-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method protected q()I
    .locals 1

    .line 661
    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->t:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .line 668
    iget-object v0, p0, Lcom/adobe/mobile/ao;->p:Ljava/lang/String;

    return-object v0
.end method

.method protected s()I
    .locals 1

    .line 672
    iget v0, p0, Lcom/adobe/mobile/ao;->q:I

    return v0
.end method

.method protected t()Ljava/lang/String;
    .locals 1

    .line 683
    iget-object v0, p0, Lcom/adobe/mobile/ao;->s:Ljava/lang/String;

    return-object v0
.end method

.method protected u()Z
    .locals 1

    .line 685
    iget-boolean v0, p0, Lcom/adobe/mobile/ao;->t:Z

    return v0
.end method

.method protected v()I
    .locals 1

    .line 687
    iget v0, p0, Lcom/adobe/mobile/ao;->u:I

    return v0
.end method

.method protected w()V
    .locals 2

    .line 704
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->s()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ao$10;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/ao$10;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 724
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->t()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ao$11;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/ao$11;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 743
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->w()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ao$12;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/ao$12;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 763
    iget-object v0, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/adobe/mobile/ao;->o:Ljava/lang/String;

    new-instance v1, Lcom/adobe/mobile/ao$2;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/ao$2;-><init>(Lcom/adobe/mobile/ao;)V

    invoke-static {v0, v1}, Lcom/adobe/mobile/au;->a(Ljava/lang/String;Lcom/adobe/mobile/au$b;)V

    :cond_0
    return-void
.end method

.method protected x()V
    .locals 2

    .line 813
    iget-object v0, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 814
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    .line 817
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ay;->b()Lcom/adobe/mobile/ak;

    move-result-object v0

    .line 819
    iget-object v1, v0, Lcom/adobe/mobile/ak;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/adobe/mobile/an;->a(Ljava/lang/String;)Lcom/adobe/mobile/r;

    move-result-object v1

    if-nez v1, :cond_1

    .line 821
    iget-object v1, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method protected y()V
    .locals 4

    .line 826
    iget-object v0, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-void

    .line 830
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ay;->b()Lcom/adobe/mobile/ak;

    move-result-object v0

    .line 832
    iget-object v1, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 833
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 834
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adobe/mobile/p;

    .line 835
    iget-object v2, v2, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/adobe/mobile/ak;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 836
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    :cond_2
    return-void
.end method

.method protected z()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/adobe/mobile/p;",
            ">;"
        }
    .end annotation

    .line 882
    iget-object v0, p0, Lcom/adobe/mobile/ao;->y:Ljava/util/ArrayList;

    return-object v0
.end method
