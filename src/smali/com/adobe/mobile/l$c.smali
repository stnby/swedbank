.class public final enum Lcom/adobe/mobile/l$c;
.super Ljava/lang/Enum;
.source "Config.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adobe/mobile/l$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/adobe/mobile/l$c;

.field public static final enum b:Lcom/adobe/mobile/l$c;

.field public static final enum c:Lcom/adobe/mobile/l$c;

.field private static final synthetic e:[Lcom/adobe/mobile/l$c;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 51
    new-instance v0, Lcom/adobe/mobile/l$c;

    const-string v1, "MOBILE_EVENT_LIFECYCLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/adobe/mobile/l$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/l$c;->a:Lcom/adobe/mobile/l$c;

    .line 52
    new-instance v0, Lcom/adobe/mobile/l$c;

    const-string v1, "MOBILE_EVENT_ACQUISITION_INSTALL"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/adobe/mobile/l$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/l$c;->b:Lcom/adobe/mobile/l$c;

    .line 53
    new-instance v0, Lcom/adobe/mobile/l$c;

    const-string v1, "MOBILE_EVENT_ACQUISITION_LAUNCH"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/adobe/mobile/l$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/l$c;->c:Lcom/adobe/mobile/l$c;

    const/4 v0, 0x3

    .line 50
    new-array v0, v0, [Lcom/adobe/mobile/l$c;

    sget-object v1, Lcom/adobe/mobile/l$c;->a:Lcom/adobe/mobile/l$c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/l$c;->b:Lcom/adobe/mobile/l$c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adobe/mobile/l$c;->c:Lcom/adobe/mobile/l$c;

    aput-object v1, v0, v4

    sput-object v0, Lcom/adobe/mobile/l$c;->e:[Lcom/adobe/mobile/l$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/adobe/mobile/l$c;->d:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/l$c;
    .locals 1

    .line 50
    const-class v0, Lcom/adobe/mobile/l$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adobe/mobile/l$c;

    return-object p0
.end method

.method public static values()[Lcom/adobe/mobile/l$c;
    .locals 1

    .line 50
    sget-object v0, Lcom/adobe/mobile/l$c;->e:[Lcom/adobe/mobile/l$c;

    invoke-virtual {v0}, [Lcom/adobe/mobile/l$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/l$c;

    return-object v0
.end method
