.class final Lcom/adobe/mobile/az;
.super Ljava/lang/Object;
.source "TargetWorker.java"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Ljava/lang/String;

.field private static g:Ljava/lang/String;

.field private static h:Ljava/lang/String;

.field private static i:Ljava/lang/String;

.field private static j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:Ljava/lang/Object;

.field private static final l:Ljava/lang/Object;

.field private static final m:Ljava/lang/Object;

.field private static final n:Ljava/lang/Object;

.field private static o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const-string v0, "mbox"

    const-string v1, "parameters"

    const-string v2, "product"

    const-string v3, "order"

    const-string v4, "content"

    const-string v5, "eventTokens"

    const-string v6, "clientSideAnalyticsLoggingPayload"

    const-string v7, "errorType"

    const-string v8, "profileScriptToken"

    const-string v9, "clickToken"

    .line 60
    filled-new-array/range {v0 .. v9}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/az;->a:Ljava/util/List;

    const-string v0, "mbox"

    const-string v1, "profileScriptToken"

    const-string v2, "clickToken"

    .line 64
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/az;->b:Ljava/util/List;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/adobe/mobile/az;->c:Ljava/util/Map;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/adobe/mobile/az;->d:Ljava/util/List;

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/adobe/mobile/az;->e:Ljava/util/Map;

    const/4 v0, 0x0

    .line 107
    sput-object v0, Lcom/adobe/mobile/az;->f:Ljava/lang/String;

    .line 108
    sput-object v0, Lcom/adobe/mobile/az;->g:Ljava/lang/String;

    .line 109
    sput-object v0, Lcom/adobe/mobile/az;->h:Ljava/lang/String;

    .line 110
    sput-object v0, Lcom/adobe/mobile/az;->i:Ljava/lang/String;

    .line 111
    sput-object v0, Lcom/adobe/mobile/az;->j:Ljava/util/HashMap;

    .line 116
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/az;->k:Ljava/lang/Object;

    .line 117
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/az;->l:Ljava/lang/Object;

    .line 118
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/az;->m:Ljava/lang/Object;

    .line 119
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/az;->n:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 1122
    sput-boolean v0, Lcom/adobe/mobile/az;->o:Z

    return-void
.end method

.method protected static a()Ljava/lang/String;
    .locals 4

    .line 343
    sget-object v0, Lcom/adobe/mobile/az;->l:Ljava/lang/Object;

    monitor-enter v0

    .line 344
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/az;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/adobe/mobile/az;->d(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 346
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ADBMOBILE_TARGET_3RD_PARTY_ID"

    .line 347
    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/adobe/mobile/az;->h:Ljava/lang/String;
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    const-string v2, "Target - Error retrieving shared preferences - application context is null"

    const/4 v3, 0x0

    .line 350
    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    monitor-exit v0

    return-object v1

    .line 355
    :cond_0
    :goto_0
    sget-object v1, Lcom/adobe/mobile/az;->h:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 356
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static a(Ljava/lang/String;)V
    .locals 3

    .line 365
    sget-object v0, Lcom/adobe/mobile/az;->l:Ljava/lang/Object;

    monitor-enter v0

    if-eqz p0, :cond_0

    .line 366
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v1

    sget-object v2, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    if-ne v1, v2, :cond_0

    .line 368
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    goto :goto_2

    .line 371
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/az;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz p0, :cond_1

    .line 372
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 373
    monitor-exit v0

    return-void

    .line 376
    :cond_1
    sput-object p0, Lcom/adobe/mobile/az;->h:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    .line 380
    sget-object v1, Lcom/adobe/mobile/az;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/adobe/mobile/az;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "ADBMOBILE_TARGET_3RD_PARTY_ID"

    .line 381
    invoke-interface {p0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_2
    const-string v1, "ADBMOBILE_TARGET_3RD_PARTY_ID"

    .line 384
    sget-object v2, Lcom/adobe/mobile/az;->h:Ljava/lang/String;

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 386
    :goto_0
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    :try_start_2
    const-string p0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    .line 389
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    :goto_1
    monitor-exit v0

    return-void

    :goto_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_2

    .line 1189
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v0

    :cond_2
    const/16 v0, 0x2e

    .line 1193
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    goto :goto_0

    .line 1194
    :cond_3
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 1196
    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v3, :cond_4

    goto :goto_1

    .line 1197
    :cond_4
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 1199
    :goto_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0

    :cond_5
    :goto_2
    return v1
.end method

.method protected static b()V
    .locals 2

    .line 1124
    sget-object v0, Lcom/adobe/mobile/az;->n:Ljava/lang/Object;

    monitor-enter v0

    .line 1125
    :try_start_0
    sget-boolean v1, Lcom/adobe/mobile/az;->o:Z

    if-eqz v1, :cond_0

    .line 1126
    monitor-exit v0

    return-void

    :cond_0
    const-string v1, "mboxPC"

    .line 1130
    invoke-static {v1}, Lcom/adobe/mobile/az;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1132
    invoke-static {v1}, Lcom/adobe/mobile/az;->b(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x1

    .line 1135
    sput-boolean v1, Lcom/adobe/mobile/az;->o:Z

    .line 1136
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static b(Ljava/lang/String;)V
    .locals 3

    .line 425
    sget-object v0, Lcom/adobe/mobile/az;->k:Ljava/lang/Object;

    monitor-enter v0

    .line 427
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/az;->g:Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/adobe/mobile/az;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    monitor-exit v0

    return-void

    .line 431
    :cond_0
    sput-object p0, Lcom/adobe/mobile/az;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    .line 435
    sget-object v1, Lcom/adobe/mobile/az;->g:Ljava/lang/String;

    invoke-static {v1}, Lcom/adobe/mobile/az;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ADBMOBILE_TARGET_TNT_ID"

    .line 436
    invoke-interface {p0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_1
    const-string v1, "ADBMOBILE_TARGET_TNT_ID"

    .line 439
    sget-object v2, Lcom/adobe/mobile/az;->g:Ljava/lang/String;

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 441
    :goto_0
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    :try_start_2
    const-string p0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    .line 444
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    .line 1145
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_1

    .line 1152
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1153
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_Expires"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1155
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_Expires"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 1157
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_Value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1158
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    move-object v0, v1

    .line 1164
    :cond_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1165
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_Value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1166
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "_Expires"

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1167
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    .line 1171
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    :goto_1
    return-object v0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 0

    if-eqz p0, :cond_1

    .line 1358
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
