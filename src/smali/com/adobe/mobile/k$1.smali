.class final Lcom/adobe/mobile/k$1;
.super Ljava/lang/Object;
.source "AudienceManagerWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/k;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 160
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->t()Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-static {}, Lcom/adobe/mobile/k;->d()Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/bd;->d()Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v3

    invoke-virtual {v3}, Lcom/adobe/mobile/ao;->B()Ljava/lang/String;

    move-result-object v3

    .line 168
    invoke-static {v1}, Lcom/adobe/mobile/StaticMethods;->e(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x0

    if-nez v4, :cond_0

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->e(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v2, "https://%s/demoptout.jpg?d_uuid=%s"

    .line 170
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v7

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 171
    :cond_0
    invoke-static {v2}, Lcom/adobe/mobile/StaticMethods;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v3}, Lcom/adobe/mobile/StaticMethods;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "https://%s/demoptout.jpg?d_mid=%s&d_orgid=%s"

    const/4 v1, 0x3

    .line 173
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adobe/mobile/bd;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v7

    aput-object v2, v1, v5

    aput-object v3, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "Audience Manager - Opting user out of server-side segments."

    .line 179
    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    const/16 v2, 0x1388

    const-string v3, "Audience Manager"

    .line 180
    invoke-static {v0, v1, v2, v3}, Lcom/adobe/mobile/aw;->b(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)V

    return-void

    :cond_1
    return-void
.end method
