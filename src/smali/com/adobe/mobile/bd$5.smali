.class Lcom/adobe/mobile/bd$5;
.super Ljava/lang/Object;
.source "VisitorIDService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/bd;->i()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/bd;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/bd;)V
    .locals 0

    .line 971
    iput-object p1, p0, Lcom/adobe/mobile/bd$5;->a:Lcom/adobe/mobile/bd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 975
    iget-object v0, p0, Lcom/adobe/mobile/bd$5;->a:Lcom/adobe/mobile/bd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 976
    iget-object v0, p0, Lcom/adobe/mobile/bd$5;->a:Lcom/adobe/mobile/bd;

    invoke-static {v0, v1}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;Ljava/util/List;)Ljava/util/List;

    .line 977
    iget-object v0, p0, Lcom/adobe/mobile/bd$5;->a:Lcom/adobe/mobile/bd;

    invoke-static {v0, v1}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 978
    iget-object v0, p0, Lcom/adobe/mobile/bd$5;->a:Lcom/adobe/mobile/bd;

    invoke-static {v0, v1}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 979
    iget-object v0, p0, Lcom/adobe/mobile/bd$5;->a:Lcom/adobe/mobile/bd;

    invoke-static {v0, v1}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 980
    iget-object v0, p0, Lcom/adobe/mobile/bd$5;->a:Lcom/adobe/mobile/bd;

    invoke-static {v0, v1}, Lcom/adobe/mobile/bd;->d(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    .line 984
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ADBMOBILE_VISITORID_IDS"

    .line 985
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "ADBMOBILE_PERSISTED_MID"

    .line 986
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "ADBMOBILE_PERSISTED_MID_HINT"

    .line 987
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "ADBMOBILE_PERSISTED_MID_BLOB"

    .line 988
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 989
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "ID Service - Unable to purge identities (application context is null)"

    const/4 v1, 0x0

    .line 991
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
