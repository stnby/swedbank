.class Lcom/adobe/mobile/ak$b;
.super Lcom/adobe/mobile/r$a;
.source "MessageTargetExperienceUIFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/ak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# direct methods
.method protected constructor <init>(Lcom/adobe/mobile/r;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/adobe/mobile/r$a;-><init>(Lcom/adobe/mobile/r;)V

    return-void
.end method


# virtual methods
.method protected a()Landroid/webkit/WebView;
    .locals 4

    .line 53
    invoke-super {p0}, Lcom/adobe/mobile/r$a;->a()Landroid/webkit/WebView;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    .line 57
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 58
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 60
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setDatabasePath(Ljava/lang/String;)V

    .line 61
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    :cond_0
    return-object v0
.end method
