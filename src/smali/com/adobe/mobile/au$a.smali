.class Lcom/adobe/mobile/au$a;
.super Ljava/lang/Object;
.source "RemoteDownload.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/au;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/adobe/mobile/au$b;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/adobe/mobile/au$b;IILjava/lang/String;)V
    .locals 0

    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    iput-object p1, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    .line 332
    iput-object p2, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    .line 333
    iput p3, p0, Lcom/adobe/mobile/au$a;->c:I

    .line 334
    iput p4, p0, Lcom/adobe/mobile/au$a;->d:I

    .line 335
    iput-object p5, p0, Lcom/adobe/mobile/au$a;->e:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/adobe/mobile/au$b;IILjava/lang/String;Lcom/adobe/mobile/au$1;)V
    .locals 0

    .line 320
    invoke-direct/range {p0 .. p5}, Lcom/adobe/mobile/au$a;-><init>(Ljava/lang/String;Lcom/adobe/mobile/au$b;IILjava/lang/String;)V

    return-void
.end method

.method protected static a(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 3

    .line 488
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 489
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p0

    check-cast p0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string v0, "Cached Files - Exception opening URL(%s)"

    const/4 v1, 0x1

    .line 491
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 340
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez v0, :cond_1

    const-string v0, "Cached Files - url is null and cannot be cached"

    .line 341
    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V

    :cond_0
    return-void

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/au;->a(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x1

    if-nez v0, :cond_3

    const-string v0, "Cached Files - given url is not valid and cannot be cached (\"%s\")"

    .line 350
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V

    :cond_2
    return-void

    .line 357
    :cond_3
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/adobe/mobile/au$a;->e:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/adobe/mobile/au;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 359
    invoke-static {}, Lcom/adobe/mobile/au;->a()Ljava/text/SimpleDateFormat;

    move-result-object v4

    .line 360
    iget-object v5, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    invoke-static {v5}, Lcom/adobe/mobile/au$a;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v5

    if-nez v5, :cond_5

    .line 366
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_4

    .line 367
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V

    :cond_4
    return-void

    .line 372
    :cond_5
    iget v6, p0, Lcom/adobe/mobile/au$a;->c:I

    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 373
    iget v6, p0, Lcom/adobe/mobile/au$a;->d:I

    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    if-eqz v0, :cond_7

    .line 376
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/adobe/mobile/au;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/adobe/mobile/StaticMethods;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 377
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/adobe/mobile/au;->f(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 379
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_6

    const-string v8, "If-Modified-Since"

    .line 380
    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v8, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    if-eqz v6, :cond_7

    const-string v4, "If-None-Match"

    .line 383
    invoke-virtual {v5, v4, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_7
    :try_start_0
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 389
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    const/16 v6, 0x130

    if-ne v4, v6, :cond_9

    const-string v4, "Cached Files - File has not been modified since last download. (%s)"

    .line 390
    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-static {v4, v6}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    iget-object v4, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v4, :cond_8

    .line 392
    iget-object v4, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v4, v2, v0}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_12
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_e
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 477
    :cond_8
    :try_start_1
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 396
    :cond_9
    :try_start_2
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    const/16 v6, 0x194

    if-ne v4, v6, :cond_b

    const-string v4, "Cached Files - File not found. (%s)"

    .line 397
    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-static {v4, v6}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 398
    iget-object v4, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v4, :cond_a

    .line 399
    iget-object v4, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v4, v2, v0}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_12
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_e
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 477
    :cond_a
    :try_start_3
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 403
    :cond_b
    :try_start_4
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    const/16 v6, 0xc8

    if-eq v4, v6, :cond_d

    const-string v4, "Cached Files - File could not be downloaded from URL (%s) Response: (%d) Message: (%s)"

    const/4 v6, 0x3

    .line 404
    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x2

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v6}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 405
    iget-object v4, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v4, :cond_c

    .line 406
    iget-object v4, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v4, v2, v0}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_12
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_10
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_e
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_c
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 477
    :cond_c
    :try_start_5
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    return-void

    :cond_d
    if-eqz v0, :cond_e

    .line 412
    :try_start_6
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/adobe/mobile/au$a;->e:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/adobe/mobile/au;->b(Ljava/lang/String;Ljava/lang/String;)Z

    .line 415
    :cond_e
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getLastModified()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    const-string v4, "ETag"

    .line 416
    invoke-virtual {v5, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_f

    .line 419
    invoke-static {v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 423
    :cond_f
    iget-object v6, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    iget-object v7, p0, Lcom/adobe/mobile/au$a;->e:Ljava/lang/String;

    invoke-static {v6, v0, v4, v7}, Lcom/adobe/mobile/au;->a(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_11

    .line 426
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_10

    .line 427
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_12
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_10
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_e
    .catch Ljava/lang/Error; {:try_start_6 .. :try_end_6} :catch_c
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 477
    :cond_10
    :try_start_7
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_3

    :catch_3
    move-exception v0

    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    return-void

    .line 432
    :cond_11
    :try_start_8
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_12
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_10
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_e
    .catch Ljava/lang/Error; {:try_start_8 .. :try_end_8} :catch_c
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 433
    :try_start_9
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_b
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/lang/Error; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/16 v7, 0x1000

    .line 435
    :try_start_a
    new-array v7, v7, [B

    .line 438
    :goto_4
    invoke-virtual {v4, v7}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_12

    .line 439
    invoke-virtual {v6, v7, v2, v8}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_4

    :cond_12
    const-string v7, "Cached Files - Caching successful (%s)"

    .line 442
    new-array v8, v3, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 444
    iget-object v7, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v7, :cond_13

    .line 445
    iget-object v7, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v7, v3, v0}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_a
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_13
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/lang/Error; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 472
    :cond_13
    :try_start_b
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    if-eqz v4, :cond_14

    .line 475
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_14
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_12

    :catch_4
    move-exception v0

    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    goto/16 :goto_11

    :catch_5
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_8

    :catch_7
    move-exception v0

    goto/16 :goto_b

    :catchall_0
    move-exception v0

    goto/16 :goto_13

    :catch_8
    move-exception v0

    move-object v6, v1

    goto :goto_5

    :catch_9
    move-exception v0

    move-object v6, v1

    goto :goto_8

    :catch_a
    move-exception v0

    move-object v6, v1

    goto/16 :goto_b

    :catch_b
    move-object v6, v1

    goto/16 :goto_e

    :catchall_1
    move-exception v0

    move-object v4, v1

    goto/16 :goto_13

    :catch_c
    move-exception v0

    move-object v4, v1

    move-object v6, v4

    :goto_5
    :try_start_c
    const-string v7, "Cached Files - Unexpected error while attempting to get remote file (%s)"

    .line 463
    new-array v8, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    invoke-static {v7, v8}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_15

    .line 465
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :cond_15
    if-eqz v6, :cond_16

    .line 472
    :try_start_d
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    goto :goto_6

    :catch_d
    move-exception v0

    goto :goto_7

    :cond_16
    :goto_6
    if-eqz v4, :cond_17

    .line 475
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_17
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_d

    goto/16 :goto_12

    :goto_7
    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    goto/16 :goto_11

    :catch_e
    move-exception v0

    move-object v4, v1

    move-object v6, v4

    :goto_8
    :try_start_e
    const-string v7, "Cached Files - Unexpected exception while attempting to get remote file (%s)"

    .line 458
    new-array v8, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    invoke-static {v7, v8}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_18

    .line 460
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :cond_18
    if-eqz v6, :cond_19

    .line 472
    :try_start_f
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    goto :goto_9

    :catch_f
    move-exception v0

    goto :goto_a

    :cond_19
    :goto_9
    if-eqz v4, :cond_1a

    .line 475
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_1a
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_f

    goto/16 :goto_12

    :goto_a
    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    goto/16 :goto_11

    :catch_10
    move-exception v0

    move-object v4, v1

    move-object v6, v4

    :goto_b
    :try_start_10
    const-string v7, "Cached Files - IOException while making request (%s)"

    .line 453
    new-array v8, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    invoke-static {v7, v8}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 454
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_1b

    .line 455
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :cond_1b
    if-eqz v6, :cond_1c

    .line 472
    :try_start_11
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    goto :goto_c

    :catch_11
    move-exception v0

    goto :goto_d

    :cond_1c
    :goto_c
    if-eqz v4, :cond_1d

    .line 475
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_1d
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_11

    goto :goto_12

    :goto_d
    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    goto :goto_11

    :catch_12
    move-object v4, v1

    move-object v6, v4

    :catch_13
    :goto_e
    :try_start_12
    const-string v0, "Cached Files - Timed out making request (%s)"

    .line 448
    new-array v7, v3, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/adobe/mobile/au$a;->b:Ljava/lang/String;

    aput-object v8, v7, v2

    invoke-static {v0, v7}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    if-eqz v0, :cond_1e

    .line 450
    iget-object v0, p0, Lcom/adobe/mobile/au$a;->a:Lcom/adobe/mobile/au$b;

    invoke-interface {v0, v2, v1}, Lcom/adobe/mobile/au$b;->a(ZLjava/io/File;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    :cond_1e
    if-eqz v6, :cond_1f

    .line 472
    :try_start_13
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    goto :goto_f

    :catch_14
    move-exception v0

    goto :goto_10

    :cond_1f
    :goto_f
    if-eqz v4, :cond_20

    .line 475
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_20
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_14

    goto :goto_12

    :goto_10
    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    .line 480
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    :goto_11
    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_12
    return-void

    :catchall_2
    move-exception v0

    move-object v1, v6

    :goto_13
    if-eqz v1, :cond_21

    .line 472
    :try_start_14
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    goto :goto_14

    :catch_15
    move-exception v1

    goto :goto_15

    :cond_21
    :goto_14
    if-eqz v4, :cond_22

    .line 475
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_22
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_15

    goto :goto_16

    .line 480
    :goto_15
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    const-string v1, "Cached Files - Exception while attempting to close streams (%s)"

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 481
    :goto_16
    throw v0
.end method
