.class final Lcom/adobe/mobile/h;
.super Lcom/adobe/mobile/AbstractDatabaseBacking;
.source "AnalyticsTrackTimedAction.java"


# static fields
.field private static q:Lcom/adobe/mobile/h;

.field private static final r:Ljava/lang/Object;


# instance fields
.field private f:Landroid/database/sqlite/SQLiteStatement;

.field private g:Landroid/database/sqlite/SQLiteStatement;

.field private h:Landroid/database/sqlite/SQLiteStatement;

.field private i:Ljava/lang/String;

.field private j:Landroid/database/sqlite/SQLiteStatement;

.field private k:Ljava/lang/String;

.field private l:Landroid/database/sqlite/SQLiteStatement;

.field private m:Landroid/database/sqlite/SQLiteStatement;

.field private n:Ljava/lang/String;

.field private o:Landroid/database/sqlite/SQLiteStatement;

.field private p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/h;->r:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 65
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;-><init>()V

    const-string v0, "ADBMobileTimedActionsCache.sqlite"

    .line 66
    iput-object v0, p0, Lcom/adobe/mobile/h;->d:Ljava/lang/String;

    const-string v0, "Analytics"

    .line 67
    iput-object v0, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/adobe/mobile/h;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/h;->a(Ljava/io/File;)V

    return-void
.end method

.method public static g()Lcom/adobe/mobile/h;
    .locals 2

    .line 56
    sget-object v0, Lcom/adobe/mobile/h;->r:Ljava/lang/Object;

    monitor-enter v0

    .line 57
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/h;->q:Lcom/adobe/mobile/h;

    if-nez v1, :cond_0

    .line 58
    new-instance v1, Lcom/adobe/mobile/h;

    invoke-direct {v1}, Lcom/adobe/mobile/h;-><init>()V

    sput-object v1, Lcom/adobe/mobile/h;->q:Lcom/adobe/mobile/h;

    .line 61
    :cond_0
    sget-object v1, Lcom/adobe/mobile/h;->q:Lcom/adobe/mobile/h;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 62
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method protected a()V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 292
    :try_start_0
    iget-object v3, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "CREATE TABLE IF NOT EXISTS TIMEDACTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, STARTTIME INTEGER, ADJSTARTTIME INTEGER)"

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 293
    iget-object v3, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "CREATE TABLE IF NOT EXISTS CONTEXTDATA (ID INTEGER PRIMARY KEY AUTOINCREMENT, ACTIONID INTEGER, KEY TEXT, VALUE TEXT, FOREIGN KEY(ACTIONID) REFERENCES TIMEDACTIONS(ID))"

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v4, "%s - Uknown error creating timed actions database (%s)"

    .line 299
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v3

    const-string v4, "%s - Unable to open or create timed actions database (%s)"

    .line 296
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected a(J)V
    .locals 5

    .line 183
    iget-object v0, p0, Lcom/adobe/mobile/h;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 185
    :try_start_0
    iget-object v4, p0, Lcom/adobe/mobile/h;->g:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4, v3, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 186
    iget-object p1, p0, Lcom/adobe/mobile/h;->g:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 187
    iget-object p1, p0, Lcom/adobe/mobile/h;->g:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    const-string p2, "%s - Unable to adjust start times for timed actions (%s)"

    .line 196
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v4, v2, v1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {p2, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p1

    const-string p2, "%s - Unable to bind prepared statement values for updating the adjusted start time for timed action (%s)"

    .line 190
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v4, v2, v1

    invoke-virtual {p1}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {p2, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/h;->a(Ljava/lang/Exception;)V

    .line 198
    :goto_0
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected b()V
    .locals 4

    const-string v0, "SELECT ID, STARTTIME, ADJSTARTTIME FROM TIMEDACTIONS WHERE NAME=?"

    .line 306
    iput-object v0, p0, Lcom/adobe/mobile/h;->i:Ljava/lang/String;

    const-string v0, "SELECT COUNT(*) FROM TIMEDACTIONS WHERE NAME=?"

    .line 307
    iput-object v0, p0, Lcom/adobe/mobile/h;->k:Ljava/lang/String;

    const-string v0, "SELECT KEY, VALUE FROM CONTEXTDATA WHERE ACTIONID=?"

    .line 308
    iput-object v0, p0, Lcom/adobe/mobile/h;->n:Ljava/lang/String;

    const-string v0, "SELECT COUNT(*) FROM CONTEXTDATA WHERE ACTIONID=? AND KEY=?"

    .line 309
    iput-object v0, p0, Lcom/adobe/mobile/h;->p:Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 314
    :try_start_0
    iget-object v2, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "INSERT INTO TIMEDACTIONS (NAME, STARTTIME, ADJSTARTTIME) VALUES (@NAME, @START, @START)"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/h;->f:Landroid/database/sqlite/SQLiteStatement;

    .line 315
    iget-object v2, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "UPDATE TIMEDACTIONS SET ADJSTARTTIME=ADJSTARTTIME+@DELTA WHERE ADJSTARTTIME!=-1"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/h;->g:Landroid/database/sqlite/SQLiteStatement;

    .line 316
    iget-object v2, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "UPDATE TIMEDACTIONS SET ADJSTARTTIME=-1"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/h;->h:Landroid/database/sqlite/SQLiteStatement;

    .line 317
    iget-object v2, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "DELETE FROM TIMEDACTIONS WHERE ID=@ID"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/h;->j:Landroid/database/sqlite/SQLiteStatement;

    .line 318
    iget-object v2, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "INSERT INTO CONTEXTDATA(ACTIONID, KEY, VALUE) VALUES (@ACTIONID, @KEY, @VALUE)"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/h;->l:Landroid/database/sqlite/SQLiteStatement;

    .line 319
    iget-object v2, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "UPDATE CONTEXTDATA SET VALUE=@VALUE WHERE ACTIONID=@ACTIONID AND KEY=@KEY"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/h;->m:Landroid/database/sqlite/SQLiteStatement;

    .line 320
    iget-object v2, p0, Lcom/adobe/mobile/h;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "DELETE FROM CONTEXTDATA WHERE ACTIONID=@ACTIONID"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/h;->o:Landroid/database/sqlite/SQLiteStatement;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Analytics - Unknown error preparing sql statements (%s)"

    .line 326
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v3, "Analytics - unable to prepare the needed SQL statements for interacting with the timed actions database (%s)"

    .line 323
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 6

    .line 268
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "ADBMobileDataCache.sqlite"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adobe/mobile/h;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 269
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/adobe/mobile/h;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 272
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 274
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "%s - Unable to migrate old Timed Actions db, creating new Timed Actions db (move file returned false)"

    .line 275
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "%s - Unable to migrate old Timed Actions db, creating new Timed Actions db (%s)"

    const/4 v4, 0x2

    .line 278
    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v5, v4, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v1, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected h()V
    .locals 7

    .line 202
    iget-object v0, p0, Lcom/adobe/mobile/h;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 204
    :try_start_0
    iget-object v4, p0, Lcom/adobe/mobile/h;->h:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 205
    iget-object v4, p0, Lcom/adobe/mobile/h;->h:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v4

    :try_start_1
    const-string v5, "%s - Unknown error clearing adjusted start times for timed actions (%s)"

    .line 214
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 217
    invoke-virtual {p0, v4}, Lcom/adobe/mobile/h;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v4

    const-string v5, "%s - Unable to update adjusted time for timed actions after crash (%s)"

    .line 208
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/h;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    invoke-virtual {p0, v4}, Lcom/adobe/mobile/h;->a(Ljava/lang/Exception;)V

    .line 219
    :goto_0
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
