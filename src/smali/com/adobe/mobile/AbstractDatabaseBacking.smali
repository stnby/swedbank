.class abstract Lcom/adobe/mobile/AbstractDatabaseBacking;
.super Ljava/lang/Object;
.source "AbstractDatabaseBacking.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;,
        Lcom/adobe/mobile/AbstractDatabaseBacking$a;
    }
.end annotation


# instance fields
.field protected a:Landroid/database/sqlite/SQLiteDatabase;

.field protected b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

.field protected final c:Ljava/lang/Object;

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field private f:Ljava/io/File;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->f:Ljava/io/File;

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->c:Ljava/lang/Object;

    return-void
.end method

.method private g()V
    .locals 5

    .line 57
    :try_start_0
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->f()V

    const v0, 0x10000010

    .line 59
    iget-object v1, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 60
    sget-object v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->a:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    iput-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 62
    sget-object v1, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    iput-object v1, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    const-string v1, "%s - Unable to open database (%s)."

    const/4 v2, 0x2

    .line 63
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .line 34
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "initializeDatabase must be overwritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/io/File;)V
    .locals 1

    .line 77
    iput-object p1, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->f:Ljava/io/File;

    .line 79
    iget-object p1, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->c:Ljava/lang/Object;

    monitor-enter p1

    .line 80
    :try_start_0
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->d()V

    .line 82
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->g()V

    .line 84
    iget-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->a:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->e()V

    .line 86
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->a()V

    .line 87
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->b()V

    .line 89
    :cond_0
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final a(Ljava/lang/Exception;)V
    .locals 5

    const-string v0, "%s - Database in unrecoverable state (%s), resetting."

    const/4 v1, 0x2

    .line 94
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->e:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    iget-object p1, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->c:Ljava/lang/Object;

    monitor-enter p1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "%s - Failed to delete database file(%s)."

    .line 100
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->e:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->f:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    sget-object v0, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    iput-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    .line 102
    monitor-exit p1

    return-void

    :cond_0
    const-string v0, "%s - Database file(%s) was corrupt and had to be deleted."

    .line 105
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->e:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->f:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->g()V

    .line 109
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->a()V

    .line 110
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->b()V

    .line 112
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;->c()V

    .line 113
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected b()V
    .locals 2

    .line 38
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "prepareStatements must be overwritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected f()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->a:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/adobe/mobile/AbstractDatabaseBacking;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_0
    return-void
.end method
