.class Lcom/adobe/mobile/ao$3;
.super Ljava/lang/Object;
.source "MobileConfig.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/ao;->K()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/ao;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/ao;)V
    .locals 0

    .line 1175
    iput-object p1, p0, Lcom/adobe/mobile/ao$3;->a:Lcom/adobe/mobile/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 1179
    iget-object v0, p0, Lcom/adobe/mobile/ao$3;->a:Lcom/adobe/mobile/ao;

    invoke-static {v0}, Lcom/adobe/mobile/ao;->d(Lcom/adobe/mobile/ao;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/adobe/mobile/ao$3;->a:Lcom/adobe/mobile/ao;

    invoke-static {v0}, Lcom/adobe/mobile/ao;->d(Lcom/adobe/mobile/ao;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto/16 :goto_4

    .line 1184
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1187
    iget-object v1, p0, Lcom/adobe/mobile/ao$3;->a:Lcom/adobe/mobile/ao;

    invoke-static {v1}, Lcom/adobe/mobile/ao;->d(Lcom/adobe/mobile/ao;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adobe/mobile/p;

    .line 1189
    iget-object v3, v2, Lcom/adobe/mobile/p;->h:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/adobe/mobile/p;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_2

    goto :goto_0

    .line 1194
    :cond_2
    iget-object v2, v2, Lcom/adobe/mobile/p;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 1195
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_4

    goto :goto_1

    .line 1200
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1201
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x0

    const-string v6, "messageImages"

    const/16 v7, 0x2710

    .line 1202
    invoke-static {v4, v7, v7, v5, v6}, Lcom/adobe/mobile/au;->b(Ljava/lang/String;IILcom/adobe/mobile/au$b;Ljava/lang/String;)V

    goto :goto_2

    .line 1208
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    const-string v1, "messageImages"

    .line 1209
    invoke-static {v1, v0}, Lcom/adobe/mobile/au;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_3

    :cond_6
    const-string v0, "messageImages"

    .line 1212
    invoke-static {v0}, Lcom/adobe/mobile/au;->c(Ljava/lang/String;)V

    :goto_3
    return-void

    :cond_7
    :goto_4
    const-string v0, "messageImages"

    .line 1180
    invoke-static {v0}, Lcom/adobe/mobile/au;->c(Ljava/lang/String;)V

    return-void
.end method
