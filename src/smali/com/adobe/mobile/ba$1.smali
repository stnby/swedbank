.class Lcom/adobe/mobile/ba$1;
.super Ljava/lang/Object;
.source "ThirdPartyQueue.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/ba;->h()Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/ba;

.field final synthetic b:Lcom/adobe/mobile/ba;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/ba;Lcom/adobe/mobile/ba;)V
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/adobe/mobile/ba$1;->b:Lcom/adobe/mobile/ba;

    iput-object p2, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/16 v0, 0xa

    .line 222
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 224
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->k()Z

    move-result v0

    .line 227
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Accept-Language"

    .line 229
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->C()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "User-Agent"

    .line 230
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :cond_0
    :goto_0
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v1

    sget-object v2, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    const/4 v8, 0x0

    if-ne v1, v2, :cond_8

    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->G()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 235
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    invoke-virtual {v1}, Lcom/adobe/mobile/ba;->g()Lcom/adobe/mobile/a$a;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 238
    iget-object v1, v9, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    if-nez v1, :cond_1

    goto/16 :goto_5

    :cond_1
    if-nez v0, :cond_2

    .line 243
    iget-wide v1, v9, Lcom/adobe/mobile/a$a;->c:J

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v3

    const-wide/16 v5, 0x3c

    sub-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    .line 245
    :try_start_0
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    iget-object v2, v9, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/adobe/mobile/ba;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 248
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    invoke-virtual {v1, v0}, Lcom/adobe/mobile/ba;->a(Ljava/lang/Exception;)V

    goto/16 :goto_5

    .line 255
    :cond_2
    iget-object v1, v9, Lcom/adobe/mobile/a$a;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v9, Lcom/adobe/mobile/a$a;->d:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string v1, ""

    :goto_1
    iput-object v1, v9, Lcom/adobe/mobile/a$a;->d:Ljava/lang/String;

    .line 256
    iget-object v1, v9, Lcom/adobe/mobile/a$a;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, v9, Lcom/adobe/mobile/a$a;->e:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const-string v1, ""

    :goto_2
    iput-object v1, v9, Lcom/adobe/mobile/a$a;->e:Ljava/lang/String;

    .line 259
    iget v1, v9, Lcom/adobe/mobile/a$a;->f:I

    const/4 v10, 0x2

    if-ge v1, v10, :cond_5

    const/16 v1, 0x7d0

    goto :goto_3

    :cond_5
    iget v1, v9, Lcom/adobe/mobile/a$a;->f:I

    mul-int/lit16 v1, v1, 0x3e8

    :goto_3
    iput v1, v9, Lcom/adobe/mobile/a$a;->f:I

    .line 264
    iget-object v1, v9, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    iget-object v2, v9, Lcom/adobe/mobile/a$a;->d:Ljava/lang/String;

    iget v4, v9, Lcom/adobe/mobile/a$a;->f:I

    iget-object v5, v9, Lcom/adobe/mobile/a$a;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/adobe/mobile/ba$1;->b:Lcom/adobe/mobile/ba;

    iget-object v6, v3, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    move-object v3, v7

    invoke-static/range {v1 .. v6}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 268
    :try_start_1
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    iget-object v2, v9, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/adobe/mobile/ba;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_1 .. :try_end_1} :catch_1

    .line 275
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    iget-wide v2, v9, Lcom/adobe/mobile/a$a;->c:J

    iput-wide v2, v1, Lcom/adobe/mobile/ba;->g:J

    goto/16 :goto_0

    :catch_1
    move-exception v0

    .line 271
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    invoke-virtual {v1, v0}, Lcom/adobe/mobile/ba;->a(Ljava/lang/Exception;)V

    goto :goto_5

    :cond_6
    const-string v1, "%s - Unable to forward hit (%s)"

    .line 281
    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/ba$1;->b:Lcom/adobe/mobile/ba;

    iget-object v3, v3, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v3, v2, v8

    iget-object v3, v9, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->k()Z

    move-result v1

    if-eqz v1, :cond_7

    const-wide/16 v1, 0x1e

    const-string v3, "%s - Network error, imposing internal cooldown (%d seconds)"

    .line 287
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/ba$1;->b:Lcom/adobe/mobile/ba;

    iget-object v6, v6, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, 0x0

    :goto_4
    int-to-long v5, v3

    cmp-long v5, v5, v1

    if-gez v5, :cond_0

    .line 305
    :try_start_2
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v5

    invoke-virtual {v5}, Lcom/adobe/mobile/ao;->G()Z

    move-result v5

    if-eqz v5, :cond_0

    const-wide/16 v5, 0x3e8

    .line 306
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :catch_2
    move-exception v1

    const-string v2, "%s - Background Thread Interrupted (%s)"

    .line 310
    new-array v3, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/ba$1;->b:Lcom/adobe/mobile/ba;

    iget-object v5, v5, Lcom/adobe/mobile/ba;->e:Ljava/lang/String;

    aput-object v5, v3, v8

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 292
    :cond_7
    :try_start_3
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    iget-object v2, v9, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/adobe/mobile/ba;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v0

    .line 295
    iget-object v1, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    invoke-virtual {v1, v0}, Lcom/adobe/mobile/ba;->a(Ljava/lang/Exception;)V

    .line 315
    :cond_8
    :goto_5
    iget-object v0, p0, Lcom/adobe/mobile/ba$1;->a:Lcom/adobe/mobile/ba;

    iput-boolean v8, v0, Lcom/adobe/mobile/ba;->i:Z

    return-void
.end method
