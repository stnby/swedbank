.class final Lcom/adobe/mobile/q$a$c;
.super Ljava/lang/Object;
.source "MessageAlert.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/q$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/adobe/mobile/q;


# direct methods
.method public constructor <init>(Lcom/adobe/mobile/q;)V
    .locals 0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput-object p1, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .line 149
    iget-object p1, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    invoke-virtual {p1}, Lcom/adobe/mobile/q;->h()V

    .line 150
    iget-object p1, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    const/4 p2, 0x0

    iput-boolean p2, p1, Lcom/adobe/mobile/q;->f:Z

    .line 153
    iget-object p1, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    iget-object p1, p1, Lcom/adobe/mobile/q;->m:Ljava/lang/String;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    iget-object p1, p1, Lcom/adobe/mobile/q;->m:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_3

    .line 155
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 157
    iget-object p1, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    iget-object v0, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    iget-object v1, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/adobe/mobile/q;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/adobe/mobile/q;->a(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object p1

    const-string v0, "{userId}"

    const-string v2, "0"

    .line 158
    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{trackingId}"

    const-string v2, "0"

    .line 159
    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{messageId}"

    .line 160
    iget-object v2, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    iget-object v2, v2, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{lifetimeValue}"

    .line 161
    invoke-static {}, Lcom/adobe/mobile/f;->a()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v0

    sget-object v2, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    if-ne v0, v2, :cond_2

    const-string v0, "{userId}"

    .line 164
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, ""

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{trackingId}"

    .line 165
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->y()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, ""

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->y()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/adobe/mobile/q$a$c;->a:Lcom/adobe/mobile/q;

    iget-object v0, v0, Lcom/adobe/mobile/q;->m:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    .line 172
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v0
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 180
    :try_start_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 182
    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    const-string v0, "Messages - Could not load click-through intent for message (%s)"

    .line 184
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, p2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :catch_1
    move-exception p1

    .line 175
    invoke-virtual {p1}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object p1

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_3
    :goto_2
    return-void
.end method
