.class abstract Lcom/adobe/mobile/a;
.super Lcom/adobe/mobile/AbstractDatabaseBacking;
.source "AbstractHitDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/a$b;,
        Lcom/adobe/mobile/a$a;
    }
.end annotation


# instance fields
.field protected f:J

.field protected g:J

.field protected h:Ljava/lang/String;

.field protected i:Z

.field protected final j:Ljava/lang/Object;

.field private final k:Ljava/lang/Object;

.field private l:Ljava/util/Timer;

.field private m:Ljava/util/TimerTask;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/adobe/mobile/a;->i:Z

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/a;->j:Ljava/lang/Object;

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/a;->k:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 48
    :try_start_0
    iget-object v3, p0, Lcom/adobe/mobile/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v4, p0, Lcom/adobe/mobile/a;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v4, "%s - Unable to create database due to an unexpected error (%s)"

    .line 57
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v3

    const-string v4, "%s - Unable to create database due to a sql error (%s)"

    .line 54
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v3

    const-string v4, "%s - Unable to create database due to an invalid path (%s)"

    .line 51
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 62
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_2

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/adobe/mobile/a;->c:Ljava/lang/Object;

    monitor-enter v2

    const/4 v3, 0x2

    .line 69
    :try_start_0
    iget-object v4, p0, Lcom/adobe/mobile/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "HITS"

    const-string v6, "ID = ?"

    new-array v7, v1, [Ljava/lang/String;

    aput-object p1, v7, v0

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 70
    iget-wide v4, p0, Lcom/adobe/mobile/a;->f:J

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/adobe/mobile/a;->f:J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    const-string v4, "%s - Unable to delete hit due to an unexpected error (%s)"

    .line 80
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v5, v3, v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v4, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    new-instance v0, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected exception, database probably corrupted ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception p1

    const-string v4, "%s - Unable to delete hit due to a sql error (%s)"

    .line 76
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v5, v3, v0

    invoke-virtual {p1}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v4, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    new-instance v0, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete, database probably corrupted ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception p1

    const-string v4, "%s - Unable to delete hit due to an unopened database (%s)"

    .line 73
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v5, v3, v0

    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v1

    invoke-static {v4, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    :goto_0
    monitor-exit v2

    return-void

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_1
    :goto_2
    const-string p1, "%s - Unable to delete hit due to an invalid parameter"

    .line 63
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected a(Z)V
    .locals 8

    .line 117
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    .line 120
    invoke-static {}, Lcom/adobe/mobile/at;->a()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->p()I

    move-result v1

    if-lez v1, :cond_1

    .line 122
    iget-object v1, p0, Lcom/adobe/mobile/a;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/adobe/mobile/a;->m:Ljava/util/TimerTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 125
    :try_start_1
    new-instance v0, Lcom/adobe/mobile/a$b;

    invoke-direct {v0, p0, p1}, Lcom/adobe/mobile/a$b;-><init>(Lcom/adobe/mobile/a;Z)V

    iput-object v0, p0, Lcom/adobe/mobile/a;->m:Ljava/util/TimerTask;

    .line 126
    new-instance p1, Ljava/util/Timer;

    invoke-direct {p1}, Ljava/util/Timer;-><init>()V

    iput-object p1, p0, Lcom/adobe/mobile/a;->l:Ljava/util/Timer;

    .line 127
    iget-object p1, p0, Lcom/adobe/mobile/a;->l:Ljava/util/Timer;

    iget-object v0, p0, Lcom/adobe/mobile/a;->m:Ljava/util/TimerTask;

    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v5

    invoke-virtual {v5}, Lcom/adobe/mobile/ao;->p()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {p1, v0, v5, v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string v0, "%s - Error creating referrer timer (%s)"

    .line 129
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v5, v2, v3

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v4

    invoke-static {v0, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/adobe/mobile/a;->l:Ljava/util/Timer;

    if-eqz v1, :cond_2

    .line 137
    iget-object v1, p0, Lcom/adobe/mobile/a;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_3
    iget-object v5, p0, Lcom/adobe/mobile/a;->l:Ljava/util/Timer;

    invoke-virtual {v5}, Ljava/util/Timer;->cancel()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception v5

    :try_start_4
    const-string v6, "%s - Error cancelling referrer timer (%s)"

    .line 142
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v7, v2, v3

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v6, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    const/4 v2, 0x0

    .line 144
    iput-object v2, p0, Lcom/adobe/mobile/a;->m:Ljava/util/TimerTask;

    .line 145
    monitor-exit v1

    goto :goto_3

    :goto_2
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p1

    .line 149
    :cond_2
    :goto_3
    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v1

    sget-object v2, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    if-eq v1, v2, :cond_3

    return-void

    .line 154
    :cond_3
    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->k()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-wide v1, p0, Lcom/adobe/mobile/a;->f:J

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->n()I

    move-result v0

    int-to-long v5, v0

    cmp-long v0, v1, v5

    if-lez v0, :cond_5

    :cond_4
    const/4 v3, 0x1

    :cond_5
    if-nez v3, :cond_6

    if-eqz p1, :cond_7

    .line 157
    :cond_6
    invoke-virtual {p0}, Lcom/adobe/mobile/a;->j()V

    :cond_7
    return-void
.end method

.method protected c()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 191
    iput-wide v0, p0, Lcom/adobe/mobile/a;->f:J

    return-void
.end method

.method protected g()Lcom/adobe/mobile/a$a;
    .locals 2

    .line 34
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getFirstHitInQueue must be overwritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected h()Ljava/lang/Runnable;
    .locals 2

    .line 38
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "workerThread must be overwritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected i()V
    .locals 7

    .line 87
    iget-object v0, p0, Lcom/adobe/mobile/a;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 89
    :try_start_0
    iget-object v4, p0, Lcom/adobe/mobile/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "HITS"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-wide/16 v4, 0x0

    .line 90
    iput-wide v4, p0, Lcom/adobe/mobile/a;->f:J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v4

    :try_start_1
    const-string v5, "%s - Unable to clear tracking queue due to an unexpected error (%s)"

    .line 99
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v4

    const-string v5, "%s - Unable to clear tracking queue due to a sql error (%s)"

    .line 96
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v4

    const-string v5, "%s - Unable to clear tracking queue due to an unopened database (%s)"

    .line 93
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    :goto_0
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected j()V
    .locals 4

    .line 107
    iget-boolean v0, p0, Lcom/adobe/mobile/a;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 109
    iput-boolean v0, p0, Lcom/adobe/mobile/a;->i:Z

    .line 110
    iget-object v0, p0, Lcom/adobe/mobile/a;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 111
    :try_start_0
    new-instance v1, Ljava/lang/Thread;

    invoke-virtual {p0}, Lcom/adobe/mobile/a;->h()Ljava/lang/Runnable;

    move-result-object v2

    const-string v3, "ADBMobileBackgroundThread"

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 112
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :goto_0
    return-void
.end method

.method protected k()J
    .locals 7

    .line 169
    iget-object v0, p0, Lcom/adobe/mobile/a;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 171
    :try_start_0
    iget-object v4, p0, Lcom/adobe/mobile/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "HITS"

    invoke-static {v4, v5}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v4
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_2

    :catch_0
    move-exception v4

    :try_start_1
    const-string v5, "%s - Unable to get tracking queue size due to an unexpected error (%s)"

    .line 180
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v4

    const-string v5, "%s - Unable to get tracking queue size due to a sql error (%s)"

    .line 177
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v4

    const-string v5, "%s - Unable to get tracking queue size due to an unopened database (%s)"

    .line 174
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/a;->e:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const-wide/16 v4, 0x0

    .line 182
    :goto_1
    monitor-exit v0

    return-wide v4

    :goto_2
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
