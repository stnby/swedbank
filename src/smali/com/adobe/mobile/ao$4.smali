.class Lcom/adobe/mobile/ao$4;
.super Landroid/content/BroadcastReceiver;
.source "MobileConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/ao;->H()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/ao;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/ao;)V
    .locals 0

    .line 1241
    iput-object p1, p0, Lcom/adobe/mobile/ao$4;->a:Lcom/adobe/mobile/ao;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .line 1244
    iget-object p2, p0, Lcom/adobe/mobile/ao$4;->a:Lcom/adobe/mobile/ao;

    iget-object v0, p0, Lcom/adobe/mobile/ao$4;->a:Lcom/adobe/mobile/ao;

    invoke-virtual {v0, p1}, Lcom/adobe/mobile/ao;->a(Landroid/content/Context;)Z

    move-result p1

    invoke-static {p2, p1}, Lcom/adobe/mobile/ao;->a(Lcom/adobe/mobile/ao;Z)Z

    .line 1246
    iget-object p1, p0, Lcom/adobe/mobile/ao$4;->a:Lcom/adobe/mobile/ao;

    invoke-static {p1}, Lcom/adobe/mobile/ao;->e(Lcom/adobe/mobile/ao;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1247
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance p2, Lcom/adobe/mobile/ao$4$1;

    invoke-direct {p2, p0}, Lcom/adobe/mobile/ao$4$1;-><init>(Lcom/adobe/mobile/ao$4;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    const-string p1, "Analytics - Network status changed (unreachable)"

    const/4 p2, 0x0

    .line 1255
    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
