.class public final enum Lcom/adobe/mobile/aq;
.super Ljava/lang/Enum;
.source "MobilePrivacyStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adobe/mobile/aq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/adobe/mobile/aq;

.field public static final enum b:Lcom/adobe/mobile/aq;

.field public static final enum c:Lcom/adobe/mobile/aq;

.field private static final synthetic e:[Lcom/adobe/mobile/aq;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 22
    new-instance v0, Lcom/adobe/mobile/aq;

    const-string v1, "MOBILE_PRIVACY_STATUS_OPT_IN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/adobe/mobile/aq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    .line 23
    new-instance v0, Lcom/adobe/mobile/aq;

    const-string v1, "MOBILE_PRIVACY_STATUS_OPT_OUT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/adobe/mobile/aq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    .line 24
    new-instance v0, Lcom/adobe/mobile/aq;

    const-string v1, "MOBILE_PRIVACY_STATUS_UNKNOWN"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/adobe/mobile/aq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/aq;->c:Lcom/adobe/mobile/aq;

    const/4 v0, 0x3

    .line 21
    new-array v0, v0, [Lcom/adobe/mobile/aq;

    sget-object v1, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adobe/mobile/aq;->c:Lcom/adobe/mobile/aq;

    aput-object v1, v0, v4

    sput-object v0, Lcom/adobe/mobile/aq;->e:[Lcom/adobe/mobile/aq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/adobe/mobile/aq;->d:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/aq;
    .locals 1

    .line 21
    const-class v0, Lcom/adobe/mobile/aq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adobe/mobile/aq;

    return-object p0
.end method

.method public static values()[Lcom/adobe/mobile/aq;
    .locals 1

    .line 21
    sget-object v0, Lcom/adobe/mobile/aq;->e:[Lcom/adobe/mobile/aq;

    invoke-virtual {v0}, [Lcom/adobe/mobile/aq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/aq;

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/adobe/mobile/aq;->d:I

    return v0
.end method
