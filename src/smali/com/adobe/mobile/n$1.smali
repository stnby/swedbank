.class Lcom/adobe/mobile/n$1;
.super Ljava/lang/Object;
.source "FloatingButton.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/n;->a(Landroid/app/Activity;Lcom/adobe/mobile/n$a;Lcom/adobe/mobile/n$b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/ViewGroup;

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Lcom/adobe/mobile/n;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/n;Landroid/view/ViewGroup;II)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    iput-object p2, p0, Lcom/adobe/mobile/n$1;->a:Landroid/view/ViewGroup;

    iput p3, p0, Lcom/adobe/mobile/n$1;->b:I

    iput p4, p0, Lcom/adobe/mobile/n$1;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 159
    iget-object v0, p0, Lcom/adobe/mobile/n$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/adobe/mobile/n$1;->b:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/adobe/mobile/n$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    .line 160
    :goto_0
    iget-object v1, p0, Lcom/adobe/mobile/n$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/adobe/mobile/n$1;->c:I

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/adobe/mobile/n$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    .line 162
    :goto_1
    iget-object v2, p0, Lcom/adobe/mobile/n$1;->a:Landroid/view/ViewGroup;

    const-string v3, "ADBFloatingButtonTag"

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/adobe/mobile/n;

    if-eqz v2, :cond_2

    .line 166
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/adobe/mobile/ay;->d()F

    move-result v3

    .line 167
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adobe/mobile/ay;->e()F

    move-result v4

    int-to-float v0, v0

    .line 169
    invoke-static {v2, v0, v3}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;FF)F

    move-result v0

    int-to-float v1, v1

    .line 170
    invoke-static {v2, v1, v4}, Lcom/adobe/mobile/n;->b(Lcom/adobe/mobile/n;FF)F

    move-result v1

    .line 171
    invoke-virtual {v2, v0, v1}, Lcom/adobe/mobile/n;->a(FF)V

    return-void

    .line 175
    :cond_2
    iget-object v2, p0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-virtual {v2}, Lcom/adobe/mobile/n;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 176
    new-instance v3, Lcom/adobe/mobile/n$1$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/adobe/mobile/n$1$1;-><init>(Lcom/adobe/mobile/n$1;II)V

    .line 191
    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 192
    iget-object v0, p0, Lcom/adobe/mobile/n$1;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 194
    iget-object v0, p0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-virtual {v0}, Lcom/adobe/mobile/n;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 196
    iget-object v1, p0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    const/16 v2, 0x50

    invoke-static {v1, v2}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 197
    iget-object v1, p0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-static {v1, v2}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 199
    iget-object v1, p0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-virtual {v1, v0}, Lcom/adobe/mobile/n;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    return-void
.end method
