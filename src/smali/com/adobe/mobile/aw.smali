.class final Lcom/adobe/mobile/aw;
.super Ljava/lang/Object;
.source "RequestHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/aw$a;
    }
.end annotation


# direct methods
.method protected static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/adobe/mobile/ar;
    .locals 5

    .line 401
    invoke-static {p0}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 407
    :cond_0
    new-instance v1, Lcom/adobe/mobile/ar;

    invoke-direct {v1}, Lcom/adobe/mobile/ar;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 410
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    mul-int/lit16 p4, p4, 0x3e8

    .line 411
    invoke-virtual {p0, p4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 412
    invoke-virtual {p0, p4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    if-eqz p5, :cond_1

    .line 413
    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_1

    const-string p4, "Content-Type"

    .line 414
    invoke-virtual {p0, p4, p5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_2

    .line 416
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_2

    const-string p4, "Accept"

    .line 417
    invoke-virtual {p0, p4, p2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string p2, "Accept-Encoding"

    const-string p4, "identity"

    .line 419
    invoke-virtual {p0, p2, p4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "Accept-Language"

    .line 420
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->C()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p2, p4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "User-Agent"

    .line 421
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->k()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p2, p4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p7, :cond_3

    .line 424
    invoke-virtual {p7}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_3

    const-string p2, "session-id"

    .line 425
    invoke-virtual {p0, p2, p7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz p1, :cond_5

    const-string p2, "POST"

    .line 430
    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_4

    const-string p2, "PUT"

    .line 431
    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 432
    :cond_4
    invoke-virtual {p0, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    :cond_5
    if-eqz p3, :cond_6

    .line 435
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_6

    const-string p1, "UTF-8"

    .line 436
    invoke-virtual {p3, p1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    .line 437
    array-length p2, p1

    invoke-virtual {p0, p2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 440
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p2

    .line 441
    new-instance p3, Ljava/io/BufferedOutputStream;

    invoke-direct {p3, p2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 442
    invoke-virtual {p3, p1}, Ljava/io/OutputStream;->write([B)V

    .line 443
    invoke-virtual {p3}, Ljava/io/OutputStream;->close()V

    .line 447
    :cond_6
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result p1

    iput p1, v1, Lcom/adobe/mobile/ar;->a:I

    .line 450
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 452
    :try_start_1
    new-instance p2, Ljava/io/BufferedReader;

    new-instance p3, Ljava/io/InputStreamReader;

    const-string p4, "UTF-8"

    invoke-direct {p3, p1, p4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {p2, p3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 454
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 456
    :goto_0
    invoke-virtual {p2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p4

    if-eqz p4, :cond_7

    .line 457
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 460
    :cond_7
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v1, Lcom/adobe/mobile/ar;->b:Ljava/lang/String;

    .line 463
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object p2

    iput-object p2, v1, Lcom/adobe/mobile/ar;->c:Ljava/util/Map;
    :try_end_1
    .catch Ljava/net/ProtocolException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz p1, :cond_8

    .line 485
    :try_start_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_7

    :catch_0
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 487
    new-array p2, v3, [Ljava/lang/Object;

    aput-object p6, p2, v2

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v4

    goto/16 :goto_6

    :catchall_0
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    goto/16 :goto_8

    :catch_1
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    goto :goto_1

    :catch_2
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    goto :goto_2

    :catch_3
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    goto :goto_3

    :catch_4
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    goto/16 :goto_4

    :catch_5
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    goto/16 :goto_5

    :catchall_1
    move-exception p1

    goto/16 :goto_8

    :catch_6
    move-exception p1

    :goto_1
    :try_start_3
    const-string p2, "%s - Exception while trying to get content (%s)"

    .line 478
    new-array p3, v3, [Ljava/lang/Object;

    aput-object p6, p3, v2

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v4

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 481
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v0, :cond_8

    .line 485
    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    goto/16 :goto_7

    :catch_7
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 487
    new-array p2, v3, [Ljava/lang/Object;

    aput-object p6, p2, v2

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v4

    goto/16 :goto_6

    :catch_8
    move-exception p1

    :goto_2
    :try_start_5
    const-string p2, "%s - Exception while trying to get content (%s)"

    .line 475
    new-array p3, v3, [Ljava/lang/Object;

    aput-object p6, p3, v2

    aput-object p1, p3, v4

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 481
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v0, :cond_8

    .line 485
    :try_start_6
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9

    goto/16 :goto_7

    :catch_9
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 487
    new-array p2, v3, [Ljava/lang/Object;

    aput-object p6, p2, v2

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v4

    goto :goto_6

    :catch_a
    move-exception p1

    :goto_3
    :try_start_7
    const-string p2, "%s - IOException while trying to get content (%s)"

    .line 472
    new-array p3, v3, [Ljava/lang/Object;

    aput-object p6, p3, v2

    aput-object p1, p3, v4

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 481
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v0, :cond_8

    .line 485
    :try_start_8
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_b

    goto :goto_7

    :catch_b
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 487
    new-array p2, v3, [Ljava/lang/Object;

    aput-object p6, p2, v2

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v4

    goto :goto_6

    :catch_c
    move-exception p1

    :goto_4
    :try_start_9
    const-string p2, "%s - NullPointerException while trying to get content (%s)"

    .line 469
    new-array p3, v3, [Ljava/lang/Object;

    aput-object p6, p3, v2

    aput-object p1, p3, v4

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 481
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v0, :cond_8

    .line 485
    :try_start_a
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_d

    goto :goto_7

    :catch_d
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 487
    new-array p2, v3, [Ljava/lang/Object;

    aput-object p6, p2, v2

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v4

    goto :goto_6

    :catch_e
    move-exception p1

    :goto_5
    :try_start_b
    const-string p2, "%s - ProtocolException while trying to get content (%s)"

    .line 466
    new-array p3, v3, [Ljava/lang/Object;

    aput-object p6, p3, v2

    aput-object p1, p3, v4

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 481
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v0, :cond_8

    .line 485
    :try_start_c
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_f

    goto :goto_7

    :catch_f
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 487
    new-array p2, v3, [Ljava/lang/Object;

    aput-object p6, p2, v2

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v4

    :goto_6
    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    :goto_7
    return-object v1

    .line 481
    :goto_8
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v0, :cond_9

    .line 485
    :try_start_d
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_10

    goto :goto_9

    :catch_10
    move-exception p0

    .line 487
    new-array p2, v3, [Ljava/lang/Object;

    aput-object p6, p2, v2

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v4

    const-string p0, "%s - Unable to close stream (%s)"

    invoke-static {p0, p2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 488
    :cond_9
    :goto_9
    throw p1
.end method

.method protected static a(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 3

    .line 390
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p0

    check-cast p0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string v0, "Adobe Mobile - Exception opening URL(%s)"

    const/4 v1, 0x1

    .line 392
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 323
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 324
    invoke-static {p0, p1, p3, p4, p5}, Lcom/adobe/mobile/be;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0

    .line 327
    :cond_1
    invoke-static {p0}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v1

    if-nez v1, :cond_2

    return v0

    :cond_2
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 335
    :try_start_0
    invoke-virtual {v1, p3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 336
    invoke-virtual {v1, p3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const-string p3, "GET"

    .line 337
    invoke-virtual {v1, p3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    if-eqz p2, :cond_3

    .line 341
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Map$Entry;

    .line 342
    invoke-interface {p3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {p3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    invoke-virtual {v1, v4, p3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_5

    .line 347
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_5

    const-string p2, "POST"

    .line 348
    invoke-virtual {v1, p2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    if-eqz p4, :cond_4

    .line 350
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_4

    move-object p2, p4

    goto :goto_1

    :cond_4
    const-string p2, "application/x-www-form-urlencoded"

    :goto_1
    const-string p3, "UTF-8"

    .line 352
    invoke-virtual {p1, p3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p3

    .line 353
    array-length v4, p3

    invoke-virtual {v1, v4}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    const-string v4, "Content-Type"

    .line 354
    invoke-virtual {v1, v4, p2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p2

    .line 358
    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, p2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 359
    invoke-virtual {v4, p3}, Ljava/io/OutputStream;->write([B)V

    .line 360
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 364
    :cond_5
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p2

    const/16 p3, 0xa

    .line 365
    new-array p3, p3, [B

    .line 366
    :goto_2
    invoke-virtual {p2, p3}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-lez v1, :cond_6

    goto :goto_2

    .line 367
    :cond_6
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    const-string p2, "%s - Successfully forwarded hit (%s body: %s type: %s)"

    const/4 p3, 0x4

    .line 369
    new-array p3, p3, [Ljava/lang/Object;

    aput-object p5, p3, v0

    aput-object p0, p3, v3

    aput-object p1, p3, v2

    const/4 p0, 0x3

    aput-object p4, p3, p0

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p0

    const-string p1, "%s - Exception while attempting to send hit, will not retry (%s)"

    .line 382
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p5, p2, v0

    invoke-virtual {p0}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v3

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :catch_1
    move-exception p0

    const-string p1, "%s - Exception while attempting to send hit, will not retry (%s)"

    .line 379
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p5, p2, v0

    invoke-virtual {p0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v3

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :catch_2
    move-exception p0

    const-string p1, "%s - IOException while sending request, will not retry (%s)"

    .line 376
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p5, p2, v0

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v3

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    return v3

    :catch_3
    const-string p0, "%s - Timed out sending request (%s)"

    .line 372
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p5, p2, v0

    aput-object p1, p2, v3

    invoke-static {p0, p2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0
.end method

.method protected static a(Ljava/lang/String;ILjava/lang/String;Ljava/util/concurrent/Callable;Lcom/adobe/mobile/aw$a;)[B
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Callable<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/adobe/mobile/aw$a;",
            ")[B"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    move-object v3, p0

    move-object v4, v0

    const/4 v2, 0x0

    const/4 v5, 0x0

    :goto_0
    const/16 v6, 0x15

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-le v2, v6, :cond_0

    :try_start_0
    const-string p1, "%s - Too many redirects for (%s) - %d"

    const/4 p3, 0x3

    .line 90
    new-array p3, p3, [Ljava/lang/Object;

    aput-object p2, p3, v1

    aput-object p0, p3, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, p3, v7

    invoke-static {p1, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v10, v5

    move-object v5, v4

    move v4, v10

    goto/16 :goto_3

    :catchall_0
    move-exception p0

    :goto_1
    move-object v5, v4

    goto/16 :goto_c

    :catch_0
    move-exception p0

    move-object p1, v0

    goto/16 :goto_6

    :catch_1
    move-exception p0

    move-object p1, v0

    goto/16 :goto_8

    :catch_2
    move-exception p0

    move-object p1, v0

    goto/16 :goto_a

    .line 95
    :cond_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v4, 0x7d0

    .line 96
    :try_start_1
    invoke-virtual {v5, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 97
    invoke-virtual {v5, p1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 98
    invoke-virtual {v5, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    const-string v4, "Accept-Language"

    .line 101
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->C()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "User-Agent"

    .line 102
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p3, :cond_1

    .line 106
    invoke-interface {p3}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 109
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 110
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v9, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 116
    :cond_1
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    if-eqz p4, :cond_2

    .line 120
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v6

    invoke-interface {p4, v6}, Lcom/adobe/mobile/aw$a;->a(Ljava/util/Map;)V

    :cond_2
    packed-switch v4, :pswitch_data_0

    goto :goto_3

    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    const-string v6, "Location"

    .line 129
    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 130
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 131
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v9, v6}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v3}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v3

    move-object v10, v5

    move v5, v4

    move-object v4, v10

    goto/16 :goto_0

    :goto_3
    const/16 p0, 0xc8

    if-ne v4, p0, :cond_6

    .line 141
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const/16 p1, 0x400

    .line 143
    :try_start_2
    new-array p1, p1, [B

    .line 145
    new-instance p3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 149
    :goto_4
    invoke-virtual {p0, p1}, Ljava/io/InputStream;->read([B)I

    move-result p4

    const/4 v2, -0x1

    if-ne p4, v2, :cond_5

    .line 157
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 160
    invoke-virtual {p3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v5, :cond_3

    .line 173
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    if-eqz p0, :cond_4

    .line 178
    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_5

    :catch_3
    move-exception p0

    const-string p3, "%s - Unable to close stream (%s)"

    .line 180
    new-array p4, v7, [Ljava/lang/Object;

    aput-object p2, p4, v1

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p4, v8

    invoke-static {p3, p4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    :goto_5
    return-object p1

    .line 153
    :cond_5
    :try_start_4
    invoke-virtual {p3, p1, v1, p4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    :catchall_1
    move-exception p1

    move-object v0, p0

    move-object p0, p1

    goto/16 :goto_c

    :catch_4
    move-exception p1

    move-object v4, v5

    move-object v10, p1

    move-object p1, p0

    move-object p0, v10

    goto :goto_6

    :catch_5
    move-exception p1

    move-object v4, v5

    move-object v10, p1

    move-object p1, p0

    move-object p0, v10

    goto :goto_8

    :catch_6
    move-exception p1

    move-object v4, v5

    move-object v10, p1

    move-object p1, p0

    move-object p0, v10

    goto/16 :goto_a

    :cond_6
    if-eqz v5, :cond_7

    .line 173
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_7
    return-object v0

    :catchall_2
    move-exception p0

    goto/16 :goto_c

    :catch_7
    move-exception p0

    move-object p1, v0

    move-object v4, v5

    goto :goto_6

    :catch_8
    move-exception p0

    move-object p1, v0

    move-object v4, v5

    goto :goto_8

    :catch_9
    move-exception p0

    move-object p1, v0

    move-object v4, v5

    goto :goto_a

    :goto_6
    :try_start_5
    const-string p3, "%s - Unexpected error while sending request (%s)"

    .line 169
    new-array p4, v7, [Ljava/lang/Object;

    aput-object p2, p4, v1

    invoke-virtual {p0}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p4, v8

    invoke-static {p3, p4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-eqz v4, :cond_8

    .line 173
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_8
    if-eqz p1, :cond_9

    .line 178
    :try_start_6
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_a

    goto :goto_7

    :catch_a
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 180
    new-array p3, v7, [Ljava/lang/Object;

    aput-object p2, p3, v1

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p3, v8

    invoke-static {p1, p3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_9
    :goto_7
    return-object v0

    :goto_8
    :try_start_7
    const-string p3, "%s - Exception while sending request (%s)"

    .line 166
    new-array p4, v7, [Ljava/lang/Object;

    aput-object p2, p4, v1

    invoke-virtual {p0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p4, v8

    invoke-static {p3, p4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v4, :cond_a

    .line 173
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_a
    if-eqz p1, :cond_b

    .line 178
    :try_start_8
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_b

    goto :goto_9

    :catch_b
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 180
    new-array p3, v7, [Ljava/lang/Object;

    aput-object p2, p3, v1

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p3, v8

    invoke-static {p1, p3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_b
    :goto_9
    return-object v0

    :goto_a
    :try_start_9
    const-string p3, "%s - IOException while sending request (%s)"

    .line 163
    new-array p4, v7, [Ljava/lang/Object;

    aput-object p2, p4, v1

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p4, v8

    invoke-static {p3, p4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-eqz v4, :cond_c

    .line 173
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_c
    if-eqz p1, :cond_d

    .line 178
    :try_start_a
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_c

    goto :goto_b

    :catch_c
    move-exception p0

    const-string p1, "%s - Unable to close stream (%s)"

    .line 180
    new-array p3, v7, [Ljava/lang/Object;

    aput-object p2, p3, v1

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p3, v8

    invoke-static {p1, p3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_d
    :goto_b
    return-object v0

    :catchall_3
    move-exception p0

    move-object v0, p1

    goto/16 :goto_1

    :goto_c
    if-eqz v5, :cond_e

    .line 173
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_e
    if-eqz v0, :cond_f

    .line 178
    :try_start_b
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_d

    goto :goto_d

    :catch_d
    move-exception p1

    .line 180
    new-array p3, v7, [Ljava/lang/Object;

    aput-object p2, p3, v1

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v8

    const-string p1, "%s - Unable to close stream (%s)"

    invoke-static {p1, p3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    :cond_f
    :goto_d
    throw p0

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")[B"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 242
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 243
    invoke-static {p0, p1, p3, p4}, Lcom/adobe/mobile/be;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object p0

    return-object p0

    .line 246
    :cond_1
    invoke-static {p0}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const/4 v1, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 254
    :try_start_0
    invoke-virtual {p0, p3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 255
    invoke-virtual {p0, p3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const-string p3, "POST"

    .line 256
    invoke-virtual {p0, p3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p3

    invoke-virtual {p3}, Lcom/adobe/mobile/ao;->j()Z

    move-result p3

    if-nez p3, :cond_3

    const-string p3, "connection"

    const-string v4, "close"

    .line 258
    invoke-virtual {p0, p3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const-string p3, "UTF-8"

    .line 261
    invoke-virtual {p1, p3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p3

    .line 262
    array-length v4, p3

    invoke-virtual {p0, v4}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    const-string v4, "Content-Type"

    const-string v5, "application/x-www-form-urlencoded"

    .line 263
    invoke-virtual {p0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 267
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 268
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p0, v5, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 273
    :cond_4
    new-instance p2, Ljava/io/BufferedOutputStream;

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {p2, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 274
    invoke-virtual {p2, p3}, Ljava/io/OutputStream;->write([B)V

    .line 275
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V

    .line 277
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p2

    const/16 p3, 0x400

    .line 278
    new-array p3, p3, [B

    .line 279
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 281
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v5

    invoke-virtual {v5}, Lcom/adobe/mobile/ao;->j()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_6

    .line 284
    :cond_5
    :goto_1
    invoke-virtual {p2, p3}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_8

    .line 292
    :cond_6
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    const-string p2, "%s - Request Sent(%s)"

    .line 294
    new-array p3, v2, [Ljava/lang/Object;

    aput-object p4, p3, v3

    aput-object p1, p3, v1

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p2
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ao;->j()Z

    move-result p1

    if-nez p1, :cond_7

    .line 312
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_7
    return-object p2

    .line 288
    :cond_8
    :try_start_1
    invoke-virtual {v4, p3, v3, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    :try_start_2
    const-string p2, "%s - Exception while attempting to send hit, will not retry(%s)"

    .line 308
    new-array p3, v2, [Ljava/lang/Object;

    aput-object p4, p3, v3

    invoke-virtual {p1}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v1

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    new-array p1, v3, [B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p2

    invoke-virtual {p2}, Lcom/adobe/mobile/ao;->j()Z

    move-result p2

    if-nez p2, :cond_9

    .line 312
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_9
    return-object p1

    :catch_1
    move-exception p1

    :try_start_3
    const-string p2, "%s - Exception while attempting to send hit, will not retry(%s)"

    .line 305
    new-array p3, v2, [Ljava/lang/Object;

    aput-object p4, p3, v3

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v1

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 306
    new-array p1, v3, [B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 311
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p2

    invoke-virtual {p2}, Lcom/adobe/mobile/ao;->j()Z

    move-result p2

    if-nez p2, :cond_a

    .line 312
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_a
    return-object p1

    :catch_2
    move-exception p1

    :try_start_4
    const-string p2, "%s - IOException while sending request, may retry(%s)"

    .line 302
    new-array p3, v2, [Ljava/lang/Object;

    aput-object p4, p3, v3

    invoke-virtual {p1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v1

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 311
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ao;->j()Z

    move-result p1

    if-nez p1, :cond_b

    .line 312
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_b
    return-object v0

    :catch_3
    :try_start_5
    const-string p2, "%s - Timed out sending request(%s)"

    .line 299
    new-array p3, v2, [Ljava/lang/Object;

    aput-object p4, p3, v3

    aput-object p1, p3, v1

    invoke-static {p2, p3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 311
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ao;->j()Z

    move-result p1

    if-nez p1, :cond_c

    .line 312
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_c
    return-object v0

    .line 311
    :goto_2
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p2

    invoke-virtual {p2}, Lcom/adobe/mobile/ao;->j()Z

    move-result p2

    if-nez p2, :cond_d

    .line 312
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_d
    throw p1
.end method

.method protected static a(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")[B"
        }
    .end annotation

    .line 52
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-static {p0, p2}, Lcom/adobe/mobile/be;->a(Ljava/lang/String;I)[B

    move-result-object p0

    return-object p0

    .line 56
    :cond_0
    new-instance v0, Lcom/adobe/mobile/aw$1;

    invoke-direct {v0, p1}, Lcom/adobe/mobile/aw$1;-><init>(Ljava/util/Map;)V

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 62
    :goto_0
    invoke-static {p0, p2, p3, v0, v1}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;ILjava/lang/String;Ljava/util/concurrent/Callable;Lcom/adobe/mobile/aw$a;)[B

    move-result-object p0

    return-object p0
.end method

.method protected static b(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    .line 194
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    invoke-static {p0, p2, p3}, Lcom/adobe/mobile/be;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 201
    :try_start_0
    invoke-static {p0}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 203
    invoke-virtual {v3, p2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 204
    invoke-virtual {v3, p2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    if-eqz p1, :cond_3

    .line 208
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map$Entry;

    .line 209
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 210
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 211
    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v3, p2, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string p1, "%s - Request Sent(%s)"

    .line 216
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p3, p2, v1

    aput-object p0, p2, v0

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I

    .line 220
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    .line 221
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 222
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    const-string p1, "%s - Exception while attempting to send hit, will not retry(%s)"

    .line 231
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p3, p2, v1

    invoke-virtual {p0}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v0

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception p0

    const-string p1, "%s - Exception while attempting to send hit, will not retry(%s)"

    .line 229
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p3, p2, v1

    invoke-virtual {p0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v0

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_2
    move-exception p0

    const-string p1, "%s - IOException while sending request, may retry(%s)"

    .line 227
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p3, p2, v1

    invoke-virtual {p0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, p2, v0

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_3
    const-string p1, "%s - Timed out sending request(%s)"

    .line 225
    new-array p2, v2, [Ljava/lang/Object;

    aput-object p3, p2, v1

    aput-object p0, p2, v0

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    :goto_1
    return-void
.end method
