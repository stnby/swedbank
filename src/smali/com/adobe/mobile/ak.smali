.class final Lcom/adobe/mobile/ak;
.super Lcom/adobe/mobile/r;
.source "MessageTargetExperienceUIFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/ak$a;,
        Lcom/adobe/mobile/ak$b;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Lcom/adobe/mobile/r;-><init>()V

    const/4 v0, 0x0

    .line 37
    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ak;->a(Z)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/adobe/mobile/r;)Lcom/adobe/mobile/r$a;
    .locals 1

    .line 42
    new-instance v0, Lcom/adobe/mobile/ak$b;

    invoke-direct {v0, p1}, Lcom/adobe/mobile/ak$b;-><init>(Lcom/adobe/mobile/r;)V

    return-object v0
.end method

.method protected f()V
    .locals 2

    .line 135
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ay;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/ak;->k:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/adobe/mobile/ak;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "Could not display the Target Preview Experience UI (no html payload found!)"

    const/4 v1, 0x0

    .line 137
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 140
    :cond_0
    invoke-super {p0}, Lcom/adobe/mobile/r;->f()V

    return-void
.end method

.method protected k()Lcom/adobe/mobile/r$b;
    .locals 1

    .line 130
    new-instance v0, Lcom/adobe/mobile/ak$a;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/ak$a;-><init>(Lcom/adobe/mobile/r;)V

    return-object v0
.end method
