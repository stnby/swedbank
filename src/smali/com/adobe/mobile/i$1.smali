.class Lcom/adobe/mobile/i$1;
.super Ljava/lang/Object;
.source "AnalyticsWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/i;->h()Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/i;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/i;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/adobe/mobile/i$1;->a:Lcom/adobe/mobile/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 238
    invoke-static {}, Lcom/adobe/mobile/i;->l()Lcom/adobe/mobile/i;

    move-result-object v0

    const/16 v1, 0xa

    .line 241
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 244
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "Accept-Language"

    .line 246
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->C()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "User-Agent"

    .line 247
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->k()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :cond_0
    :goto_0
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v2

    sget-object v3, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    const/4 v4, 0x0

    if-ne v2, v3, :cond_7

    .line 251
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/ao;->G()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/adobe/mobile/i;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    sget-object v3, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->a:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    if-ne v2, v3, :cond_7

    .line 254
    invoke-virtual {v0}, Lcom/adobe/mobile/i;->g()Lcom/adobe/mobile/a$a;

    move-result-object v2

    if-nez v2, :cond_1

    goto/16 :goto_4

    .line 261
    :cond_1
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v3

    invoke-virtual {v3}, Lcom/adobe/mobile/ao;->k()Z

    move-result v3

    const/4 v5, 0x1

    if-eqz v3, :cond_2

    .line 263
    iget-wide v6, v2, Lcom/adobe/mobile/a$a;->c:J

    iget-wide v8, v0, Lcom/adobe/mobile/i;->g:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-gez v3, :cond_3

    .line 268
    iget-wide v6, v0, Lcom/adobe/mobile/i;->g:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    .line 269
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "&ts="

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v8, v2, Lcom/adobe/mobile/a$a;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 270
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "&ts="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 272
    iget-object v9, v2, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    invoke-virtual {v9, v3, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    const-string v3, "Analytics - Adjusting out of order hit timestamp(%d->%d)"

    const/4 v8, 0x2

    .line 274
    new-array v8, v8, [Ljava/lang/Object;

    iget-wide v9, v2, Lcom/adobe/mobile/a$a;->c:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v3, v8}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iput-wide v6, v2, Lcom/adobe/mobile/a$a;->c:J

    goto :goto_1

    .line 280
    :cond_2
    iget-wide v6, v2, Lcom/adobe/mobile/a$a;->c:J

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v8

    const-wide/16 v10, 0x3c

    sub-long/2addr v8, v10

    cmp-long v3, v6, v8

    if-gez v3, :cond_3

    .line 286
    :try_start_0
    iget-object v2, v2, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/adobe/mobile/i;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    .line 289
    invoke-static {}, Lcom/adobe/mobile/i;->l()Lcom/adobe/mobile/i;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/adobe/mobile/i;->a(Ljava/lang/Exception;)V

    goto/16 :goto_4

    .line 301
    :cond_3
    :goto_1
    iget-object v3, v2, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    const-string v6, "ndh"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    goto :goto_2

    :cond_4
    iget-object v3, v2, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    iget-object v6, v2, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    const/16 v7, 0x3f

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/2addr v6, v5

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 304
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/i;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/adobe/mobile/i;->n()Ljava/security/SecureRandom;

    move-result-object v7

    const v8, 0x5f5e100

    invoke-virtual {v7, v8}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x1388

    iget-object v8, p0, Lcom/adobe/mobile/i$1;->a:Lcom/adobe/mobile/i;

    iget-object v8, v8, Lcom/adobe/mobile/i;->e:Ljava/lang/String;

    invoke-static {v6, v3, v1, v7, v8}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v3

    if-nez v3, :cond_5

    const-wide/16 v2, 0x1e

    const/4 v6, 0x0

    :goto_3
    int-to-long v7, v6

    cmp-long v7, v7, v2

    if-gez v7, :cond_0

    .line 349
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v7

    invoke-virtual {v7}, Lcom/adobe/mobile/ao;->G()Z

    move-result v7

    if-eqz v7, :cond_0

    const-wide/16 v7, 0x3e8

    .line 350
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :catch_1
    move-exception v2

    const-string v3, "Analytics - Background Thread Interrupted(%s)"

    .line 354
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v4

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 309
    :cond_5
    array-length v6, v3

    if-le v6, v5, :cond_6

    .line 312
    :try_start_2
    iget-object v6, v2, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/adobe/mobile/i;->a(Ljava/lang/String;)V

    .line 313
    iget-wide v6, v2, Lcom/adobe/mobile/a$a;->c:J

    iput-wide v6, v0, Lcom/adobe/mobile/i;->g:J

    .line 315
    new-instance v2, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-direct {v2, v3, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 316
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 319
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->v()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    new-instance v6, Lcom/adobe/mobile/i$1$1;

    invoke-direct {v6, p0, v3}, Lcom/adobe/mobile/i$1$1;-><init>(Lcom/adobe/mobile/i$1;Lorg/json/JSONObject;)V

    invoke-interface {v2, v6}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v2

    const-string v3, "Audience Manager - Unable to parse JSON data (%s)"

    .line 331
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v4

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :catch_3
    move-exception v2

    const-string v3, "Audience Manager - Unable to decode server response (%s)"

    .line 329
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v4

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :catch_4
    move-exception v1

    .line 326
    invoke-static {}, Lcom/adobe/mobile/i;->l()Lcom/adobe/mobile/i;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/adobe/mobile/i;->a(Ljava/lang/Exception;)V

    goto :goto_4

    .line 337
    :cond_6
    :try_start_3
    iget-object v3, v2, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/adobe/mobile/i;->a(Ljava/lang/String;)V

    .line 338
    iget-wide v2, v2, Lcom/adobe/mobile/a$a;->c:J

    iput-wide v2, v0, Lcom/adobe/mobile/i;->g:J
    :try_end_3
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v1

    .line 341
    invoke-static {}, Lcom/adobe/mobile/i;->l()Lcom/adobe/mobile/i;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/adobe/mobile/i;->a(Ljava/lang/Exception;)V

    .line 359
    :cond_7
    :goto_4
    iput-boolean v4, v0, Lcom/adobe/mobile/i;->i:Z

    return-void
.end method
