.class final Lcom/adobe/mobile/av;
.super Ljava/lang/Object;
.source "RequestBuilder.java"


# direct methods
.method protected static a(Ljava/util/Map;Ljava/util/Map;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;J)V"
        }
    .end annotation

    .line 36
    invoke-static {}, Lcom/adobe/mobile/be;->b()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 40
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 42
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->l()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 43
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->F()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_1

    const-string v3, "a.TimeSinceLaunch"

    .line 45
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz p0, :cond_2

    .line 50
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 53
    :cond_2
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p0

    invoke-virtual {p0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object p0

    sget-object v1, Lcom/adobe/mobile/aq;->c:Lcom/adobe/mobile/aq;

    if-ne p0, v1, :cond_3

    const-string p0, "a.privacy.mode"

    const-string v1, "unknown"

    .line 54
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    if-eqz p1, :cond_4

    .line 57
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0

    :cond_4
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 61
    :goto_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->y()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    const-string p1, "aid"

    .line 63
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :cond_5
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->e()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_6

    const-string p1, "vid"

    .line 68
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    const-string p1, "ce"

    .line 71
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ao;->k()Z

    move-result p1

    if-eqz p1, :cond_7

    const-string p1, "ts"

    .line 73
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    const-string p1, "t"

    .line 75
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "cp"

    .line 79
    invoke-static {}, Lcom/adobe/mobile/o;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "background"

    goto :goto_1

    :cond_8
    const-string v1, "foreground"

    .line 78
    :goto_1
    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 83
    :cond_9
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 84
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 85
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_a

    .line 88
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_a
    const-string v3, "&&"

    .line 90
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "&&"

    .line 92
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 91
    invoke-virtual {p0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 98
    :cond_b
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ao;->E()Z

    move-result p1

    if-eqz p1, :cond_c

    .line 99
    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/bd;->g()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 103
    :cond_c
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    new-instance v2, Ljava/util/HashMap;

    .line 105
    invoke-static {}, Lcom/adobe/mobile/o;->c()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 103
    invoke-static {p1, v1, v2}, Lcom/adobe/mobile/an;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 108
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-static {p1, v1}, Lcom/adobe/mobile/an;->a(Ljava/util/Map;Ljava/util/Map;)V

    const-string p1, "c"

    .line 111
    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/util/Map;)Lcom/adobe/mobile/m;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ao;->D()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_d

    .line 116
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ao;->u()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 117
    new-instance p1, Lcom/adobe/mobile/m;

    invoke-direct {p1}, Lcom/adobe/mobile/m;-><init>()V

    .line 118
    new-instance v1, Lcom/adobe/mobile/m;

    invoke-direct {v1}, Lcom/adobe/mobile/m;-><init>()V

    .line 119
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/adobe/mobile/m;->a:Ljava/lang/Object;

    const-string v2, "coop_unsafe"

    .line 120
    invoke-virtual {p1, v2, v1}, Lcom/adobe/mobile/m;->a(Ljava/lang/String;Lcom/adobe/mobile/m;)V

    const-string v1, "d"

    .line 122
    invoke-virtual {p0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_d
    new-instance p1, Ljava/lang/StringBuilder;

    const/16 v1, 0x800

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "ndh=1"

    .line 127
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->E()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 129
    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/bd;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_e
    invoke-static {p0, p1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/util/Map;Ljava/lang/StringBuilder;)V

    const-string p0, "Analytics - Request Queued (%s)"

    .line 135
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    invoke-static {}, Lcom/adobe/mobile/i;->l()Lcom/adobe/mobile/i;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/adobe/mobile/i;->a(Ljava/lang/String;J)V

    return-void
.end method
