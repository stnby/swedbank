.class Lcom/adobe/mobile/r;
.super Lcom/adobe/mobile/p;
.source "MessageFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/r$b;,
        Lcom/adobe/mobile/r$a;
    }
.end annotation


# instance fields
.field protected k:Ljava/lang/String;

.field protected l:Ljava/lang/String;

.field protected m:Landroid/webkit/WebView;

.field protected n:Landroid/app/Activity;

.field protected o:Landroid/view/ViewGroup;

.field protected p:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Lcom/adobe/mobile/p;-><init>()V

    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lcom/adobe/mobile/r;->p:Z

    return-void
.end method

.method static synthetic b(Lcom/adobe/mobile/r;)V
    .locals 0

    .line 43
    invoke-static {p0}, Lcom/adobe/mobile/r;->c(Lcom/adobe/mobile/r;)V

    return-void
.end method

.method private static c(Lcom/adobe/mobile/r;)V
    .locals 2

    .line 396
    iget-object v0, p0, Lcom/adobe/mobile/r;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 397
    iget-object v0, p0, Lcom/adobe/mobile/r;->n:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 398
    iput-boolean v1, p0, Lcom/adobe/mobile/r;->f:Z

    return-void
.end method


# virtual methods
.method protected a(Lcom/adobe/mobile/r;)Lcom/adobe/mobile/r$a;
    .locals 1

    .line 228
    new-instance v0, Lcom/adobe/mobile/r$a;

    invoke-direct {v0, p1}, Lcom/adobe/mobile/r$a;-><init>(Lcom/adobe/mobile/r;)V

    return-object v0
.end method

.method protected a(Z)V
    .locals 0

    .line 146
    iput-boolean p1, p0, Lcom/adobe/mobile/r;->p:Z

    return-void
.end method

.method protected b(Lorg/json/JSONObject;)Z
    .locals 9

    const/4 v0, 0x0

    if-eqz p1, :cond_7

    .line 69
    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-gtz v1, :cond_0

    goto/16 :goto_2

    .line 74
    :cond_0
    invoke-super {p0, p1}, Lcom/adobe/mobile/p;->b(Lorg/json/JSONObject;)Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    :cond_1
    const/4 v1, 0x1

    :try_start_0
    const-string v2, "payload"

    .line 81
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-gtz v2, :cond_2

    const-string p1, "Messages - Unable to create fullscreen message \"%s\", payload is empty"

    .line 83
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/r;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    return v0

    :cond_2
    :try_start_1
    const-string v2, "html"

    .line 94
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/r;->k:Ljava/lang/String;

    .line 95
    iget-object v2, p0, Lcom/adobe/mobile/r;->k:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_3

    const-string p1, "Messages - Unable to create fullscreen message \"%s\", html is empty"

    .line 96
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/r;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    return v0

    :cond_3
    :try_start_2
    const-string v2, "assets"

    .line 107
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 108
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_6

    .line 109
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/adobe/mobile/r;->h:Ljava/util/ArrayList;

    .line 110
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_6

    .line 112
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 113
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 114
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 115
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_4

    .line 117
    invoke-virtual {v4, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 120
    :cond_4
    iget-object v4, p0, Lcom/adobe/mobile/r;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    const-string p1, "Messages - No assets found for fullscreen message \"%s\""

    .line 126
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/r;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    return v1

    :catch_1
    const-string p1, "Messages - Unable to create fullscreen message \"%s\", html is required"

    .line 101
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/r;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :catch_2
    const-string p1, "Messages - Unable to create fullscreen message \"%s\", payload is required"

    .line 88
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/r;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :cond_7
    :goto_2
    return v0
.end method

.method protected f()V
    .locals 12

    const/4 v0, 0x0

    .line 154
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v1
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 161
    invoke-super {p0}, Lcom/adobe/mobile/p;->f()V

    .line 163
    iget-boolean v2, p0, Lcom/adobe/mobile/r;->p:Z

    if-eqz v2, :cond_0

    .line 164
    invoke-virtual {p0}, Lcom/adobe/mobile/r;->e()V

    .line 168
    :cond_0
    invoke-static {p0}, Lcom/adobe/mobile/an;->a(Lcom/adobe/mobile/r;)V

    .line 170
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 173
    iget-object v3, p0, Lcom/adobe/mobile/r;->h:Ljava/util/ArrayList;

    const/4 v4, 0x1

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/adobe/mobile/r;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 174
    iget-object v3, p0, Lcom/adobe/mobile/r;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 175
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_2

    goto :goto_0

    .line 182
    :cond_2
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const/4 v8, 0x0

    .line 185
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v11, "messageImages"

    .line 187
    invoke-static {v10, v11}, Lcom/adobe/mobile/au;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 189
    invoke-virtual {v10}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v8

    invoke-virtual {v8}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v8

    :cond_4
    if-nez v8, :cond_5

    add-int/lit8 v6, v6, -0x1

    .line 196
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 197
    invoke-static {v5}, Lcom/adobe/mobile/au;->a(Ljava/lang/String;)Z

    move-result v6

    xor-int/2addr v6, v4

    if-eqz v6, :cond_5

    goto :goto_1

    :cond_5
    move-object v5, v8

    :goto_1
    if-eqz v5, :cond_1

    .line 205
    invoke-virtual {v2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 210
    :cond_6
    iget-object v3, p0, Lcom/adobe/mobile/r;->k:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/r;->l:Ljava/lang/String;

    .line 213
    :try_start_1
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v5, Lcom/adobe/mobile/s;

    invoke-direct {v2, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x10000

    .line 214
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 215
    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 216
    invoke-virtual {v1, v0, v0}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    const-string v2, "Messages - Must declare MessageFullScreenActivity in AndroidManifest.xml in order to show full screen messages (%s)"

    .line 219
    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    return-void

    :catch_1
    move-exception v1

    .line 157
    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected j()V
    .locals 2

    .line 133
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->H()I

    move-result v0

    .line 134
    iget-boolean v1, p0, Lcom/adobe/mobile/r;->f:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/adobe/mobile/r;->g:I

    if-ne v1, v0, :cond_0

    return-void

    .line 138
    :cond_0
    iput v0, p0, Lcom/adobe/mobile/r;->g:I

    .line 141
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 142
    invoke-virtual {p0, p0}, Lcom/adobe/mobile/r;->a(Lcom/adobe/mobile/r;)Lcom/adobe/mobile/r$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected k()Lcom/adobe/mobile/r$b;
    .locals 1

    .line 224
    new-instance v0, Lcom/adobe/mobile/r$b;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/r$b;-><init>(Lcom/adobe/mobile/r;)V

    return-object v0
.end method
