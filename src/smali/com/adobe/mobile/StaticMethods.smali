.class final Lcom/adobe/mobile/StaticMethods;
.super Ljava/lang/Object;
.source "StaticMethods.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/StaticMethods$NullActivityException;,
        Lcom/adobe/mobile/StaticMethods$NullContextException;
    }
.end annotation


# static fields
.field private static final A:Ljava/lang/Object;

.field private static B:Ljava/lang/String;

.field private static final C:Ljava/lang/Object;

.field private static D:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final E:Ljava/lang/Object;

.field private static F:Ljava/lang/String;

.field private static final G:Ljava/lang/Object;

.field private static H:Ljava/lang/String;

.field private static final I:Ljava/lang/Object;

.field private static J:Ljava/lang/String;

.field private static final K:Ljava/lang/Object;

.field private static L:Ljava/lang/String;

.field private static final M:Ljava/lang/Object;

.field private static N:Ljava/util/concurrent/ExecutorService;

.field private static final O:Ljava/lang/Object;

.field private static P:Ljava/util/concurrent/ExecutorService;

.field private static final Q:Ljava/lang/Object;

.field private static R:Ljava/util/concurrent/ExecutorService;

.field private static final S:Ljava/lang/Object;

.field private static T:Ljava/util/concurrent/ExecutorService;

.field private static final U:Ljava/lang/Object;

.field private static V:Ljava/util/concurrent/ExecutorService;

.field private static final W:Ljava/lang/Object;

.field private static X:Ljava/util/concurrent/ExecutorService;

.field private static final Y:Ljava/lang/Object;

.field private static Z:Ljava/util/concurrent/ExecutorService;

.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final aa:Ljava/lang/Object;

.field private static ab:Ljava/util/concurrent/ExecutorService;

.field private static final ac:Ljava/lang/Object;

.field private static ad:Ljava/util/concurrent/ExecutorService;

.field private static final ae:Ljava/lang/Object;

.field private static af:Ljava/lang/String;

.field private static final ag:Ljava/lang/Object;

.field private static ah:Landroid/content/Context;

.field private static ai:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static final aj:Ljava/lang/Object;

.field private static final ak:[C

.field private static final al:[C

.field static b:I

.field private static final c:[Ljava/lang/String;

.field private static final d:[Z

.field private static final e:[Z

.field private static f:Z

.field private static g:Lcom/adobe/mobile/l$b;

.field private static h:Z

.field private static i:Landroid/content/SharedPreferences;

.field private static final j:Ljava/lang/Object;

.field private static k:Ljava/lang/String;

.field private static final l:Ljava/lang/Object;

.field private static m:Ljava/lang/String;

.field private static final n:Ljava/lang/Object;

.field private static o:Ljava/lang/String;

.field private static final p:Ljava/lang/Object;

.field private static q:I

.field private static final r:Ljava/lang/Object;

.field private static s:Ljava/lang/String;

.field private static final t:Ljava/lang/Object;

.field private static final u:Ljava/lang/Object;

.field private static v:Z

.field private static final w:Ljava/lang/Object;

.field private static x:Ljava/lang/String;

.field private static final y:Ljava/lang/Object;

.field private static z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x100

    .line 110
    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "%00"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "%01"

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "%02"

    const/4 v4, 0x2

    aput-object v2, v1, v4

    const-string v2, "%03"

    const/4 v4, 0x3

    aput-object v2, v1, v4

    const-string v2, "%04"

    const/4 v4, 0x4

    aput-object v2, v1, v4

    const-string v2, "%05"

    const/4 v4, 0x5

    aput-object v2, v1, v4

    const-string v2, "%06"

    const/4 v4, 0x6

    aput-object v2, v1, v4

    const-string v2, "%07"

    const/4 v4, 0x7

    aput-object v2, v1, v4

    const-string v2, "%08"

    const/16 v4, 0x8

    aput-object v2, v1, v4

    const-string v2, "%09"

    const/16 v4, 0x9

    aput-object v2, v1, v4

    const-string v2, "%0A"

    const/16 v4, 0xa

    aput-object v2, v1, v4

    const-string v2, "%0B"

    const/16 v4, 0xb

    aput-object v2, v1, v4

    const-string v2, "%0C"

    const/16 v4, 0xc

    aput-object v2, v1, v4

    const-string v2, "%0D"

    const/16 v4, 0xd

    aput-object v2, v1, v4

    const-string v2, "%0E"

    const/16 v4, 0xe

    aput-object v2, v1, v4

    const-string v2, "%0F"

    const/16 v4, 0xf

    aput-object v2, v1, v4

    const-string v2, "%10"

    const/16 v4, 0x10

    aput-object v2, v1, v4

    const-string v2, "%11"

    const/16 v4, 0x11

    aput-object v2, v1, v4

    const-string v2, "%12"

    const/16 v4, 0x12

    aput-object v2, v1, v4

    const-string v2, "%13"

    const/16 v4, 0x13

    aput-object v2, v1, v4

    const-string v2, "%14"

    const/16 v4, 0x14

    aput-object v2, v1, v4

    const-string v2, "%15"

    const/16 v4, 0x15

    aput-object v2, v1, v4

    const-string v2, "%16"

    const/16 v4, 0x16

    aput-object v2, v1, v4

    const-string v2, "%17"

    const/16 v4, 0x17

    aput-object v2, v1, v4

    const-string v2, "%18"

    const/16 v4, 0x18

    aput-object v2, v1, v4

    const-string v2, "%19"

    const/16 v4, 0x19

    aput-object v2, v1, v4

    const-string v2, "%1A"

    const/16 v4, 0x1a

    aput-object v2, v1, v4

    const-string v2, "%1B"

    const/16 v4, 0x1b

    aput-object v2, v1, v4

    const-string v2, "%1C"

    const/16 v4, 0x1c

    aput-object v2, v1, v4

    const-string v2, "%1D"

    const/16 v4, 0x1d

    aput-object v2, v1, v4

    const-string v2, "%1E"

    const/16 v4, 0x1e

    aput-object v2, v1, v4

    const-string v2, "%1F"

    const/16 v4, 0x1f

    aput-object v2, v1, v4

    const-string v2, "%20"

    const/16 v4, 0x20

    aput-object v2, v1, v4

    const-string v2, "%21"

    const/16 v4, 0x21

    aput-object v2, v1, v4

    const-string v2, "%22"

    const/16 v4, 0x22

    aput-object v2, v1, v4

    const-string v2, "%23"

    const/16 v4, 0x23

    aput-object v2, v1, v4

    const-string v2, "%24"

    const/16 v4, 0x24

    aput-object v2, v1, v4

    const-string v2, "%25"

    const/16 v4, 0x25

    aput-object v2, v1, v4

    const-string v2, "%26"

    const/16 v4, 0x26

    aput-object v2, v1, v4

    const-string v2, "%27"

    const/16 v4, 0x27

    aput-object v2, v1, v4

    const-string v2, "%28"

    const/16 v4, 0x28

    aput-object v2, v1, v4

    const-string v2, "%29"

    const/16 v4, 0x29

    aput-object v2, v1, v4

    const-string v2, "*"

    const/16 v4, 0x2a

    aput-object v2, v1, v4

    const-string v2, "%2B"

    const/16 v4, 0x2b

    aput-object v2, v1, v4

    const-string v2, "%2C"

    const/16 v4, 0x2c

    aput-object v2, v1, v4

    const-string v2, "-"

    const/16 v4, 0x2d

    aput-object v2, v1, v4

    const-string v2, "."

    const/16 v4, 0x2e

    aput-object v2, v1, v4

    const-string v2, "%2F"

    const/16 v4, 0x2f

    aput-object v2, v1, v4

    const-string v2, "0"

    const/16 v4, 0x30

    aput-object v2, v1, v4

    const-string v2, "1"

    const/16 v4, 0x31

    aput-object v2, v1, v4

    const-string v2, "2"

    const/16 v4, 0x32

    aput-object v2, v1, v4

    const-string v2, "3"

    const/16 v4, 0x33

    aput-object v2, v1, v4

    const-string v2, "4"

    const/16 v4, 0x34

    aput-object v2, v1, v4

    const-string v2, "5"

    const/16 v4, 0x35

    aput-object v2, v1, v4

    const-string v2, "6"

    const/16 v4, 0x36

    aput-object v2, v1, v4

    const-string v2, "7"

    const/16 v4, 0x37

    aput-object v2, v1, v4

    const-string v2, "8"

    const/16 v4, 0x38

    aput-object v2, v1, v4

    const-string v2, "9"

    const/16 v4, 0x39

    aput-object v2, v1, v4

    const-string v2, "%3A"

    const/16 v4, 0x3a

    aput-object v2, v1, v4

    const-string v2, "%3B"

    const/16 v4, 0x3b

    aput-object v2, v1, v4

    const-string v2, "%3C"

    const/16 v4, 0x3c

    aput-object v2, v1, v4

    const-string v2, "%3D"

    const/16 v4, 0x3d

    aput-object v2, v1, v4

    const-string v2, "%3E"

    const/16 v4, 0x3e

    aput-object v2, v1, v4

    const-string v2, "%3F"

    const/16 v4, 0x3f

    aput-object v2, v1, v4

    const-string v2, "%40"

    const/16 v4, 0x40

    aput-object v2, v1, v4

    const-string v2, "A"

    const/16 v4, 0x41

    aput-object v2, v1, v4

    const-string v2, "B"

    const/16 v4, 0x42

    aput-object v2, v1, v4

    const-string v2, "C"

    const/16 v4, 0x43

    aput-object v2, v1, v4

    const-string v2, "D"

    const/16 v4, 0x44

    aput-object v2, v1, v4

    const-string v2, "E"

    const/16 v4, 0x45

    aput-object v2, v1, v4

    const-string v2, "F"

    const/16 v4, 0x46

    aput-object v2, v1, v4

    const-string v2, "G"

    const/16 v4, 0x47

    aput-object v2, v1, v4

    const-string v2, "H"

    const/16 v4, 0x48

    aput-object v2, v1, v4

    const-string v2, "I"

    const/16 v4, 0x49

    aput-object v2, v1, v4

    const-string v2, "J"

    const/16 v4, 0x4a

    aput-object v2, v1, v4

    const-string v2, "K"

    const/16 v4, 0x4b

    aput-object v2, v1, v4

    const-string v2, "L"

    const/16 v4, 0x4c

    aput-object v2, v1, v4

    const-string v2, "M"

    const/16 v4, 0x4d

    aput-object v2, v1, v4

    const-string v2, "N"

    const/16 v4, 0x4e

    aput-object v2, v1, v4

    const-string v2, "O"

    const/16 v4, 0x4f

    aput-object v2, v1, v4

    const-string v2, "P"

    const/16 v4, 0x50

    aput-object v2, v1, v4

    const-string v2, "Q"

    const/16 v4, 0x51

    aput-object v2, v1, v4

    const-string v2, "R"

    const/16 v4, 0x52

    aput-object v2, v1, v4

    const-string v2, "S"

    const/16 v4, 0x53

    aput-object v2, v1, v4

    const-string v2, "T"

    const/16 v4, 0x54

    aput-object v2, v1, v4

    const-string v2, "U"

    const/16 v4, 0x55

    aput-object v2, v1, v4

    const-string v2, "V"

    const/16 v4, 0x56

    aput-object v2, v1, v4

    const-string v2, "W"

    const/16 v4, 0x57

    aput-object v2, v1, v4

    const-string v2, "X"

    const/16 v4, 0x58

    aput-object v2, v1, v4

    const-string v2, "Y"

    const/16 v4, 0x59

    aput-object v2, v1, v4

    const-string v2, "Z"

    const/16 v4, 0x5a

    aput-object v2, v1, v4

    const-string v2, "%5B"

    const/16 v4, 0x5b

    aput-object v2, v1, v4

    const-string v2, "%5C"

    const/16 v4, 0x5c

    aput-object v2, v1, v4

    const-string v2, "%5D"

    const/16 v4, 0x5d

    aput-object v2, v1, v4

    const-string v2, "%5E"

    const/16 v4, 0x5e

    aput-object v2, v1, v4

    const-string v2, "_"

    const/16 v4, 0x5f

    aput-object v2, v1, v4

    const-string v2, "%60"

    const/16 v4, 0x60

    aput-object v2, v1, v4

    const-string v2, "a"

    const/16 v4, 0x61

    aput-object v2, v1, v4

    const-string v2, "b"

    const/16 v4, 0x62

    aput-object v2, v1, v4

    const-string v2, "c"

    const/16 v4, 0x63

    aput-object v2, v1, v4

    const-string v2, "d"

    const/16 v4, 0x64

    aput-object v2, v1, v4

    const-string v2, "e"

    const/16 v4, 0x65

    aput-object v2, v1, v4

    const-string v2, "f"

    const/16 v4, 0x66

    aput-object v2, v1, v4

    const-string v2, "g"

    const/16 v4, 0x67

    aput-object v2, v1, v4

    const-string v2, "h"

    const/16 v4, 0x68

    aput-object v2, v1, v4

    const-string v2, "i"

    const/16 v4, 0x69

    aput-object v2, v1, v4

    const-string v2, "j"

    const/16 v4, 0x6a

    aput-object v2, v1, v4

    const-string v2, "k"

    const/16 v4, 0x6b

    aput-object v2, v1, v4

    const-string v2, "l"

    const/16 v4, 0x6c

    aput-object v2, v1, v4

    const-string v2, "m"

    const/16 v4, 0x6d

    aput-object v2, v1, v4

    const-string v2, "n"

    const/16 v4, 0x6e

    aput-object v2, v1, v4

    const-string v2, "o"

    const/16 v4, 0x6f

    aput-object v2, v1, v4

    const-string v2, "p"

    const/16 v4, 0x70

    aput-object v2, v1, v4

    const-string v2, "q"

    const/16 v4, 0x71

    aput-object v2, v1, v4

    const-string v2, "r"

    const/16 v4, 0x72

    aput-object v2, v1, v4

    const-string v2, "s"

    const/16 v4, 0x73

    aput-object v2, v1, v4

    const-string v2, "t"

    const/16 v4, 0x74

    aput-object v2, v1, v4

    const-string v2, "u"

    const/16 v4, 0x75

    aput-object v2, v1, v4

    const-string v2, "v"

    const/16 v4, 0x76

    aput-object v2, v1, v4

    const-string v2, "w"

    const/16 v4, 0x77

    aput-object v2, v1, v4

    const-string v2, "x"

    const/16 v4, 0x78

    aput-object v2, v1, v4

    const-string v2, "y"

    const/16 v4, 0x79

    aput-object v2, v1, v4

    const-string v2, "z"

    const/16 v4, 0x7a

    aput-object v2, v1, v4

    const-string v2, "%7B"

    const/16 v4, 0x7b

    aput-object v2, v1, v4

    const-string v2, "%7C"

    const/16 v4, 0x7c

    aput-object v2, v1, v4

    const-string v2, "%7D"

    const/16 v4, 0x7d

    aput-object v2, v1, v4

    const-string v2, "%7E"

    const/16 v4, 0x7e

    aput-object v2, v1, v4

    const-string v2, "%7F"

    const/16 v4, 0x7f

    aput-object v2, v1, v4

    const-string v2, "%80"

    const/16 v4, 0x80

    aput-object v2, v1, v4

    const-string v2, "%81"

    const/16 v4, 0x81

    aput-object v2, v1, v4

    const-string v2, "%82"

    const/16 v4, 0x82

    aput-object v2, v1, v4

    const-string v2, "%83"

    const/16 v4, 0x83

    aput-object v2, v1, v4

    const-string v2, "%84"

    const/16 v4, 0x84

    aput-object v2, v1, v4

    const-string v2, "%85"

    const/16 v4, 0x85

    aput-object v2, v1, v4

    const-string v2, "%86"

    const/16 v4, 0x86

    aput-object v2, v1, v4

    const-string v2, "%87"

    const/16 v4, 0x87

    aput-object v2, v1, v4

    const-string v2, "%88"

    const/16 v4, 0x88

    aput-object v2, v1, v4

    const-string v2, "%89"

    const/16 v4, 0x89

    aput-object v2, v1, v4

    const-string v2, "%8A"

    const/16 v4, 0x8a

    aput-object v2, v1, v4

    const-string v2, "%8B"

    const/16 v4, 0x8b

    aput-object v2, v1, v4

    const-string v2, "%8C"

    const/16 v4, 0x8c

    aput-object v2, v1, v4

    const-string v2, "%8D"

    const/16 v4, 0x8d

    aput-object v2, v1, v4

    const-string v2, "%8E"

    const/16 v4, 0x8e

    aput-object v2, v1, v4

    const-string v2, "%8F"

    const/16 v4, 0x8f

    aput-object v2, v1, v4

    const-string v2, "%90"

    const/16 v4, 0x90

    aput-object v2, v1, v4

    const-string v2, "%91"

    const/16 v4, 0x91

    aput-object v2, v1, v4

    const-string v2, "%92"

    const/16 v4, 0x92

    aput-object v2, v1, v4

    const-string v2, "%93"

    const/16 v4, 0x93

    aput-object v2, v1, v4

    const-string v2, "%94"

    const/16 v4, 0x94

    aput-object v2, v1, v4

    const-string v2, "%95"

    const/16 v4, 0x95

    aput-object v2, v1, v4

    const-string v2, "%96"

    const/16 v4, 0x96

    aput-object v2, v1, v4

    const-string v2, "%97"

    const/16 v4, 0x97

    aput-object v2, v1, v4

    const-string v2, "%98"

    const/16 v4, 0x98

    aput-object v2, v1, v4

    const-string v2, "%99"

    const/16 v4, 0x99

    aput-object v2, v1, v4

    const-string v2, "%9A"

    const/16 v4, 0x9a

    aput-object v2, v1, v4

    const-string v2, "%9B"

    const/16 v4, 0x9b

    aput-object v2, v1, v4

    const-string v2, "%9C"

    const/16 v4, 0x9c

    aput-object v2, v1, v4

    const-string v2, "%9D"

    const/16 v4, 0x9d

    aput-object v2, v1, v4

    const-string v2, "%9E"

    const/16 v4, 0x9e

    aput-object v2, v1, v4

    const-string v2, "%9F"

    const/16 v4, 0x9f

    aput-object v2, v1, v4

    const-string v2, "%A0"

    const/16 v4, 0xa0

    aput-object v2, v1, v4

    const-string v2, "%A1"

    const/16 v4, 0xa1

    aput-object v2, v1, v4

    const-string v2, "%A2"

    const/16 v4, 0xa2

    aput-object v2, v1, v4

    const-string v2, "%A3"

    const/16 v4, 0xa3

    aput-object v2, v1, v4

    const-string v2, "%A4"

    const/16 v4, 0xa4

    aput-object v2, v1, v4

    const-string v2, "%A5"

    const/16 v4, 0xa5

    aput-object v2, v1, v4

    const-string v2, "%A6"

    const/16 v4, 0xa6

    aput-object v2, v1, v4

    const-string v2, "%A7"

    const/16 v4, 0xa7

    aput-object v2, v1, v4

    const-string v2, "%A8"

    const/16 v4, 0xa8

    aput-object v2, v1, v4

    const-string v2, "%A9"

    const/16 v4, 0xa9

    aput-object v2, v1, v4

    const-string v2, "%AA"

    const/16 v4, 0xaa

    aput-object v2, v1, v4

    const-string v2, "%AB"

    const/16 v4, 0xab

    aput-object v2, v1, v4

    const-string v2, "%AC"

    const/16 v4, 0xac

    aput-object v2, v1, v4

    const-string v2, "%AD"

    const/16 v4, 0xad

    aput-object v2, v1, v4

    const-string v2, "%AE"

    const/16 v4, 0xae

    aput-object v2, v1, v4

    const-string v2, "%AF"

    const/16 v4, 0xaf

    aput-object v2, v1, v4

    const-string v2, "%B0"

    const/16 v4, 0xb0

    aput-object v2, v1, v4

    const-string v2, "%B1"

    const/16 v4, 0xb1

    aput-object v2, v1, v4

    const-string v2, "%B2"

    const/16 v4, 0xb2

    aput-object v2, v1, v4

    const-string v2, "%B3"

    const/16 v4, 0xb3

    aput-object v2, v1, v4

    const-string v2, "%B4"

    const/16 v4, 0xb4

    aput-object v2, v1, v4

    const-string v2, "%B5"

    const/16 v4, 0xb5

    aput-object v2, v1, v4

    const-string v2, "%B6"

    const/16 v4, 0xb6

    aput-object v2, v1, v4

    const-string v2, "%B7"

    const/16 v4, 0xb7

    aput-object v2, v1, v4

    const-string v2, "%B8"

    const/16 v4, 0xb8

    aput-object v2, v1, v4

    const-string v2, "%B9"

    const/16 v4, 0xb9

    aput-object v2, v1, v4

    const-string v2, "%BA"

    const/16 v4, 0xba

    aput-object v2, v1, v4

    const-string v2, "%BB"

    const/16 v4, 0xbb

    aput-object v2, v1, v4

    const-string v2, "%BC"

    const/16 v4, 0xbc

    aput-object v2, v1, v4

    const-string v2, "%BD"

    const/16 v4, 0xbd

    aput-object v2, v1, v4

    const-string v2, "%BE"

    const/16 v4, 0xbe

    aput-object v2, v1, v4

    const-string v2, "%BF"

    const/16 v4, 0xbf

    aput-object v2, v1, v4

    const-string v2, "%C0"

    const/16 v4, 0xc0

    aput-object v2, v1, v4

    const-string v2, "%C1"

    const/16 v4, 0xc1

    aput-object v2, v1, v4

    const-string v2, "%C2"

    const/16 v4, 0xc2

    aput-object v2, v1, v4

    const-string v2, "%C3"

    const/16 v4, 0xc3

    aput-object v2, v1, v4

    const-string v2, "%C4"

    const/16 v4, 0xc4

    aput-object v2, v1, v4

    const-string v2, "%C5"

    const/16 v4, 0xc5

    aput-object v2, v1, v4

    const-string v2, "%C6"

    const/16 v4, 0xc6

    aput-object v2, v1, v4

    const-string v2, "%C7"

    const/16 v4, 0xc7

    aput-object v2, v1, v4

    const-string v2, "%C8"

    const/16 v4, 0xc8

    aput-object v2, v1, v4

    const-string v2, "%C9"

    const/16 v4, 0xc9

    aput-object v2, v1, v4

    const-string v2, "%CA"

    const/16 v4, 0xca

    aput-object v2, v1, v4

    const-string v2, "%CB"

    const/16 v4, 0xcb

    aput-object v2, v1, v4

    const-string v2, "%CC"

    const/16 v4, 0xcc

    aput-object v2, v1, v4

    const-string v2, "%CD"

    const/16 v4, 0xcd

    aput-object v2, v1, v4

    const-string v2, "%CE"

    const/16 v4, 0xce

    aput-object v2, v1, v4

    const-string v2, "%CF"

    const/16 v4, 0xcf

    aput-object v2, v1, v4

    const-string v2, "%D0"

    const/16 v4, 0xd0

    aput-object v2, v1, v4

    const-string v2, "%D1"

    const/16 v4, 0xd1

    aput-object v2, v1, v4

    const-string v2, "%D2"

    const/16 v4, 0xd2

    aput-object v2, v1, v4

    const-string v2, "%D3"

    const/16 v4, 0xd3

    aput-object v2, v1, v4

    const-string v2, "%D4"

    const/16 v4, 0xd4

    aput-object v2, v1, v4

    const-string v2, "%D5"

    const/16 v4, 0xd5

    aput-object v2, v1, v4

    const-string v2, "%D6"

    const/16 v4, 0xd6

    aput-object v2, v1, v4

    const-string v2, "%D7"

    const/16 v4, 0xd7

    aput-object v2, v1, v4

    const-string v2, "%D8"

    const/16 v4, 0xd8

    aput-object v2, v1, v4

    const-string v2, "%D9"

    const/16 v4, 0xd9

    aput-object v2, v1, v4

    const-string v2, "%DA"

    const/16 v4, 0xda

    aput-object v2, v1, v4

    const-string v2, "%DB"

    const/16 v4, 0xdb

    aput-object v2, v1, v4

    const-string v2, "%DC"

    const/16 v4, 0xdc

    aput-object v2, v1, v4

    const-string v2, "%DD"

    const/16 v4, 0xdd

    aput-object v2, v1, v4

    const-string v2, "%DE"

    const/16 v4, 0xde

    aput-object v2, v1, v4

    const-string v2, "%DF"

    const/16 v4, 0xdf

    aput-object v2, v1, v4

    const-string v2, "%E0"

    const/16 v4, 0xe0

    aput-object v2, v1, v4

    const-string v2, "%E1"

    const/16 v4, 0xe1

    aput-object v2, v1, v4

    const-string v2, "%E2"

    const/16 v4, 0xe2

    aput-object v2, v1, v4

    const-string v2, "%E3"

    const/16 v4, 0xe3

    aput-object v2, v1, v4

    const-string v2, "%E4"

    const/16 v4, 0xe4

    aput-object v2, v1, v4

    const-string v2, "%E5"

    const/16 v4, 0xe5

    aput-object v2, v1, v4

    const-string v2, "%E6"

    const/16 v4, 0xe6

    aput-object v2, v1, v4

    const-string v2, "%E7"

    const/16 v4, 0xe7

    aput-object v2, v1, v4

    const-string v2, "%E8"

    const/16 v4, 0xe8

    aput-object v2, v1, v4

    const-string v2, "%E9"

    const/16 v4, 0xe9

    aput-object v2, v1, v4

    const-string v2, "%EA"

    const/16 v4, 0xea

    aput-object v2, v1, v4

    const-string v2, "%EB"

    const/16 v4, 0xeb

    aput-object v2, v1, v4

    const-string v2, "%EC"

    const/16 v4, 0xec

    aput-object v2, v1, v4

    const-string v2, "%ED"

    const/16 v4, 0xed

    aput-object v2, v1, v4

    const-string v2, "%EE"

    const/16 v4, 0xee

    aput-object v2, v1, v4

    const-string v2, "%EF"

    const/16 v4, 0xef

    aput-object v2, v1, v4

    const-string v2, "%F0"

    const/16 v4, 0xf0

    aput-object v2, v1, v4

    const-string v2, "%F1"

    const/16 v4, 0xf1

    aput-object v2, v1, v4

    const-string v2, "%F2"

    const/16 v4, 0xf2

    aput-object v2, v1, v4

    const-string v2, "%F3"

    const/16 v4, 0xf3

    aput-object v2, v1, v4

    const-string v2, "%F4"

    const/16 v4, 0xf4

    aput-object v2, v1, v4

    const-string v2, "%F5"

    const/16 v4, 0xf5

    aput-object v2, v1, v4

    const-string v2, "%F6"

    const/16 v4, 0xf6

    aput-object v2, v1, v4

    const-string v2, "%F7"

    const/16 v4, 0xf7

    aput-object v2, v1, v4

    const-string v2, "%F8"

    const/16 v4, 0xf8

    aput-object v2, v1, v4

    const-string v2, "%F9"

    const/16 v4, 0xf9

    aput-object v2, v1, v4

    const-string v2, "%FA"

    const/16 v4, 0xfa

    aput-object v2, v1, v4

    const-string v2, "%FB"

    const/16 v4, 0xfb

    aput-object v2, v1, v4

    const-string v2, "%FC"

    const/16 v4, 0xfc

    aput-object v2, v1, v4

    const-string v2, "%FD"

    const/16 v4, 0xfd

    aput-object v2, v1, v4

    const-string v2, "%FE"

    const/16 v4, 0xfe

    aput-object v2, v1, v4

    const-string v2, "%FF"

    const/16 v4, 0xff

    aput-object v2, v1, v4

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->c:[Ljava/lang/String;

    .line 129
    new-array v1, v0, [Z

    fill-array-data v1, :array_0

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->d:[Z

    .line 148
    new-array v1, v0, [Z

    fill-array-data v1, :array_1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->e:[Z

    .line 169
    sget-object v1, Lcom/adobe/mobile/l$b;->a:Lcom/adobe/mobile/l$b;

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->g:Lcom/adobe/mobile/l$b;

    .line 170
    sput-boolean v3, Lcom/adobe/mobile/StaticMethods;->h:Z

    const/4 v1, 0x0

    .line 192
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->i:Landroid/content/SharedPreferences;

    .line 193
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->j:Ljava/lang/Object;

    .line 211
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->k:Ljava/lang/String;

    .line 212
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->l:Ljava/lang/Object;

    .line 233
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;

    .line 234
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->n:Ljava/lang/Object;

    .line 268
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;

    .line 269
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->p:Ljava/lang/Object;

    const/4 v2, -0x1

    .line 302
    sput v2, Lcom/adobe/mobile/StaticMethods;->q:I

    .line 303
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->r:Ljava/lang/Object;

    .line 336
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->s:Ljava/lang/String;

    .line 337
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->t:Ljava/lang/Object;

    .line 352
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->u:Ljava/lang/Object;

    .line 381
    sput-boolean v3, Lcom/adobe/mobile/StaticMethods;->v:Z

    .line 382
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->w:Ljava/lang/Object;

    .line 414
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->x:Ljava/lang/String;

    .line 415
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->y:Ljava/lang/Object;

    .line 504
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->z:Ljava/lang/String;

    .line 505
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->A:Ljava/lang/Object;

    .line 536
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->B:Ljava/lang/String;

    .line 537
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->C:Ljava/lang/Object;

    .line 549
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    .line 550
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->E:Ljava/lang/Object;

    .line 568
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->F:Ljava/lang/String;

    .line 569
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->G:Ljava/lang/Object;

    .line 585
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->H:Ljava/lang/String;

    .line 586
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->I:Ljava/lang/Object;

    .line 602
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->J:Ljava/lang/String;

    .line 603
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->K:Ljava/lang/Object;

    .line 627
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->L:Ljava/lang/String;

    .line 628
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->M:Ljava/lang/Object;

    .line 653
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->N:Ljava/util/concurrent/ExecutorService;

    .line 654
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->O:Ljava/lang/Object;

    .line 665
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->P:Ljava/util/concurrent/ExecutorService;

    .line 666
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->Q:Ljava/lang/Object;

    .line 677
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->R:Ljava/util/concurrent/ExecutorService;

    .line 678
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->S:Ljava/lang/Object;

    .line 689
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->T:Ljava/util/concurrent/ExecutorService;

    .line 690
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->U:Ljava/lang/Object;

    .line 701
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->V:Ljava/util/concurrent/ExecutorService;

    .line 702
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->W:Ljava/lang/Object;

    .line 713
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->X:Ljava/util/concurrent/ExecutorService;

    .line 714
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->Y:Ljava/lang/Object;

    .line 724
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->Z:Ljava/util/concurrent/ExecutorService;

    .line 725
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->aa:Ljava/lang/Object;

    .line 736
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->ab:Ljava/util/concurrent/ExecutorService;

    .line 737
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->ac:Ljava/lang/Object;

    .line 748
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->ad:Ljava/util/concurrent/ExecutorService;

    .line 749
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->ae:Ljava/lang/Object;

    .line 760
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    .line 761
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->ag:Ljava/lang/Object;

    .line 1148
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    sput-object v2, Lcom/adobe/mobile/StaticMethods;->a:Ljava/util/Map;

    .line 1149
    sput v3, Lcom/adobe/mobile/StaticMethods;->b:I

    .line 1320
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->ah:Landroid/content/Context;

    .line 1407
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->ai:Ljava/lang/ref/WeakReference;

    .line 1408
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->aj:Ljava/lang/Object;

    const-string v0, "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E7F808182838485868788898A8B8C8D8E8F909192939495969798999A9B9C9D9E9FA0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF"

    .line 1466
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->ak:[C

    const-string v0, "0123456789abcdef"

    .line 1606
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->al:[C

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method protected static A()Z
    .locals 1

    .line 935
    sget-boolean v0, Lcom/adobe/mobile/StaticMethods;->h:Z

    return v0
.end method

.method protected static B()J
    .locals 4

    .line 1256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method protected static C()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    .line 1283
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    return-object v0

    .line 1293
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    if-nez v1, :cond_1

    return-object v0

    .line 1298
    :cond_1
    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-nez v1, :cond_2

    return-object v0

    .line 1303
    :cond_2
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5f

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "Config - Error getting application resources for default accepted language. (%s)"

    const/4 v3, 0x1

    .line 1285
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method protected static D()Landroid/content/SharedPreferences$Editor;
    .locals 2

    .line 1311
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 1313
    :cond_0
    new-instance v0, Lcom/adobe/mobile/StaticMethods$NullContextException;

    const-string v1, "Config - Unable to create an instance of a SharedPreferences Editor"

    invoke-direct {v0, v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected static E()Landroid/content/Context;
    .locals 2

    .line 1322
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->ah:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 1326
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->ah:Landroid/content/Context;

    return-object v0

    .line 1323
    :cond_0
    new-instance v0, Lcom/adobe/mobile/StaticMethods$NullContextException;

    const-string v1, "Config - No Application Context (Application context must be set prior to calling any library functions.)"

    invoke-direct {v0, v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected static F()J
    .locals 4

    .line 1340
    sget-wide v0, Lcom/adobe/mobile/o;->a:J

    .line 1341
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v0, 0x93a80

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2
.end method

.method protected static G()Landroid/app/Activity;
    .locals 3

    .line 1416
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->aj:Ljava/lang/Object;

    monitor-enter v0

    .line 1417
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ai:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ai:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1420
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ai:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    monitor-exit v0

    return-object v1

    .line 1418
    :cond_0
    new-instance v1, Lcom/adobe/mobile/StaticMethods$NullActivityException;

    const-string v2, "Message - No Current Activity (Messages must have a reference to the current activity to work properly)"

    invoke-direct {v1, v2}, Lcom/adobe/mobile/StaticMethods$NullActivityException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    .line 1421
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static H()I
    .locals 1

    .line 1443
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, -0x1

    return v0
.end method

.method protected static I()Ljava/lang/String;
    .locals 1

    .line 1598
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static J()Ljava/lang/String;
    .locals 6

    .line 236
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->n:Ljava/lang/Object;

    monitor-enter v0

    .line 237
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, ""

    .line 238
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 240
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 242
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 244
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 245
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v3, ""

    :goto_0
    sput-object v3, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const-string v3, "Analytics - ApplicationInfo was null"

    .line 248
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, ""

    .line 249
    sput-object v3, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v3, "Analytics - PackageManager was null"

    .line 253
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, ""

    .line 254
    sput-object v3, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "Config - Unable to get package to pull application name. (%s)"

    .line 260
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, ""

    .line 261
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception v3

    const-string v4, "Analytics - PackageManager couldn\'t find application name (%s)"

    .line 257
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, ""

    .line 258
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;

    .line 264
    :cond_3
    :goto_1
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->m:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 265
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private static K()Ljava/lang/String;
    .locals 8

    .line 856
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 857
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "https"

    goto :goto_0

    :cond_0
    const-string v1, "http"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "://"

    .line 858
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 859
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/id"

    .line 860
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->E()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 865
    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/bd;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v2, "Analytics ID - Sending Analytics ID call(%s)"

    const/4 v3, 0x1

    .line 868
    new-array v4, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 869
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x1f4

    const-string v4, "Analytics ID"

    const/4 v6, 0x0

    invoke-static {v0, v6, v2, v4}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    .line 874
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    new-instance v4, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v4, v0, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-direct {v2, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "id"

    .line 875
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    if-nez v1, :cond_2

    const-string v2, "Analytics ID - Unable to parse /id response(%s)"

    .line 880
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v2, "Analytics ID - Unable to decode /id response(%s)"

    .line 877
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_1
    move-object v0, v6

    :goto_2
    if-eqz v0, :cond_3

    .line 886
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x21

    if-eq v2, v3, :cond_5

    :cond_3
    if-eqz v1, :cond_4

    return-object v6

    .line 891
    :cond_4
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->L()Ljava/lang/String;

    move-result-object v0

    :cond_5
    return-object v0
.end method

.method private static L()Ljava/lang/String;
    .locals 5

    .line 898
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 899
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "^[89A-F]"

    .line 901
    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v2, "^[4-9A-F]"

    .line 902
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    const/16 v3, 0x10

    const/4 v4, 0x0

    .line 903
    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const/16 v4, 0x20

    .line 904
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 906
    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    const/4 v3, 0x7

    .line 907
    invoke-virtual {v2, v3}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x3

    .line 908
    invoke-virtual {v2, v3}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 910
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static M()Ljava/lang/String;
    .locals 1

    .line 1307
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method private static N()Ljava/util/Locale;
    .locals 4

    .line 1579
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    .line 1586
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    return-object v0

    .line 1589
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1591
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    return-object v0

    .line 1594
    :cond_1
    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v1, :cond_2

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_0

    :cond_2
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Config - Error getting application resources for device locale. (%s)"

    const/4 v2, 0x1

    .line 1581
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1582
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    return-object v0
.end method

.method protected static a()Landroid/content/SharedPreferences;
    .locals 4

    .line 195
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 196
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->i:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 197
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v1

    const-string v2, "APP_MEASUREMENT_CACHE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->i:Landroid/content/SharedPreferences;

    .line 198
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->i:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    const-string v1, "Config - No SharedPreferences available"

    .line 199
    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->i:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    .line 207
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->i:Landroid/content/SharedPreferences;

    monitor-exit v0

    return-object v1

    .line 204
    :cond_1
    new-instance v1, Lcom/adobe/mobile/StaticMethods$NullContextException;

    const-string v2, "Config - No SharedPreferences available"

    invoke-direct {v1, v2}, Lcom/adobe/mobile/StaticMethods$NullContextException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    .line 208
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static a(Ljava/util/Map;)Lcom/adobe/mobile/m;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/adobe/mobile/m;"
        }
    .end annotation

    .line 1106
    new-instance v0, Lcom/adobe/mobile/m;

    invoke-direct {v0}, Lcom/adobe/mobile/m;-><init>()V

    .line 1108
    invoke-static {p0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1109
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1111
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    const/16 v6, 0x2e

    .line 1113
    invoke-virtual {v2, v6, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    if-ltz v6, :cond_0

    .line 1114
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v6, 0x1

    goto :goto_1

    .line 1117
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1119
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0, v3, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/Object;Lcom/adobe/mobile/m;Ljava/util/List;I)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method protected static a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1002
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1004
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 1006
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1007
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1008
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 1011
    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1013
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method protected static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "UTF-8"

    .line 1026
    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 1028
    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    .line 1032
    sget-object v5, Lcom/adobe/mobile/StaticMethods;->d:[Z

    aget-byte v6, v2, v4

    and-int/lit16 v6, v6, 0xff

    aget-boolean v5, v5, v6

    if-eqz v5, :cond_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    if-ne v4, v3, :cond_2

    return-object p0

    .line 1043
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    array-length v5, v2

    shl-int/lit8 v5, v5, 0x1

    invoke-direct {p0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    if-lez v4, :cond_3

    .line 1047
    new-instance v5, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-direct {v5, v2, v1, v4, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_1
    if-ge v4, v3, :cond_4

    .line 1052
    sget-object v5, Lcom/adobe/mobile/StaticMethods;->c:[Ljava/lang/String;

    aget-byte v6, v2, v4

    and-int/lit16 v6, v6, 0xff

    aget-object v5, v5, v6

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1056
    :cond_4
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 1058
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UnsupportedEncodingException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method protected static a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1430
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1431
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "Unable to expand tokens (%s)"

    const/4 v1, 0x1

    .line 1435
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object p0
.end method

.method protected static a(Ljava/util/Date;)Ljava/lang/String;
    .locals 3

    .line 1602
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ssZZZ"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->N()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    if-eqz p0, :cond_0

    goto :goto_0

    .line 1603
    :cond_0
    new-instance p0, Ljava/util/Date;

    invoke-direct {p0}, Ljava/util/Date;-><init>()V

    :goto_0
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method protected static a(Lorg/json/JSONObject;)Ljava/util/HashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1354
    :cond_0
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 1355
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1356
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1357
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1359
    :try_start_0
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Problem parsing json data (%s)"

    const/4 v4, 0x1

    .line 1361
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method protected static a(Landroid/app/Activity;)V
    .locals 2

    .line 1410
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->aj:Ljava/lang/Object;

    monitor-enter v0

    .line 1411
    :try_start_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->ai:Ljava/lang/ref/WeakReference;

    .line 1412
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method protected static a(Landroid/content/Context;)V
    .locals 1

    if-eqz p0, :cond_1

    .line 1331
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1332
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lcom/adobe/mobile/StaticMethods;->ah:Landroid/content/Context;

    goto :goto_0

    .line 1334
    :cond_0
    sput-object p0, Lcom/adobe/mobile/StaticMethods;->ah:Landroid/content/Context;

    :cond_1
    :goto_0
    return-void
.end method

.method protected static a(Lcom/adobe/mobile/l$b;)V
    .locals 1

    .line 926
    sput-object p0, Lcom/adobe/mobile/StaticMethods;->g:Lcom/adobe/mobile/l$b;

    .line 927
    sget-object p0, Lcom/adobe/mobile/StaticMethods;->g:Lcom/adobe/mobile/l$b;

    sget-object v0, Lcom/adobe/mobile/l$b;->b:Lcom/adobe/mobile/l$b;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    sput-boolean p0, Lcom/adobe/mobile/StaticMethods;->h:Z

    return-void
.end method

.method protected static a(Ljava/lang/Long;)V
    .locals 5

    .line 1261
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p0, "Config - Lost config instance"

    .line 1263
    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p0, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1267
    :cond_0
    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->k()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 1272
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "ADBLastKnownTimestampKey"

    .line 1273
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1274
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "Config - Error while updating last known timestamp. (%s)"

    const/4 v2, 0x1

    .line 1276
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v1

    invoke-static {v0, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private static a(Ljava/lang/Object;Lcom/adobe/mobile/m;Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/adobe/mobile/m;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    if-eqz p1, :cond_5

    if-nez p2, :cond_0

    goto :goto_2

    .line 1233
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_1

    .line 1234
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    return-void

    .line 1239
    :cond_2
    new-instance v2, Lcom/adobe/mobile/m;

    invoke-direct {v2}, Lcom/adobe/mobile/m;-><init>()V

    .line 1240
    invoke-virtual {p1, v1}, Lcom/adobe/mobile/m;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1241
    invoke-virtual {p1, v1}, Lcom/adobe/mobile/m;->b(Ljava/lang/String;)Lcom/adobe/mobile/m;

    move-result-object v2

    :cond_3
    add-int/lit8 v0, v0, -0x1

    if-ne v0, p3, :cond_4

    .line 1246
    iput-object p0, v2, Lcom/adobe/mobile/m;->a:Ljava/lang/Object;

    .line 1247
    invoke-virtual {p1, v1, v2}, Lcom/adobe/mobile/m;->a(Ljava/lang/String;Lcom/adobe/mobile/m;)V

    goto :goto_1

    .line 1250
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/adobe/mobile/m;->a(Ljava/lang/String;Lcom/adobe/mobile/m;)V

    add-int/lit8 p3, p3, 0x1

    .line 1251
    invoke-static {p0, v2, p2, p3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/Object;Lcom/adobe/mobile/m;Ljava/util/List;I)V

    :goto_1
    return-void

    :cond_5
    :goto_2
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuilder;)V
    .locals 1

    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    .line 982
    instance-of v0, p1, Lcom/adobe/mobile/m;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_1

    .line 986
    :cond_0
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    return-void

    :cond_1
    const-string v0, "&"

    .line 990
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 991
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "="

    .line 992
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 994
    instance-of p0, p1, Ljava/util/List;

    if-eqz p0, :cond_2

    .line 995
    check-cast p1, Ljava/util/List;

    const-string p0, ","

    invoke-static {p1, p0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 997
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_3
    :goto_1
    return-void
.end method

.method protected static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 1067
    array-length v0, p1

    if-lez v0, :cond_0

    .line 1068
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Error : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "ADBMobile"

    .line 1069
    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string p1, "ADBMobile"

    .line 1072
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Error : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method protected static a(Ljava/util/Map;Ljava/lang/StringBuilder;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    .line 947
    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 948
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 955
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 958
    instance-of v2, v0, Lcom/adobe/mobile/m;

    if-eqz v2, :cond_4

    .line 959
    check-cast v0, Lcom/adobe/mobile/m;

    .line 961
    iget-object v2, v0, Lcom/adobe/mobile/m;->a:Ljava/lang/Object;

    if-eqz v2, :cond_3

    .line 962
    iget-object v2, v0, Lcom/adobe/mobile/m;->a:Ljava/lang/Object;

    invoke-static {v1, v2, p1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 966
    :cond_3
    iget-object v2, v0, Lcom/adobe/mobile/m;->b:Ljava/util/HashMap;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/adobe/mobile/m;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_1

    const-string v2, "&"

    .line 967
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 968
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "."

    .line 969
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 970
    iget-object v0, v0, Lcom/adobe/mobile/m;->b:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/util/Map;Ljava/lang/StringBuilder;)V

    const-string v0, "&."

    .line 971
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 975
    :cond_4
    invoke-static {v1, v0, p1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_5
    return-void
.end method

.method protected static a(Z)V
    .locals 4

    .line 396
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->w:Ljava/lang/Object;

    monitor-enter v0

    .line 399
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ADBMOBILE_KEY_PUSH_ENABLED"

    .line 400
    invoke-interface {v1, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 401
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 404
    sput-boolean p0, Lcom/adobe/mobile/StaticMethods;->v:Z

    .line 407
    invoke-static {p0}, Lcom/adobe/mobile/be;->a(Z)V
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    const-string v1, "Config - Unable to set pushEnabled shared preferences. (%s)"

    const/4 v2, 0x1

    .line 409
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    :goto_0
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method protected static b()Ljava/lang/String;
    .locals 8

    .line 214
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->l:Ljava/lang/Object;

    monitor-enter v0

    .line 215
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->k:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->J()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->J()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->d()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_2

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "(%d)"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v2, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ADOBEMOBILE_STOREDDEFAULTS_APPID"

    .line 222
    sget-object v5, Lcom/adobe/mobile/StaticMethods;->k:Ljava/lang/String;

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 223
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "Config - Unable to set Application ID in preferences (%s)"

    .line 226
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v3

    invoke-static {v2, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    :cond_3
    :goto_3
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->k:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 230
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 1156
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->a:Ljava/util/Map;

    monitor-enter v1

    .line 1157
    :try_start_0
    sget-object v2, Lcom/adobe/mobile/StaticMethods;->a:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1161
    monitor-exit v1

    return-object v2

    .line 1163
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_1
    const-string v3, "UTF-8"

    .line 1170
    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    .line 1171
    array-length v4, v3

    new-array v4, v4, [B

    .line 1177
    array-length v5, v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x2e

    if-ge v6, v5, :cond_4

    aget-byte v10, v3, v6

    if-ne v10, v9, :cond_2

    if-ne v8, v9, :cond_2

    goto :goto_1

    .line 1182
    :cond_2
    sget-object v9, Lcom/adobe/mobile/StaticMethods;->e:[Z

    and-int/lit16 v11, v10, 0xff

    aget-boolean v9, v9, v11

    if-eqz v9, :cond_3

    add-int/lit8 v8, v7, 0x1

    .line 1184
    aput-byte v10, v4, v7

    move v7, v8

    move v8, v10

    :cond_3
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_4
    if-nez v7, :cond_5

    return-object v0

    .line 1195
    :cond_5
    aget-byte v3, v4, v2

    if-ne v3, v9, :cond_6

    const/4 v3, 0x1

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    :goto_2
    add-int/lit8 v5, v7, -0x1

    .line 1196
    aget-byte v5, v4, v5

    if-ne v5, v9, :cond_7

    const/4 v5, 0x1

    goto :goto_3

    :cond_7
    const/4 v5, 0x0

    :goto_3
    sub-int/2addr v7, v5

    sub-int/2addr v7, v3

    if-gtz v7, :cond_8

    return-object v0

    .line 1205
    :cond_8
    new-instance v5, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-direct {v5, v4, v3, v7, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1212
    sget-object v3, Lcom/adobe/mobile/StaticMethods;->a:Ljava/util/Map;

    monitor-enter v3

    .line 1214
    :try_start_2
    sget v0, Lcom/adobe/mobile/StaticMethods;->b:I

    const/16 v4, 0xfa

    if-le v0, v4, :cond_9

    .line 1216
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1217
    sput v2, Lcom/adobe/mobile/StaticMethods;->b:I

    .line 1221
    :cond_9
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->a:Ljava/util/Map;

    invoke-interface {v0, p0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1222
    sget p0, Lcom/adobe/mobile/StaticMethods;->b:I

    add-int/2addr p0, v1

    sput p0, Lcom/adobe/mobile/StaticMethods;->b:I

    .line 1223
    monitor-exit v3

    return-object v5

    :catchall_0
    move-exception p0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0

    :catch_0
    move-exception p0

    const-string v3, "Analytics - Unable to clean context data key (%s)"

    .line 1207
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :catchall_1
    move-exception p0

    .line 1163
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p0
.end method

.method protected static b(Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 1128
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-object p0

    .line 1131
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1133
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1134
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1136
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method protected static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 1077
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 1078
    array-length v0, p1

    if-lez v0, :cond_0

    .line 1079
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Warning : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "ADBMobile"

    .line 1080
    invoke-static {p1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string p1, "ADBMobile"

    .line 1083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Warning : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method protected static c()Ljava/lang/String;
    .locals 5

    .line 271
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->p:Ljava/lang/Object;

    monitor-enter v0

    .line 272
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, ""

    .line 273
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 275
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 277
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 279
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v3, ""

    :goto_0
    sput-object v3, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const-string v3, "Analytics - PackageInfo was null"

    .line 282
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, ""

    .line 283
    sput-object v3, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v3, "Analytics - PackageManager was null"

    .line 287
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, ""

    .line 288
    sput-object v3, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "Config - Unable to get package to pull application version. (%s)"

    .line 294
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, ""

    .line 295
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception v3

    const-string v4, "Analytics - PackageManager couldn\'t find application version (%s)"

    .line 291
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, ""

    .line 292
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;

    .line 298
    :cond_3
    :goto_1
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->o:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 299
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 1469
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    const-string v3, "UTF-8"

    .line 1475
    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1481
    array-length v0, p0

    shl-int/lit8 v3, v0, 0x1

    .line 1482
    new-array v3, v3, [C

    const/4 v4, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    add-int/lit8 v5, v1, 0x1

    .line 1487
    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/2addr v1, v2

    add-int/lit8 v6, v4, 0x1

    .line 1488
    sget-object v7, Lcom/adobe/mobile/StaticMethods;->ak:[C

    add-int/lit8 v8, v1, 0x1

    aget-char v1, v7, v1

    aput-char v1, v3, v4

    add-int/lit8 v4, v6, 0x1

    .line 1489
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ak:[C

    aget-char v1, v1, v8

    aput-char v1, v3, v6

    move v1, v5

    goto :goto_0

    .line 1491
    :cond_1
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v3}, Ljava/lang/String;-><init>([C)V

    return-object p0

    :catch_0
    move-exception p0

    const-string v3, "Failed to get hex from string (%s)"

    .line 1478
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v1

    invoke-static {v3, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :cond_2
    :goto_1
    return-object v0
.end method

.method protected static varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 1089
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 1090
    array-length v0, p1

    if-lez v0, :cond_0

    .line 1091
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Debug : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "ADBMobile"

    .line 1092
    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string p1, "ADBMobile"

    .line 1095
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Debug : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method protected static d()I
    .locals 5

    .line 305
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->r:Ljava/lang/Object;

    monitor-enter v0

    .line 306
    :try_start_0
    sget v1, Lcom/adobe/mobile/StaticMethods;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 308
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 310
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 312
    iget v4, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    if-lez v4, :cond_0

    iget v3, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    sput v3, Lcom/adobe/mobile/StaticMethods;->q:I

    goto :goto_1

    :cond_1
    const-string v3, "Analytics - PackageInfo was null"

    .line 315
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    sput v2, Lcom/adobe/mobile/StaticMethods;->q:I

    goto :goto_1

    :cond_2
    const-string v3, "Analytics - PackageManager was null"

    .line 320
    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    sput v2, Lcom/adobe/mobile/StaticMethods;->q:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "Config - Unable to get package to pull application version code. (%s)"

    .line 328
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    sput v2, Lcom/adobe/mobile/StaticMethods;->q:I

    goto :goto_1

    :catch_1
    move-exception v3

    const-string v4, "Analytics - PackageManager couldn\'t find application version code (%s)"

    .line 325
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 326
    sput v2, Lcom/adobe/mobile/StaticMethods;->q:I

    .line 332
    :cond_3
    :goto_1
    sget v1, Lcom/adobe/mobile/StaticMethods;->q:I

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 333
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 1495
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    goto :goto_2

    .line 1499
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 1500
    div-int/lit8 v2, v1, 0x2

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_1

    .line 1502
    div-int/lit8 v5, v4, 0x2

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    shl-int/lit8 v6, v6, 0x4

    add-int/lit8 v8, v4, 0x1

    .line 1503
    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v7

    add-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v2, v5

    add-int/lit8 v4, v4, 0x2

    goto :goto_0

    .line 1508
    :cond_1
    :try_start_0
    new-instance p0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {p0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    const-string v1, "Failed to get string from hex (%s)"

    const/4 v2, 0x1

    .line 1511
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object p0, v0

    :goto_1
    return-object p0

    :cond_2
    :goto_2
    return-object v0
.end method

.method protected static e()Ljava/lang/String;
    .locals 5

    .line 339
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->t:Ljava/lang/Object;

    monitor-enter v0

    .line 340
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->s:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 342
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "APP_MEASUREMENT_VISITOR_ID"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->s:Ljava/lang/String;
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "Config - Unable to pull visitorID from shared preferences. (%s)"

    const/4 v3, 0x1

    .line 344
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    :cond_0
    :goto_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->s:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 349
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method static e(Ljava/lang/String;)Z
    .locals 0

    if-eqz p0, :cond_1

    .line 1645
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method protected static f()V
    .locals 5

    .line 368
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->u:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 369
    :try_start_0
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->s:Ljava/lang/String;

    .line 370
    invoke-static {v1}, Lcom/adobe/mobile/be;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "APP_MEASUREMENT_VISITOR_ID"

    .line 373
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 374
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "Config - Error purging visitorID. (%s)"

    const/4 v3, 0x1

    .line 376
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 378
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private static f(Ljava/lang/String;)V
    .locals 5

    if-nez p0, :cond_0

    return-void

    .line 827
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->E()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 833
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ADOBEMOBILE_STOREDDEFAULTS_AID_SYNCED"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Visitor ID - Null context when attempting to determine visitor ID sync status (%s)"

    .line 835
    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_2

    .line 839
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "AVID"

    .line 840
    invoke-virtual {v2, v3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 841
    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object p0

    invoke-virtual {p0, v2}, Lcom/adobe/mobile/bd;->a(Ljava/util/Map;)V

    .line 843
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    const-string v2, "ADOBEMOBILE_STOREDDEFAULTS_AID_SYNCED"

    .line 844
    invoke-interface {p0, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 845
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    const-string v2, "Visitor ID - Null context when attempting to persist visitor ID sync status (%s)"

    .line 847
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v0, v1

    invoke-static {v2, v0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected static g()Ljava/lang/String;
    .locals 2

    .line 417
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->y:Ljava/lang/Object;

    monitor-enter v0

    .line 418
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->x:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 419
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static h()V
    .locals 5

    .line 454
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->y:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 455
    :try_start_0
    invoke-static {v1}, Lcom/adobe/mobile/StaticMethods;->a(Z)V

    const/4 v2, 0x0

    .line 456
    sput-object v2, Lcom/adobe/mobile/StaticMethods;->x:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ADBMOBILE_KEY_PUSH_TOKEN"

    .line 459
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 460
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    const-string v3, "Config - Failed to remove push identifier from shared preferences. (%s)"

    const/4 v4, 0x1

    .line 462
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static i()Ljava/lang/String;
    .locals 2

    .line 507
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->A:Ljava/lang/Object;

    monitor-enter v0

    .line 508
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->z:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 509
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static j()V
    .locals 2

    .line 530
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->A:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 531
    :try_start_0
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->z:Ljava/lang/String;

    .line 532
    invoke-static {v1}, Lcom/adobe/mobile/be;->b(Ljava/lang/String;)V

    .line 533
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static k()Ljava/lang/String;
    .locals 3

    .line 539
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->C:Ljava/lang/Object;

    monitor-enter v0

    .line 540
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->B:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 541
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mozilla/5.0 (Linux; U; Android "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->M()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 542
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Build/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->B:Ljava/lang/String;

    .line 545
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->B:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 546
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static l()Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 552
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->E:Ljava/lang/Object;

    monitor-enter v0

    .line 553
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    if-nez v1, :cond_1

    .line 554
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    .line 555
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    const-string v2, "a.DeviceName"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    const-string v2, "a.Resolution"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    const-string v2, "a.OSVersion"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    const-string v2, "a.CarrierName"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    const-string v2, "a.AppID"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    const-string v2, "a.RunMode"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "Extension"

    goto :goto_0

    :cond_0
    const-string v3, "Application"

    :goto_0
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    :cond_1
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->D:Ljava/util/HashMap;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 564
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static m()Ljava/lang/String;
    .locals 5

    .line 571
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->G:Ljava/lang/Object;

    monitor-enter v0

    .line 572
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->F:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 574
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 575
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->F:Ljava/lang/String;
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "Config - Error getting device resolution. (%s)"

    const/4 v3, 0x1

    .line 577
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 581
    :cond_0
    :goto_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->F:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 582
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static n()Ljava/lang/String;
    .locals 5

    .line 588
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->I:Ljava/lang/Object;

    monitor-enter v0

    .line 589
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->H:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 591
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v1

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 592
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->H:Ljava/lang/String;
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "Config - Error getting device carrier. (%s)"

    const/4 v3, 0x1

    .line 594
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 598
    :cond_0
    :goto_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->H:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 599
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static o()Ljava/lang/String;
    .locals 5

    .line 605
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->K:Ljava/lang/Object;

    monitor-enter v0

    .line 606
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->J:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 607
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Android "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->M()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->J:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 610
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ADOBEMOBILE_STOREDDEFAULTS_OS"

    .line 611
    sget-object v3, Lcom/adobe/mobile/StaticMethods;->J:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 612
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "Config - Unable to set OS version in preferences (%s)"

    const/4 v3, 0x1

    .line 615
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    :cond_0
    :goto_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->J:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 620
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static p()Ljava/lang/String;
    .locals 11

    .line 630
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->M:Ljava/lang/Object;

    monitor-enter v0

    .line 631
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->L:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 632
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 633
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 634
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 635
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "00/00/0000 00:00:00 0 "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 636
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v6

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v9

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x3c

    const/16 v10, 0xc

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    add-int/2addr v3, v10

    mul-int/lit8 v3, v3, 0x3c

    const/16 v10, 0xd

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    add-int/2addr v3, v10

    mul-int/lit16 v3, v3, 0x3e8

    const/16 v10, 0xe

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int v10, v3, v2

    invoke-virtual/range {v4 .. v10}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v2

    const v3, 0xea60

    div-int/2addr v2, v3

    mul-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->L:Ljava/lang/String;

    .line 639
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->L:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 640
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static q()Ljava/io/File;
    .locals 4

    .line 645
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->E()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Config - Error getting cache directory. (%s)"

    const/4 v2, 0x1

    .line 647
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected static r()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 692
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->U:Ljava/lang/Object;

    monitor-enter v0

    .line 693
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->T:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 694
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->T:Ljava/util/concurrent/ExecutorService;

    .line 697
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->T:Ljava/util/concurrent/ExecutorService;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 698
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static s()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 704
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->W:Ljava/lang/Object;

    monitor-enter v0

    .line 705
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->V:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 706
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->V:Ljava/util/concurrent/ExecutorService;

    .line 709
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->V:Ljava/util/concurrent/ExecutorService;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 710
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static t()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 716
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->Y:Ljava/lang/Object;

    monitor-enter v0

    .line 717
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->X:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 718
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->X:Ljava/util/concurrent/ExecutorService;

    .line 720
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->X:Ljava/util/concurrent/ExecutorService;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 721
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static u()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 727
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->aa:Ljava/lang/Object;

    monitor-enter v0

    .line 728
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->Z:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 729
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->Z:Ljava/util/concurrent/ExecutorService;

    .line 732
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->Z:Ljava/util/concurrent/ExecutorService;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 733
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static v()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 739
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->ac:Ljava/lang/Object;

    monitor-enter v0

    .line 740
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ab:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 741
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->ab:Ljava/util/concurrent/ExecutorService;

    .line 744
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ab:Ljava/util/concurrent/ExecutorService;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 745
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static w()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 751
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->ae:Ljava/lang/Object;

    monitor-enter v0

    .line 752
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ad:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 753
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->ad:Ljava/util/concurrent/ExecutorService;

    .line 756
    :cond_0
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->ad:Ljava/util/concurrent/ExecutorService;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 757
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static x()V
    .locals 3

    .line 763
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->ag:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 764
    :try_start_0
    sput-object v1, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 768
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ADOBEMOBILE_STOREDDEFAULTS_AID"

    .line 769
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "ADOBEMOBILE_STOREDDEFAULTS_IGNORE_AID"

    .line 770
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "ADOBEMOBILE_STOREDDEFAULTS_AID_SYNCED"

    .line 771
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 772
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    const-string v1, "Config - Failed to purge AID (application context is null)"

    const/4 v2, 0x0

    .line 774
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 776
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected static y()Ljava/lang/String;
    .locals 7

    .line 780
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 781
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v0

    sget-object v2, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    if-ne v0, v2, :cond_0

    goto/16 :goto_2

    .line 785
    :cond_0
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->ag:Ljava/lang/Object;

    monitor-enter v0

    .line 786
    :try_start_0
    sget-object v2, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_5

    :cond_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 788
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "ADOBEMOBILE_STOREDDEFAULTS_IGNORE_AID"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 789
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "ADOBEMOBILE_STOREDDEFAULTS_AID"

    invoke-interface {v5, v6, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 794
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    if-eqz v1, :cond_3

    :cond_2
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/ao;->E()Z

    move-result v1

    if-nez v1, :cond_5

    if-eqz v4, :cond_5

    .line 795
    :cond_3
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->K()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    .line 796
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 798
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    if-eqz v4, :cond_4

    const-string v4, "ADOBEMOBILE_STOREDDEFAULTS_AID"

    .line 800
    sget-object v5, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "ADOBEMOBILE_STOREDDEFAULTS_IGNORE_AID"

    .line 801
    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_4
    const-string v4, "ADOBEMOBILE_STOREDDEFAULTS_IGNORE_AID"

    .line 803
    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 806
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 808
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    invoke-static {v1}, Lcom/adobe/mobile/StaticMethods;->f(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    const-string v4, "Config - Error getting AID. (%s)"

    .line 811
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v4, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 814
    :cond_5
    :goto_1
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 816
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->af:Ljava/lang/String;

    return-object v0

    :catchall_0
    move-exception v1

    .line 814
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :cond_6
    :goto_2
    return-object v1
.end method

.method protected static z()Z
    .locals 1

    .line 921
    sget-boolean v0, Lcom/adobe/mobile/StaticMethods;->f:Z

    return v0
.end method
