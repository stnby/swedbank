.class Lcom/adobe/mobile/n$1$1;
.super Ljava/lang/Object;
.source "FloatingButton.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/n$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lcom/adobe/mobile/n$1;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/n$1;II)V
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iput p2, p0, Lcom/adobe/mobile/n$1$1;->a:I

    iput p3, p0, Lcom/adobe/mobile/n$1$1;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .line 179
    iget-object v0, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v0, v0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-virtual {v0, p0}, Lcom/adobe/mobile/n;->a(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 180
    iget-object v0, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v0, v0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-static {v0}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v0, v0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-static {v0}, Lcom/adobe/mobile/n;->b(Lcom/adobe/mobile/n;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v0, v0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    iget-object v1, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v1, v1, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    iget v2, p0, Lcom/adobe/mobile/n$1$1;->a:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v3, v3, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-static {v3}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;)F

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;FF)F

    move-result v1

    invoke-static {v0, v1}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;F)F

    .line 183
    iget-object v0, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v0, v0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    iget-object v1, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v1, v1, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    iget v2, p0, Lcom/adobe/mobile/n$1$1;->b:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v3, v3, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-static {v3}, Lcom/adobe/mobile/n;->b(Lcom/adobe/mobile/n;)F

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/adobe/mobile/n;->b(Lcom/adobe/mobile/n;FF)F

    move-result v1

    invoke-static {v0, v1}, Lcom/adobe/mobile/n;->b(Lcom/adobe/mobile/n;F)F

    .line 184
    iget-object v0, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v0, v0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    iget-object v1, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v1, v1, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-static {v1}, Lcom/adobe/mobile/n;->a(Lcom/adobe/mobile/n;)F

    move-result v1

    iget-object v2, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v2, v2, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-static {v2}, Lcom/adobe/mobile/n;->b(Lcom/adobe/mobile/n;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/adobe/mobile/n;->a(FF)V

    goto :goto_0

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v0, v0, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    iget v1, p0, Lcom/adobe/mobile/n$1$1;->a:I

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v2, v2, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-virtual {v2}, Lcom/adobe/mobile/n;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/adobe/mobile/n$1$1;->b:I

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/adobe/mobile/n$1$1;->c:Lcom/adobe/mobile/n$1;

    iget-object v3, v3, Lcom/adobe/mobile/n$1;->d:Lcom/adobe/mobile/n;

    invoke-virtual {v3}, Lcom/adobe/mobile/n;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/adobe/mobile/n;->a(FF)V

    :goto_0
    return-void
.end method
