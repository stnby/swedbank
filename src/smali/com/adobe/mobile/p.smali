.class abstract Lcom/adobe/mobile/p;
.super Ljava/lang/Object;
.source "Message.java"


# static fields
.field private static final m:Ljava/lang/Long;

.field private static final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field private static o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final p:Ljava/lang/Object;

.field private static final q:[Z

.field private static final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/adobe/mobile/an$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Lcom/adobe/mobile/an$a;

.field protected c:Ljava/util/Date;

.field protected d:Ljava/util/Date;

.field protected e:Z

.field protected f:Z

.field protected g:I

.field protected h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adobe/mobile/u;",
            ">;"
        }
    .end annotation
.end field

.field protected j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adobe/mobile/u;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/security/SecureRandom;

.field private l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 83
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/p;->m:Ljava/lang/Long;

    .line 117
    new-instance v0, Lcom/adobe/mobile/p$1;

    invoke-direct {v0}, Lcom/adobe/mobile/p$1;-><init>()V

    sput-object v0, Lcom/adobe/mobile/p;->n:Ljava/util/Map;

    .line 262
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/p;->p:Ljava/lang/Object;

    const/16 v0, 0x100

    .line 558
    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lcom/adobe/mobile/p;->q:[Z

    .line 609
    new-instance v0, Lcom/adobe/mobile/p$2;

    invoke-direct {v0}, Lcom/adobe/mobile/p$2;-><init>()V

    sput-object v0, Lcom/adobe/mobile/p;->r:Ljava/util/Map;

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method constructor <init>()V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/p;->k:Ljava/security/SecureRandom;

    return-void
.end method

.method protected static a(Lorg/json/JSONObject;)Lcom/adobe/mobile/p;
    .locals 5

    const-string v0, ""

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    const-string v4, "template"

    .line 133
    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :try_start_1
    sget-object v0, Lcom/adobe/mobile/p;->n:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 135
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adobe/mobile/p;

    .line 136
    invoke-virtual {v0, p0}, Lcom/adobe/mobile/p;->b(Lorg/json/JSONObject;)Z

    move-result p0
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v3

    :goto_0
    return-object v0

    :catch_0
    move-exception p0

    const-string v0, "Messages - unable to create instance of message (%s)"

    .line 151
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v3

    :catch_1
    move-exception p0

    const-string v0, "Messages - unable to create instance of message (%s)"

    .line 147
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v3

    :catch_2
    move-object v4, v0

    :catch_3
    const-string p0, "Messages - invalid template specified for message (%s)"

    .line 143
    new-array v0, v1, [Ljava/lang/Object;

    aput-object v4, v0, v2

    invoke-static {p0, v0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v3

    :catch_4
    const-string p0, "Messages - template is required for in-app message"

    .line 139
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p0, v0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v3
.end method

.method private a(Ljava/util/Map;)Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 450
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "%sdkver%"

    const-string v2, "4.17.2-AN"

    .line 452
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "%cachebust%"

    .line 453
    iget-object v2, p0, Lcom/adobe/mobile/p;->k:Ljava/security/SecureRandom;

    const v3, 0x5f5e100

    invoke-virtual {v2, v3}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "%adid%"

    .line 454
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "%timestampu%"

    .line 455
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "%timestampz%"

    .line 456
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "%pushid%"

    .line 457
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "%mcid%"

    .line 458
    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/bd;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/adobe/mobile/bd;->a()Lcom/adobe/mobile/bd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/bd;->d()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 461
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 462
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-nez v4, :cond_1

    goto :goto_1

    .line 467
    :cond_1
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_2

    const-string v5, ""

    goto :goto_2

    .line 468
    :cond_2
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 470
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v5}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 472
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    const-string p1, "&"

    .line 477
    invoke-static {p1, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "%all_url%"

    .line 478
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1, v2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 482
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    .line 484
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    const-string v1, "%all_json%"

    .line 485
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    const-string v1, "Data Callback - unable to create json string for vars:  (%s)"

    const/4 v2, 0x1

    .line 488
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    :goto_3
    return-object v0
.end method

.method private b(Ljava/util/Map;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 638
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 639
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "UTF-8"

    .line 579
    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    .line 582
    array-length v2, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-byte v4, p1, v3

    .line 584
    sget-object v5, Lcom/adobe/mobile/p;->q:[Z

    and-int/lit16 v4, v4, 0xff

    aget-boolean v4, v5, v4
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v4, :cond_0

    return v1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v0

    :catch_0
    move-exception p1

    const-string v2, "Data Callback - Unable to validate token (%s)"

    .line 590
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    invoke-static {v2, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1
.end method

.method private static c(Ljava/lang/String;)Lcom/adobe/mobile/an$a;
    .locals 1

    .line 618
    sget-object v0, Lcom/adobe/mobile/p;->r:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/adobe/mobile/an$a;

    return-object p0
.end method

.method private d(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 622
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 624
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 625
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object p1

    .line 626
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 627
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 628
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "Messages- Unable to deserialize blacklist(%s)"

    const/4 v2, 0x1

    .line 631
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_7

    .line 510
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_5

    .line 515
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 517
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_6

    .line 522
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x7b

    const/4 v5, 0x1

    if-ne v3, v4, :cond_5

    add-int/lit8 v3, v2, 0x1

    :goto_1
    if-ge v3, v1, :cond_2

    .line 526
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v6, 0x7d

    if-ne v4, v6, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    if-ne v3, v1, :cond_3

    goto :goto_4

    :cond_3
    add-int/lit8 v4, v3, 0x1

    .line 537
    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 540
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/adobe/mobile/p;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    goto :goto_3

    .line 545
    :cond_4
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    :cond_5
    :goto_3
    add-int/2addr v2, v5

    goto :goto_0

    :cond_6
    :goto_4
    return-object v0

    :cond_7
    :goto_5
    const/4 p1, 0x0

    return-object p1
.end method

.method protected a(Ljava/util/ArrayList;Z)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 495
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 497
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 498
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 499
    iget-object v3, p0, Lcom/adobe/mobile/p;->l:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, ""

    goto :goto_1

    .line 500
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    if-eqz p2, :cond_1

    .line 502
    invoke-static {v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 503
    :cond_1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method protected a()V
    .locals 6

    .line 264
    sget-object v0, Lcom/adobe/mobile/p;->p:Ljava/lang/Object;

    monitor-enter v0

    .line 265
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/adobe/mobile/p;->d()Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    .line 269
    :cond_0
    sget-object v1, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    invoke-virtual {v3}, Lcom/adobe/mobile/an$a;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Messages - adding message \"%s\" to blacklist"

    const/4 v2, 0x1

    .line 270
    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "messagesBlackList"

    .line 274
    sget-object v4, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    invoke-direct {p0, v4}, Lcom/adobe/mobile/p;->b(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 275
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v3, "Messages - Error persisting blacklist map (%s)."

    .line 278
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-static {v3, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .line 328
    iget-boolean v0, p0, Lcom/adobe/mobile/p;->f:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/adobe/mobile/p;->g:I

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->H()I

    move-result v2

    if-eq v0, v2, :cond_0

    instance-of v0, p0, Lcom/adobe/mobile/q;

    if-eqz v0, :cond_0

    return v1

    .line 333
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/an;->f()Lcom/adobe/mobile/p;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 334
    instance-of v0, p0, Lcom/adobe/mobile/t;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/adobe/mobile/al;

    if-nez v0, :cond_1

    return v2

    :cond_1
    if-eqz p2, :cond_2

    .line 339
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :goto_0
    if-eqz p1, :cond_3

    .line 342
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 346
    :cond_3
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v3

    if-gtz v3, :cond_4

    return v2

    .line 350
    :cond_4
    invoke-direct {p0, v0}, Lcom/adobe/mobile/p;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    if-eqz p3, :cond_5

    .line 353
    invoke-virtual {v0, p3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 356
    :cond_5
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v3, p0, Lcom/adobe/mobile/p;->l:Ljava/util/HashMap;

    .line 359
    invoke-virtual {p0}, Lcom/adobe/mobile/p;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    return v2

    .line 364
    :cond_6
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->G()Z

    move-result v0

    if-nez v0, :cond_7

    .line 365
    iget-boolean v0, p0, Lcom/adobe/mobile/p;->e:Z

    if-nez v0, :cond_7

    return v2

    .line 371
    :cond_7
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 372
    iget-object v3, p0, Lcom/adobe/mobile/p;->c:Ljava/util/Date;

    invoke-virtual {v0, v3}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_8

    return v2

    .line 376
    :cond_8
    iget-object v3, p0, Lcom/adobe/mobile/p;->d:Ljava/util/Date;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/adobe/mobile/p;->d:Ljava/util/Date;

    invoke-virtual {v0, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_9

    return v2

    .line 381
    :cond_9
    iget-object v0, p0, Lcom/adobe/mobile/p;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/adobe/mobile/u;

    .line 382
    new-array v4, v1, [Ljava/util/Map;

    aput-object p3, v4, v2

    invoke-virtual {v3, v4}, Lcom/adobe/mobile/u;->a([Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_a

    return v2

    .line 388
    :cond_b
    invoke-static {p2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p2

    .line 390
    iget-object p3, p0, Lcom/adobe/mobile/p;->j:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_c
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adobe/mobile/u;

    const/4 v3, 0x2

    .line 391
    new-array v3, v3, [Ljava/util/Map;

    aput-object p1, v3, v2

    aput-object p2, v3, v1

    invoke-virtual {v0, v3}, Lcom/adobe/mobile/u;->a([Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_c

    return v2

    :cond_d
    return v1
.end method

.method protected b()V
    .locals 6

    .line 284
    invoke-virtual {p0}, Lcom/adobe/mobile/p;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    sget-object v0, Lcom/adobe/mobile/p;->p:Ljava/lang/Object;

    monitor-enter v0

    .line 286
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Messages - removing message \"%s\" from blacklist"

    const/4 v2, 0x1

    .line 287
    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "messagesBlackList"

    .line 291
    sget-object v4, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    invoke-direct {p0, v4}, Lcom/adobe/mobile/p;->b(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 292
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v3, "Messages - Error persisting blacklist map (%s)."

    .line 295
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-static {v3, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :goto_1
    return-void
.end method

.method protected b(Lorg/json/JSONObject;)Z
    .locals 8

    const/4 v0, 0x0

    if-eqz p1, :cond_7

    .line 160
    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_6

    :cond_0
    :try_start_0
    const-string v1, "messageId"

    .line 166
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    .line 167
    iget-object v1, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    const-string p1, "Messages - Unable to create message, messageId is empty"

    .line 168
    new-array v1, v0, [Ljava/lang/Object;

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_6

    return v0

    :cond_1
    const/4 v1, 0x1

    :try_start_1
    const-string v2, "showRule"

    .line 178
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 179
    invoke-static {v2}, Lcom/adobe/mobile/p;->c(Ljava/lang/String;)Lcom/adobe/mobile/an$a;

    move-result-object v3

    iput-object v3, p0, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    .line 180
    iget-object v3, p0, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    const/4 v4, 0x2

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    sget-object v5, Lcom/adobe/mobile/an$a;->a:Lcom/adobe/mobile/an$a;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5

    if-ne v3, v5, :cond_2

    goto/16 :goto_5

    :cond_2
    const-wide/16 v2, 0x3e8

    :try_start_2
    const-string v5, "startDate"

    .line 193
    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 194
    new-instance v7, Ljava/util/Date;

    mul-long v5, v5, v2

    invoke-direct {v7, v5, v6}, Ljava/util/Date;-><init>(J)V

    iput-object v7, p0, Lcom/adobe/mobile/p;->c:Ljava/util/Date;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    const-string v5, "Messages - Tried to read startDate from message \"%s\" but none found. Using default value"

    .line 197
    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v7, v6, v0

    invoke-static {v5, v6}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    new-instance v5, Ljava/util/Date;

    sget-object v6, Lcom/adobe/mobile/p;->m:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    mul-long v6, v6, v2

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    iput-object v5, p0, Lcom/adobe/mobile/p;->c:Ljava/util/Date;

    :goto_0
    :try_start_3
    const-string v5, "endDate"

    .line 203
    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 204
    new-instance v7, Ljava/util/Date;

    mul-long v5, v5, v2

    invoke-direct {v7, v5, v6}, Ljava/util/Date;-><init>(J)V

    iput-object v7, p0, Lcom/adobe/mobile/p;->d:Ljava/util/Date;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    const-string v2, "Messages - Tried to read endDate from message \"%s\" but none found. Using default value"

    .line 207
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v5, v3, v0

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    :try_start_4
    const-string v2, "showOffline"

    .line 212
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/adobe/mobile/p;->e:Z
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    const-string v2, "Messages - Tried to read showOffline from message \"%s\" but none found. Using default value"

    .line 215
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v5, v3, v0

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    iput-boolean v0, p0, Lcom/adobe/mobile/p;->e:Z

    .line 220
    :goto_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/adobe/mobile/p;->i:Ljava/util/ArrayList;

    :try_start_5
    const-string v2, "audiences"

    .line 222
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 223
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v3, :cond_3

    .line 225
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 226
    iget-object v7, p0, Lcom/adobe/mobile/p;->i:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/adobe/mobile/u;->a(Lorg/json/JSONObject;)Lcom/adobe/mobile/u;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_3

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :catch_3
    move-exception v2

    const-string v3, "Messages - failed to read audience for message \"%s\", error: %s"

    .line 230
    new-array v5, v4, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/adobe/mobile/p;->j:Ljava/util/ArrayList;

    :try_start_6
    const-string v2, "triggers"

    .line 236
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    .line 237
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v2, :cond_4

    .line 239
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 240
    iget-object v6, p0, Lcom/adobe/mobile/p;->j:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/adobe/mobile/u;->a(Lorg/json/JSONObject;)Lcom/adobe/mobile/u;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_4

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :catch_4
    move-exception p1

    const-string v2, "Messages - failed to read trigger for message \"%s\", error: %s"

    .line 244
    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v4, v3, v0

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v1

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    :cond_4
    iget-object p1, p0, Lcom/adobe/mobile/p;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-gtz p1, :cond_5

    const-string p1, "Messages - Unable to load message \"%s\" - at least one valid trigger is required for a message"

    .line 249
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    .line 253
    :cond_5
    iput-boolean v0, p0, Lcom/adobe/mobile/p;->f:Z

    return v1

    :cond_6
    :goto_5
    :try_start_7
    const-string p1, "Messages - Unable to create message \"%s\", showRule not valid (%s)"

    .line 182
    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v4, v3, v0

    aput-object v2, v3, v1

    invoke-static {p1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_5

    return v0

    :catch_5
    const-string p1, "Messages - Unable to create message \"%s\", showRule is required"

    .line 187
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :catch_6
    const-string p1, "Messages - Unable to create message, messageId is required"

    .line 173
    new-array v1, v0, [Ljava/lang/Object;

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :cond_7
    :goto_6
    return v0
.end method

.method protected c()Z
    .locals 3

    .line 302
    sget-object v0, Lcom/adobe/mobile/p;->p:Ljava/lang/Object;

    monitor-enter v0

    .line 303
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/adobe/mobile/p;->d()Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    .line 307
    :cond_0
    sget-object v1, Lcom/adobe/mobile/p;->o:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 308
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected d()Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 313
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "messagesBlackList"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0

    .line 318
    :cond_0
    invoke-direct {p0, v0}, Lcom/adobe/mobile/p;->d(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Messaging - Unable to get shared preferences while loading blacklist. (%s)"

    const/4 v2, 0x1

    .line 320
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method protected e()V
    .locals 4

    .line 400
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "a.message.id"

    .line 401
    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "a.message.triggered"

    const/4 v2, 0x1

    .line 402
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "In-App Message"

    .line 403
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v2

    invoke-static {v1, v0, v2, v3}, Lcom/adobe/mobile/e;->a(Ljava/lang/String;Ljava/util/Map;J)V

    return-void
.end method

.method protected f()V
    .locals 2

    .line 408
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->H()I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/p;->g:I

    .line 411
    iget-object v0, p0, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    sget-object v1, Lcom/adobe/mobile/an$a;->c:Lcom/adobe/mobile/an$a;

    if-ne v0, v1, :cond_0

    .line 412
    invoke-virtual {p0}, Lcom/adobe/mobile/p;->a()V

    .line 416
    :cond_0
    instance-of v0, p0, Lcom/adobe/mobile/q;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/adobe/mobile/r;

    if-eqz v0, :cond_2

    .line 417
    :cond_1
    invoke-static {p0}, Lcom/adobe/mobile/an;->a(Lcom/adobe/mobile/p;)V

    :cond_2
    return-void
.end method

.method protected g()V
    .locals 4

    .line 425
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "a.message.id"

    .line 426
    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "a.message.viewed"

    const/4 v2, 0x1

    .line 427
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "In-App Message"

    .line 428
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v2

    invoke-static {v1, v0, v2, v3}, Lcom/adobe/mobile/e;->a(Ljava/lang/String;Ljava/util/Map;J)V

    const/4 v0, 0x0

    .line 431
    invoke-static {v0}, Lcom/adobe/mobile/an;->a(Lcom/adobe/mobile/p;)V

    return-void
.end method

.method protected h()V
    .locals 4

    .line 435
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "a.message.id"

    .line 436
    iget-object v2, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "a.message.clicked"

    const/4 v2, 0x1

    .line 437
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "In-App Message"

    .line 438
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v2

    invoke-static {v1, v0, v2, v3}, Lcom/adobe/mobile/e;->a(Ljava/lang/String;Ljava/util/Map;J)V

    .line 441
    iget-object v0, p0, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    sget-object v1, Lcom/adobe/mobile/an$a;->d:Lcom/adobe/mobile/an$a;

    if-ne v0, v1, :cond_0

    .line 442
    invoke-virtual {p0}, Lcom/adobe/mobile/p;->a()V

    :cond_0
    const/4 v0, 0x0

    .line 446
    invoke-static {v0}, Lcom/adobe/mobile/an;->a(Lcom/adobe/mobile/p;)V

    return-void
.end method

.method protected i()Ljava/lang/String;
    .locals 2

    .line 598
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Message ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; Show Rule: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/adobe/mobile/p;->b:Lcom/adobe/mobile/an$a;

    .line 601
    invoke-virtual {v1}, Lcom/adobe/mobile/an$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; Blacklisted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 603
    invoke-virtual {p0}, Lcom/adobe/mobile/p;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
