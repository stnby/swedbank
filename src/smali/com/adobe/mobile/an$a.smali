.class public final enum Lcom/adobe/mobile/an$a;
.super Ljava/lang/Enum;
.source "Messages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/an;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adobe/mobile/an$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/adobe/mobile/an$a;

.field public static final enum b:Lcom/adobe/mobile/an$a;

.field public static final enum c:Lcom/adobe/mobile/an$a;

.field public static final enum d:Lcom/adobe/mobile/an$a;

.field private static final synthetic f:[Lcom/adobe/mobile/an$a;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 62
    new-instance v0, Lcom/adobe/mobile/an$a;

    const-string v1, "MESSAGE_SHOW_RULE_UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/adobe/mobile/an$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/an$a;->a:Lcom/adobe/mobile/an$a;

    .line 63
    new-instance v0, Lcom/adobe/mobile/an$a;

    const-string v1, "MESSAGE_SHOW_RULE_ALWAYS"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/adobe/mobile/an$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/an$a;->b:Lcom/adobe/mobile/an$a;

    .line 64
    new-instance v0, Lcom/adobe/mobile/an$a;

    const-string v1, "MESSAGE_SHOW_RULE_ONCE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/adobe/mobile/an$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/an$a;->c:Lcom/adobe/mobile/an$a;

    .line 65
    new-instance v0, Lcom/adobe/mobile/an$a;

    const-string v1, "MESSAGE_SHOW_RULE_UNTIL_CLICK"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5, v5}, Lcom/adobe/mobile/an$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/an$a;->d:Lcom/adobe/mobile/an$a;

    const/4 v0, 0x4

    .line 61
    new-array v0, v0, [Lcom/adobe/mobile/an$a;

    sget-object v1, Lcom/adobe/mobile/an$a;->a:Lcom/adobe/mobile/an$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/an$a;->b:Lcom/adobe/mobile/an$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adobe/mobile/an$a;->c:Lcom/adobe/mobile/an$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/adobe/mobile/an$a;->d:Lcom/adobe/mobile/an$a;

    aput-object v1, v0, v5

    sput-object v0, Lcom/adobe/mobile/an$a;->f:[Lcom/adobe/mobile/an$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 69
    iput p3, p0, Lcom/adobe/mobile/an$a;->e:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/an$a;
    .locals 1

    .line 61
    const-class v0, Lcom/adobe/mobile/an$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adobe/mobile/an$a;

    return-object p0
.end method

.method public static values()[Lcom/adobe/mobile/an$a;
    .locals 1

    .line 61
    sget-object v0, Lcom/adobe/mobile/an$a;->f:[Lcom/adobe/mobile/an$a;

    invoke-virtual {v0}, [Lcom/adobe/mobile/an$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/an$a;

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/adobe/mobile/an$a;->e:I

    return v0
.end method
