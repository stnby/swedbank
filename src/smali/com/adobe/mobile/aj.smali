.class final Lcom/adobe/mobile/aj;
.super Lcom/adobe/mobile/al;
.source "MessageOpenURL.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/adobe/mobile/al;-><init>()V

    return-void
.end method


# virtual methods
.method protected f()V
    .locals 7

    const/4 v0, 0x0

    .line 34
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v1
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 42
    iget-object v2, p0, Lcom/adobe/mobile/aj;->b:Lcom/adobe/mobile/an$a;

    sget-object v3, Lcom/adobe/mobile/an$a;->c:Lcom/adobe/mobile/an$a;

    if-ne v2, v3, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/adobe/mobile/aj;->a()V

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/adobe/mobile/aj;->k()Ljava/lang/String;

    move-result-object v2

    const-string v3, "%s - Creating intent with uri: %s"

    const/4 v4, 0x2

    .line 48
    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/adobe/mobile/aj;->j()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :try_start_1
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 53
    invoke-virtual {v1, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "%s - Could not load intent for message (%s)"

    .line 56
    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/adobe/mobile/aj;->j()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :catch_1
    move-exception v1

    .line 37
    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    const-string v0, "OpenURL"

    return-object v0
.end method
