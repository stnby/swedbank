.class Lcom/adobe/mobile/bd$6;
.super Ljava/lang/Object;
.source "VisitorIDService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/bd;->a(Ljava/util/Map;Ljava/util/Map;Lcom/adobe/mobile/bc$a;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/util/HashMap;

.field final synthetic c:Ljava/util/HashMap;

.field final synthetic d:Lcom/adobe/mobile/bc$a;

.field final synthetic e:Lcom/adobe/mobile/bd;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/bd;ZLjava/util/HashMap;Ljava/util/HashMap;Lcom/adobe/mobile/bc$a;)V
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    iput-boolean p2, p0, Lcom/adobe/mobile/bd$6;->a:Z

    iput-object p3, p0, Lcom/adobe/mobile/bd$6;->b:Ljava/util/HashMap;

    iput-object p4, p0, Lcom/adobe/mobile/bd$6;->c:Ljava/util/HashMap;

    iput-object p5, p0, Lcom/adobe/mobile/bd$6;->d:Lcom/adobe/mobile/bc$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 192
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->E()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 194
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const-string v0, "ID Service - Ignoring ID Sync due to privacy status not being opt in"

    .line 195
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 199
    :cond_1
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->B()Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v3

    iget-object v1, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v1}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-object v1, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v1}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;)J

    move-result-wide v5

    const/4 v1, 0x1

    cmp-long v3, v3, v5

    if-gtz v3, :cond_3

    iget-boolean v3, p0, Lcom/adobe/mobile/bd$6;->a:Z

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v3, 0x1

    .line 201
    :goto_1
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->b:Ljava/util/HashMap;

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    .line 202
    :goto_2
    iget-object v5, p0, Lcom/adobe/mobile/bd$6;->c:Ljava/util/HashMap;

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    .line 204
    :goto_3
    iget-object v6, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v6}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    if-nez v4, :cond_6

    if-nez v5, :cond_6

    if-nez v3, :cond_6

    return-void

    .line 208
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adobe/mobile/ao;->j()Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "https"

    goto :goto_4

    :cond_7
    const-string v4, "http"

    :goto_4
    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "://"

    .line 209
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4}, Lcom/adobe/mobile/bd;->d(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/id?d_ver=2&d_rtbd=json&d_orgid="

    .line 211
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v0, "&"

    .line 215
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "d_mid"

    .line 216
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "="

    .line 217
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_8
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v0, "&"

    .line 222
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "d_blob"

    .line 223
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "="

    .line 224
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :cond_9
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string v0, "&"

    .line 229
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "dcs_region"

    .line 230
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "="

    .line 231
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    :cond_a
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->b:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/adobe/mobile/bd$6;->d:Lcom/adobe/mobile/bc$a;

    invoke-static {v0, v4, v5}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;Ljava/util/Map;Lcom/adobe/mobile/bc$a;)Ljava/util/List;

    move-result-object v0

    .line 236
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4, v0}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 239
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_b
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    iget-object v5, p0, Lcom/adobe/mobile/bd$6;->c:Ljava/util/HashMap;

    invoke-static {v4, v5}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 244
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_c
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adobe/mobile/ao;->D()Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "&d_coop_unsafe=1"

    .line 249
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ID Service - Sending id sync call (%s)"

    .line 253
    new-array v5, v1, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v4, 0x0

    const/16 v5, 0x7d0

    const-string v6, "ID Service"

    .line 255
    invoke-static {v3, v4, v5, v6}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v3

    .line 257
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-virtual {v4, v3}, Lcom/adobe/mobile/bd;->a([B)Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_12

    const-string v4, "d_mid"

    .line 259
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v4, "error_msg"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_12

    :try_start_0
    const-string v4, "d_blob"

    .line 264
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 265
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    const-string v5, "d_blob"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/adobe/mobile/bd;->d(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    :cond_e
    const-string v4, "dcs_region"

    .line 268
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 269
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    const-string v5, "dcs_region"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;Ljava/lang/String;)Ljava/lang/String;

    :cond_f
    const-string v4, "id_sync_ttl"

    .line 272
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 273
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    const-string v5, "id_sync_ttl"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    int-to-long v5, v5

    invoke-static {v4, v5, v6}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;J)J

    :cond_10
    const-string v4, ""

    const-string v5, "d_optout"

    .line 277
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    const-string v5, "d_optout"

    .line 278
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 279
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_11

    .line 280
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v3

    sget-object v4, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    invoke-virtual {v3, v4}, Lcom/adobe/mobile/ao;->a(Lcom/adobe/mobile/aq;)V

    const-string v4, ", global privacy status: opted out"

    :cond_11
    const-string v3, "ID Service - Got ID Response (mid: %s, blob: %s, hint: %s, ttl: %d%s)"

    const/4 v5, 0x5

    .line 285
    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v6}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v6, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v6}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v7}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v7}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    aput-object v4, v5, v6

    invoke-static {v3, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v3

    const-string v4, "ID Service - Error parsing response (%s)"

    .line 287
    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_12
    if-eqz v3, :cond_13

    const-string v4, "error_msg"

    .line 291
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    :try_start_1
    const-string v4, "ID Service - Service returned error (%s)"

    .line 293
    new-array v5, v1, [Ljava/lang/Object;

    const-string v6, "error_msg"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception v3

    const-string v4, "ID Service - Unable to read error condition(%s)"

    .line 295
    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    :cond_13
    :goto_5
    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;J)J

    .line 302
    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4, v0}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;Ljava/util/List;)V

    .line 303
    iget-object v0, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v3}, Lcom/adobe/mobile/bd;->g(Lcom/adobe/mobile/bd;)Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/adobe/mobile/bd;->d(Lcom/adobe/mobile/bd;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 305
    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v3}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v3}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v3}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v6

    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v3}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;)J

    move-result-wide v7

    iget-object v3, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v3}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;)J

    move-result-wide v9

    move-object v11, v0

    invoke-static/range {v4 .. v11}, Lcom/adobe/mobile/be;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    .line 308
    :try_start_2
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->D()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "ADBMOBILE_VISITORID_IDS"

    .line 309
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "ADBMOBILE_PERSISTED_MID"

    .line 310
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "ADBMOBILE_PERSISTED_MID_HINT"

    .line 311
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "ADBMOBILE_PERSISTED_MID_BLOB"

    .line 312
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "ADBMOBILE_VISITORID_TTL"

    .line 313
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4}, Lcom/adobe/mobile/bd;->b(Lcom/adobe/mobile/bd;)J

    move-result-wide v4

    invoke-interface {v3, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v0, "ADBMOBILE_VISITORID_SYNC"

    .line 314
    iget-object v4, p0, Lcom/adobe/mobile/bd$6;->e:Lcom/adobe/mobile/bd;

    invoke-static {v4}, Lcom/adobe/mobile/bd;->a(Lcom/adobe/mobile/bd;)J

    move-result-wide v4

    invoke-interface {v3, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 315
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_6

    :catch_2
    move-exception v0

    const-string v3, "ID Service - Unable to persist identifiers to shared preferences(%s)"

    .line 319
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_6
    return-void
.end method
