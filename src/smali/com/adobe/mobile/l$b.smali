.class public final enum Lcom/adobe/mobile/l$b;
.super Ljava/lang/Enum;
.source "Config.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adobe/mobile/l$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/adobe/mobile/l$b;

.field public static final enum b:Lcom/adobe/mobile/l$b;

.field private static final synthetic d:[Lcom/adobe/mobile/l$b;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 36
    new-instance v0, Lcom/adobe/mobile/l$b;

    const-string v1, "APPLICATION_TYPE_HANDHELD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/adobe/mobile/l$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/l$b;->a:Lcom/adobe/mobile/l$b;

    .line 37
    new-instance v0, Lcom/adobe/mobile/l$b;

    const-string v1, "APPLICATION_TYPE_WEARABLE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/adobe/mobile/l$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/l$b;->b:Lcom/adobe/mobile/l$b;

    const/4 v0, 0x2

    .line 35
    new-array v0, v0, [Lcom/adobe/mobile/l$b;

    sget-object v1, Lcom/adobe/mobile/l$b;->a:Lcom/adobe/mobile/l$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/l$b;->b:Lcom/adobe/mobile/l$b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/adobe/mobile/l$b;->d:[Lcom/adobe/mobile/l$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Lcom/adobe/mobile/l$b;->c:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/l$b;
    .locals 1

    .line 35
    const-class v0, Lcom/adobe/mobile/l$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adobe/mobile/l$b;

    return-object p0
.end method

.method public static values()[Lcom/adobe/mobile/l$b;
    .locals 1

    .line 35
    sget-object v0, Lcom/adobe/mobile/l$b;->d:[Lcom/adobe/mobile/l$b;

    invoke-virtual {v0}, [Lcom/adobe/mobile/l$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/l$b;

    return-object v0
.end method
