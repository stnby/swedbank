.class Lcom/adobe/mobile/ay$2;
.super Ljava/lang/Object;
.source "TargetPreviewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/ay;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/adobe/mobile/ay;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/ay;)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/adobe/mobile/ay$2;->a:Lcom/adobe/mobile/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 197
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/ay;->a(Lcom/adobe/mobile/ay;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GET"

    const-string v3, "text/html"

    .line 198
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->s()I

    move-result v5

    const-string v7, "Target Preview"

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 197
    invoke-static/range {v1 .. v8}, Lcom/adobe/mobile/aw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/adobe/mobile/ar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    iget v1, v0, Lcom/adobe/mobile/ar;->a:I

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/adobe/mobile/ar;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/adobe/mobile/ay$2;->a:Lcom/adobe/mobile/ay;

    iget-object v0, v0, Lcom/adobe/mobile/ar;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/adobe/mobile/ay;->a(Lcom/adobe/mobile/ay;Ljava/lang/String;)V

    .line 203
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->x()V

    .line 205
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "a.targetpreview.show"

    const-string v2, "true"

    .line 206
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    .line 207
    invoke-static {v0, v1, v1}, Lcom/adobe/mobile/an;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_0

    .line 211
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ay$2$1;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/ay$2$1;-><init>(Lcom/adobe/mobile/ay$2;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not show error message!(%s) "

    const/4 v2, 0x1

    .line 222
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
