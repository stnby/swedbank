.class public final enum Lcom/adobe/mobile/bc$a;
.super Ljava/lang/Enum;
.source "VisitorID.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/bc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adobe/mobile/bc$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/adobe/mobile/bc$a;

.field public static final enum b:Lcom/adobe/mobile/bc$a;

.field public static final enum c:Lcom/adobe/mobile/bc$a;

.field private static final synthetic f:[Lcom/adobe/mobile/bc$a;


# instance fields
.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 28
    new-instance v0, Lcom/adobe/mobile/bc$a;

    const-string v1, "VISITOR_ID_AUTHENTICATION_STATE_UNKNOWN"

    const-string v2, "unknown"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/adobe/mobile/bc$a;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/adobe/mobile/bc$a;->a:Lcom/adobe/mobile/bc$a;

    .line 29
    new-instance v0, Lcom/adobe/mobile/bc$a;

    const-string v1, "VISITOR_ID_AUTHENTICATION_STATE_AUTHENTICATED"

    const-string v2, "authenticated"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/adobe/mobile/bc$a;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/adobe/mobile/bc$a;->b:Lcom/adobe/mobile/bc$a;

    .line 30
    new-instance v0, Lcom/adobe/mobile/bc$a;

    const-string v1, "VISITOR_ID_AUTHENTICATION_STATE_LOGGED_OUT"

    const-string v2, "logged_out"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/adobe/mobile/bc$a;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/adobe/mobile/bc$a;->c:Lcom/adobe/mobile/bc$a;

    const/4 v0, 0x3

    .line 27
    new-array v0, v0, [Lcom/adobe/mobile/bc$a;

    sget-object v1, Lcom/adobe/mobile/bc$a;->a:Lcom/adobe/mobile/bc$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adobe/mobile/bc$a;->b:Lcom/adobe/mobile/bc$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/adobe/mobile/bc$a;->c:Lcom/adobe/mobile/bc$a;

    aput-object v1, v0, v5

    sput-object v0, Lcom/adobe/mobile/bc$a;->f:[Lcom/adobe/mobile/bc$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput p3, p0, Lcom/adobe/mobile/bc$a;->d:I

    .line 36
    iput-object p4, p0, Lcom/adobe/mobile/bc$a;->e:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/bc$a;
    .locals 1

    .line 27
    const-class v0, Lcom/adobe/mobile/bc$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adobe/mobile/bc$a;

    return-object p0
.end method

.method public static values()[Lcom/adobe/mobile/bc$a;
    .locals 1

    .line 27
    sget-object v0, Lcom/adobe/mobile/bc$a;->f:[Lcom/adobe/mobile/bc$a;

    invoke-virtual {v0}, [Lcom/adobe/mobile/bc$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/bc$a;

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/adobe/mobile/bc$a;->d:I

    return v0
.end method
