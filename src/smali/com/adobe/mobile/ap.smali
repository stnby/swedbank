.class Lcom/adobe/mobile/ap;
.super Ljava/lang/Object;
.source "MobileIdentities.java"


# direct methods
.method static a()V
    .locals 2

    const-string v0, "Config - Privacy status set to opt out, purging all Adobe SDK identities from device."

    const/4 v1, 0x0

    .line 197
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    invoke-static {}, Lcom/adobe/mobile/k;->a()V

    .line 202
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->v()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ap$1;

    invoke-direct {v1}, Lcom/adobe/mobile/ap$1;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b()V
    .locals 0

    .line 12
    invoke-static {}, Lcom/adobe/mobile/ap;->d()V

    return-void
.end method

.method static synthetic c()V
    .locals 0

    .line 12
    invoke-static {}, Lcom/adobe/mobile/ap;->e()V

    return-void
.end method

.method private static d()V
    .locals 2

    .line 216
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ap$2;

    invoke-direct {v1}, Lcom/adobe/mobile/ap$2;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static e()V
    .locals 2

    .line 227
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ap$3;

    invoke-direct {v1}, Lcom/adobe/mobile/ap$3;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
