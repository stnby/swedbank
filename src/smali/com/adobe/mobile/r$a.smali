.class public Lcom/adobe/mobile/r$a;
.super Ljava/lang/Object;
.source "MessageFullScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/adobe/mobile/r;


# direct methods
.method protected constructor <init>(Lcom/adobe/mobile/r;)V
    .locals 0

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    iput-object p1, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    return-void
.end method


# virtual methods
.method protected a()Landroid/webkit/WebView;
    .locals 3

    .line 288
    new-instance v0, Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v1, v1, Lcom/adobe/mobile/r;->n:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 289
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 290
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 291
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 292
    iget-object v1, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    invoke-virtual {v1}, Lcom/adobe/mobile/r;->k()Lcom/adobe/mobile/r$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 293
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    .line 294
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 295
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    const-string v2, "UTF-8"

    .line 296
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    return-object v0
.end method

.method public run()V
    .locals 9

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 246
    :try_start_0
    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    invoke-virtual {p0}, Lcom/adobe/mobile/r$a;->a()Landroid/webkit/WebView;

    move-result-object v3

    iput-object v3, v2, Lcom/adobe/mobile/r;->m:Landroid/webkit/WebView;

    .line 247
    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v3, v2, Lcom/adobe/mobile/r;->m:Landroid/webkit/WebView;

    const-string v4, "file:///android_asset/"

    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v5, v2, Lcom/adobe/mobile/r;->l:Ljava/lang/String;

    const-string v6, "text/html"

    const-string v7, "UTF-8"

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v2, v2, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    const-string v2, "Messages - unable to get root view group from os"

    .line 250
    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    invoke-static {v2}, Lcom/adobe/mobile/r;->b(Lcom/adobe/mobile/r;)V

    return-void

    .line 255
    :cond_0
    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v2, v2, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v2

    .line 256
    iget-object v3, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v3, v3, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    if-eqz v2, :cond_3

    if-nez v3, :cond_1

    goto :goto_1

    .line 266
    :cond_1
    iget-object v4, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-boolean v4, v4, Lcom/adobe/mobile/r;->f:Z

    if-eqz v4, :cond_2

    .line 267
    iget-object v4, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v4, v4, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v5, v5, Lcom/adobe/mobile/r;->m:Landroid/webkit/WebView;

    invoke-virtual {v4, v5, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    goto :goto_0

    .line 271
    :cond_2
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    int-to-float v5, v3

    const/4 v6, 0x0

    invoke-direct {v4, v6, v6, v5, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v5, 0x12c

    .line 272
    invoke-virtual {v4, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 273
    iget-object v5, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v5, v5, Lcom/adobe/mobile/r;->m:Landroid/webkit/WebView;

    invoke-virtual {v5, v4}, Landroid/webkit/WebView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 276
    iget-object v4, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v4, v4, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iget-object v5, v5, Lcom/adobe/mobile/r;->m:Landroid/webkit/WebView;

    invoke-virtual {v4, v5, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 280
    :goto_0
    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    iput-boolean v0, v2, Lcom/adobe/mobile/r;->f:Z

    goto :goto_2

    :cond_3
    :goto_1
    const-string v2, "Messages - root view hasn\'t been measured, cannot show message"

    .line 260
    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    iget-object v2, p0, Lcom/adobe/mobile/r$a;->a:Lcom/adobe/mobile/r;

    invoke-static {v2}, Lcom/adobe/mobile/r;->b(Lcom/adobe/mobile/r;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v2

    const-string v3, "Messages - Failed to show full screen message (%s)"

    .line 283
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    return-void
.end method
