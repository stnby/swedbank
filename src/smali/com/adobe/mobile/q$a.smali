.class final Lcom/adobe/mobile/q$a;
.super Ljava/lang/Object;
.source "MessageAlert.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/q$a$a;,
        Lcom/adobe/mobile/q$a$b;,
        Lcom/adobe/mobile/q$a$c;
    }
.end annotation


# instance fields
.field private final a:Lcom/adobe/mobile/q;


# direct methods
.method public constructor <init>(Lcom/adobe/mobile/q;)V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v0, 0x0

    .line 222
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v1
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x1

    .line 230
    :try_start_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 231
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->k:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 232
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->l:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 234
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->n:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 235
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->n:Ljava/lang/String;

    new-instance v4, Lcom/adobe/mobile/q$a$c;

    iget-object v5, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    invoke-direct {v4, v5}, Lcom/adobe/mobile/q$a$c;-><init>(Lcom/adobe/mobile/q;)V

    invoke-virtual {v3, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->o:Ljava/lang/String;

    new-instance v4, Lcom/adobe/mobile/q$a$b;

    iget-object v5, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    invoke-direct {v4, v5}, Lcom/adobe/mobile/q$a$b;-><init>(Lcom/adobe/mobile/q;)V

    invoke-virtual {v3, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 239
    new-instance v1, Lcom/adobe/mobile/q$a$a;

    iget-object v4, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    invoke-direct {v1, v4}, Lcom/adobe/mobile/q$a$a;-><init>(Lcom/adobe/mobile/q;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 241
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, v1, Lcom/adobe/mobile/q;->p:Landroid/app/AlertDialog;

    .line 242
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->p:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 243
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iget-object v1, v1, Lcom/adobe/mobile/q;->p:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 245
    iget-object v1, p0, Lcom/adobe/mobile/q$a;->a:Lcom/adobe/mobile/q;

    iput-boolean v2, v1, Lcom/adobe/mobile/q;->f:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "Messages - Could not show alert message (%s)"

    .line 248
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v3, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :catch_1
    move-exception v1

    .line 225
    invoke-virtual {v1}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
