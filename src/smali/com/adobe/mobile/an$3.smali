.class final Lcom/adobe/mobile/an$3;
.super Ljava/lang/Object;
.source "Messages.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/an;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/adobe/mobile/an$3;->a:Ljava/util/Map;

    iput-object p2, p0, Lcom/adobe/mobile/an$3;->b:Ljava/util/Map;

    iput-object p3, p0, Lcom/adobe/mobile/an$3;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 181
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->z()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    .line 185
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_1

    goto :goto_1

    .line 190
    :cond_1
    iget-object v1, p0, Lcom/adobe/mobile/an$3;->a:Ljava/util/Map;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/adobe/mobile/an$3;->a:Ljava/util/Map;

    const-string v2, "pev2"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/adobe/mobile/an$3;->a:Ljava/util/Map;

    const-string v2, "pev2"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ADBINTERNAL:In-App Message"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    .line 196
    :cond_2
    iget-object v1, p0, Lcom/adobe/mobile/an$3;->b:Ljava/util/Map;

    invoke-static {v1}, Lcom/adobe/mobile/an;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v1

    .line 197
    iget-object v2, p0, Lcom/adobe/mobile/an$3;->a:Ljava/util/Map;

    invoke-static {v2}, Lcom/adobe/mobile/an;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v2

    .line 199
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/adobe/mobile/p;

    .line 200
    iget-object v4, p0, Lcom/adobe/mobile/an$3;->c:Ljava/util/Map;

    invoke-virtual {v3, v2, v1, v4}, Lcom/adobe/mobile/p;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 201
    invoke-virtual {v3}, Lcom/adobe/mobile/p;->f()V

    :cond_4
    return-void

    :cond_5
    :goto_1
    return-void
.end method
