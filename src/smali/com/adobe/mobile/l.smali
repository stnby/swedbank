.class public final Lcom/adobe/mobile/l;
.super Ljava/lang/Object;
.source "Config.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/l$a;,
        Lcom/adobe/mobile/l$c;,
        Lcom/adobe/mobile/l$b;
    }
.end annotation


# direct methods
.method public static a()Ljava/lang/String;
    .locals 4

    .line 133
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lcom/adobe/mobile/l$4;

    invoke-direct {v1}, Lcom/adobe/mobile/l$4;-><init>()V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 140
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 143
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Analytics - Unable to get UserIdentifier (%s)"

    const/4 v2, 0x1

    .line 145
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .line 254
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "Analytics - Method collectLifecycleData is not available for Wearable"

    const/4 v0, 0x0

    .line 255
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 258
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/l$2;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/l$2;-><init>(Landroid/app/Activity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .line 82
    sget-object v0, Lcom/adobe/mobile/l$b;->a:Lcom/adobe/mobile/l$b;

    invoke-static {p0, v0}, Lcom/adobe/mobile/l;->a(Landroid/content/Context;Lcom/adobe/mobile/l$b;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/adobe/mobile/l$b;)V
    .locals 0

    .line 88
    invoke-static {p0}, Lcom/adobe/mobile/StaticMethods;->a(Landroid/content/Context;)V

    .line 89
    invoke-static {p1}, Lcom/adobe/mobile/l;->a(Lcom/adobe/mobile/l$b;)V

    .line 90
    sget-object p0, Lcom/adobe/mobile/l$b;->b:Lcom/adobe/mobile/l$b;

    if-ne p1, p0, :cond_0

    .line 91
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    new-instance p1, Lcom/adobe/mobile/l$1;

    invoke-direct {p1}, Lcom/adobe/mobile/l$1;-><init>()V

    invoke-interface {p0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public static a(Lcom/adobe/mobile/l$b;)V
    .locals 0

    .line 206
    invoke-static {p0}, Lcom/adobe/mobile/StaticMethods;->a(Lcom/adobe/mobile/l$b;)V

    return-void
.end method

.method public static b()V
    .locals 2

    .line 280
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Analytics - Method pauseCollectingLifecycleData is not available for Wearable"

    const/4 v1, 0x0

    .line 281
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 284
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/q;->j()V

    .line 285
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/l$3;

    invoke-direct {v1}, Lcom/adobe/mobile/l$3;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
