.class final Lcom/adobe/mobile/an;
.super Ljava/lang/Object;
.source "Messages.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/an$a;
    }
.end annotation


# static fields
.field protected static final a:Ljava/lang/Integer;

.field private static b:Lcom/adobe/mobile/r;

.field private static final c:Ljava/lang/Object;

.field private static d:I

.field private static e:I

.field private static f:Lcom/adobe/mobile/p;

.field private static final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const v0, 0xb7267

    .line 54
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/an;->a:Ljava/lang/Integer;

    const/4 v0, 0x0

    .line 234
    sput-object v0, Lcom/adobe/mobile/an;->b:Lcom/adobe/mobile/r;

    .line 235
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/adobe/mobile/an;->c:Ljava/lang/Object;

    const/4 v1, -0x1

    .line 264
    sput v1, Lcom/adobe/mobile/an;->d:I

    .line 272
    sput v1, Lcom/adobe/mobile/an;->e:I

    .line 280
    sput-object v0, Lcom/adobe/mobile/an;->f:Lcom/adobe/mobile/p;

    .line 281
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/an;->g:Ljava/lang/Object;

    return-void
.end method

.method protected static a(Ljava/lang/String;)Lcom/adobe/mobile/r;
    .locals 4

    .line 216
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->A()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 217
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->z()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 220
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    goto :goto_1

    .line 224
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adobe/mobile/p;

    .line 225
    iget-object v3, v2, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/adobe/mobile/p;->a:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    instance-of v3, v2, Lcom/adobe/mobile/r;

    if-eqz v3, :cond_2

    .line 226
    move-object v1, v2

    check-cast v1, Lcom/adobe/mobile/r;

    :cond_3
    return-object v1

    :cond_4
    :goto_1
    return-object v1
.end method

.method protected static a(Ljava/util/Map;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_2

    .line 78
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_1

    .line 82
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 83
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 84
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0

    :cond_2
    :goto_1
    const/4 p0, 0x0

    return-object p0
.end method

.method protected static a()V
    .locals 2

    .line 93
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->t()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/an$1;

    invoke-direct {v1}, Lcom/adobe/mobile/an$1;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected static a(Lcom/adobe/mobile/p;)V
    .locals 1

    .line 289
    sget-object v0, Lcom/adobe/mobile/an;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 290
    :try_start_0
    sput-object p0, Lcom/adobe/mobile/an;->f:Lcom/adobe/mobile/p;

    .line 291
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method protected static a(Lcom/adobe/mobile/r;)V
    .locals 1

    .line 237
    sget-object v0, Lcom/adobe/mobile/an;->c:Ljava/lang/Object;

    monitor-enter v0

    .line 238
    :try_start_0
    sput-object p0, Lcom/adobe/mobile/an;->b:Lcom/adobe/mobile/r;

    .line 239
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method protected static a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 109
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->t()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/an$2;

    invoke-direct {v1, p1, p0}, Lcom/adobe/mobile/an$2;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected static a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 176
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->s()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/an$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/adobe/mobile/an$3;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected static b()Lcom/adobe/mobile/r;
    .locals 2

    .line 243
    sget-object v0, Lcom/adobe/mobile/an;->c:Ljava/lang/Object;

    monitor-enter v0

    .line 244
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/an;->b:Lcom/adobe/mobile/r;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 245
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected static c()V
    .locals 2

    .line 249
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->s()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/an$4;

    invoke-direct {v1}, Lcom/adobe/mobile/an$4;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected static d()I
    .locals 1

    .line 269
    sget v0, Lcom/adobe/mobile/an;->d:I

    return v0
.end method

.method protected static e()I
    .locals 1

    .line 277
    sget v0, Lcom/adobe/mobile/an;->e:I

    return v0
.end method

.method protected static f()Lcom/adobe/mobile/p;
    .locals 2

    .line 283
    sget-object v0, Lcom/adobe/mobile/an;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 284
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/an;->f:Lcom/adobe/mobile/p;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 285
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
