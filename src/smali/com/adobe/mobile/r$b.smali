.class public Lcom/adobe/mobile/r$b;
.super Landroid/webkit/WebViewClient;
.source "MessageFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation


# instance fields
.field protected a:Lcom/adobe/mobile/r;


# direct methods
.method protected constructor <init>(Lcom/adobe/mobile/r;)V
    .locals 0

    .line 306
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 307
    iput-object p1, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    return-void
.end method


# virtual methods
.method protected a(Landroid/webkit/WebView;)V
    .locals 3

    .line 311
    iget-object v0, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-object v0, v0, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string p1, "Messages - unable to get root view group from os"

    const/4 v0, 0x0

    .line 312
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 317
    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-object v1, v1, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v1, 0x12c

    .line 318
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 319
    new-instance v1, Lcom/adobe/mobile/r$b$1;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/r$b$1;-><init>(Lcom/adobe/mobile/r$b;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 332
    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 333
    iget-object v0, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-object v0, v0, Lcom/adobe/mobile/r;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    const-string v0, "adbinapp"

    .line 338
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v0, "cancel"

    .line 343
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    iget-object p2, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-boolean p2, p2, Lcom/adobe/mobile/r;->p:Z

    if-eqz p2, :cond_1

    .line 346
    iget-object p2, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    invoke-virtual {p2}, Lcom/adobe/mobile/r;->g()V

    .line 349
    :cond_1
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/r$b;->a(Landroid/webkit/WebView;)V

    goto/16 :goto_2

    :cond_2
    const-string v0, "confirm"

    .line 351
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 352
    iget-object v0, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-boolean v0, v0, Lcom/adobe/mobile/r;->p:Z

    if-eqz v0, :cond_3

    .line 354
    iget-object v0, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    invoke-virtual {v0}, Lcom/adobe/mobile/r;->h()V

    .line 357
    :cond_3
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/r$b;->a(Landroid/webkit/WebView;)V

    const-string p1, "url="

    .line 359
    invoke-virtual {p2, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-gez p1, :cond_4

    return v1

    :cond_4
    add-int/lit8 p1, p1, 0x4

    .line 366
    invoke-virtual {p2, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 367
    iget-object p2, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-object v0, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    invoke-virtual {v0, p1}, Lcom/adobe/mobile/r;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p2, v0, v1}, Lcom/adobe/mobile/r;->a(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object p2

    const-string v0, "{userId}"

    const-string v2, "0"

    .line 368
    invoke-virtual {p2, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{trackingId}"

    const-string v2, "0"

    .line 369
    invoke-virtual {p2, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{messageId}"

    .line 370
    iget-object v2, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-object v2, v2, Lcom/adobe/mobile/r;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{lifetimeValue}"

    .line 371
    invoke-static {}, Lcom/adobe/mobile/f;->a()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v0

    sget-object v2, Lcom/adobe/mobile/aq;->a:Lcom/adobe/mobile/aq;

    if-ne v0, v2, :cond_7

    const-string v0, "{userId}"

    .line 374
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    const-string v2, ""

    goto :goto_0

    :cond_5
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {p2, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "{trackingId}"

    .line 375
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->y()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    const-string v2, ""

    goto :goto_1

    :cond_6
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->y()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {p2, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    :cond_7
    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 379
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_8

    .line 381
    :try_start_0
    new-instance p2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 382
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 383
    iget-object p1, p0, Lcom/adobe/mobile/r$b;->a:Lcom/adobe/mobile/r;

    iget-object p1, p1, Lcom/adobe/mobile/r;->n:Landroid/app/Activity;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    const-string p2, "Messages - unable to launch intent from full screen message (%s)"

    .line 385
    new-array v0, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    invoke-static {p2, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    :cond_8
    :goto_2
    return v1
.end method
