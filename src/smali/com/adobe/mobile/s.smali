.class public Lcom/adobe/mobile/s;
.super Lcom/adobe/mobile/b;
.source "MessageFullScreenActivity.java"


# instance fields
.field protected a:Lcom/adobe/mobile/r;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/adobe/mobile/b;-><init>()V

    return-void
.end method

.method private a(Landroid/os/Bundle;)Lcom/adobe/mobile/r;
    .locals 2

    const-string v0, "MessageFullScreenActivity.messageId"

    .line 137
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-static {v0}, Lcom/adobe/mobile/an;->a(Ljava/lang/String;)Lcom/adobe/mobile/r;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "MessageFullScreenActivity.replacedHtml"

    .line 141
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/adobe/mobile/r;->l:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method private a()Z
    .locals 3

    .line 124
    iget-object v0, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    if-nez v0, :cond_0

    const-string v0, "Messages - unable to display fullscreen message, message is undefined"

    const/4 v1, 0x0

    .line 125
    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 126
    invoke-static {v0}, Lcom/adobe/mobile/an;->a(Lcom/adobe/mobile/p;)V

    .line 127
    invoke-virtual {p0}, Lcom/adobe/mobile/s;->finish()V

    .line 128
    invoke-virtual {p0, v1, v1}, Lcom/adobe/mobile/s;->overridePendingTransition(II)V

    return v1

    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    iput-boolean v1, v0, Lcom/adobe/mobile/r;->f:Z

    .line 113
    iget-object v0, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    invoke-virtual {v0}, Lcom/adobe/mobile/r;->g()V

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/adobe/mobile/s;->finish()V

    .line 117
    invoke-virtual {p0, v1, v1}, Lcom/adobe/mobile/s;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 35
    invoke-super {p0, p1}, Lcom/adobe/mobile/b;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    .line 38
    invoke-direct {p0, p1}, Lcom/adobe/mobile/s;->a(Landroid/os/Bundle;)Lcom/adobe/mobile/r;

    move-result-object p1

    iput-object p1, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    .line 41
    iget-object p1, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    invoke-static {p1}, Lcom/adobe/mobile/an;->a(Lcom/adobe/mobile/r;)V

    goto :goto_0

    .line 43
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/an;->b()Lcom/adobe/mobile/r;

    move-result-object p1

    iput-object p1, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    .line 47
    :goto_0
    invoke-direct {p0}, Lcom/adobe/mobile/s;->a()Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    .line 53
    :cond_1
    iget-object p1, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    iput-object p0, p1, Lcom/adobe/mobile/r;->n:Landroid/app/Activity;

    const/4 p1, 0x1

    .line 55
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/s;->requestWindowFeature(I)Z

    .line 58
    new-instance p1, Landroid/widget/RelativeLayout;

    invoke-direct {p1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/s;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    .line 68
    invoke-super {p0}, Lcom/adobe/mobile/b;->onResume()V

    .line 71
    invoke-direct {p0}, Lcom/adobe/mobile/s;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x1020002

    const/4 v1, 0x0

    .line 77
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/adobe/mobile/s;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const-string v0, "Messages - unable to get root view group from os"

    .line 80
    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    invoke-virtual {p0}, Lcom/adobe/mobile/s;->finish()V

    .line 82
    invoke-virtual {p0, v1, v1}, Lcom/adobe/mobile/s;->overridePendingTransition(II)V

    goto :goto_0

    .line 85
    :cond_1
    new-instance v2, Lcom/adobe/mobile/s$1;

    invoke-direct {v2, p0, v0}, Lcom/adobe/mobile/s$1;-><init>(Lcom/adobe/mobile/s;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Messages - content view is in undefined state (%s)"

    const/4 v3, 0x1

    .line 95
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    invoke-virtual {p0}, Lcom/adobe/mobile/s;->finish()V

    .line 97
    invoke-virtual {p0, v1, v1}, Lcom/adobe/mobile/s;->overridePendingTransition(II)V

    :goto_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "MessageFullScreenActivity.messageId"

    .line 104
    iget-object v1, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    iget-object v1, v1, Lcom/adobe/mobile/r;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "MessageFullScreenActivity.replacedHtml"

    .line 105
    iget-object v1, p0, Lcom/adobe/mobile/s;->a:Lcom/adobe/mobile/r;

    iget-object v1, v1, Lcom/adobe/mobile/r;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-super {p0, p1}, Lcom/adobe/mobile/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
