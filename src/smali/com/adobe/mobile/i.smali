.class final Lcom/adobe/mobile/i;
.super Lcom/adobe/mobile/a;
.source "AnalyticsWorker.java"


# static fields
.field private static final l:Ljava/security/SecureRandom;

.field private static m:Lcom/adobe/mobile/i;

.field private static final n:Ljava/lang/Object;

.field private static o:Ljava/lang/String;

.field private static volatile p:Z


# instance fields
.field protected k:Landroid/database/sqlite/SQLiteStatement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/adobe/mobile/i;->l:Ljava/security/SecureRandom;

    const/4 v0, 0x0

    .line 58
    sput-object v0, Lcom/adobe/mobile/i;->m:Lcom/adobe/mobile/i;

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/i;->n:Ljava/lang/Object;

    const/4 v0, 0x1

    .line 142
    sput-boolean v0, Lcom/adobe/mobile/i;->p:Z

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .line 71
    invoke-direct {p0}, Lcom/adobe/mobile/a;-><init>()V

    const/4 v0, 0x0

    .line 53
    iput-object v0, p0, Lcom/adobe/mobile/i;->k:Landroid/database/sqlite/SQLiteStatement;

    const-string v0, "ADBMobileDataCache.sqlite"

    .line 72
    iput-object v0, p0, Lcom/adobe/mobile/i;->d:Ljava/lang/String;

    const-string v0, "Analytics"

    .line 73
    iput-object v0, p0, Lcom/adobe/mobile/i;->e:Ljava/lang/String;

    const-string v0, "CREATE TABLE IF NOT EXISTS HITS (ID INTEGER PRIMARY KEY AUTOINCREMENT, URL TEXT, TIMESTAMP INTEGER)"

    .line 74
    iput-object v0, p0, Lcom/adobe/mobile/i;->h:Ljava/lang/String;

    const-wide/16 v0, 0x0

    .line 75
    iput-wide v0, p0, Lcom/adobe/mobile/i;->g:J

    .line 77
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/adobe/mobile/i;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/i;->a(Ljava/io/File;)V

    .line 78
    invoke-virtual {p0}, Lcom/adobe/mobile/i;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/adobe/mobile/i;->f:J

    return-void
.end method

.method public static l()Lcom/adobe/mobile/i;
    .locals 2

    .line 62
    sget-object v0, Lcom/adobe/mobile/i;->n:Ljava/lang/Object;

    monitor-enter v0

    .line 63
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/i;->m:Lcom/adobe/mobile/i;

    if-nez v1, :cond_0

    .line 64
    new-instance v1, Lcom/adobe/mobile/i;

    invoke-direct {v1}, Lcom/adobe/mobile/i;-><init>()V

    sput-object v1, Lcom/adobe/mobile/i;->m:Lcom/adobe/mobile/i;

    .line 67
    :cond_0
    sget-object v1, Lcom/adobe/mobile/i;->m:Lcom/adobe/mobile/i;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 68
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method static synthetic m()Ljava/lang/String;
    .locals 1

    .line 38
    invoke-static {}, Lcom/adobe/mobile/i;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n()Ljava/security/SecureRandom;
    .locals 1

    .line 38
    sget-object v0, Lcom/adobe/mobile/i;->l:Ljava/security/SecureRandom;

    return-object v0
.end method

.method private static o()Ljava/lang/String;
    .locals 4

    .line 144
    sget-boolean v0, Lcom/adobe/mobile/i;->p:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 145
    sput-boolean v0, Lcom/adobe/mobile/i;->p:Z

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/ao;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "https://"

    goto :goto_0

    :cond_0
    const-string v2, "http://"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/ao;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/b/ss/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/ao;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/ao;->q()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "/JAVA-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "4.17.2-AN"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/adobe/mobile/i;->o:Ljava/lang/String;

    const-string v1, "Analytics - Setting base request URL(%s)"

    const/4 v2, 0x1

    .line 153
    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lcom/adobe/mobile/i;->o:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    :cond_1
    sget-object v0, Lcom/adobe/mobile/i;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;J)V
    .locals 5

    .line 86
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p1, "Analytics - Cannot send hit, MobileConfig is null (this really shouldn\'t happen)"

    .line 88
    new-array p2, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 93
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/ao;->b()Z

    move-result v2

    if-nez v2, :cond_1

    return-void

    .line 98
    :cond_1
    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->o()Lcom/adobe/mobile/aq;

    move-result-object v0

    sget-object v2, Lcom/adobe/mobile/aq;->b:Lcom/adobe/mobile/aq;

    if-ne v0, v2, :cond_2

    const-string p1, "Analytics - Ignoring hit due to privacy status being opted out"

    .line 99
    new-array p2, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/adobe/mobile/i;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    sget-object v2, Lcom/adobe/mobile/AbstractDatabaseBacking$a;->b:Lcom/adobe/mobile/AbstractDatabaseBacking$a;

    if-ne v0, v2, :cond_3

    const-string p1, "Analytics - Ignoring hit due to database error"

    .line 104
    new-array p2, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/adobe/mobile/i;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v2, 0x1

    .line 112
    :try_start_0
    iget-object v3, p0, Lcom/adobe/mobile/i;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3, v2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 113
    iget-object v3, p0, Lcom/adobe/mobile/i;->k:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x2

    invoke-virtual {v3, v4, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 116
    iget-object v3, p0, Lcom/adobe/mobile/i;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 118
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/Long;)V

    .line 120
    iget-wide p2, p0, Lcom/adobe/mobile/i;->f:J

    const-wide/16 v3, 0x1

    add-long/2addr p2, v3

    iput-wide p2, p0, Lcom/adobe/mobile/i;->f:J

    .line 123
    iget-object p2, p0, Lcom/adobe/mobile/i;->k:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p2

    :try_start_1
    const-string p3, "Analytics - Unknown error while inserting url (%s)"

    .line 130
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {p3, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    invoke-virtual {p0, p2}, Lcom/adobe/mobile/i;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception p2

    const-string p3, "Analytics - Unable to insert url (%s)"

    .line 126
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {p3, v2}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    invoke-virtual {p0, p2}, Lcom/adobe/mobile/i;->a(Ljava/lang/Exception;)V

    .line 133
    :goto_0
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    invoke-virtual {p0, v1}, Lcom/adobe/mobile/i;->a(Z)V

    return-void

    .line 133
    :goto_1
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method protected b()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 183
    :try_start_0
    iget-object v2, p0, Lcom/adobe/mobile/i;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "INSERT INTO HITS (URL, TIMESTAMP) VALUES (?, ?)"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/i;->k:Landroid/database/sqlite/SQLiteStatement;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Analytics - Unable to create database due to an unexpected error (%s)"

    .line 192
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v3, "Analytics - Unable to create database due to a sql error (%s)"

    .line 189
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v2

    const-string v3, "Analytics - Unable to create database due to an invalid path (%s)"

    .line 186
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v3, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected d()V
    .locals 4

    .line 165
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adobe/mobile/i;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 166
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->q()Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/adobe/mobile/i;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 169
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 171
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Analytics - Unable to migrate old hits db, creating new hits db (move file returned false)"

    .line 172
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Analytics - Unable to migrate old hits db, creating new hits db (%s)"

    const/4 v3, 0x1

    .line 175
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void
.end method

.method protected g()Lcom/adobe/mobile/a$a;
    .locals 14

    .line 200
    iget-object v0, p0, Lcom/adobe/mobile/i;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 205
    :try_start_0
    iget-object v4, p0, Lcom/adobe/mobile/i;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "HITS"

    const-string v6, "ID"

    const-string v7, "URL"

    const-string v8, "TIMESTAMP"

    filled-new-array {v6, v7, v8}, [Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "ID ASC"

    const-string v12, "1"

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 207
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 208
    new-instance v5, Lcom/adobe/mobile/a$a;

    invoke-direct {v5}, Lcom/adobe/mobile/a$a;-><init>()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    :try_start_2
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/adobe/mobile/a$a;->b:Ljava/lang/String;

    .line 212
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/adobe/mobile/a$a;->a:Ljava/lang/String;

    const/4 v1, 0x2

    .line 213
    invoke-interface {v4, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/adobe/mobile/a$a;->c:J
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v5

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v13, v4

    move-object v4, v1

    move-object v1, v13

    goto :goto_1

    :catch_1
    move-exception v1

    move-object v13, v4

    move-object v4, v1

    move-object v1, v13

    goto :goto_3

    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 226
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_1
    move-object v5, v1

    goto :goto_4

    :catchall_0
    move-exception v1

    move-object v2, v1

    move-object v1, v4

    goto :goto_5

    :catch_2
    move-exception v5

    move-object v13, v5

    move-object v5, v1

    move-object v1, v4

    move-object v4, v13

    goto :goto_1

    :catch_3
    move-exception v5

    move-object v13, v5

    move-object v5, v1

    move-object v1, v4

    move-object v4, v13

    goto :goto_3

    :catchall_1
    move-exception v2

    goto :goto_5

    :catch_4
    move-exception v4

    move-object v5, v1

    :goto_1
    :try_start_4
    const-string v6, "Analytics - Unknown error reading from database (%s)"

    .line 222
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v6, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_2

    .line 226
    :goto_2
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_4

    :catch_5
    move-exception v4

    move-object v5, v1

    :goto_3
    :try_start_6
    const-string v6, "Analytics - Unable to read from database (%s)"

    .line 218
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v6, v3}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_2

    goto :goto_2

    .line 229
    :cond_2
    :goto_4
    :try_start_7
    monitor-exit v0

    return-object v5

    :goto_5
    if-eqz v1, :cond_3

    .line 226
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_6

    :catchall_2
    move-exception v1

    goto :goto_7

    :cond_3
    :goto_6
    throw v2

    .line 229
    :goto_7
    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v1
.end method

.method protected final h()Ljava/lang/Runnable;
    .locals 1

    .line 235
    new-instance v0, Lcom/adobe/mobile/i$1;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/i$1;-><init>(Lcom/adobe/mobile/i;)V

    return-object v0
.end method
