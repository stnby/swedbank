.class Lcom/adobe/mobile/al;
.super Lcom/adobe/mobile/p;
.source "MessageTemplateCallback.java"


# instance fields
.field protected k:Ljava/lang/String;

.field protected l:Ljava/lang/String;

.field protected m:Ljava/lang/String;

.field protected n:I


# direct methods
.method constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/adobe/mobile/p;-><init>()V

    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 4

    .line 172
    iget-object v0, p0, Lcom/adobe/mobile/al;->l:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/adobe/mobile/al;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/adobe/mobile/al;->m:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/adobe/mobile/al;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "application/json"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 175
    :goto_0
    iget-object v2, p0, Lcom/adobe/mobile/al;->l:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/adobe/mobile/al;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0, v2, v0}, Lcom/adobe/mobile/al;->a(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v0

    .line 177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "{%all_url%}"

    .line 178
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "{%all_json%}"

    .line 179
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    invoke-virtual {p0, v2, v1}, Lcom/adobe/mobile/al;->a(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v1

    .line 182
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 184
    iget-object v1, p0, Lcom/adobe/mobile/al;->l:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    return-object v0
.end method


# virtual methods
.method protected b(Lorg/json/JSONObject;)Z
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_6

    .line 56
    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-gtz v1, :cond_0

    goto/16 :goto_3

    .line 61
    :cond_0
    invoke-super {p0, p1}, Lcom/adobe/mobile/p;->b(Lorg/json/JSONObject;)Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/adobe/mobile/al;->j()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    :try_start_0
    const-string v4, "payload"

    .line 70
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v4

    if-gtz v4, :cond_2

    const-string p1, "%s - Unable to create data callback %s, \"payload\" is empty"

    .line 72
    new-array v4, v2, [Ljava/lang/Object;

    aput-object v1, v4, v0

    iget-object v5, p0, Lcom/adobe/mobile/al;->a:Ljava/lang/String;

    aput-object v5, v4, v3

    invoke-static {p1, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_6

    return v0

    :cond_2
    :try_start_1
    const-string v4, "templateurl"

    .line 83
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/adobe/mobile/al;->k:Ljava/lang/String;

    .line 84
    iget-object v4, p0, Lcom/adobe/mobile/al;->k:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_3

    const-string p1, "%s - Unable to create data callback %s, \"templateurl\" is empty"

    .line 85
    new-array v4, v2, [Ljava/lang/Object;

    aput-object v1, v4, v0

    iget-object v5, p0, Lcom/adobe/mobile/al;->a:Ljava/lang/String;

    aput-object v5, v4, v3

    invoke-static {p1, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5

    return v0

    :cond_3
    :try_start_2
    const-string v4, "timeout"

    .line 96
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/adobe/mobile/al;->n:I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    const-string v4, "%s - Tried to read \"timeout\" for data callback, but found none.  Using default value of two (2) seconds"

    .line 99
    new-array v5, v3, [Ljava/lang/Object;

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iput v2, p0, Lcom/adobe/mobile/al;->n:I

    :goto_0
    :try_start_3
    const-string v4, "templatebody"

    .line 106
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 107
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 109
    invoke-static {v4, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    if-eqz v4, :cond_4

    .line 112
    new-instance v5, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-direct {v5, v4, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 113
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    .line 114
    iput-object v5, p0, Lcom/adobe/mobile/al;->l:Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v4

    const-string v5, "%s - Failed to decode \"templatebody\" for data callback (%s).  This is not a required field"

    .line 126
    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v0

    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v5, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_2
    move-exception v4

    const-string v5, "%s - Failed to decode \"templatebody\" for data callback (%s).  This is not a required field"

    .line 123
    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v0

    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v5, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_3
    const-string v2, "%s - Tried to read \"templatebody\" for data callback, but found none.  This is not a required field"

    .line 120
    new-array v4, v3, [Ljava/lang/Object;

    aput-object v1, v4, v0

    invoke-static {v2, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/adobe/mobile/al;->l:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/adobe/mobile/al;->l:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    :try_start_4
    const-string v2, "contenttype"

    .line 132
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adobe/mobile/al;->m:Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_2

    :catch_4
    const-string p1, "%s - Tried to read \"contenttype\" for data callback, but found none.  This is not a required field"

    .line 135
    new-array v2, v3, [Ljava/lang/Object;

    aput-object v1, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    :goto_2
    return v3

    :catch_5
    const-string p1, "%s - Unable to create data callback %s, \"templateurl\" is required"

    .line 90
    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v0

    iget-object v1, p0, Lcom/adobe/mobile/al;->a:Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :catch_6
    const-string p1, "%s - Unable to create create data callback %s, \"payload\" is required"

    .line 77
    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v0

    iget-object v1, p0, Lcom/adobe/mobile/al;->a:Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :cond_6
    :goto_3
    return v0
.end method

.method protected f()V
    .locals 8

    .line 145
    invoke-virtual {p0}, Lcom/adobe/mobile/al;->k()Ljava/lang/String;

    move-result-object v1

    .line 146
    invoke-direct {p0}, Lcom/adobe/mobile/al;->m()Ljava/lang/String;

    move-result-object v2

    .line 147
    invoke-virtual {p0}, Lcom/adobe/mobile/al;->j()Ljava/lang/String;

    move-result-object v0

    const-string v3, "%s - Request Queued (url:%s body:%s contentType:%s)"

    const/4 v4, 0x4

    .line 149
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    iget-object v0, p0, Lcom/adobe/mobile/al;->m:Ljava/lang/String;

    const/4 v5, 0x3

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    invoke-virtual {p0}, Lcom/adobe/mobile/al;->l()Lcom/adobe/mobile/ba;

    move-result-object v0

    .line 151
    iget-object v3, p0, Lcom/adobe/mobile/al;->m:Ljava/lang/String;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v4

    iget v6, p0, Lcom/adobe/mobile/al;->n:I

    int-to-long v6, v6

    invoke-virtual/range {v0 .. v7}, Lcom/adobe/mobile/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    const-string v0, "Postbacks"

    return-object v0
.end method

.method protected k()Ljava/lang/String;
    .locals 3

    .line 158
    iget-object v0, p0, Lcom/adobe/mobile/al;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/al;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "{%all_url%}"

    .line 160
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v1, p0, Lcom/adobe/mobile/al;->k:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/adobe/mobile/al;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/adobe/mobile/al;->a(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v1

    const/4 v2, 0x0

    .line 162
    invoke-virtual {p0, v0, v2}, Lcom/adobe/mobile/al;->a(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v0

    .line 163
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 164
    iget-object v0, p0, Lcom/adobe/mobile/al;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method protected l()Lcom/adobe/mobile/ba;
    .locals 1

    .line 193
    invoke-static {}, Lcom/adobe/mobile/ba;->p()Lcom/adobe/mobile/ba;

    move-result-object v0

    return-object v0
.end method
