.class Lcom/adobe/mobile/bd$11;
.super Ljava/lang/Object;
.source "VisitorIDService.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/bd;->h()Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/StringBuilder;

.field final synthetic b:Lcom/adobe/mobile/bd;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/bd;Ljava/lang/StringBuilder;)V
    .locals 0

    .line 525
    iput-object p1, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    iput-object p2, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 3

    .line 528
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 529
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "d_mid"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 532
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v2}, Lcom/adobe/mobile/bd;->c(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 537
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "d_blob"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v2}, Lcom/adobe/mobile/bd;->e(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "dcs_region"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v2}, Lcom/adobe/mobile/bd;->f(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    :cond_1
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v0}, Lcom/adobe/mobile/bd;->i(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 552
    iget-object v0, p0, Lcom/adobe/mobile/bd$11;->a:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adobe/mobile/bd$11;->b:Lcom/adobe/mobile/bd;

    invoke-static {v2}, Lcom/adobe/mobile/bd;->i(Lcom/adobe/mobile/bd;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    return-object v1

    :cond_3
    return-object v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 525
    invoke-virtual {p0}, Lcom/adobe/mobile/bd$11;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
