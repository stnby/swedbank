.class final Lcom/adobe/mobile/q;
.super Lcom/adobe/mobile/p;
.source "MessageAlert.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/q$a;
    }
.end annotation


# instance fields
.field protected k:Ljava/lang/String;

.field protected l:Ljava/lang/String;

.field protected m:Ljava/lang/String;

.field protected n:Ljava/lang/String;

.field protected o:Ljava/lang/String;

.field protected p:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/adobe/mobile/p;-><init>()V

    return-void
.end method

.method protected static j()V
    .locals 3

    .line 273
    invoke-static {}, Lcom/adobe/mobile/an;->f()Lcom/adobe/mobile/p;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 274
    instance-of v1, v0, Lcom/adobe/mobile/q;

    if-nez v1, :cond_0

    goto :goto_0

    .line 279
    :cond_0
    iget v1, v0, Lcom/adobe/mobile/p;->g:I

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->H()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 280
    check-cast v0, Lcom/adobe/mobile/q;

    iget-object v1, v0, Lcom/adobe/mobile/q;->p:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/adobe/mobile/q;->p:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 281
    iget-object v1, v0, Lcom/adobe/mobile/q;->p:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    const/4 v1, 0x0

    .line 283
    iput-object v1, v0, Lcom/adobe/mobile/q;->p:Landroid/app/AlertDialog;

    :cond_2
    return-void

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method protected b(Lorg/json/JSONObject;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_6

    .line 54
    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-gtz v1, :cond_0

    goto/16 :goto_2

    .line 59
    :cond_0
    invoke-super {p0, p1}, Lcom/adobe/mobile/p;->b(Lorg/json/JSONObject;)Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    :cond_1
    const/4 v1, 0x1

    :try_start_0
    const-string v2, "payload"

    .line 66
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-gtz v2, :cond_2

    const-string p1, "Messages - Unable to create alert message \"%s\", payload is empty"

    .line 68
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5

    return v0

    :cond_2
    :try_start_1
    const-string v2, "title"

    .line 78
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/q;->k:Ljava/lang/String;

    .line 79
    iget-object v2, p0, Lcom/adobe/mobile/q;->k:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_3

    const-string p1, "Messages - Unable to create alert message \"%s\", title is empty"

    .line 80
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4

    return v0

    :cond_3
    :try_start_2
    const-string v2, "content"

    .line 90
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/q;->l:Ljava/lang/String;

    .line 91
    iget-object v2, p0, Lcom/adobe/mobile/q;->l:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_4

    const-string p1, "Messages - Unable to create alert message \"%s\", content is empty"

    .line 92
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    return v0

    :cond_4
    :try_start_3
    const-string v2, "cancel"

    .line 102
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/q;->o:Ljava/lang/String;

    .line 103
    iget-object v2, p0, Lcom/adobe/mobile/q;->o:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_5

    const-string p1, "Messages - Unable to create alert message \"%s\", cancel is empty"

    .line 104
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {p1, v2}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    return v0

    :cond_5
    :try_start_4
    const-string v2, "confirm"

    .line 115
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/adobe/mobile/q;->n:Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    const-string v2, "Messages - Tried to read \"confirm\" for alert message but found none. This is not a required field"

    .line 118
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    :try_start_5
    const-string v2, "url"

    .line 123
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adobe/mobile/q;->m:Ljava/lang/String;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    :catch_1
    const-string p1, "Messages - Tried to read url for alert message but found none. This is not a required field"

    .line 126
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return v1

    :catch_2
    const-string p1, "Messages - Unable to create alert message \"%s\", cancel is required"

    .line 109
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :catch_3
    const-string p1, "Messages - Unable to create alert message \"%s\", content is required"

    .line 97
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :catch_4
    const-string p1, "Messages - Unable to create alert message \"%s\", title is required"

    .line 85
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :catch_5
    const-string p1, "Messages - Unable to create alert message \"%s\", payload is required"

    .line 72
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/q;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/adobe/mobile/StaticMethods;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    :cond_6
    :goto_2
    return v0
.end method

.method protected f()V
    .locals 2

    .line 256
    iget-object v0, p0, Lcom/adobe/mobile/q;->o:Ljava/lang/String;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adobe/mobile/q;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/adobe/mobile/q;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/adobe/mobile/q;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    goto :goto_0

    .line 262
    :cond_1
    invoke-super {p0}, Lcom/adobe/mobile/p;->f()V

    .line 264
    invoke-virtual {p0}, Lcom/adobe/mobile/q;->e()V

    .line 267
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 268
    new-instance v1, Lcom/adobe/mobile/q$a;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/q$a;-><init>(Lcom/adobe/mobile/q;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_2
    :goto_0
    return-void
.end method
