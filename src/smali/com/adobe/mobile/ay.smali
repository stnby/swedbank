.class Lcom/adobe/mobile/ay;
.super Ljava/lang/Object;
.source "TargetPreviewManager.java"

# interfaces
.implements Lcom/adobe/mobile/n$a;
.implements Lcom/adobe/mobile/n$b;


# static fields
.field private static c:Lcom/adobe/mobile/ay;

.field private static final g:Ljava/lang/Object;

.field private static final l:Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private d:F

.field private e:F

.field private f:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private final i:Ljava/lang/Object;

.field private j:Ljava/lang/String;

.field private k:Lcom/adobe/mobile/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/ay;->g:Ljava/lang/Object;

    .line 87
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/ay;->l:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 40
    iput-object v0, p0, Lcom/adobe/mobile/ay;->a:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lcom/adobe/mobile/ay;->b:Ljava/lang/String;

    const/high16 v1, -0x40800000    # -1.0f

    .line 49
    iput v1, p0, Lcom/adobe/mobile/ay;->d:F

    .line 51
    iput v1, p0, Lcom/adobe/mobile/ay;->e:F

    .line 53
    iput-object v0, p0, Lcom/adobe/mobile/ay;->f:Ljava/lang/String;

    .line 68
    iput-object v0, p0, Lcom/adobe/mobile/ay;->h:Ljava/lang/String;

    .line 69
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/adobe/mobile/ay;->i:Ljava/lang/Object;

    .line 83
    iput-object v0, p0, Lcom/adobe/mobile/ay;->j:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/adobe/mobile/ay;->k:Lcom/adobe/mobile/ak;

    return-void
.end method

.method static synthetic a(Lcom/adobe/mobile/ay;)Ljava/lang/String;
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/adobe/mobile/ay;->m()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/adobe/mobile/ay;Ljava/lang/String;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/adobe/mobile/ay;->e(Ljava/lang/String;)V

    return-void
.end method

.method private b(FF)V
    .locals 0

    .line 148
    iput p1, p0, Lcom/adobe/mobile/ay;->d:F

    .line 149
    iput p2, p0, Lcom/adobe/mobile/ay;->e:F

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/adobe/mobile/ay;->j:Ljava/lang/String;

    return-void
.end method

.method static f()Lcom/adobe/mobile/ay;
    .locals 2

    .line 161
    sget-object v0, Lcom/adobe/mobile/ay;->l:Ljava/lang/Object;

    monitor-enter v0

    .line 162
    :try_start_0
    sget-object v1, Lcom/adobe/mobile/ay;->c:Lcom/adobe/mobile/ay;

    if-nez v1, :cond_0

    .line 163
    new-instance v1, Lcom/adobe/mobile/ay;

    invoke-direct {v1}, Lcom/adobe/mobile/ay;-><init>()V

    sput-object v1, Lcom/adobe/mobile/ay;->c:Lcom/adobe/mobile/ay;

    .line 166
    :cond_0
    sget-object v1, Lcom/adobe/mobile/ay;->c:Lcom/adobe/mobile/ay;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 167
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private declared-synchronized l()V
    .locals 4

    monitor-enter p0

    .line 125
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->G()Landroid/app/Activity;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/adobe/mobile/n;

    iget v2, p0, Lcom/adobe/mobile/ay;->d:F

    iget v3, p0, Lcom/adobe/mobile/ay;->e:F

    invoke-direct {v1, v0, v2, v3}, Lcom/adobe/mobile/n;-><init>(Landroid/content/Context;FF)V

    const-string v2, "ADBFloatingButtonTag"

    .line 127
    invoke-virtual {v1, v2}, Lcom/adobe/mobile/n;->setTag(Ljava/lang/Object;)V

    .line 129
    new-instance v2, Lcom/adobe/mobile/ay$1;

    invoke-direct {v2, p0}, Lcom/adobe/mobile/ay$1;-><init>(Lcom/adobe/mobile/ay;)V

    invoke-virtual {v1, v2}, Lcom/adobe/mobile/n;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    invoke-virtual {v1, v0, p0, p0}, Lcom/adobe/mobile/n;->a(Landroid/app/Activity;Lcom/adobe/mobile/n$a;Lcom/adobe/mobile/n$b;)V
    :try_end_0
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Target - Could not show the floating button (%s)"

    const/4 v2, 0x1

    .line 143
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    :goto_0
    monitor-exit p0

    return-void

    .line 124
    :goto_1
    monitor-exit p0

    throw v0
.end method

.method private m()Ljava/lang/String;
    .locals 5

    const-string v0, "https://hal.testandtarget.omniture.com"

    .line 179
    iget-object v1, p0, Lcom/adobe/mobile/ay;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/adobe/mobile/ay;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    iget-object v0, p0, Lcom/adobe/mobile/ay;->a:Ljava/lang/String;

    .line 183
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/ui/admin/%s/preview/?token=%s"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 185
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adobe/mobile/ao;->r()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/adobe/mobile/ay;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/adobe/mobile/StaticMethods;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 1

    const/4 v0, 0x0

    .line 238
    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ay;->a(Ljava/lang/String;)V

    .line 239
    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ay;->b(Ljava/lang/String;)V

    .line 240
    invoke-direct {p0, v0}, Lcom/adobe/mobile/ay;->e(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ay;->d(Ljava/lang/String;)V

    const/high16 v0, -0x40800000    # -1.0f

    .line 242
    invoke-direct {p0, v0, v0}, Lcom/adobe/mobile/ay;->b(FF)V

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 2

    .line 63
    sget-object v0, Lcom/adobe/mobile/ay;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 64
    :try_start_0
    iget-object v1, p0, Lcom/adobe/mobile/ay;->f:Ljava/lang/String;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 65
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public a(FF)V
    .locals 0

    .line 264
    invoke-direct {p0, p1, p2}, Lcom/adobe/mobile/ay;->b(FF)V

    return-void
.end method

.method public a(Lcom/adobe/mobile/n;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 258
    invoke-virtual {p1}, Lcom/adobe/mobile/n;->getXCompat()F

    move-result v0

    invoke-virtual {p1}, Lcom/adobe/mobile/n;->getYCompat()F

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/adobe/mobile/ay;->b(FF)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    .line 57
    sget-object v0, Lcom/adobe/mobile/ay;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 58
    :try_start_0
    iput-object p1, p0, Lcom/adobe/mobile/ay;->f:Ljava/lang/String;

    .line 59
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method protected b()Lcom/adobe/mobile/ak;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/adobe/mobile/ay;->k:Lcom/adobe/mobile/ak;

    if-nez v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/adobe/mobile/ay;->c()Lcom/adobe/mobile/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/ay;->k:Lcom/adobe/mobile/ak;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/adobe/mobile/ay;->k:Lcom/adobe/mobile/ak;

    return-object v0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/adobe/mobile/ay;->i:Ljava/lang/Object;

    monitor-enter v0

    .line 73
    :try_start_0
    iput-object p1, p0, Lcom/adobe/mobile/ay;->h:Ljava/lang/String;

    .line 74
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method protected c()Lcom/adobe/mobile/ak;
    .locals 6

    .line 100
    new-instance v0, Lcom/adobe/mobile/ak;

    invoke-direct {v0}, Lcom/adobe/mobile/ak;-><init>()V

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TargetPreview-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/adobe/mobile/ak;->a:Ljava/lang/String;

    .line 103
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->B()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v2, v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, v0, Lcom/adobe/mobile/ak;->c:Ljava/util/Date;

    .line 105
    invoke-virtual {p0}, Lcom/adobe/mobile/ay;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/adobe/mobile/ak;->k:Ljava/lang/String;

    .line 107
    sget-object v1, Lcom/adobe/mobile/an$a;->b:Lcom/adobe/mobile/an$a;

    iput-object v1, v0, Lcom/adobe/mobile/ak;->b:Lcom/adobe/mobile/an$a;

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/adobe/mobile/ak;->j:Ljava/util/ArrayList;

    .line 111
    new-instance v1, Lcom/adobe/mobile/x;

    invoke-direct {v1}, Lcom/adobe/mobile/x;-><init>()V

    const-string v2, "a.targetpreview.show"

    .line 112
    iput-object v2, v1, Lcom/adobe/mobile/u;->a:Ljava/lang/String;

    .line 113
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/adobe/mobile/u;->b:Ljava/util/ArrayList;

    .line 114
    iget-object v2, v1, Lcom/adobe/mobile/u;->b:Ljava/util/ArrayList;

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v2, v0, Lcom/adobe/mobile/ak;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/adobe/mobile/ak;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method c(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 171
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->e()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 174
    :cond_0
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/ay;->a(Ljava/lang/String;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method d()F
    .locals 1

    .line 153
    iget v0, p0, Lcom/adobe/mobile/ay;->d:F

    return v0
.end method

.method d(Ljava/lang/String;)V
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/adobe/mobile/ay;->a:Ljava/lang/String;

    return-void
.end method

.method e()F
    .locals 1

    .line 157
    iget v0, p0, Lcom/adobe/mobile/ay;->e:F

    return v0
.end method

.method g()V
    .locals 2

    .line 189
    invoke-virtual {p0}, Lcom/adobe/mobile/ay;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/adobe/mobile/ay;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ay$2;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/ay$2;-><init>(Lcom/adobe/mobile/ay;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    :goto_0
    const-string v0, "No Target Preview token setup!"

    const/4 v1, 0x0

    .line 190
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method h()Ljava/lang/String;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/adobe/mobile/ay;->j:Ljava/lang/String;

    return-object v0
.end method

.method i()V
    .locals 1

    .line 246
    invoke-virtual {p0}, Lcom/adobe/mobile/ay;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 248
    invoke-direct {p0}, Lcom/adobe/mobile/ay;->l()V

    goto :goto_0

    .line 251
    :cond_0
    invoke-static {}, Lcom/adobe/mobile/n;->a()V

    :goto_0
    return-void
.end method

.method j()Ljava/lang/String;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/adobe/mobile/ay;->b:Ljava/lang/String;

    return-object v0
.end method

.method public k()V
    .locals 1

    .line 284
    invoke-static {}, Lcom/adobe/mobile/ao;->a()Lcom/adobe/mobile/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/ao;->y()V

    .line 285
    invoke-direct {p0}, Lcom/adobe/mobile/ay;->n()V

    return-void
.end method
