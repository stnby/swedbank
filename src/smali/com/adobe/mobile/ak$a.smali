.class Lcom/adobe/mobile/ak$a;
.super Lcom/adobe/mobile/r$b;
.source "MessageTargetExperienceUIFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/ak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method protected constructor <init>(Lcom/adobe/mobile/r;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcom/adobe/mobile/r$b;-><init>(Lcom/adobe/mobile/r;)V

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .line 76
    invoke-super {p0, p1, p2}, Lcom/adobe/mobile/r$b;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    const-string v0, "adbinapp"

    .line 79
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "Error while reading the response from the Experience UI! (Response mal-formed)"

    invoke-static {p2, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/Toast;->show()V

    .line 81
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/ak$a;->a(Landroid/webkit/WebView;)V

    goto/16 :goto_1

    :cond_0
    const-string p1, "confirm"

    .line 82
    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "at_preview_params="

    .line 84
    invoke-virtual {p2, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_2

    const/16 v0, 0x26

    .line 87
    invoke-virtual {p2, v0, p1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    if-gez v0, :cond_1

    .line 89
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    :cond_1
    const-string v3, "at_preview_params="

    .line 93
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr p1, v3

    .line 92
    invoke-virtual {p2, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 94
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_2

    .line 96
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object p2

    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/adobe/mobile/ay;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "Could not decode the Target Preview parameters (%s)"

    .line 98
    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    :cond_2
    :goto_0
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ay;->j()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 106
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_4

    .line 108
    :try_start_1
    new-instance p2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 110
    iget-object p1, p0, Lcom/adobe/mobile/ak$a;->a:Lcom/adobe/mobile/r;

    iget-object p1, p1, Lcom/adobe/mobile/r;->n:Landroid/app/Activity;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    const-string p2, "Messages - unable to launch restart deeplink intent from Target Preview message (%s)"

    .line 112
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    invoke-static {p2, v0}, Lcom/adobe/mobile/StaticMethods;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    const-string p1, "cancel"

    .line 115
    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 117
    invoke-static {}, Lcom/adobe/mobile/ay;->f()Lcom/adobe/mobile/ay;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adobe/mobile/ay;->k()V

    :cond_4
    :goto_1
    const/4 p1, 0x0

    .line 122
    invoke-static {p1}, Lcom/adobe/mobile/an;->a(Lcom/adobe/mobile/p;)V

    return v1
.end method
