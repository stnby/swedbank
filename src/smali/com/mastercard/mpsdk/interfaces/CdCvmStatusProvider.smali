.class public interface abstract Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getTimeOfLastSuccessfulCdCvm()J
.end method

.method public abstract isCdCvmBlocked()Z
.end method

.method public abstract isCdCvmEnabled()Z
.end method

.method public abstract isCdCvmSuccessful(Lcom/mastercard/mpsdk/componentinterface/Card;)Z
.end method
