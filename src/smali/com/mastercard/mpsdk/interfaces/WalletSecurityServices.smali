.class public interface abstract Lcom/mastercard/mpsdk/interfaces/WalletSecurityServices;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getWalletCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;
.end method

.method public abstract rolloverDatabaseStorageKeys()V
.end method

.method public abstract rolloverLocalDataEncryptionKey()V
.end method

.method public abstract rolloverRemoteManagementKeyEncryptionKey()V
.end method

.method public abstract rolloverWalletDataEncryptionKey()V
.end method
