.class public interface abstract Lcom/mastercard/mpsdk/interfaces/SecurityServices;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/WalletSecurityServices;


# virtual methods
.method public abstract getDatabaseCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;
.end method

.method public abstract getRemoteManagementCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;
.end method

.method public abstract getRemoteManagementKeysForMobileKeySetId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;
.end method

.method public abstract getTransactionCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;
.end method

.method public abstract wipeCryptoParameters()Z
.end method
