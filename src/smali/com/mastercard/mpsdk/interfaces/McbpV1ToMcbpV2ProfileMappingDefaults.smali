.class public interface abstract Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;
.end method

.method public abstract getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
.end method

.method public abstract getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
.end method

.method public abstract isTransitSupported()Z
.end method

.method public abstract isUsAipMaskSupported()Z
.end method
