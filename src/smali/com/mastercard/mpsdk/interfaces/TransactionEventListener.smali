.class public interface abstract Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onContactlessPaymentAborted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
.end method

.method public abstract onContactlessPaymentCompleted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
.end method

.method public abstract onContactlessPaymentIncident(Lcom/mastercard/mpsdk/componentinterface/Card;Ljava/lang/Exception;)V
.end method

.method public abstract onTransactionStopped()V
.end method
