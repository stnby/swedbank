.class public interface abstract Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.super Ljava/lang/Object;


# virtual methods
.method public abstract initialize()Lcom/mastercard/mpsdk/interfaces/Mcbp;
.end method

.method public abstract usingOptionalAdviceManager(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalCardLevelPin()Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalCommunicationRetryParametersProvider(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalCredentialsAccessibilityPolicy(Lcom/mastercard/mpsdk/implementation/g;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalCredentialsReplenishmentPolicy(Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalCustomDatabase(Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalDefaultDataForMcbpV1ProfileMapping(Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalEmvOnlyMode()Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalMcbpLogger(Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalMpaKeyToSupportUpgrade([B)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalPdolItems(Ljava/util/List;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)",
            "Lcom/mastercard/mpsdk/interfaces/McbpInitializer;"
        }
    .end annotation
.end method

.method public abstract usingOptionalPinDataProviderForKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalPinDataProviderForTransactions(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalRemoteCommunicationEventListener(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalRemoteCommunicationManager(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalSecurityIncidentService(Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalTransactionApduListener(Lcom/mastercard/mpsdk/interfaces/ApduListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract usingOptionalUdolItems(Ljava/util/List;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)",
            "Lcom/mastercard/mpsdk/interfaces/McbpInitializer;"
        }
    .end annotation
.end method

.method public abstract withActiveCardProvider(Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withCardManagerEventListener(Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withCdCvmStatusProvider(Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withCryptoEngine(Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withHttpManager(Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withKeyRolloverEventListener(Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withTransactionEventListener(Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withWalletConsentManager(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method

.method public abstract withWalletIdentificationDataProvider(Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
.end method
