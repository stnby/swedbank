.class public interface abstract Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onCardMobilePinResetCompleted(Ljava/lang/String;)Z
.end method

.method public abstract onCardPinChangeCompleted(Ljava/lang/String;)Z
.end method

.method public abstract onCardPinChangeFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onCardPinSetCompleted(Ljava/lang/String;)Z
.end method

.method public abstract onCardPinSetFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onCardProvisionCompleted(Ljava/lang/String;)Z
.end method

.method public abstract onCardProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract onChangeWalletMobilePinStarted(Ljava/lang/String;)Z
.end method

.method public abstract onDeleteCardCompleted(Ljava/lang/String;)Z
.end method

.method public abstract onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onReplenishCompleted(Ljava/lang/String;I)Z
.end method

.method public abstract onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onRequestSessionCompleted()Z
.end method

.method public abstract onRequestSessionFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onSetWalletPinCompleted()Z
.end method

.method public abstract onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onSystemHealthCompleted()Z
.end method

.method public abstract onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onTaskStatusCompleted(Ljava/lang/String;)Z
.end method

.method public abstract onTaskStatusFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method

.method public abstract onWalletMobilePinResetCompleted()Z
.end method

.method public abstract onWalletPinChangeCompleted()Z
.end method

.method public abstract onWalletPinChangeFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
.end method
