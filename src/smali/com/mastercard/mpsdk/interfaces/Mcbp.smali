.class public interface abstract Lcom/mastercard/mpsdk/interfaces/Mcbp;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getApduProcessor()Lcom/mastercard/mpsdk/interfaces/ApduProcessor;
.end method

.method public abstract getCardManager()Lcom/mastercard/mpsdk/componentinterface/CardManager;
.end method

.method public abstract getMpaManagementHelper()Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;
.end method

.method public abstract getRemoteCommunicationManager()Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;
.end method

.method public abstract getWalletData()Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
.end method

.method public abstract getWalletSecurityServices()Lcom/mastercard/mpsdk/interfaces/WalletSecurityServices;
.end method

.method public abstract isMobileKeySetAvailable()Z
.end method

.method public abstract resetToUninitializedState()V
.end method

.method public abstract saveWalletData(Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
.end method
