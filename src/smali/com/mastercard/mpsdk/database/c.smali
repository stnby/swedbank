.class public final Lcom/mastercard/mpsdk/database/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/SecurityIncident;


# instance fields
.field public a:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

.field private final b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

.field private final c:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DATABASE"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/database/c;->c:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p2, p0, Lcom/mastercard/mpsdk/database/c;->a:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/c;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/c;->a:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;[B[B)Z
    .locals 3

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/c;->d:Ljava/lang/String;

    const/4 v0, 0x0

    new-array v1, v0, [B

    :try_start_0
    iget-object v2, p0, Lcom/mastercard/mpsdk/database/c;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {v2, p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B)[B

    move-result-object p3
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object p3, p0, Lcom/mastercard/mpsdk/database/c;->a:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    invoke-interface {p3, p0}, Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;->onSecurityIncident(Lcom/mastercard/mpsdk/componentinterface/SecurityIncident;)V

    move-object p3, v1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Calculated Checksum "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stored Checksum : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " : "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2, p3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    return v0
.end method

.method public final getReason()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Tamper detected : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/database/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getReasonCode()Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentReasonCode;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentReasonCode;->DATABASE_TAMPER_DETECTED:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentReasonCode;

    return-object v0
.end method
