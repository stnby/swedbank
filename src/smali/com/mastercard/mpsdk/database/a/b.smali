.class public final Lcom/mastercard/mpsdk/database/a/b;
.super Lcom/mastercard/mpsdk/database/a/a;


# instance fields
.field public e:Lcom/mastercard/mpsdk/database/b/b;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/database/b/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/database/a/a;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    new-instance p1, Lcom/mastercard/mpsdk/database/b/b;

    invoke-direct {p1}, Lcom/mastercard/mpsdk/database/b/b;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;)I
    .locals 0

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/database/a/b;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/database/b/a;

    move-result-object p1

    if-eqz p1, :cond_0

    iget p1, p1, Lcom/mastercard/mpsdk/database/b/a;->c:I

    return p1

    :cond_0
    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->getValue()I

    move-result p1

    return p1
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    iget-boolean v0, v0, Lcom/mastercard/mpsdk/database/b/b;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT * FROM card_profiles_lists"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Lcom/mastercard/mpsdk/database/b/b;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/database/b/b;-><init>()V

    iput-object v1, p0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/mastercard/mpsdk/database/b/a;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/database/b/a;-><init>()V

    const-string v2, "card_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mastercard/mpsdk/database/b/a;->a:Ljava/lang/String;

    const-string v2, "card_state"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/mastercard/mpsdk/database/b/a;->c:I

    const-string v2, "profile_data_version"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mastercard/mpsdk/database/b/a;->d:Ljava/lang/String;

    const-string v2, "checksum"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    iput-object v2, v1, Lcom/mastercard/mpsdk/database/b/a;->e:[B

    const-string v2, "profile_data"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    iput-object v2, v1, Lcom/mastercard/mpsdk/database/b/a;->b:[B

    iget-object v2, p0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    iget-object v3, v2, Lcom/mastercard/mpsdk/database/b/b;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    iput-boolean v1, v2, Lcom/mastercard/mpsdk/database/b/b;->a:Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/mastercard/mpsdk/database/b/a;
    .locals 3

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/b;->c()V

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/b;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/b/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/database/b/a;

    iget-object v2, v1, Lcom/mastercard/mpsdk/database/b/a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final c()V
    .locals 7

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/c;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/b;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v2, "Card Profile Table | Security incident service is disabled"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget v0, Lcom/mastercard/mpsdk/database/c/b;->c:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/b;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/database/b/a;

    iget-object v2, v1, Lcom/mastercard/mpsdk/database/b/a;->a:Ljava/lang/String;

    iget v3, v1, Lcom/mastercard/mpsdk/database/b/a;->c:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/mastercard/mpsdk/database/b/a;->d:Ljava/lang/String;

    iget-object v5, v1, Lcom/mastercard/mpsdk/database/b/a;->b:[B

    iget-object v1, v1, Lcom/mastercard/mpsdk/database/b/a;->e:[B

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Card profile table"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {p0, v3, v1, v2}, Lcom/mastercard/mpsdk/database/a/b;->a(Ljava/lang/String;[B[B)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    if-eqz v1, :cond_3

    sget v0, Lcom/mastercard/mpsdk/database/c/b;->a:I

    goto :goto_0

    :cond_3
    sget v0, Lcom/mastercard/mpsdk/database/c/b;->b:I

    :goto_0
    sget v1, Lcom/mastercard/mpsdk/database/c/b;->a:I

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/b;->a()V

    :cond_4
    return-void
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/database/b/a;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/b;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/b/b;->b:Ljava/util/List;

    return-object v0
.end method
