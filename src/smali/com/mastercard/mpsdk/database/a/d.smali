.class public final Lcom/mastercard/mpsdk/database/a/d;
.super Lcom/mastercard/mpsdk/database/a/a;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/database/b/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/database/a/a;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mpsdk/database/b/f;)V
    .locals 6

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/d;->b()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/database/b/d;

    iget-object v2, v2, Lcom/mastercard/mpsdk/database/b/d;->a:[B

    iget-object v3, p1, Lcom/mastercard/mpsdk/database/b/f;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;

    iget-object v4, p1, Lcom/mastercard/mpsdk/database/b/f;->b:[B

    iget-object v5, p1, Lcom/mastercard/mpsdk/database/b/f;->c:[B

    invoke-interface {v3, v4, v5, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;->rolloverData([B[B[B)[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/database/a/d;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v5, p1, Lcom/mastercard/mpsdk/database/b/f;->d:[B

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-interface {v4, v5, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B[B)[B

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/database/a/d;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "UPDATE environment_data_container SET remote_url = ?, checksum = ? ;"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v2, 0x2

    invoke-virtual {v4, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/database/b/d;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/d;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT * FROM environment_data_container"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/mastercard/mpsdk/database/b/d;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/database/b/d;-><init>()V

    const-string v3, "remote_url"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const-string v4, "checksum"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/d;->a:[B

    iput-object v4, v2, Lcom/mastercard/mpsdk/database/b/d;->b:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1
.end method
