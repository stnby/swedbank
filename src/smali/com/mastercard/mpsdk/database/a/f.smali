.class public final Lcom/mastercard/mpsdk/database/a/f;
.super Lcom/mastercard/mpsdk/database/a/a;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/database/b/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/database/a/a;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    return-void
.end method

.method public static a(Landroid/database/Cursor;)Lcom/mastercard/mpsdk/database/b/g;
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/database/b/g;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/database/b/g;-><init>()V

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "card_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/database/b/g;->a:Ljava/lang/String;

    const-string v1, "atc"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v0, Lcom/mastercard/mpsdk/database/b/g;->e:I

    const-string v1, "credential"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/database/b/g;->b:[B

    const-string v1, "time_stamp"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/database/b/g;->f:Ljava/lang/String;

    const-string v1, "credential_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/database/b/g;->c:Ljava/lang/String;

    const-string v1, "credential_status"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v0, Lcom/mastercard/mpsdk/database/b/g;->d:I

    const-string v1, "credential_data_version"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/database/b/g;->h:Ljava/lang/String;

    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/database/b/g;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT * FROM transaction_credentials_list"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/mastercard/mpsdk/database/b/g;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/database/b/g;-><init>()V

    const-string v3, "card_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/g;->a:Ljava/lang/String;

    const-string v3, "atc"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    long-to-int v3, v3

    iput v3, v2, Lcom/mastercard/mpsdk/database/b/g;->e:I

    const-string v3, "credential"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/g;->b:[B

    const-string v3, "time_stamp"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/g;->f:Ljava/lang/String;

    const-string v3, "credential_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/g;->c:Ljava/lang/String;

    const-string v3, "credential_status"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    long-to-int v3, v3

    iput v3, v2, Lcom/mastercard/mpsdk/database/b/g;->d:I

    const-string v3, "credential_data_version"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/g;->h:Ljava/lang/String;

    const-string v3, "checksum"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/g;->g:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/mastercard/mpsdk/database/b/f;)V
    .locals 12

    invoke-direct {p0}, Lcom/mastercard/mpsdk/database/a/f;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v2, v2, Lcom/mastercard/mpsdk/database/b/g;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v3, v3, Lcom/mastercard/mpsdk/database/b/g;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v4, v4, Lcom/mastercard/mpsdk/database/b/g;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/mpsdk/database/b/g;

    iget v5, v5, Lcom/mastercard/mpsdk/database/b/g;->e:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mastercard/mpsdk/database/b/g;

    iget v6, v6, Lcom/mastercard/mpsdk/database/b/g;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v7, v7, Lcom/mastercard/mpsdk/database/b/g;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v8, v8, Lcom/mastercard/mpsdk/database/b/g;->b:[B

    iget-object v9, p1, Lcom/mastercard/mpsdk/database/b/f;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;

    iget-object v10, p1, Lcom/mastercard/mpsdk/database/b/f;->b:[B

    iget-object v11, p1, Lcom/mastercard/mpsdk/database/b/f;->c:[B

    invoke-interface {v9, v10, v11, v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;->rolloverData([B[B[B)[B

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v8}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/mastercard/mpsdk/database/a/f;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v6, p1, Lcom/mastercard/mpsdk/database/b/f;->d:[B

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-interface {v5, v6, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B[B)[B

    move-result-object v3

    iget-object v5, p0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "UPDATE transaction_credentials_list SET credential = ?, checksum = ?  WHERE  card_id = ? AND credential_id = ? "

    invoke-virtual {v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v8}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v6, 0x2

    invoke-virtual {v5, v6, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x3

    invoke-virtual {v5, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v5, v2, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_0
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;[BLjava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    monitor-enter p0

    move-object/from16 v8, p6

    :try_start_0
    invoke-virtual {v1, v8}, Lcom/mastercard/mpsdk/database/a/f;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v8

    invoke-virtual/range {p0 .. p2}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v5, :cond_0

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :cond_0
    const-string v11, ""

    :goto_0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v11

    invoke-static {v11}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v11

    invoke-virtual {v11}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;)[B

    move-result-object v10

    const/4 v13, 0x5

    const/4 v14, 0x4

    const/4 v15, 0x3

    const/4 v11, 0x2

    const/4 v12, 0x1

    if-nez v9, :cond_2

    const-string v9, "INSERT INTO transaction_credentials_list ( card_id , credential_id , credential_status , atc , time_stamp , credential , credential_data_version , checksum )  VALUES (?,?,?,?,?,?,?,?);"

    invoke-virtual {v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v7

    invoke-virtual {v7, v12, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v7, v11, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    int-to-long v2, v3

    invoke-virtual {v7, v15, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    int-to-long v2, v4

    invoke-virtual {v7, v14, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    invoke-virtual {v7, v13, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v7, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v2, 0x7

    invoke-virtual {v7, v2, v6}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v2, 0x8

    invoke-virtual {v7, v2, v10}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v5, -0x1

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v3, "Unable to insert in the database"

    invoke-direct {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "UPDATE transaction_credentials_list SET credential_status = ? , atc = ?"

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v5, :cond_3

    const-string v9, " , time_stamp = ?"

    goto :goto_1

    :cond_3
    const-string v9, ""

    :goto_1
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " , credential = ?,checksum = ? WHERE  card_id = ? AND credential_id = ? "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    int-to-long v13, v3

    invoke-virtual {v6, v12, v13, v14}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    int-to-long v12, v4

    invoke-virtual {v6, v11, v12, v13}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    if-eqz v5, :cond_4

    invoke-virtual {v6, v15, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    const/4 v5, 0x4

    invoke-virtual {v6, v5, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x5

    invoke-virtual {v6, v3, v10}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x6

    invoke-virtual {v6, v3, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v3, 0x7

    invoke-virtual {v6, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v2

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5

    monitor-exit p0

    return-void

    :cond_5
    :try_start_2
    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v3, "Unable to update the database"

    invoke-direct {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    :try_start_3
    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateTransactionCredential;

    const-string v3, "Duplicate Card ID and ATC"

    invoke-direct {v2, v0, v4, v3}, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateTransactionCredential;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    const-string v0, "card_id = ?  AND credential_id = ? "

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const-string v4, "transaction_credentials_list"

    invoke-static {p3, v4, v0, v2}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "DELETE FROM transaction_credentials_list WHERE card_id = ?  AND credential_id = ? ;"

    invoke-virtual {p3, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p3

    invoke-virtual {p3, v3, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {p3, v1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {p3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result p1

    invoke-virtual {p3}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string p2, "Unable to update the database"

    invoke-direct {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Ljava/util/HashMap;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[B>;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-virtual {p0, v3}, Lcom/mastercard/mpsdk/database/a/f;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v3

    const-string v5, "UPDATE transaction_credentials_list SET credential = ?, checksum = ?  WHERE  card_id = ? AND credential_id = ? "

    invoke-virtual {v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/f;->b()V

    iget-object v6, p0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v6}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const/4 v7, 0x2

    new-array v8, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    aput-object v9, v8, v10

    const-string v9, "SELECT * FROM transaction_credentials_list WHERE card_id = ? AND credential_id = ? "

    invoke-virtual {v6, v9, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-static {v6}, Lcom/mastercard/mpsdk/database/a/f;->a(Landroid/database/Cursor;)Lcom/mastercard/mpsdk/database/b/g;

    move-result-object v6

    iget-object v8, v6, Lcom/mastercard/mpsdk/database/b/g;->f:Ljava/lang/String;

    iget-object v9, v6, Lcom/mastercard/mpsdk/database/b/g;->c:Ljava/lang/String;

    iget v11, v6, Lcom/mastercard/mpsdk/database/b/g;->e:I

    iget v12, v6, Lcom/mastercard/mpsdk/database/b/g;->d:I

    iget-object v6, v6, Lcom/mastercard/mpsdk/database/b/g;->h:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v5, v10, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v5, v7, v6}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x3

    invoke-virtual {v5, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {v5, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw p1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/f;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const-string v2, "SELECT credential FROM transaction_credentials_list WHERE card_id = ? AND credential_id = ?"

    invoke-virtual {v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    const-string v2, "credential"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CardID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "Key Id: "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " SUK protected data:"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Lcom/mastercard/mpsdk/database/a/f;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object p1
.end method

.method public final b()V
    .locals 12

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/c;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/f;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v2, "Transaction Credential Table | Security incident service is disabled"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget v0, Lcom/mastercard/mpsdk/database/c/b;->c:I

    goto/16 :goto_1

    :cond_0
    invoke-direct {p0}, Lcom/mastercard/mpsdk/database/a/f;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v3, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v3, v3, Lcom/mastercard/mpsdk/database/b/g;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v4, v4, Lcom/mastercard/mpsdk/database/b/g;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v5, v5, Lcom/mastercard/mpsdk/database/b/g;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mastercard/mpsdk/database/b/g;

    iget v6, v6, Lcom/mastercard/mpsdk/database/b/g;->e:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mastercard/mpsdk/database/b/g;

    iget v7, v7, Lcom/mastercard/mpsdk/database/b/g;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v8, v8, Lcom/mastercard/mpsdk/database/b/g;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v9, v9, Lcom/mastercard/mpsdk/database/b/g;->b:[B

    invoke-static {v9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mastercard/mpsdk/database/b/g;

    iget-object v10, v10, Lcom/mastercard/mpsdk/database/b/g;->g:[B

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Transaction credential table"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p0, v4, v10, v3}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;[B[B)Z

    move-result v3

    if-nez v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    sget v0, Lcom/mastercard/mpsdk/database/c/b;->a:I

    goto :goto_1

    :cond_2
    sget v0, Lcom/mastercard/mpsdk/database/c/b;->b:I

    :goto_1
    sget v1, Lcom/mastercard/mpsdk/database/c/b;->a:I

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/f;->a()V

    :cond_3
    return-void
.end method
