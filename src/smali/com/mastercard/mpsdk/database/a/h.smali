.class public final Lcom/mastercard/mpsdk/database/a/h;
.super Lcom/mastercard/mpsdk/database/a/a;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/database/b/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/database/a/a;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mpsdk/database/b/f;)V
    .locals 8

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/h;->b()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/database/b/i;

    iget-object v2, v2, Lcom/mastercard/mpsdk/database/b/i;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mpsdk/database/b/i;

    iget-object v3, v3, Lcom/mastercard/mpsdk/database/b/i;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mpsdk/database/b/i;

    iget-object v4, v4, Lcom/mastercard/mpsdk/database/b/i;->b:[B

    iget-object v5, p1, Lcom/mastercard/mpsdk/database/b/f;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;

    iget-object v6, p1, Lcom/mastercard/mpsdk/database/b/f;->b:[B

    iget-object v7, p1, Lcom/mastercard/mpsdk/database/b/f;->c:[B

    invoke-interface {v5, v6, v7, v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;->rolloverData([B[B[B)[B

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/mastercard/mpsdk/database/a/h;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v6, p1, Lcom/mastercard/mpsdk/database/b/f;->d:[B

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-interface {v5, v6, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B[B)[B

    move-result-object v2

    iget-object v5, p0, Lcom/mastercard/mpsdk/database/a/h;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "UPDATE wallet_data_container SET wallet_data = ?, checksum = ?  WHERE  card_id = ? ;"

    invoke-virtual {v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v4}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v4, 0x2

    invoke-virtual {v5, v4, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v2, 0x3

    invoke-virtual {v5, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/database/b/i;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/h;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT * FROM wallet_data_container"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/mastercard/mpsdk/database/b/i;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/database/b/i;-><init>()V

    const-string v3, "card_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/i;->a:Ljava/lang/String;

    const-string v3, "wallet_data_version"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/i;->c:Ljava/lang/String;

    const-string v3, "wallet_data"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/i;->b:[B

    const-string v3, "checksum"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/i;->d:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1
.end method
