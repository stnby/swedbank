.class public Lcom/mastercard/mpsdk/database/a/c;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# static fields
.field private static b:Lcom/mastercard/mpsdk/database/a/c;


# instance fields
.field public a:Lcom/mastercard/mpsdk/database/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string v0, "MCBP.db"

    const/4 v1, 0x0

    const v2, 0x20301

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/mastercard/mpsdk/database/a/c;
    .locals 2

    sget-object v0, Lcom/mastercard/mpsdk/database/a/c;->b:Lcom/mastercard/mpsdk/database/a/c;

    if-nez v0, :cond_1

    const-class v0, Lcom/mastercard/mpsdk/database/a/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/mastercard/mpsdk/database/a/c;->b:Lcom/mastercard/mpsdk/database/a/c;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/mastercard/mpsdk/database/a/c;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/mastercard/mpsdk/database/a/c;->b:Lcom/mastercard/mpsdk/database/a/c;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lcom/mastercard/mpsdk/database/a/c;->b:Lcom/mastercard/mpsdk/database/a/c;

    return-object p0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/c;->a:Lcom/mastercard/mpsdk/database/b;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/database/b;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/c;->a:Lcom/mastercard/mpsdk/database/b;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/database/b;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    return-void
.end method
