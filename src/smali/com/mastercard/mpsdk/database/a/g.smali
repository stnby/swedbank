.class public final Lcom/mastercard/mpsdk/database/a/g;
.super Lcom/mastercard/mpsdk/database/a/a;


# instance fields
.field public final e:I


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/database/b/c;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/database/a/a;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    iput p2, p0, Lcom/mastercard/mpsdk/database/a/g;->e:I

    return-void
.end method

.method private c()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/database/b/h;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT * FROM transaction_logs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/mastercard/mpsdk/database/b/h;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/database/b/h;-><init>()V

    const-string v3, "card_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "transaction_log_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "transaction_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "time_stamp"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "log_version"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "transaction_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    const-string v9, "checksum"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    iput-object v3, v2, Lcom/mastercard/mpsdk/database/b/h;->b:Ljava/lang/String;

    iput-object v4, v2, Lcom/mastercard/mpsdk/database/b/h;->a:Ljava/lang/String;

    iput-object v5, v2, Lcom/mastercard/mpsdk/database/b/h;->c:Ljava/lang/String;

    iput-object v6, v2, Lcom/mastercard/mpsdk/database/b/h;->e:Ljava/lang/String;

    iput-object v7, v2, Lcom/mastercard/mpsdk/database/b/h;->f:Ljava/lang/String;

    iput-object v8, v2, Lcom/mastercard/mpsdk/database/b/h;->d:[B

    iput-object v9, v2, Lcom/mastercard/mpsdk/database/b/h;->g:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/mastercard/mpsdk/database/b/f;)V
    .locals 11

    invoke-direct {p0}, Lcom/mastercard/mpsdk/database/a/g;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v2, v2, Lcom/mastercard/mpsdk/database/b/h;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v3, v3, Lcom/mastercard/mpsdk/database/b/h;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v4, v4, Lcom/mastercard/mpsdk/database/b/h;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v5, v5, Lcom/mastercard/mpsdk/database/b/h;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v6, v6, Lcom/mastercard/mpsdk/database/b/h;->d:[B

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v7, v7, Lcom/mastercard/mpsdk/database/b/h;->a:Ljava/lang/String;

    iget-object v8, p1, Lcom/mastercard/mpsdk/database/b/f;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;

    iget-object v9, p1, Lcom/mastercard/mpsdk/database/b/f;->b:[B

    iget-object v10, p1, Lcom/mastercard/mpsdk/database/b/f;->c:[B

    invoke-interface {v8, v9, v10, v6}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;->rolloverData([B[B[B)[B

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/database/a/g;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v5, p1, Lcom/mastercard/mpsdk/database/b/f;->d:[B

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-interface {v4, v5, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B[B)[B

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Rollover transaction logs unprotectedData checksum : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "UPDATE transaction_logs SET transaction_data = ?, checksum = ?  WHERE  card_id = ? AND transaction_log_id = ?; "

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v5, 0x2

    invoke-virtual {v4, v5, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x3

    invoke-virtual {v4, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v4, v2, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 10

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/c;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/g;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v2, "Transaction Logs Table | Security incident service is disabled"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget v0, Lcom/mastercard/mpsdk/database/c/b;->c:I

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lcom/mastercard/mpsdk/database/a/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v3, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v3, v3, Lcom/mastercard/mpsdk/database/b/h;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v4, v4, Lcom/mastercard/mpsdk/database/b/h;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v5, v5, Lcom/mastercard/mpsdk/database/b/h;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v6, v6, Lcom/mastercard/mpsdk/database/b/h;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v7, v7, Lcom/mastercard/mpsdk/database/b/h;->d:[B

    invoke-static {v7}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mastercard/mpsdk/database/b/h;

    iget-object v8, v8, Lcom/mastercard/mpsdk/database/b/h;->g:[B

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Transaction logs table"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p0, v4, v8, v3}, Lcom/mastercard/mpsdk/database/a/g;->a(Ljava/lang/String;[B[B)Z

    move-result v3

    if-nez v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    sget v0, Lcom/mastercard/mpsdk/database/c/b;->a:I

    goto :goto_1

    :cond_2
    sget v0, Lcom/mastercard/mpsdk/database/c/b;->b:I

    :goto_1
    sget v1, Lcom/mastercard/mpsdk/database/c/b;->a:I

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/g;->a()V

    :cond_3
    return-void
.end method
