.class public abstract Lcom/mastercard/mpsdk/database/a/a;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field public final b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

.field public final c:Lcom/mastercard/mpsdk/database/a/c;

.field public final d:Lcom/mastercard/mpsdk/database/c;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/database/b/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DATABASE"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iget-object v0, p1, Lcom/mastercard/mpsdk/database/b/c;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iput-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v0, p1, Lcom/mastercard/mpsdk/database/b/c;->b:Lcom/mastercard/mpsdk/database/a/c;

    iput-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->c:Lcom/mastercard/mpsdk/database/a/c;

    new-instance v0, Lcom/mastercard/mpsdk/database/c;

    iget-object v1, p0, Lcom/mastercard/mpsdk/database/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object p1, p1, Lcom/mastercard/mpsdk/database/b/c;->c:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    invoke-direct {v0, v1, p1}, Lcom/mastercard/mpsdk/database/c;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)V

    iput-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    return-void
.end method


# virtual methods
.method public final a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->encryptDataForStorage([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p1

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/c;->a:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    invoke-interface {v1, v0}, Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;->onSecurityIncident(Lcom/mastercard/mpsdk/componentinterface/SecurityIncident;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;[B[B)Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/database/c;->a(Ljava/lang/String;[B[B)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B
    .locals 2

    :try_start_0
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dataFromDatabase unEncryptDataForStorage(): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dataFromDatabase length unEncryptDataForStorage(): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->unencryptStoredDataForUse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Ljava/lang/String;)[B
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/database/a/a;->a()V

    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method
