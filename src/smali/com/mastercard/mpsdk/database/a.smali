.class public final Lcom/mastercard/mpsdk/database/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
.implements Lcom/mastercard/mpsdk/database/b;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private b:Landroid/content/Context;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

.field private d:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field private e:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

.field private f:Lcom/mastercard/mpsdk/database/a/b;

.field private g:Lcom/mastercard/mpsdk/database/a/f;

.field private h:Lcom/mastercard/mpsdk/database/a/e;

.field private i:Lcom/mastercard/mpsdk/database/a/h;

.field private j:Lcom/mastercard/mpsdk/database/a/g;

.field private k:Lcom/mastercard/mpsdk/database/a/d;

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DATABASE"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/database/a;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "CREATE TABLE wallet_data_container (card_id TEXT PRIMARY KEY NOT NULL, wallet_data_version TEXT NOT NULL, wallet_data BLOB NOT NULL, checksum BLOB); "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE environment_data_container (remote_url BLOB, checksum BLOB); "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE card_profiles_lists (card_id TEXT PRIMARY KEY NOT NULL, card_state INTEGER NOT NULL, profile_data BLOB NOT NULL, profile_data_version TEXT NOT NULL, checksum BLOB); "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE transaction_credentials_list (card_id TEXT NOT NULL, credential_id TEXT NOT NULL, credential_status INTEGER NOT NULL, atc INTEGER, time_stamp TEXT NOT NULL, credential BLOB NOT NULL, credential_data_version TEXT, checksum BLOB, PRIMARY KEY (card_id,atc)); "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE transaction_logs (transaction_log_id INTEGER PRIMARY KEY AUTOINCREMENT, time_stamp INTEGER NOT NULL, transaction_id TEXT, card_id TEXT NOT NULL, log_version TEXT NOT NULL, checksum BLOB, transaction_data BLOB NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE mobile_keys (mobile_keyset_id TEXT, mobile_key_type TEXT NOT NULL, mobile_key_value BLOB NOT NULL, checksum BLOB, PRIMARY KEY (mobile_keyset_id,mobile_key_type)); "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->d:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/mastercard/upgrade/j;

    invoke-direct {v0}, Lcom/mastercard/upgrade/j;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/database/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/mastercard/mpsdk/database/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v3, p0, Lcom/mastercard/mpsdk/database/a;->d:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    iput-object p1, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    iput-object v1, v0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iput-object v2, v0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iput-object v3, v0, Lcom/mastercard/upgrade/j;->d:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    new-instance p1, Lcom/mastercard/upgrade/c;

    iget-object v1, v0, Lcom/mastercard/upgrade/j;->d:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {p1, v1}, Lcom/mastercard/upgrade/c;-><init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V

    const v1, 0x10005

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq p2, v2, :cond_2

    if-ne p2, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_3

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p3, "Upgrade from all versions"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object p2, Lcom/mastercard/upgrade/utils/a/b;->a:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, p2}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object p2

    iget-object p3, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, v0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v2, v0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {p2, p3, v1, v2}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->a(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void

    :cond_3
    const v5, 0x10006

    const/4 v6, 0x3

    if-eq p2, v6, :cond_5

    if-ne p2, v5, :cond_4

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v7, 0x1

    :goto_3
    if-eqz v7, :cond_6

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p3, "Upgrade from TransactionLogAndSeparateModuleVersion"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->a(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void

    :cond_6
    const v7, 0x10100

    if-eq p2, v2, :cond_7

    if-eq p2, v1, :cond_7

    if-eq p2, v6, :cond_7

    if-eq p2, v5, :cond_7

    if-ge p2, v7, :cond_7

    const/4 v1, 0x1

    goto :goto_4

    :cond_7
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_8

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p3, "Upgrade from OneZeroSeries"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object p2, Lcom/mastercard/upgrade/utils/a/b;->c:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, p2}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object p2

    iget-object p3, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, v0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v2, v0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {p2, p3, v1, v2}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->b(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->c(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void

    :cond_8
    const v1, 0x10200

    if-lt p2, v7, :cond_9

    if-ge p2, v1, :cond_9

    const/4 v2, 0x1

    goto :goto_5

    :cond_9
    const/4 v2, 0x0

    :goto_5
    if-nez v2, :cond_14

    const v2, 0x10300

    if-lt p2, v1, :cond_a

    if-ge p2, v2, :cond_a

    const/4 v1, 0x1

    goto :goto_6

    :cond_a
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_b

    goto/16 :goto_a

    :cond_b
    const/high16 v1, 0x20000

    if-lt p2, v2, :cond_c

    if-ge p2, v1, :cond_c

    const/4 v2, 0x1

    goto :goto_7

    :cond_c
    const/4 v2, 0x0

    :goto_7
    if-eqz v2, :cond_d

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p3, "Upgrade from OneThreeSeries"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->b(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->c(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void

    :cond_d
    if-lt p2, v1, :cond_e

    const v1, 0x20100

    if-ge p2, v1, :cond_e

    const/4 v1, 0x1

    goto :goto_8

    :cond_e
    const/4 v1, 0x0

    :goto_8
    if-eqz v1, :cond_f

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p3, "Upgrade from OneTwoSeries"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->c(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void

    :cond_f
    const v1, 0x20200

    const v2, 0x20300

    if-lt p2, v1, :cond_10

    if-lt p2, v2, :cond_12

    :cond_10
    if-ne p2, v2, :cond_11

    goto :goto_9

    :cond_11
    const/4 v3, 0x0

    :cond_12
    :goto_9
    if-eqz v3, :cond_13

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p3, "Upgrade from TwoTwoZeroOrTwoThreeOne"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    sget-object p3, Lcom/mastercard/upgrade/utils/a/b;->g:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, p3}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object p1

    iget-object p3, v0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v0, v0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {p1, p2, p3, v0}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    return-void

    :cond_13
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "No upgrade needed from "

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " version to "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    return-void

    :cond_14
    :goto_a
    iget-object p2, v0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p3, "Upgrade from OneOneSeries or OneTwoSeries"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object p2, Lcom/mastercard/upgrade/utils/a/b;->d:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, p2}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object p2

    iget-object p3, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, v0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v2, v0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {p2, p3, v1, v2}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->b(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object p2, v0, Lcom/mastercard/upgrade/j;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/upgrade/j;->c(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public final declared-synchronized getAllCardIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/b;->c()V

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/b;->b()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/b/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/database/b/a;

    iget-object v2, v2, Lcom/mastercard/mpsdk/database/b/a;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getCardProfileByCardId(Ljava/lang/String;)[B
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/b;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/database/b/a;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    :cond_0
    :try_start_1
    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    iget-object p1, p1, Lcom/mastercard/mpsdk/database/b/a;->b:[B

    invoke-direct {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/database/a/b;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getCardProfileVersionByCardId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/b;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/database/b/a;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    :cond_0
    :try_start_1
    iget-object p1, p1, Lcom/mastercard/mpsdk/database/b/a;->d:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getCardStateByCardId(Ljava/lang/String;)I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/b;->b(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/mpsdk/database/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getFirstTransactionCredentialIdForCardIdWithStatus(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/f;->b()V

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, p1

    const-string p1, "SELECT credential_id FROM transaction_credentials_list WHERE card_id = ? AND credential_status = ?"

    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    const/4 p2, 0x0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "credential_id"

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getMobileKeySetId()Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/e;->b()V

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/e;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT mobile_keyset_id FROM mobile_keys"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    :cond_0
    :try_start_1
    const-string v1, "mobile_keyset_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getMobileKeyTypesInKeySet(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/e;->b()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/e;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string p1, "SELECT mobile_key_type FROM mobile_keys WHERE mobile_keyset_id = ?"

    invoke-virtual {v0, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    const-string v0, "mobile_key_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getRemoteManagementUrl()Ljava/lang/String;
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->k:Lcom/mastercard/mpsdk/database/a/d;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/c;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/d;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v3, "Environment Table | Security incident service is disabled"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v2}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget v1, Lcom/mastercard/mpsdk/database/c/b;->c:I

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/d;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v4, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mpsdk/database/b/d;

    iget-object v4, v4, Lcom/mastercard/mpsdk/database/b/d;->a:[B

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/mpsdk/database/b/d;

    iget-object v5, v5, Lcom/mastercard/mpsdk/database/b/d;->b:[B

    invoke-static {v4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "Environment data table"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v0, v6, v5, v4}, Lcom/mastercard/mpsdk/database/a/d;->a(Ljava/lang/String;[B[B)Z

    move-result v4

    if-nez v4, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_2

    sget v1, Lcom/mastercard/mpsdk/database/c/b;->a:I

    goto :goto_1

    :cond_2
    sget v1, Lcom/mastercard/mpsdk/database/c/b;->b:I

    :goto_1
    sget v2, Lcom/mastercard/mpsdk/database/c/b;->a:I

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/d;->a()V

    :cond_3
    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/d;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "SELECT * FROM environment_data_container"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v3

    :cond_4
    :try_start_1
    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    const-string v3, "remote_url"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-virtual {v0, v2}, Lcom/mastercard/mpsdk/database/a/d;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/f;->b()V

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const-string p1, "SELECT credential_id FROM transaction_credentials_list WHERE card_id = ?"

    invoke-virtual {v0, p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    const-string v0, "credential_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    new-array p1, v2, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getTransactionCredentialStatusForCardId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/f;->b()V

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v1

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object p2, v2, p1

    const-string p1, "SELECT credential_status FROM transaction_credentials_list WHERE card_id = ? AND credential_id = ?"

    invoke-virtual {v0, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "credential_status"

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final getTransactionCredentialTimeStampForCardId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/f;->b()V

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const-string p1, "SELECT time_stamp FROM transaction_credentials_list WHERE card_id = ? AND credential_id = ?"

    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "time_stamp"

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object p2
.end method

.method public final declared-synchronized getTransactionLogByIdForCardId(Ljava/lang/String;I)[B
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/g;->b()V

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, p1

    const-string p1, "SELECT transaction_data FROM transaction_logs WHERE card_id = ? AND transaction_log_id = ? "

    invoke-virtual {v1, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    if-nez p2, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    :cond_0
    :try_start_1
    new-instance p2, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    const-string v1, "transaction_data"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    invoke-virtual {v0, p2}, Lcom/mastercard/mpsdk/database/a/g;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getTransactionLogIdsForCardId(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/g;->b()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string p1, "SELECT transaction_log_id FROM transaction_logs WHERE card_id = ?  ORDER BY time_stamp DESC"

    invoke-virtual {v0, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    const-string v0, "transaction_log_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getWalletData()Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "WALLET_DATA_ID"

    invoke-virtual {p0, v0}, Lcom/mastercard/mpsdk/database/a;->getWalletDataForCardId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/database/WalletData;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getWalletDataForCardId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
    .locals 10

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->i:Lcom/mastercard/mpsdk/database/a/h;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/a;->d:Lcom/mastercard/mpsdk/database/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/c;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/h;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v3, "Wallet Data Table | Security incident service is disabled"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget v1, Lcom/mastercard/mpsdk/database/c/b;->c:I

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/h;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/mpsdk/database/b/i;

    iget-object v5, v5, Lcom/mastercard/mpsdk/database/b/i;->c:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mastercard/mpsdk/database/b/i;

    iget-object v6, v6, Lcom/mastercard/mpsdk/database/b/i;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mastercard/mpsdk/database/b/i;

    iget-object v7, v7, Lcom/mastercard/mpsdk/database/b/i;->b:[B

    invoke-static {v7}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v7

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mastercard/mpsdk/database/b/i;

    iget-object v8, v8, Lcom/mastercard/mpsdk/database/b/i;->d:[B

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Wallet data table"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v0, v6, v8, v5}, Lcom/mastercard/mpsdk/database/a/h;->a(Ljava/lang/String;[B[B)Z

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    if-eqz v5, :cond_2

    sget v1, Lcom/mastercard/mpsdk/database/c/b;->a:I

    goto :goto_1

    :cond_2
    sget v1, Lcom/mastercard/mpsdk/database/c/b;->b:I

    :goto_1
    sget v3, Lcom/mastercard/mpsdk/database/c/b;->a:I

    if-ne v1, v3, :cond_3

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/h;->a()V

    :cond_3
    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/h;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v2

    const-string p1, "SELECT wallet_data,wallet_data_version FROM wallet_data_container WHERE card_id = ? "

    invoke-virtual {v1, p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    const-string v2, "wallet_data"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/database/a/h;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object v1

    const-string v2, "wallet_data_version"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/mastercard/mpsdk/database/a/h$1;

    invoke-direct {v3, v0, v2, v1}, Lcom/mastercard/mpsdk/database/a/h$1;-><init>(Lcom/mastercard/mpsdk/database/a/h;Ljava/lang/String;[B)V

    move-object v1, v3

    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final synthetic initialize(Landroid/content/Context;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
    .locals 4

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/mastercard/mpsdk/database/a/c;->a(Landroid/content/Context;)Lcom/mastercard/mpsdk/database/a/c;

    move-result-object p1

    iput-object p0, p1, Lcom/mastercard/mpsdk/database/a/c;->a:Lcom/mastercard/mpsdk/database/b;

    new-instance v0, Lcom/mastercard/mpsdk/database/b/c;

    iget-object v1, p0, Lcom/mastercard/mpsdk/database/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/mastercard/mpsdk/database/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v3, p0, Lcom/mastercard/mpsdk/database/a;->e:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/mastercard/mpsdk/database/b/c;-><init>(Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;Lcom/mastercard/mpsdk/database/a/c;Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)V

    new-instance p1, Lcom/mastercard/mpsdk/database/a/b;

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/database/a/b;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    new-instance p1, Lcom/mastercard/mpsdk/database/a/f;

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/database/a/f;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    new-instance p1, Lcom/mastercard/mpsdk/database/a/e;

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/database/a/e;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    new-instance p1, Lcom/mastercard/mpsdk/database/a/h;

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/database/a/h;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->i:Lcom/mastercard/mpsdk/database/a/h;

    new-instance p1, Lcom/mastercard/mpsdk/database/a/g;

    iget v1, p0, Lcom/mastercard/mpsdk/database/a;->l:I

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mpsdk/database/a/g;-><init>(Lcom/mastercard/mpsdk/database/b/c;I)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    new-instance p1, Lcom/mastercard/mpsdk/database/a/d;

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/database/a/d;-><init>(Lcom/mastercard/mpsdk/database/b/c;)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->k:Lcom/mastercard/mpsdk/database/a/d;

    return-object p0
.end method

.method public final declared-synchronized rolloverData(Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;[B[B[B[B)V
    .locals 8

    monitor-enter p0

    :try_start_0
    new-instance v7, Lcom/mastercard/mpsdk/database/b/f;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/mastercard/mpsdk/database/b/f;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;[B[B[B[B)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/database/a/b;->d()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/mastercard/mpsdk/database/b/a;

    iget-object p4, p3, Lcom/mastercard/mpsdk/database/b/a;->a:Ljava/lang/String;

    iget p5, p3, Lcom/mastercard/mpsdk/database/b/a;->c:I

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p5

    iget-object p6, p3, Lcom/mastercard/mpsdk/database/b/a;->d:Ljava/lang/String;

    iget-object p3, p3, Lcom/mastercard/mpsdk/database/b/a;->b:[B

    iget-object v0, v7, Lcom/mastercard/mpsdk/database/b/f;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;

    iget-object v1, v7, Lcom/mastercard/mpsdk/database/b/f;->b:[B

    iget-object v2, v7, Lcom/mastercard/mpsdk/database/b/f;->c:[B

    invoke-interface {v0, v1, v2, p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;->rolloverData([B[B[B)[B

    move-result-object p3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p5

    invoke-virtual {p5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    iget-object p6, p1, Lcom/mastercard/mpsdk/database/a/b;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    iget-object v0, v7, Lcom/mastercard/mpsdk/database/b/f;->d:[B

    invoke-virtual {p5}, Ljava/lang/String;->getBytes()[B

    move-result-object p5

    invoke-interface {p6, v0, p5}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B[B)[B

    move-result-object p5

    new-instance p6, Ljava/lang/StringBuilder;

    const-string v0, "Rollover card profile unprotectedData checksum : "

    invoke-direct {p6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p6, p1, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {p6}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p6

    const-string v0, "UPDATE card_profiles_lists SET profile_data = ?, checksum = ?  WHERE  card_id = ? ;"

    invoke-virtual {p6, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p6

    const/4 v0, 0x1

    invoke-virtual {p6, v0, p3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p3, 0x2

    invoke-virtual {p6, p3, p5}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p3, 0x3

    invoke-virtual {p6, p3, p4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {p6}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {p6}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/database/b/b;->a()V

    iget-object p1, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {p1, v7}, Lcom/mastercard/mpsdk/database/a/f;->a(Lcom/mastercard/mpsdk/database/b/f;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    invoke-virtual {p1, v7}, Lcom/mastercard/mpsdk/database/a/e;->a(Lcom/mastercard/mpsdk/database/b/f;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/database/a;->i:Lcom/mastercard/mpsdk/database/a/h;

    invoke-virtual {p1, v7}, Lcom/mastercard/mpsdk/database/a/h;->a(Lcom/mastercard/mpsdk/database/b/f;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/database/a;->k:Lcom/mastercard/mpsdk/database/a/d;

    invoke-virtual {p1, v7}, Lcom/mastercard/mpsdk/database/a/d;->a(Lcom/mastercard/mpsdk/database/b/f;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    invoke-virtual {p1, v7}, Lcom/mastercard/mpsdk/database/a/g;->a(Lcom/mastercard/mpsdk/database/b/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized rolloverMobileKeys(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/e;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mastercard/mpsdk/database/a/e;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/e;->b()V

    iget-object v4, v0, Lcom/mastercard/mpsdk/database/a/e;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v5, 0x2

    new-array v6, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    const-string v8, "SELECT * FROM mobile_keys WHERE mobile_keyset_id = ? AND mobile_key_type = ? "

    invoke-virtual {v4, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    new-instance v6, Lcom/mastercard/mpsdk/database/b/e;

    invoke-direct {v6}, Lcom/mastercard/mpsdk/database/b/e;-><init>()V

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "mobile_key_value"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    const-string v9, "checksum"

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    iput-object p1, v6, Lcom/mastercard/mpsdk/database/b/e;->a:Ljava/lang/String;

    iput-object v8, v6, Lcom/mastercard/mpsdk/database/b/e;->b:[B

    iput-object v2, v6, Lcom/mastercard/mpsdk/database/b/e;->c:Ljava/lang/String;

    iput-object v9, v6, Lcom/mastercard/mpsdk/database/b/e;->d:[B

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    :cond_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    move-object v4, v6

    :goto_1
    const-string v6, "UPDATE mobile_keys SET mobile_key_value = ?, checksum = ? WHERE mobile_keyset_id = ? AND mobile_key_type = ?  ;"

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    if-eqz v4, :cond_0

    iget-object v4, v4, Lcom/mastercard/mpsdk/database/b/e;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mastercard/mpsdk/database/a/e;->a(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v6, v5, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x3

    invoke-virtual {v6, v3, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {v6, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    goto/16 :goto_0

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unable to store the mobile key"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized rolloverTransactionCredentialsAndCardProfiles(Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[B>;>;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-virtual {v0, v3}, Lcom/mastercard/mpsdk/database/a/b;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v3

    const-string v4, "UPDATE card_profiles_lists SET profile_data = ?, checksum = ?  WHERE  card_id = ? ;"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/mastercard/mpsdk/database/a/b;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/database/b/a;

    move-result-object v5

    if-eqz v5, :cond_0

    iget v6, v5, Lcom/mastercard/mpsdk/database/b/a;->c:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    iget-object v5, v5, Lcom/mastercard/mpsdk/database/b/a;->d:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/mastercard/mpsdk/database/a/b;->a(Ljava/lang/String;)[B

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, v6, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x2

    invoke-virtual {v4, v3, v5}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x3

    invoke-virtual {v4, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-object p2, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/database/b/b;->a()V

    iget-object p2, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {p2, p1}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/util/HashMap;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-object p2, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/database/b/b;->a()V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized saveCardProfile(Ljava/lang/String;[BLjava/lang/String;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    invoke-virtual {v0, p2}, Lcom/mastercard/mpsdk/database/a/b;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v1

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/b;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/database/b/a;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v6, "INSERT INTO card_profiles_lists ( card_id , profile_data , card_state , profile_data_version , checksum )  VALUES (?,?,?,?,?);"

    invoke-virtual {v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->NOT_ACTIVATED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    invoke-virtual {v7}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->getValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v7

    invoke-static {v7}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/mastercard/mpsdk/database/a/b;->a(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v2, v5, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-virtual {v2, v4, p1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->NOT_ACTIVATED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->getValue()I

    move-result p1

    int-to-long v4, p1

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 p1, 0x4

    invoke-virtual {v2, p1, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p1, 0x5

    invoke-virtual {v2, p1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v1, -0x1

    cmp-long p1, v3, v1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string p2, "Unable to update the database"

    invoke-direct {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/b;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/database/b/a;

    move-result-object v2

    iget-object v6, v0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v6}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, v2, Lcom/mastercard/mpsdk/database/b/a;->c:I

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p3

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/mastercard/mpsdk/database/a/b;->a(Ljava/lang/String;)[B

    move-result-object p3

    const-string v2, "UPDATE card_profiles_lists SET profile_data = ? checksum = ? WHERE  card_id = ? ;"

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v2, v4, p3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v2, v3, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    :goto_0
    invoke-static {p2}, Lcom/mastercard/mpsdk/database/c/a;->a([B)V

    iget-object p1, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/database/b/b;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized saveCardStateByCardId(Ljava/lang/String;I)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "UPDATE card_profiles_lists SET card_state = ?  , checksum = ?  WHERE card_id = ? "

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v5, "SELECT * FROM card_profiles_lists WHERE card_id = ?"

    invoke-virtual {v1, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    const-string v4, "profile_data_version"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v4, "profile_data"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v5

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/database/a/b;->a(Ljava/lang/String;)[B

    move-result-object v1

    int-to-long v4, p2

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 p2, 0x2

    invoke-virtual {v2, p2, v1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p2, 0x3

    invoke-virtual {v2, p2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    iget-object p1, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/database/b/b;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized saveEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/mastercard/mpsdk/database/a/e;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p3

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/e;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/mpsdk/database/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const-string v5, "INSERT INTO mobile_keys ( mobile_key_value , checksum , mobile_keyset_id , mobile_key_type )  VALUES (?,?,?,?);"

    :goto_1
    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    goto :goto_2

    :cond_1
    const-string v5, "UPDATE mobile_keys SET mobile_key_value = ?  , checksum = ? WHERE mobile_keyset_id = ? AND mobile_key_type = ?  ;"

    goto :goto_1

    :goto_2
    if-eqz p3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v5

    invoke-static {v5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    invoke-virtual {v1, v4, p3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    goto :goto_3

    :cond_2
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v3, v3, [B

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    :goto_3
    invoke-virtual {v0, v3}, Lcom/mastercard/mpsdk/database/a/e;->a(Ljava/lang/String;)[B

    move-result-object p3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Save MobileKeys | Checksum : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x2

    invoke-virtual {v1, v0, p3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p3, 0x3

    invoke-virtual {v1, p3, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p1, 0x4

    invoke-virtual {v1, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide p1

    goto :goto_4

    :cond_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result p1

    int-to-long p1, p1

    :goto_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v0, -0x1

    cmp-long p1, p1, v0

    if-eqz p1, :cond_4

    monitor-exit p0

    return-void

    :cond_4
    :try_start_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unable to store the mobile key"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized saveRemoteManagementUrl(Ljava/lang/String;)V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->k:Lcom/mastercard/mpsdk/database/a/d;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/d;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "environment_data_container"

    invoke-static {v1, v2}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    const-string v2, "INSERT INTO environment_data_container ( remote_url , checksum )  VALUES (?,?);"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-array p1, v7, [B

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v8}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/d;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-virtual {v1, v8, p1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    move-object p1, v2

    :goto_0
    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/d;->a(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {v1, v6, p1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v0, -0x1

    cmp-long p1, v2, v0

    if-eqz p1, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v0, "Unable to update the database"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const-string v2, "UPDATE environment_data_container SET remote_url = ?  , checksum = ? ;"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-array p1, v7, [B

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v8}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/d;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-virtual {v1, v8, p1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    move-object p1, v2

    :goto_1
    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/d;->a(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {v1, v6, p1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result p1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_4

    monitor-exit p0

    return-void

    :cond_4
    :try_start_2
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v0, "Unable to update the database"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized saveTransactionLogForCardId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    monitor-enter p0

    :try_start_0
    iget-object v6, v1, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    iget-object v7, v6, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v7}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const/4 v8, 0x1

    new-array v9, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const-string v11, "SELECT transaction_log_id FROM transaction_logs WHERE card_id = ? "

    invoke-virtual {v7, v11, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v11

    iget v12, v6, Lcom/mastercard/mpsdk/database/a/g;->e:I

    if-lt v11, v12, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v10

    const-string v9, "SELECT transaction_log_id FROM transaction_logs WHERE card_id = ?  ORDER BY time_stamp ASC"

    invoke-virtual {v7, v9, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-nez v9, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v9, "transaction_log_id"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    invoke-virtual {v6, v5}, Lcom/mastercard/mpsdk/database/a/g;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v3

    invoke-virtual {v6}, Lcom/mastercard/mpsdk/database/a/g;->b()V

    iget-object v5, v6, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    new-array v11, v8, [Ljava/lang/String;

    aput-object v9, v11, v10

    const-string v10, "SELECT * FROM transaction_logs WHERE transaction_log_id = ?"

    invoke-virtual {v5, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10

    if-nez v10, :cond_1

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    const/4 v5, 0x0

    move-object v10, v5

    goto :goto_0

    :cond_1
    new-instance v10, Lcom/mastercard/mpsdk/database/b/h;

    invoke-direct {v10}, Lcom/mastercard/mpsdk/database/b/h;-><init>()V

    const-string v11, "transaction_log_id"

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "transaction_id"

    invoke-interface {v5, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v5, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v14, "time_stamp"

    invoke-interface {v5, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v5, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v15, "log_version"

    invoke-interface {v5, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v5, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const-string v13, "transaction_data"

    invoke-interface {v5, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v5, v13}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    const-string v8, "checksum"

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    iput-object v11, v10, Lcom/mastercard/mpsdk/database/b/h;->a:Ljava/lang/String;

    iput-object v12, v10, Lcom/mastercard/mpsdk/database/b/h;->c:Ljava/lang/String;

    iput-object v14, v10, Lcom/mastercard/mpsdk/database/b/h;->e:Ljava/lang/String;

    iput-object v15, v10, Lcom/mastercard/mpsdk/database/b/h;->f:Ljava/lang/String;

    iput-object v13, v10, Lcom/mastercard/mpsdk/database/b/h;->d:[B

    iput-object v8, v10, Lcom/mastercard/mpsdk/database/b/h;->g:[B

    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-nez v10, :cond_2

    monitor-exit p0

    return-void

    :cond_2
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v10, Lcom/mastercard/mpsdk/database/b/h;->f:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/mastercard/mpsdk/database/a/g;->a(Ljava/lang/String;)[B

    move-result-object v0

    const-string v5, "UPDATE transaction_logs SET transaction_data = ?, transaction_id = ?, time_stamp =?, checksum =?  WHERE transaction_log_id = ? ;"

    invoke-virtual {v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    if-eqz v2, :cond_3

    const/4 v3, 0x2

    invoke-virtual {v5, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v5, v2, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v5, v2, v0}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v0, 0x5

    invoke-virtual {v5, v0, v9}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v0

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_4

    monitor-exit p0

    return-void

    :cond_4
    :try_start_3
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v2, "Unable to update the database"

    invoke-direct {v0, v2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    invoke-virtual {v6, v5}, Lcom/mastercard/mpsdk/database/a/g;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v5

    const-string v8, "INSERT INTO transaction_logs ( card_id , transaction_id , transaction_data , time_stamp , log_version , checksum )  VALUES (?,?,?,?,?,?);"

    invoke-virtual {v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v9

    invoke-static {v9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/mastercard/mpsdk/database/a/g;->a(Ljava/lang/String;)[B

    move-result-object v6

    const/4 v8, 0x1

    invoke-virtual {v7, v8, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    if-eqz v2, :cond_6

    const/4 v0, 0x2

    invoke-virtual {v7, v0, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v7, v2, v0}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v0, 0x4

    invoke-virtual {v7, v0, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v0, 0x5

    invoke-virtual {v7, v0, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v0, 0x6

    invoke-virtual {v7, v0, v6}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    monitor-exit p0

    return-void

    :cond_7
    :try_start_4
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v2, "Unable to update the database"

    invoke-direct {v0, v2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized saveWalletData(Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "WALLET_DATA_ID"

    invoke-virtual {p0, v0, p1}, Lcom/mastercard/mpsdk/database/a;->saveWalletDataForCardId(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized saveWalletDataForCardId(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->i:Lcom/mastercard/mpsdk/database/a/h;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/h;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    iget-object v4, v0, Lcom/mastercard/mpsdk/database/a/h;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/String;

    aput-object p1, v5, v3

    const-string v6, "wallet_data_container"

    const-string v7, "card_id = ?"

    invoke-static {v4, v6, v7, v5}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v4, 0x0

    :goto_1
    if-nez v4, :cond_2

    const-string v4, "INSERT INTO wallet_data_container ( wallet_data , wallet_data_version , checksum , card_id )  VALUES (?,?,?,?);"

    :goto_2
    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    goto :goto_3

    :cond_2
    const-string v4, "UPDATE wallet_data_container SET wallet_data = ?, wallet_data_version = ?, checksum = ? WHERE  card_id = ? ;"

    goto :goto_2

    :goto_3
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/database/WalletData;->getData()[B

    move-result-object v4

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/database/WalletData;->getVersion()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, v4}, Lcom/mastercard/mpsdk/database/a/h;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v5

    if-eqz v4, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    goto :goto_4

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v3, v3, [B

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    :goto_4
    invoke-virtual {v0, v3}, Lcom/mastercard/mpsdk/database/a/h;->a(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p2, 0x3

    invoke-virtual {v1, p2, v0}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p2, 0x4

    invoke-virtual {v1, p2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized updateCardProfile(Ljava/lang/String;[BLjava/lang/String;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, p2}, Lcom/mastercard/mpsdk/database/a/b;->a([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p2

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/database/a/b;->b(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mastercard/mpsdk/database/a/b;->a(Ljava/lang/String;)[B

    move-result-object v2

    const-string v3, "UPDATE card_profiles_lists SET profile_data = ? , profile_data_version = ? , checksum = ? WHERE  card_id = ? "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    invoke-virtual {v1, v3, p2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p2, 0x2

    invoke-virtual {v1, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p2, 0x3

    invoke-virtual {v1, p2, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p2, 0x4

    invoke-virtual {v1, p2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    iget-object p1, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/database/b/b;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized updateTransactionCredentialStatusForCardId(Ljava/lang/String;IILjava/lang/String;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/f;->b()V

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const-string v4, "SELECT * FROM transaction_credentials_list WHERE card_id = ? AND atc = ? "

    invoke-virtual {v1, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/database/a/f;->a(Landroid/database/Cursor;)Lcom/mastercard/mpsdk/database/b/g;

    move-result-object v1

    iget-object v3, v1, Lcom/mastercard/mpsdk/database/b/g;->c:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget-object v6, v1, Lcom/mastercard/mpsdk/database/b/g;->h:Ljava/lang/String;

    iget-object v1, v1, Lcom/mastercard/mpsdk/database/b/g;->b:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;)[B

    move-result-object v1

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "UPDATE transaction_credentials_list SET credential_status = ? , checksum = ?  , time_stamp = ?  WHERE  card_id = ? AND atc = ? "

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    int-to-long v3, p3

    invoke-virtual {v0, v5, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    invoke-virtual {v0, v2, v1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 p3, 0x3

    invoke-virtual {v0, p3, p4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p3, 0x4

    invoke-virtual {v0, p3, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p1, 0x5

    int-to-long p2, p2

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final bridge synthetic usingOptionalDatabaseUpgradeHelper(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->d:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    return-object p0
.end method

.method public final bridge synthetic usingOptionalSecurityIncidentService(Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->e:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    return-object p0
.end method

.method public final declared-synchronized wipeAllData()V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/e;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "mobile_keys"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v3, "card_profiles_lists"

    invoke-virtual {v1, v3, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/b/b;->a()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "transaction_credentials_list"

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_1

    const-string v1, "DELETE FROM transaction_credentials_list;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "transaction_logs"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->i:Lcom/mastercard/mpsdk/database/a/h;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/h;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "wallet_data_container"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->k:Lcom/mastercard/mpsdk/database/a/d;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/d;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "environment_data_container"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized wipeCardProfileAndRelatedData(Ljava/lang/String;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->f:Lcom/mastercard/mpsdk/database/a/b;

    iget-object v1, v0, Lcom/mastercard/mpsdk/database/a/b;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "card_id = ? "

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v5, "card_profiles_lists"

    invoke-static {v1, v5, v2, v4}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_1

    const-string v2, "DELETE FROM card_profiles_lists WHERE card_id = ? ;"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    invoke-virtual {v1, v3, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result p1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    if-eqz p1, :cond_0

    iget-object p1, v0, Lcom/mastercard/mpsdk/database/a/b;->e:Lcom/mastercard/mpsdk/database/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/database/b/b;->a()V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v0, "Unable to update the database"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized wipeMobileKeysForKeySetId(Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->h:Lcom/mastercard/mpsdk/database/a/e;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/e;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DELETE FROM mobile_keys WHERE mobile_keyset_id = ? ;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized wipeOldAndSaveNewTransactionCredentialsForCardId(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;",
            ">;)V"
        }
    .end annotation

    move-object v1, p0

    monitor-enter p0

    :try_start_0
    iget-object v0, v1, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    iget-object v11, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    monitor-enter v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v13, p1

    invoke-virtual {v0, v13, v3, v12}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :cond_0
    move-object/from16 v13, p1

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;->getTransactionCredentialId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;->getTransactionCredentialStatus()I

    move-result v5

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;->getAtc()I

    move-result v6

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;->getTimeStamp()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;->getSerializedTransactionCredential()[B

    move-result-object v8

    move-object v2, v0

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    move-object v10, v12

    invoke-virtual/range {v2 .. v10}, Lcom/mastercard/mpsdk/database/a/f;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;[BLjava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :cond_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized wipeTransactionCredentialsForCardId(Ljava/lang/String;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->g:Lcom/mastercard/mpsdk/database/a/f;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/f;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "card_id = ? "

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const-string v4, "transaction_credentials_list"

    invoke-static {v0, v4, v1, v3}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_1

    const-string v1, "DELETE FROM transaction_credentials_list WHERE card_id = ? ;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    invoke-virtual {v0, v2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v0, "Unable to update the database"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized wipeTransactionLogByIdForCardId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DELETE FROM transaction_logs WHERE card_id = ? AND transaction_log_id = ? ;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p1, 0x2

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized wipeTransactionLogsForCardId(Ljava/lang/String;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/database/a;->j:Lcom/mastercard/mpsdk/database/a/g;

    iget-object v0, v0, Lcom/mastercard/mpsdk/database/a/g;->c:Lcom/mastercard/mpsdk/database/a/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/database/a/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "card_id = ? "

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const-string v4, "transaction_logs"

    invoke-static {v0, v4, v1, v3}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_1

    const-string v1, "DELETE FROM transaction_logs WHERE card_id = ? ;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    invoke-virtual {v0, v2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v0, "Unable to update the database"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final bridge synthetic withDatabaseCrypto(Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/database/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    return-object p0
.end method

.method public final withMaxTransactionLogsCount(I)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/database/a;->l:I

    return-object p0
.end method
