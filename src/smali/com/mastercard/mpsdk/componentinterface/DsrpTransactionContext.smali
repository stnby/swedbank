.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getCountryCode()I
.end method

.method public abstract getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;
.end method

.method public abstract getCurrencyCode()I
.end method

.method public abstract getOptionalUnpredictableNumber()I
.end method

.method public abstract getTransactionAmount()J
.end method

.method public abstract getTransactionDate()Ljava/util/Date;
.end method

.method public abstract getTransactionType()B
.end method
