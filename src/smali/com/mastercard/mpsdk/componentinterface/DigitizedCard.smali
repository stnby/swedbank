.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getCardCountryCode()[B
.end method

.method public abstract getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
.end method

.method public abstract getDigitizedCardId()[B
.end method

.method public abstract getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;
.end method

.method public abstract getPan()[B
.end method

.method public abstract getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
.end method

.method public abstract getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
.end method

.method public abstract isTransactionIdRequired()Z
.end method
