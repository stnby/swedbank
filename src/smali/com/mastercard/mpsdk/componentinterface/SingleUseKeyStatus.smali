.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;
    }
.end annotation


# virtual methods
.method public abstract getAtc()I
.end method

.method public abstract getStatus()Ljava/lang/String;
.end method

.method public abstract getTimestamp()Ljava/lang/String;
.end method
