.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;
    }
.end annotation


# virtual methods
.method public abstract getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;
.end method

.method public abstract getResult()Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;
.end method
