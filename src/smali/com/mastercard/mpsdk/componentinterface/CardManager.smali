.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/CardManager;
.super Ljava/lang/Object;


# virtual methods
.method public abstract activateCard(Lcom/mastercard/mpsdk/componentinterface/Card;)V
.end method

.method public abstract changeWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract deleteAllTransactionCredentials()V
.end method

.method public abstract deleteCard(Lcom/mastercard/mpsdk/componentinterface/Card;)Ljava/lang/String;
.end method

.method public abstract getAllCards()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Card;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;
.end method

.method public abstract getCredentialsReplenishmentPolicy()Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;
.end method

.method public abstract setWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract suspendCard(Lcom/mastercard/mpsdk/componentinterface/Card;)V
.end method
