.class public final enum Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field public static final enum DECLINED:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field public static final enum ERROR_INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field public static final enum ERROR_INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field public static final enum ERROR_UNEXPECTED_DATA:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field public static final enum ERROR_WRONG_STATE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field public static final enum INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field public static final enum OK:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const-string v1, "OK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->OK:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const-string v1, "INTERNAL_ERROR"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const-string v1, "ERROR_UNEXPECTED_DATA"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_UNEXPECTED_DATA:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const-string v1, "ERROR_INVALID_INPUT"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const-string v1, "ERROR_WRONG_STATE"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_WRONG_STATE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const-string v1, "ERROR_INCOMPATIBLE_PROFILE"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const-string v1, "DECLINED"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->DECLINED:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->OK:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_UNEXPECTED_DATA:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_WRONG_STATE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->DECLINED:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    aput-object v1, v0, v8

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    return-object v0
.end method
