.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAtc()I
.end method

.method public abstract getCardId()Ljava/lang/String;
.end method

.method public abstract getSerializedTransactionCredential()[B
.end method

.method public abstract getTimeStamp()Ljava/lang/String;
.end method

.method public abstract getTransactionCredentialId()Ljava/lang/String;
.end method

.method public abstract getTransactionCredentialStatus()I
.end method
