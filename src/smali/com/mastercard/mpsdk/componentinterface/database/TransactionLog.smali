.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAmount()J
.end method

.method public abstract getCardId()Ljava/lang/String;
.end method

.method public abstract getCryptogramFormat()B
.end method

.method public abstract getCurrencyCode()I
.end method

.method public abstract getDate()J
.end method

.method public abstract getTransactionId()[B
.end method

.method public abstract getUnpredictableNumber()[B
.end method

.method public abstract isValid()Z
.end method
