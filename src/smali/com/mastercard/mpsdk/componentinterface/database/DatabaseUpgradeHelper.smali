.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getDatabaseUpgradeCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;
.end method

.method public abstract getMpaKey()[B
.end method

.method public abstract serializeTransactionCredential(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)[B
.end method

.method public abstract serializeTransactionLog(Ljava/lang/String;[BJJIB[B)[B
.end method
