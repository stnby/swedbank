.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/database/DatabaseInitParams;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getCmsMpaId()[B
.end method

.method public abstract getMpaFingerPrint()[B
.end method

.method public abstract getRnsMpaId()[B
.end method

.method public abstract getUrlRemoteManagement()Ljava/lang/String;
.end method

.method public abstract isValid()Z
.end method

.method public abstract setRnsMpaId([B)V
.end method

.method public abstract setUrlRemoteManagement(Ljava/lang/String;)V
.end method
