.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAllCardIds()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCardProfileByCardId(Ljava/lang/String;)[B
.end method

.method public abstract getCardProfileVersionByCardId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getCardStateByCardId(Ljava/lang/String;)I
.end method

.method public abstract getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
.end method

.method public abstract getFirstTransactionCredentialIdForCardIdWithStatus(Ljava/lang/String;I)Ljava/lang/String;
.end method

.method public abstract getMobileKeySetId()Ljava/lang/String;
.end method

.method public abstract getMobileKeyTypesInKeySet(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRemoteManagementUrl()Ljava/lang/String;
.end method

.method public abstract getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B
.end method

.method public abstract getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTransactionCredentialStatusForCardId(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract getTransactionCredentialTimeStampForCardId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getTransactionLogByIdForCardId(Ljava/lang/String;I)[B
.end method

.method public abstract getTransactionLogIdsForCardId(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getWalletData()Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
.end method

.method public abstract getWalletDataForCardId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
.end method

.method public abstract initialize(Landroid/content/Context;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
.end method

.method public abstract rolloverData(Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;[B[B[B[B)V
.end method

.method public abstract rolloverMobileKeys(Ljava/lang/String;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract rolloverTransactionCredentialsAndCardProfiles(Ljava/util/HashMap;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[B>;>;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation
.end method

.method public abstract saveCardProfile(Ljava/lang/String;[BLjava/lang/String;)V
.end method

.method public abstract saveCardStateByCardId(Ljava/lang/String;I)V
.end method

.method public abstract saveEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V
.end method

.method public abstract saveRemoteManagementUrl(Ljava/lang/String;)V
.end method

.method public abstract saveTransactionLogForCardId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
.end method

.method public abstract saveWalletData(Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
.end method

.method public abstract saveWalletDataForCardId(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
.end method

.method public abstract updateCardProfile(Ljava/lang/String;[BLjava/lang/String;)V
.end method

.method public abstract updateTransactionCredentialStatusForCardId(Ljava/lang/String;IILjava/lang/String;)V
.end method

.method public abstract usingOptionalDatabaseUpgradeHelper(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
.end method

.method public abstract usingOptionalSecurityIncidentService(Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
.end method

.method public abstract wipeAllData()V
.end method

.method public abstract wipeCardProfileAndRelatedData(Ljava/lang/String;)V
.end method

.method public abstract wipeMobileKeysForKeySetId(Ljava/lang/String;)V
.end method

.method public abstract wipeOldAndSaveNewTransactionCredentialsForCardId(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract wipeTransactionCredentialsForCardId(Ljava/lang/String;)V
.end method

.method public abstract wipeTransactionLogByIdForCardId(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract wipeTransactionLogsForCardId(Ljava/lang/String;)V
.end method

.method public abstract withDatabaseCrypto(Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
.end method

.method public abstract withMaxTransactionLogsCount(I)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
.end method
