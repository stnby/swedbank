.class public final enum Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

.field public static final enum INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

.field public static final enum UNINITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;


# instance fields
.field private mDatabaseState:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    const-string v1, "UNINITIALIZED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->UNINITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    const-string v1, "INITIALIZED"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->UNINITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->mDatabaseState:I

    return-void
.end method

.method public static valueOf(I)Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    sget-object p0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->UNINITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    return-object p0

    :pswitch_0
    sget-object p0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    return-object p0

    :pswitch_1
    sget-object p0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->UNINITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseState;->mDatabaseState:I

    return v0
.end method
