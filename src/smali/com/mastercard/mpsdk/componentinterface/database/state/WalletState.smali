.class public final enum Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

.field public static final enum NOT_REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

.field public static final enum REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;


# instance fields
.field final mWalletState:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    const-string v1, "NOT_REGISTER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->NOT_REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    const-string v1, "REGISTER"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->NOT_REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->mWalletState:I

    return-void
.end method

.method public static valueOf(I)Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    sget-object p0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->NOT_REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    return-object p0

    :pswitch_0
    sget-object p0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    return-object p0

    :pswitch_1
    sget-object p0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->NOT_REGISTER:Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/componentinterface/database/state/WalletState;->mWalletState:I

    return v0
.end method
