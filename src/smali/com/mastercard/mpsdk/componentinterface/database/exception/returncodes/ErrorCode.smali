.class public final enum Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ReturnCode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;",
        ">;",
        "Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ReturnCode;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum ANOTHER_REQUEST_IS_ALREADY_IN_PROCESS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum CH_VALIDATION_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum CRYPTO_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum DIGITIZED_CARD_ID_NOT_FOUND:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum FAILED_DUE_TO_UNKNOWN_REASON:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum FAILED_TO_FETCH_SESSION:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum GCM_REGISTRATION_FAILED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_CONTACTLESS_CREDENTIALS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_DIGITIZED_CARD_ID:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_DIGITIZED_CARD_PROFILE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_DIGITIZED_CARD_SINGLE_USE_KEY:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_LOG_RECORD_FORMAT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_PROTOCOL_MESSAGE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum INVALID_REMOTE_PAYMENT_CREDENTIALS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum LDE_ALREADY_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum LDE_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum LDE_NOT_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum LOGGING_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum NETWORK_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum NOT_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum NO_SESSION_KEYS_AVAILABLE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum NO_USER_INFORMATION_FOUND:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum PROVISIONING_SUK_FAILED_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum REMOTE_MANAGEMENT_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum ROLLOVER_IN_PROGRESS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum TRANSACTION_STORAGE_LIMIT_REACH:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum UNEXPECTED_DATA:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum UNEXPECTED_PROTOCOL_MESSAGE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

.field public static final enum WRONG_STATE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_INPUT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_DIGITIZED_CARD_ID"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_ID:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_DIGITIZED_CARD_PROFILE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_PROFILE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_DIGITIZED_CARD_SINGLE_USE_KEY"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_SINGLE_USE_KEY:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "DIGITIZED_CARD_ID_NOT_FOUND"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->DIGITIZED_CARD_ID_NOT_FOUND:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "LDE_NOT_INITIALIZED"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LDE_NOT_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "LDE_ALREADY_INITIALIZED"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LDE_ALREADY_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_LOG_RECORD_FORMAT"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_LOG_RECORD_FORMAT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "NO_SESSION_KEYS_AVAILABLE"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NO_SESSION_KEYS_AVAILABLE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "NO_USER_INFORMATION_FOUND"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NO_USER_INFORMATION_FOUND:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "WRONG_STATE"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->WRONG_STATE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INCOMPATIBLE_PROFILE"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_REMOTE_PAYMENT_CREDENTIALS"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_REMOTE_PAYMENT_CREDENTIALS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_CONTACTLESS_CREDENTIALS"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_CONTACTLESS_CREDENTIALS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "NOT_INITIALIZED"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NOT_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INTERNAL_ERROR"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "REMOTE_MANAGEMENT_ERROR"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->REMOTE_MANAGEMENT_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "UNEXPECTED_DATA"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->UNEXPECTED_DATA:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "UNEXPECTED_PROTOCOL_MESSAGE"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->UNEXPECTED_PROTOCOL_MESSAGE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "LOGGING_ERROR"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LOGGING_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "CH_VALIDATION_ERROR"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->CH_VALIDATION_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "INVALID_PROTOCOL_MESSAGE"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_PROTOCOL_MESSAGE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "CRYPTO_ERROR"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->CRYPTO_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "LDE_ERROR"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LDE_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "PROVISIONING_SUK_FAILED_ERROR"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->PROVISIONING_SUK_FAILED_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "TRANSACTION_STORAGE_LIMIT_REACH"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->TRANSACTION_STORAGE_LIMIT_REACH:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "GCM_REGISTRATION_FAILED"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->GCM_REGISTRATION_FAILED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "NETWORK_ERROR"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NETWORK_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "ANOTHER_REQUEST_IS_ALREADY_IN_PROCESS"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->ANOTHER_REQUEST_IS_ALREADY_IN_PROCESS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "FAILED_TO_FETCH_SESSION"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->FAILED_TO_FETCH_SESSION:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "FAILED_DUE_TO_UNKNOWN_REASON"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->FAILED_DUE_TO_UNKNOWN_REASON:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const-string v1, "ROLLOVER_IN_PROGRESS"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->ROLLOVER_IN_PROGRESS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v0, 0x20

    new-array v0, v0, [Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_ID:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_PROFILE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_SINGLE_USE_KEY:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->DIGITIZED_CARD_ID_NOT_FOUND:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LDE_NOT_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LDE_ALREADY_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_LOG_RECORD_FORMAT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v9

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NO_SESSION_KEYS_AVAILABLE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v10

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NO_USER_INFORMATION_FOUND:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v11

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->WRONG_STATE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v12

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v13

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_REMOTE_PAYMENT_CREDENTIALS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    aput-object v1, v0, v14

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_CONTACTLESS_CREDENTIALS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NOT_INITIALIZED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->REMOTE_MANAGEMENT_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->UNEXPECTED_DATA:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->UNEXPECTED_PROTOCOL_MESSAGE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LOGGING_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->CH_VALIDATION_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_PROTOCOL_MESSAGE:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->CRYPTO_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->LDE_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->PROVISIONING_SUK_FAILED_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->TRANSACTION_STORAGE_LIMIT_REACH:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->GCM_REGISTRATION_FAILED:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->NETWORK_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->ANOTHER_REQUEST_IS_ALREADY_IN_PROCESS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->FAILED_TO_FETCH_SESSION:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->FAILED_DUE_TO_UNKNOWN_REASON:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->ROLLOVER_IN_PROGRESS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    return-object v0
.end method
