.class public Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput;
.super Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeCheckedException;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    invoke-direct {p0, p1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeCheckedException;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V
    .locals 0

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeCheckedException;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V

    return-void
.end method
