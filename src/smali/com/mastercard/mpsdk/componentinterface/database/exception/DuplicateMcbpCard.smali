.class public Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateMcbpCard;
.super Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;


# instance fields
.field private final mtokenUniqueReference:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_ID:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    invoke-direct {p0, p2, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateMcbpCard;->mtokenUniqueReference:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public gettokenUniqueReference()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateMcbpCard;->mtokenUniqueReference:Ljava/lang/String;

    return-object v0
.end method
