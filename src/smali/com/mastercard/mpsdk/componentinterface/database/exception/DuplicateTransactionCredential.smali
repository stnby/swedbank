.class public Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateTransactionCredential;
.super Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;


# instance fields
.field private final mAtc:I

.field private final mtokenUniqueReference:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_ID:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    invoke-direct {p0, p3, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateTransactionCredential;->mtokenUniqueReference:Ljava/lang/String;

    iput p2, p0, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateTransactionCredential;->mAtc:I

    return-void
.end method


# virtual methods
.method public getAtc()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateTransactionCredential;->mAtc:I

    return v0
.end method

.method public getTokenUniqueReference()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/componentinterface/database/exception/DuplicateTransactionCredential;->mtokenUniqueReference:Ljava/lang/String;

    return-object v0
.end method
