.class public Lcom/mastercard/mpsdk/componentinterface/http/HttpException;
.super Ljava/lang/Exception;


# static fields
.field public static final HTTP_TIMEOUT_OCCURRED:I = 0x453

.field public static final SSL_ERROR:I = 0x452

.field public static final UNKNOWN_HTTP_ERROR:I = 0x515


# instance fields
.field UNKNOWN_HTTP_ERROR_CODE:I

.field private errorCode:I

.field private errorMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x451

    iput v0, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->UNKNOWN_HTTP_ERROR_CODE:I

    const/16 v0, 0x515

    iput v0, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorCode:I

    iput p1, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorCode:I

    iput-object p2, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorMessage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x451

    iput v0, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->UNKNOWN_HTTP_ERROR_CODE:I

    const/16 v0, 0x515

    iput v0, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorCode:I

    iput-object p1, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorMessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorCode:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;->errorMessage:Ljava/lang/String;

    return-object v0
.end method
