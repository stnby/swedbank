.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/http/HttpRequest;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getRequestMethod()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
.end method

.method public abstract getRequestPayload()Ljava/lang/String;
.end method

.method public abstract getRequestPropertyMap()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method
