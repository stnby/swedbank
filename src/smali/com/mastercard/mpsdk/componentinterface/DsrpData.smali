.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/DsrpData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAip()[B
.end method

.method public abstract getCiacDecline()[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
.end method

.method public abstract getCvrMaskAnd()[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getExpiryDate()[B
.end method

.method public abstract getIssuerApplicationData()[B
.end method

.method public abstract getPanSequenceNumber()[B
.end method

.method public abstract getPar()[B
.end method

.method public abstract getTrack2EquivalentData()[B
.end method

.method public abstract getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;
.end method

.method public abstract getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
.end method
