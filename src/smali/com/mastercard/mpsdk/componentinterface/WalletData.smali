.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/WalletData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
.end method

.method public abstract getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;
.end method

.method public abstract getCvmResetTimeout()I
.end method

.method public abstract getDualTapResetTimeout()I
.end method

.method public abstract getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;
.end method
