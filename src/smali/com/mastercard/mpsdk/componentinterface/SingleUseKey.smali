.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAtc()I
.end method

.method public abstract getHash()[B
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getIdn()[B
.end method

.method public abstract getInfo()[B
.end method

.method public abstract getSessionKeyContactlessMd()[B
.end method

.method public abstract getSessionKeyContactlessUmd()[B
.end method

.method public abstract getSessionKeyRemotePaymentMd()[B
.end method

.method public abstract getSessionKeyRemotePaymentUmd()[B
.end method

.method public abstract getSukContactlessUmd()[B
.end method

.method public abstract getSukRemotePaymentUmd()[B
.end method

.method public abstract getTimestamp()Ljava/lang/String;
.end method

.method public abstract setStatus(I)V
.end method

.method public abstract wipe()V
.end method
