.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/MobileKeys;
.super Ljava/lang/Object;


# static fields
.field public static final DEK_KEY_NAME:Ljava/lang/String; = "DEK_KEY"

.field public static final MAC_KEY_NAME:Ljava/lang/String; = "MAC_KEY"

.field public static final TRANSPORT_KEY_NAME:Ljava/lang/String; = "TRANSPORT_KEY"


# virtual methods
.method public abstract getDataEncryptionKey()[B
.end method

.method public abstract getMacKey()[B
.end method

.method public abstract getTransportKey()[B
.end method
