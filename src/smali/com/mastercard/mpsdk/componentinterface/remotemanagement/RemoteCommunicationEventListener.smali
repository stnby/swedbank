.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onCardPinReset(Ljava/lang/String;)V
.end method

.method public abstract onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onChangeCardPinSucceeded(Ljava/lang/String;)V
.end method

.method public abstract onChangeWalletMobilePinStarted(Ljava/lang/String;)V
.end method

.method public abstract onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onChangeWalletPinSucceeded()V
.end method

.method public abstract onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V
.end method

.method public abstract onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onRequestSessionSuccess()V
.end method

.method public abstract onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onSetCardPinSucceeded(Ljava/lang/String;)V
.end method

.method public abstract onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onSetWalletPinSucceeded()V
.end method

.method public abstract onSystemHealthCompleted()V
.end method

.method public abstract onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onTaskStatusReceived(Ljava/lang/String;)V
.end method

.method public abstract onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onWalletPinReset()V
.end method
