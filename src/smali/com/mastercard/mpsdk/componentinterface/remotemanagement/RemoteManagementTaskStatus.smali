.class public final enum Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

.field public static final enum COMPLETED:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

.field public static final enum FAILED:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

.field public static final enum IN_PROGRESS:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

.field public static final enum PENDING:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    const-string v1, "PENDING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->PENDING:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    const-string v1, "IN_PROGRESS"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->IN_PROGRESS:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    const-string v1, "COMPLETED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->COMPLETED:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    const-string v1, "FAILED"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->FAILED:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->PENDING:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->IN_PROGRESS:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->COMPLETED:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->FAILED:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->$VALUES:[Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementTaskStatus;

    return-object v0
.end method
