.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getRegistrationRequestData([BLcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RegistrationRequestParameters;
.end method

.method public abstract getSetPinRequestData(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;
.end method

.method public abstract setRegistrationResponseData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;Ljava/lang/String;)V
.end method
