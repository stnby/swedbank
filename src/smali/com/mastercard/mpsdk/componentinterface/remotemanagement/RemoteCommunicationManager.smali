.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;


# virtual methods
.method public abstract cancelPendingRequests()Z
.end method

.method public abstract getCurrentRequestId()Ljava/lang/String;
.end method

.method public abstract getWalletIdentificationDataProvider()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;
.end method

.method public abstract initialize(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
.end method

.method public abstract processNotificationData(Ljava/lang/String;)V
.end method

.method public abstract requestChangePin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract requestChangeWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract requestDeleteCard(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;
.end method

.method public abstract requestReplenish(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;
.end method

.method public abstract requestSetPin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract requestSetWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract requestSystemHealth()Ljava/lang/String;
.end method

.method public abstract requestTaskStatusUpdate(Ljava/lang/String;)V
.end method
