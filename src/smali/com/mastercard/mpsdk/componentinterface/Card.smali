.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/Card;
.super Ljava/lang/Object;


# virtual methods
.method public abstract changePin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract deleteTransactionCredentials()V
.end method

.method public abstract getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
.end method

.method public abstract getCardId()Ljava/lang/String;
.end method

.method public abstract getCardState()Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;
.end method

.method public abstract getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;
.end method

.method public abstract getCdCvmModel(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;)Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
.end method

.method public abstract getCvmResetTimeout()I
.end method

.method public abstract getDigitizedCardId()Ljava/lang/String;
.end method

.method public abstract getDisplayablePanDigits()Ljava/lang/String;
.end method

.method public abstract getDualTapResetTimeout()I
.end method

.method public abstract getHostUmdConfig(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;)Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
.end method

.method public abstract getNumberOfAvailableCredentials()I
.end method

.method public abstract getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;
.end method

.method public abstract getSupportedAids()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTransactionLog()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
.end method

.method public abstract getWalletDataForCard()Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
.end method

.method public abstract isContactlessSupported()Z
.end method

.method public abstract isDsrpSupported()Z
.end method

.method public abstract isOnDeviceCvmSupported()Z
.end method

.method public abstract processDsrpTransaction(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;
.end method

.method public abstract replenishCredentials()Ljava/lang/String;
.end method

.method public abstract saveWalletDataForCard(Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
.end method

.method public abstract setPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
.end method

.method public abstract stopContactlessTransaction()V
.end method
