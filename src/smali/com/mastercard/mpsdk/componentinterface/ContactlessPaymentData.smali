.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAid()[B
.end method

.method public abstract getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
.end method

.method public abstract getCdol1RelatedDataLength()I
.end method

.method public abstract getCiacDecline()[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getCiacDeclineOnPpms()[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
.end method

.method public abstract getCvrMaskAnd()[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getGpoResponse()[B
.end method

.method public abstract getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end method

.method public abstract getIssuerApplicationData()[B
.end method

.method public abstract getPaymentFci()[B
.end method

.method public abstract getPinIvCvc3Track2()[B
.end method

.method public abstract getPpseFci()[B
.end method

.method public abstract getRecords()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTrack1ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
.end method

.method public abstract getTrack2ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
.end method

.method public abstract getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
.end method

.method public abstract isTransitSupported()Z
.end method

.method public abstract isUsAipMaskingSupported()Z
.end method
