.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;
.super Ljava/lang/Object;


# virtual methods
.method public abstract buildRemoteServiceRequest(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;I)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;
.end method

.method public abstract calculateAuthenticationCode(Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;)[B
.end method

.method public abstract createSignedRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;
.end method

.method public abstract decryptPushedRemoteData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)[B
.end method

.method public abstract decryptRemoteServiceResponse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;)[B
.end method

.method public abstract encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;
.end method

.method public abstract encryptPinBlockWithRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
.end method

.method public abstract exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end method

.method public abstract exchangeIccKekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end method

.method public abstract exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
.end method

.method public abstract generateEncryptedRgk()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
.end method

.method public abstract getLocalDekEncryptedIdn(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end method
