.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getDatabaseCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;
.end method

.method public abstract getDatabaseStorageKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.end method

.method public abstract getDatabaseStorageMacKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.end method

.method public abstract getDatabaseUpgradeCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;
.end method

.method public abstract getLocalDataEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.end method

.method public abstract getRemoteManagementCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;
.end method

.method public abstract getRemoteManagementKeyEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.end method

.method public abstract getTransactionCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;
.end method

.method public abstract getWalletDataCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;
.end method

.method public abstract getWalletDataEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.end method

.method public abstract wipeCryptoParameters()Z
.end method
