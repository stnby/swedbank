.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
    }
.end annotation


# virtual methods
.method public abstract buildComputeCcCryptograms([BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
.end method

.method public abstract buildGenerateAcCryptograms([BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
.end method

.method public abstract buildSignedDynamicApplicationData([B[B[B[BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)[B
.end method

.method public abstract deriveSessionKeyFromSingleUseKey(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end method

.method public abstract encryptSessionKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end method

.method public abstract initIccKey(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)I
.end method

.method public abstract sha1([B)[B
.end method

.method public abstract sha256([B)[B
.end method
