.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;
.end method

.method public abstract getRemoteManagementServiceUrl()Ljava/lang/String;
.end method

.method public abstract hasCommunicationParameters()Z
.end method

.method public abstract setCommunicationParameters(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;Ljava/lang/String;)V
.end method
