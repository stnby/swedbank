.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getEncryptedDeviceFingerPrint()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
.end method

.method public abstract getPaymentAppInstanceId()[B
.end method

.method public abstract getPaymentAppProviderId()[B
.end method

.method public abstract onKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V
.end method
