.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;


# virtual methods
.method public abstract abandonRollover()V
.end method

.method public abstract getCurrentKeyId()[B
.end method

.method public abstract isRolloverInProgress()Z
.end method

.method public abstract rolloverComplete()V
.end method

.method public abstract startRollover()[B
.end method

.method public abstract wipeKey()Z
.end method
