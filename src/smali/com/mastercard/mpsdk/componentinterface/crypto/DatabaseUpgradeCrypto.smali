.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;
.super Ljava/lang/Object;


# virtual methods
.method public abstract encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end method

.method public abstract encryptDataUsingRemoteKekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
.end method
