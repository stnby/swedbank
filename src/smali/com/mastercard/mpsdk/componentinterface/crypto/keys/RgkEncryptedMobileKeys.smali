.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
.end method

.method public abstract getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
.end method

.method public abstract getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
.end method

.method public abstract getKeySetId()Ljava/lang/String;
.end method
