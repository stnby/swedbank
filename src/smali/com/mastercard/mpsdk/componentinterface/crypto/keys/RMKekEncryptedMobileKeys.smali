.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
.end method

.method public abstract getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
.end method

.method public abstract getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
.end method

.method public abstract getKeySetId()Ljava/lang/String;
.end method
