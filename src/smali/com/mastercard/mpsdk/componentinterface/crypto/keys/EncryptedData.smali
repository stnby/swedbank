.class public abstract Lcom/mastercard/mpsdk/componentinterface/crypto/keys/EncryptedData;
.super Ljava/lang/Object;


# instance fields
.field protected final mEncryptedData:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/EncryptedData;->mEncryptedData:[B

    return-void
.end method


# virtual methods
.method public getEncryptedData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/EncryptedData;->mEncryptedData:[B

    return-object v0
.end method

.method public wipe()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/EncryptedData;->mEncryptedData:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/EncryptedData;->mEncryptedData:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    :cond_0
    return-void
.end method
