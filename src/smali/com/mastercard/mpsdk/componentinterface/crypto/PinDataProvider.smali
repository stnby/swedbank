.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
.end method

.method public abstract getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
.end method

.method public abstract onKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V
.end method
