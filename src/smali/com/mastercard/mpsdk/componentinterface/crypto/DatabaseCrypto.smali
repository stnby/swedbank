.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;
.super Ljava/lang/Object;


# virtual methods
.method public abstract encryptDataForStorage([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;
.end method

.method public abstract generateMac([B)[B
.end method

.method public abstract generateMac([B[B)[B
.end method

.method public abstract isMacValid([B[B)Z
.end method

.method public abstract unencryptStoredDataForUse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B
.end method
