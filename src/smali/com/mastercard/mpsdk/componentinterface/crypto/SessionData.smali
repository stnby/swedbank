.class public interface abstract Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getExpiryTimestamp()Ljava/lang/String;
.end method

.method public abstract getPendingAction()Ljava/lang/String;
.end method

.method public abstract getSessionCode()[B
.end method

.method public abstract getTokenUniqueReference()Ljava/lang/String;
.end method

.method public abstract getValidForSeconds()I
.end method

.method public abstract getVersion()Ljava/lang/String;
.end method
