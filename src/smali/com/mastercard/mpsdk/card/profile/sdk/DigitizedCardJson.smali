.class public Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;
.super Ljava/lang/Object;


# instance fields
.field public accountType:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "accountType"
    .end annotation
.end field

.field public cardCountryCode:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cardCountryCode"
    .end annotation
.end field

.field public contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;
    .annotation runtime Lflexjson/h;
        a = "contactlessPaymentData"
    .end annotation
.end field

.field public digitizedCardId:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "digitizedCardId"
    .end annotation
.end field

.field public dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;
    .annotation runtime Lflexjson/h;
        a = "dsrpData"
    .end annotation
.end field

.field public isTransactionIdRequired:Z
    .annotation runtime Lflexjson/h;
        a = "isTransactionIdRequired"
    .end annotation
.end field

.field public pan:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "pan"
    .end annotation
.end field

.field public productType:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "productType"
    .end annotation
.end field

.field public profileVersion:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "version"
    .end annotation
.end field

.field public walletRelatedData:Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;
    .annotation runtime Lflexjson/h;
        a = "walletRelatedData"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->profileVersion:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDigitizedCardId()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->digitizedCardId:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getCardCountryCode()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->cardCountryCode:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getPan()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->pan:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->accountType:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->productType:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->isTransactionIdRequired()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->isTransactionIdRequired:Z

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->assignWalletRelatedData(Lcom/mastercard/mpsdk/componentinterface/WalletData;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->assignDsrpData(Lcom/mastercard/mpsdk/componentinterface/DsrpData;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->assignContactlessData(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;)V

    return-void
.end method

.method private assignAlternateContactlessData(Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getPaymentFci()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->paymentFci:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getAid()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->aid:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getCiacDecline()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->cvrMaskAnd:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getgpoResponse()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->gpoResponse:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private assignContactlessData(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;)V
    .locals 2

    if-eqz p1, :cond_2

    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cvrMaskAnd:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCiacDeclineOnPpms()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ciacDeclineOnPpms:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCiacDecline()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAid()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->aid:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCdol1RelatedDataLength()I

    move-result v1

    iput v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cdol1RelatedDataLength:I

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cvmModel:Ljava/lang/String;

    :cond_0
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->umdGeneration:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getGpoResponse()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->gpoResponse:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->iccPrivateKeyCrtComponents:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPaymentFci()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->paymentFci:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPpseFci()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ppseFci:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIssuerApplicationData()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->issuerApplicationData:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPinIvCvc3Track2()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->pinIvCvc3Track2:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->isTransitSupported()Z

    move-result v1

    iput-boolean v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->isTransitSupported:Z

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->isUsAipMaskingSupported()Z

    move-result v1

    iput-boolean v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->isUsAipMaskingSupported:Z

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->assignAlternateContactlessData(Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getTrack1ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->assignTrack1ConstructionData(Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getTrack2ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->assignTrack2ConstructionData(Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getRecords()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->assignRecords(Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method private assignDsrpData(Lcom/mastercard/mpsdk/componentinterface/DsrpData;)V
    .locals 2

    if-eqz p1, :cond_2

    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCvrMaskAnd()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->cvrMaskAnd:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCiacDecline()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->ciacDecline:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getAip()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->aip:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getExpiryDate()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->expiryDate:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getIssuerApplicationData()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->issuerApplicationData:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getPanSequenceNumber()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->panSequenceNumber:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getPar()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->par:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getTrack2EquivalentData()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->track2EquivalentData:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->ucafVersion:Ljava/lang/String;

    :cond_0
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->umdGeneration:Ljava/lang/String;

    :cond_1
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->cvmModel:Ljava/lang/String;

    :cond_2
    return-void
.end method

.method private assignRecords(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/Records;

    new-instance v3, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    invoke-direct {v3}, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;-><init>()V

    aput-object v3, v0, v1

    aget-object v3, v0, v1

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/Records;->getRecordNumber()B

    move-result v4

    iput v4, v3, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;->recordNumber:I

    aget-object v3, v0, v1

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/Records;->getSfi()[B

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;->sfi:Ljava/lang/String;

    aget-object v3, v0, v1

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/Records;->getRecordValue()[B

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;->recordValue:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iput-object v0, p1, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->records:[Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    :cond_1
    return-void
.end method

.method private assignTrack1ConstructionData(Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getNAtc()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->nAtc:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getPUnAtc()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->pUnAtc:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getPCvc3()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->pCvc3:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getTrackData()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->trackData:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private assignTrack2ConstructionData(Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track2ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track2ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getNAtc()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->nAtc:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track2ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getPUnAtc()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->pUnAtc:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track2ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getPCvc3()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->pCvc3:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track2ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;->getTrackData()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->buildValue([B)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->trackData:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private assignWalletRelatedData(Lcom/mastercard/mpsdk/componentinterface/WalletData;)V
    .locals 2

    if-eqz p1, :cond_1

    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->walletRelatedData:Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->walletRelatedData:Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;->cardholderValidator:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->walletRelatedData:Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getCvmResetTimeout()I

    move-result v1

    iput v1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;->cvmResetTimeout:I

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->walletRelatedData:Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getDualTapResetTimeout()I

    move-result p1

    iput p1, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;->dualTapResetTimeout:I

    :cond_1
    return-void
.end method

.method private buildValue([B)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public getCard([B)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
    .locals 2

    new-instance v0, Lflexjson/j;

    invoke-direct {v0}, Lflexjson/j;-><init>()V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    const-class p1, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-virtual {v0, v1, p1}, Lflexjson/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    invoke-direct {v0, p0, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;)V

    return-object v0
.end method

.method public getContent(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;)[B
    .locals 2

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "contactlessPaymentData.records"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-direct {v1, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;-><init>(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;)V

    invoke-virtual {v0, v1}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    return-object p1
.end method
