.class Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->getCard([B)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

.field final synthetic val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->this$0:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iput-object p2, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCardCountryCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->cardCountryCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;

    invoke-direct {v1, p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;)V

    return-object v1
.end method

.method public getDigitizedCardId()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->digitizedCardId:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;)V

    return-object v0
.end method

.method public getPan()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->pan:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->profileVersion:Ljava/lang/String;

    const-string v1, "1.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    return-object v0

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    return-object v0
.end method

.method public getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$1;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;)V

    return-object v0
.end method

.method public isTransactionIdRequired()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-boolean v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->isTransactionIdRequired:Z

    return v0
.end method
