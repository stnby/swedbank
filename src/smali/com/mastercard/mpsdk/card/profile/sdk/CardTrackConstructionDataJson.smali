.class Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;
.super Ljava/lang/Object;


# instance fields
.field public nAtc:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "nAtc"
    .end annotation
.end field

.field public pCvc3:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "pCvc3"
    .end annotation
.end field

.field public pUnAtc:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "pUnAtc"
    .end annotation
.end field

.field public trackData:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "trackData"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
