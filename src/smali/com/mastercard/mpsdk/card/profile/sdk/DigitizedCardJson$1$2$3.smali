.class Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->getTrack1ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;

.field final synthetic val$track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;->this$2:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;

    iput-object p2, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;->val$track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNAtc()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;->val$track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->nAtc:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPCvc3()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;->val$track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->pCvc3:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPUnAtc()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;->val$track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->pUnAtc:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getTrackData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;->val$track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;->trackData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method
