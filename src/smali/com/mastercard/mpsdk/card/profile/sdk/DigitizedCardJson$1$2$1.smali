.class Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;

.field final synthetic val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->this$2:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;

    iput-object p2, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->aid:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->cvrMaskAnd:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->paymentFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getgpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;->gpoResponse:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method
