.class Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;
.super Ljava/lang/Object;


# instance fields
.field public aid:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "aid"
    .end annotation
.end field

.field public ciacDecline:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "ciacDecline"
    .end annotation
.end field

.field public cvrMaskAnd:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cvrMaskAnd"
    .end annotation
.end field

.field public gpoResponse:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "gpoResponse"
    .end annotation
.end field

.field public paymentFci:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "paymentFci"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
