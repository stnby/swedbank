.class Lcom/mastercard/mpsdk/card/profile/sdk/CardWalletRelatedDataJson;
.super Ljava/lang/Object;


# instance fields
.field public cardholderValidator:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cardholderValidator"
    .end annotation
.end field

.field public cvmResetTimeout:I
    .annotation runtime Lflexjson/h;
        a = "cvmResetTimeout"
    .end annotation
.end field

.field public dualTapResetTimeout:I
    .annotation runtime Lflexjson/h;
        a = "dualTapResetTimeout"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
