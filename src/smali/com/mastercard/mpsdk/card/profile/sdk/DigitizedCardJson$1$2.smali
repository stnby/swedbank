.class Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

.field final synthetic val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iput-object p2, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->aid:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;

    invoke-direct {v1, p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$1;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;Lcom/mastercard/mpsdk/card/profile/sdk/CardAlternateContactlessPaymentDataJson;)V

    return-object v1
.end method

.method public getCdol1RelatedDataLength()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cdol1RelatedDataLength:I

    return v0
.end method

.method public getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getCiacDeclineOnPpms()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ciacDeclineOnPpms:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ciacDeclineOnPpms:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cvmModel:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cvmModel:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x66c3a89a

    if-eq v2, v3, :cond_3

    const v3, -0x5e1f552b

    if-eq v2, v3, :cond_2

    const v3, 0x7473b8c5

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "FLEXIBLE_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "CDCVM_ALWAYS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const-string v2, "CARD_LIKE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CARD_LIKE:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cvrMaskAnd:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->cvrMaskAnd:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getGpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->gpoResponse:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->iccPrivateKeyCrtComponents:Ljava/lang/String;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->issuerApplicationData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->paymentFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPinIvCvc3Track2()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->pinIvCvc3Track2:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPpseFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->ppseFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getRecords()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->records:[Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    new-instance v5, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;

    invoke-direct {v5, p0, v4}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getTrack1ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;

    invoke-direct {v1, p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$3;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;)V

    return-object v1
.end method

.method public getTrack2ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->track2ConstructionData:Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$4;

    invoke-direct {v1, p0, v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$4;-><init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;Lcom/mastercard/mpsdk/card/profile/sdk/CardTrackConstructionDataJson;)V

    return-object v1
.end method

.method public getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->umdGeneration:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->umdGeneration:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0xc95c30a

    if-eq v2, v3, :cond_3

    const v3, 0x42a6720a

    if-eq v2, v3, :cond_2

    const v3, 0x42ec93be

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "ALWAYS_VALIDATED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v2, "ALWAYS_GENERATE_RANDOM_UMD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    goto :goto_0

    :cond_3
    const-string v2, "VALIDATED_ON_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isTransitSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-boolean v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->isTransitSupported:Z

    return v0
.end method

.method public isUsAipMaskingSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;

    iget-boolean v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardContactlessPaymentDataJson;->isUsAipMaskingSupported:Z

    return v0
.end method
