.class Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DsrpData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAip()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->aip:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->ciacDecline:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->ciacDecline:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->cvmModel:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->cvmModel:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x66c3a89a

    if-eq v2, v3, :cond_3

    const v3, -0x5e1f552b

    if-eq v2, v3, :cond_2

    const v3, 0x7473b8c5

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "FLEXIBLE_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "CDCVM_ALWAYS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const-string v2, "CARD_LIKE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CARD_LIKE:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->cvrMaskAnd:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->cvrMaskAnd:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getExpiryDate()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->expiryDate:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->issuerApplicationData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPanSequenceNumber()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->panSequenceNumber:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPar()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->par:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->par:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getTrack2EquivalentData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->track2EquivalentData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->ucafVersion:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->ucafVersion:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/16 v3, 0xa9a

    if-eq v2, v3, :cond_2

    const v3, 0x1ca0c41f

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "V0_PLUS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "V0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    :cond_3
    :goto_0
    packed-switch v1, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0_PLUS:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->umdGeneration:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$3;->this$1:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1;->val$digitizedCardJson:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->dsrpData:Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardDsrpDataJson;->umdGeneration:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0xc95c30a

    if-eq v2, v3, :cond_3

    const v3, 0x42a6720a

    if-eq v2, v3, :cond_2

    const v3, 0x42ec93be

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "ALWAYS_VALIDATED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v2, "ALWAYS_GENERATE_RANDOM_UMD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    goto :goto_0

    :cond_3
    const-string v2, "VALIDATED_ON_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
