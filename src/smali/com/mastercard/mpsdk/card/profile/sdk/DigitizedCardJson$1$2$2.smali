.class Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/Records;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;->getRecords()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;

.field final synthetic val$cardRecords:Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;->this$2:Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2;

    iput-object p2, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;->val$cardRecords:Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRecordNumber()B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;->val$cardRecords:Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    iget v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;->recordNumber:I

    int-to-byte v0, v0

    return v0
.end method

.method public getRecordValue()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;->val$cardRecords:Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;->recordValue:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getSfi()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson$1$2$2;->val$cardRecords:Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/sdk/CardRecordsJson;->sfi:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method
