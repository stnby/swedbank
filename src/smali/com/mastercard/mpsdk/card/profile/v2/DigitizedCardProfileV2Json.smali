.class public Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;


# instance fields
.field public mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;
    .annotation runtime Lflexjson/h;
        a = "mchipCardProfile"
    .end annotation
.end field

.field public version:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "version"
    .end annotation
.end field

.field public walletRelatedData:Lcom/mastercard/mpsdk/card/profile/v2/WalletRelatedDataV2Json;
    .annotation runtime Lflexjson/h;
        a = "walletRelatedData"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static valueOf([B)Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;
    .locals 2

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v0, p0, v1

    new-instance p0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v0, Lflexjson/j;

    invoke-direct {v0}, Lflexjson/j;-><init>()V

    const-class v1, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    invoke-virtual {v0, p0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    return-object p0
.end method


# virtual methods
.method public buildAsJson()Ljava/lang/String;
    .locals 2

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProfileVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    return-object v0
.end method
