.class public Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;
.super Ljava/lang/Object;


# instance fields
.field public accountType:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "accountType"
    .end annotation
.end field

.field public cardCountryCode:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cardCountryCode"
    .end annotation
.end field

.field public digitizedCardId:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "digitizedCardId"
    .end annotation
.end field

.field public isTransactionIdRequired:Z
    .annotation runtime Lflexjson/h;
        a = "isTransactionIdRequired"
    .end annotation
.end field

.field public pan:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "pan"
    .end annotation
.end field

.field public productType:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "productType"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
