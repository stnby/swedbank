.class public Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;
.super Ljava/lang/Object;


# instance fields
.field public aip:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "aip"
    .end annotation
.end field

.field public cvmModel:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cvmModel"
    .end annotation
.end field

.field public expirationDate:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "expirationDate"
    .end annotation
.end field

.field public issuerApplicationData:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "issuerApplicationData"
    .end annotation
.end field

.field public panSequenceNumber:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "panSequenceNumber"
    .end annotation
.end field

.field public par:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "par"
    .end annotation
.end field

.field public track2Equivalent:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "track2EquivalentData"
    .end annotation
.end field

.field public ucafVersion:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "ucafVersion"
    .end annotation
.end field

.field public umdGeneration:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "umdGeneration"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
