.class public Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;
.super Ljava/lang/Object;


# instance fields
.field public commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;
    .annotation runtime Lflexjson/h;
        a = "commonData"
    .end annotation
.end field

.field public contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;
    .annotation runtime Lflexjson/h;
        a = "contactlessPaymentData"
    .end annotation
.end field

.field public dsrpData:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;
    .annotation runtime Lflexjson/h;
        a = "dsrpData"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
