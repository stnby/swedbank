.class public Lcom/mastercard/mpsdk/card/profile/v1/MppLiteModuleV1Json;
.super Ljava/lang/Object;


# instance fields
.field public cardRiskManagementData:Lcom/mastercard/mpsdk/card/profile/v1/CardRiskManagementDataV1Json;
    .annotation runtime Lflexjson/h;
        a = "cardRiskManagementData"
    .end annotation
.end field

.field public contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;
    .annotation runtime Lflexjson/h;
        a = "contactlessPaymentData"
    .end annotation
.end field

.field public remotePaymentData:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;
    .annotation runtime Lflexjson/h;
        a = "remotePaymentData"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
