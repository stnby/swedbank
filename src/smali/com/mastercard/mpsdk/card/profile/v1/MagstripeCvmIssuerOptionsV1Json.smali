.class public Lcom/mastercard/mpsdk/card/profile/v1/MagstripeCvmIssuerOptionsV1Json;
.super Ljava/lang/Object;


# instance fields
.field public ackAlwaysRequiredIfCurrencyNotProvided:Z
    .annotation runtime Lflexjson/h;
        a = "ackAlwaysRequiredIfCurrencyNotProvided"
    .end annotation
.end field

.field public ackAlwaysRequiredIfCurrencyProvided:Z
    .annotation runtime Lflexjson/h;
        a = "ackAlwaysRequiredIfCurrencyProvided"
    .end annotation
.end field

.field public ackAutomaticallyResetByApplication:Z
    .annotation runtime Lflexjson/h;
        a = "ackAutomaticallyResetByApplication"
    .end annotation
.end field

.field public ackPreEntryAllowed:Z
    .annotation runtime Lflexjson/h;
        a = "ackPreEntryAllowed"
    .end annotation
.end field

.field public pinAlwaysRequiredIfCurrencyNotProvided:Z
    .annotation runtime Lflexjson/h;
        a = "pinAlwaysRequiredIfCurrencyNotProvided"
    .end annotation
.end field

.field public pinAlwaysRequiredIfCurrencyProvided:Z
    .annotation runtime Lflexjson/h;
        a = "pinAlwaysRequiredIfCurrencyProvided"
    .end annotation
.end field

.field public pinAutomaticallyResetByApplication:Z
    .annotation runtime Lflexjson/h;
        a = "pinAutomaticallyResetByApplication"
    .end annotation
.end field

.field public pinPreEntryAllowed:Z
    .annotation runtime Lflexjson/h;
        a = "pinPreEntryAllowed"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
