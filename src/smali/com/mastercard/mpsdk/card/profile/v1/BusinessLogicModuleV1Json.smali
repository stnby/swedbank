.class public Lcom/mastercard/mpsdk/card/profile/v1/BusinessLogicModuleV1Json;
.super Ljava/lang/Object;


# instance fields
.field public applicationLifeCycleData:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "applicationLifeCycleData"
    .end annotation
.end field

.field public cardLayoutDescription:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cardLayoutDescription"
    .end annotation
.end field

.field public cardholderValidators:[Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cardholderValidators"
    .end annotation
.end field

.field public cvmResetTimeout:I
    .annotation runtime Lflexjson/h;
        a = "cvmResetTimeout"
    .end annotation
.end field

.field public dualTapResetTimeout:I
    .annotation runtime Lflexjson/h;
        a = "dualTapResetTimeout"
    .end annotation
.end field

.field public magstripeCvmIssuerOptions:Lcom/mastercard/mpsdk/card/profile/v1/MagstripeCvmIssuerOptionsV1Json;
    .annotation runtime Lflexjson/h;
        a = "magstripeCvmIssuerOptions"
    .end annotation
.end field

.field public mchipCvmIssuerOptions:Lcom/mastercard/mpsdk/card/profile/v1/MChipCvmIssuerOptionsV1Json;
    .annotation runtime Lflexjson/h;
        a = "mChipCvmIssuerOptions"
    .end annotation
.end field

.field public securityWord:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "securityWord"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
