.class public Lcom/mastercard/mpsdk/exceptions/InvalidCardStateException;
.super Lcom/mastercard/mpsdk/exceptions/McbpCheckedException;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/exceptions/McbpCheckedException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/mastercard/mpsdk/exceptions/McbpCheckedException;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V

    return-void
.end method
