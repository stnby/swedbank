.class public Lcom/mastercard/mpsdk/exceptions/McbpCardNotFound;
.super Lcom/mastercard/mpsdk/exceptions/McbpCardException;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INVALID_DIGITIZED_CARD_ID:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    invoke-direct {p0, p1, v0}, Lcom/mastercard/mpsdk/exceptions/McbpCardException;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V

    return-void
.end method
