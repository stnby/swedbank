.class public Lcom/mastercard/mpsdk/exceptions/McbpUncheckedException;
.super Ljava/lang/RuntimeException;


# instance fields
.field private final errorCode:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    iput-object p1, p0, Lcom/mastercard/mpsdk/exceptions/McbpUncheckedException;->errorCode:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    :goto_0
    iput-object p2, p0, Lcom/mastercard/mpsdk/exceptions/McbpUncheckedException;->errorCode:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    return-void
.end method


# virtual methods
.method public final getErrorCode()Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/exceptions/McbpUncheckedException;->errorCode:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    return-object v0
.end method
