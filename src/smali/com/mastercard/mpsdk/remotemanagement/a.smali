.class public Lcom/mastercard/mpsdk/remotemanagement/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "REMOTE MANAGEMENT | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDPushNotification;)Lcom/mastercard/mpsdk/remotemanagement/c/b;
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->hasCommunicationParameters()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v2

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDPushNotification;->getEncryptedData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->decryptPushedRemoteData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)[B

    move-result-object v0

    const/16 v1, 0x10

    array-length v2, v0

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cmsDSessionData = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Lcom/mastercard/mpsdk/remotemanagement/c/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDPushNotification;->getResponseHost()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/c/b;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;Ljava/lang/String;)V

    return-object v1

    :cond_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string v0, "Failed to decrypt push notification data, Mobile keys are missing"

    const-string v1, "REGISTRATION_DATA_NOT_AVAILABLE"

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string v1, "Failed to decrypt push notification data"

    const-string v2, "SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {v0, v1, v2, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    throw p1
.end method
