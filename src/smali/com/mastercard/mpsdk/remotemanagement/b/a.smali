.class public Lcom/mastercard/mpsdk/remotemanagement/b/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/remotemanagement/b/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/remotemanagement/b/a$b;,
        Lcom/mastercard/mpsdk/remotemanagement/b/a$a;
    }
.end annotation


# instance fields
.field a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

.field final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/mastercard/mpsdk/remotemanagement/a/d;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/mastercard/mpsdk/remotemanagement/b/d;

.field d:Ljava/util/concurrent/Semaphore;

.field e:J

.field private final f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

.field private g:Ljava/lang/String;

.field private h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

.field private final i:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private final j:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/remotemanagement/b/d;Lcom/mastercard/mpsdk/remotemanagement/c/c;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "REMOTE MANAGEMENT | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/b/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->i:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-wide/16 v2, 0x1e

    iput-wide v2, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->e:J

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->c:Lcom/mastercard/mpsdk/remotemanagement/b/d;

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    new-instance p1, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-direct {p1, p0, v1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;-><init>(Lcom/mastercard/mpsdk/remotemanagement/b/a;B)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b:Ljava/util/LinkedList;

    new-instance p1, Ljava/util/concurrent/Semaphore;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/remotemanagement/b/a;Lcom/mastercard/mpsdk/remotemanagement/a/d;)Lcom/mastercard/mpsdk/remotemanagement/a/d;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/remotemanagement/b/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private declared-synchronized b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method static synthetic b(Lcom/mastercard/mpsdk/remotemanagement/b/a;)V
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/b/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$b;-><init>(Lcom/mastercard/mpsdk/remotemanagement/b/a;B)V

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/a$b;->start()V

    return-void
.end method

.method static synthetic b(Lcom/mastercard/mpsdk/remotemanagement/b/a;Lcom/mastercard/mpsdk/remotemanagement/a/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    return-void
.end method

.method private c(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Lcom/mastercard/mpsdk/remotemanagement/a/d;
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/remotemanagement/a/d;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->i:I

    invoke-interface {p1, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->b(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate Command not added to queue "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    monitor-exit v0

    return-object v2

    :cond_1
    monitor-exit v0

    const/4 p1, 0x0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic c(Lcom/mastercard/mpsdk/remotemanagement/b/a;)V
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    iget-wide v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->e:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    iget-object p0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void
.end method

.method static synthetic d(Lcom/mastercard/mpsdk/remotemanagement/b/a;)Lcom/mastercard/mpsdk/remotemanagement/b/a$a;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    return-object p0
.end method

.method private declared-synchronized d(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V
    .locals 3

    monitor-enter p0

    if-nez p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->b()V

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->e(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->g()Lcom/mastercard/mpsdk/remotemanagement/b/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Command "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "recommended action= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/a$1;->b:[I

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/f;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    :try_start_3
    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->f()Lcom/mastercard/mpsdk/remotemanagement/b/b;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Command "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "recommended action= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/a$1;->a:[I

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_1
    :try_start_4
    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->g:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "error code= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->b()V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_2
    :try_start_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "retry interval "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->i()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    :try_start_6
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    monitor-exit p0

    return-void

    :cond_3
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic e(Lcom/mastercard/mpsdk/remotemanagement/b/a;)Lcom/mastercard/mpsdk/remotemanagement/a/d;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    return-object p0
.end method

.method private declared-synchronized e(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->c:I

    if-eq v0, v1, :cond_1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->e()I

    move-result p1

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit p0

    return p1

    :cond_1
    :goto_1
    const/4 p1, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/mastercard/mpsdk/remotemanagement/b/a;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->c:Lcom/mastercard/mpsdk/remotemanagement/b/d;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/d;->f()Lcom/mastercard/mpsdk/remotemanagement/a/a/i;

    move-result-object v0

    iget-object p0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Z

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Ljava/lang/String;
    .locals 1

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    const-string p1, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->c(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Lcom/mastercard/mpsdk/remotemanagement/a/d;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->a()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Z

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->a()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method public final a()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSessionArrived availablePermits= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSessionArrived availablePermits= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->a(I)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->c()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/remotemanagement/a/d;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->c()V

    invoke-direct {p0, v2}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->clear()V

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lcom/mastercard/mpsdk/remotemanagement/a/d;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    return-object v0
.end method

.method public final declared-synchronized d()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->h:Lcom/mastercard/mpsdk/remotemanagement/a/d;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->a()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "NULL"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a;->f:Lcom/mastercard/mpsdk/remotemanagement/b/a$a;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->size()I

    move-result v0

    return v0
.end method
