.class final Lcom/mastercard/mpsdk/remotemanagement/b/a$a;
.super Ljava/util/concurrent/LinkedBlockingDeque;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/remotemanagement/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/LinkedBlockingDeque<",
        "Lcom/mastercard/mpsdk/remotemanagement/a/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/remotemanagement/b/a;


# direct methods
.method private constructor <init>(Lcom/mastercard/mpsdk/remotemanagement/b/a;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->a:Lcom/mastercard/mpsdk/remotemanagement/b/a;

    invoke-direct {p0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mastercard/mpsdk/remotemanagement/b/a;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;-><init>(Lcom/mastercard/mpsdk/remotemanagement/b/a;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->a:Lcom/mastercard/mpsdk/remotemanagement/b/a;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->a(Lcom/mastercard/mpsdk/remotemanagement/b/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->a:Lcom/mastercard/mpsdk/remotemanagement/b/a;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b(Lcom/mastercard/mpsdk/remotemanagement/b/a;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Adding to head of Queue "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public final synthetic add(Ljava/lang/Object;)Z
    .locals 0

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/a/d;

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Z

    move-result p1

    return p1
.end method

.method public final synthetic addFirst(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/a/d;

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    return-void
.end method

.method public final b(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->a:Lcom/mastercard/mpsdk/remotemanagement/b/a;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->a(Lcom/mastercard/mpsdk/remotemanagement/b/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b/a$a;->a:Lcom/mastercard/mpsdk/remotemanagement/b/a;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/a;->b(Lcom/mastercard/mpsdk/remotemanagement/b/a;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Adding to Queue "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
