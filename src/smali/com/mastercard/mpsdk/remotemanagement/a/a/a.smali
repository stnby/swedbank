.class abstract Lcom/mastercard/mpsdk/remotemanagement/a/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/remotemanagement/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/mastercard/mpsdk/remotemanagement/a/d;"
    }
.end annotation


# instance fields
.field protected a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field protected final b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

.field protected final c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field protected d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

.field protected e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

.field protected f:I

.field protected g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

.field protected h:Lcom/mastercard/mpsdk/remotemanagement/b/f;

.field protected i:Ljava/lang/String;

.field private j:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

.field private final k:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

.field private l:Ljava/lang/String;

.field private final m:I

.field private n:I

.field private o:I

.field private final p:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "REMOTE MANAGEMENT | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->p:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    invoke-interface {p8}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;->getRetryCount()I

    move-result v0

    iput v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->m:I

    iget v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->m:I

    iput v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->n:I

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->l:Ljava/lang/String;

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->j:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iput-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iput-object p4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iput-object p5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iput-object p7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iput-object p8, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->k:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    iput-object p6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    sget p1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->g:I

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->q:Ljava/util/HashMap;

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->q:Ljava/util/HashMap;

    const-string p2, "Content-Type"

    const-string p3, "application/json"

    invoke-virtual {p1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->q:Ljava/util/HashMap;

    const-string p2, "Accept"

    const-string p3, "application/json"

    invoke-virtual {p1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 12

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->hasCommunicationParameters()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v1

    iget v2, v1, Lcom/mastercard/mpsdk/remotemanagement/c/b;->e:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    iput v2, v1, Lcom/mastercard/mpsdk/remotemanagement/c/b;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "MPA to CMS counter Increased to: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, v1, Lcom/mastercard/mpsdk/remotemanagement/c/b;->e:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "MPA to CMS Counter is currently: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Lcom/mastercard/mpsdk/remotemanagement/c/b;->e:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v11, v1, Lcom/mastercard/mpsdk/remotemanagement/c/b;->e:I

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v6

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    iget-object v9, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object p1

    iget-object v10, p1, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    invoke-interface/range {v5 .. v11}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->buildRemoteServiceRequest(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;I)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;->getEncryptedData()[B

    move-result-object p1

    const/4 v1, 0x2

    invoke-static {p1, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v2

    iget-object v2, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    invoke-interface {v1, v2, v4, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->calculateAuthenticationCode(Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v2

    iget-boolean v4, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->d:Z

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget-object v6, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    invoke-interface {v6}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getValidForSeconds()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    add-long/2addr v4, v6

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v6}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Session used for the first time. expires in "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "milis"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iput-boolean v3, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->d:Z

    :cond_0
    new-instance v2, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;-><init>()V

    invoke-virtual {v2, v1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->setAuthenticationCode([B)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    invoke-virtual {v2, p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->setEncryptedData(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    invoke-virtual {v2, v0}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->setMobileKeysetId(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    return-object v2

    :cond_1
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "Communication parameters are missing SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method abstract a([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->n:I

    return-void
.end method

.method abstract a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method abstract a(Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->f:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->i:Ljava/lang/String;

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->b:I

    invoke-virtual {p0, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->h:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->j:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->d()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->q:Ljava/util/HashMap;

    invoke-interface {v1, v2, v3, p1, v4}, Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;->execute(Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HTTP response code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_7

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    const/16 v5, 0xcc

    if-ne v3, v5, :cond_7

    :cond_1
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    if-ne v3, v4, :cond_5

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getContent()[B

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getContent()[B

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_5

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getContent()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;->valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;->getEncryptedData()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->decodeBase64([B)[B

    move-result-object p1

    const/4 v2, 0x3

    invoke-static {p1, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CMS to MPA Counter is currently: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->f:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v2, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->f:I

    if-ge v2, v1, :cond_3

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v2

    iput v1, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->f:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CMS to MPA Counter set to: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->hasCommunicationParameters()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v3

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    new-instance v4, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;

    invoke-direct {v4, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;-><init>([B)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object p1

    iget-object p1, p1, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    invoke-interface {v2, v3, v1, v4, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->decryptRemoteServiceResponse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;)[B

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "decrypted CmsD response= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cmsDResponse= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;->toJsonString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CMS-D error code= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;->getErrorDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, v2, p1, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_2
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "Communication parameters are missing SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string v0, "CMS to MPA Counter mismatch"

    const-string v1, "COUNTER_MISMATCH"

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1

    :cond_4
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;->getErrorDescription()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;->getErrorDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDResponse;->getErrorCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object p1, v0

    :cond_6
    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;)V

    return-void

    :cond_7
    if-eqz p1, :cond_8

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    const/16 v4, 0x191

    if-ne v3, v4, :cond_8

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->b:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    :goto_0
    iput-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    goto :goto_2

    :cond_8
    iget v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->n:I

    if-lez v3, :cond_c

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    const/16 v4, 0x12e

    const/16 v5, 0x1f7

    if-eq v3, v4, :cond_9

    const/16 v4, 0x1f4

    if-eq v3, v4, :cond_9

    const/16 v4, 0x198

    if-eq v3, v4, :cond_9

    const/16 v4, 0x1f8

    if-eq v3, v4, :cond_9

    if-eq v3, v5, :cond_9

    const/16 v4, 0x453

    if-ne v3, v4, :cond_a

    :cond_9
    const/4 v1, 0x1

    :cond_a
    if-eqz v1, :cond_c

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v1

    if-ne v1, v5, :cond_b

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getRetryAfterValue()I

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getRetryAfterValue()I

    move-result v1

    goto :goto_1

    :cond_b
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->k:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;->getRetryIntervals()[I

    move-result-object v1

    iget v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->m:I

    iget v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->n:I

    sub-int/2addr v3, v4

    aget v1, v1, v3

    :goto_1
    mul-int/lit16 v1, v1, 0x3e8

    iput v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->o:I

    iget v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->n:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->n:I

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->a:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    goto :goto_0

    :cond_c
    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    goto :goto_0

    :goto_2
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    if-eqz p1, :cond_d

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    :cond_d
    const-string p1, "NETWORK_ERROR"

    :goto_3
    const-string v2, "HTTP Error"

    invoke-virtual {p0, v1, p1, v2, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->f:I

    return v0
.end method

.method public f()Lcom/mastercard/mpsdk/remotemanagement/b/b;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    return-object v0
.end method

.method public g()Lcom/mastercard/mpsdk/remotemanagement/b/f;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->h:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->o:I

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v0

    const-string v1, ""

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getRemoteManagementServiceUrl()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method abstract l()Ljava/lang/String;
.end method
