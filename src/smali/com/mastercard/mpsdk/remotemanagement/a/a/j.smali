.class public Lcom/mastercard/mpsdk/remotemanagement/a/a/j;
.super Lcom/mastercard/mpsdk/remotemanagement/a/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mpsdk/remotemanagement/a/a/a<",
        "Lcom/mastercard/mpsdk/remotemanagement/json/response/ChangeMobilePinResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected j:Ljava/lang/String;

.field protected k:Ljava/lang/String;

.field protected final l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

.field protected m:Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 10

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v8}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    const-string v0, "SUCCESS"

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->n:Ljava/lang/String;

    const-string v0, "INCORRECT_PIN"

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->o:Ljava/lang/String;

    const-string v0, "INVALID_INPUT"

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->p:Ljava/lang/String;

    move-object/from16 v0, p8

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->k:Ljava/lang/String;

    move-object/from16 v0, p9

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    iget-object v3, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->k:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->m:Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    iget-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->m:Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;->toString()Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    move-result-object p1

    return-object p1
.end method

.method final synthetic a([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;
    .locals 0

    invoke-static {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ChangeMobilePinResponse;->valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/ChangeMobilePinResponse;

    move-result-object p1

    return-object p1
.end method

.method public final bridge synthetic a()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(I)V

    return-void
.end method

.method protected a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    const-string v0, "CANCELED"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Change Pin request is cancelled or "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_0
    move-object v4, p3

    iget-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_1
    iget-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    invoke-interface {p3, p1, p2, v4, p4}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->i:Ljava/lang/String;

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->e:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final synthetic a(Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;)V
    .locals 3

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/json/response/ChangeMobilePinResponse;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ChangeMobilePinResponse;->getResult()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SUCCESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->c:I

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    sget-object p1, Lcom/mastercard/mpsdk/remotemanagement/b/f;->b:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->h:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->m()V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ChangeMobilePinResponse;->getResult()Ljava/lang/String;

    move-result-object v0

    const-string v1, "INCORRECT_PIN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->e:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ChangeMobilePinResponse;->getMobilePinTriesRemaining()I

    move-result p1

    const-string v0, "INCORRECT_MOBILE_PIN"

    const-string v1, "Incorrect current PIN supplied to set/change PIN request"

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->hasCommunicationParameters()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v1, "MOBILE_KEYS_MISSING"

    const-string v2, "Failed to execute set pin command"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    :try_start_0
    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->b:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->m:Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    invoke-virtual {v1, v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;->setNewMobilePin(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;)Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->m:Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;->buildAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->buildAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v3, "Failed to execute set PIN command"

    goto :goto_0

    :catch_2
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "SDK_COMMUNICATION_ERROR"

    const-string v3, "Failed to execute Set PIN HTTP request"

    goto :goto_0
.end method

.method public final bridge synthetic b(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final c()V
    .locals 4

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->h:I

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "CANCELED"

    const-string v1, "Another request is already in process"

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_0
    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->i:I

    if-ne v0, v1, :cond_1

    const-string v0, "CANCELED"

    const-string v1, "Duplicate Request"

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_1
    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->f:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    const-string v0, "CANCELED"

    const-string v1, "Could not get a valid session from CMS-D"

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    return-void
.end method

.method public final d()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method

.method public final bridge synthetic e()I
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;

    iget-object p1, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final bridge synthetic f()Lcom/mastercard/mpsdk/remotemanagement/b/b;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->f()Lcom/mastercard/mpsdk/remotemanagement/b/b;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g()Lcom/mastercard/mpsdk/remotemanagement/b/f;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->g()Lcom/mastercard/mpsdk/remotemanagement/b/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic h()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic i()I
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->i()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic j()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic k()Z
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->k()Z

    move-result v0

    return v0
.end method

.method final l()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/changeMobilePin"

    return-object v0
.end method

.method protected m()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->d()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->c(Ljava/lang/String;)V

    return-void
.end method
