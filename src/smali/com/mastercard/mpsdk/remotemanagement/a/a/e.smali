.class public final Lcom/mastercard/mpsdk/remotemanagement/a/a/e;
.super Lcom/mastercard/mpsdk/remotemanagement/a/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mpsdk/remotemanagement/a/a/a<",
        "Lcom/mastercard/mpsdk/remotemanagement/json/response/GetTaskStatusResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object/from16 v1, p8

    iput-object v1, v0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    move-result-object p1

    return-object p1
.end method

.method final synthetic a([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;
    .locals 0

    invoke-static {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/GetTaskStatusResponse;->valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/GetTaskStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public final bridge synthetic a()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(I)V

    return-void
.end method

.method final a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->i:Ljava/lang/String;

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->e:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    invoke-interface {p1, p2, p3, p4}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final synthetic a(Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;)V
    .locals 1

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/json/response/GetTaskStatusResponse;

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->d:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/f;->b:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->h:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/GetTaskStatusResponse;->getStatus()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final b()V
    .locals 4

    :try_start_0
    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->b:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->j:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->toString()Ljava/lang/String;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->buildAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->buildAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v3, "Failed to execute set  Get Task Status command"

    goto :goto_0

    :catch_2
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "SDK_COMMUNICATION_ERROR"

    const-string v3, "Failed to execute Get Task Status HTTP request"

    goto :goto_0
.end method

.method public final bridge synthetic b(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final c()V
    .locals 4

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->h:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    const-string v1, "CANCELED"

    const-string v3, "Another request is already in process"

    invoke-interface {v0, v1, v3, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_0
    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->i:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    const-string v1, "CANCELED"

    const-string v3, "Duplicate Request"

    invoke-interface {v0, v1, v3, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_1
    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->f:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    const-string v1, "CANCELED"

    const-string v3, "Get Task Status command cancelled or Could not get a valid session from CMS-D"

    invoke-interface {v0, v1, v3, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    return-void
.end method

.method public final d()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method

.method public final bridge synthetic e()I
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;

    return p1
.end method

.method public final bridge synthetic f()Lcom/mastercard/mpsdk/remotemanagement/b/b;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->f()Lcom/mastercard/mpsdk/remotemanagement/b/b;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g()Lcom/mastercard/mpsdk/remotemanagement/b/f;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->g()Lcom/mastercard/mpsdk/remotemanagement/b/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic h()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic i()I
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->i()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic j()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic k()Z
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->k()Z

    move-result v0

    return v0
.end method

.method final l()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/getTaskStatus"

    return-object v0
.end method
