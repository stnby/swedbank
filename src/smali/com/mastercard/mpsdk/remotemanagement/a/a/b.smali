.class public final Lcom/mastercard/mpsdk/remotemanagement/a/a/b;
.super Lcom/mastercard/mpsdk/remotemanagement/a/a/j;


# instance fields
.field private n:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 0

    invoke-direct/range {p0 .. p10}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-void
.end method


# virtual methods
.method protected final a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    const-string v0, "CANCELED"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Change Pin request is cancelled or "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_0
    move-object v4, p3

    iget-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_1
    iget-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    invoke-interface {p3, p1, p2, v4, p4}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b()V
    .locals 5

    :try_start_0
    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->b:I

    invoke-virtual {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->b(I)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "MOBILE_KEYS_MISSING"

    const-string v3, "Failed to execute change PIN command"

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->m:Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    invoke-virtual {v1, v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;->setCurrentMobilePin(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;)Lcom/mastercard/mpsdk/remotemanagement/json/request/ChangeMobilePinRequest;

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->n:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->d(Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->n:Z

    :cond_2
    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->b()V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v3, "Failed to execute change PIN command"

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;

    iget-object p1, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method protected final m()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->e()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Ljava/lang/String;)V

    return-void
.end method
