.class public final Lcom/mastercard/mpsdk/remotemanagement/a/a/h;
.super Lcom/mastercard/mpsdk/remotemanagement/a/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mpsdk/remotemanagement/a/a/a<",
        "Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:Ljava/lang/String;

.field private final k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 10

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v8}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object/from16 v0, p8

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    move-object/from16 v0, p9

    iput-object v0, v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    return-void
.end method

.method private a([Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Ljava/util/ArrayList;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;"
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    new-instance v13, Ljava/util/ArrayList;

    array-length v0, v11

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v14, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;

    invoke-direct {v14, v10, v12}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;-><init>(Lcom/mastercard/mpsdk/remotemanagement/a/a/h;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    const/4 v0, 0x0

    const/4 v15, 0x0

    :goto_0
    array-length v0, v11

    if-ge v15, v0, :cond_0

    aget-object v0, v11, v15

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getContactlessMdSessionKey()[B

    move-result-object v0

    aget-object v1, v11, v15

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getContactlessUmdSingleUseKey()[B

    move-result-object v1

    aget-object v2, v11, v15

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getContactlessUmdSessionKey()[B

    move-result-object v2

    invoke-interface {v14, v0, v1, v2}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->a([B[B[B)V

    invoke-interface {v14}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->a()[B

    move-result-object v4

    invoke-interface {v14}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->b()[B

    move-result-object v2

    invoke-interface {v14}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->c()[B

    move-result-object v6

    aget-object v0, v11, v15

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getDsrpMdSessionKey()[B

    move-result-object v0

    aget-object v1, v11, v15

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getDsrpUmdSingleUseKey()[B

    move-result-object v1

    aget-object v3, v11, v15

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getDsrpUmdSessionKey()[B

    move-result-object v3

    invoke-interface {v14, v0, v1, v3}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->a([B[B[B)V

    invoke-interface {v14}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->a()[B

    move-result-object v5

    invoke-interface {v14}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->b()[B

    move-result-object v3

    invoke-interface {v14}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;->c()[B

    move-result-object v7

    :try_start_0
    iget-object v0, v10, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    aget-object v8, v11, v15

    invoke-virtual {v8}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getIdn()[B

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    invoke-interface {v0, v12, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->getLocalDekEncryptedIdn(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v8
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    aget-object v0, v11, v15

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->getAtc()I

    move-result v9

    new-instance v1, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$1;

    move-object v0, v1

    move-object v10, v1

    move-object/from16 v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$1;-><init>(Lcom/mastercard/mpsdk/remotemanagement/a/a/h;[B[B[B[B[B[B[BI)V

    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v10, p0

    goto :goto_0

    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid IDN"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v13
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    move-result-object p1

    return-object p1
.end method

.method final synthetic a([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;
    .locals 0

    invoke-static {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;

    move-result-object p1

    return-object p1
.end method

.method public final bridge synthetic a()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(I)V

    return-void
.end method

.method final a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 7

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->i:Ljava/lang/String;

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->e:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    if-ne p1, v0, :cond_0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-interface/range {v1 .. v6}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final synthetic a(Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;)V
    .locals 4

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->hasCommunicationParameters()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "MOBILE_KEYS_MISSING"

    const-string v3, "Failed to decrypt replenish response"

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->getTransactionCredentials()[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->getTransactionCredentials()[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->getTransactionCredentials()[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a([Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Ljava/util/ArrayList;

    move-result-object p1

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->d:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/f;->b:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->h:Lcom/mastercard/mpsdk/remotemanagement/b/f;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void

    :cond_1
    sget-object p1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v0, "MISSING_TRANSACTION_CREDENTIALS"

    const-string v2, "No transaction credentials received from CMS-D"

    invoke-virtual {p0, p1, v0, v2, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to exchange transaction credential encryption"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b()V
    .locals 4

    :try_start_0
    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->b:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/request/ReplenishRequest;

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    invoke-direct {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ReplenishRequest;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)V

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ReplenishRequest;->toString()Ljava/lang/String;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/request/ReplenishRequest;->buildAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->buildAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a(Lcom/mastercard/mpsdk/remotemanagement/b/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "SDK_COMMUNICATION_ERROR"

    const-string v3, "Failed to execute replenish HTTP request"

    goto :goto_0

    :catch_2
    move-exception v0

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    const-string v2, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v3, "Failed to execute replenish command"

    goto :goto_0
.end method

.method public final bridge synthetic b(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final c()V
    .locals 8

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->h:I

    if-ne v0, v1, :cond_0

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    const-string v5, "CANCELED"

    const-string v6, "Another request is already in process"

    const/4 v7, 0x0

    move-object v3, p0

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_0
    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mpsdk/remotemanagement/b/e;->i:I

    if-ne v0, v1, :cond_1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    const-string v5, "CANCELED"

    const-string v6, "Duplicate Request"

    const/4 v7, 0x0

    move-object v3, p0

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_1
    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->f:I

    invoke-super {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->b(I)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->e:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    const-string v4, "CANCELED"

    const-string v5, "Replenish command cancelled or Could not get a valid session from CMS-D"

    const/4 v6, 0x0

    move-object v2, p0

    invoke-interface/range {v1 .. v6}, Lcom/mastercard/mpsdk/remotemanagement/a/b;->b(Lcom/mastercard/mpsdk/remotemanagement/a/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    sget-object v0, Lcom/mastercard/mpsdk/remotemanagement/b/b;->d:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->g:Lcom/mastercard/mpsdk/remotemanagement/b/b;

    return-void
.end method

.method public final d()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method

.method public final bridge synthetic e()I
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->e()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    iget-object p1, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final bridge synthetic f()Lcom/mastercard/mpsdk/remotemanagement/b/b;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->f()Lcom/mastercard/mpsdk/remotemanagement/b/b;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g()Lcom/mastercard/mpsdk/remotemanagement/b/f;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->g()Lcom/mastercard/mpsdk/remotemanagement/b/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic h()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic i()I
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->i()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic j()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic k()Z
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/a;->k()Z

    move-result v0

    return v0
.end method

.method final l()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/replenish"

    return-object v0
.end method
