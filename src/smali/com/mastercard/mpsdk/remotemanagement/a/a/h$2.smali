.class final Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/remotemanagement/a/a/h$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/remotemanagement/a/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field final synthetic b:Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

.field private c:[B

.field private d:[B

.field private e:[B


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/remotemanagement/a/a/h;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->b:Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([B[B[B)V
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-array p1, v0, [B

    :goto_0
    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->c:[B

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->b:Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    iget-object v1, v1, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    invoke-direct {v2, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {v1, v2, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    goto :goto_0

    :goto_1
    if-nez p2, :cond_1

    new-array p1, v0, [B

    :goto_2
    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->d:[B

    goto :goto_3

    :cond_1
    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->b:Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    iget-object p1, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    invoke-direct {v1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    iget-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1, v1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    goto :goto_2

    :goto_3
    if-nez p3, :cond_2

    new-array p1, v0, [B

    :goto_4
    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->e:[B

    return-void

    :cond_2
    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->b:Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    iget-object p1, p1, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance p2, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    invoke-direct {p2, p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    iget-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    goto :goto_4
.end method

.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->c:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->c:[B

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->d:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->d:[B

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->e:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a/h$2;->e:[B

    return-object v0
.end method
