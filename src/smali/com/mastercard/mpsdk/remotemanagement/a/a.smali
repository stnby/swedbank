.class public final Lcom/mastercard/mpsdk/remotemanagement/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/remotemanagement/a/c;


# instance fields
.field private a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

.field private b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field private final c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

.field private final d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field private e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

.field private final f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

.field private final g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

.field private final h:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->h:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iput-object p4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iput-object p5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iput-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iput-object p6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iput-object p7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 4

    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMddHHmmssSSS"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mpsdk/remotemanagement/c/c;Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/a/a/e;
    .locals 11

    new-instance v10, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v9, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v10

    move-object v4, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v9}, Lcom/mastercard/mpsdk/remotemanagement/a/a/e;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v10
.end method

.method public final a(Lcom/mastercard/mpsdk/remotemanagement/c/c;Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/a/a/f;
    .locals 12

    new-instance v11, Lcom/mastercard/mpsdk/remotemanagement/a/a/f;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v4, p1

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v0 .. v10}, Lcom/mastercard/mpsdk/remotemanagement/a/a/f;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v11
.end method

.method public final a(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Lcom/mastercard/mpsdk/remotemanagement/a/a/h;
    .locals 12

    new-instance v11, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v10}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v11
.end method

.method public final a()Lcom/mastercard/mpsdk/remotemanagement/a/a/i;
    .locals 10

    new-instance v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/i;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v8, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/mastercard/mpsdk/remotemanagement/a/a/i;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v9
.end method

.method public final a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/remotemanagement/a/a/j;
    .locals 12

    new-instance v11, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v10}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v11
.end method

.method public final b(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/remotemanagement/a/a/b;
    .locals 12

    new-instance v11, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v10}, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v11
.end method

.method public final b(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Lcom/mastercard/mpsdk/remotemanagement/a/a/c;
    .locals 12

    new-instance v11, Lcom/mastercard/mpsdk/remotemanagement/a/a/c;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v10}, Lcom/mastercard/mpsdk/remotemanagement/a/a/c;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v11
.end method

.method public final b()Lcom/mastercard/mpsdk/remotemanagement/a/a/g;
    .locals 10

    new-instance v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/g;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v8, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/mastercard/mpsdk/remotemanagement/a/a/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v9
.end method

.method public final c()Lcom/mastercard/mpsdk/remotemanagement/a/a/d;
    .locals 10

    new-instance v9, Lcom/mastercard/mpsdk/remotemanagement/a/a/d;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->c:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    iget-object v5, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->g:Lcom/mastercard/mpsdk/remotemanagement/a/b;

    iget-object v7, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v8, p0, Lcom/mastercard/mpsdk/remotemanagement/a/a;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/mastercard/mpsdk/remotemanagement/a/a/d;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v9
.end method
