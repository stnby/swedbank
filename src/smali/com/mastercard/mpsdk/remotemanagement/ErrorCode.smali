.class public Lcom/mastercard/mpsdk/remotemanagement/ErrorCode;
.super Ljava/lang/Object;


# static fields
.field public static final COMMAND_CANCELED:Ljava/lang/String; = "CANCELED"

.field public static final COUNTER_MISMATCH:Ljava/lang/String; = "COUNTER_MISMATCH"

.field public static final INCORRECT_MOBILE_PIN:Ljava/lang/String; = "INCORRECT_MOBILE_PIN"

.field public static final MISSING_TRANSACTION_CREDENTIALS:Ljava/lang/String; = "MISSING_TRANSACTION_CREDENTIALS"

.field public static final MOBILE_KEYS_MISSING:Ljava/lang/String; = "MOBILE_KEYS_MISSING"

.field public static final NETWORK_ERROR:Ljava/lang/String; = "NETWORK_ERROR"

.field public static final REGISTRATION_DATA_NOT_AVAILABLE:Ljava/lang/String; = "REGISTRATION_DATA_NOT_AVAILABLE"

.field public static final SDK_CRYPTOGRAPHY_ERROR:Ljava/lang/String; = "SDK_CRYPTO_OPERATION_FAILED"

.field public static final SDK_HTTP_ERROR:Ljava/lang/String; = "SDK_COMMUNICATION_ERROR"

.field public static final SDK_INVALID_INPUTS:Ljava/lang/String; = "SDK_INVALID_INPUTS"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
