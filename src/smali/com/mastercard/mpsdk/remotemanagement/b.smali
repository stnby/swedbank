.class public Lcom/mastercard/mpsdk/remotemanagement/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;
.implements Lcom/mastercard/mpsdk/remotemanagement/a/b;
.implements Lcom/mastercard/mpsdk/remotemanagement/b/d;


# instance fields
.field a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

.field b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

.field c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

.field d:Lcom/mastercard/mpsdk/remotemanagement/a;

.field private final e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

.field private g:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

.field private h:Ljava/lang/String;

.field private i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field private j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field private k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field private l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "REMOTE MANAGEMENT | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/b;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method private a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->e()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->c()Lcom/mastercard/mpsdk/remotemanagement/a/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ALREADY_IN_PROCESS "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_0

    const-string v0, "NULL"

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->j()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v0, Lcom/mastercard/mpsdk/remotemanagement/b/e;->h:I

    invoke-interface {p1, v0}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->b(I)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->c()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthCompleted()V

    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->g:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->h:Ljava/lang/String;

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    const-string v1, "SUCCESS"

    invoke-interface {p1, v0, p2, v1}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->a(Lcom/mastercard/mpsdk/remotemanagement/c/c;Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/a/a/f;

    move-result-object p1

    iget-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Ljava/lang/String;

    return-void
.end method

.method public final a(Lcom/mastercard/mpsdk/remotemanagement/a/d;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p2, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/mastercard/mpsdk/remotemanagement/a/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->a()Ljava/lang/String;

    move-result-object v2

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceived(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->g:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->g:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->h:Ljava/lang/String;

    return-void
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b(Lcom/mastercard/mpsdk/remotemanagement/a/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/d;->a()Ljava/lang/String;

    move-result-object v2

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinSucceeded(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionSuccess()V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinSucceeded(Ljava/lang/String;)V

    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public cancelPendingRequests()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->b()V

    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinSucceeded()V

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletMobilePinStarted(Ljava/lang/String;)V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinSucceeded()V

    return-void
.end method

.method public final f()Lcom/mastercard/mpsdk/remotemanagement/a/a/i;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->a()Lcom/mastercard/mpsdk/remotemanagement/a/a/i;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "creating a requestSessionCommand request ID= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getCurrentRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegistrationRequestData([BLcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RegistrationRequestParameters;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->generateEncryptedRgk()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->createSignedRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;

    move-result-object p1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object p2

    invoke-interface {v0, v1, v2, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/b$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/mastercard/mpsdk/remotemanagement/b$1;-><init>(Lcom/mastercard/mpsdk/remotemanagement/b;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    new-instance p2, Ljava/security/GeneralSecurityException;

    const-string v0, "Failed to get registration request parameters SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p2, v0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    throw p1
.end method

.method public getSetPinRequestData(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->hasCommunicationParameters()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object p1

    invoke-interface {v1, v0, v2, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    move-result-object p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "Failed to get registration request parameters SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {v0, v1, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    throw p1

    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "Communication parameters are missing SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getWalletIdentificationDataProvider()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    return-object v0
.end method

.method public initialize(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 8

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/c/a;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/remotemanagement/c/a;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/b/a;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-direct {v0, p0, v1}, Lcom/mastercard/mpsdk/remotemanagement/b/a;-><init>(Lcom/mastercard/mpsdk/remotemanagement/b/d;Lcom/mastercard/mpsdk/remotemanagement/c/c;)V

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    iput-object p4, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iput-object p5, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iput-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    new-instance p3, Lcom/mastercard/mpsdk/remotemanagement/a;

    invoke-direct {p3, p1, p4}, Lcom/mastercard/mpsdk/remotemanagement/a;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;)V

    iput-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->d:Lcom/mastercard/mpsdk/remotemanagement/a;

    new-instance p3, Lcom/mastercard/mpsdk/remotemanagement/a/a;

    iget-object v4, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    move-object v0, p3

    move-object v1, p2

    move-object v2, p1

    move-object v3, p0

    move-object v5, p5

    move-object v6, p4

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/mastercard/mpsdk/remotemanagement/a/a;-><init>(Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/remotemanagement/a/b;Lcom/mastercard/mpsdk/remotemanagement/c/c;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    iput-object p3, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    return-void
.end method

.method public processNotificationData(Ljava/lang/String;)V
    .locals 5

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_7

    :try_start_0
    invoke-static {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDPushNotification;->valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDPushNotification;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cmsDPushNotification= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDPushNotification;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->d:Lcom/mastercard/mpsdk/remotemanagement/a;

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/a;->a(Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDPushNotification;)Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a(Lcom/mastercard/mpsdk/remotemanagement/c/b;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->a()V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v0

    iget-object v0, v0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    if-nez v0, :cond_0

    goto/16 :goto_3

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/c/c;->a()Lcom/mastercard/mpsdk/remotemanagement/c/b;

    move-result-object v0

    iget-object v0, v0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getPendingAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getPendingAction()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x7a84574b

    if-eq v3, v4, :cond_2

    const v4, 0x62d13568

    if-eq v3, v4, :cond_1

    goto :goto_0

    :cond_1
    const-string v3, "RESET_MOBILE_PIN"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "PROVISION"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    :cond_3
    :goto_0
    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Default pending action= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getPendingAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :pswitch_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getTokenUniqueReference()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getTokenUniqueReference()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onCardPinReset(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onWalletPinReset()V

    :goto_1
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->b()V

    goto :goto_2

    :pswitch_1
    iget-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->b()Lcom/mastercard/mpsdk/remotemanagement/a/a/g;

    move-result-object p1

    :cond_5
    :goto_2
    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->c:Lcom/mastercard/mpsdk/remotemanagement/b/c;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b/c;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)Ljava/lang/String;

    :cond_6
    :goto_3
    return-void

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception p1

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unable to parse push notification data: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "SDK_INVALID_INPUTS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse push notification data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "SDK_INVALID_INPUTS"

    invoke-direct {v0, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unable to parse push notification data: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse push notification data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_4
    throw p1

    :cond_7
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string v0, "Push notification data is null or zero length"

    const-string v1, "SDK_INVALID_INPUTS"

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public requestChangePin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->b(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/remotemanagement/a/a/b;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->a()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public requestChangeWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->b(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/remotemanagement/a/a/b;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/b;->a()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public requestDeleteCard(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->b(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Lcom/mastercard/mpsdk/remotemanagement/a/a/c;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/c;->a()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public requestReplenish(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->a(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Lcom/mastercard/mpsdk/remotemanagement/a/a/h;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/h;->a()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public requestSetPin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/remotemanagement/a/a/j;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public requestSetWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/remotemanagement/a/a/j;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/remotemanagement/a/a/j;->a()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public requestSystemHealth()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->c()Lcom/mastercard/mpsdk/remotemanagement/a/a/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/remotemanagement/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public requestTaskStatusUpdate(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->b:Lcom/mastercard/mpsdk/remotemanagement/a/c;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->a:Lcom/mastercard/mpsdk/remotemanagement/c/c;

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/remotemanagement/a/c;->a(Lcom/mastercard/mpsdk/remotemanagement/c/c;Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/a/a/e;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/remotemanagement/b;->a(Lcom/mastercard/mpsdk/remotemanagement/a/d;)V

    return-void
.end method

.method public setRegistrationResponseData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;Ljava/lang/String;)V
    .locals 6

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v5

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v3

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v4

    new-instance p1, Lcom/mastercard/mpsdk/remotemanagement/b$2;

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/mastercard/mpsdk/remotemanagement/b$2;-><init>(Lcom/mastercard/mpsdk/remotemanagement/b;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/b;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->setCommunicationParameters(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    new-instance p2, Ljava/security/GeneralSecurityException;

    const-string v0, "Failed to get registration request parameters SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p2, v0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    throw p1

    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "Unable to retrieve RGK SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
