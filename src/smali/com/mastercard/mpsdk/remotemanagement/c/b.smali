.class public Lcom/mastercard/mpsdk/remotemanagement/c/b;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:I

.field public f:I

.field private final g:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->d:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "REMOTE MANAGEMENT | "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/mastercard/mpsdk/remotemanagement/c/b;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v1

    iput-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->g:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->d:Z

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->c:Ljava/lang/String;

    iput v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->e:I

    iput v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->f:I

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getExpiryTimestamp()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/TimeUtils;->isBefore(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/c/b;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/TimeUtils;->isBefore(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_2
    return v1
.end method
