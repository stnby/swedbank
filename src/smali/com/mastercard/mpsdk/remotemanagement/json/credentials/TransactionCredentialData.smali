.class public Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;
.super Ljava/lang/Object;


# instance fields
.field private atc:I
    .annotation runtime Lflexjson/h;
        a = "atc"
    .end annotation
.end field

.field private contactlessMdSessionKey:[B
    .annotation runtime Lflexjson/h;
        a = "contactlessMdSessionKey"
    .end annotation
.end field

.field private contactlessUmdSessionKey:[B
    .annotation runtime Lflexjson/h;
        a = "contactlessUmdSessionKey"
    .end annotation
.end field

.field private contactlessUmdSingleUseKey:[B
    .annotation runtime Lflexjson/h;
        a = "contactlessUmdSingleUseKey"
    .end annotation
.end field

.field private dsrpMdSessionKey:[B
    .annotation runtime Lflexjson/h;
        a = "dsrpMdSessionKey"
    .end annotation
.end field

.field private dsrpUmdSessionKey:[B
    .annotation runtime Lflexjson/h;
        a = "dsrpUmdSessionKey"
    .end annotation
.end field

.field private dsrpUmdSingleUseKey:[B
    .annotation runtime Lflexjson/h;
        a = "dsrpUmdSingleUseKey"
    .end annotation
.end field

.field private idn:[B
    .annotation runtime Lflexjson/h;
        a = "idn"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;
    .locals 3

    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "idn"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "contactlessMdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "contactlessUmdSingleUseKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "dsrpMdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "dsrpUmdSingleUseKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "dsrpUmdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "contactlessUmdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    return-object p0
.end method


# virtual methods
.method public getAtc()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->atc:I

    return v0
.end method

.method public getContactlessMdSessionKey()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessMdSessionKey:[B

    return-object v0
.end method

.method public getContactlessUmdSessionKey()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessUmdSessionKey:[B

    return-object v0
.end method

.method public getContactlessUmdSingleUseKey()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessUmdSingleUseKey:[B

    return-object v0
.end method

.method public getDsrpMdSessionKey()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpMdSessionKey:[B

    return-object v0
.end method

.method public getDsrpUmdSessionKey()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpUmdSessionKey:[B

    return-object v0
.end method

.method public getDsrpUmdSingleUseKey()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpUmdSingleUseKey:[B

    return-object v0
.end method

.method public getIdn()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->idn:[B

    return-object v0
.end method

.method public setAtc(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->atc:I

    return-void
.end method

.method public setContactlessMdSessionKey([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessMdSessionKey:[B

    return-void
.end method

.method public setContactlessUmdSessionKey([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessUmdSessionKey:[B

    return-void
.end method

.method public setContactlessUmdSingleUseKey([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessUmdSingleUseKey:[B

    return-void
.end method

.method public setDsrpMdSessionKey([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpMdSessionKey:[B

    return-void
.end method

.method public setDsrpUmdSessionKey([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpUmdSessionKey:[B

    return-void
.end method

.method public setDsrpUmdSingleUseKey([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpUmdSingleUseKey:[B

    return-void
.end method

.method public setIdn([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->idn:[B

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "REMOTE MANAGEMENT"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TransactionCredentialData{atc="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->atc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", idn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->idn:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", contactlessMdSessionKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessMdSessionKey:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", contactlessUmdSingleUseKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->contactlessUmdSingleUseKey:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", dsrpMdSessionKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpMdSessionKey:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", dsrpUmdSingleUseKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;->dsrpUmdSingleUseKey:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-class v0, Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
