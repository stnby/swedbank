.class public Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
.super Ljava/lang/Object;


# instance fields
.field private authenticationCode:[B
    .annotation runtime Lflexjson/h;
        a = "authenticationCode"
    .end annotation
.end field

.field private encryptedData:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "encryptedData"
    .end annotation
.end field

.field private final mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private mobileKeysetId:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "mobileKeysetId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "REMOTE MANAGEMENT"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 3

    new-instance v0, Lflexjson/j;

    invoke-direct {v0}, Lflexjson/j;-><init>()V

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "authenticationCode"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object v0

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    invoke-virtual {v0, p0, v1}, Lflexjson/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    return-object p0
.end method


# virtual methods
.method public buildAsJson()Ljava/lang/String;
    .locals 6

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "authenticationCode"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, [B

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthenticationCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->authenticationCode:[B

    return-object v0
.end method

.method public getEncryptedData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->encryptedData:Ljava/lang/String;

    return-object v0
.end method

.method public getMobileKeysetId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->mobileKeysetId:Ljava/lang/String;

    return-object v0
.end method

.method public setAuthenticationCode([B)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->authenticationCode:[B

    return-object p0
.end method

.method public setEncryptedData(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->encryptedData:Ljava/lang/String;

    return-object p0
.end method

.method public setMobileKeysetId(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->mobileKeysetId:Ljava/lang/String;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CmsDRequest{mobileKeysetId=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->mobileKeysetId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", authenticationCode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->authenticationCode:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", encryptedData=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;->encryptedData:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-class v0, Lcom/mastercard/mpsdk/remotemanagement/json/encrypted/CmsDRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
