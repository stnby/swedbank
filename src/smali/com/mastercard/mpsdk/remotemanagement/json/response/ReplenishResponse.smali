.class public Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;
.super Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;


# instance fields
.field private final mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private transactionCredentials:[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;
    .annotation runtime Lflexjson/h;
        a = "transactionCredentials"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;-><init>()V

    const-string v0, "REMOTE MANAGEMENT"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public static valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;
    .locals 3

    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "transactionCredentials.values.idn"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "transactionCredentials.values.contactlessMdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "transactionCredentials.values.contactlessUmdSingleUseKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "transactionCredentials.values.dsrpMdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "transactionCredentials.values.dsrpUmdSingleUseKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "transactionCredentials.values.dsrpUmdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "transactionCredentials.values.contactlessUmdSessionKey"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;

    return-object p0
.end method


# virtual methods
.method public buildAsJson()Ljava/lang/String;
    .locals 6

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "idn.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "contactlessMdSessionKey.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "contactlessUmdSingleUseKey.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "dsrpMdSessionKey.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "dsrpUmdSingleUseKey.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "dsrpUmdSessionKey.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "contactlessUmdSessionKey.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    const-string v1, "atc.transactionCredentials"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->b([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, [B

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTransactionCredentials()[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->transactionCredentials:[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    return-object v0
.end method

.method public setTransactionCredentials([Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->transactionCredentials:[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReplenishResponse{transactionCredentials="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;->transactionCredentials:[Lcom/mastercard/mpsdk/remotemanagement/json/credentials/TransactionCredentialData;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-class v0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ReplenishResponse;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
