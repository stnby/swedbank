.class public Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;
.super Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;


# instance fields
.field public cardProfile:Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;
    .annotation runtime Lflexjson/h;
        a = "cardProfile"
    .end annotation
.end field

.field public iccKek:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "iccKek"
    .end annotation
.end field

.field private final mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/response/CmsDBaseResponse;-><init>()V

    const-string v0, "REMOTE MANAGEMENT"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public static valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;
    .locals 3

    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    :try_start_0
    const-string p0, "version"

    invoke-virtual {v1, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "cardProfile"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "version"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    const-string v1, "cardProfile"

    const-class v2, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->b(Ljava/lang/String;Ljava/lang/Class;)Lflexjson/j;

    move-result-object p0

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;

    return-object p0

    :cond_0
    new-instance p0, Lorg/json/JSONException;

    const-string v0, "Profile version not supported"

    invoke-direct {p0, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    const-string v1, "cardProfile"

    const-class v2, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->b(Ljava/lang/String;Ljava/lang/Class;)Lflexjson/j;

    move-result-object p0

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error in parsing Provision Response "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public getCardProfile()Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;->cardProfile:Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;

    return-object v0
.end method

.method public getIccKek()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;->iccKek:Ljava/lang/String;

    return-object v0
.end method

.method public setCardProfile(Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;->cardProfile:Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;

    return-void
.end method

.method public setIccKek(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;->iccKek:Ljava/lang/String;

    return-void
.end method

.method public toJsonString()Ljava/lang/String;
    .locals 5

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ProvisionResponse{cardProfile="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;->cardProfile:Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", iccKek=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;->iccKek:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-class v0, Lcom/mastercard/mpsdk/remotemanagement/json/response/ProvisionResponse;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
