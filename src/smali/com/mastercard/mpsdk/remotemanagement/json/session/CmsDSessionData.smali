.class public Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;


# instance fields
.field private expiryTimestamp:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "expiryTimestamp"
    .end annotation
.end field

.field private final mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private pendingAction:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "pendingAction"
    .end annotation
.end field

.field private sessionCode:[B
    .annotation runtime Lflexjson/h;
        a = "sessionCode"
    .end annotation
.end field

.field private tokenUniqueReference:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "tokenUniqueReference"
    .end annotation
.end field

.field private validForSeconds:I
    .annotation runtime Lflexjson/h;
        a = "validForSeconds"
    .end annotation
.end field

.field private version:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "version"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "REMOTE MANAGEMENT"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public static valueOf([B)Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;
    .locals 3

    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayObjectFactory;-><init>()V

    const-string v2, "sessionCode"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;

    move-result-object p0

    const-class v1, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;

    return-object p0
.end method


# virtual methods
.method public final getExpiryTimestamp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->expiryTimestamp:Ljava/lang/String;

    return-object v0
.end method

.method public final getPendingAction()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->pendingAction:Ljava/lang/String;

    return-object v0
.end method

.method public final getSessionCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->sessionCode:[B

    return-object v0
.end method

.method public final getTokenUniqueReference()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->tokenUniqueReference:Ljava/lang/String;

    return-object v0
.end method

.method public final getValidForSeconds()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->validForSeconds:I

    return v0
.end method

.method public final getVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->version:Ljava/lang/String;

    return-object v0
.end method

.method public final setExpiryTimestamp(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->expiryTimestamp:Ljava/lang/String;

    return-void
.end method

.method public final setPendingAction(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->pendingAction:Ljava/lang/String;

    return-void
.end method

.method public final setSessionCode([B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->sessionCode:[B

    return-void
.end method

.method public final setTokenUniqueReference(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->tokenUniqueReference:Ljava/lang/String;

    return-void
.end method

.method public final setValidForSeconds(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->validForSeconds:I

    return-void
.end method

.method public final setVersion(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->version:Ljava/lang/String;

    return-void
.end method

.method public final toJsonString()Ljava/lang/String;
    .locals 6

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/NativeByteArrayTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, [B

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CmsDSessionData{version=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", sessionCode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->sessionCode:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", expiryTimestamp=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->expiryTimestamp:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", validForSeconds="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->validForSeconds:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", pendingAction=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->pendingAction:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", tokenUniqueReference=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;->tokenUniqueReference:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-class v0, Lcom/mastercard/mpsdk/remotemanagement/json/session/CmsDSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
