.class public Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 0

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->buildContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 0

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->buildDsrpData(Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 0

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->buildTrackConstructionData(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;

    move-result-object p0

    return-object p0
.end method

.method private static buildContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v1, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$2;

    invoke-direct {v1, v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$2;-><init>(Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)V

    return-object v1
.end method

.method public static buildDigitizedCard(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;-><init>(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)V

    return-object v0
.end method

.method private static buildDsrpData(Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$4;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$4;-><init>(Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;)V

    return-object v0
.end method

.method private static buildTrackConstructionData(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;-><init>(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)V

    return-object v0
.end method
