.class final Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->buildDigitizedCard(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCardCountryCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->cardCountryCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->access$000(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v0

    return-object v0
.end method

.method public final getDigitizedCardId()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->digitizedCardId:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->dsrpData:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->access$100(Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object v0

    return-object v0
.end method

.method public final getPan()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->pan:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->getProfileVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    return-object v0
.end method

.method public final getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1$1;-><init>(Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;)V

    return-object v0
.end method

.method public final isTransactionIdRequired()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-boolean v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->isTransactionIdRequired:Z

    return v0
.end method
