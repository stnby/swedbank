.class final Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->buildTrackConstructionData(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$trackConstructionDataJson:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;->val$trackConstructionDataJson:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNAtc()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;->val$trackConstructionDataJson:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->nAtc:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPCvc3()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;->val$trackConstructionDataJson:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->pCvc3:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPUnAtc()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;->val$trackConstructionDataJson:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->pUnAtc:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTrackData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2$3;->val$trackConstructionDataJson:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->trackData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
