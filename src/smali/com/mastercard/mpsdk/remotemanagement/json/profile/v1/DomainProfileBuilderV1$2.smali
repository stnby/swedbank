.class final Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1;->buildContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->aid:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->buildAlternateContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    return-object v0
.end method

.method public final getCdol1RelatedDataLength()I
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->cdol1RelatedDataLength:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->ciacDecline:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDeclineOnPpms()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->ciacDeclineOnPpms:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->cvrMaskAnd:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getGpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->gpoResponse:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->buildIccComponents(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v0

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->issuerApplicationData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->paymentFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPinIvCvc3Track2()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->pinIvCvc3Track2:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPpseFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->ppseFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;->val$contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;->records:[Lcom/mastercard/mpsdk/card/profile/RecordsJson;

    invoke-static {v0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->buildRecords([Lcom/mastercard/mpsdk/card/profile/RecordsJson;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getTrack1ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTrack2ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final isTransitSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isUsAipMaskingSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
