.class final Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->buildIccComponents(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;->val$iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDp()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;->val$iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->dp:Ljava/lang/String;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getDq()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;->val$iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->dq:Ljava/lang/String;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getP()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;->val$iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->p:Ljava/lang/String;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getQ()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;->val$iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->q:Ljava/lang/String;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getU()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;->val$iccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->u:Ljava/lang/String;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method
