.class final Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->buildAlternateContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->aid:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->cvrMaskAnd:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->paymentFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getgpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;->val$alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->gpoResponse:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
