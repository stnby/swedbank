.class Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/WalletData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1$1;->this$0:Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0
.end method

.method public getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->MOBILE_PIN:Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    return-object v0
.end method

.method public getCvmResetTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1$1;->this$0:Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->businessLogicModule:Lcom/mastercard/mpsdk/card/profile/v1/BusinessLogicModuleV1Json;

    iget v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/BusinessLogicModuleV1Json;->cvmResetTimeout:I

    return v0
.end method

.method public getDualTapResetTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1$1;->this$0:Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;->val$cardProfile:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->businessLogicModule:Lcom/mastercard/mpsdk/card/profile/v1/BusinessLogicModuleV1Json;

    iget v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/BusinessLogicModuleV1Json;->dualTapResetTimeout:I

    return v0
.end method

.method public getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0
.end method
