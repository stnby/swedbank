.class public Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;
.super Ljava/lang/Object;


# static fields
.field private static mCryptoEngine:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field private static mDekEncryptedIccKek:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

.field private static mEncryptedDekKey:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static build(Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    sput-object p1, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->mDekEncryptedIccKek:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    sput-object p2, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->mEncryptedDekKey:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    sput-object p3, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->mCryptoEngine:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {p0}, Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;->getProfileVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object p1

    sget-object p2, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne p1, p2, :cond_1

    check-cast p0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1;->buildDigitizedCard(Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-interface {p0}, Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;->getProfileVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object p1

    sget-object p2, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne p1, p2, :cond_2

    check-cast p0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v2/DomainProfileBuilderV2;->buildDigitizedCard(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object p0

    return-object p0

    :cond_2
    return-object v0
.end method

.method public static buildAlternateContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$3;-><init>(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)V

    return-object v0
.end method

.method public static buildIccComponents(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$2;-><init>(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)V

    :try_start_0
    sget-object p0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->mCryptoEngine:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    sget-object v1, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->mDekEncryptedIccKek:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    sget-object v2, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder;->mEncryptedDekKey:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p0, v1, v2, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeIccKekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p0
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    new-instance p0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    const-string v0, ""

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object p0
.end method

.method public static buildRecords([Lcom/mastercard/mpsdk/card/profile/RecordsJson;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/mastercard/mpsdk/card/profile/RecordsJson;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    new-instance v4, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$1;

    invoke-direct {v4, v3}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/DomainProfileBuilder$1;-><init>(Lcom/mastercard/mpsdk/card/profile/RecordsJson;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method
