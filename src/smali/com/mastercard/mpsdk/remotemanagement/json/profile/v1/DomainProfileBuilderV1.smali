.class public Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)[B
    .locals 0

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1;->getPanFromDigitizedCardId(Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 0

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1;->buildContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;)Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 0

    invoke-static {p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1;->buildDsrpData(Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;)Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object p0

    return-object p0
.end method

.method private static buildContactlessPaymentData(Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;)Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$2;-><init>(Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;)V

    return-object v0
.end method

.method public static buildDigitizedCard(Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$1;-><init>(Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;)V

    return-object v0
.end method

.method private static buildDsrpData(Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;)Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$3;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/remotemanagement/json/profile/v1/DomainProfileBuilderV1$3;-><init>(Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;)V

    return-object v0
.end method

.method private static getPanFromDigitizedCardId(Ljava/lang/String;)[B
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x13

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string v0, "F"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "F"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method
