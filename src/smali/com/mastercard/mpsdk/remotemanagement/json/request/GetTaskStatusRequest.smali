.class public Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;
.super Ljava/lang/Object;


# instance fields
.field private final mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private requestId:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "requestId"
    .end annotation
.end field

.field private taskId:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "taskId"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "REMOTE MANAGEMENT"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->requestId:Ljava/lang/String;

    iput-object p2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->taskId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public buildAsJson()Ljava/lang/String;
    .locals 5

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->requestId:Ljava/lang/String;

    return-object v0
.end method

.method public getTaskId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->taskId:Ljava/lang/String;

    return-object v0
.end method

.method public setRequestId(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->requestId:Ljava/lang/String;

    return-object p0
.end method

.method public setTaskId(Ljava/lang/String;)Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->taskId:Ljava/lang/String;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GetTaskStatusRequest{requestId=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", taskId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mpsdk/remotemanagement/json/request/GetTaskStatusRequest;->taskId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
