.class public Lcom/mastercard/mpsdk/implementation/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;


# instance fields
.field private a:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

.field private b:Lcom/mastercard/mpsdk/implementation/d;

.field private c:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/implementation/d;Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/n;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/n;->c:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/n;->a:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    return-void
.end method


# virtual methods
.method public onContactlessTransactionAbort(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    .locals 12

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "abortReason= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :try_start_0
    new-instance v0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/implementation/d;->getCardId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v1, 0x0

    new-array v11, v1, [B

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;-><init>(Ljava/lang/String;[BJJIB[B)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v1, v0}, Lcom/mastercard/mpsdk/implementation/d;->a(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/implementation/d;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/n;->a:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0, v1, p1, p2}, Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;->onContactlessPaymentAborted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V

    return-void
.end method

.method public onContactlessTransactionCompleted(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
    .locals 2

    sget-object v0, Lcom/mastercard/mpsdk/implementation/n$1;->a:[I

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionOutcome()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    :try_start_0
    new-instance v0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/implementation/d;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;-><init>(Ljava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v1, v0}, Lcom/mastercard/mpsdk/implementation/d;->a(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/implementation/d;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/n;->a:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;->onContactlessPaymentCompleted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onContactlessTransactionIncident(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/n;->a:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/n;->b:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;->onContactlessPaymentIncident(Lcom/mastercard/mpsdk/componentinterface/Card;Ljava/lang/Exception;)V

    return-void
.end method
