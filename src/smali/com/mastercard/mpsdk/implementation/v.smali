.class public Lcom/mastercard/mpsdk/implementation/v;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/Mcbp;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

.field private final b:Lcom/mastercard/mpsdk/componentinterface/CardManager;

.field private final c:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

.field private d:Lcom/mastercard/mpsdk/interfaces/SecurityServices;

.field private final e:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

.field private f:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mpsdk/interfaces/ApduProcessor;Lcom/mastercard/mpsdk/interfaces/SecurityServices;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/v;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->f:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/v;->b:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    iput-object p5, p0, Lcom/mastercard/mpsdk/implementation/v;->d:Lcom/mastercard/mpsdk/interfaces/SecurityServices;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/v;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/v;->e:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/v;->c:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    return-void
.end method


# virtual methods
.method public getApduProcessor()Lcom/mastercard/mpsdk/interfaces/ApduProcessor;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->c:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    return-object v0
.end method

.method public getCardManager()Lcom/mastercard/mpsdk/componentinterface/CardManager;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->b:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    return-object v0
.end method

.method public getMpaManagementHelper()Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    return-object v0
.end method

.method public getRemoteCommunicationManager()Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    return-object v0
.end method

.method public getWalletData()Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->e:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/database/WalletData;

    move-result-object v0

    return-object v0
.end method

.method public getWalletSecurityServices()Lcom/mastercard/mpsdk/interfaces/WalletSecurityServices;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->d:Lcom/mastercard/mpsdk/interfaces/SecurityServices;

    return-object v0
.end method

.method public isMobileKeySetAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->e:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getMobileKeySetId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public resetToUninitializedState()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->e:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->wipeAllData()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->d:Lcom/mastercard/mpsdk/interfaces/SecurityServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/SecurityServices;->wipeCryptoParameters()Z

    return-void
.end method

.method public saveWalletData(Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/v;->e:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveWalletData(Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V

    return-void
.end method
