.class public Lcom/mastercard/mpsdk/implementation/MPSdkCardEventListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContactlessPaymentAborted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onContactlessPaymentCompleted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
    .locals 0

    return-void
.end method

.method public onContactlessPaymentIncident(Lcom/mastercard/mpsdk/componentinterface/Card;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onTransactionStopped()V
    .locals 0

    return-void
.end method
