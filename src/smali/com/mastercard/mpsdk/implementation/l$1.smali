.class final Lcom/mastercard/mpsdk/implementation/l$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/l;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

.field final synthetic b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

.field final synthetic c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

.field final synthetic d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/DsrpData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/l$1;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/l$1;->c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/l$1;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAip()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getAip()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCvrMaskAnd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDeclineConditions()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCiacDecline()[B

    move-result-object v0

    return-object v0
.end method

.method public final getExpirationDate()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getExpiryDate()[B

    move-result-object v0

    return-object v0
.end method

.method public final getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getIssuerApplicationData()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPanSequenceNumber()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getPanSequenceNumber()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentAccountReference()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getPar()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTrack2EquivalentData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getTrack2EquivalentData()[B

    move-result-object v0

    return-object v0
.end method

.method public final getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/l$1;->c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    return-object v0
.end method
