.class public Lcom/mastercard/mpsdk/implementation/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/CardManager;
.implements Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;


# instance fields
.field a:Z

.field private final b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

.field private final c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

.field private final d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

.field private final e:Lcom/mastercard/mpsdk/implementation/b;

.field private final f:Z

.field private g:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

.field private h:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;ZLcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;Ljava/util/List;Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;Lcom/mastercard/mpsdk/implementation/g;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;",
            "Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;",
            "Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;",
            "Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;",
            "Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;",
            "Z",
            "Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;",
            "Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;",
            "Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;",
            "Lcom/mastercard/mpsdk/implementation/g;",
            "Z)V"
        }
    .end annotation

    move-object v0, p0

    move v1, p8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SDK | "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/mastercard/mpsdk/implementation/e;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v2

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/e;->h:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/mastercard/mpsdk/implementation/e;->a:Z

    move-object v2, p3

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-object v2, p5

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/e;->c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    move-object v2, p6

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    move-object v2, p9

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/e;->g:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

    iput-boolean v1, v0, Lcom/mastercard/mpsdk/implementation/e;->f:Z

    new-instance v2, Lcom/mastercard/mpsdk/implementation/b;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/implementation/b;-><init>()V

    move-object v3, p10

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->h:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    iput-object v0, v2, Lcom/mastercard/mpsdk/implementation/b;->d:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    iget-object v3, v0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iget-object v3, v0, Lcom/mastercard/mpsdk/implementation/e;->c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->i:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    move-object v3, p2

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    move-object v3, p4

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    move-object v3, p1

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->e:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AccountType= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p14 .. p14}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ProductType= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p14 .. p14}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UcafVersion= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p14 .. p14}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isTransitSupported= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p14 .. p14}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->isTransitSupported()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isUsAipMaskSupported= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p14 .. p14}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->isUsAipMaskSupported()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-object/from16 v3, p14

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->n:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    move-object/from16 v3, p11

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->m:Ljava/util/List;

    move-object/from16 v3, p12

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->l:Ljava/util/List;

    move-object v3, p7

    iput-object v3, v2, Lcom/mastercard/mpsdk/implementation/b;->k:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    iput-boolean v1, v2, Lcom/mastercard/mpsdk/implementation/b;->j:Z

    move-object/from16 v1, p13

    iput-object v1, v2, Lcom/mastercard/mpsdk/implementation/b;->q:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-object/from16 v1, p15

    iput-object v1, v2, Lcom/mastercard/mpsdk/implementation/b;->o:Lcom/mastercard/mpsdk/implementation/g;

    move/from16 v1, p16

    iput-boolean v1, v2, Lcom/mastercard/mpsdk/implementation/b;->p:Z

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/e;->e:Lcom/mastercard/mpsdk/implementation/b;

    return-void
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/implementation/e;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    return-object p0
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/e;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException;

    const-string v1, "Rollover in progress"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v2, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialStatusForCardId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->fromValue(I)Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    move-result-object v2

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v3, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    if-ne v2, v3, :cond_0

    invoke-static {v1}, Lcom/mastercard/mpsdk/implementation/y;->a([B)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v1

    sget-object v4, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_DISCARDED:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v4

    invoke-interface {v3, p1, v1, v4, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->updateTransactionCredentialStatusForCardId(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getAllCardIds()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/implementation/e;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public activateCard(Lcom/mastercard/mpsdk/componentinterface/Card;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cardId= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->a()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->ACTIVATED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->getValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveCardStateByCardId(Ljava/lang/String;I)V

    return-void
.end method

.method public changeWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->a()V

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/e;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;->requestChangeWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot change wallet pin whilst in Card Level Pin Mode"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public deleteAllTransactionCredentials()V
    .locals 2

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/e;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->b()V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot delete all transactions whilst in Card Level Pin Mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deleteCard(Lcom/mastercard/mpsdk/componentinterface/Card;)Ljava/lang/String;
    .locals 12

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->a()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v2, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v9, v2, [Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    const/4 v2, 0x0

    const/4 v10, 0x0

    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v10, v2, :cond_0

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Ljava/lang/String;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v2, p1, v5}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v3, p1, v5}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialTimeStampForCardId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2}, Lcom/mastercard/mpsdk/implementation/y;->a([B)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v6

    new-instance v11, Lcom/mastercard/mpsdk/implementation/e$1;

    move-object v2, v11

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/mastercard/mpsdk/implementation/e$1;-><init>(Lcom/mastercard/mpsdk/implementation/e;Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;Ljava/lang/String;)V

    aput-object v11, v9, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v0, v1, v9}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;->requestDeleteCard(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAllCards()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Card;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->a()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getAllCardIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/mastercard/mpsdk/implementation/e;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;
    .locals 25

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    invoke-direct/range {p0 .. p0}, Lcom/mastercard/mpsdk/implementation/e;->a()V

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :try_start_0
    iget-object v2, v1, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v2, v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getCardProfileByCardId(Ljava/lang/String;)[B

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v4, v1, Lcom/mastercard/mpsdk/implementation/e;->e:Lcom/mastercard/mpsdk/implementation/b;

    new-instance v5, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-direct {v5}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;-><init>()V

    invoke-virtual {v5, v2}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->getCard([B)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v2

    iput-object v2, v4, Lcom/mastercard/mpsdk/implementation/b;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    iput-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->g:Ljava/lang/String;

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->d:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->e:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->h:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->i:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->k:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/mastercard/mpsdk/implementation/b;->n:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mastercard/mpsdk/implementation/c;

    iget-object v6, v4, Lcom/mastercard/mpsdk/implementation/b;->g:Ljava/lang/String;

    iget-object v7, v4, Lcom/mastercard/mpsdk/implementation/b;->d:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    iget-object v8, v4, Lcom/mastercard/mpsdk/implementation/b;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iget-object v9, v4, Lcom/mastercard/mpsdk/implementation/b;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    iget-object v10, v4, Lcom/mastercard/mpsdk/implementation/b;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    iget-object v11, v4, Lcom/mastercard/mpsdk/implementation/b;->i:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    iget-boolean v12, v4, Lcom/mastercard/mpsdk/implementation/b;->j:Z

    iget-object v13, v4, Lcom/mastercard/mpsdk/implementation/b;->k:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    iget-object v14, v4, Lcom/mastercard/mpsdk/implementation/b;->h:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    iget-object v15, v4, Lcom/mastercard/mpsdk/implementation/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    iget-object v2, v4, Lcom/mastercard/mpsdk/implementation/b;->e:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    iget-object v5, v4, Lcom/mastercard/mpsdk/implementation/b;->m:Ljava/util/List;

    iget-object v3, v4, Lcom/mastercard/mpsdk/implementation/b;->l:Ljava/util/List;

    iget-object v1, v4, Lcom/mastercard/mpsdk/implementation/b;->q:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-object/from16 v23, v1

    iget-object v1, v4, Lcom/mastercard/mpsdk/implementation/b;->n:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    move-object/from16 v24, v1

    iget-object v1, v4, Lcom/mastercard/mpsdk/implementation/b;->o:Lcom/mastercard/mpsdk/implementation/g;

    iget-boolean v4, v4, Lcom/mastercard/mpsdk/implementation/b;->p:Z

    move-object/from16 v17, v5

    move-object v5, v0

    move-object/from16 v16, v2

    move-object/from16 v18, v3

    move-object/from16 v19, v23

    move-object/from16 v20, v24

    move-object/from16 v21, v1

    move/from16 v22, v4

    invoke-direct/range {v5 .. v22}, Lcom/mastercard/mpsdk/implementation/c;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;ZLcom/mastercard/mpsdk/interfaces/TransactionEventListener;Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;Ljava/util/List;Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;Lcom/mastercard/mpsdk/implementation/g;Z)V

    move-object v3, v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0

    :catch_0
    :cond_1
    const/4 v3, 0x0

    :goto_0
    return-object v3
.end method

.method public getCredentialsReplenishmentPolicy()Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->g:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

    return-object v0
.end method

.method public onCardPinReset(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardMobilePinResetCompleted(Ljava/lang/String;)Z

    return-void
.end method

.method public onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinChangeFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onChangeCardPinSucceeded(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinChangeCompleted(Ljava/lang/String;)Z

    return-void
.end method

.method public onChangeWalletMobilePinStarted(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onChangeWalletMobilePinStarted(Ljava/lang/String;)Z

    return-void
.end method

.method public onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onWalletPinChangeFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onChangeWalletPinSucceeded()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onWalletPinChangeCompleted()Z

    return-void
.end method

.method public onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {p2, p1, p3, p4, p5}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->wipeTransactionLogsForCardId(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->wipeTransactionCredentialsForCardId(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->wipeCardProfileAndRelatedData(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onDeleteCardCompleted(Ljava/lang/String;)Z

    return-void
.end method

.method public onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {p1, p2, p3, p4}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;-><init>()V

    invoke-virtual {v0, p1}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->getContent(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p2, v0, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveCardProfile(Ljava/lang/String;[BLjava/lang/String;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardProvisionCompleted(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->ROLLOVER_IN_PROGRESS:Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/returncodes/ErrorCode;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1, p1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :goto_0
    throw p1
.end method

.method public onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {p2, p1, p3, p4, p5}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;)V"
        }
    .end annotation

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v3, p1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialStatusForCardId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    sget-object v4, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v4

    if-eq v3, v4, :cond_0

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_3

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0000"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "000000"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    new-instance v9, Lcom/mastercard/mpsdk/implementation/e$2;

    invoke-direct {v9, p0, v8, v2, v10}, Lcom/mastercard/mpsdk/implementation/e$2;-><init>(Lcom/mastercard/mpsdk/implementation/e;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;Ljava/lang/String;)V

    new-instance v2, Lcom/mastercard/mpsdk/implementation/e$3;

    move-object v5, v2

    move-object v6, p0

    move-object v7, p1

    invoke-direct/range {v5 .. v10}, Lcom/mastercard/mpsdk/implementation/e$3;-><init>(Lcom/mastercard/mpsdk/implementation/e;Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getCardProfileVersionByCardId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v2, p1, p2, v1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->wipeOldAndSaveNewTransactionCredentialsForCardId(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    :cond_3
    iget-object p2, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    invoke-interface {p2, p1, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onReplenishCompleted(Ljava/lang/String;I)Z

    return-void
.end method

.method public onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onRequestSessionFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onRequestSessionSuccess()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onRequestSessionCompleted()Z

    return-void
.end method

.method public onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinSetFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onSetCardPinSucceeded(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinSetCompleted(Ljava/lang/String;)Z

    return-void
.end method

.method public onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onSetWalletPinSucceeded()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSetWalletPinCompleted()Z

    return-void
.end method

.method public onSystemHealthCompleted()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSystemHealthCompleted()Z

    return-void
.end method

.method public onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onTaskStatusReceived(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onTaskStatusCompleted(Ljava/lang/String;)Z

    return-void
.end method

.method public onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onTaskStatusFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    return-void
.end method

.method public onWalletPinReset()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->b()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->d:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onWalletMobilePinResetCompleted()Z

    return-void
.end method

.method public setWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->a()V

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/e;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;->requestSetWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot change wallet pin whilst in Card Level Pin Mode"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public suspendCard(Lcom/mastercard/mpsdk/componentinterface/Card;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cardId= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/e;->a()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e;->b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->SUSPENDED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->getValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveCardStateByCardId(Ljava/lang/String;I)V

    return-void
.end method
