.class public Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;
.implements Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;
    }
.end annotation


# instance fields
.field final mCard:Lcom/mastercard/mpsdk/implementation/d;

.field private mCredentialsAccessibilityPolicies:Lcom/mastercard/mpsdk/implementation/g;

.field private mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private mPinDataProvider:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

.field protected mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

.field private final mTransactionCrypto:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/implementation/d;Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/implementation/g;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mTransactionCrypto:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mPinDataProvider:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCredentialsAccessibilityPolicies:Lcom/mastercard/mpsdk/implementation/g;

    return-void
.end method

.method private isValidCredentials([B)Z
    .locals 0

    if-eqz p1, :cond_0

    array-length p1, p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private prepareAtc(I)[B
    .locals 3

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0000"

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public areUmdCredentialsSubjectToCvmFor(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Z
    .locals 0

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCredentialsAccessibilityPolicies:Lcom/mastercard/mpsdk/implementation/g;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/implementation/g;->a()Z

    const/4 p1, 0x0

    return p1
.end method

.method buildTransactionCredentials(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/implementation/z;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mdSessionKey="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "umdSessionKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->getDummyCredentials()Lcom/mastercard/mpsdk/implementation/z;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/implementation/z;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->prepareAtc(I)[B

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getIdn()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/mastercard/mpsdk/implementation/z;-><init>([B[B[B[B)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->wipe()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/implementation/d;->d()V

    return-object v0
.end method

.method fromSingleUseKeyToSessionKey(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "encryptedSingleUseKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p1, :cond_0

    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->getRandomByteArray(I)[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isEncryptedPinAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    return-object p1

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mTransactionCrypto:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mPinDataProvider:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->deriveSessionKeyFromSingleUseKey(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sessionKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getCredentials(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mpsdk/implementation/z;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CredentialsScope= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_3

    sget-object p1, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$1;->a:[I

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/implementation/d;->getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->ordinal()I

    move-result v0

    aget p1, p1, v0

    packed-switch p1, :pswitch_data_0

    move-object p1, v1

    goto :goto_0

    :pswitch_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    goto :goto_0

    :pswitch_1
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukContactlessUmd()[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    invoke-interface {v0, v2}, Lcom/mastercard/mpsdk/implementation/d;->getCdCvmModel(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;)Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v0

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CARD_LIKE:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    if-eq v0, v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->fromSingleUseKeyToSessionKey(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mTransactionCrypto:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->getRandomByteArray(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->encryptSessionKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    :goto_0
    new-instance v0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;-><init>(Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;Lcom/mastercard/mpsdk/implementation/d;I)V

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-virtual {v0, v2}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)V

    if-eqz p1, :cond_2

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    :goto_1
    invoke-virtual {p0, v0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->buildTransactionCredentials(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/implementation/z;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v1

    :cond_3
    sget-object p1, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$1;->a:[I

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/implementation/d;->getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->ordinal()I

    move-result v0

    aget p1, p1, v0

    packed-switch p1, :pswitch_data_1

    move-object p1, v1

    goto :goto_2

    :pswitch_2
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentUmd()[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    goto :goto_2

    :pswitch_3
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukRemotePaymentUmd()[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->fromSingleUseKeyToSessionKey(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    :goto_2
    new-instance v0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;-><init>(Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;Lcom/mastercard/mpsdk/implementation/d;I)V

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->DSRP:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-virtual {v0, v2}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)V

    if-eqz p1, :cond_4

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentMd()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    goto :goto_1

    :cond_4
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public getDummyCredentials()Lcom/mastercard/mpsdk/implementation/z;
    .locals 5

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mTransactionCrypto:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->getRandomByteArray(I)[B

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->encryptSessionKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v0

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mTransactionCrypto:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->getRandomByteArray(I)[B

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->encryptSessionKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mTransactionCrypto:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    const/16 v3, 0x8

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->getRandomByteArray(I)[B

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->encryptSessionKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [B

    fill-array-data v3, :array_0

    new-instance v4, Lcom/mastercard/mpsdk/implementation/z;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-direct {v4, v3, v2, v0, v1}, Lcom/mastercard/mpsdk/implementation/z;-><init>([B[B[B[B)V

    return-object v4

    :array_0
    .array-data 1
        0x0t
        0x1t
    .end array-data
.end method

.method public hasValidCredentialsFor(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Z
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "credentialsScope= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/implementation/d;->a()Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$1;->b:[I

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    invoke-interface {p1, v2}, Lcom/mastercard/mpsdk/implementation/d;->getCdCvmModel(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;)Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object p1

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CARD_LIKE:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    if-ne p1, v2, :cond_1

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    return p1

    :cond_1
    sget-object p1, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$1;->a:[I

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/implementation/d;->getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->ordinal()I

    move-result v2

    aget p1, p1, v2

    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_2

    return v0

    :cond_2
    return v1

    :pswitch_2
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukContactlessUmd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_3

    return v0

    :cond_3
    return v1

    :goto_0
    :pswitch_3
    sget-object p1, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$1;->a:[I

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/implementation/d;->getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->ordinal()I

    move-result v2

    aget p1, p1, v2

    packed-switch p1, :pswitch_data_2

    goto :goto_1

    :pswitch_4
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentMd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentUmd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_4

    return v0

    :cond_4
    return v1

    :pswitch_5
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentMd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mSingleUseKey:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukRemotePaymentUmd()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->isValidCredentials([B)Z

    move-result p1

    if-eqz p1, :cond_5

    return v0

    :cond_5
    :goto_1
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method isEncryptedPinAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mPinDataProvider:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;->mPinDataProvider:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method
