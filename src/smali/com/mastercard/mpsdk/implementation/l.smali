.class public Lcom/mastercard/mpsdk/implementation/l;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/l;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mpsdk/implementation/l;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/mastercard/mpsdk/componentinterface/DsrpData;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
    .locals 5

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    sget-object v1, Lcom/mastercard/mpsdk/implementation/l$2;->a:[I

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    :goto_0
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    sget-object v2, Lcom/mastercard/mpsdk/implementation/l$2;->b:[I

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    goto :goto_1

    :pswitch_4
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    goto :goto_1

    :pswitch_5
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    :goto_1
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0_PLUS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    sget-object v3, Lcom/mastercard/mpsdk/implementation/l$2;->c:[I

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0_PLUS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    :goto_2
    invoke-static {p0, v0, v1, v2}, Lcom/mastercard/mpsdk/implementation/l;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public static a(Lcom/mastercard/mpsdk/componentinterface/DsrpData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "hostUmdConfig= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cdCvmModel= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ucafVersion= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/mastercard/mpsdk/implementation/l$1;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/mastercard/mpsdk/implementation/l$1;-><init>(Lcom/mastercard/mpsdk/componentinterface/DsrpData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)V

    return-object v0
.end method
