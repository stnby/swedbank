.class final Lcom/mastercard/mpsdk/implementation/f$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/f$1;->getAlternateContactlessPaymentData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/implementation/f$1;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/f$1;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/f$1$1;->a:Lcom/mastercard/mpsdk/implementation/f$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$1;->a:Lcom/mastercard/mpsdk/implementation/f$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getAid()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$1;->a:Lcom/mastercard/mpsdk/implementation/f$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDeclineConditions()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$1;->a:Lcom/mastercard/mpsdk/implementation/f$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getCiacDecline()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDeclineConditionsOnPpms()[B
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0
.end method

.method public final getGpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$1;->a:Lcom/mastercard/mpsdk/implementation/f$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getgpoResponse()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$1;->a:Lcom/mastercard/mpsdk/implementation/f$1;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getPaymentFci()[B

    move-result-object v0

    return-object v0
.end method
