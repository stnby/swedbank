.class public Lcom/mastercard/mpsdk/implementation/TransactionLogJson;
.super Ljava/lang/Object;


# instance fields
.field private mAmount:J
    .annotation runtime Lflexjson/h;
        a = "amount"
    .end annotation
.end field

.field private mCryptogramFormat:B
    .annotation runtime Lflexjson/h;
        a = "cryptogramFormat"
    .end annotation
.end field

.field private mCurrencyCode:I
    .annotation runtime Lflexjson/h;
        a = "currencyCode"
    .end annotation
.end field

.field private mDate:J
    .annotation runtime Lflexjson/h;
        a = "date"
    .end annotation
.end field

.field private mTokenUniqueReference:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "tokenUniqueReference"
    .end annotation
.end field

.field private mTransactionId:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "transactionId"
    .end annotation
.end field

.field private mUnpredictableNumber:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "unpredictableNumber"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BJJIB[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mTokenUniqueReference:Ljava/lang/String;

    if-eqz p2, :cond_0

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mUnpredictableNumber:Ljava/lang/String;

    :cond_0
    iput-wide p3, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mDate:J

    iput-wide p5, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mAmount:J

    iput p7, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mCurrencyCode:I

    iput-byte p8, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mCryptogramFormat:B

    invoke-static {p9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mTransactionId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAmount()J
    .locals 2

    iget-wide v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mAmount:J

    return-wide v0
.end method

.method public getCryptogramFormat()B
    .locals 1

    iget-byte v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mCryptogramFormat:B

    return v0
.end method

.method public getCurrencyCode()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mCurrencyCode:I

    return v0
.end method

.method public getDate()J
    .locals 2

    iget-wide v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mDate:J

    return-wide v0
.end method

.method public getTokenUniqueReference()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mTokenUniqueReference:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mTransactionId:Ljava/lang/String;

    return-object v0
.end method

.method public getUnpredictableNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mUnpredictableNumber:Ljava/lang/String;

    return-object v0
.end method

.method public setAmount(J)V
    .locals 0

    iput-wide p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mAmount:J

    return-void
.end method

.method public setCryptogramFormat(B)V
    .locals 0

    iput-byte p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mCryptogramFormat:B

    return-void
.end method

.method public setCurrencyCode(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mCurrencyCode:I

    return-void
.end method

.method public setDate(J)V
    .locals 0

    iput-wide p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mDate:J

    return-void
.end method

.method public setTokenUniqueReference(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mTokenUniqueReference:Ljava/lang/String;

    return-void
.end method

.method public setTransactionId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mTransactionId:Ljava/lang/String;

    return-void
.end method

.method public setUnpredictableNumber(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->mUnpredictableNumber:Ljava/lang/String;

    return-void
.end method
