.class final Lcom/mastercard/mpsdk/implementation/s$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/s;->getRemoteManagementKeysForMobileKeySetId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field final synthetic c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field final synthetic d:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field final synthetic e:Lcom/mastercard/mpsdk/implementation/s;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/s;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/s$2;->e:Lcom/mastercard/mpsdk/implementation/s;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/s$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/s$2;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/s$2;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    iput-object p5, p0, Lcom/mastercard/mpsdk/implementation/s$2;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$2;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    return-object v0
.end method

.method public final getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$2;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    return-object v0
.end method

.method public final getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$2;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    return-object v0
.end method

.method public final getKeySetId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$2;->a:Ljava/lang/String;

    return-object v0
.end method
