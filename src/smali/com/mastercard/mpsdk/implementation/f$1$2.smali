.class final Lcom/mastercard/mpsdk/implementation/f$1$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/f$1;->getRecords()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/Records;

.field final synthetic b:Lcom/mastercard/mpsdk/implementation/f$1;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/f$1;Lcom/mastercard/mpsdk/componentinterface/Records;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/f$1$2;->b:Lcom/mastercard/mpsdk/implementation/f$1;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/f$1$2;->a:Lcom/mastercard/mpsdk/componentinterface/Records;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getRecordNumber()B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$2;->a:Lcom/mastercard/mpsdk/componentinterface/Records;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Records;->getRecordNumber()B

    move-result v0

    return v0
.end method

.method public final getRecordValue()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$2;->a:Lcom/mastercard/mpsdk/componentinterface/Records;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Records;->getRecordValue()[B

    move-result-object v0

    return-object v0
.end method

.method public final getSfi()B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1$2;->a:Lcom/mastercard/mpsdk/componentinterface/Records;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Records;->getSfi()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    int-to-byte v0, v0

    shr-int/lit8 v0, v0, 0x3

    int-to-byte v0, v0

    return v0
.end method
