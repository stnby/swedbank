.class final Lcom/mastercard/mpsdk/implementation/q$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/q;->getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/implementation/q;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/q;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->b(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/implementation/q$2;->a:[I

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v1}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;->DEBIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;->CREDIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCardCountryCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getCardCountryCode()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPan()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getPan()[B

    move-result-object v0

    return-object v0
.end method

.method public final getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->b(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/implementation/q$2;->b:[I

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v1}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->PREPAID:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->DEBIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->CREDIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->COMMERCIAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final isTransactionIdRequired()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q$1;->a:Lcom/mastercard/mpsdk/implementation/q;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/q;->a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->isTransactionIdRequired()Z

    move-result v0

    return v0
.end method
