.class public Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/ApduProcessor;


# instance fields
.field private final mActiveCardProvider:Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;

.field private final mApduListener:Lcom/mastercard/mpsdk/interfaces/ApduListener;

.field private final mCardManager:Lcom/mastercard/mpsdk/componentinterface/CardManager;

.field private mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

.field private mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;Lcom/mastercard/mpsdk/interfaces/ApduListener;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mActiveCardProvider:Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mApduListener:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCardManager:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    return-void
.end method

.method private determineApduCommandType$d0aa43e([B)I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "apdu= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->INS_OFFSET:I

    aget-byte p1, p1, v0

    const/16 v0, -0x5c

    if-eq p1, v0, :cond_2

    const/16 v0, -0x4e

    if-eq p1, v0, :cond_1

    const/16 v0, -0x36

    if-eq p1, v0, :cond_0

    sget p1, Lcom/mastercard/mpsdk/implementation/a;->d:I

    return p1

    :cond_0
    sget p1, Lcom/mastercard/mpsdk/implementation/a;->a:I

    return p1

    :cond_1
    sget p1, Lcom/mastercard/mpsdk/implementation/a;->b:I

    return p1

    :cond_2
    sget p1, Lcom/mastercard/mpsdk/implementation/a;->c:I

    return p1
.end method

.method private notifyApduListener([B[B)[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "apdu= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "responseApdu= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mApduListener:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor$1;->a:[I

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->determineApduCommandType$d0aa43e([B)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;-><init>([B)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mApduListener:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;->getFileName()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    invoke-interface {p1, p2, v0}, Lcom/mastercard/mpsdk/interfaces/ApduListener;->onSelectApdu([B[B)[B

    move-result-object p1

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;-><init>([B)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mApduListener:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;->getSfiNumber()B

    move-result v1

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;->getRecordNumber()B

    move-result v0

    invoke-interface {p1, v1, v0, p2}, Lcom/mastercard/mpsdk/interfaces/ApduListener;->onReadRecord(BB[B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object p2, p1

    :catch_0
    :cond_0
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "modifiedResponseApdu= "

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public processApdu([B)[B
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cApdu= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mApduListener:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mApduListener:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/interfaces/ApduListener;->onTransactionStarted()V

    :cond_0
    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mActiveCardProvider:Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;

    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    iget-object v4, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCardManager:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    invoke-interface {v2, v3, v4}, Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;->getActiveCard(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/implementation/d;

    iput-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/implementation/d;->c()V

    :cond_1
    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    const/16 v3, 0x6985

    if-nez v2, :cond_2

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(C)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    return-object p1

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    invoke-interface {v2, p1}, Lcom/mastercard/mpsdk/implementation/d;->a([B)[B

    move-result-object v2
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0, p1, v2}, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->notifyApduListener([B[B)[B

    move-result-object p1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v2, v3

    return-object p1

    :catch_0
    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(C)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    return-object p1
.end method

.method public processOnDeactivated()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;->mCurrentCard:Lcom/mastercard/mpsdk/implementation/d;

    return-void
.end method
