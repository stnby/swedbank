.class public Lcom/mastercard/mpsdk/implementation/y;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[B

.field private final c:[B

.field private final d:[B

.field private final e:[B

.field private final f:[B

.field private final g:[B

.field private final h:[B

.field private final i:[B

.field private final j:I

.field private final k:[B

.field private l:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/y;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->l:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->b:[B

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->c:[B

    invoke-static {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->d:[B

    invoke-static {p5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->e:[B

    invoke-static {p6}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->f:[B

    invoke-static {p7}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->g:[B

    invoke-static {p8}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->h:[B

    invoke-static {p9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->i:[B

    iput p10, p0, Lcom/mastercard/mpsdk/implementation/y;->j:I

    invoke-static {p11}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/y;->k:[B

    return-void
.end method

.method public static a([B)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;
    .locals 14

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    const-class v1, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyContactlessMd()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyContactlessMd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, v1

    :goto_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyRemotePaymentMd()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyRemotePaymentMd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    move-object v8, v0

    goto :goto_1

    :cond_1
    move-object v8, v1

    :goto_1
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSukContactlessUmd()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSukContactlessUmd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object v5, v1

    :goto_2
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSukRemotePaymentUmd()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSukRemotePaymentUmd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    move-object v6, v0

    goto :goto_3

    :cond_3
    move-object v6, v1

    :goto_3
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyContactlessUmd()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyContactlessUmd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    move-object v9, v0

    goto :goto_4

    :cond_4
    move-object v9, v1

    :goto_4
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyRemotePaymentUmd()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSessionKeyRemotePaymentUmd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    :cond_5
    move-object v10, v1

    new-instance v0, Lcom/mastercard/mpsdk/implementation/y;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getSukInfo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v4

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getIdn()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v11

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getAtc()I

    move-result v12

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->getHash()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v13

    move-object v2, v0

    invoke-direct/range {v2 .. v13}, Lcom/mastercard/mpsdk/implementation/y;-><init>(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)V

    return-object v0
.end method

.method public static a(Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;)[B
    .locals 13

    new-instance v12, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getInfo()[B

    move-result-object v2

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukContactlessUmd()[B

    move-result-object v3

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukRemotePaymentUmd()[B

    move-result-object v4

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object v5

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentMd()[B

    move-result-object v6

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object v7

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentUmd()[B

    move-result-object v8

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getIdn()[B

    move-result-object v9

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v10

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getHash()[B

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;-><init>(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)V

    new-instance p0, Lcom/mastercard/mpsdk/utils/json/JsonUtils;

    const-class v0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/utils/json/JsonUtils;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v12}, Lcom/mastercard/mpsdk/utils/json/JsonUtils;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getAtc()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Atc= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mastercard/mpsdk/implementation/y;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/mastercard/mpsdk/implementation/y;->j:I

    return v0
.end method

.method public getHash()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mHash= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->k:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->k:[B

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Id= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getIdn()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Idn= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->i:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->i:[B

    return-object v0
.end method

.method public getInfo()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SukInfo= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->b:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->b:[B

    return-object v0
.end method

.method public getSessionKeyContactlessMd()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SessionKeyContactlessMd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->e:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->e:[B

    return-object v0
.end method

.method public getSessionKeyContactlessUmd()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SessionKeyContactlessUmd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->g:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->g:[B

    return-object v0
.end method

.method public getSessionKeyRemotePaymentMd()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SessionKeyRemotePaymentMd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->f:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->f:[B

    return-object v0
.end method

.method public getSessionKeyRemotePaymentUmd()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mSessionKeyRemotePaymentUmd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->h:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->h:[B

    return-object v0
.end method

.method public getSukContactlessUmd()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SukContactlessUmd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->c:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->c:[B

    return-object v0
.end method

.method public getSukRemotePaymentUmd()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SukContactlessUmd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/y;->d:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->d:[B

    return-object v0
.end method

.method public getTimestamp()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public setStatus(I)V
    .locals 0

    return-void
.end method

.method public wipe()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->b:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->c:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->d:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->e:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->f:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->i:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/y;->k:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    return-void
.end method
