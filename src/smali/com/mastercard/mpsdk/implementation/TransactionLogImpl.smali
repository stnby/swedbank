.class public Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;


# static fields
.field public static final FORMAT_FAILED:B = 0x4t

.field public static final FORMAT_MAGSTRIPE:B = 0x2t

.field public static final FORMAT_MCHIP:B = 0x1t

.field public static final FORMAT_PPMC_DSRP:B = 0x3t

.field private static sLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# instance fields
.field private final mAmount:J

.field private final mCryptogramFormat:B

.field private final mCurrencyCode:I

.field private final mDate:J

.field private final mTokenUniqueReference:Ljava/lang/String;

.field private final mTransactionId:[B

.field private final mUnpredictableNumber:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->sLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTokenUniqueReference:Ljava/lang/String;

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mUnpredictableNumber:[B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mDate:J

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getAuthorizedAmount()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mAmount:J

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getCurrencyCode()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCurrencyCode:I

    invoke-static {p2}, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->getTransactionTechnology(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)B

    move-result p1

    iput-byte p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCryptogramFormat:B

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionId()[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTransactionId:[B

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;[B)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTokenUniqueReference:Ljava/lang/String;

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getUnpredictableNumber()[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mUnpredictableNumber:[B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mDate:J

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getAmount()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mAmount:J

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCurrencyCode()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCurrencyCode:I

    const/4 p1, 0x3

    iput-byte p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCryptogramFormat:B

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTransactionId:[B

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BJJIB[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTokenUniqueReference:Ljava/lang/String;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mUnpredictableNumber:[B

    iput-wide p3, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mDate:J

    iput-wide p5, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mAmount:J

    iput p7, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCurrencyCode:I

    iput-byte p8, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCryptogramFormat:B

    iput-object p9, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTransactionId:[B

    return-void
.end method

.method public static fromValue([B)Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;
    .locals 11

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    const-class v1, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;

    new-instance v10, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getTokenUniqueReference()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getUnpredictableNumber()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [B

    :goto_0
    move-object v2, v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getUnpredictableNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    goto :goto_0

    :goto_1
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getDate()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getAmount()J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getCurrencyCode()I

    move-result v7

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getCryptogramFormat()B

    move-result v8

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;->getTransactionId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;-><init>(Ljava/lang/String;[BJJIB[B)V

    return-object v10
.end method

.method private static getTransactionTechnology(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)B
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-interface {p0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTerminalInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object v1

    if-nez v1, :cond_1

    return v0

    :cond_1
    invoke-interface {v1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;->getTerminalTechnology()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    move-result-object v1

    invoke-interface {p0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionOutcome()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    move-result-object p0

    if-eqz v1, :cond_3

    if-nez p0, :cond_2

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl$1;->b:[I

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result p0

    aget p0, v2, p0

    packed-switch p0, :pswitch_data_0

    return v0

    :pswitch_0
    const/4 p0, 0x4

    return p0

    :pswitch_1
    sget-object p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl$1;->a:[I

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->ordinal()I

    move-result v1

    aget p0, p0, v1

    packed-switch p0, :pswitch_data_1

    return v0

    :pswitch_2
    const/4 p0, 0x2

    return p0

    :pswitch_3
    const/4 p0, 0x1

    return p0

    :cond_3
    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static serialize(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)[B
    .locals 11

    new-instance v10, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getUnpredictableNumber()[B

    move-result-object v2

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getDate()J

    move-result-wide v3

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getAmount()J

    move-result-wide v5

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getCurrencyCode()I

    move-result v7

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getCryptogramFormat()B

    move-result v8

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getTransactionId()[B

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;-><init>(Ljava/lang/String;[BJJIB[B)V

    new-instance p0, Lcom/mastercard/mpsdk/utils/json/JsonUtils;

    const-class v0, Lcom/mastercard/mpsdk/implementation/TransactionLogJson;

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/utils/json/JsonUtils;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v10}, Lcom/mastercard/mpsdk/utils/json/JsonUtils;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getAmount()J
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Amount "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mAmount:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mAmount:J

    return-wide v0
.end method

.method public getCardId()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenUniqueReference= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTokenUniqueReference:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTokenUniqueReference:Ljava/lang/String;

    return-object v0
.end method

.method public getCryptogramFormat()B
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mCryptogramFormat= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [B

    iget-byte v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCryptogramFormat:B

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCryptogramFormat:B

    return v0
.end method

.method public getCurrencyCode()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CurrencyCode= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCurrencyCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mCurrencyCode:I

    return v0
.end method

.method public getDate()J
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Date= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mDate:J

    return-wide v0
.end method

.method public getTransactionId()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTransactionId:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTransactionId:[B

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TransactionId= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getUnpredictableNumber()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mUnpredictableNumber:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mUnpredictableNumber:[B

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnpredictableNumber= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public wipe()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mTransactionId:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->mUnpredictableNumber:[B

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->clearByteArray([B)V

    return-void
.end method
