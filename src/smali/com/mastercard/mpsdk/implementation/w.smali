.class public final Lcom/mastercard/mpsdk/implementation/w;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/McbpInitializer;


# instance fields
.field private A:Lcom/mastercard/mpsdk/implementation/g;

.field private B:Z

.field private final a:Lcom/mastercard/mpsdk/implementation/u;

.field private b:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

.field private d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

.field private e:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

.field private f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

.field private g:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

.field private h:Lcom/mastercard/mpsdk/interfaces/ApduListener;

.field private i:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

.field private j:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

.field private k:Lcom/mastercard/mpsdk/implementation/x;

.field private l:Z

.field private m:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

.field private n:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

.field private o:Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;

.field private p:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field private t:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

.field private u:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

.field private v:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

.field private w:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

.field private x:[B

.field private y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

.field private z:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/implementation/u;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/w;->l:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/w;->q:Ljava/util/List;

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/w;->r:Ljava/util/List;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->a:Lcom/mastercard/mpsdk/implementation/u;

    new-instance p1, Lcom/mastercard/mpsdk/implementation/x;

    invoke-direct {p1}, Lcom/mastercard/mpsdk/implementation/x;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->k:Lcom/mastercard/mpsdk/implementation/x;

    return-void
.end method

.method private static a(I[I)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget v3, p1, v2

    if-lez v3, :cond_2

    const/16 v4, 0x3c

    if-le v3, v4, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v0

    :cond_3
    if-lez p0, :cond_5

    const/4 v1, 0x5

    if-gt p0, v1, :cond_5

    array-length p1, p1

    if-eq p0, p1, :cond_4

    goto :goto_2

    :cond_4
    const/4 p0, 0x1

    return p0

    :cond_5
    :goto_2
    return v0
.end method


# virtual methods
.method public final initialize()Lcom/mastercard/mpsdk/interfaces/Mcbp;
    .locals 26

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->i:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->e:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->n:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->o:Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->j:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->p:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->s:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->b:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mastercard/mpsdk/remotemanagement/b;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/remotemanagement/b;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->b:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    :cond_0
    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->g:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    if-nez v1, :cond_1

    new-instance v1, Lcom/mastercard/mpsdk/implementation/AdviceManager;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/implementation/AdviceManager;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->g:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    :cond_1
    new-instance v1, Lcom/mastercard/mpsdk/implementation/i;

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getDatabaseUpgradeCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    move-result-object v2

    iget-object v3, v0, Lcom/mastercard/mpsdk/implementation/w;->x:[B

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mpsdk/implementation/i;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;[B)V

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    if-nez v2, :cond_2

    new-instance v2, Lcom/mastercard/mpsdk/database/a;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/database/a;-><init>()V

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    :cond_2
    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->z:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    if-nez v2, :cond_b

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iget-object v3, v0, Lcom/mastercard/mpsdk/implementation/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getDatabaseCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->withDatabaseCrypto(Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-result-object v2

    const/16 v3, 0xa

    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->withMaxTransactionLogsCount(I)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->usingOptionalDatabaseUpgradeHelper(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-result-object v1

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->z:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    invoke-interface {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->usingOptionalSecurityIncidentService(Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-result-object v1

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->a:Lcom/mastercard/mpsdk/implementation/u;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/implementation/u;->a()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->initialize(Landroid/content/Context;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->m:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

    if-nez v1, :cond_3

    new-instance v1, Lcom/mastercard/mpsdk/implementation/h;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/implementation/h;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->m:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

    :cond_3
    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->h:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    if-nez v1, :cond_4

    new-instance v1, Lcom/mastercard/mpsdk/implementation/MPSdkApduListener;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/implementation/MPSdkApduListener;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->h:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    :cond_4
    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->v:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    if-nez v1, :cond_5

    new-instance v1, Lcom/mastercard/mpsdk/implementation/j;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/implementation/j;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->v:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    :cond_5
    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->v:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;->getRetryCount()I

    move-result v1

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->v:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;->getRetryIntervals()[I

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mastercard/mpsdk/implementation/w;->a(I[I)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    if-nez v1, :cond_6

    new-instance v1, Lcom/mastercard/mpsdk/implementation/w$1;

    invoke-direct {v1, v0}, Lcom/mastercard/mpsdk/implementation/w$1;-><init>(Lcom/mastercard/mpsdk/implementation/w;)V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    :cond_6
    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    move-result-object v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    goto :goto_0

    :cond_7
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->A:Lcom/mastercard/mpsdk/implementation/g;

    if-nez v1, :cond_8

    new-instance v1, Lcom/mastercard/mpsdk/implementation/k;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/implementation/k;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->A:Lcom/mastercard/mpsdk/implementation/g;

    :cond_8
    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->b:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getRemoteManagementCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    move-result-object v3

    iget-object v4, v0, Lcom/mastercard/mpsdk/implementation/w;->e:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v5, v0, Lcom/mastercard/mpsdk/implementation/w;->k:Lcom/mastercard/mpsdk/implementation/x;

    new-instance v6, Lcom/mastercard/mpsdk/implementation/m;

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-direct {v6, v1}, Lcom/mastercard/mpsdk/implementation/m;-><init>(Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;)V

    iget-object v7, v0, Lcom/mastercard/mpsdk/implementation/w;->s:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v8, v0, Lcom/mastercard/mpsdk/implementation/w;->v:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    invoke-interface/range {v2 .. v8}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;->initialize(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    new-instance v1, Lcom/mastercard/mpsdk/implementation/e;

    move-object v9, v1

    iget-object v10, v0, Lcom/mastercard/mpsdk/implementation/w;->f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getTransactionCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    move-result-object v11

    iget-object v12, v0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iget-object v13, v0, Lcom/mastercard/mpsdk/implementation/w;->g:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    iget-object v14, v0, Lcom/mastercard/mpsdk/implementation/w;->b:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    iget-object v15, v0, Lcom/mastercard/mpsdk/implementation/w;->i:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->j:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    move-object/from16 v16, v2

    iget-boolean v2, v0, Lcom/mastercard/mpsdk/implementation/w;->l:Z

    move/from16 v17, v2

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->m:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

    move-object/from16 v18, v2

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->n:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    move-object/from16 v19, v2

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->r:Ljava/util/List;

    move-object/from16 v20, v2

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->q:Ljava/util/List;

    move-object/from16 v21, v2

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->u:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-object/from16 v22, v2

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    move-object/from16 v23, v2

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->A:Lcom/mastercard/mpsdk/implementation/g;

    move-object/from16 v24, v2

    iget-boolean v2, v0, Lcom/mastercard/mpsdk/implementation/w;->B:Z

    move/from16 v25, v2

    invoke-direct/range {v9 .. v25}, Lcom/mastercard/mpsdk/implementation/e;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;ZLcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;Ljava/util/List;Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;Lcom/mastercard/mpsdk/implementation/g;Z)V

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->k:Lcom/mastercard/mpsdk/implementation/x;

    iget-object v2, v2, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;

    iget-object v3, v0, Lcom/mastercard/mpsdk/implementation/w;->o:Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;

    iget-object v4, v0, Lcom/mastercard/mpsdk/implementation/w;->h:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    invoke-direct {v2, v3, v4, v1}, Lcom/mastercard/mpsdk/implementation/MpSdkApduProcessor;-><init>(Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;Lcom/mastercard/mpsdk/interfaces/ApduListener;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V

    iput-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->w:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    new-instance v7, Lcom/mastercard/mpsdk/implementation/o;

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/w;->p:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-direct {v7, v2, v1}, Lcom/mastercard/mpsdk/implementation/o;-><init>(Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;Lcom/mastercard/mpsdk/implementation/e;)V

    new-instance v2, Lcom/mastercard/mpsdk/implementation/s;

    iget-object v6, v0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iget-object v8, v0, Lcom/mastercard/mpsdk/implementation/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    iget-object v9, v0, Lcom/mastercard/mpsdk/implementation/w;->s:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v10, v0, Lcom/mastercard/mpsdk/implementation/w;->t:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-object v5, v2

    invoke-direct/range {v5 .. v10}, Lcom/mastercard/mpsdk/implementation/s;-><init>(Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)V

    sget-object v3, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;->INSTANCE:Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->initializeAsyncEngine(Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;)V

    new-instance v3, Lcom/mastercard/mpsdk/implementation/v;

    iget-object v11, v0, Lcom/mastercard/mpsdk/implementation/w;->b:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    iget-object v12, v0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iget-object v13, v0, Lcom/mastercard/mpsdk/implementation/w;->w:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    move-object v9, v3

    move-object v10, v1

    move-object v14, v2

    invoke-direct/range {v9 .. v14}, Lcom/mastercard/mpsdk/implementation/v;-><init>(Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mpsdk/interfaces/ApduProcessor;Lcom/mastercard/mpsdk/interfaces/SecurityServices;)V

    return-object v3

    :cond_9
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Security incident service for future use"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_c
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1
.end method

.method public final usingOptionalAdviceManager(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->g:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    return-object p0
.end method

.method public final usingOptionalCardLevelPin()Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/w;->l:Z

    return-object p0
.end method

.method public final usingOptionalCommunicationRetryParametersProvider(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->v:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    return-object p0
.end method

.method public final usingOptionalCredentialsAccessibilityPolicy(Lcom/mastercard/mpsdk/implementation/g;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->A:Lcom/mastercard/mpsdk/implementation/g;

    return-object p0
.end method

.method public final usingOptionalCredentialsReplenishmentPolicy(Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->m:Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

    return-object p0
.end method

.method public final usingOptionalCustomDatabase(Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->d:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    return-object p0
.end method

.method public final usingOptionalDefaultDataForMcbpV1ProfileMapping(Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->y:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    return-object p0
.end method

.method public final usingOptionalEmvOnlyMode()Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/w;->B:Z

    return-object p0
.end method

.method public final usingOptionalMcbpLogger(Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->setInstance(Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    return-object p0
.end method

.method public final usingOptionalMpaKeyToSupportUpgrade([B)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->x:[B

    return-object p0
.end method

.method public final usingOptionalPdolItems(Ljava/util/List;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)",
            "Lcom/mastercard/mpsdk/interfaces/McbpInitializer;"
        }
    .end annotation

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->r:Ljava/util/List;

    return-object p0
.end method

.method public final usingOptionalPinDataProviderForKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->t:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    return-object p0
.end method

.method public final usingOptionalPinDataProviderForTransactions(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->u:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    return-object p0
.end method

.method public final usingOptionalRemoteCommunicationEventListener(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/w;->k:Lcom/mastercard/mpsdk/implementation/x;

    if-eqz p1, :cond_0

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public final usingOptionalRemoteCommunicationManager(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->b:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    return-object p0
.end method

.method public final usingOptionalSecurityIncidentService(Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->z:Lcom/mastercard/mpsdk/componentinterface/SecurityIncidentService;

    return-object p0
.end method

.method public final usingOptionalTransactionApduListener(Lcom/mastercard/mpsdk/interfaces/ApduListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->h:Lcom/mastercard/mpsdk/interfaces/ApduListener;

    return-object p0
.end method

.method public final usingOptionalUdolItems(Ljava/util/List;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)",
            "Lcom/mastercard/mpsdk/interfaces/McbpInitializer;"
        }
    .end annotation

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->q:Ljava/util/List;

    return-object p0
.end method

.method public final withActiveCardProvider(Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->o:Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;

    return-object p0
.end method

.method public final withCardManagerEventListener(Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->i:Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    return-object p0
.end method

.method public final withCdCvmStatusProvider(Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->n:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    return-object p0
.end method

.method public final withCryptoEngine(Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    return-object p0
.end method

.method public final withHttpManager(Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->e:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    return-object p0
.end method

.method public final withKeyRolloverEventListener(Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->p:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    return-object p0
.end method

.method public final withTransactionEventListener(Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->j:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    return-object p0
.end method

.method public final withWalletConsentManager(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    return-object p0
.end method

.method public final withWalletIdentificationDataProvider(Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w;->s:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    return-object p0
.end method
