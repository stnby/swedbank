.class public Lcom/mastercard/mpsdk/implementation/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;


# instance fields
.field private a:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

.field private b:Lcom/mastercard/mpsdk/implementation/e;

.field private c:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;Lcom/mastercard/mpsdk/implementation/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/o;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/o;->c:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/o;->a:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/o;->b:Lcom/mastercard/mpsdk/implementation/e;

    return-void
.end method


# virtual methods
.method public onTransactionsResumed()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/o;->b:Lcom/mastercard/mpsdk/implementation/e;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/mastercard/mpsdk/implementation/e;->a:Z

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/o;->a:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    return-void
.end method

.method public onTransactionsSuspended()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/o;->b:Lcom/mastercard/mpsdk/implementation/e;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mastercard/mpsdk/implementation/e;->a:Z

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/o;->a:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsSuspended()V

    return-void
.end method
