.class public Lcom/mastercard/mpsdk/implementation/AdviceManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;


# instance fields
.field private logUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/AdviceManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/AdviceManager;->logUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method


# virtual methods
.method public getFinalAssessment(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;
    .locals 1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getAdvice()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object p2

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "recommendedAdvice= "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "reasons= "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object p2
.end method
