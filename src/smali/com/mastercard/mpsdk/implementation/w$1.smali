.class final Lcom/mastercard/mpsdk/implementation/w$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/implementation/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/implementation/w;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/w;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/w$1;->a:Lcom/mastercard/mpsdk/implementation/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    return-object v0
.end method

.method public final getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0
.end method

.method public final getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    return-object v0
.end method

.method public final isTransitSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isUsAipMaskSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
