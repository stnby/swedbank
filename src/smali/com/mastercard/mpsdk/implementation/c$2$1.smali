.class final Lcom/mastercard/mpsdk/implementation/c$2$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/c$2;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/implementation/c$2;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/c$2;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;
    .locals 2

    sget-object v0, Lcom/mastercard/mpsdk/implementation/c$4;->a:[I

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v1, v1, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getCryptogramDataType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;->UCAF:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    return-object v0

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;->DE55:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    return-object v0
.end method

.method public final getExpirationDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getExpirationDate()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPan()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v1, v1, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getPan()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method public final getPanSequenceNumber()I
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getPanSequenceNumber()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->longFromByteArray([B)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final getPar()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getPar()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTrack2Data()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v1, v1, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getTrack2EquivalentData()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method public final getTransactionCryptogramData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getTransactionCryptogramData()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTransactionId()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$2$1;->a:Lcom/mastercard/mpsdk/implementation/c$2;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getTransactionId()[B

    move-result-object v0

    return-object v0
.end method
