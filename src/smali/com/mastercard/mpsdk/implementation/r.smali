.class public Lcom/mastercard/mpsdk/implementation/r;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;


# instance fields
.field a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private final b:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

.field private final c:Lcom/mastercard/mpsdk/componentinterface/Card;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;Lcom/mastercard/mpsdk/componentinterface/Card;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/r;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/r;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/r;->b:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/r;->c:Lcom/mastercard/mpsdk/componentinterface/Card;

    return-void
.end method


# virtual methods
.method public getTimeOfLastAuthentication()J
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/r;->b:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;->getTimeOfLastSuccessfulCdCvm()J

    move-result-wide v0

    return-wide v0
.end method

.method public isAuthenticated()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/r;->b:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/r;->c:Lcom/mastercard/mpsdk/componentinterface/Card;

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;->isCdCvmSuccessful(Lcom/mastercard/mpsdk/componentinterface/Card;)Z

    move-result v0

    return v0
.end method

.method public isCdCvmBlocked()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/r;->b:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;->isCdCvmBlocked()Z

    move-result v0

    return v0
.end method

.method public isCdCvmEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/r;->b:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;->isCdCvmEnabled()Z

    move-result v0

    return v0
.end method
