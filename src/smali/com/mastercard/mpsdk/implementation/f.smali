.class public Lcom/mastercard/mpsdk/implementation/f;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/f;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mpsdk/implementation/f;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;ZZ)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "hostUmdConfig= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cdCvmModel= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Lcom/mastercard/mpsdk/implementation/f$1;

    move-object v2, v0

    move-object v3, p0

    move v4, p3

    move v5, p4

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v2 .. v7}, Lcom/mastercard/mpsdk/implementation/f$1;-><init>(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;ZZLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)V

    return-object v0
.end method

.method public static a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;Z)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    sget-object v1, Lcom/mastercard/mpsdk/implementation/f$2;->a:[I

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    :goto_0
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    sget-object v2, Lcom/mastercard/mpsdk/implementation/f$2;->b:[I

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    goto :goto_1

    :pswitch_4
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    goto :goto_1

    :pswitch_5
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    :goto_1
    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->isTransitSupported()Z

    move-result v2

    invoke-static {p0, v0, v1, v2, p1}, Lcom/mastercard/mpsdk/implementation/f;->a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;ZZ)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object p0

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;)Z
    .locals 2

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCdol1RelatedDataLength()I

    move-result v0

    const/16 v1, 0x2d

    if-lt v0, v1, :cond_0

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIssuerApplicationData()[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIssuerApplicationData()[B

    move-result-object v0

    array-length v0, v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static synthetic a([B)Z
    .locals 1

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "A0000000043060"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method
