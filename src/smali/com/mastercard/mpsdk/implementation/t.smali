.class Lcom/mastercard/mpsdk/implementation/t;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibrary;
.implements Lcom/mastercard/mpsdk/implementation/u;


# instance fields
.field private a:Landroid/app/Application;

.field private b:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/t;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/t;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/t;->a:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/t;->a:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getMcbpInitializer()Lcom/mastercard/mpsdk/interfaces/McbpInitializer;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/implementation/w;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/implementation/w;-><init>(Lcom/mastercard/mpsdk/implementation/u;)V

    return-object v0
.end method
