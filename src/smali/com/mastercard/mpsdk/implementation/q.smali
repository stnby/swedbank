.class public Lcom/mastercard/mpsdk/implementation/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;


# instance fields
.field a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

.field private c:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/q;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/q;->a:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/q;->c:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    iput-boolean p3, p0, Lcom/mastercard/mpsdk/implementation/q;->d:Z

    return-void
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    return-object p0
.end method

.method static synthetic b(Lcom/mastercard/mpsdk/implementation/q;)Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/implementation/q;->c:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    return-object p0
.end method


# virtual methods
.method public getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/implementation/q$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/implementation/q$1;-><init>(Lcom/mastercard/mpsdk/implementation/q;)V

    return-object v0
.end method

.method public getContactlessProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
    .locals 5

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/q;->c:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->isTransitSupported()Z

    move-result v3

    iget-boolean v4, p0, Lcom/mastercard/mpsdk/implementation/q;->d:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/mastercard/mpsdk/implementation/f;->a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;ZZ)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mastercard/mpsdk/implementation/q;->d:Z

    invoke-static {v0, v1}, Lcom/mastercard/mpsdk/implementation/f;->a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;Z)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object v0

    return-object v0
.end method

.method public getDsrpProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/q;->c:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/implementation/l;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/q;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/l;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpData;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    move-result-object v0

    return-object v0
.end method
