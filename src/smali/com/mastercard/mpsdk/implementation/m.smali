.class public Lcom/mastercard/mpsdk/implementation/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;


# instance fields
.field private a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

.field private b:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/m;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    return-void
.end method


# virtual methods
.method public getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;
    .locals 7

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getMobileKeySetId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    const-string v1, "DEK_KEY"

    invoke-interface {v0, v3, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "encryptedDekKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    const-string v1, "MAC_KEY"

    invoke-interface {v0, v3, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "encryptedMacKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    const-string v1, "TRANSPORT_KEY"

    invoke-interface {v0, v3, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "encryptedTransportKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Lcom/mastercard/mpsdk/implementation/m$1;

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/mastercard/mpsdk/implementation/m$1;-><init>(Lcom/mastercard/mpsdk/implementation/m;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemoteManagementServiceUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getRemoteManagementUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasCommunicationParameters()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/m;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    :catchall_0
    move-exception v0

    throw v0

    :catch_0
    return v0
.end method

.method public setCommunicationParameters(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "KeySetId= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EncryptedDek= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EncryptedMacKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EncryptedTransportKey= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DEK_KEY"

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MAC_KEY"

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TRANSPORT_KEY"

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object p1

    invoke-interface {v0, v1, v2, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/m;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveRemoteManagementUrl(Ljava/lang/String;)V

    return-void
.end method
