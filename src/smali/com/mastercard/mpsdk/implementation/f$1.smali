.class final Lcom/mastercard/mpsdk/implementation/f$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/f;->a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;ZZ)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

.field final synthetic e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;ZZLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    iput-boolean p2, p0, Lcom/mastercard/mpsdk/implementation/f$1;->b:Z

    iput-boolean p3, p0, Lcom/mastercard/mpsdk/implementation/f$1;->c:Z

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/f$1;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    iput-object p5, p0, Lcom/mastercard/mpsdk/implementation/f$1;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAid()[B

    move-result-object v0

    return-object v0
.end method

.method public final getAlternateContactlessPaymentData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/implementation/f$1$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/implementation/f$1$1;-><init>(Lcom/mastercard/mpsdk/implementation/f$1;)V

    return-object v0
.end method

.method public final getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0
.end method

.method public final getCdol1RelatedDataLength()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCdol1RelatedDataLength()I

    move-result v0

    int-to-byte v0, v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDeclineConditions()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCiacDecline()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDeclineConditionsOnPpms()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCiacDeclineOnPpms()[B

    move-result-object v0

    return-object v0
.end method

.method public final getGpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getGpoResponse()[B

    move-result-object v0

    return-object v0
.end method

.method public final getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIssuerApplicationData()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPaymentFci()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPinIvCvc3Track2()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPinIvCvc3Track2()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPpseFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPpseFci()[B

    move-result-object v0

    return-object v0
.end method

.method public final getRecords()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getRecords()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/Records;

    new-instance v3, Lcom/mastercard/mpsdk/implementation/f$1$2;

    invoke-direct {v3, p0, v2}, Lcom/mastercard/mpsdk/implementation/f$1$2;-><init>(Lcom/mastercard/mpsdk/implementation/f$1;Lcom/mastercard/mpsdk/componentinterface/Records;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public final getTrack1ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTrack2ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final isAlternateAidMchipDataValid()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/f;->a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isMagstripeDataValid()Z
    .locals 2

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPinIvCvc3Track2()[B

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getPinIvCvc3Track2()[B

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isPrimaryAidMchipDataValid()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/f;->a(Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;)Z

    move-result v0

    return v0
.end method

.method public final isTransitSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->b:Z

    return v0
.end method

.method public final isUsAipMaskSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAid()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/f;->a([B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/f$1;->a:Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->isUsAipMaskingSupported()Z

    move-result v0

    return v0
.end method
