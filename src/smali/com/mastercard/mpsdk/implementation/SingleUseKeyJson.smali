.class public Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;
.super Ljava/lang/Object;


# instance fields
.field private mAtc:I
    .annotation runtime Lflexjson/h;
        a = "atc"
    .end annotation
.end field

.field private mHash:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "hash"
    .end annotation
.end field

.field private mId:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "id"
    .end annotation
.end field

.field private mIdn:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "idn"
    .end annotation
.end field

.field private mSessionKeyContactlessMd:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "sessionKeyContactlessMd"
    .end annotation
.end field

.field private mSessionKeyContactlessUmd:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "sessionKeyContactlessUmd"
    .end annotation
.end field

.field private mSessionKeyRemotePaymentMd:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "sessionKeyRemotePaymentMd"
    .end annotation
.end field

.field private mSessionKeyRemotePaymentUmd:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "sessionKeyRemotePaymentUmd"
    .end annotation
.end field

.field private mSukContactlessUmd:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "sukContactlessUmd"
    .end annotation
.end field

.field private mSukInfo:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "sukInfo"
    .end annotation
.end field

.field private mSukRemotePaymentUmd:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "sukRemotePaymentUmd"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mId:Ljava/lang/String;

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukInfo:Ljava/lang/String;

    if-eqz p5, :cond_0

    invoke-static {p5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyContactlessMd:Ljava/lang/String;

    :cond_0
    if-eqz p6, :cond_1

    invoke-static {p6}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyRemotePaymentMd:Ljava/lang/String;

    :cond_1
    if-eqz p3, :cond_2

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukContactlessUmd:Ljava/lang/String;

    :cond_2
    if-eqz p4, :cond_3

    invoke-static {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukRemotePaymentUmd:Ljava/lang/String;

    :cond_3
    if-eqz p7, :cond_4

    invoke-static {p7}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyContactlessUmd:Ljava/lang/String;

    :cond_4
    if-eqz p8, :cond_5

    invoke-static {p8}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyRemotePaymentUmd:Ljava/lang/String;

    :cond_5
    invoke-static {p9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mIdn:Ljava/lang/String;

    iput p10, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mAtc:I

    invoke-static {p11}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mHash:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAtc()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mAtc:I

    return v0
.end method

.method public getHash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mHash:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getIdn()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mIdn:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionKeyContactlessMd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyContactlessMd:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionKeyContactlessUmd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyContactlessUmd:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionKeyRemotePaymentMd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyRemotePaymentMd:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionKeyRemotePaymentUmd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyRemotePaymentUmd:Ljava/lang/String;

    return-object v0
.end method

.method public getSukContactlessUmd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukContactlessUmd:Ljava/lang/String;

    return-object v0
.end method

.method public getSukInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getSukRemotePaymentUmd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukRemotePaymentUmd:Ljava/lang/String;

    return-object v0
.end method

.method public setAtc(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mAtc:I

    return-void
.end method

.method public setHash(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mHash:Ljava/lang/String;

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mId:Ljava/lang/String;

    return-void
.end method

.method public setIdn(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mIdn:Ljava/lang/String;

    return-void
.end method

.method public setSessionKeyContactlessMd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyContactlessMd:Ljava/lang/String;

    return-void
.end method

.method public setSessionKeyContactlessUmd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyContactlessUmd:Ljava/lang/String;

    return-void
.end method

.method public setSessionKeyRemotePaymentMd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyRemotePaymentMd:Ljava/lang/String;

    return-void
.end method

.method public setSessionKeyRemotePaymentUmd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSessionKeyRemotePaymentUmd:Ljava/lang/String;

    return-void
.end method

.method public setSukContactlessUmd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukContactlessUmd:Ljava/lang/String;

    return-void
.end method

.method public setSukInfo(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukInfo:Ljava/lang/String;

    return-void
.end method

.method public setSukRemotePaymentUmd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/SingleUseKeyJson;->mSukRemotePaymentUmd:Ljava/lang/String;

    return-void
.end method
