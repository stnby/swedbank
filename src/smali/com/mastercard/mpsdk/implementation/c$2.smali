.class final Lcom/mastercard/mpsdk/implementation/c$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/implementation/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

.field final synthetic b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

.field final synthetic c:Lcom/mastercard/mpsdk/implementation/c;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/c;Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/c$2;->c:Lcom/mastercard/mpsdk/implementation/c;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/c$2;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/c$2;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/implementation/c$2$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/implementation/c$2$1;-><init>(Lcom/mastercard/mpsdk/implementation/c$2;)V

    return-object v0
.end method

.method public final getResult()Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$2;->a:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    return-object v0
.end method
