.class final Lcom/mastercard/mpsdk/implementation/e$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/database/TransactionCredentialInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/implementation/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/mastercard/mpsdk/implementation/e;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/e;Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/e$3;->e:Lcom/mastercard/mpsdk/implementation/e;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/e$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/e$3;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/e$3;->c:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    iput-object p5, p0, Lcom/mastercard/mpsdk/implementation/e$3;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAtc()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$3;->c:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v0

    return v0
.end method

.method public final getCardId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$3;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getSerializedTransactionCredential()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$3;->c:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/y;->a(Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getTimeStamp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$3;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final getTransactionCredentialId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$3;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getTransactionCredentialStatus()I
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v0

    return v0
.end method
