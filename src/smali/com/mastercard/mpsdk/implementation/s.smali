.class public Lcom/mastercard/mpsdk/implementation/s;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/SecurityServices;


# instance fields
.field private a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

.field private b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field private d:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

.field private e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

.field private f:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/s;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->f:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/s;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iput-object p5, p0, Lcom/mastercard/mpsdk/implementation/s;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    return-void
.end method


# virtual methods
.method public getDatabaseCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getDatabaseCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteManagementCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getRemoteManagementCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteManagementKeysForMobileKeySetId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;
    .locals 8

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    const-string v1, "DEK_KEY"

    invoke-interface {v0, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v7

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    const-string v1, "TRANSPORT_KEY"

    invoke-interface {v0, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v6

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    const-string v1, "MAC_KEY"

    invoke-interface {v0, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getEncryptedMobileKey(Ljava/lang/String;Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v5

    new-instance v0, Lcom/mastercard/mpsdk/implementation/s$2;

    move-object v2, v0

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/mastercard/mpsdk/implementation/s$2;-><init>(Lcom/mastercard/mpsdk/implementation/s;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    return-object v0
.end method

.method public getTransactionCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getTransactionCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    move-result-object v0

    return-object v0
.end method

.method public getWalletCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getWalletDataCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    move-result-object v0

    return-object v0
.end method

.method public rolloverDatabaseStorageKeys()V
    .locals 9

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsSuspended()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getDatabaseStorageKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getDatabaseStorageMacKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    move-result-object v8

    :try_start_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->getCurrentKeyId()[B

    move-result-object v4

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->startRollover()[B

    move-result-object v5

    invoke-interface {v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->getCurrentKeyId()[B

    move-result-object v6

    invoke-interface {v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->startRollover()[B

    move-result-object v7

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-object v2, v0

    move-object v3, v8

    invoke-interface/range {v1 .. v7}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->rolloverData(Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;Lcom/mastercard/mpsdk/componentinterface/crypto/KeyDataRollover;[B[B[B[B)V

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverComplete()V

    invoke-interface {v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverComplete()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DST_DEK and DST_MAC Rollover failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->abandonRollover()V

    invoke-interface {v8}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->abandonRollover()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    throw v0
.end method

.method public rolloverLocalDataEncryptionKey()V
    .locals 24

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsSuspended()V

    iget-object v0, v1, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getLocalDataEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->getCurrentKeyId()[B

    move-result-object v0

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->startRollover()[B

    move-result-object v3

    iget-object v4, v1, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v4}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getAllCardIds()Ljava/util/List;

    move-result-object v4

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget-object v8, v1, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v8, v7}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    iget-object v11, v1, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v11, v7, v10}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v11

    invoke-static {v11}, Lcom/mastercard/mpsdk/implementation/y;->a([B)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v11

    new-instance v23, Lcom/mastercard/mpsdk/implementation/y;

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getInfo()[B

    move-result-object v13

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukContactlessUmd()[B

    move-result-object v12

    invoke-interface {v2, v0, v3, v12}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v14

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukRemotePaymentUmd()[B

    move-result-object v12

    invoke-interface {v2, v0, v3, v12}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v15

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object v12

    invoke-interface {v2, v0, v3, v12}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v16

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentMd()[B

    move-result-object v12

    invoke-interface {v2, v0, v3, v12}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v17

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object v12

    invoke-interface {v2, v0, v3, v12}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v18

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentUmd()[B

    move-result-object v12

    invoke-interface {v2, v0, v3, v12}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v19

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getIdn()[B

    move-result-object v12

    invoke-interface {v2, v0, v3, v12}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v20

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v21

    invoke-interface {v11}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getHash()[B

    move-result-object v22

    move-object/from16 v11, v23

    move-object v12, v10

    invoke-direct/range {v11 .. v22}, Lcom/mastercard/mpsdk/implementation/y;-><init>(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)V

    invoke-static/range {v23 .. v23}, Lcom/mastercard/mpsdk/implementation/y;->a(Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;)[B

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    invoke-virtual {v5, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v8, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-direct {v8}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;-><init>()V

    iget-object v9, v1, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v9, v7}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getCardProfileByCardId(Ljava/lang/String;)[B

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->getCard([B)Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-result-object v8

    invoke-interface {v8}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v9

    invoke-interface {v9}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v9

    invoke-interface {v2, v0, v3, v9}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v9

    new-instance v10, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-direct {v10, v9}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    new-instance v9, Lcom/mastercard/mpsdk/implementation/s$1;

    invoke-direct {v9, v1, v8, v10}, Lcom/mastercard/mpsdk/implementation/s$1;-><init>(Lcom/mastercard/mpsdk/implementation/s;Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)V

    new-instance v8, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-direct {v8}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;-><init>()V

    invoke-virtual {v8, v9}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->getContent(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;)[B

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_1
    iget-object v0, v1, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v0, v5, v6}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->rolloverTransactionCredentialsAndCardProfiles(Ljava/util/HashMap;Ljava/util/HashMap;)V

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverComplete()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    iget-object v0, v1, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_0
    move-exception v0

    :try_start_1
    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->abandonRollover()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "L_DEK Rollover failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :goto_3
    iget-object v2, v1, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    throw v0
.end method

.method public rolloverRemoteManagementKeyEncryptionKey()V
    .locals 8

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsSuspended()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getRemoteManagementKeyEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->getCurrentKeyId()[B

    move-result-object v1

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->startRollover()[B

    move-result-object v2

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getMobileKeySetId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, v3}, Lcom/mastercard/mpsdk/implementation/s;->getRemoteManagementKeysForMobileKeySetId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v4

    new-instance v5, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v6

    invoke-interface {v0, v1, v2, v6}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;-><init>([B)V

    new-instance v6, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v7

    invoke-interface {v0, v1, v2, v7}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;-><init>([B)V

    new-instance v7, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    invoke-interface {v0, v1, v2, v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v1

    invoke-direct {v7, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;-><init>([B)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "DEK_KEY"

    invoke-virtual {v1, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "TRANSPORT_KEY"

    invoke-virtual {v1, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "MAC_KEY"

    invoke-virtual {v1, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/s;->a:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v2, v3, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->rolloverMobileKeys(Ljava/lang/String;Ljava/util/HashMap;)V

    :cond_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverComplete()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->abandonRollover()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "RM_KEK Rollover failed: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    throw v0
.end method

.method public rolloverWalletDataEncryptionKey()V
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsSuspended()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->getWalletDataEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->getCurrentKeyId()[B

    move-result-object v1

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->startRollover()[B

    move-result-object v2

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/s;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;->getEncryptedDeviceFingerPrint()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/implementation/s;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    new-instance v5, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    invoke-direct {v5, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;-><init>([B)V

    invoke-interface {v4, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;->onKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/s;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/s;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_0

    new-instance v4, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverData([B[B[B)[B

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;-><init>([B)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/s;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    invoke-interface {v1, v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->onKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V

    :cond_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->rolloverComplete()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;->abandonRollover()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "W_DEK Rollover failed: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/s;->e:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;->onTransactionsResumed()V

    throw v0
.end method

.method public wipeCryptoParameters()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;->wipeCryptoParameters()Z

    move-result v0

    return v0
.end method
