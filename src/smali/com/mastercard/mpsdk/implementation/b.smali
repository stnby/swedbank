.class public Lcom/mastercard/mpsdk/implementation/b;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

.field b:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

.field c:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

.field d:Lcom/mastercard/mpsdk/componentinterface/CardManager;

.field e:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

.field f:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

.field g:Ljava/lang/String;

.field h:Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;

.field i:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

.field j:Z

.field k:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field n:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

.field o:Lcom/mastercard/mpsdk/implementation/g;

.field p:Z

.field q:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

.field private r:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/b;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/b;->l:Ljava/util/List;

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/b;->m:Ljava/util/List;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/b;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/b;->r:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method
