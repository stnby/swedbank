.class public final Lcom/mastercard/mpsdk/implementation/x;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/s;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->c:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final onCardPinReset(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onCardPinReset(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onCardPinReset(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 8

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onChangeCardPinSucceeded(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinSucceeded(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinSucceeded(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onChangeWalletMobilePinStarted(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletMobilePinStarted(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletMobilePinStarted(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onChangeWalletPinSucceeded()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinSucceeded()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinSucceeded()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 8

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 8

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onRequestSessionSuccess()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionSuccess()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionSuccess()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 8

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onSetCardPinSucceeded(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinSucceeded(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinSucceeded(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onSetWalletPinSucceeded()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinSucceeded()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinSucceeded()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onSystemHealthCompleted()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthCompleted()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthCompleted()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onTaskStatusReceived(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceived(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceived(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onWalletPinReset()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onWalletPinReset()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onWalletPinReset()V

    goto :goto_1

    :cond_1
    return-void
.end method
