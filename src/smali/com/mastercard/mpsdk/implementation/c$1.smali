.class final Lcom/mastercard/mpsdk/implementation/c$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/c;->replenishCredentials()Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/mastercard/mpsdk/implementation/c;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/c;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/c$1;->d:Lcom/mastercard/mpsdk/implementation/c;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/c$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/c$1;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/c$1;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAtc()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$1;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v0

    return v0
.end method

.method public final getStatus()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$1;->d:Lcom/mastercard/mpsdk/implementation/c;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/implementation/c;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c$1;->d:Lcom/mastercard/mpsdk/implementation/c;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/c$1;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialStatusForCardId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->fromValue(I)Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTimestamp()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c$1;->getStatus()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$1;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$1;->c:Ljava/lang/String;

    return-object v0

    :cond_1
    :goto_0
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
