.class final Lcom/mastercard/mpsdk/implementation/p$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mpsdk/implementation/p;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/implementation/p;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/p;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/p$1;->a:Lcom/mastercard/mpsdk/implementation/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAtc()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p$1;->a:Lcom/mastercard/mpsdk/implementation/p;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/p;->a(Lcom/mastercard/mpsdk/implementation/p;)Lcom/mastercard/mpsdk/implementation/z;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "atc= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/z;->a:[B

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/z;->a:[B

    return-object v0
.end method

.method public final getProtectedIdn()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p$1;->a:Lcom/mastercard/mpsdk/implementation/p;

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/p;->a(Lcom/mastercard/mpsdk/implementation/p;)Lcom/mastercard/mpsdk/implementation/z;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EncryptedIdn= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/mastercard/mpsdk/implementation/z;->b:[B

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/mastercard/mpsdk/implementation/z;->b:[B

    return-object v0
.end method
