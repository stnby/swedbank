.class Lcom/mastercard/mpsdk/implementation/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/implementation/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/implementation/c$a;
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;

.field private static k:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

.field private final c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

.field private final d:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

.field private final e:Lcom/mastercard/mpsdk/componentinterface/CardManager;

.field private final f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

.field private final g:Z

.field private final h:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

.field private i:Lcom/mastercard/mchipengine/MChipEngine;

.field private l:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/mastercard/mpsdk/implementation/c;->j:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;ZLcom/mastercard/mpsdk/interfaces/TransactionEventListener;Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;Ljava/util/List;Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;Lcom/mastercard/mpsdk/implementation/g;Z)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/mastercard/mpsdk/componentinterface/CardManager;",
            "Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;",
            "Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;",
            "Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;",
            "Z",
            "Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;",
            "Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;",
            "Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;",
            "Lcom/mastercard/mpsdk/implementation/g;",
            "Z)V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/mastercard/mpsdk/implementation/c;->i:Lcom/mastercard/mchipengine/MChipEngine;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SDK | "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v5, Lcom/mastercard/mpsdk/implementation/c;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v4

    iput-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->l:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-object v4, p1

    iput-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    move-object/from16 v4, p3

    iput-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    move-object/from16 v4, p2

    iput-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->e:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-object/from16 v4, p6

    iput-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    move/from16 v4, p7

    iput-boolean v4, v0, Lcom/mastercard/mpsdk/implementation/c;->g:Z

    move-object/from16 v4, p8

    iput-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->d:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    move-object/from16 v4, p15

    iput-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->h:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    new-instance v7, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;

    move-object/from16 v4, p14

    move-object/from16 v5, p16

    invoke-direct {v7, p0, v2, v4, v5}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;-><init>(Lcom/mastercard/mpsdk/implementation/d;Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/implementation/g;)V

    new-instance v5, Lcom/mastercard/mpsdk/implementation/q;

    iget-object v4, v0, Lcom/mastercard/mpsdk/implementation/c;->h:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    move/from16 v6, p17

    invoke-direct {v5, v1, v4, v6}, Lcom/mastercard/mpsdk/implementation/q;-><init>(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;Z)V

    invoke-interface/range {p4 .. p4}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface/range {p4 .. p4}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    new-instance v6, Lcom/mastercard/mpsdk/implementation/p;

    invoke-direct {v6, v2, v7, v1}, Lcom/mastercard/mpsdk/implementation/p;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)V

    new-instance v10, Lcom/mastercard/mpsdk/implementation/n;

    iget-object v1, v0, Lcom/mastercard/mpsdk/implementation/c;->d:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    invoke-direct {v10, p0, v1}, Lcom/mastercard/mpsdk/implementation/n;-><init>(Lcom/mastercard/mpsdk/implementation/d;Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;)V

    :try_start_0
    new-instance v1, Lcom/mastercard/mchipengine/MChipEngine;

    new-instance v9, Lcom/mastercard/mpsdk/implementation/r;

    move-object/from16 v2, p9

    invoke-direct {v9, v2, p0}, Lcom/mastercard/mpsdk/implementation/r;-><init>(Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;Lcom/mastercard/mpsdk/componentinterface/Card;)V

    move-object/from16 v2, p12

    invoke-direct {p0, v2}, Lcom/mastercard/mpsdk/implementation/c;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    move-object/from16 v2, p13

    invoke-direct {p0, v2}, Lcom/mastercard/mpsdk/implementation/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    move-object v4, v1

    move-object/from16 v8, p11

    move-object/from16 v11, p10

    invoke-direct/range {v4 .. v13}, Lcom/mastercard/mchipengine/MChipEngine;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;Ljava/util/List;Ljava/util/List;)V

    iput-object v1, v0, Lcom/mastercard/mpsdk/implementation/c;->i:Lcom/mastercard/mchipengine/MChipEngine;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/walletinterface/walletexceptions/InvalidProfileException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    iput-object v3, v0, Lcom/mastercard/mpsdk/implementation/c;->i:Lcom/mastercard/mchipengine/MChipEngine;

    return-void
.end method

.method private a(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;
    .locals 2

    new-instance v0, Lcom/mastercard/mpsdk/implementation/c$3;

    invoke-direct {v0, p0, p1}, Lcom/mastercard/mpsdk/implementation/c$3;-><init>(Lcom/mastercard/mpsdk/implementation/c;Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;)V

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "result= "

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getResult()Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;
    .locals 0

    sput-object p0, Lcom/mastercard/mpsdk/implementation/c;->k:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    return-object p0
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/implementation/c;)Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    return-object p0
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string v2, "9F33"

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    return-object v0
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string v2, "9F1D"

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v2

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string v2, "9F7E"

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    return-object v0
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->i:Lcom/mastercard/mchipengine/MChipEngine;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a()Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;
    .locals 4

    sget-object v0, Lcom/mastercard/mpsdk/implementation/c;->j:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/mastercard/mpsdk/implementation/c;->k:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/mastercard/mpsdk/implementation/c;->k:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    monitor-exit v0

    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getFirstTransactionCredentialIdForCardIdWithStatus(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    monitor-exit v0

    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/implementation/y;->a([B)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "atc= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final a(ILcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "status= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/implementation/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result p2

    invoke-interface {v2, v3, p1, p2, v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->updateTransactionCredentialStatusForCardId(Ljava/lang/String;IILjava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)V
    .locals 9

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getTransactionId()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v5, v0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :goto_1
    const-string v6, "2.0"

    invoke-static {p1}, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->serialize(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)[B

    move-result-object v8

    invoke-interface/range {v3 .. v8}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveTransactionLogForCardId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    return-void

    :cond_1
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput;

    const-string v0, "Card Id does not match Card Id of TransactionLog"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a([B)[B
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardState()Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->ACTIVATED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->isContactlessSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "C-APDU to MchipEngine= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->i:Lcom/mastercard/mchipengine/MChipEngine;

    invoke-virtual {v0, p1}, Lcom/mastercard/mchipengine/MChipEngine;->processApdu([B)[B

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "R-APDU from MchipEngine= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p1

    :cond_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentNotSupportedException;

    const-string v0, "Card not active"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentNotSupportedException;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->d:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-interface {v0, p0, v1, p1}, Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;->onContactlessPaymentAborted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentNotSupportedException;

    const-string v0, "Card not active"

    invoke-direct {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentNotSupportedException;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->d:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-interface {v0, p0, v1, p1}, Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;->onContactlessPaymentAborted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V

    throw p1
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->e:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCredentialsReplenishmentPolicy()Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;->shouldRequestNewCredentials(Lcom/mastercard/mpsdk/componentinterface/Card;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->replenishCredentials()Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/implementation/c$a;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/implementation/c$a;-><init>(Lcom/mastercard/mpsdk/implementation/c;)V

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/implementation/c$a;->start()V

    return-void
.end method

.method public changePin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/c;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;->requestChangePin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot change card pin whilst in Wallet Level Pin Mode"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final d()V
    .locals 2

    sget-object v0, Lcom/mastercard/mpsdk/implementation/c;->j:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/mastercard/mpsdk/implementation/c;->k:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/mastercard/mpsdk/implementation/c;->k:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->wipe()V

    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/mastercard/mpsdk/implementation/c;->k:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public deleteTransactionCredentials()V
    .locals 6

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v1, v0}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v3, v0, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialStatusForCardId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->fromValue(I)Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v4, v0, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    sget-object v4, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    if-ne v3, v4, :cond_0

    invoke-static {v2}, Lcom/mastercard/mpsdk/implementation/y;->a([B)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v2

    sget-object v5, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_DISCARDED:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v5

    invoke-interface {v4, v0, v2, v5, v3}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->updateTransactionCredentialStatusForCardId(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/implementation/c$4;->b:[I

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->h:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->DEBIT:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->CREDIT:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->toString()Ljava/lang/String;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getCardId()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CardId= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getCardState()Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getCardStateByCardId(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->fromValue(I)Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cardState= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cardholderValidator= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getCdCvmModel(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;)Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object p1

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->isContactlessSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->toString()Ljava/lang/String;

    return-object p1

    :cond_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->DSRP:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->isDsrpSupported()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object p1

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object p1
.end method

.method public getCvmResetTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getCvmResetTimeout()I

    move-result v0

    return v0
.end method

.method public getDigitizedCardId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDigitizedCardId()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayablePanDigits()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getDigitizedCardId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->retrieveDevicePanFromDigitizedCardId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDualTapResetTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getDualTapResetTimeout()I

    move-result v0

    return v0
.end method

.method public getHostUmdConfig(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;)Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object p1

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->isContactlessSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->toString()Ljava/lang/String;

    return-object p1

    :cond_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->DSRP:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->isDsrpSupported()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpData;->getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    move-result-object p1

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object p1
.end method

.method public getNumberOfAvailableCredentials()I
    .locals 5

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialStatusForCardId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->fromValue(I)Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    move-result-object v2

    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    if-ne v2, v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/implementation/c$4;->c:[I

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->h:Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;->getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->PREPAID:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->DEBIT:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->CREDIT:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->COMMERCIAL:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/WalletData;->getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->toString()Ljava/lang/String;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSupportedAids()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SupportedAids= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAid()[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;->getAid()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SupportedAids= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getTransactionLog()Ljava/util/Iterator;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionLogIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v3, v4, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionLogByIdForCardId(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;->fromValue([B)Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "version= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getWalletDataForCard()Lcom/mastercard/mpsdk/componentinterface/database/WalletData;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getWalletDataForCardId(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/database/WalletData;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WalletData version= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/WalletData;->getVersion()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, "No wallet data"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public isContactlessSupported()Z
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isDsrpSupported()Z
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isOnDeviceCvmSupported()Z
    .locals 5

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->isContactlessSupported()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->b:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;->getGpoResponse()[B

    move-result-object v0

    const/4 v2, 0x2

    new-array v3, v2, [B

    const/4 v4, 0x4

    invoke-static {v0, v4, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-byte v0, v3, v1

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method public processDsrpTransaction(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CountryCode= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getCountryCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CurrencyCode= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getCurrencyCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TransactionAmount= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionAmount()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OptionalUnpredictableNumber= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getOptionalUnpredictableNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TransactionDate= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TransactionType= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [B

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionType()B

    move-result v2

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CryptogramType= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardState()Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->ACTIVATED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    if-eq v0, v1, :cond_0

    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_WRONG_STATE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    :goto_0
    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->isDsrpSupported()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;

    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;-><init>(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;)V

    :try_start_0
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/c;->i:Lcom/mastercard/mchipengine/MChipEngine;

    invoke-virtual {p1, v0}, Lcom/mastercard/mchipengine/MChipEngine;->processDsrp(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    move-result-object p1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->b()V

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->OK:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Track2EquivalentData= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getTrack2EquivalentData()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TransactionCryptogramData= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getTransactionCryptogramData()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ExpirationDate= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getExpirationDate()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pan= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getPan()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PanSequenceNumber= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getPanSequenceNumber()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Par= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getPar()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TransactionId= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getTransactionId()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CryptogramDataType= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getCryptogramDataType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DsrpTransactionOutcome.Result= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v2, Lcom/mastercard/mpsdk/implementation/c$2;

    invoke-direct {v2, p0, v1, p1}, Lcom/mastercard/mpsdk/implementation/c$2;-><init>(Lcom/mastercard/mpsdk/implementation/c;Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;)V

    new-instance v1, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;->getTransactionId()[B

    move-result-object p1

    invoke-direct {v1, v3, v0, p1}, Lcom/mastercard/mpsdk/implementation/TransactionLogImpl;-><init>(Ljava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;[B)V

    invoke-virtual {p0, v1}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)V

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "Result= "

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getResult()Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/mastercard/mchipengine/walletinterface/walletexceptions/TransactionDeclinedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mchipengine/walletinterface/walletexceptions/InvalidProfileException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v2

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    :try_start_1
    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->INTERNAL_ERROR:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;

    move-result-object p1

    return-object p1

    :catch_1
    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_INCOMPATIBLE_PROFILE:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;

    move-result-object p1

    return-object p1

    :catch_2
    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->DECLINED:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;

    move-result-object p1

    return-object p1

    :catch_3
    sget-object p1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ERROR_INVALID_INPUT:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object p1

    :goto_1
    throw p1
.end method

.method public replenishCredentials()Ljava/lang/String;
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialIdsForCardId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialForCardId(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->getTransactionCredentialTimeStampForCardId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Lcom/mastercard/mpsdk/implementation/y;->a([B)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v3

    new-instance v5, Lcom/mastercard/mpsdk/implementation/c$1;

    invoke-direct {v5, p0, v2, v3, v4}, Lcom/mastercard/mpsdk/implementation/c$1;-><init>(Lcom/mastercard/mpsdk/implementation/c;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/c;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    invoke-interface {v2, v3, v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;->requestReplenish(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public saveWalletDataForCard(Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WalletData version= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/WalletData;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->c:Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/database/McbpDataBase;->saveWalletDataForCardId(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/database/WalletData;)V

    return-void
.end method

.method public setPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/mastercard/mpsdk/implementation/c;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;->requestSetPin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot set card pin whilst in Wallet Level Pin Mode"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public stopContactlessTransaction()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mpsdk/implementation/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->i:Lcom/mastercard/mchipengine/MChipEngine;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/MChipEngine;->stopContactlessTransaction()V

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c;->d:Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;->onTransactionStopped()V

    return-void
.end method
