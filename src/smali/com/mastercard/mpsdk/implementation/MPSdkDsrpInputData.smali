.class public Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;


# instance fields
.field private final mCryptogramType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

.field private mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

.field private final mTransactionDate:Lcom/mastercard/mchipengine/utils/MChipDate;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionDate()Ljava/util/Date;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getYear()I

    move-result v2

    add-int/lit16 v2, v2, 0x76c

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getDate()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lcom/mastercard/mchipengine/utils/MChipDate;-><init>(III)V

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mTransactionDate:Lcom/mastercard/mchipengine/utils/MChipDate;

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mTransactionDate:Lcom/mastercard/mchipengine/utils/MChipDate;

    :goto_0
    sget-object v0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData$1;->a:[I

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    iput-object v1, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mCryptogramType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-void

    :pswitch_0
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    :goto_1
    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mCryptogramType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-void

    :pswitch_1
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getAmount()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionAmount()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Amount= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getCountryCode()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getCountryCode()I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "countryCode= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CryptogramType= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mCryptogramType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mCryptogramType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-object v0
.end method

.method public getCurrencyCode()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getCurrencyCode()I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CurrencyCode= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getTransactionDate()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mTransactionDate:Lcom/mastercard/mchipengine/utils/MChipDate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mTransactionDate:Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipDate;->toHexString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "transactionDate= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTransactionType()B
    .locals 5

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getTransactionType()B

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Byte;->valueOf(Ljava/lang/String;I)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "transactionType= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [B

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    const/4 v4, 0x0

    aput-byte v3, v2, v4

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    return v0
.end method

.method public getUnpredictableNumber()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkDsrpInputData;->mRemoteTransactionContext:Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;->getOptionalUnpredictableNumber()I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnpredictableNumber= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method
