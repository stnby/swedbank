.class final Lcom/mastercard/mpsdk/implementation/s$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/implementation/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

.field final synthetic b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

.field final synthetic c:Lcom/mastercard/mpsdk/implementation/s;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/s;Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/s$1;->c:Lcom/mastercard/mpsdk/implementation/s;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/s$1;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCardCountryCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getCardCountryCode()[B

    move-result-object v0

    return-object v0
.end method

.method public final getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/implementation/s$1$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/implementation/s$1$1;-><init>(Lcom/mastercard/mpsdk/implementation/s$1;)V

    return-object v0
.end method

.method public final getDigitizedCardId()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDigitizedCardId()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;

    move-result-object v0

    return-object v0
.end method

.method public final getPan()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getPan()[B

    move-result-object v0

    return-object v0
.end method

.method public final getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    return-object v0
.end method

.method public final getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;

    move-result-object v0

    return-object v0
.end method

.method public final isTransactionIdRequired()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/s$1;->a:Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->isTransactionIdRequired()Z

    move-result v0

    return v0
.end method
