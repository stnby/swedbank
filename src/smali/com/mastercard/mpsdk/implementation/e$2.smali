.class final Lcom/mastercard/mpsdk/implementation/e$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/implementation/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/mastercard/mpsdk/implementation/e;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/e;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/e$2;->d:Lcom/mastercard/mpsdk/implementation/e;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/e$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    iput-object p4, p0, Lcom/mastercard/mpsdk/implementation/e$2;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAtc()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v0

    return v0
.end method

.method public final getHash()[B
    .locals 1

    const-string v0, "0000"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getIdn()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getIdn()[B

    move-result-object v0

    return-object v0
.end method

.method public final getInfo()[B
    .locals 1

    const-string v0, "56"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getSessionKeyContactlessMd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getSessionKeyContactlessUmd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getSessionKeyRemotePaymentMd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentMd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getSessionKeyRemotePaymentUmd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentUmd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getSukContactlessUmd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukContactlessUmd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getSukRemotePaymentUmd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukRemotePaymentUmd()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTimestamp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final setStatus(I)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/e$2;->b:Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->setStatus(I)V

    return-void
.end method

.method public final wipe()V
    .locals 0

    return-void
.end method
