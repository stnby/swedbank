.class Lcom/mastercard/mpsdk/implementation/c$a;
.super Landroid/os/HandlerThread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/implementation/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/implementation/c;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/c;)V
    .locals 1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/c$a;->a:Lcom/mastercard/mpsdk/implementation/c;

    const-class p1, Lcom/mastercard/mpsdk/implementation/c$a;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onLooperPrepared()V
    .locals 1

    invoke-static {}, Ljava/lang/Thread;->yield()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/c$a;->a:Lcom/mastercard/mpsdk/implementation/c;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/implementation/c;->a()Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/c;->a(Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;)Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/c$a;->quitSafely()Z

    return-void
.end method
