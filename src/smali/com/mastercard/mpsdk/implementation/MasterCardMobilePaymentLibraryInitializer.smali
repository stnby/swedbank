.class public final enum Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

.field public static final enum INSTANCE:Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;


# instance fields
.field private mAndroidApplicationContext:Landroid/app/Application;

.field private mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->INSTANCE:Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    sget-object v1, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->INSTANCE:Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->$VALUES:[Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "SDK | "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class p2, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->$VALUES:[Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    return-object v0
.end method


# virtual methods
.method public final forApplication(Landroid/app/Application;)Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->mAndroidApplicationContext:Landroid/app/Application;

    sget-object p1, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->INSTANCE:Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    return-object p1
.end method

.method public final initialize()Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibrary;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->mAndroidApplicationContext:Landroid/app/Application;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mastercard/mpsdk/implementation/t;

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->mAndroidApplicationContext:Landroid/app/Application;

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/implementation/t;-><init>(Landroid/app/Application;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing application context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
