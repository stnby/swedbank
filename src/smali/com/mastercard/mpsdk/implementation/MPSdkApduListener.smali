.class public Lcom/mastercard/mpsdk/implementation/MPSdkApduListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/ApduListener;


# instance fields
.field private mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/MPSdkApduListener;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/MPSdkApduListener;->mLogUtils:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method


# virtual methods
.method public onReadRecord(BB[B)[B
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sfi= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v2, v1, [B

    const/4 v3, 0x0

    aput-byte p1, v2, v3

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "recordNumber= "

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-array v0, v1, [B

    aput-byte p2, v0, v3

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "proposedResponse= "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p3
.end method

.method public onSelectApdu([B[B)[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "aid= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "proposedResponse= "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p1
.end method

.method public onTransactionStarted()V
    .locals 0

    return-void
.end method
