.class public Lcom/mastercard/mpsdk/implementation/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;


# static fields
.field private static f:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# instance fields
.field private a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

.field private b:Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

.field private d:Lcom/mastercard/mpsdk/implementation/z;

.field private e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/p;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mpsdk/implementation/p;->f:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/p;->b:Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;

    iput-object p3, p0, Lcom/mastercard/mpsdk/implementation/p;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    return-void
.end method

.method static synthetic a(Lcom/mastercard/mpsdk/implementation/p;)Lcom/mastercard/mpsdk/implementation/z;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    return-object p0
.end method

.method private a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dynamicApplicationData= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "idn= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :try_start_0
    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/p;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    const/4 v0, 0x1

    new-array v3, v0, [B

    const/16 v1, 0x6a

    const/4 v4, 0x0

    aput-byte v1, v3, v4

    new-array v0, v0, [B

    const/16 v1, -0x44

    aput-byte v1, v0, v4

    const/4 v1, 0x3

    new-array v5, v1, [B

    fill-array-data v5, :array_0

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v6

    new-instance v7, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-direct {v7, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    move-object v4, v0

    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->buildSignedDynamicApplicationData([B[B[B[BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception p1

    throw p1

    :catch_0
    const/4 p1, 0x0

    return-object p1

    nop

    :array_0
    .array-data 1
        0x5t
        0x1t
        0x26t
    .end array-data
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/mastercard/mpsdk/utils/bytes/ByteArray;",
            ">;)",
            "Lcom/mastercard/mpsdk/utils/bytes/ByteArray;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->get(I)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->setBytes(I[B)V

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_1

    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    const-string v1, "result= "

    invoke-direct {p0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object v0
.end method


# virtual methods
.method public computeApplicationCryptogram([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "keyType= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/implementation/z;->b()[B

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/implementation/z;->a()[B

    move-result-object v1

    const/4 v2, 0x1

    :try_start_0
    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v4

    aput-object v4, v3, v5

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/p;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    new-instance v3, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-direct {v3, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    invoke-interface {v2, p1, v3, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->buildGenerateAcCryptograms([BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    return-object p1

    :cond_0
    :goto_0
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->MD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    if-ne p2, p1, :cond_1

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;->getMdCryptogram()[B

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "MdCryptogram= "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p1

    :cond_1
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    if-ne p2, p1, :cond_2

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;->getUmdCryptogram()[B

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "umdCryptogram= "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/16 p1, 0x8

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->getRandomByteArray(I)[B

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "random Cryptogram= "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public computeCvc3([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B
    .locals 7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "transactionData= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "keyType= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/implementation/z;->b()[B

    move-result-object v0

    iget-object v4, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/implementation/z;->a()[B

    move-result-object v4

    :try_start_0
    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v4}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v5, p0, Lcom/mastercard/mpsdk/implementation/p;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    new-instance v6, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-direct {v6, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-direct {v0, v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    invoke-interface {v5, p1, v6, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->buildComputeCcCryptograms([BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    return-object v1

    :cond_0
    :goto_0
    sget-object p1, Lcom/mastercard/mpsdk/implementation/p$2;->a:[I

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_0

    return-object v1

    :pswitch_0
    const/16 p1, 0x8

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->getRandomByteArray(I)[B

    move-result-object p1

    new-array p2, v3, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, v2

    return-object p1

    :pswitch_1
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;->getUmdCryptogram()[B

    move-result-object p1

    new-array p2, v3, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, v2

    return-object p1

    :pswitch_2
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;->getMdCryptogram()[B

    move-result-object p1

    new-array p2, v3, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, v2

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public computeSignedDynamicData([BB[B[B[B[B[B)[B
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cryptogramInformationData= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v2, v1, [B

    const/4 v3, 0x0

    aput-byte p2, v2, v3

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "applicationCryptogram= "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "pdolRelatedData= "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p4}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "generateAcResponse= "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p5}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "additionalIccDynamicData= "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p6}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object p6

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p6, Ljava/lang/StringBuilder;

    const-string v0, "unpredictableNumber= "

    invoke-direct {p6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p7}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/p;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    invoke-interface {v0, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->initIccKey(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)I

    move-result v0
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "receivedPdolValues= "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p4}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "generateAcResponse= "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p5}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p4, :cond_0

    array-length v2, p4

    if-lez v2, :cond_0

    array-length v2, p4

    array-length v4, p5

    add-int/2addr v2, v4

    new-array v2, v2, [B

    array-length v4, p4

    invoke-static {p4, v3, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p4, p4

    array-length v4, p5

    invoke-static {p5, v3, v2, p4, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_0
    array-length p4, p5

    new-array v2, p4, [B

    array-length p4, p5

    invoke-static {p5, v3, v2, v3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    invoke-virtual {p0, v2}, Lcom/mastercard/mpsdk/implementation/p;->sha1([B)[B

    move-result-object p4

    invoke-static {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p5

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->clearByteArray([B)V

    invoke-static {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->clearByteArray([B)V

    new-instance p4, Ljava/lang/StringBuilder;

    const-string v2, "hash= "

    invoke-direct {p4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array p4, v1, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, p4, v3

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p2

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p3

    invoke-static {p7}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p4

    new-instance p7, Ljava/lang/StringBuilder;

    const-string v2, "cid= "

    invoke-direct {p7, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p7, Ljava/lang/StringBuilder;

    const-string v2, "umdCryptogram= "

    invoke-direct {p7, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p7, Ljava/lang/StringBuilder;

    const-string v2, "hash= "

    invoke-direct {p7, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p7, Ljava/lang/StringBuilder;

    const-string v2, "unpredictableNumber= "

    invoke-direct {p7, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, -0x3f

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->get(I)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p7

    const/16 v0, -0x45

    invoke-virtual {p7, v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->fill(B)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/mastercard/mpsdk/implementation/p;->a(Ljava/lang/Iterable;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p2

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "DynamicApplicationData= "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array p3, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p4

    aput-object p4, p3, v3

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/mastercard/mpsdk/implementation/p;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    if-eqz p1, :cond_1

    new-array p2, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p2, v3

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    return-object p1

    :cond_1
    return-object p6

    :catch_0
    new-instance p1, Lcom/mastercard/mchipengine/exceptionmanagement/MchipCrtComponentMissingException;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/exceptionmanagement/MchipCrtComponentMissingException;-><init>()V

    throw p1

    :catch_1
    return-object p6
.end method

.method public setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "keysContext= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "credentialsScope= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->VALID_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->b:Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;

    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;->getCredentials(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mpsdk/implementation/z;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->b:Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/interfaces/TransactionCredentialManager;->getDummyCredentials()Lcom/mastercard/mpsdk/implementation/z;

    move-result-object p1

    goto :goto_0

    :goto_1
    iget-object p1, p0, Lcom/mastercard/mpsdk/implementation/p;->d:Lcom/mastercard/mpsdk/implementation/z;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/mastercard/mpsdk/implementation/p$1;

    invoke-direct {p1, p0}, Lcom/mastercard/mpsdk/implementation/p$1;-><init>(Lcom/mastercard/mpsdk/implementation/p;)V

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "Atc= "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;->getAtc()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "ProtectedIdn= "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;->getProtectedIdn()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p1

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/exceptionmanagement/MchipEngineNoValidCredentialsException;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/exceptionmanagement/MchipEngineNoValidCredentialsException;-><init>()V

    throw p1
.end method

.method public sha1([B)[B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->sha1([B)[B

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sha1= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object p1
.end method

.method public sha256([B)[B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/p;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;->sha256([B)[B

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sha256= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object p1
.end method
