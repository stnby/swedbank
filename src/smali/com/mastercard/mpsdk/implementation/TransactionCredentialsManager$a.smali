.class Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;
.super Landroid/os/HandlerThread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;

.field private b:Lcom/mastercard/mpsdk/implementation/d;

.field private c:I

.field private d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;Lcom/mastercard/mpsdk/implementation/d;I)V
    .locals 1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->a:Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager;

    const-class p1, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object p2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->b:Lcom/mastercard/mpsdk/implementation/d;

    iput p3, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->c:I

    return-void
.end method


# virtual methods
.method final a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->start()V

    return-void
.end method

.method public onLooperPrepared()V
    .locals 4

    invoke-static {}, Ljava/lang/Thread;->yield()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->b:Lcom/mastercard/mpsdk/implementation/d;

    iget v1, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->c:I

    iget-object v2, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->USED_FOR_CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->USED_FOR_DSRP:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    :goto_0
    invoke-interface {v0, v1, v2}, Lcom/mastercard/mpsdk/implementation/d;->a(ILcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->b:Lcom/mastercard/mpsdk/implementation/d;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->c:I

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/implementation/TransactionCredentialsManager$a;->quitSafely()Z

    return-void
.end method
