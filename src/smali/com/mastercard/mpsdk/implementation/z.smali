.class public Lcom/mastercard/mpsdk/implementation/z;
.super Ljava/lang/Object;


# instance fields
.field a:[B

.field b:[B

.field private c:[B

.field private d:[B

.field private e:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>([B[B[B[B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mastercard/mpsdk/implementation/z;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/implementation/z;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/z;->a:[B

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/z;->b:[B

    invoke-static {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/z;->c:[B

    invoke-static {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->arrayCopy([B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/implementation/z;->d:[B

    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EncryptedMd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/z;->c:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/z;->c:[B

    return-object v0
.end method

.method public final b()[B
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EncryptedUmd= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/implementation/z;->d:[B

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mastercard/mpsdk/implementation/z;->d:[B

    return-object v0
.end method
