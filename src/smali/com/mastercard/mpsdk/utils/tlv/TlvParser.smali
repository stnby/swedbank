.class public Lcom/mastercard/mpsdk/utils/tlv/TlvParser;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseTlv([BIILcom/mastercard/mpsdk/utils/tlv/TlvHandler;)V
    .locals 4

    add-int/2addr p2, p1

    :goto_0
    if-ge p1, p2, :cond_2

    :try_start_0
    aget-byte v0, p0, p1

    const/4 v1, 0x0

    and-int/lit8 v2, v0, 0x1f

    int-to-byte v2, v2

    const/16 v3, 0x1f

    if-ne v2, v3, :cond_0

    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/Utils;->readShort([BI)S

    move-result v1

    add-int/lit8 p1, p1, 0x2

    goto :goto_1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    :goto_1
    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/BerTlvUtils;->getTlvLength([BI)I

    move-result v2

    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/BerTlvUtils;->getTlvLengthByte([BI)I

    move-result v3

    add-int/2addr p1, v3

    if-nez v1, :cond_1

    invoke-virtual {p3, v0, v2, p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/TlvHandler;->parseTag(BI[BI)V

    goto :goto_2

    :cond_1
    invoke-virtual {p3, v1, v2, p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/TlvHandler;->parseTag(SI[BI)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/2addr p1, v2

    goto :goto_0

    :catch_0
    new-instance p0, Lcom/mastercard/mpsdk/utils/tlv/ParsingException;

    invoke-direct {p0}, Lcom/mastercard/mpsdk/utils/tlv/ParsingException;-><init>()V

    throw p0

    :cond_2
    return-void
.end method
