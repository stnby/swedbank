.class public final enum Lcom/mastercard/mpsdk/utils/tlv/Tlv;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/utils/tlv/Tlv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/utils/tlv/Tlv;

.field public static final enum INSTANCE:Lcom/mastercard/mpsdk/utils/tlv/Tlv;

.field public static mLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/tlv/Tlv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->INSTANCE:Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    sget-object v1, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->INSTANCE:Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->$VALUES:[Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->mLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static create(BLcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte p0, v0, v1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p0

    invoke-static {v0, p0}, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->create([B[B)[B

    move-result-object p0

    array-length p1, p0

    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([BI)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    return-object p0
.end method

.method public static create(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p0

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->create([B[B)[B

    move-result-object p0

    array-length p1, p0

    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([BI)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    return-object p0
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->lengthBytes([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static create([B[B)[B
    .locals 5

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->lengthBytes([B)[B

    move-result-object v0

    array-length v1, p0

    array-length v2, v0

    add-int/2addr v1, v2

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v1, v1, [B

    array-length v2, p0

    const/4 v3, 0x0

    invoke-static {p0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v2, p0

    array-length v4, v0

    invoke-static {v0, v3, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p0, p0

    array-length v0, v0

    add-int/2addr p0, v0

    array-length v0, p1

    invoke-static {p1, v3, v1, p0, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method public static lengthBytes(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->lengthBytes([B)[B

    move-result-object p0

    array-length v0, p0

    invoke-static {p0, v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([BI)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    return-object p0
.end method

.method private static lengthBytes([B)[B
    .locals 10

    array-length v0, p0

    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v4, 0x7f

    if-gt v1, v4, :cond_0

    new-array p0, v3, [B

    int-to-byte v0, v0

    aput-byte v0, p0, v2

    return-object p0

    :cond_0
    array-length v1, p0

    const/4 v4, 0x2

    const/16 v5, 0xff

    if-gt v1, v5, :cond_1

    new-array p0, v4, [B

    const/16 v1, -0x7f

    aput-byte v1, p0, v2

    and-int/2addr v0, v5

    int-to-byte v0, v0

    aput-byte v0, p0, v3

    return-object p0

    :cond_1
    array-length v1, p0

    const v6, 0xffff

    const v7, 0xff00

    const/4 v8, 0x3

    if-gt v1, v6, :cond_2

    new-array p0, v8, [B

    const/16 v1, -0x7e

    aput-byte v1, p0, v2

    and-int v1, v0, v7

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p0, v3

    and-int/2addr v0, v5

    int-to-byte v0, v0

    aput-byte v0, p0, v4

    return-object p0

    :cond_2
    array-length p0, p0

    const v1, 0xffffff

    const/high16 v6, 0xff0000

    const/4 v9, 0x4

    if-gt p0, v1, :cond_3

    new-array p0, v9, [B

    const/16 v1, -0x7d

    aput-byte v1, p0, v2

    and-int v1, v0, v6

    shr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p0, v3

    and-int v1, v0, v7

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p0, v4

    and-int/2addr v0, v5

    int-to-byte v0, v0

    aput-byte v0, p0, v8

    return-object p0

    :cond_3
    const/4 p0, 0x5

    new-array p0, p0, [B

    const/16 v1, -0x7c

    aput-byte v1, p0, v2

    const/high16 v1, -0x1000000

    and-int/2addr v1, v0

    shr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p0, v3

    and-int v1, v0, v6

    shr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p0, v4

    and-int v1, v0, v7

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p0, v8

    and-int/2addr v0, v5

    int-to-byte v0, v0

    aput-byte v0, p0, v9

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/tlv/Tlv;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/utils/tlv/Tlv;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->$VALUES:[Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/utils/tlv/Tlv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/utils/tlv/Tlv;

    return-object v0
.end method
