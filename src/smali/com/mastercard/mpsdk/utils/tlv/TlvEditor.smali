.class public Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;
    }
.end annotation


# static fields
.field public static mLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;


# instance fields
.field private mTlv:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    return-void
.end method

.method private constructor <init>([B)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    invoke-static {p1, v1}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->readNextTag([BI)[B

    move-result-object v2

    array-length v3, v2

    add-int/2addr v3, v1

    invoke-static {p1, v3}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->getNumberOfBytesInLength([BI)I

    move-result v4

    invoke-static {p1, v3}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->getDataLength([BI)I

    move-result v5

    new-array v6, v5, [B

    add-int/2addr v3, v4

    invoke-static {p1, v3, v6, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    new-instance v7, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;

    invoke-direct {v7, p0, v2, v6}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;-><init>(Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;[B[B)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v2, v2

    add-int/2addr v2, v4

    add-int/2addr v2, v5

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return-void
.end method

.method private find([B)Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;

    iget-object v2, v1, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;->mTag:[B

    invoke-static {p1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private static getDataLength([BI)I
    .locals 6

    if-eqz p0, :cond_3

    if-ltz p1, :cond_3

    array-length v0, p0

    if-ge p1, v0, :cond_3

    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->getNumberOfBytesInLength([BI)I

    move-result v0

    array-length v1, p0

    add-int v2, p1, v0

    if-lt v1, v2, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    aget-byte p0, p0, p1

    return p0

    :cond_0
    const/4 v2, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    sub-int v4, v0, v2

    sub-int/2addr v4, v1

    mul-int/lit8 v4, v4, 0x8

    add-int v5, v2, p1

    aget-byte v5, p0, v5

    and-int/lit16 v5, v5, 0xff

    shl-int v4, v5, v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v3

    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Invalid length"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Invalid offset or data"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static getNumberOfBytesInLength([BI)I
    .locals 1

    if-eqz p0, :cond_1

    if-ltz p1, :cond_1

    array-length v0, p0

    if-ge p1, v0, :cond_1

    aget-byte p0, p0, p1

    and-int/lit16 p1, p0, 0x80

    const/16 v0, 0x80

    if-ne p1, v0, :cond_0

    and-int/lit8 p0, p0, 0x7f

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    add-int/lit8 p0, p0, 0x1

    return p0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Invalid offset or data"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static getTagLength([BI)I
    .locals 4

    if-eqz p0, :cond_2

    if-ltz p1, :cond_2

    array-length v0, p0

    if-ge p1, v0, :cond_2

    aget-byte v0, p0, p1

    const/16 v1, 0x1f

    and-int/2addr v0, v1

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    array-length v1, p0

    if-ge v2, v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    add-int v1, p1, v2

    aget-byte v1, p0, v1

    const/16 v3, 0x80

    and-int/2addr v1, v3

    if-ne v1, v3, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Invalid offset or data"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private indexOf([B)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;

    iget-object v1, v1, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;->mTag:[B

    invoke-static {v1, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public static of([B)Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;
    .locals 1

    :try_start_0
    new-instance v0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;

    invoke-direct {v0, p0}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    const/4 p0, 0x0

    return-object p0
.end method

.method private static readNextTag([BI)[B
    .locals 3

    if-eqz p0, :cond_1

    if-ltz p1, :cond_1

    array-length v0, p0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->getTagLength([BI)I

    move-result v0

    new-array v1, v0, [B

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1

    :cond_1
    :goto_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid TLV: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p0, :cond_2

    const-string p0, "null"

    goto :goto_1

    :cond_2
    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p0

    :goto_1
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public addTlv([B[B)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->indexOf([B)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    new-instance v1, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;

    invoke-direct {v1, p0, p1, p2}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;-><init>(Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;[B[B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    new-instance v2, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;

    invoke-direct {v2, p0, p1, p2}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;-><init>(Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;[B[B)V

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getValue(Ljava/lang/String;)[B
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->getValue([B)[B

    move-result-object p1

    return-object p1
.end method

.method public getValue([B)[B
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->find([B)Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;->mValue:[B

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public rebuildByteArray()[B
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor;->mTlv:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;

    iget-object v3, v2, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;->mTag:[B

    iget-object v2, v2, Lcom/mastercard/mpsdk/utils/tlv/TlvEditor$Field;->mValue:[B

    invoke-static {v3, v2}, Lcom/mastercard/mpsdk/utils/tlv/Tlv;->create([B[B)[B

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v4, v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_1
    new-array v1, v3, [B

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v5, v4

    invoke-static {v4, v2, v1, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, v4

    add-int/2addr v3, v4

    goto :goto_2

    :cond_2
    return-object v1
.end method
