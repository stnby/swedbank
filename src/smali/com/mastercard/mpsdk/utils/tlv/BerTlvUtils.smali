.class public Lcom/mastercard/mpsdk/utils/tlv/BerTlvUtils;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTlvLength([BI)I
    .locals 2

    aget-byte v0, p0, p1

    if-lez v0, :cond_0

    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0x80

    if-ge v0, v1, :cond_0

    aget-byte p0, p0, p1

    and-int/lit16 p0, p0, 0xff

    return p0

    :cond_0
    aget-byte v0, p0, p1

    const/16 v1, -0x7f

    if-ne v0, v1, :cond_1

    add-int/lit8 p1, p1, 0x1

    aget-byte p0, p0, p1

    and-int/lit16 p0, p0, 0xff

    return p0

    :cond_1
    aget-byte v0, p0, p1

    const/16 v1, -0x7e

    if-ne v0, v1, :cond_2

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 p1, p1, 0x2

    aget-byte p0, p0, p1

    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, v0

    return p0

    :cond_2
    aget-byte v0, p0, p1

    const/16 v1, -0x7d

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x3

    aget-byte p0, p0, p1

    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, v0

    return p0

    :cond_3
    aget-byte v0, p0, p1

    const/16 v1, -0x7c

    if-ne v0, v1, :cond_4

    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x3

    aget-byte p0, p0, p1

    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, v0

    return p0

    :cond_4
    aget-byte p0, p0, p1

    and-int/lit16 p0, p0, 0xff

    return p0
.end method

.method public static getTlvLengthByte([BI)I
    .locals 2

    aget-byte v0, p0, p1

    const/16 v1, -0x7f

    if-ne v0, v1, :cond_0

    const/4 p0, 0x2

    return p0

    :cond_0
    aget-byte v0, p0, p1

    const/16 v1, -0x7e

    if-ne v0, v1, :cond_1

    const/4 p0, 0x3

    return p0

    :cond_1
    aget-byte v0, p0, p1

    const/16 v1, -0x7d

    if-ne v0, v1, :cond_2

    const/4 p0, 0x4

    return p0

    :cond_2
    aget-byte p0, p0, p1

    const/16 p1, -0x7c

    if-ne p0, p1, :cond_3

    const/4 p0, 0x5

    return p0

    :cond_3
    const/4 p0, 0x1

    return p0
.end method
