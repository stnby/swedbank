.class public final enum Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

.field public static final enum INSTANCE:Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

.field private static sCodes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->INSTANCE:Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    sget-object v1, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->INSTANCE:Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->$VALUES:[Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "971"

    const-string v2, "AFN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "008"

    const-string v2, "ALL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "012"

    const-string v2, "DZD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "973"

    const-string v2, "AOA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "032"

    const-string v2, "ARS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "051"

    const-string v2, "AMD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "533"

    const-string v2, "AWG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "944"

    const-string v2, "AZN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "044"

    const-string v2, "BSD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "048"

    const-string v2, "BHD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "050"

    const-string v2, "BDT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "052"

    const-string v2, "BBD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "974"

    const-string v2, "BYR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "084"

    const-string v2, "BZD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "060"

    const-string v2, "BMD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "064"

    const-string v2, "BTN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "356"

    const-string v2, "INR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "068"

    const-string v2, "BOB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "984"

    const-string v2, "BOV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "977"

    const-string v2, "BAM"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "072"

    const-string v2, "BWP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "578"

    const-string v2, "NOK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "986"

    const-string v2, "BRL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "096"

    const-string v2, "BND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "975"

    const-string v2, "BGN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "108"

    const-string v2, "BIF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "132"

    const-string v2, "CVE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "116"

    const-string v2, "KHR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "950"

    const-string v2, "XAF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "124"

    const-string v2, "CAD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "136"

    const-string v2, "KYD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "950"

    const-string v2, "XAF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "950"

    const-string v2, "XAF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "990"

    const-string v2, "CLF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "152"

    const-string v2, "CLP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "156"

    const-string v2, "CNY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "170"

    const-string v2, "COP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "970"

    const-string v2, "COU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "174"

    const-string v2, "KMF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "976"

    const-string v2, "CDF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "950"

    const-string v2, "XAF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "554"

    const-string v2, "NZD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "188"

    const-string v2, "CRC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "191"

    const-string v2, "HRK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "931"

    const-string v2, "CUC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "192"

    const-string v2, "CUP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "532"

    const-string v2, "ANG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "203"

    const-string v2, "CZK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "208"

    const-string v2, "DKK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "262"

    const-string v2, "DJF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "214"

    const-string v2, "DOP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "818"

    const-string v2, "EGP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "222"

    const-string v2, "SVC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "950"

    const-string v2, "XAF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "232"

    const-string v2, "ERN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "230"

    const-string v2, "ETB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "238"

    const-string v2, "FKP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "208"

    const-string v2, "DKK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "242"

    const-string v2, "FJD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "953"

    const-string v2, "XPF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "950"

    const-string v2, "XAF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "270"

    const-string v2, "GMD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "981"

    const-string v2, "GEL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "936"

    const-string v2, "GHS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "292"

    const-string v2, "GIP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "208"

    const-string v2, "DKK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "320"

    const-string v2, "GTQ"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "826"

    const-string v2, "GBP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "324"

    const-string v2, "GNF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "328"

    const-string v2, "GYD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "332"

    const-string v2, "HTG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "340"

    const-string v2, "HNL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "344"

    const-string v2, "HKD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "348"

    const-string v2, "HUF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "352"

    const-string v2, "ISK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "356"

    const-string v2, "INR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "360"

    const-string v2, "IDR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "960"

    const-string v2, "XDR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "364"

    const-string v2, "IRR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "368"

    const-string v2, "IQD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "826"

    const-string v2, "GBP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "376"

    const-string v2, "ILS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "388"

    const-string v2, "JMD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "392"

    const-string v2, "JPY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "826"

    const-string v2, "GBP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "400"

    const-string v2, "JOD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "398"

    const-string v2, "KZT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "404"

    const-string v2, "KES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "408"

    const-string v2, "KPW"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "410"

    const-string v2, "KRW"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "414"

    const-string v2, "KWD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "417"

    const-string v2, "KGS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "418"

    const-string v2, "LAK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "422"

    const-string v2, "LBP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "426"

    const-string v2, "LSL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "710"

    const-string v2, "ZAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "430"

    const-string v2, "LRD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "434"

    const-string v2, "LYD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "756"

    const-string v2, "CHF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "446"

    const-string v2, "MOP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "807"

    const-string v2, "MKD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "969"

    const-string v2, "MGA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "454"

    const-string v2, "MWK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "458"

    const-string v2, "MYR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "462"

    const-string v2, "MVR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "478"

    const-string v2, "MRO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "480"

    const-string v2, "MUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "965"

    const-string v2, "XUA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "484"

    const-string v2, "MXN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "979"

    const-string v2, "MXV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "498"

    const-string v2, "MDL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "496"

    const-string v2, "MNT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "504"

    const-string v2, "MAD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "943"

    const-string v2, "MZN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "104"

    const-string v2, "MMK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "516"

    const-string v2, "NAD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "710"

    const-string v2, "ZAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "524"

    const-string v2, "NPR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "953"

    const-string v2, "XPF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "554"

    const-string v2, "NZD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "558"

    const-string v2, "NIO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "566"

    const-string v2, "NGN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "554"

    const-string v2, "NZD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "578"

    const-string v2, "NOK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "512"

    const-string v2, "OMR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "586"

    const-string v2, "PKR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, ""

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "590"

    const-string v2, "PAB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "598"

    const-string v2, "PGK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "600"

    const-string v2, "PYG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "604"

    const-string v2, "PEN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "608"

    const-string v2, "PHP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "554"

    const-string v2, "NZD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "985"

    const-string v2, "PLN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "634"

    const-string v2, "QAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "946"

    const-string v2, "RON"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "643"

    const-string v2, "RUB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "646"

    const-string v2, "RWF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "654"

    const-string v2, "SHP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "951"

    const-string v2, "XCD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "882"

    const-string v2, "WST"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "678"

    const-string v2, "STD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "682"

    const-string v2, "SAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "941"

    const-string v2, "RSD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "690"

    const-string v2, "SCR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "694"

    const-string v2, "SLL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "702"

    const-string v2, "SGD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "532"

    const-string v2, "ANG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "994"

    const-string v2, "XSU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "090"

    const-string v2, "SBD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "706"

    const-string v2, "SOS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "710"

    const-string v2, "ZAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, ""

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "728"

    const-string v2, "SSP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "978"

    const-string v2, "EUR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "144"

    const-string v2, "LKR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "938"

    const-string v2, "SDG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "968"

    const-string v2, "SRD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "578"

    const-string v2, "NOK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "748"

    const-string v2, "SZL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "752"

    const-string v2, "SEK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "947"

    const-string v2, "CHE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "756"

    const-string v2, "CHF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "948"

    const-string v2, "CHW"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "760"

    const-string v2, "SYP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "901"

    const-string v2, "TWD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "972"

    const-string v2, "TJS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "834"

    const-string v2, "TZS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "764"

    const-string v2, "THB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "952"

    const-string v2, "XOF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "554"

    const-string v2, "NZD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "776"

    const-string v2, "TOP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "780"

    const-string v2, "TTD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "788"

    const-string v2, "TND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "949"

    const-string v2, "TRY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "934"

    const-string v2, "TMT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "036"

    const-string v2, "AUD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "800"

    const-string v2, "UGX"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "980"

    const-string v2, "UAH"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "784"

    const-string v2, "AED"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "826"

    const-string v2, "GBP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "997"

    const-string v2, "USN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "940"

    const-string v2, "UYI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "858"

    const-string v2, "UYU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "860"

    const-string v2, "UZS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "548"

    const-string v2, "VUV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "937"

    const-string v2, "VEF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "704"

    const-string v2, "VND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "840"

    const-string v2, "USD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "953"

    const-string v2, "XPF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "504"

    const-string v2, "MAD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "886"

    const-string v2, "YER"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "967"

    const-string v2, "ZMW"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "932"

    const-string v2, "ZWL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "955"

    const-string v2, "XBA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "956"

    const-string v2, "XBB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "957"

    const-string v2, "XBC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "958"

    const-string v2, "XBD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "963"

    const-string v2, "XTS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "999"

    const-string v2, "XXX"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "959"

    const-string v2, "XAU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "964"

    const-string v2, "XPD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "962"

    const-string v2, "XPT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    const-string v1, "961"

    const-string v2, "XAG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static convertAmountToDouble(JLjava/util/Currency;)D
    .locals 4

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->getFractionDigits(Ljava/util/Currency;)I

    move-result p2

    long-to-double p0, p0

    if-lez p2, :cond_0

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    int-to-double v2, p2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    div-double/2addr p0, v0

    return-wide p0
.end method

.method public static convertBcdAmountToDouble([BLjava/util/Currency;)D
    .locals 4

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->getFractionDigits(Ljava/util/Currency;)I

    move-result p1

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0xa

    invoke-static {p0, v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-double v0, v0

    if-lez p1, :cond_0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    int-to-double p0, p1

    invoke-static {v2, v3, p0, p1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p0

    goto :goto_0

    :cond_0
    const-wide/high16 p0, 0x3ff0000000000000L    # 1.0

    :goto_0
    div-double/2addr v0, p0

    return-wide v0
.end method

.method public static convertBinaryAmountToDouble([BLjava/util/Currency;)D
    .locals 5

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->getFractionDigits(Ljava/util/Currency;)I

    move-result p1

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_0

    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v2

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    mul-int/lit8 v4, v2, 0x8

    shl-int/2addr v3, v4

    int-to-long v3, v3

    add-long/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    long-to-double v0, v0

    if-lez p1, :cond_1

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    int-to-double p0, p1

    invoke-static {v2, v3, p0, p1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p0

    goto :goto_1

    :cond_1
    const-wide/high16 p0, 0x3ff0000000000000L    # 1.0

    :goto_1
    div-double/2addr v0, p0

    return-wide v0
.end method

.method public static getCodeFromNumericValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->sCodes:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static getCurrencyByCode(I)Ljava/util/Currency;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%03d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->getCodeFromNumericValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    return-object v0
.end method

.method private static getFractionDigits(Ljava/util/Currency;)I
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Currency;->getDefaultFractionDigits()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->$VALUES:[Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;

    return-object v0
.end method
