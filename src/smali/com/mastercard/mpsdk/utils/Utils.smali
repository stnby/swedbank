.class public final enum Lcom/mastercard/mpsdk/utils/Utils;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/utils/Utils;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/utils/Utils;

.field private static final HEX_DIGITS:[C

.field private static final HEX_PREFIX:Ljava/lang/String; = "0x"

.field private static final HEX_REGEX:Ljava/lang/String; = "^([A-Fa-f0-9]{2})+$"

.field public static final enum INSTANCE:Lcom/mastercard/mpsdk/utils/Utils;

.field private static final PAN_BEGIN_INDEX:I = 0x0

.field private static final PAN_END_INDEX:I = 0x13


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/utils/Utils;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/Utils;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/Utils;->INSTANCE:Lcom/mastercard/mpsdk/utils/Utils;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/mastercard/mpsdk/utils/Utils;

    sget-object v1, Lcom/mastercard/mpsdk/utils/Utils;->INSTANCE:Lcom/mastercard/mpsdk/utils/Utils;

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mpsdk/utils/Utils;->$VALUES:[Lcom/mastercard/mpsdk/utils/Utils;

    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mastercard/mpsdk/utils/Utils;->HEX_DIGITS:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static bcdAmountArrayToString([BII)Ljava/lang/String;
    .locals 7

    const-string v0, ""

    array-length v1, p0

    if-ge p1, v1, :cond_9

    add-int/2addr p2, p1

    array-length v1, p0

    if-gt p2, v1, :cond_9

    const/4 v1, 0x1

    :goto_0
    const/4 v2, 0x0

    if-ge p1, p2, :cond_6

    aget-byte v3, p0, p1

    ushr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, 0xf

    int-to-byte v3, v3

    aget-byte v4, p0, p1

    and-int/lit8 v4, v4, 0xf

    int-to-byte v4, v4

    const/16 v5, 0x9

    if-gt v3, v5, :cond_5

    if-gt v4, v5, :cond_5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v1, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_1
    if-eqz v1, :cond_2

    if-eqz v4, :cond_3

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_3
    add-int/lit8 v3, p2, -0x2

    if-ne p1, v3, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_4
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_5
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0

    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_7

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 p1, 0x2e

    if-ne p0, p1, :cond_8

    :cond_7
    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "0"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_8
    return-object v0

    :cond_9
    new-instance p0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {p0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw p0
.end method

.method public static bcdByteToInt(B)I
    .locals 1

    and-int/lit16 v0, p0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    mul-int/lit8 v0, v0, 0xa

    and-int/lit8 p0, p0, 0xf

    add-int/2addr v0, p0

    return v0
.end method

.method public static clearByteArray(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    :cond_0
    return-void
.end method

.method public static clearByteArray([B)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    :cond_0
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aput-byte v1, p0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static copyArrayRange([BII)[B
    .locals 2

    sub-int/2addr p2, p1

    new-array v0, p2, [B

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static decodeBase64([B)[B
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/util/Base64;->decode([BI)[B

    move-result-object p0

    return-object p0
.end method

.method public static doXor(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;I)[B
    .locals 1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p0

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1, v0, p2}, Lcom/mastercard/mpsdk/utils/Utils;->doXor([BI[BII)[B

    move-result-object p0

    return-object p0
.end method

.method public static doXor([BI[BII)[B
    .locals 4

    new-array v0, p4, [B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p4, :cond_0

    add-int v2, v1, p1

    aget-byte v2, p0, v2

    add-int v3, v1, p3

    aget-byte v3, p2, v3

    xor-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static encodeBase64([B)[B
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/util/Base64;->encode([BI)[B

    move-result-object p0

    return-object p0
.end method

.method public static equals([B[B)Z
    .locals 5

    const/4 v0, 0x1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    array-length v2, p0

    array-length v3, p1

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_3

    aget-byte v3, p0, v2

    aget-byte v4, p1, v2

    if-eq v3, v4, :cond_2

    return v1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return v0

    :cond_4
    :goto_1
    return v1
.end method

.method public static equals([B[BIII)Z
    .locals 5

    const/4 v0, 0x1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    add-int v2, p2, p4

    array-length v3, p0

    if-gt v2, v3, :cond_3

    add-int v2, p3, p4

    array-length v3, p1

    if-gt v2, v3, :cond_3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p4, :cond_2

    add-int v3, p2, v2

    aget-byte v3, p0, v3

    add-int v4, p3, v2

    aget-byte v4, p1, v4

    if-eq v3, v4, :cond_1

    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return v0

    :cond_3
    return v1
.end method

.method public static fromByteArrayToHexString([B)Ljava/lang/String;
    .locals 7

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    :try_start_0
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_1

    aget-byte v3, p0, v1

    add-int/lit8 v4, v2, 0x1

    sget-object v5, Lcom/mastercard/mpsdk/utils/Utils;->HEX_DIGITS:[C

    ushr-int/lit8 v6, v3, 0x4

    and-int/lit8 v6, v6, 0xf

    aget-char v5, v5, v6

    aput-char v5, v0, v2

    add-int/lit8 v2, v4, 0x1

    sget-object v5, Lcom/mastercard/mpsdk/utils/Utils;->HEX_DIGITS:[C

    and-int/lit8 v3, v3, 0xf

    aget-char v3, v5, v3

    aput-char v3, v0, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([C)V

    return-object p0

    :catch_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid Input"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static fromHexStringToByteArray(Ljava/lang/String;)[B
    .locals 7

    const/4 v0, 0x0

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "0x"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_2

    :cond_0
    const-string v1, "0x"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    rem-int/2addr v1, v2

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    const-string v1, "^([A-Fa-f0-9]{2})+$"

    invoke-static {v1, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v2, v1, 0x2

    new-array v2, v2, [B

    :goto_1
    if-ge v0, v1, :cond_3

    div-int/lit8 v3, v0, 0x2

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v5, 0x10

    invoke-static {v0, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    shl-int/lit8 v0, v0, 0x4

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    add-int/2addr v0, v4

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    move v0, v6

    goto :goto_1

    :cond_3
    return-object v2

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid hex string ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "]"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_2
    new-array p0, v0, [B

    return-object p0
.end method

.method public static getLastFourDigitOfDevicePan(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/Utils;->retrieveDevicePanFromDigitizedCardId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static isTerminalOffline(B)Z
    .locals 1

    and-int/lit8 p0, p0, 0xf

    int-to-byte p0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x6

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public static isZero(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Z
    .locals 0

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/Utils;->isZero([B)Z

    move-result p0

    return p0
.end method

.method public static isZero([B)Z
    .locals 4

    if-eqz p0, :cond_2

    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-byte v3, p0, v2

    if-eqz v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0

    :cond_2
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v0, "Input data is null in isZero(...)"

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static longToBcd(JI)[B
    .locals 12

    const/4 v0, 0x0

    const/4 v3, 0x0

    move-wide v1, p0

    :goto_0
    const-wide/16 v4, 0x0

    const-wide/16 v6, 0xa

    cmp-long v4, v1, v4

    if-eqz v4, :cond_0

    add-int/lit8 v3, v3, 0x1

    div-long/2addr v1, v6

    goto :goto_0

    :cond_0
    rem-int/lit8 v1, v3, 0x2

    if-nez v1, :cond_1

    div-int/lit8 v2, v3, 0x2

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v3, 0x1

    div-int/lit8 v2, v2, 0x2

    :goto_1
    const/4 v4, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    new-array v5, v2, [B

    move-wide v8, p0

    const/4 p0, 0x0

    :goto_3
    if-ge p0, v3, :cond_5

    rem-long v10, v8, v6

    long-to-int p1, v10

    int-to-byte p1, p1

    add-int/lit8 v10, v3, -0x1

    if-ne p0, v10, :cond_3

    if-eqz v1, :cond_3

    div-int/lit8 v10, p0, 0x2

    aput-byte p1, v5, v10

    goto :goto_4

    :cond_3
    rem-int/lit8 v10, p0, 0x2

    if-nez v10, :cond_4

    div-int/lit8 v10, p0, 0x2

    aput-byte p1, v5, v10

    goto :goto_4

    :cond_4
    shl-int/lit8 p1, p1, 0x4

    int-to-byte p1, p1

    div-int/lit8 v10, p0, 0x2

    aget-byte v11, v5, v10

    or-int/2addr p1, v11

    int-to-byte p1, p1

    aput-byte p1, v5, v10

    :goto_4
    div-long/2addr v8, v6

    add-int/lit8 p0, p0, 0x1

    goto :goto_3

    :cond_5
    const/4 p0, 0x0

    :goto_5
    div-int/lit8 p1, v2, 0x2

    if-ge p0, p1, :cond_6

    aget-byte p1, v5, p0

    sub-int v1, v2, p0

    sub-int/2addr v1, v4

    aget-byte v3, v5, v1

    aput-byte v3, v5, p0

    aput-byte p1, v5, v1

    add-int/lit8 p0, p0, 0x1

    goto :goto_5

    :cond_6
    if-ne p2, v2, :cond_7

    return-object v5

    :cond_7
    new-array p0, p2, [B

    sub-int/2addr p2, v2

    invoke-static {v5, v0, p0, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p0
.end method

.method public static longToBcdByteArray(JI)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 2

    invoke-static {p0, p1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object p0

    const/16 p1, 0x10

    invoke-virtual {p0, p1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object p0

    mul-int/lit8 p2, p2, 0x2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-ne p2, p1, :cond_0

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x0

    if-ge p2, p1, :cond_1

    invoke-virtual {p0, v0, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :goto_0
    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p0

    return-object p0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, p2, v1

    if-ge v0, v1, :cond_2

    const-string v1, "0"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static longToBinaryByteArray(JI)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 8

    const/16 v0, 0x8

    if-ltz p2, :cond_0

    if-le p2, v0, :cond_1

    :cond_0
    const/16 p2, 0x8

    :cond_1
    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->get(I)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p2, :cond_2

    add-int/lit8 v3, p2, -0x1

    sub-int/2addr v3, v2

    const/16 v4, 0xff

    mul-int/lit8 v5, v2, 0x8

    shl-int/2addr v4, v5

    int-to-long v6, v4

    and-long/2addr v6, p0

    shr-long v4, v6, v5

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static padPan(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "F"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static readInt([BI)I
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/mastercard/mpsdk/utils/Utils;->readInt([BIZ)I

    move-result p0

    return p0
.end method

.method private static readInt([BIZ)I
    .locals 4

    array-length v0, p0

    sub-int/2addr v0, p1

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    const/4 v3, 0x0

    sub-int/2addr v1, v0

    invoke-static {p0, v3, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v2

    :cond_0
    if-eqz p2, :cond_1

    add-int/lit8 p2, p1, 0x3

    aget-byte p2, p0, p2

    and-int/lit16 p2, p2, 0xff

    shl-int/lit8 p2, p2, 0x18

    add-int/lit8 v0, p1, 0x2

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr p2, v0

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr p2, v0

    aget-byte p0, p0, p1

    :goto_0
    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, p2

    return p0

    :cond_1
    aget-byte p2, p0, p1

    and-int/lit16 p2, p2, 0xff

    shl-int/lit8 p2, p2, 0x18

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr p2, v0

    add-int/lit8 v0, p1, 0x2

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr p2, v0

    add-int/lit8 p1, p1, 0x3

    aget-byte p0, p0, p1

    goto :goto_0

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private static readShort([BIZ)I
    .locals 1

    const v0, 0xffff

    if-eqz p2, :cond_0

    add-int/lit8 p2, p1, 0x1

    aget-byte p2, p0, p2

    shl-int/lit8 p2, p2, 0x8

    aget-byte p0, p0, p1

    :goto_0
    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, p2

    and-int/2addr p0, v0

    return p0

    :cond_0
    aget-byte p2, p0, p1

    shl-int/lit8 p2, p2, 0x8

    add-int/lit8 p1, p1, 0x1

    aget-byte p0, p0, p1

    goto :goto_0
.end method

.method public static readShort([BI)S
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/mastercard/mpsdk/utils/Utils;->readShort([BIZ)I

    move-result p0

    int-to-short p0, p0

    return p0
.end method

.method public static retrieveDevicePanFromDigitizedCardId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x13

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string v0, "F"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/Utils;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/utils/Utils;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/utils/Utils;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/utils/Utils;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/Utils;->$VALUES:[Lcom/mastercard/mpsdk/utils/Utils;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/utils/Utils;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/utils/Utils;

    return-object v0
.end method

.method public static wordToChar(BB)C
    .locals 1

    and-int/lit16 v0, p0, 0xf0

    ushr-int/lit8 v0, v0, 0x4

    shl-int/lit8 v0, v0, 0xc

    and-int/lit8 p0, p0, 0xf

    shl-int/lit8 p0, p0, 0x8

    add-int/2addr v0, p0

    and-int/lit16 p0, p1, 0xf0

    ushr-int/lit8 p0, p0, 0x4

    shl-int/lit8 p0, p0, 0x4

    add-int/2addr v0, p0

    and-int/lit8 p0, p1, 0xf

    add-int/2addr v0, p0

    int-to-char p0, v0

    return p0
.end method

.method public static writeInt(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;IJ)V
    .locals 6

    const/16 v0, 0x18

    shr-long v0, p2, v0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-virtual {p0, p1, v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->setByte(IB)V

    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x10

    shr-long v4, p2, v1

    and-long/2addr v4, v2

    long-to-int v1, v4

    int-to-byte v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->setByte(IB)V

    add-int/lit8 v0, p1, 0x2

    const/16 v1, 0x8

    shr-long v4, p2, v1

    and-long/2addr v4, v2

    long-to-int v1, v4

    int-to-byte v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->setByte(IB)V

    add-int/lit8 p1, p1, 0x3

    and-long/2addr p2, v2

    long-to-int p2, p2

    int-to-byte p2, p2

    invoke-virtual {p0, p1, p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->setByte(IB)V

    return-void
.end method
