.class public Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;
.super Ljava/lang/Object;


# static fields
.field private static asyncLogger:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;-><init>()V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->asyncLogger:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->asyncLogger:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    return-object v0
.end method

.method public static setInstance(Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->asyncLogger:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    invoke-virtual {v0, p0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->setMcbpLogger(Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V

    return-void
.end method
