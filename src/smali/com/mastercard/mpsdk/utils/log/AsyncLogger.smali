.class Lcom/mastercard/mpsdk/utils/log/AsyncLogger;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/McbpLogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;,
        Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;,
        Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;,
        Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;
    }
.end annotation


# static fields
.field private static sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

.field private static sLoggerThread:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    return-object v0
.end method


# virtual methods
.method public varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    new-instance v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    sget-object v2, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->DEBUG:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z

    return-void
.end method

.method public varargs e(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    new-instance v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    sget-object v2, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->ERROR:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z

    return-void
.end method

.method public varargs e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 8

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    new-instance v7, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    sget-object v3, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->EXCEPTION:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    move-object v1, v7

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v7}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z

    return-void
.end method

.method public varargs i(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    new-instance v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    sget-object v2, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->INFO:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z

    return-void
.end method

.method setMcbpLogger(Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLoggerThread:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLoggerThread:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->quitSafely()Z

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->clear()V

    :cond_1
    if-nez p1, :cond_2

    return-void

    :cond_2
    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    invoke-direct {v0, p0, p1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;

    invoke-direct {v0, p0, p1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLoggerThread:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->start()V

    return-void
.end method

.method public varargs v(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    new-instance v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    sget-object v2, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->VERBOSE:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z

    return-void
.end method

.method public varargs w(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->sLogMessagesQueue:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    new-instance v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    sget-object v2, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->WARN:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z

    return-void
.end method
