.class final enum Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/utils/log/AsyncLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MessageFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

.field public static final enum DEBUG:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

.field public static final enum ERROR:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

.field public static final enum EXCEPTION:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

.field public static final enum INFO:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

.field public static final enum VERBOSE:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

.field public static final enum WARN:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    const-string v1, "DEBUG"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->DEBUG:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    const-string v1, "VERBOSE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->VERBOSE:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    const-string v1, "EXCEPTION"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->EXCEPTION:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    const-string v1, "ERROR"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->ERROR:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    const-string v1, "INFO"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->INFO:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    new-instance v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    const-string v1, "WARN"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->WARN:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    sget-object v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->DEBUG:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->VERBOSE:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->EXCEPTION:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->ERROR:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->INFO:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->WARN:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    aput-object v1, v0, v7

    sput-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->$VALUES:[Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->$VALUES:[Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    return-object v0
.end method
