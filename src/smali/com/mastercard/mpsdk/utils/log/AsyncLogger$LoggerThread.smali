.class Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;
.super Landroid/os/HandlerThread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/utils/log/AsyncLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoggerThread"
.end annotation


# instance fields
.field private mAlive:Z

.field private final mMcbpLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

.field final synthetic this$0:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V
    .locals 1

    iput-object p1, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->this$0:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    const-class p1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0xb

    invoke-direct {p0, p1, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->mAlive:Z

    iput-object p2, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->mMcbpLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    return-void
.end method

.method private doLog(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->mMcbpLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$1;->$SwitchMap$com$mastercard$mpsdk$utils$log$AsyncLogger$MessageFilter:[I

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->getFilter()Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->getMessage()Ljava/lang/String;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->getObjects()[Ljava/lang/Object;

    return-void

    :pswitch_1
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->getThrowable()Ljava/lang/Throwable;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected onLooperPrepared()V
    .locals 1

    :goto_0
    iget-boolean v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->mAlive:Z

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->yield()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->mMcbpLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->mAlive:Z

    goto :goto_1

    :cond_0
    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger;->access$000()Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->doLog(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LoggerThread;->quit()Z

    return-void
.end method
