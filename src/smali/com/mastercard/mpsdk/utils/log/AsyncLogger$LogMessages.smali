.class Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/utils/log/AsyncLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LogMessages"
.end annotation


# instance fields
.field private final mFilter:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

.field private final mMessage:Ljava/lang/String;

.field private final mObjects:[Ljava/lang/Object;

.field private final mThrowable:Ljava/lang/Throwable;

.field private final mTimestamp:Ljava/lang/String;

.field final synthetic this$0:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;-><init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->this$0:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mMessage:Ljava/lang/String;

    iput-object p4, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mObjects:[Ljava/lang/Object;

    iput-object p2, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mFilter:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    iput-object p5, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mThrowable:Ljava/lang/Throwable;

    new-instance p1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    invoke-direct {p1, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mTimestamp:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFilter()Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mFilter:Lcom/mastercard/mpsdk/utils/log/AsyncLogger$MessageFilter;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " | "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mTimestamp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjects()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mObjects:[Ljava/lang/Object;

    return-object v0
.end method

.method public getThrowable()Ljava/lang/Throwable;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;->mThrowable:Ljava/lang/Throwable;

    return-object v0
.end method
