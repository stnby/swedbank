.class Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;
.super Ljava/util/concurrent/LinkedBlockingQueue;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mpsdk/utils/log/AsyncLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LogMessagesQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/LinkedBlockingQueue<",
        "Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;",
        ">;"
    }
.end annotation


# instance fields
.field private final mMcbpLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

.field final synthetic this$0:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/utils/log/AsyncLogger;Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->this$0:Lcom/mastercard/mpsdk/utils/log/AsyncLogger;

    invoke-direct {p0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object p2, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->mMcbpLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    return-void
.end method


# virtual methods
.method public add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->mMcbpLogger:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 0

    check-cast p1, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessagesQueue;->add(Lcom/mastercard/mpsdk/utils/log/AsyncLogger$LogMessages;)Z

    move-result p1

    return p1
.end method
