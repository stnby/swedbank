.class public Lcom/mastercard/mpsdk/utils/json/JsonUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private classType:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mpsdk/utils/json/JsonUtils;->classType:Ljava/lang/Class;

    return-void
.end method

.method public static deserializeStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result p0

    sub-int/2addr p0, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string v0, "\\},\\{"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, p0, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "}"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p0, v2

    const/4 v0, 0x1

    :goto_0
    array-length v3, p0

    sub-int/2addr v3, v1

    if-ge v0, v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "{"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, p0, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p0, v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, p0, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v0, p0

    sub-int/2addr v0, v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "{"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, p0

    sub-int/2addr v4, v1

    aget-object v4, p0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p0, v0

    array-length v0, p0

    if-ne v0, v1, :cond_1

    aget-object v0, p0, v2

    aget-object v3, p0, v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p0, v2

    :cond_1
    return-object p0
.end method

.method public static serializeObjectWithByteArray(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/ByteArrayTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/ByteArrayTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/ByteTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/ByteTransformer;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/Byte;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p1}, Lflexjson/k;->a(Ljava/lang/String;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/ByteArrayTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/ByteArrayTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/ByteTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/ByteTransformer;-><init>()V

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p1}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toJsonString([Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/ByteArrayTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/ByteArrayTransformer;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/ByteTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/ByteTransformer;-><init>()V

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v1}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p1}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public valueOf([B)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation

    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance p1, Lflexjson/j;

    invoke-direct {p1}, Lflexjson/j;-><init>()V

    const-class v1, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    new-instance v2, Lcom/mastercard/mpsdk/utils/json/ByteArrayObjectFactory;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/utils/json/ByteArrayObjectFactory;-><init>()V

    invoke-virtual {p1, v1, v2}, Lflexjson/j;->a(Ljava/lang/Class;Lflexjson/o;)Lflexjson/j;

    move-result-object p1

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    new-instance v2, Lcom/mastercard/mpsdk/utils/json/ByteObjectFactory;

    invoke-direct {v2}, Lcom/mastercard/mpsdk/utils/json/ByteObjectFactory;-><init>()V

    invoke-virtual {p1, v1, v2}, Lflexjson/j;->a(Ljava/lang/Class;Lflexjson/o;)Lflexjson/j;

    move-result-object p1

    iget-object v1, p0, Lcom/mastercard/mpsdk/utils/json/JsonUtils;->classType:Ljava/lang/Class;

    invoke-virtual {p1, v0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
