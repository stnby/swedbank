.class Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;
.super Landroid/os/AsyncTask;

# interfaces
.implements Lcom/mastercard/mpsdk/utils/task/McbpAsyncTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/mastercard/mpsdk/utils/task/McbpAsyncTask;"
    }
.end annotation


# instance fields
.field private mExecutorListener:Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    iget-object p1, p0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->mExecutorListener:Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;->onRun()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public execute(Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;)V
    .locals 2

    iput-object p1, p0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->mExecutorListener:Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Void;

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput-object v1, p1, v0

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public getState()I
    .locals 1

    invoke-super {p0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask$Status;->ordinal()I

    move-result v0

    return v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->mExecutorListener:Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;->onPostExecute()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->mExecutorListener:Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/utils/task/McbpTaskListener;->onPreExecute()V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;->onProgressUpdate([Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    return-void
.end method
