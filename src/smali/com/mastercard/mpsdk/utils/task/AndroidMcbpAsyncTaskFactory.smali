.class public final enum Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;",
        ">;",
        "Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

.field public static final enum INSTANCE:Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;->INSTANCE:Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    sget-object v1, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;->INSTANCE:Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;->$VALUES:[Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;->$VALUES:[Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTaskFactory;

    return-object v0
.end method


# virtual methods
.method public final getTask()Lcom/mastercard/mpsdk/utils/task/McbpAsyncTask;
    .locals 1

    new-instance v0, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/utils/task/AndroidMcbpAsyncTask;-><init>()V

    return-object v0
.end method
