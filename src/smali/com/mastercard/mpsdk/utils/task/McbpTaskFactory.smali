.class public final enum Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

.field public static final enum INSTANCE:Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

.field private static sMcbpAsyncTaskFactory:Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->INSTANCE:Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    sget-object v1, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->INSTANCE:Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->$VALUES:[Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    const/4 v0, 0x0

    sput-object v0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->sMcbpAsyncTaskFactory:Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getMcbpAsyncTask()Lcom/mastercard/mpsdk/utils/task/McbpAsyncTask;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->sMcbpAsyncTaskFactory:Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;->getTask()Lcom/mastercard/mpsdk/utils/task/McbpAsyncTask;

    move-result-object v0

    return-object v0
.end method

.method public static initializeAsyncEngine(Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;)V
    .locals 0

    sput-object p0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->sMcbpAsyncTaskFactory:Lcom/mastercard/mpsdk/utils/task/McbpAsyncTaskFactory;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;
    .locals 1

    const-class v0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->$VALUES:[Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    invoke-virtual {v0}, [Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mpsdk/utils/task/McbpTaskFactory;

    return-object v0
.end method
