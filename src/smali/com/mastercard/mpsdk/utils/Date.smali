.class public Lcom/mastercard/mpsdk/utils/Date;
.super Ljava/lang/Object;


# instance fields
.field private mDay:I

.field private mMonth:I

.field private mYear:I


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/mastercard/mpsdk/utils/Date;->mYear:I

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/2addr v2, v1

    iput v2, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mastercard/mpsdk/utils/Date;->mYear:I

    iput p2, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    iput p3, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    return-void
.end method


# virtual methods
.method public getDay()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    return v0
.end method

.method public getMonth()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    return v0
.end method

.method public getYear()I
    .locals 1

    iget v0, p0, Lcom/mastercard/mpsdk/utils/Date;->mYear:I

    return v0
.end method

.method public isValid()Z
    .locals 4
    .annotation runtime Lflexjson/h;
        b = false
    .end annotation

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mYear:I

    const/4 v2, 0x1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    if-nez v1, :cond_0

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    if-nez v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/Date;->getYear()I

    move-result v1

    const/16 v3, 0x7d0

    if-le v1, v3, :cond_1

    invoke-virtual {p0}, Lcom/mastercard/mpsdk/utils/Date;->getYear()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    const/16 v3, 0xc

    if-gt v1, v3, :cond_1

    const/4 v1, 0x2

    iget v3, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    sub-int/2addr v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    if-gt v0, v3, :cond_1

    return v2

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public setDay(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    return-void
.end method

.method public setMonth(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    return-void
.end method

.method public setYear(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/utils/Date;->mYear:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Date{Year="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mYear:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", Day="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mDay:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", Month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/mastercard/mpsdk/utils/Date;->mMonth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
