.class public abstract Lcom/mastercard/mpsdk/httpmanager/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mpsdk/httpmanager/e$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

.field protected final b:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

.field protected c:Ljava/net/HttpURLConnection;

.field protected d:Ljava/net/URL;

.field protected e:Ljava/lang/String;

.field protected final f:I

.field protected final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final i:Ljava/lang/String;

.field protected final j:Ljava/lang/String;

.field private final k:I

.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;ILjava/util/List;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            "Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x452

    iput v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->k:I

    const-string v0, "Retry-After"

    iput-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->l:Ljava/lang/String;

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->a:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    const-string v0, "Content-Type"

    iput-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->i:Ljava/lang/String;

    const-string v0, "Accept"

    iput-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->j:Ljava/lang/String;

    iput-object p1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->d:Ljava/net/URL;

    iput-object p2, p0, Lcom/mastercard/mpsdk/httpmanager/e;->b:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    iput p3, p0, Lcom/mastercard/mpsdk/httpmanager/e;->f:I

    iput-object p4, p0, Lcom/mastercard/mpsdk/httpmanager/e;->g:Ljava/util/List;

    iput-object p5, p0, Lcom/mastercard/mpsdk/httpmanager/e;->h:Ljava/util/HashMap;

    return-void
.end method

.method private static a(Ljava/io/OutputStream;)V
    .locals 2

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HTTP_COMMUNICATION;CLOSING INPUT STREAM : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private static a(Ljava/io/InputStream;)[B
    .locals 6

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x800

    new-array v2, v1, [B

    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    if-ne v3, v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_0

    :cond_0
    new-array v4, v3, [B

    const/4 v5, 0x0

    invoke-static {v2, v5, v4, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0
.end method

.method private static b(Ljava/net/HttpURLConnection;)I
    .locals 5

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    const-string v2, "Retry-After"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    const-string v2, "Retry-After"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :try_start_0
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    goto :goto_0

    :catch_0
    :try_start_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "EEE, dd MM yyyy HH:mm:ss zzz"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x0

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result p0

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;-><init>(ILjava/lang/String;)V

    throw v1

    :cond_0
    :goto_0
    return v1
.end method

.method private static b(Ljava/io/InputStream;)V
    .locals 2

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HTTP_COMMUNICATION;CLOSING OUTPUT STREAM : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HTTP_COMMUNICATION; CONNECTING TO "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mastercard/mpsdk/httpmanager/e;->d:Ljava/net/URL;

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HTTP_COMMUNICATION;HTTP REQUEST METHOD : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HTTP_COMMUNICATION; SENDING DATA: mData = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mastercard/mpsdk/httpmanager/e;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    iget-object v1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, p0, Lcom/mastercard/mpsdk/httpmanager/e;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {v1}, Lcom/mastercard/mpsdk/httpmanager/e;->a(Ljava/io/OutputStream;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    goto :goto_0

    :catchall_1
    move-exception v2

    move-object v1, v0

    :goto_0
    invoke-static {v1}, Lcom/mastercard/mpsdk/httpmanager/e;->a(Ljava/io/OutputStream;)V

    throw v2

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    iget-object v1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    iget-object v2, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-static {v2}, Lcom/mastercard/mpsdk/httpmanager/e;->b(Ljava/net/HttpURLConnection;)I

    move-result v2

    const/16 v3, 0xc8

    if-ne v1, v3, :cond_1

    iget-object v3, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljavax/net/ssl/SSLException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    invoke-static {v3}, Lcom/mastercard/mpsdk/httpmanager/e;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    goto :goto_2

    :cond_1
    move-object v3, v0

    :goto_2
    new-instance v4, Lcom/mastercard/mpsdk/httpmanager/e$a;

    invoke-direct {v4, p0, v1, v0, v2}, Lcom/mastercard/mpsdk/httpmanager/e$a;-><init>(Lcom/mastercard/mpsdk/httpmanager/e;I[BI)V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljavax/net/ssl/SSLException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    invoke-static {v3}, Lcom/mastercard/mpsdk/httpmanager/e;->b(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    return-object v4

    :catchall_2
    move-exception v1

    move-object v0, v3

    goto :goto_6

    :catch_0
    move-exception v1

    move-object v0, v3

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v0, v3

    goto :goto_4

    :catch_2
    move-exception v1

    move-object v0, v3

    goto :goto_5

    :catchall_3
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    :goto_3
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP_COMMUNICATION;HTTP ERROR : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_4
    move-exception v1

    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP_COMMUNICATION;SSL ERROR : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;

    const/16 v3, 0x452

    invoke-virtual {v1}, Ljavax/net/ssl/SSLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;-><init>(ILjava/lang/String;)V

    throw v2

    :catch_5
    move-exception v1

    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP_COMMUNICATION;HTTP SOCKET TIMEOUT : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;

    const/16 v3, 0x198

    invoke-virtual {v1}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;-><init>(ILjava/lang/String;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :goto_6
    invoke-static {v0}, Lcom/mastercard/mpsdk/httpmanager/e;->b(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    throw v1
.end method

.method public final a(Ljava/lang/String;)Lcom/mastercard/mpsdk/httpmanager/e;
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->e:Ljava/lang/String;

    return-object p0
.end method

.method protected final a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->b:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    iget-object v1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->b:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const-string v0, "Content-Type"

    iget-object v1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->h:Ljava/util/HashMap;

    const-string v2, "Content-Type"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "Accept"

    iget-object v1, p0, Lcom/mastercard/mpsdk/httpmanager/e;->h:Ljava/util/HashMap;

    const-string v2, "Accept"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->f:I

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    iget v0, p0, Lcom/mastercard/mpsdk/httpmanager/e;->f:I

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    return-object p1
.end method

.method abstract a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
.end method
