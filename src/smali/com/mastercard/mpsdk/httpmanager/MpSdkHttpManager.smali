.class public Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;


# instance fields
.field private final mCertificate:[B

.field private final mHostnames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTimeout:I

.field private final mTlsProtocol:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;[B[Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;[B[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x7530

    iput v0, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mTimeout:I

    iput-object p1, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mHostnames:Ljava/util/List;

    iput-object p2, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mCertificate:[B

    iput-object p3, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mTlsProtocol:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;"
        }
    .end annotation

    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object p2

    const-string v0, "https"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Lcom/mastercard/mpsdk/httpmanager/d;

    iget v3, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mTimeout:I

    iget-object v4, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mCertificate:[B

    iget-object v5, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mHostnames:Ljava/util/List;

    iget-object v6, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mTlsProtocol:[Ljava/lang/String;

    move-object v0, p2

    move-object v2, p1

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/mastercard/mpsdk/httpmanager/d;-><init>(Ljava/net/URL;Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;I[BLjava/util/List;[Ljava/lang/String;Ljava/util/HashMap;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/mastercard/mpsdk/httpmanager/c;

    iget v3, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mTimeout:I

    iget-object v4, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mHostnames:Ljava/util/List;

    move-object v0, p2

    move-object v2, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mastercard/mpsdk/httpmanager/c;-><init>(Ljava/net/URL;Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;ILjava/util/List;Ljava/util/HashMap;)V

    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p2, p3}, Lcom/mastercard/mpsdk/httpmanager/e;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/httpmanager/e;

    :cond_1
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/httpmanager/e;->a()Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public withOptionalTimeout(I)Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;
    .locals 0

    iput p1, p0, Lcom/mastercard/mpsdk/httpmanager/MpSdkHttpManager;->mTimeout:I

    return-object p0
.end method
