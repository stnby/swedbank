.class public final Lcom/mastercard/mpsdk/httpmanager/c;
.super Lcom/mastercard/mpsdk/httpmanager/e;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;ILjava/util/List;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            "Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Lcom/mastercard/mpsdk/httpmanager/e;-><init>(Ljava/net/URL;Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;ILjava/util/List;Ljava/util/HashMap;)V

    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/httpmanager/c;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mpsdk/httpmanager/c;->c:Ljava/net/HttpURLConnection;

    return-void
.end method


# virtual methods
.method final a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    check-cast p1, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/mastercard/mpsdk/httpmanager/c;->a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, p1

    move-object p1, v1

    goto :goto_0

    :catch_1
    move-exception p1

    :goto_0
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_1
    return-object v0
.end method
