.class public final Lcom/mastercard/mchipengine/apduprocessing/a/d;
.super Lcom/mastercard/mchipengine/apduprocessing/e;


# static fields
.field private static final b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final d:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x70

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x74

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    nop

    :array_0
    .array-data 1
        -0x61t
        0x69t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/d/b;)V
    .locals 4

    :try_start_0
    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->f:Ljava/util/List;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->f:Ljava/util/List;

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/b;->a(Ljava/util/List;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->g:Ljava/util/HashMap;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-instance v2, Lcom/mastercard/mchipengine/d/d;

    sget-object v3, Lcom/mastercard/mchipengine/apduprocessing/a/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v2, v3, p1}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->e:Ljava/util/List;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->e:Ljava/util/List;

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/b;->a(Ljava/util/List;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->g:Ljava/util/HashMap;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-instance v2, Lcom/mastercard/mchipengine/d/d;

    sget-object v3, Lcom/mastercard/mchipengine/apduprocessing/a/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v2, v3, p1}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/d/d;

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Ljava/lang/Iterable;)V

    invoke-virtual {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/a/d;->a(Lcom/mastercard/mchipengine/d/d;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method

.method public final a(Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->g:Ljava/util/HashMap;

    iput-object p3, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->f:Ljava/util/List;

    iput-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->e:Ljava/util/List;

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->e:Ljava/util/List;

    const-string v4, ","

    invoke-static {v2, v4}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->f:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->f:Ljava/util/List;

    const-string v4, ","

    invoke-static {v2, v4}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->g:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/d;->g:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    const-string v4, ","

    invoke-static {v2, v4}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v0, "ReadRecordResponseApdu"

    return-object v0
.end method
