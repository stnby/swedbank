.class public final Lcom/mastercard/mchipengine/apduprocessing/a/c;
.super Lcom/mastercard/mchipengine/apduprocessing/e;


# static fields
.field public static final b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final f:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# instance fields
.field public c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public d:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x77

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x7e

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x6c

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/d/b;)V
    .locals 5

    :try_start_0
    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/c;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {p1, v0}, Lcom/mastercard/mchipengine/d/b;->a(Lcom/mastercard/mchipengine/d/d;)V

    new-instance v1, Lcom/mastercard/mchipengine/d/d;

    sget-object v2, Lcom/mastercard/mchipengine/apduprocessing/a/c;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {p1, v1}, Lcom/mastercard/mchipengine/d/b;->a(Lcom/mastercard/mchipengine/d/d;)V

    new-instance p1, Lcom/mastercard/mchipengine/d/d;

    sget-object v2, Lcom/mastercard/mchipengine/apduprocessing/a/c;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/mastercard/mchipengine/d/d;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-direct {p1, v2, v3}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;[Lcom/mastercard/mchipengine/d/d;)V

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/e;->a(Lcom/mastercard/mchipengine/d/d;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v0, "GetProcessingOptionsResponseApdu"

    return-object v0
.end method
