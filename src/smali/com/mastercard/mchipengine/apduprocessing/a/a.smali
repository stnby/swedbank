.class public final Lcom/mastercard/mchipengine/apduprocessing/a/a;
.super Lcom/mastercard/mchipengine/apduprocessing/e;


# static fields
.field private static final d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final g:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# instance fields
.field public b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public c:Lcom/mastercard/mchipengine/d/b/b/e;

.field private h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private i:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x77

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v0, 0x2

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    sput-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-array v1, v0, [B

    fill-array-data v1, :array_1

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    sput-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/a;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    nop

    :array_0
    .array-data 1
        -0x61t
        0x61t
    .end array-data

    nop

    :array_1
    .array-data 1
        -0x61t
        0x60t
    .end array-data

    nop

    :array_2
    .array-data 1
        -0x61t
        0x36t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/d/b;)V
    .locals 3

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/a;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/a;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/b/e;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/a;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, v1, p1}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Ljava/lang/Iterable;)V

    invoke-super {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/e;->a(Lcom/mastercard/mchipengine/d/d;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method

.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/b/b/e;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p3, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p4, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    aput-object v2, v1, v3

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_3
    const-string v1, ""

    :goto_3
    aput-object v1, v0, v3
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string v0, "ComputeCryptographicChecksumResponseApdu"

    return-object v0
.end method
