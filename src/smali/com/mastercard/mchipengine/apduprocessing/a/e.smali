.class public final Lcom/mastercard/mchipengine/apduprocessing/a/e;
.super Lcom/mastercard/mchipengine/apduprocessing/e;


# static fields
.field private static final e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final g:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# instance fields
.field public b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public c:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x6f

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x7c

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x5b

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/d/b;)V
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/e;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/f; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v1, :cond_1

    new-instance v1, Lcom/mastercard/mchipengine/d/d;

    sget-object v2, Lcom/mastercard/mchipengine/apduprocessing/a/e;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {p1, v1}, Lcom/mastercard/mchipengine/d/b;->a(Lcom/mastercard/mchipengine/d/d;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v1, Lcom/mastercard/mchipengine/d/d;

    sget-object v2, Lcom/mastercard/mchipengine/apduprocessing/a/e;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Ljava/lang/Iterable;)V

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {p1, v3}, Lcom/mastercard/mchipengine/d/b;->a(Lcom/mastercard/mchipengine/d/d;)V

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p1, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/a/e;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {p1, v1, v0}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Ljava/lang/Iterable;)V

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/e;->a(Lcom/mastercard/mchipengine/d/d;)V
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_1
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->c:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    const-string v4, ","

    invoke-static {v2, v4}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v0, "SelectResponseApdu"

    return-object v0
.end method
