.class public final Lcom/mastercard/mchipengine/apduprocessing/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/apduprocessing/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:I = 0x1

.field private static b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/mastercard/mchipengine/apduprocessing/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final synthetic c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    sget v1, Lcom/mastercard/mchipengine/apduprocessing/b;->a:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/b;->c:[I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/b;->b:Ljava/util/Map;

    return-void
.end method

.method public static a([BLcom/mastercard/mchipengine/apduprocessing/d;)Lcom/mastercard/mchipengine/apduprocessing/a;
    .locals 2

    :try_start_0
    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/b;->b:Ljava/util/Map;

    sget v1, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->INS_OFFSET:I

    aget-byte v1, p0, v1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/apduprocessing/a;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-interface {v0, p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/a;->a([BLcom/mastercard/mchipengine/apduprocessing/d;)V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    new-instance p0, Lcom/mastercard/mchipengine/e/c;

    sget-object p1, Lcom/mastercard/mchipengine/e/a;->X:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p0

    :catch_1
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance p0, Lcom/mastercard/mchipengine/e/c;

    sget-object p1, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p0

    :catch_2
    new-instance p0, Lcom/mastercard/mchipengine/e/c;

    sget-object p1, Lcom/mastercard/mchipengine/e/a;->U:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p0
.end method

.method public static a(BLjava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B",
            "Ljava/lang/Class<",
            "+",
            "Lcom/mastercard/mchipengine/apduprocessing/a;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/b;->b:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/b;->b:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p0

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/e/h;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p0

    aput-object p0, v0, v1

    const-string p0, "INS %02X already registered in sSupportedCommands"

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/mastercard/mchipengine/e/h;-><init>(Ljava/lang/String;)V

    throw p1
.end method
