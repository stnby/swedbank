.class public Lcom/mastercard/mchipengine/apduprocessing/e;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(C)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(C)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method


# virtual methods
.method public a(Lcom/mastercard/mchipengine/d/b;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/mastercard/mchipengine/d/d;)V
    .locals 0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/e;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-void
.end method

.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const v0, 0x9000

    invoke-virtual {p1, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendChar(C)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/d/a/f;

    const-string v0, "Null input: data"

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/d/a/f;-><init>(Ljava/lang/String;)V

    throw p1
.end method
