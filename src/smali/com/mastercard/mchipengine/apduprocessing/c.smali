.class public abstract Lcom/mastercard/mchipengine/apduprocessing/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/utils/Wipeable;


# instance fields
.field protected mLogger:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private final mValue:[B


# direct methods
.method public constructor <init>([B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mLogger:Lcom/mastercard/mchipengine/utils/MChipLogger;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/c;->parse()V

    return-void

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->V:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method


# virtual methods
.method public final getCla()B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    sget v1, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->CLA_OFFSET:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public getDataC()[B
    .locals 5

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLc()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    new-array v1, v0, [B

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method public final getIns()B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    sget v1, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->INS_OFFSET:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public final getLc()B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    return v0
.end method

.method public final getLe()B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    return v0
.end method

.method public final getLength()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    array-length v0, v0

    return v0
.end method

.method public final getP1()B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    sget v1, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->P1_OFFSET:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public final getP2()B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    sget v1, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->P2_OFFSET:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public abstract parse()V
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[Raw="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "P1=%02X, P2=%02X, Lc=%02X"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/c;->getP1()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/c;->getP2()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLc()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    const/4 v5, 0x2

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    const-string v0, "CommandApdu"

    return-object v0
.end method

.method public wipe()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/c;->mValue:[B

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/i;->a([B)V

    return-void
.end method
