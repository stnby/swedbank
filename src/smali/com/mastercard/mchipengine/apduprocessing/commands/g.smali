.class public Lcom/mastercard/mchipengine/apduprocessing/commands/g;
.super Lcom/mastercard/mchipengine/apduprocessing/commands/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/apduprocessing/commands/a<",
        "Lcom/mastercard/mchipengine/apduprocessing/commands/f;",
        ">;"
    }
.end annotation


# static fields
.field private static final h:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, -0x7d

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-class v0, Lcom/mastercard/mchipengine/apduprocessing/commands/g;

    const/16 v1, -0x58

    invoke-static {v1, v0}, Lcom/mastercard/mchipengine/apduprocessing/b;->a(BLjava/lang/Class;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/a;-><init>()V

    const/16 v0, -0x80

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->a(B)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->b(B)V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->c(B)V

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->b()V

    sget-object v0, Lcom/mastercard/mchipengine/g/c/a$a;->b:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a([B)Lcom/mastercard/mchipengine/apduprocessing/c;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/f;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/f;-><init>([B)V

    return-object v0
.end method

.method protected final c()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 11

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v0, Lcom/mastercard/mchipengine/apduprocessing/commands/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/f;->getDataC()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/d/d;->a([B)Lcom/mastercard/mchipengine/d/d;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_3

    iget-object v2, v0, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    sget-object v4, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2, v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/d;->b()I

    move-result v2

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v4, Lcom/mastercard/mchipengine/apduprocessing/commands/f;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/commands/f;->getDataC()[B

    move-result-object v4

    array-length v4, v4

    if-ne v2, v4, :cond_6

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v2, v2, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v2, Lcom/mastercard/mchipengine/g/b/a;

    iget-object v4, v2, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v4, Lcom/mastercard/mchipengine/f/a;

    iget-object v5, v2, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    iget-object v6, v2, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v6, Lcom/mastercard/mchipengine/f/a;

    iget-boolean v6, v6, Lcom/mastercard/mchipengine/f/a;->a:Z

    if-eqz v6, :cond_0

    iget-object v6, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v6, v6, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v6, v6, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    :goto_0
    iget-object v6, v6, Lcom/mastercard/mchipengine/b/a;->a:Lcom/mastercard/mchipengine/b/i;

    iget-object v6, v6, Lcom/mastercard/mchipengine/b/i;->c:Ljava/util/List;

    goto :goto_1

    :cond_0
    iget-object v6, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v6, v6, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v6, v6, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    goto :goto_0

    :goto_1
    new-array v1, v1, [Ljava/lang/Object;

    const-string v7, ","

    invoke-static {v6, v7}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v3

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/b;->b(Ljava/util/List;)J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    int-to-long v9, v1

    cmp-long v1, v7, v9

    if-nez v1, :cond_5

    :try_start_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v0, v3, v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->copyOfRange(II)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v7

    new-instance v8, Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getTag()[B

    move-result-object v9

    invoke-static {v9}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v9

    invoke-direct {v8, v9, v7}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "Received PDOL: "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/d;)V

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v6
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    add-int/2addr v3, v6

    goto :goto_2

    :cond_1
    iput-object v0, v2, Lcom/mastercard/mchipengine/g/b/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-boolean v0, v4, Lcom/mastercard/mchipengine/f/a;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    goto :goto_3

    :cond_2
    iget-object v0, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    :goto_3
    iget-object v1, v0, Lcom/mastercard/mchipengine/b/a;->b:Lcom/mastercard/mchipengine/b/f;

    iget-object v3, v2, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v3, Lcom/mastercard/mchipengine/f/a;

    iget-boolean v3, v3, Lcom/mastercard/mchipengine/f/a;->a:Z

    if-nez v3, :cond_4

    :try_start_2
    invoke-virtual {v5}, Lcom/mastercard/mchipengine/d/a;->d()Lcom/mastercard/mchipengine/d/b/a/m;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/m;->b()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/d/a;->d()Lcom/mastercard/mchipengine/d/b/a/m;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/m;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean v3, v3, Lcom/mastercard/mchipengine/b/j;->h:Z

    if-eqz v3, :cond_4

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean v2, v2, Lcom/mastercard/mchipengine/b/j;->e:Z
    :try_end_2
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v2, :cond_4

    :try_start_3
    invoke-virtual {v5}, Lcom/mastercard/mchipengine/d/a;->c()Lcom/mastercard/mchipengine/d/b/a/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/a/n;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/mastercard/mchipengine/b/a;->c:Lcom/mastercard/mchipengine/b/f;
    :try_end_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_3 .. :try_end_3} :catch_0

    move-object v1, v2

    goto :goto_4

    :catch_0
    :try_start_4
    iget-object v0, v0, Lcom/mastercard/mchipengine/b/a;->c:Lcom/mastercard/mchipengine/b/f;
    :try_end_4
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_4 .. :try_end_4} :catch_1

    move-object v1, v0

    :catch_1
    :cond_4
    :goto_4
    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/a/c;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/a/c;-><init>()V

    iget-object v2, v1, Lcom/mastercard/mchipengine/b/f;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/f;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v2, v0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v1, v1, Lcom/mastercard/mchipengine/apduprocessing/d;->b:Lcom/mastercard/mchipengine/g/c/a;

    sget-object v2, Lcom/mastercard/mchipengine/g/c/a$a;->c:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {v1, v2}, Lcom/mastercard/mchipengine/g/c/a;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    return-object v0

    :catch_2
    new-instance v0, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->ac:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_5
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_6
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_7
    new-array v0, v1, [Ljava/lang/Object;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/commands/g;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    aput-object v1, v0, v3

    new-instance v0, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->ac:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :catch_3
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :catch_4
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->af:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0
.end method
