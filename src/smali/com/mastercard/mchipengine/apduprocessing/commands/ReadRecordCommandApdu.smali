.class public Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;
.super Lcom/mastercard/mchipengine/apduprocessing/c;


# direct methods
.method public constructor <init>([B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/c;-><init>([B)V

    return-void
.end method


# virtual methods
.method public final getRecordNumber()B
    .locals 1

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;->getP1()B

    move-result v0

    return v0
.end method

.method public final getSfiNumber()B
    .locals 1

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;->getP2()B

    move-result v0

    ushr-int/lit8 v0, v0, 0x3

    int-to-byte v0, v0

    return v0
.end method

.method protected parse()V
    .locals 0

    return-void
.end method
