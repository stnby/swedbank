.class public abstract Lcom/mastercard/mchipengine/apduprocessing/commands/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/apduprocessing/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/mastercard/mchipengine/apduprocessing/c;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/mastercard/mchipengine/apduprocessing/a;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/g/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field protected e:Lcom/mastercard/mchipengine/apduprocessing/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected f:Lcom/mastercard/mchipengine/apduprocessing/d;

.field protected g:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->d:Z

    return-void
.end method

.method protected static a(Ljava/util/List;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;)",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    return-object p0

    :cond_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->UNSUPPORTED_TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_POI_AUTHENTICATION:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CONTEXT_NOT_MATCHING:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    return-object p0

    :cond_2
    :goto_0
    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    return-object p0
.end method


# virtual methods
.method protected abstract a([B)Lcom/mastercard/mchipengine/apduprocessing/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation
.end method

.method public final a()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/apduprocessing/c;->toString()Ljava/lang/String;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->h:Ljava/util/List;

    const/4 v2, 0x2

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->h:Ljava/util/List;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getCla()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getCla()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1a

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getP1()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getP1()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->b:Ljava/util/List;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_4

    :cond_6
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->b:Ljava/util/List;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getP2()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    goto :goto_4

    :cond_7
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getP2()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v1, 0x0

    goto :goto_5

    :cond_8
    :goto_4
    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->i:Ljava/util/List;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->i:Ljava/util/List;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLc()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    goto :goto_6

    :cond_a
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLc()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v1, 0x0

    goto :goto_7

    :cond_b
    :goto_6
    const/4 v1, 0x1

    :goto_7
    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->j:Ljava/util/List;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    goto :goto_8

    :cond_c
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->j:Ljava/util/List;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLe()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    goto :goto_8

    :cond_d
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLe()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v1, 0x0

    goto :goto_9

    :cond_e
    :goto_8
    const/4 v1, 0x1

    :goto_9
    if-eqz v1, :cond_16

    iget-boolean v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->d:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLc()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-short v1, v1

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLength()I

    move-result v4

    add-int/lit8 v1, v1, 0x6

    if-ne v4, v1, :cond_f

    const/4 v1, 0x1

    goto :goto_a

    :cond_f
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/apduprocessing/c;->getLc()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v1, 0x0

    :goto_a
    if-eqz v1, :cond_10

    goto :goto_b

    :cond_10
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_11
    :goto_b
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c:Ljava/util/List;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    goto :goto_c

    :cond_12
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v1, v1, Lcom/mastercard/mchipengine/apduprocessing/d;->b:Lcom/mastercard/mchipengine/g/c/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/c/a;->a:Lcom/mastercard/mchipengine/g/c/a$a;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    goto :goto_c

    :cond_13
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/g/c/a$a;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v1, 0x0

    goto :goto_d

    :cond_14
    :goto_c
    const/4 v1, 0x1

    :goto_d
    if-eqz v1, :cond_15

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c()Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v2, v2, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {v1, v2}, Lcom/mastercard/mchipengine/apduprocessing/e;->a(Lcom/mastercard/mchipengine/d/b;)V

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    return-object v1

    :cond_15
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->ac:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_16
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->Y:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_17
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->X:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_18
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->W:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_19
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->W:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_1a
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->V:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0
.end method

.method protected final a(B)V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->h:Ljava/util/List;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->h:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final a(BB)V
    .locals 2

    and-int/lit16 p1, p1, 0xff

    and-int/lit16 p2, p2, 0xff

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->i:Ljava/util/List;

    :goto_0
    if-gt p1, p2, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->i:Ljava/util/List;

    int-to-byte v1, p1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final a(Lcom/mastercard/mchipengine/g/c/a$a;)V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c:Ljava/util/List;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a([BLcom/mastercard/mchipengine/apduprocessing/d;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a([B)Lcom/mastercard/mchipengine/apduprocessing/c;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    iput-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->g:Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void
.end method

.method protected final b()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->j:Ljava/util/List;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->j:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final b(B)V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected abstract c()Lcom/mastercard/mchipengine/apduprocessing/e;
.end method

.method protected final c(B)V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->b:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
