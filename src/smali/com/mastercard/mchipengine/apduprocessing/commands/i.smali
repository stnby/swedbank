.class public Lcom/mastercard/mchipengine/apduprocessing/commands/i;
.super Lcom/mastercard/mchipengine/apduprocessing/commands/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/apduprocessing/commands/a<",
        "Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;",
        ">;"
    }
.end annotation


# static fields
.field private static final h:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "325041592E5359532E4444463031"

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-class v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i;

    const/16 v1, -0x5c

    invoke-static {v1, v0}, Lcom/mastercard/mchipengine/apduprocessing/b;->a(BLjava/lang/Class;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/a;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->a(B)V

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->b(B)V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->c(B)V

    const/4 v0, 0x5

    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->a(BB)V

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c:Ljava/util/List;

    return-void
.end method

.method private static a(Lcom/mastercard/mchipengine/b/a;)Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 4

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/a/e;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/a;->a:Lcom/mastercard/mchipengine/b/i;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/a;->a:Lcom/mastercard/mchipengine/b/i;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/i;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object p0, p0, Lcom/mastercard/mchipengine/b/a;->a:Lcom/mastercard/mchipengine/b/i;

    iget-object p0, p0, Lcom/mastercard/mchipengine/b/i;->b:Ljava/util/LinkedHashMap;

    iput-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p0, v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->c:Ljava/util/LinkedHashMap;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a([B)Lcom/mastercard/mchipengine/apduprocessing/c;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;-><init>([B)V

    return-object v0
.end method

.method protected final c()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 7

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v0, v0, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, v0, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v2, v1, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    iget-object v3, v1, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v4, v4, Lcom/mastercard/mchipengine/apduprocessing/d;->b:Lcom/mastercard/mchipengine/g/c/a;

    sget-object v5, Lcom/mastercard/mchipengine/g/c/a$a;->a:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {v4, v5}, Lcom/mastercard/mchipengine/g/c/a;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    iget-object v5, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v5, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;->getFileName()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v3, :cond_0

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    sget-object v6, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v5, v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->c:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    goto :goto_1

    :cond_1
    iget-object v0, v0, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v5, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, v0, Lcom/mastercard/mchipengine/b/j;->a:Z

    if-nez v2, :cond_2

    iget-boolean v2, v0, Lcom/mastercard/mchipengine/b/j;->e:Z

    if-eqz v2, :cond_3

    :cond_2
    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->a:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v5, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v0, v0, Lcom/mastercard/mchipengine/b/j;->b:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->b:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->d:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    :goto_1
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v3, v6

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v3, v3, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v3, Lcom/mastercard/mchipengine/f/a;

    sget-object v5, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->b:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    if-ne v0, v5, :cond_5

    iput-boolean v2, v3, Lcom/mastercard/mchipengine/f/a;->a:Z

    goto :goto_2

    :cond_5
    iput-boolean v6, v3, Lcom/mastercard/mchipengine/f/a;->a:Z

    :goto_2
    sget-object v3, Lcom/mastercard/mchipengine/apduprocessing/commands/i$1;->a:[I

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mchipengine/g/c/a$a;->a:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {v4, v0}, Lcom/mastercard/mchipengine/g/c/a;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->aa:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :pswitch_0
    iget-object v0, v1, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    goto :goto_3

    :pswitch_1
    iget-object v0, v1, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    :goto_3
    invoke-static {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/i;->a(Lcom/mastercard/mchipengine/b/a;)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/g/c/a$a;->b:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {v4, v1}, Lcom/mastercard/mchipengine/g/c/a;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    goto :goto_4

    :pswitch_2
    sget-object v0, Lcom/mastercard/mchipengine/g/c/a$a;->a:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {v4, v0}, Lcom/mastercard/mchipengine/g/c/a;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/a/e;-><init>()V

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, v1, Lcom/mastercard/mchipengine/b/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    aput-object v3, v2, v6

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    iput-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/a/e;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :goto_4
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
