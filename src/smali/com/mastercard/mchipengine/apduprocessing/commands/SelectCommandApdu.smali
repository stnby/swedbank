.class public Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;
.super Lcom/mastercard/mchipengine/apduprocessing/c;


# instance fields
.field private mFileName:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>([B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/c;-><init>([B)V

    return-void
.end method


# virtual methods
.method public getFileName()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;->mFileName:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-object v0
.end method

.method protected parse()V
    .locals 1

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;->getDataC()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;->mFileName:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method

.method public wipe()V
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mchipengine/apduprocessing/c;->wipe()V

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/SelectCommandApdu;->mFileName:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/i;->a(Lcom/mastercard/mchipengine/utils/Wipeable;)V

    return-void
.end method
