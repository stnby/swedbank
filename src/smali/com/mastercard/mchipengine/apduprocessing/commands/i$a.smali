.class final enum Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/mchipengine/apduprocessing/commands/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

.field public static final enum b:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

.field public static final enum c:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

.field public static final enum d:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

.field private static final synthetic e:[Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    const-string v1, "PRIMARY_AID"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->a:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    const-string v1, "ALTERNATE_AID"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->b:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    const-string v1, "PPSE_AID"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->c:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    const-string v1, "UNKNOWN"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->d:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->a:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->b:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->c:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->d:Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->e:[Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->e:[Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/apduprocessing/commands/i$a;

    return-object v0
.end method
