.class public Lcom/mastercard/mchipengine/apduprocessing/commands/e;
.super Lcom/mastercard/mchipengine/apduprocessing/commands/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/apduprocessing/commands/a<",
        "Lcom/mastercard/mchipengine/apduprocessing/commands/d;",
        ">;"
    }
.end annotation


# instance fields
.field private h:Lcom/mastercard/mchipengine/g/b/a;

.field private i:Lcom/mastercard/mchipengine/f/a;

.field private j:Lcom/mastercard/mchipengine/b/c;

.field private k:Lcom/mastercard/mchipengine/d/a;

.field private l:Lcom/mastercard/mchipengine/d/b;

.field private m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

.field private n:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;

    const/16 v1, -0x52

    invoke-static {v1, v0}, Lcom/mastercard/mchipengine/apduprocessing/b;->a(BLjava/lang/Class;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/a;-><init>()V

    const/16 v0, -0x80

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(B)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->c(B)V

    const/16 v1, 0x2d

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(BB)V

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->b()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0xff

    if-gt v2, v3, :cond_3

    and-int/lit8 v3, v2, -0x40

    const/16 v4, 0x80

    if-eq v3, v4, :cond_1

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_2

    int-to-byte v3, v2

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    goto :goto_0

    :cond_3
    iput-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    sget-object v0, Lcom/mastercard/mchipengine/g/c/a$a;->c:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    return-void
.end method

.method private a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 12

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {v0, p1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    :cond_0
    const/4 p1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->p:Lcom/mastercard/mchipengine/c/b;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/c/b;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->MD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    invoke-interface {v1, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->computeApplicationCryptogram([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v3

    invoke-interface {v2, v3, p2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->computeApplicationCryptogram([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B

    move-result-object p2

    if-eqz v1, :cond_4

    if-eqz p2, :cond_4

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/b/c;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v4, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v3

    invoke-static {v3, v4, v1}, Lcom/mastercard/mchipengine/c/b;->a([B[B[B)Lcom/mastercard/mchipengine/c/a/a;

    move-result-object v1

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v3, v3, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/b/b;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3
    :try_end_1
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    iget-object v6, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v6, v6, Lcom/mastercard/mchipengine/g/b/a;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v3, v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v6, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v6, v6, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/d/b/b/c;->b()Lcom/mastercard/mchipengine/d/d;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v6, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v6, v6, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/d/b/b/a;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/c/a/a;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/b/b/e;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    :try_end_2
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    :try_start_3
    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v4, v4, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    iget-object v6, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    invoke-interface {v6}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;->getProtectedIdn()[B

    move-result-object v6

    iget-object v7, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v7, v7, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v7}, Lcom/mastercard/mchipengine/d/b/b/c;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v7

    aget-byte v7, v7, v5

    iget-object v5, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v5, v5, Lcom/mastercard/mchipengine/g/b/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v8

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v9

    const/4 v10, 0x0

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->k:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/a;->f()Lcom/mastercard/mchipengine/d/b/a/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/t;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v11

    move-object v5, v6

    move v6, v7

    move-object v7, p2

    invoke-interface/range {v4 .. v11}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->computeSignedDynamicData([BB[B[B[B[B[B)[B

    move-result-object v3
    :try_end_3
    .catch Lcom/mastercard/mchipengine/e/i; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_2

    :try_start_4
    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    new-instance v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;

    invoke-direct {v4}, Lcom/mastercard/mchipengine/apduprocessing/a/b;-><init>()V

    iget-object v5, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v5, v5, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    iget-object v6, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v6, v6, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v7, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v7, v7, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    iput-boolean v2, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->h:Z

    iput-object v5, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    iput-object v6, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->c:Lcom/mastercard/mchipengine/d/b/b/a;

    iput-object v3, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p1, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v1, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->f:Lcom/mastercard/mchipengine/c/a/a;

    :goto_0
    iput-object v7, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->g:Lcom/mastercard/mchipengine/d/b/b/e;
    :try_end_4
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :cond_2
    :try_start_5
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->ap:Lcom/mastercard/mchipengine/e/a;

    const-string v1, "Error during Signed Dynamic Data computation"

    invoke-direct {p1, p2, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1
    :try_end_5
    .catch Lcom/mastercard/mchipengine/e/i; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    :try_start_6
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->ap:Lcom/mastercard/mchipengine/e/a;

    const-string v1, "Error during Signed Dynamic Data computation"

    invoke-direct {p1, p2, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :catch_1
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_3
    new-instance v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;

    invoke-direct {v4}, Lcom/mastercard/mchipengine/apduprocessing/a/b;-><init>()V

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v3, v3, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    iget-object v7, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v7, v7, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    iput-boolean v5, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->h:Z

    iput-object v2, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    iput-object v3, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->c:Lcom/mastercard/mchipengine/d/b/b/a;

    iput-object p1, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v6, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v1, v4, Lcom/mastercard/mchipengine/apduprocessing/a/b;->f:Lcom/mastercard/mchipengine/c/a/a;

    goto :goto_0

    :goto_1
    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->u:Lcom/mastercard/mchipengine/g/a;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/k;->l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, v3, v1, v2, p2}, Lcom/mastercard/mchipengine/g/a;->a(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->n:Lcom/mastercard/mchipengine/utils/MChipByteArray;
    :try_end_6
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/i;->a(Lcom/mastercard/mchipengine/utils/Wipeable;)V

    return-object v4

    :cond_4
    :try_start_7
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    const-string v1, "Error during AC computation"

    invoke-direct {p1, p2, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1
    :try_end_7
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catch_2
    move-exception p1

    goto :goto_2

    :catch_3
    move-exception p1

    goto :goto_3

    :catchall_0
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    goto :goto_4

    :catch_4
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    :goto_2
    :try_start_8
    new-instance p2, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->af:Lcom/mastercard/mchipengine/e/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot find Input Tag Models (IccDynamicNumber or DataAuthenticationCode): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/a/e;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p2

    :catch_5
    move-exception p2

    move-object v0, p1

    move-object p1, p2

    :goto_3
    new-instance p2, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Crypto module failed during Cryptogram Input generation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/c/a;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception p1

    :goto_4
    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/i;->a(Lcom/mastercard/mchipengine/utils/Wipeable;)V

    throw p1
.end method

.method private a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;ZLcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 8

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-eq p1, v0, :cond_0

    invoke-static {}, Lcom/mastercard/mchipengine/g/b/a;->b()V

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v4

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/b/c;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object v5

    sget-object v6, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v6}, Lcom/mastercard/mchipengine/f/c;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)I

    move-result v0

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/b/c;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v2

    const/16 v4, -0x80

    const/4 v5, 0x1

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/b/b;->a()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    const/4 v6, 0x7

    invoke-virtual {v2, v3, v6}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array v6, v5, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    :goto_0
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v2, Lcom/mastercard/mchipengine/apduprocessing/commands/d;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/apduprocessing/commands/d;->getP1()B

    move-result v2

    const/16 v6, 0x10

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    const/4 v6, 0x6

    invoke-virtual {v2, v5, v6}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array v6, v5, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    :cond_2
    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/f/a;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    move-result-object v2

    sget-object v6, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->INTERNATIONAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    if-ne v2, v6, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v6, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v6, v6, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    xor-int/2addr v2, v5

    invoke-virtual {v6, v2}, Lcom/mastercard/mchipengine/d/b/b/b;->a(Z)V

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CONSENT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const/4 v6, 0x3

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    const/4 v7, 0x5

    invoke-virtual {v2, v7, v6}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array v7, v5, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v3

    :cond_4
    :try_start_0
    sget v2, Lcom/mastercard/mchipengine/assessment/b;->f:I

    if-eq v0, v2, :cond_6

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->k:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/a;->h()Lcom/mastercard/mchipengine/d/b/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/a/d;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {v1, v6, v3}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_6
    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {v1, v0, v5}, Lcom/mastercard/mchipengine/d/b/b/b;->a(IZ)V

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/b/c;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mastercard/mchipengine/f/c;->a(ILcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/b/c;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p2

    invoke-virtual {v2, v0, p1, p2}, Lcom/mastercard/mchipengine/d/b;->a(ILcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Ljava/util/List;)V

    goto :goto_2

    :cond_7
    iget-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    const/4 v0, 0x0

    iput-object v0, p2, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    :goto_2
    iget-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object p2, p2, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/b/c;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    if-ne v0, v4, :cond_8

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->AUTHORIZE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    goto :goto_3

    :cond_8
    if-eqz p3, :cond_a

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/d/b/b/b;->c()Z

    move-result p2

    if-eqz p2, :cond_9

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->AUTHENTICATE_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    goto :goto_3

    :cond_9
    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->DECLINE_BY_TERMINAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    goto :goto_3

    :cond_a
    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-ne p1, p2, :cond_b

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->DECLINE_BY_CARD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    goto :goto_3

    :cond_b
    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->WALLET_ACTION_REQUIRED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    :goto_3
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    sget-object v2, Lcom/mastercard/mchipengine/apduprocessing/commands/e$1;->a:[I

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->ordinal()I

    move-result p1

    aget p1, v2, p1

    packed-switch p1, :pswitch_data_0

    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :pswitch_0
    :try_start_1
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->VALID_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-interface {v0, p1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
    :try_end_1
    .catch Lcom/mastercard/mchipengine/e/j; {:try_start_1 .. :try_end_1} :catch_1

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V

    if-eqz p3, :cond_c

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/b/b;->b()V

    :goto_4
    invoke-direct {p0, p4, v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    goto :goto_5

    :cond_c
    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/b/b;->b()V

    invoke-static {}, Lcom/mastercard/mchipengine/a/a;->a()V

    goto :goto_4

    :catch_1
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->aq:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :pswitch_1
    :try_start_2
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->RANDOM_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-interface {v0, p1, p3}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
    :try_end_2
    .catch Lcom/mastercard/mchipengine/e/j; {:try_start_2 .. :try_end_2} :catch_3

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->RANDOM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    invoke-direct {p0, p4, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    :try_start_3
    new-instance p3, Lcom/mastercard/mchipengine/g/d/g;

    iget-object p4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->k:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {p4}, Lcom/mastercard/mchipengine/d/a;->a()Lcom/mastercard/mchipengine/d/b/a/b;

    move-result-object p4

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->k:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->b()Lcom/mastercard/mchipengine/d/b/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    iget-boolean v1, v1, Lcom/mastercard/mchipengine/f/a;->a:Z

    invoke-direct {p3, p4, v0, v1}, Lcom/mastercard/mchipengine/g/d/g;-><init>(Lcom/mastercard/mchipengine/d/b/a/b;Lcom/mastercard/mchipengine/d/b/a/q;Z)V

    invoke-static {p3}, Lcom/mastercard/mchipengine/g/b/a;->a(Lcom/mastercard/mchipengine/g/d/g;)V
    :try_end_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_5

    :catch_2
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->af:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :catch_3
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->aq:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :pswitch_2
    :try_start_4
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->RANDOM_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-interface {v0, p1, p3}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
    :try_end_4
    .catch Lcom/mastercard/mchipengine/e/j; {:try_start_4 .. :try_end_4} :catch_4

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->m:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->RANDOM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    invoke-direct {p0, p4, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    :goto_5
    iget-object p3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {p3}, Lcom/mastercard/mchipengine/f/a;->d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object p3

    iget-object p4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {p4}, Lcom/mastercard/mchipengine/f/a;->e()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object p4

    new-instance v0, Lcom/mastercard/mchipengine/g/d/d;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->n:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p2, p3, p4, v1}, Lcom/mastercard/mchipengine/g/d/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    new-array p2, v5, [Ljava/lang/Object;

    aput-object v0, p2, v3

    iget-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object p2, p2, Lcom/mastercard/mchipengine/g/b/c;->g:Lcom/mastercard/mchipengine/a;

    invoke-interface {p2, v0}, Lcom/mastercard/mchipengine/a;->onTransactionProcessingFinished(Lcom/mastercard/mchipengine/g/d/d;)V

    return-object p1

    :catch_4
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->aq:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    new-instance v1, Lcom/mastercard/mchipengine/d/b/b/a;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;->getAtc()[B

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/mastercard/mchipengine/d/b/b/a;-><init>([B)V

    iput-object v1, v0, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method


# virtual methods
.method protected final synthetic a([B)Lcom/mastercard/mchipengine/apduprocessing/c;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/d;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/d;-><init>([B)V

    return-object v0
.end method

.method protected final c()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v1, Lcom/mastercard/mchipengine/apduprocessing/commands/d;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/d;->getP1()B

    move-result v1

    and-int/lit8 v2, v1, -0x40

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    const/16 v6, 0x40

    if-ne v2, v6, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    const/16 v8, -0x80

    if-ne v2, v8, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    const/16 v9, 0x10

    and-int/2addr v1, v9

    if-ne v1, v9, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v4

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v3

    const/4 v10, 0x2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v9, v10

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v1, v1, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v1, Lcom/mastercard/mchipengine/g/b/a;

    iput-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v1, Lcom/mastercard/mchipengine/f/a;

    iput-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v9, v1, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iput-object v9, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    iget-object v9, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v9, v9, Lcom/mastercard/mchipengine/g/b/c;->o:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    iget-object v10, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v10, v10, Lcom/mastercard/mchipengine/g/b/c;->j:Lcom/mastercard/mchipengine/assessment/a;

    iget-object v11, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v11, v11, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    iput-object v11, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->k:Lcom/mastercard/mchipengine/d/a;

    iget-object v11, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v11, v11, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    iput-object v11, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v11, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v11, Lcom/mastercard/mchipengine/apduprocessing/commands/d;

    invoke-virtual {v11}, Lcom/mastercard/mchipengine/apduprocessing/commands/d;->getDataC()[B

    move-result-object v11

    new-array v12, v3, [Ljava/lang/Object;

    invoke-static {v11}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v13

    aput-object v13, v12, v4

    iget-object v12, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    iget-object v12, v12, Lcom/mastercard/mchipengine/b/c;->h:Ljava/util/List;

    new-array v13, v3, [Ljava/lang/Object;

    const-string v14, ","

    invoke-static {v12, v14}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v4

    if-eqz v12, :cond_5

    invoke-static {v12}, Lcom/mastercard/mchipengine/utils/b;->b(Ljava/util/List;)J

    move-result-wide v13

    array-length v15, v11

    move/from16 v16, v5

    int-to-long v4, v15

    cmp-long v4, v13, v4

    if-nez v4, :cond_4

    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->k:Lcom/mastercard/mchipengine/d/a;

    :try_start_0
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v12, 0x0

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {v13}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v14

    add-int/2addr v14, v12

    invoke-static {v11, v12, v14}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v14

    new-instance v15, Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v13}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getTag()[B

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-static {v14}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v14

    invoke-direct {v15, v3, v14}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v14, "Received CDOL: "

    invoke-direct {v3, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v15}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/d;)V

    invoke-virtual {v13}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v3
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v12, v3

    const/4 v3, 0x1

    goto :goto_4

    :catch_0
    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->ac:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v1

    :cond_4
    new-instance v1, Lcom/mastercard/mchipengine/e/c;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v1

    :cond_5
    move/from16 v16, v5

    :cond_6
    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    invoke-static {v11}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    iput-object v4, v3, Lcom/mastercard/mchipengine/g/b/a;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v3, Lcom/mastercard/mchipengine/apduprocessing/commands/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/apduprocessing/commands/d;->getP1()B

    move-result v3

    and-int/lit8 v3, v3, -0x40

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v3, v3, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v3, Lcom/mastercard/mchipengine/g/b/a;

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHENTICATE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    :goto_5
    iput-object v4, v3, Lcom/mastercard/mchipengine/g/b/a;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    goto :goto_6

    :cond_7
    if-eq v3, v6, :cond_8

    if-ne v3, v8, :cond_9

    :cond_8
    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v3, v3, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v3, Lcom/mastercard/mchipengine/g/b/a;

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHORIZE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    goto :goto_5

    :cond_9
    :goto_6
    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    iget-boolean v3, v3, Lcom/mastercard/mchipengine/f/a;->a:Z

    if-eqz v3, :cond_a

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    goto :goto_7

    :cond_a
    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    :goto_7
    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v5, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/b/c;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/mastercard/mchipengine/d/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/b;->b()V

    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    iget-boolean v4, v4, Lcom/mastercard/mchipengine/f/a;->a:Z

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    if-eqz v4, :cond_b

    iget-boolean v1, v1, Lcom/mastercard/mchipengine/b/j;->b:Z

    goto :goto_8

    :cond_b
    iget-boolean v1, v1, Lcom/mastercard/mchipengine/b/j;->a:Z

    :goto_8
    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->j:Lcom/mastercard/mchipengine/b/c;

    iget v1, v1, Lcom/mastercard/mchipengine/b/c;->f:I

    array-length v4, v11

    if-ne v1, v4, :cond_f

    array-length v1, v11

    const/16 v4, 0x2d

    if-lt v1, v4, :cond_f

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->h:Lcom/mastercard/mchipengine/g/b/a;

    invoke-virtual {v10, v1}, Lcom/mastercard/mchipengine/assessment/a;->a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Ljava/util/List;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object v4

    new-instance v5, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;

    invoke-direct {v5, v4, v1}, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Ljava/util/List;)V

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/f/a;->e()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v4

    iget-object v8, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v8}, Lcom/mastercard/mchipengine/f/a;->d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object v8

    invoke-interface {v9, v5, v4, v8}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;->getFinalAssessment(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object v4

    new-array v8, v1, [Ljava/lang/Object;

    aput-object v4, v8, v6

    if-eqz v4, :cond_e

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-ne v4, v1, :cond_d

    if-nez v7, :cond_c

    if-eqz v2, :cond_d

    :cond_c
    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/b/c;->c()V

    goto :goto_9

    :cond_d
    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->l:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    iput-object v6, v1, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    aput-object v1, v6, v2

    :goto_9
    iget-object v1, v3, Lcom/mastercard/mchipengine/b/a;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move/from16 v3, v16

    invoke-direct {v0, v4, v5, v3, v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;ZLcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object v1

    return-object v1

    :cond_e
    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->Q:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v1

    :cond_f
    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v1

    :cond_10
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;->i:Lcom/mastercard/mchipengine/f/a;

    iget-boolean v2, v2, Lcom/mastercard/mchipengine/f/a;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->Q:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v1
.end method
