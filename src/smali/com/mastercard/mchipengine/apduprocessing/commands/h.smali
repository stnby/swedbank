.class public Lcom/mastercard/mchipengine/apduprocessing/commands/h;
.super Lcom/mastercard/mchipengine/apduprocessing/commands/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/apduprocessing/commands/a<",
        "Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;",
        ">;"
    }
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/mastercard/mchipengine/apduprocessing/commands/h;

    const/16 v1, -0x4e

    invoke-static {v1, v0}, Lcom/mastercard/mchipengine/apduprocessing/b;->a(BLjava/lang/Class;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/a;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/h;->a(B)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0xff

    if-gt v1, v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->a:Ljava/util/List;

    int-to-byte v3, v1

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_1
    if-gt v3, v2, :cond_2

    and-int/lit8 v4, v3, 0x7

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    int-to-byte v4, v3

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    iput-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->b:Ljava/util/List;

    iput-boolean v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->d:Z

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/h$1;

    invoke-direct {v0, p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/h$1;-><init>(Lcom/mastercard/mchipengine/apduprocessing/commands/h;)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/a;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected final synthetic a([B)Lcom/mastercard/mchipengine/apduprocessing/c;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;-><init>([B)V

    return-object v0
.end method

.method protected final c()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/h;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v0, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;->getP1()B

    move-result v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/h;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v1, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/ReadRecordCommandApdu;->getP2()B

    move-result v1

    ushr-int/lit8 v1, v1, 0x3

    int-to-byte v1, v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    const/4 v5, 0x1

    aput-object v3, v2, v5

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/h;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v2, v2, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v2, Lcom/mastercard/mchipengine/g/b/a;

    iget-object v3, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    invoke-virtual {v3, v0, v1}, Lcom/mastercard/mchipengine/b/c;->a(BB)Lcom/mastercard/mchipengine/b/g;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    aput-object v0, v1, v4

    new-instance v1, Lcom/mastercard/mchipengine/apduprocessing/a/d;

    invoke-direct {v1}, Lcom/mastercard/mchipengine/apduprocessing/a/d;-><init>()V

    iget-boolean v3, v0, Lcom/mastercard/mchipengine/b/g;->b:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    iput-object v3, v2, Lcom/mastercard/mchipengine/g/b/a;->a:Ljava/util/List;

    iget-object v2, v0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    invoke-virtual {v1, v2, v3, v0}, Lcom/mastercard/mchipengine/apduprocessing/a/d;->a(Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    if-eqz v3, :cond_1

    iput-object v3, v2, Lcom/mastercard/mchipengine/g/b/a;->b:Ljava/util/List;

    :cond_1
    iget-object v2, v0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    invoke-virtual {v1, v2, v0, v3}, Lcom/mastercard/mchipengine/apduprocessing/a/d;->a(Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/List;)V

    :goto_0
    return-object v1
.end method
