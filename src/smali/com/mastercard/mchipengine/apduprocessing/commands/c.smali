.class public Lcom/mastercard/mchipengine/apduprocessing/commands/c;
.super Lcom/mastercard/mchipengine/apduprocessing/commands/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/apduprocessing/commands/a<",
        "Lcom/mastercard/mchipengine/apduprocessing/commands/b;",
        ">;"
    }
.end annotation


# instance fields
.field private h:Lcom/mastercard/mchipengine/g/b/a;

.field private i:Lcom/mastercard/mchipengine/f/a;

.field private j:Lcom/mastercard/mchipengine/d/a;

.field private k:Lcom/mastercard/mchipengine/d/b;

.field private l:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

.field private m:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;

    const/16 v1, 0x2a

    invoke-static {v1, v0}, Lcom/mastercard/mchipengine/apduprocessing/b;->a(BLjava/lang/Class;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/a;-><init>()V

    const/16 v0, -0x80

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(B)V

    const/16 v1, -0x72

    invoke-virtual {p0, v1}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->b(B)V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->c(B)V

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->b()V

    sget-object v0, Lcom/mastercard/mchipengine/g/c/a$a;->c:Lcom/mastercard/mchipengine/g/c/a$a;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(Lcom/mastercard/mchipengine/g/c/a$a;)V

    return-void
.end method

.method private a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;)Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 8

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-eq p1, v0, :cond_0

    invoke-static {}, Lcom/mastercard/mchipengine/g/b/a;->b()V

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v5

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/b/c;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object v6

    sget-object v7, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v2 .. v7}, Lcom/mastercard/mchipengine/f/a;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)I

    move-result v2

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/b/c;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/mastercard/mchipengine/f/a;->a(ILcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    move-result-object v1

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/b/a/j;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p2

    invoke-virtual {v4, v2, p1, p2}, Lcom/mastercard/mchipengine/d/b;->a(ILcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Ljava/util/List;)V

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iput-object v3, p2, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iput-object v3, p2, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    :goto_0
    sget-object p2, Lcom/mastercard/mchipengine/apduprocessing/commands/c$1;->a:[I

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_0

    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :pswitch_0
    :try_start_1
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->VALID_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->l:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
    :try_end_1
    .catch Lcom/mastercard/mchipengine/e/j; {:try_start_1 .. :try_end_1} :catch_1

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->l:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V

    invoke-static {}, Lcom/mastercard/mchipengine/a/a;->a()V

    invoke-direct {p0, v1, v2}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;I)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->AUTHORIZE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    goto :goto_1

    :catch_1
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->aq:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :pswitch_1
    :try_start_2
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->RANDOM_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->l:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
    :try_end_2
    .catch Lcom/mastercard/mchipengine/e/j; {:try_start_2 .. :try_end_2} :catch_3

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->l:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->d()Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->WALLET_ACTION_REQUIRED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    :try_start_3
    new-instance v0, Lcom/mastercard/mchipengine/g/d/g;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/a;->a()Lcom/mastercard/mchipengine/d/b/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/a;->b()Lcom/mastercard/mchipengine/d/b/a/q;

    move-result-object v2

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    iget-boolean v3, v3, Lcom/mastercard/mchipengine/f/a;->a:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/mastercard/mchipengine/g/d/g;-><init>(Lcom/mastercard/mchipengine/d/b/a/b;Lcom/mastercard/mchipengine/d/b/a/q;Z)V

    invoke-static {v0}, Lcom/mastercard/mchipengine/g/b/a;->a(Lcom/mastercard/mchipengine/g/d/g;)V
    :try_end_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->af:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :catch_3
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->aq:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :pswitch_2
    :try_start_4
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->RANDOM_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->l:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
    :try_end_4
    .catch Lcom/mastercard/mchipengine/e/j; {:try_start_4 .. :try_end_4} :catch_4

    iget-object p1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->l:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->d()Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->DECLINE_BY_CARD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    :goto_1
    new-instance v0, Lcom/mastercard/mchipengine/g/d/d;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/f/a;->d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/f/a;->e()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v2

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->m:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/mastercard/mchipengine/g/d/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v0, p2, v1

    iget-object p2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object p2, p2, Lcom/mastercard/mchipengine/g/b/c;->g:Lcom/mastercard/mchipengine/a;

    invoke-interface {p2, v0}, Lcom/mastercard/mchipengine/a;->onTransactionProcessingFinished(Lcom/mastercard/mchipengine/g/d/d;)V

    return-object p1

    :catch_4
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->aq:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;I)Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 18

    move-object/from16 v0, p0

    :try_start_0
    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/b/c;->c()V

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v2, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/a;->g()Lcom/mastercard/mchipengine/d/b/a/u;

    move-result-object v2

    const/16 v3, 0x8

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    iget-object v5, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v5, v5, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v5, v5, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v5, v5, Lcom/mastercard/mchipengine/b/c;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v7, v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v6

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/a/u;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v8

    invoke-virtual {v4, v6, v8}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v5

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/a/u;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/2addr v5, v2

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    iget-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->MD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    invoke-interface {v1, v2, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->computeCvc3([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B

    move-result-object v1

    iget-object v2, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v4

    move-object/from16 v5, p1

    invoke-interface {v2, v4, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->computeCvc3([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B

    move-result-object v2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    const/4 v5, 0x2

    new-array v6, v5, [B

    aget-byte v8, v1, v7

    aput-byte v8, v6, v7

    const/4 v8, 0x1

    aget-byte v9, v1, v8

    aput-byte v9, v6, v8

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    rem-long/2addr v9, v11

    long-to-int v6, v9

    new-array v9, v5, [B

    const/4 v10, 0x6

    aget-byte v13, v2, v10

    aput-byte v13, v9, v7

    const/4 v13, 0x7

    aget-byte v14, v2, v13

    aput-byte v14, v9, v8

    invoke-static {v9}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide v14

    rem-long/2addr v14, v11

    long-to-int v9, v14

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast v4, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-array v11, v5, [B

    invoke-virtual {v4, v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v12

    aput-byte v12, v11, v7

    invoke-virtual {v4, v8}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v4

    aput-byte v4, v11, v8

    invoke-static {v11}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide v11

    const-wide/16 v14, 0x64

    rem-long/2addr v11, v14

    long-to-int v4, v11

    div-int/lit8 v11, v4, 0xa

    mul-int/lit16 v11, v11, 0x3e8

    add-int/2addr v6, v11

    rem-int/lit8 v4, v4, 0xa

    mul-int/lit16 v4, v4, 0x3e8

    add-int/2addr v9, v4

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    sget-object v12, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    sget-object v12, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v9

    new-array v3, v3, [B

    aget-byte v11, v9, v5

    aput-byte v11, v3, v7

    const/4 v11, 0x3

    aget-byte v12, v9, v11

    aput-byte v12, v3, v8

    aget-byte v12, v6, v5

    aput-byte v12, v3, v5

    aget-byte v12, v6, v11

    aput-byte v12, v3, v11

    aput-byte v7, v3, v4

    const/4 v11, 0x5

    aput-byte v7, v3, v11

    aput-byte v7, v3, v10

    aput-byte v7, v3, v13

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/i;->a([B)V

    invoke-static {v9}, Lcom/mastercard/mchipengine/utils/i;->a([B)V

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/i;->a([B)V

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/i;->a([B)V

    invoke-static {v3, v7, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    invoke-static {v3, v5, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v9, v3, Lcom/mastercard/mchipengine/g/b/c;->u:Lcom/mastercard/mchipengine/g/a;

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v10, v3, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v11, v3, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/a;->g()Lcom/mastercard/mchipengine/d/b/a/u;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/u;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v16

    iget-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/f/a;->d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object v3

    invoke-interface {v3}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;->isCdCvmSupported()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    move-result-object v3

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    if-ne v3, v4, :cond_0

    sget v3, Lcom/mastercard/mchipengine/assessment/b;->c:I

    move/from16 v4, p2

    if-ne v4, v3, :cond_0

    const/16 v17, 0x1

    goto :goto_0

    :cond_0
    const/16 v17, 0x0

    :goto_0
    iget-object v3, v10, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v13, v3, Lcom/mastercard/mchipengine/b/c;->e:Lcom/mastercard/mchipengine/b/h;

    iget-object v3, v10, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v12, v3, Lcom/mastercard/mchipengine/b/c;->d:Lcom/mastercard/mchipengine/b/h;

    move-object v14, v1

    move-object v15, v2

    invoke-virtual/range {v9 .. v17}, Lcom/mastercard/mchipengine/g/a;->a(Lcom/mastercard/mchipengine/b/b;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Z)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    iput-object v3, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->m:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-instance v3, Lcom/mastercard/mchipengine/apduprocessing/a/a;

    invoke-direct {v3}, Lcom/mastercard/mchipengine/apduprocessing/a/a;-><init>()V

    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/b/a/j;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    invoke-virtual {v3, v1, v1, v2, v4}, Lcom/mastercard/mchipengine/apduprocessing/a/a;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/b/b/e;)V

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v3, v1, v1, v2, v4}, Lcom/mastercard/mchipengine/apduprocessing/a/a;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/b/b/e;)V

    :goto_1
    return-object v3

    :cond_2
    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    const-string v3, "Error during CVC3 computation"

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->af:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v1

    :catch_1
    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v1
.end method

.method private a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    new-instance v1, Lcom/mastercard/mchipengine/d/b/b/a;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;->getAtc()[B

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/mastercard/mchipengine/d/b/b/a;-><init>([B)V

    iput-object v1, v0, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ApplicationTransactionCounter="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget v2, v2, Lcom/mastercard/mchipengine/d/b/b/a;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " (raw bytes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ")]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method

.method private d()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->m:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {p0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->e()Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object v0

    return-object v0
.end method

.method private e()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/a/a;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/a/a;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    iput-object v1, v0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v2, v0, Lcom/mastercard/mchipengine/apduprocessing/a/a;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    return-object v0

    :cond_0
    new-instance v0, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->ar:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    new-instance v0, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->af:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0
.end method


# virtual methods
.method protected final synthetic a([B)Lcom/mastercard/mchipengine/apduprocessing/c;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/b;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/apduprocessing/commands/b;-><init>([B)V

    return-object v0
.end method

.method protected final c()Lcom/mastercard/mchipengine/apduprocessing/e;
    .locals 14

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->f:Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v0, v0, Lcom/mastercard/mchipengine/apduprocessing/d;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/b/a;

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v0, Lcom/mastercard/mchipengine/f/a;

    iput-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->o:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->j:Lcom/mastercard/mchipengine/assessment/a;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    iput-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    iput-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHORIZE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    iput-object v5, v3, Lcom/mastercard/mchipengine/g/b/a;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->e:Lcom/mastercard/mchipengine/apduprocessing/c;

    check-cast v3, Lcom/mastercard/mchipengine/apduprocessing/commands/b;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/apduprocessing/commands/b;->getDataC()[B

    move-result-object v3

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    iget-object v5, v0, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v5, v5, Lcom/mastercard/mchipengine/b/c;->i:Ljava/util/List;

    new-array v6, v4, [Ljava/lang/Object;

    const-string v8, ","

    invoke-static {v5, v8}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    if-eqz v5, :cond_1

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/b;->b(Ljava/util/List;)J

    move-result-wide v8

    array-length v6, v3

    int-to-long v10, v6

    cmp-long v6, v8, v10

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->j:Lcom/mastercard/mchipengine/d/a;

    :try_start_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v8, 0x0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {v9}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v10

    add-int/2addr v10, v8

    invoke-static {v3, v8, v10}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v10

    new-instance v11, Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v9}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getTag()[B

    move-result-object v12

    invoke-static {v12}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v12

    invoke-static {v10}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v10

    invoke-direct {v11, v12, v10}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    new-array v10, v4, [Ljava/lang/Object;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v11, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v13}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, "|"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v13, v11, Lcom/mastercard/mchipengine/d/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v13, "|"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v13, v11, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v7

    invoke-virtual {v6, v11}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/d;)V

    invoke-virtual {v9}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v9
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v8, v9

    goto :goto_0

    :catch_0
    new-instance v0, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->ac:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/mastercard/mchipengine/e/c;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0

    :cond_1
    iget-object v3, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->k:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b;->b()V

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean v0, v0, Lcom/mastercard/mchipengine/b/j;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->h:Lcom/mastercard/mchipengine/g/b/a;

    invoke-virtual {v2, v0}, Lcom/mastercard/mchipengine/assessment/a;->a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(Ljava/util/List;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object v2

    new-instance v3, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;

    invoke-direct {v3, v2, v0}, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Ljava/util/List;)V

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/f/a;->e()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v0

    iget-object v2, p0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->i:Lcom/mastercard/mchipengine/f/a;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/f/a;->d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object v2

    invoke-interface {v1, v3, v0, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;->getFinalAssessment(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v0, Lcom/mastercard/mchipengine/e/b;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->Q:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw v0
.end method
