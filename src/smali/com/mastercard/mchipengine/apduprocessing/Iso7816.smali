.class public final enum Lcom/mastercard/mchipengine/apduprocessing/Iso7816;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/apduprocessing/Iso7816;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

.field public static CLA_OFFSET:I = 0x0

.field public static final C_DATA_OFFSET:I = 0x5

.field public static final enum INSTANCE:Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

.field public static INS_OFFSET:I = 0x0

.field public static final LC_OFFSET:I = 0x4

.field public static P1_OFFSET:I = 0x0

.field public static P2_OFFSET:I = 0x0

.field public static final SW_CLA_NOT_SUPPORTED:C = '\u6e00'

.field public static final SW_COMMAND_INCOMPATIBLE:C = '\u6981'

.field public static final SW_CONDITIONS_NOT_SATISFIED:C = '\u6985'

.field public static final SW_EXECUTION_ERROR:C = '\u6400'

.field public static final SW_FILE_NOT_FOUND:C = '\u6a82'

.field public static final SW_INCORRECT_P1P2:C = '\u6a86'

.field public static final SW_INS_NOT_SUPPORTED:C = '\u6d00'

.field public static final SW_NOT_ALLOWED:C = '\u6900'

.field public static final SW_NO_ERROR:C = '\u9000'

.field public static final SW_NO_PRECISE_DIAGNOSIS:C = '\u6f00'

.field public static final SW_PROCESSING_ERROR:C = '\u6500'

.field public static final SW_RECORD_NOT_FOUND:C = '\u6a83'

.field public static final SW_SECURITY_ERROR:C = '\u6600'

.field public static final SW_SECURITY_STATUS_NOT_SATISFIED:C = '\u6982'

.field public static final SW_WRONG_LENGTH:C = '\u6700'


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->INSTANCE:Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    sget-object v3, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->INSTANCE:Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    aput-object v3, v1, v2

    sput-object v1, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->$VALUES:[Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    sput v2, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->CLA_OFFSET:I

    sput v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->INS_OFFSET:I

    const/4 v0, 0x2

    sput v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->P1_OFFSET:I

    const/4 v0, 0x3

    sput v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->P2_OFFSET:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/apduprocessing/Iso7816;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/apduprocessing/Iso7816;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->$VALUES:[Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/apduprocessing/Iso7816;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/apduprocessing/Iso7816;

    return-object v0
.end method
