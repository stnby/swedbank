.class final Lcom/mastercard/mchipengine/MChipEngine$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mchipengine/MChipEngine;->onTransactionErrorAsync(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

.field final synthetic b:Ljava/lang/Exception;

.field final synthetic c:Lcom/mastercard/mchipengine/MChipEngine;


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/MChipEngine;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mchipengine/MChipEngine$3;->c:Lcom/mastercard/mchipengine/MChipEngine;

    iput-object p2, p0, Lcom/mastercard/mchipengine/MChipEngine$3;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    iput-object p3, p0, Lcom/mastercard/mchipengine/MChipEngine$3;->b:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine$3;->c:Lcom/mastercard/mchipengine/MChipEngine;

    invoke-static {v0}, Lcom/mastercard/mchipengine/MChipEngine;->access$100(Lcom/mastercard/mchipengine/MChipEngine;)Lcom/mastercard/mchipengine/utils/MChipLogger;

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine$3;->c:Lcom/mastercard/mchipengine/MChipEngine;

    invoke-static {v0}, Lcom/mastercard/mchipengine/MChipEngine;->access$200(Lcom/mastercard/mchipengine/MChipEngine;)Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/MChipEngine$3;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    iget-object v2, p0, Lcom/mastercard/mchipengine/MChipEngine$3;->b:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;->onContactlessTransactionAbort(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V

    return-void
.end method
