.class public Lcom/mastercard/mchipengine/e/e;
.super Ljava/lang/Exception;


# instance fields
.field protected a:Lcom/mastercard/mchipengine/e/a;

.field protected b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/e/a;)V
    .locals 1

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/e/e;->a:Lcom/mastercard/mchipengine/e/a;

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/e/e;->a:Lcom/mastercard/mchipengine/e/a;

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;)V
    .locals 1

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/e/e;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/e/e;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public final b()Lcom/mastercard/mchipengine/e/a;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/e/e;->a:Lcom/mastercard/mchipengine/e/a;

    return-object v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public final d()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/e/e;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/e/e;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object v0

    :cond_0
    sget-object v0, Lcom/mastercard/mchipengine/e/e$1;->a:[I

    iget-object v1, p0, Lcom/mastercard/mchipengine/e/e;->a:Lcom/mastercard/mchipengine/e/a;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/e/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->TERMINAL_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->WALLET_CANCEL_REQUEST:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
