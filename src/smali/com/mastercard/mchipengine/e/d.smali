.class public final Lcom/mastercard/mchipengine/e/d;
.super Lcom/mastercard/mchipengine/e/e;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/e/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/e/e;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/mastercard/mchipengine/e/e;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/e/e;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;)V

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/mastercard/mchipengine/e/e;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletexceptions/TransactionDeclinedException;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/e/d;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/walletinterface/walletexceptions/TransactionDeclinedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
