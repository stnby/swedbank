.class public final Lcom/mastercard/mchipengine/b/b;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/mastercard/mchipengine/b/j;

.field public b:Lcom/mastercard/mchipengine/b/c;

.field public c:Lcom/mastercard/mchipengine/b/d;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getContactlessProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getDsrpProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->g:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_1
    :goto_0
    new-instance v0, Lcom/mastercard/mchipengine/b/j;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/b/j;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean v0, v0, Lcom/mastercard/mchipengine/b/j;->c:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/mastercard/mchipengine/b/c;

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/mastercard/mchipengine/b/c;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;Lcom/mastercard/mchipengine/b/j;Ljava/util/List;Ljava/util/List;)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    :cond_2
    iget-object p2, p0, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean p2, p2, Lcom/mastercard/mchipengine/b/j;->d:Z

    if-eqz p2, :cond_3

    new-instance p2, Lcom/mastercard/mchipengine/b/d;

    invoke-direct {p2, p1}, Lcom/mastercard/mchipengine/b/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;)V

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    :cond_3
    return-void

    :cond_4
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->M:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method
