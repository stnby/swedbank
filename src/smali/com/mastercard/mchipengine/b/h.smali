.class public final Lcom/mastercard/mchipengine/b/h;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public c:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;Ljava/util/LinkedHashMap;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/mastercard/mchipengine/b/g;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;->getPCvc3()[B

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;->getPUnAtc()[B

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;->getNAtc()[B

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;->getPCvc3()[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;->getPUnAtc()[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;->getNAtc()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    goto/16 :goto_4

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->v:Lcom/mastercard/mchipengine/e/a;

    const-string p3, "NAtc missing in Track Constructed Data"

    invoke-direct {p1, p2, p3}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->v:Lcom/mastercard/mchipengine/e/a;

    const-string p3, "PUnAtc missing in Track Constructed Data"

    invoke-direct {p1, p2, p3}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->v:Lcom/mastercard/mchipengine/e/a;

    const-string p3, "PCvc3 missing in Track Constructed Data"

    invoke-direct {p1, p2, p3}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_3
    const/16 p1, 0x101

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/b/g;

    iget-object p1, p1, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    if-eqz p1, :cond_b

    if-eqz p3, :cond_7

    const-string p2, "9F62"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_4

    const-string p2, "9F62"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/mastercard/mchipengine/d/d;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_0

    :cond_4
    move-object p2, v0

    :goto_0
    iput-object p2, p0, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string p2, "9F63"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_5

    const-string p2, "9F63"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/mastercard/mchipengine/d/d;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_1

    :cond_5
    move-object p2, v0

    :goto_1
    iput-object p2, p0, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string p2, "9F64"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_6

    const-string p2, "9F64"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/d;

    iget-object v0, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_6
    iput-object v0, p0, Lcom/mastercard/mchipengine/b/h;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    :cond_7
    const-string p2, "9F65"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_8

    const-string p2, "9F65"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/mastercard/mchipengine/d/d;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_2

    :cond_8
    move-object p2, v0

    :goto_2
    iput-object p2, p0, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string p2, "9F66"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_9

    const-string p2, "9F66"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/mastercard/mchipengine/d/d;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_3

    :cond_9
    move-object p2, v0

    :goto_3
    iput-object p2, p0, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string p2, "9F67"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_a

    const-string p2, "9F67"

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/d;

    iget-object v0, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_a
    :goto_4
    iput-object v0, p0, Lcom/mastercard/mchipengine/b/h;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_b
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mPCvc3="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", mPUnAtc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", mNAtc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/h;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v0, "MChipTrackConstructionData"

    return-object v0
.end method
