.class public final Lcom/mastercard/mchipengine/b/i;
.super Ljava/lang/Object;


# static fields
.field private static final d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final h:[B

.field private static final i:[B


# instance fields
.field public a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x6f

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/i;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x7c

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/i;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x5b

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/i;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v0, 0x2

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    sput-object v1, Lcom/mastercard/mchipengine/b/i;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-array v1, v0, [B

    fill-array-data v1, :array_1

    sput-object v1, Lcom/mastercard/mchipengine/b/i;->h:[B

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mastercard/mchipengine/b/i;->i:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0x61t
        0x38t
    .end array-data

    nop

    :array_1
    .array-data 1
        -0x61t
        0x1dt
    .end array-data

    nop

    :array_2
    .array-data 1
        -0x61t
        0x1at
    .end array-data
.end method

.method public constructor <init>([BLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/i;->j:Lcom/mastercard/mchipengine/utils/MChipLogger;

    :try_start_0
    invoke-static {p1}, Lcom/mastercard/mchipengine/d/d;->a([B)Lcom/mastercard/mchipengine/d/d;

    move-result-object p1

    iget-object v0, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    sget-object v1, Lcom/mastercard/mchipengine/b/i;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/d;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/util/LinkedHashMap;

    move-result-object p1

    sget-object v0, Lcom/mastercard/mchipengine/b/i;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Profile Payment FCI does not contain DF_NAME tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mastercard/mchipengine/b/i;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/mastercard/mchipengine/b/i;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/d;

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/i;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :goto_0
    sget-object v0, Lcom/mastercard/mchipengine/b/i;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mastercard/mchipengine/b/i;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/d;

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/d;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/util/LinkedHashMap;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/i;->b:Ljava/util/LinkedHashMap;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/i;->c:Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/mastercard/mchipengine/b/i;->a(Ljava/util/List;)V

    return-void

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->t:Lcom/mastercard/mchipengine/e/a;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Missing tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mastercard/mchipengine/b/i;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " in profile Payment Fci data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p2, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->t:Lcom/mastercard/mchipengine/e/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "First tag in Payment Fci is not "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/mastercard/mchipengine/b/i;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p2
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->j:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/i;->b:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/mastercard/mchipengine/b/i;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/d;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, v0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/b;->c([B)Ljava/util/List;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/mastercard/mchipengine/b/i;->c:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->ab:Lcom/mastercard/mchipengine/e/a;

    const-string v1, "Error when parsing profile Payment FCI PDOLs"

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/mastercard/mchipengine/b/i;->c:Ljava/util/List;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    sget-object v1, Lcom/mastercard/mchipengine/b/i;->h:[B

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/mastercard/mchipengine/b/i;->c:Ljava/util/List;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    sget-object v1, Lcom/mastercard/mchipengine/b/i;->i:[B

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/i;->c:Ljava/util/List;

    const-string v2, ","

    invoke-static {v1, v2}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    :try_start_1
    new-instance p1, Lcom/mastercard/mchipengine/d/d;

    sget-object v0, Lcom/mastercard/mchipengine/b/i;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/i;->c:Ljava/util/List;

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/b;->a(Ljava/util/List;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/i;->b:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/mastercard/mchipengine/b/i;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_1
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->ab:Lcom/mastercard/mchipengine/e/a;

    const-string v1, "Error during profile Payment FCI PDOLs serialization"

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_3
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/i;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/i;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/i;->b:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/i;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    const-string v2, ","

    invoke-static {v1, v2}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string v1, ""

    :goto_1
    aput-object v1, v0, v3

    const-string v0, "PaymentFci"

    return-object v0
.end method
