.class public final Lcom/mastercard/mchipengine/b/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/utils/Wipeable;


# static fields
.field public static final a:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# instance fields
.field public b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    nop

    :array_0
    .array-data 1
        0x5ft
        0x24t
    .end array-data
.end method

.method public constructor <init>([B)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    array-length v0, v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    array-length v0, v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    :goto_0
    const-string v0, ""

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1
    new-instance v0, Lcom/mastercard/mchipengine/e/g;

    sget-object v1, Lcom/mastercard/mchipengine/e/a;->D:Lcom/mastercard/mchipengine/e/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Expiration Date in DSRP profile data: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/utils/MChipDate;
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [B

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    aput-byte v2, v1, v3

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/a;->a([B)J

    move-result-wide v1

    long-to-int v1, v1

    new-array v2, v0, [B

    iget-object v4, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    aget-byte v4, v4, v0

    aput-byte v4, v2, v3

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/a;->a([B)J

    move-result-wide v4

    long-to-int v2, v4

    iget-object v4, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    array-length v4, v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    new-array v0, v0, [B

    iget-object v4, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    const/4 v5, 0x2

    aget-byte v4, v4, v5

    aput-byte v4, v0, v3

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/a;->a([B)J

    move-result-wide v3

    long-to-int v0, v3

    :cond_0
    new-instance v3, Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-direct {v3, v1, v2, v0}, Lcom/mastercard/mchipengine/utils/MChipDate;-><init>(III)V

    return-object v3
.end method

.method public final wipe()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/e;->b:[B

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/i;->a([B)V

    return-void
.end method
