.class public final Lcom/mastercard/mchipengine/b/a;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/mastercard/mchipengine/b/i;

.field public b:Lcom/mastercard/mchipengine/b/f;

.field public c:Lcom/mastercard/mchipengine/b/f;

.field public d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public g:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>([B[B[B[B[B[BLjava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B[B[B[B[B[B",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mastercard/mchipengine/b/i;

    invoke-direct {v0, p1, p7}, Lcom/mastercard/mchipengine/b/i;-><init>([BLjava/util/List;)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/a;->a:Lcom/mastercard/mchipengine/b/i;

    new-instance p1, Lcom/mastercard/mchipengine/b/f;

    const/4 p7, 0x0

    invoke-direct {p1, p2, p7}, Lcom/mastercard/mchipengine/b/f;-><init>([BZ)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/a;->b:Lcom/mastercard/mchipengine/b/f;

    new-instance p1, Lcom/mastercard/mchipengine/b/f;

    const/4 p7, 0x1

    invoke-direct {p1, p2, p7}, Lcom/mastercard/mchipengine/b/f;-><init>([BZ)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/a;->c:Lcom/mastercard/mchipengine/b/f;

    if-nez p3, :cond_1

    if-eqz p8, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->r:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_1
    :goto_0
    if-eqz p4, :cond_5

    invoke-static {p4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz p5, :cond_2

    invoke-static {p5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/a;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_2
    if-eqz p6, :cond_3

    invoke-static {p6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/a;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_3
    if-eqz p3, :cond_4

    invoke-static {p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/a;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_4
    return-void

    :cond_5
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->q:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method
