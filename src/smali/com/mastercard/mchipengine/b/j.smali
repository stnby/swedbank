.class public final Lcom/mastercard/mchipengine/b/j;
.super Ljava/lang/Object;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getContactlessProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getContactlessProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object v0

    iput-boolean v1, p0, Lcom/mastercard/mchipengine/b/j;->c:Z

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->isPrimaryAidMchipDataValid()Z

    move-result v2

    iput-boolean v2, p0, Lcom/mastercard/mchipengine/b/j;->a:Z

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->isAlternateAidMchipDataValid()Z

    move-result v2

    iput-boolean v2, p0, Lcom/mastercard/mchipengine/b/j;->b:Z

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->isMagstripeDataValid()Z

    move-result v2

    iput-boolean v2, p0, Lcom/mastercard/mchipengine/b/j;->e:Z

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->isTransitSupported()Z

    move-result v2

    iput-boolean v2, p0, Lcom/mastercard/mchipengine/b/j;->g:Z

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->isUsAipMaskSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/mchipengine/b/j;->h:Z

    :cond_0
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getDsrpProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/mastercard/mchipengine/b/j;->d:Z

    :cond_1
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->isTransactionIdRequired()Z

    move-result p1

    iput-boolean p1, p0, Lcom/mastercard/mchipengine/b/j;->f:Z

    return-void
.end method
