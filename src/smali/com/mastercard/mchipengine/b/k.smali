.class public abstract Lcom/mastercard/mchipengine/b/k;
.super Ljava/lang/Object;


# instance fields
.field public k:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field m:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

.field n:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

.field o:Z

.field protected p:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field protected q:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

.field protected r:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

.field protected s:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/k;->s:Lcom/mastercard/mchipengine/utils/MChipLogger;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->getCardCountryCode()[B

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->getCardCountryCode()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/k;->k:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->getPan()[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->getPan()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "F"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->getPan()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/k;->l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/k;->m:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/k;->n:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;->isTransactionIdRequired()Z

    move-result p1

    iput-boolean p1, p0, Lcom/mastercard/mchipengine/b/k;->o:Z

    return-void

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->B:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->I:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/k;->p:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-object v0
.end method

.method public final b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/k;->q:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    return-object v0
.end method

.method public final c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/k;->r:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0
.end method
