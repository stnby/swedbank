.class public final Lcom/mastercard/mchipengine/b/f;
.super Ljava/lang/Object;


# static fields
.field private static final c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final e:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# instance fields
.field public a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public b:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x77

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/f;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x7e

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/f;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, -0x6c

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/f;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method

.method public constructor <init>([BZ)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_5

    :try_start_0
    invoke-static {p1}, Lcom/mastercard/mchipengine/d/d;->a([B)Lcom/mastercard/mchipengine/d/d;

    move-result-object p1

    iget-object v0, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    sget-object v1, Lcom/mastercard/mchipengine/b/f;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/d;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/util/LinkedHashMap;

    move-result-object p1

    sget-object v0, Lcom/mastercard/mchipengine/b/f;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/d;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/f;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/f;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/f;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v0, 0x1

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v1, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBit(IIZ)V

    :cond_0
    sget-object p2, Lcom/mastercard/mchipengine/b/f;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/d;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/f;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->K:Lcom/mastercard/mchipengine/e/a;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Card Profile GPO Response does not contain tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mastercard/mchipengine/b/f;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->K:Lcom/mastercard/mchipengine/e/a;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Card Profile GPO Response AIP has incorrect length ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/f;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "), expected 2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->K:Lcom/mastercard/mchipengine/e/a;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Card Profile GPO Response does not contain tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mastercard/mchipengine/b/f;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->K:Lcom/mastercard/mchipengine/e/a;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Card Profile GPO Response does not contain tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mastercard/mchipengine/b/f;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->K:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_5
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->s:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method
