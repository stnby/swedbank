.class public final Lcom/mastercard/mchipengine/b/g;
.super Ljava/lang/Object;


# static fields
.field private static final f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private static final i:[B


# instance fields
.field a:Ljava/lang/Integer;

.field public b:Z

.field public c:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x70

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/b/g;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v0, 0x2

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    sput-object v1, Lcom/mastercard/mchipengine/b/g;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v1, -0x74

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    sput-object v1, Lcom/mastercard/mchipengine/b/g;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mastercard/mchipengine/b/g;->i:[B

    return-void

    :array_0
    .array-data 1
        -0x61t
        0x69t
    .end array-data

    nop

    :array_1
    .array-data 1
        -0x61t
        0x6at
    .end array-data
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/b/c;Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/b/c;",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/g;->j:Lcom/mastercard/mchipengine/utils/MChipLogger;

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;->getRecordNumber()B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;->getSfi()B

    move-result v0

    if-ne v0, v1, :cond_0

    iput-boolean v1, p0, Lcom/mastercard/mchipengine/b/g;->b:Z

    :cond_0
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;->getRecordNumber()B

    move-result v0

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;->getSfi()B

    move-result v2

    invoke-static {v0, v2}, Lcom/mastercard/mchipengine/b/g;->a(BB)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/g;->a:Ljava/lang/Integer;

    const/4 v0, 0x3

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;->getRecordNumber()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;->getSfi()B

    move-result v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->a:Ljava/lang/Integer;

    aput-object v2, v0, v1

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;->getRecordValue()[B

    move-result-object p2

    invoke-direct {p0, p1, p2, p3}, Lcom/mastercard/mchipengine/b/g;->a(Lcom/mastercard/mchipengine/b/c;[BLjava/util/List;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/mastercard/mchipengine/e/g;

    sget-object p3, Lcom/mastercard/mchipengine/e/a;->L:Lcom/mastercard/mchipengine/e/a;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/a/d;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p2
.end method

.method public static a(BB)Ljava/lang/Integer;
    .locals 0

    mul-int/lit16 p0, p0, 0x100

    add-int/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/mastercard/mchipengine/b/c;[BLjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/b/c;",
            "[B",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2}, Lcom/mastercard/mchipengine/d/d;->a([B)Lcom/mastercard/mchipengine/d/d;

    move-result-object p2

    iget-object v0, p2, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    sget-object v1, Lcom/mastercard/mchipengine/b/g;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {p2}, Lcom/mastercard/mchipengine/d/d;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/util/LinkedHashMap;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    iget-boolean p2, p0, Lcom/mastercard/mchipengine/b/g;->b:Z

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    sget-object v2, Lcom/mastercard/mchipengine/b/g;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    sget-object v2, Lcom/mastercard/mchipengine/b/g;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/mastercard/mchipengine/d/d;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/b;->c([B)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    invoke-interface {v2, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    invoke-interface {v2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    new-instance p3, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    sget-object v2, Lcom/mastercard/mchipengine/b/g;->i:[B

    const/4 v3, 0x4

    invoke-direct {p3, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    iput-object p2, p1, Lcom/mastercard/mchipengine/b/c;->i:Ljava/util/List;

    new-array p1, v1, [Ljava/lang/Object;

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    const-string p3, ","

    invoke-static {p2, p3}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v0

    goto :goto_1

    :cond_3
    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    sget-object p3, Lcom/mastercard/mchipengine/b/g;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2, p3}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    sget-object p3, Lcom/mastercard/mchipengine/b/g;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2, p3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/mastercard/mchipengine/d/d;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/b;->c([B)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    new-array p2, v1, [Ljava/lang/Object;

    iget-object p3, p0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    const-string v2, ","

    invoke-static {p3, v2}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p3

    aput-object p3, p2, v0

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    iput-object p2, p1, Lcom/mastercard/mchipengine/b/c;->h:Ljava/util/List;

    :cond_4
    :goto_1
    new-array p1, v1, [Ljava/lang/Object;

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {p2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object p2

    const-string p3, ","

    invoke-static {p2, p3}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v0

    return-void

    :cond_5
    new-instance p1, Lcom/mastercard/mchipengine/d/a/c;

    const-string p2, "Invalid Record profile data"

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/d/a/c;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->a:Ljava/lang/Integer;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/mastercard/mchipengine/b/g;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->e:Ljava/util/List;

    const-string v4, ","

    invoke-static {v2, v4}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/g;->d:Ljava/util/List;

    const-string v4, ","

    invoke-static {v2, v4}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    const-string v2, ","

    invoke-static {v1, v2}, Lcom/mastercard/mchipengine/utils/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    const-string v1, ""

    :goto_2
    aput-object v1, v0, v3

    const-string v0, "MChipRecord"

    return-object v0
.end method
