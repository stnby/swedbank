.class public final Lcom/mastercard/mchipengine/b/c;
.super Lcom/mastercard/mchipengine/b/k;


# instance fields
.field public a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public b:Lcom/mastercard/mchipengine/b/a;

.field public c:Lcom/mastercard/mchipengine/b/a;

.field public d:Lcom/mastercard/mchipengine/b/h;

.field public e:Lcom/mastercard/mchipengine/b/h;

.field public f:I

.field public g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/mastercard/mchipengine/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;Lcom/mastercard/mchipengine/b/j;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;",
            "Lcom/mastercard/mchipengine/b/j;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/b/k;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getContactlessProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getPpseFci()[B

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getPpseFci()[B

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_e

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getPpseFci()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string v2, "9F1D"

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string v2, "9F1A"

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p4, :cond_3

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {p3, p4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    new-instance p4, Lcom/mastercard/mchipengine/b/a;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getPaymentFci()[B

    move-result-object v2

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getGpoResponse()[B

    move-result-object v3

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v4

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getAid()[B

    move-result-object v5

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getDeclineConditions()[B

    move-result-object v6

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getDeclineConditionsOnPpms()[B

    move-result-object v7

    const/4 v9, 0x1

    move-object v1, p4

    move-object v8, v0

    invoke-direct/range {v1 .. v9}, Lcom/mastercard/mchipengine/b/a;-><init>([B[B[B[B[B[BLjava/util/List;Z)V

    iput-object p4, p0, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    iget-boolean p4, p2, Lcom/mastercard/mchipengine/b/j;->b:Z

    if-eqz p4, :cond_5

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;

    move-result-object p2

    if-eqz p2, :cond_4

    new-instance p4, Lcom/mastercard/mchipengine/b/a;

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;->getPaymentFci()[B

    move-result-object v2

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;->getGpoResponse()[B

    move-result-object v3

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;->getCvrMaskAnd()[B

    move-result-object v4

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;->getAid()[B

    move-result-object v5

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;->getDeclineConditions()[B

    move-result-object v6

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;->getDeclineConditionsOnPpms()[B

    move-result-object v7

    const/4 v9, 0x0

    move-object v1, p4

    move-object v8, v0

    invoke-direct/range {v1 .. v9}, Lcom/mastercard/mchipengine/b/a;-><init>([B[B[B[B[B[BLjava/util/List;Z)V

    iput-object p4, p0, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    goto :goto_1

    :cond_4
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->N:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_5
    iget-boolean p4, p2, Lcom/mastercard/mchipengine/b/j;->a:Z

    if-nez p4, :cond_7

    iget-boolean p2, p2, Lcom/mastercard/mchipengine/b/j;->e:Z

    if-eqz p2, :cond_6

    goto :goto_1

    :cond_6
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->m:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_7
    :goto_1
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/c;->j:Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getRecords()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_8

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;

    new-instance v0, Lcom/mastercard/mchipengine/b/g;

    invoke-direct {v0, p0, p4, p3}, Lcom/mastercard/mchipengine/b/g;-><init>(Lcom/mastercard/mchipengine/b/c;Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;Ljava/util/List;)V

    iget-object p4, p0, Lcom/mastercard/mchipengine/b/c;->j:Ljava/util/LinkedHashMap;

    iget-object v1, v0, Lcom/mastercard/mchipengine/b/g;->a:Ljava/lang/Integer;

    invoke-virtual {p4, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_8
    new-instance p2, Lcom/mastercard/mchipengine/b/h;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getTrack1ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    move-result-object p3

    iget-object p4, p0, Lcom/mastercard/mchipengine/b/c;->j:Ljava/util/LinkedHashMap;

    const/4 v0, 0x1

    invoke-direct {p2, p3, p4, v0}, Lcom/mastercard/mchipengine/b/h;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;Ljava/util/LinkedHashMap;Z)V

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/c;->d:Lcom/mastercard/mchipengine/b/h;

    new-instance p2, Lcom/mastercard/mchipengine/b/h;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getTrack2ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    move-result-object p3

    iget-object p4, p0, Lcom/mastercard/mchipengine/b/c;->j:Ljava/util/LinkedHashMap;

    const/4 v0, 0x0

    invoke-direct {p2, p3, p4, v0}, Lcom/mastercard/mchipengine/b/h;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;Ljava/util/LinkedHashMap;Z)V

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/c;->e:Lcom/mastercard/mchipengine/b/h;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getIssuerApplicationData()[B

    move-result-object p2

    if-eqz p2, :cond_d

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getIssuerApplicationData()[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/c;->p:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/c;->p:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p2

    const/16 p3, 0x12

    if-lt p2, p3, :cond_c

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getCdol1RelatedDataLength()[B

    move-result-object p2

    if-eqz p2, :cond_b

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getCdol1RelatedDataLength()[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_9

    const-string p2, "00"

    :cond_9
    const/16 p3, 0x10

    invoke-static {p2, p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result p2

    iput p2, p0, Lcom/mastercard/mchipengine/b/c;->f:I

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getPinIvCvc3Track2()[B

    move-result-object p2

    if-eqz p2, :cond_a

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getPinIvCvc3Track2()[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/c;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object p2

    iput-object p2, p0, Lcom/mastercard/mchipengine/b/c;->q:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;->getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/c;->r:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-void

    :cond_a
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->A:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_b
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->O:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_c
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->P:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_d
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->y:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_e
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->z:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method


# virtual methods
.method public final a(BB)Lcom/mastercard/mchipengine/b/g;
    .locals 0

    invoke-static {p1, p2}, Lcom/mastercard/mchipengine/b/g;->a(BB)Ljava/lang/Integer;

    move-result-object p1

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/c;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {p2, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/mastercard/mchipengine/b/c;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {p2, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/b/g;

    return-object p1

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/e/c;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->ak:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/c;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method
