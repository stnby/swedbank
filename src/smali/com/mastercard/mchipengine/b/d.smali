.class public final Lcom/mastercard/mchipengine/b/d;
.super Lcom/mastercard/mchipengine/b/k;


# instance fields
.field public a:Lcom/mastercard/mchipengine/b/e;

.field public b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

.field public final i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public final j:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;)V
    .locals 2

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/b/k;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;)V

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;->getDsrpProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getIssuerApplicationData()[B

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getIssuerApplicationData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->p:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v0, p0, Lcom/mastercard/mchipengine/b/d;->p:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    const/16 v1, 0x12

    if-lt v0, v1, :cond_8

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getExpirationDate()[B

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getExpirationDate()[B

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_7

    new-instance v0, Lcom/mastercard/mchipengine/b/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getExpirationDate()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/b/e;-><init>([B)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->a:Lcom/mastercard/mchipengine/b/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getTrack2EquivalentData()[B

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getTrack2EquivalentData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getAip()[B

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getAip()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getPanSequenceNumber()[B

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getPanSequenceNumber()[B

    move-result-object v0

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    const/16 v1, 0x9

    if-gt v0, v1, :cond_3

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getPanSequenceNumber()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getCvrMaskAnd()[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getCvrMaskAnd()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_0
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getDeclineConditions()[B

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getDeclineConditions()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_1
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getPaymentAccountReference()[B

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getPaymentAccountReference()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :cond_2
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->q:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/b/d;->r:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;->getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/d;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    const-string p1, "FF0000000000"

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/d;->i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string p1, "FF0000000000"

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/b/d;->j:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    :cond_3
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->G:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_4
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->G:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_5
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->F:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_6
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->E:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_7
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->D:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_8
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->P:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_9
    new-instance p1, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->y:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/k;->k:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/k;->l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/k;->m:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/k;->n:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/mastercard/mchipengine/b/k;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->a:Lcom/mastercard/mchipengine/b/e;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/b/e;->a()Lcom/mastercard/mchipengine/utils/MChipDate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipDate;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/b/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/b/d;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    aput-object v1, v0, v3

    const-string v0, "DsrpProfileData"

    return-object v0
.end method
