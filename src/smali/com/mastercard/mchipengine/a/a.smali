.class public final Lcom/mastercard/mchipengine/a/a;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/mastercard/mchipengine/a/b; = null

.field public static e:J = -0x1L


# instance fields
.field public b:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

.field public c:Lcom/mastercard/mchipengine/g/a/a;

.field public d:Z

.field private f:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/a/a;->f:Lcom/mastercard/mchipengine/utils/MChipLogger;

    iput-object p1, p0, Lcom/mastercard/mchipengine/a/a;->b:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    new-instance p1, Lcom/mastercard/mchipengine/g/a/b;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/g/a/b;-><init>()V

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/a/b;->a:Lcom/mastercard/mchipengine/g/a/a;

    iput-object p1, p0, Lcom/mastercard/mchipengine/a/a;->c:Lcom/mastercard/mchipengine/g/a/a;

    sget-object p1, Lcom/mastercard/mchipengine/a/a;->a:Lcom/mastercard/mchipengine/a/b;

    if-nez p1, :cond_0

    new-instance p1, Lcom/mastercard/mchipengine/a/b;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/a/b;-><init>()V

    sput-object p1, Lcom/mastercard/mchipengine/a/a;->a:Lcom/mastercard/mchipengine/a/b;

    :cond_0
    return-void
.end method

.method public static a()V
    .locals 4

    sget-object v0, Lcom/mastercard/mchipengine/a/a;->a:Lcom/mastercard/mchipengine/a/b;

    iget v1, v0, Lcom/mastercard/mchipengine/a/b;->a:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, v0, Lcom/mastercard/mchipengine/a/b;->a:I

    new-array v1, v2, [Ljava/lang/Object;

    iget v0, v0, Lcom/mastercard/mchipengine/a/b;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v1, v3

    new-array v0, v2, [Ljava/lang/Object;

    sget-object v1, Lcom/mastercard/mchipengine/a/a;->a:Lcom/mastercard/mchipengine/a/b;

    iget v1, v1, Lcom/mastercard/mchipengine/a/b;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    return-void
.end method
