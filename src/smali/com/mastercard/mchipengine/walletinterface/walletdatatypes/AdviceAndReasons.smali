.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAdvice()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;
.end method

.method public abstract getReasons()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation
.end method
