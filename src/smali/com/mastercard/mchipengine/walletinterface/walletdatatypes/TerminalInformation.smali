.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getCdolValues()[B
.end method

.method public abstract getDiscretionaryDataByTag(Ljava/lang/String;)[B
.end method

.method public abstract getMerchantAndLocation()[B
.end method

.method public abstract getPdolValues()[B
.end method

.method public abstract getTerminalTechnology()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;
.end method

.method public abstract getTerminalType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;
.end method

.method public abstract isCdCvmSupported()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
.end method

.method public abstract isTwoTapSupported()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
.end method
