.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAmount()[B
.end method

.method public abstract getCountryCode()[B
.end method

.method public abstract getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;
.end method

.method public abstract getCurrencyCode()[B
.end method

.method public abstract getTransactionDate()[B
.end method

.method public abstract getTransactionType()B
.end method

.method public abstract getUnpredictableNumber()[B
.end method
