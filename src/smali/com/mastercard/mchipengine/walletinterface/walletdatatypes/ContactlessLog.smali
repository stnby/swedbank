.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getTerminalInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;
.end method

.method public abstract getTransactionId()[B
.end method

.method public abstract getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;
.end method

.method public abstract getTransactionOutcome()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;
.end method
