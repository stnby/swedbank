.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAuthorizedAmount()[B
.end method

.method public abstract getConditionsOfUse()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;
.end method

.method public abstract getCurrencyCode()[B
.end method

.method public abstract getExpectedUserActionOnPoi()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
.end method

.method public abstract getOtherAmount()[B
.end method

.method public abstract getPurpose()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;
.end method

.method public abstract getRichTransactionType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;
.end method

.method public abstract getTransactionRange()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;
.end method

.method public abstract hasTerminalRequestedCdCvm()Z
.end method

.method public abstract isAlternateAidUsed()Z
.end method
