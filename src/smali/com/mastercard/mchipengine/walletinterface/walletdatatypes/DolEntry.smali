.class public Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;
.super Ljava/lang/Object;


# instance fields
.field private final mLength:B

.field private final mTag:[B


# direct methods
.method public constructor <init>([BB)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->mTag:[B

    iput-byte p2, p0, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->mLength:B

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getTag()[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getTag()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getLength()B
    .locals 1

    iget-byte v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->mLength:B

    return v0
.end method

.method public getTag()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->mTag:[B

    return-object v0
.end method
