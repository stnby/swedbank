.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

.field public static final enum CONTACTLESS_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

.field public static final enum CONTACTLESS_MAGSTRIPE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

.field public static final enum DSRP_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

.field public static final enum DSRP_UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    const-string v1, "CONTACTLESS_EMV"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->CONTACTLESS_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    const-string v1, "CONTACTLESS_MAGSTRIPE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->CONTACTLESS_MAGSTRIPE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    const-string v1, "DSRP_EMV"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->DSRP_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    const-string v1, "DSRP_UCAF"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->DSRP_UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->CONTACTLESS_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->CONTACTLESS_MAGSTRIPE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->DSRP_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->DSRP_UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    return-object v0
.end method
