.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

.field public static final enum AUTHENTICATE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

.field public static final enum AUTHORIZE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

.field public static final enum UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    const-string v1, "AUTHORIZE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHORIZE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    const-string v1, "AUTHENTICATE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHENTICATE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    const-string v1, "UNKNOWN"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHORIZE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHENTICATE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    return-object v0
.end method
