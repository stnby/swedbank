.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum BANK_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum BANK_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum BANK_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum BANK_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum BANK_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum BANK_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum CARDHOLDER_OPERATED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum CARDHOLDER_OPERATED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum CARDHOLDER_OPERATED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum MERCHANT_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum MERCHANT_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum MERCHANT_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum MERCHANT_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum MERCHANT_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum MERCHANT_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field public static final enum UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "BANK_ATTENDED_ONLINE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "BANK_ATTENDED_OFFLINE_ONLINE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "BANK_ATTENDED_OFFLINE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "BANK_UNATTENDED_ONLINE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "BANK_UNATTENDED_OFFLINE_ONLINE"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "BANK_UNATTENDED_OFFLINE"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "MERCHANT_ATTENDED_ONLINE"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "MERCHANT_ATTENDED_OFFLINE_ONLINE"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "MERCHANT_ATTENDED_OFFLINE"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "MERCHANT_UNATTENDED_ONLINE"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "MERCHANT_UNATTENDED_OFFLINE_ONLINE"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "MERCHANT_UNATTENDED_OFFLINE"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "CARDHOLDER_OPERATED_ONLINE"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "CARDHOLDER_OPERATED_OFFLINE_ONLINE"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "CARDHOLDER_OPERATED_OFFLINE"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const-string v1, "UNKNOWN"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    aput-object v1, v0, v14

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    return-object v0
.end method
