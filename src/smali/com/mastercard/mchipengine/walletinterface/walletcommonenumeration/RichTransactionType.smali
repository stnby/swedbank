.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

.field public static final enum CASH:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

.field public static final enum PURCHASE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

.field public static final enum PURCHASE_WITH_CASHBACK:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

.field public static final enum REFUND:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

.field public static final enum TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

.field public static final enum UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    const-string v1, "PURCHASE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->PURCHASE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    const-string v1, "REFUND"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->REFUND:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    const-string v1, "CASH"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->CASH:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    const-string v1, "TRANSIT"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    const-string v1, "PURCHASE_WITH_CASHBACK"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->PURCHASE_WITH_CASHBACK:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    const-string v1, "UNKNOWN"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->PURCHASE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->REFUND:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->CASH:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->PURCHASE_WITH_CASHBACK:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object v0
.end method
