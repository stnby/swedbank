.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum CONTEXT_NOT_MATCHING:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum CREDENTIALS_NOT_ACCESSIBLE_WITHOUT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum CREDENTIALS_NOT_AVAILABLE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum INSUFFICIENT_POI_AUTHENTICATION:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum MISSING_CONSENT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

.field public static final enum UNSUPPORTED_TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "CONTEXT_NOT_MATCHING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CONTEXT_NOT_MATCHING:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "CREDENTIALS_NOT_ACCESSIBLE_WITHOUT_CDCVM"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_ACCESSIBLE_WITHOUT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "CREDENTIALS_NOT_AVAILABLE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_AVAILABLE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "INSUFFICIENT_CDCVM"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "INSUFFICIENT_POI_AUTHENTICATION"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_POI_AUTHENTICATION:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "MISSING_CDCVM"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "MISSING_CONSENT"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CONSENT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "TRANSACTION_CONDITIONS_NOT_ALLOWED"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const-string v1, "UNSUPPORTED_TRANSIT"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->UNSUPPORTED_TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CONTEXT_NOT_MATCHING:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_ACCESSIBLE_WITHOUT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_AVAILABLE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_POI_AUTHENTICATION:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CONSENT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v8

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v9

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->UNSUPPORTED_TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    aput-object v1, v0, v10

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    return-object v0
.end method
