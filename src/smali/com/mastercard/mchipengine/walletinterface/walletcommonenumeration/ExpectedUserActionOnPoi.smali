.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

.field public static final enum NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

.field public static final enum ONLINE_PIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

.field public static final enum ONLINE_PIN_OR_SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

.field public static final enum SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

.field public static final enum UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    const-string v1, "ONLINE_PIN"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->ONLINE_PIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    const-string v1, "SIGNATURE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    const-string v1, "ONLINE_PIN_OR_SIGNATURE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->ONLINE_PIN_OR_SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    const-string v1, "UNKNOWN"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->ONLINE_PIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->ONLINE_PIN_OR_SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forDsrp()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object v0
.end method

.method public static forMChip(Lcom/mastercard/mchipengine/d/b/a/d;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 2

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mchipengine/d/b/a/d$a;->g:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mchipengine/d/b/a/d$a;->c:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->ONLINE_PIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_1
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->e()I

    move-result v0

    sget v1, Lcom/mastercard/mchipengine/d/b/a/d$a;->d:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->d()Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_2
    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_3
    :goto_0
    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0
.end method

.method public static forMagstripe(Lcom/mastercard/mchipengine/d/b/a/j;Lcom/mastercard/mchipengine/d/b/a/l;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/b/a/j;->c()Z

    move-result p0

    if-eqz p0, :cond_0

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_0
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/l;->h()Z

    move-result p0

    if-nez p0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/l;->c()Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->ONLINE_PIN_OR_SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_2
    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/l;->d()Z

    move-result p0

    if-eqz p0, :cond_3

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->ONLINE_PIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_3
    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/l;->e()Z

    move-result p0

    if-eqz p0, :cond_4

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->SIGNATURE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_4
    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0

    :cond_5
    :goto_0
    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object v0
.end method
