.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

.field public static final enum V0:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

.field public static final enum V0_PLUS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    const-string v1, "V0"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    const-string v1, "V0_PLUS"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0_PLUS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0_PLUS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    return-object v0
.end method
