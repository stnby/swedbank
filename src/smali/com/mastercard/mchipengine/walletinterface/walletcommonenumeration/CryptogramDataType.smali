.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

.field public static final enum DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

.field public static final enum UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    const-string v1, "DE55"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    const-string v1, "UCAF"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-object v0
.end method
