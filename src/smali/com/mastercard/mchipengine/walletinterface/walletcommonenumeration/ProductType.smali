.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

.field public static final enum COMMERCIAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

.field public static final enum CREDIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

.field public static final enum DEBIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

.field public static final enum PREPAID:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

.field public static final enum UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    const-string v1, "CREDIT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->CREDIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    const-string v1, "DEBIT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->DEBIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    const-string v1, "COMMERCIAL"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->COMMERCIAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    const-string v1, "PREPAID"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->PREPAID:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    const-string v1, "UNKNOWN"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->CREDIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->DEBIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->COMMERCIAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->PREPAID:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0
.end method
