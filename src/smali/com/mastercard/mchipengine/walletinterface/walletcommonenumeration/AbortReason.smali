.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

.field public static final enum CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

.field public static final enum TERMINAL_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

.field public static final enum WALLET_CANCEL_REQUEST:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    const-string v1, "WALLET_CANCEL_REQUEST"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->WALLET_CANCEL_REQUEST:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    const-string v1, "CARD_ERROR"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    const-string v1, "TERMINAL_ERROR"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->TERMINAL_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->WALLET_CANCEL_REQUEST:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->TERMINAL_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    return-object v0
.end method
