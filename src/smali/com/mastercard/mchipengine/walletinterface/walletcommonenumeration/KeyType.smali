.class public final enum Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

.field public static final enum MD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

.field public static final enum RANDOM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

.field public static final enum UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    const-string v1, "RANDOM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->RANDOM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    const-string v1, "UMD"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    const-string v1, "MD"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->MD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->RANDOM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->MD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->$VALUES:[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    return-object v0
.end method
