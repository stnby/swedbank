.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;
.end method

.method public abstract getContactlessProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
.end method

.method public abstract getDsrpProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
.end method
