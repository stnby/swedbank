.class Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->withRecords(BB[B)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

.field final synthetic val$recordNumber:B

.field final synthetic val$recordValue:[B

.field final synthetic val$sfi:B


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;BB[B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    iput-byte p2, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;->val$recordNumber:B

    iput-byte p3, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;->val$sfi:B

    iput-object p4, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;->val$recordValue:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRecordNumber()B
    .locals 1

    iget-byte v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;->val$recordNumber:B

    return v0
.end method

.method public getRecordValue()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;->val$recordValue:[B

    return-object v0
.end method

.method public getSfi()B
    .locals 1

    iget-byte v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;->val$sfi:B

    return v0
.end method
