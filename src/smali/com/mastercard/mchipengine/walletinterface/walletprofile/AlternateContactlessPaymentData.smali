.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAid()[B
.end method

.method public abstract getCvrMaskAnd()[B
.end method

.method public abstract getDeclineConditions()[B
.end method

.method public abstract getDeclineConditionsOnPpms()[B
.end method

.method public abstract getGpoResponse()[B
.end method

.method public abstract getPaymentFci()[B
.end method
