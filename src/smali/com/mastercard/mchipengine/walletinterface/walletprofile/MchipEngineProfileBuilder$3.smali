.class Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->withAlternateContactlessPaymentData([B[B[B[B[B[B)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

.field final synthetic val$aid:[B

.field final synthetic val$cvrMaskAnd:[B

.field final synthetic val$declineConditions:[B

.field final synthetic val$declineConditionsOnPpms:[B

.field final synthetic val$gpoResponse:[B

.field final synthetic val$paymentFci:[B


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B[B[B[B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    iput-object p2, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$paymentFci:[B

    iput-object p3, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$gpoResponse:[B

    iput-object p4, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$cvrMaskAnd:[B

    iput-object p5, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$declineConditions:[B

    iput-object p6, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$declineConditionsOnPpms:[B

    iput-object p7, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$aid:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$aid:[B

    return-object v0
.end method

.method public getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$cvrMaskAnd:[B

    return-object v0
.end method

.method public getDeclineConditions()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$declineConditions:[B

    return-object v0
.end method

.method public getDeclineConditionsOnPpms()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$declineConditionsOnPpms:[B

    return-object v0
.end method

.method public getGpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$gpoResponse:[B

    return-object v0
.end method

.method public getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;->val$paymentFci:[B

    return-object v0
.end method
