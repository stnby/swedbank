.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAid()[B
.end method

.method public abstract getAlternateContactlessPaymentData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;
.end method

.method public abstract getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
.end method

.method public abstract getCdol1RelatedDataLength()[B
.end method

.method public abstract getCvrMaskAnd()[B
.end method

.method public abstract getDeclineConditions()[B
.end method

.method public abstract getDeclineConditionsOnPpms()[B
.end method

.method public abstract getGpoResponse()[B
.end method

.method public abstract getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;
.end method

.method public abstract getIssuerApplicationData()[B
.end method

.method public abstract getPaymentFci()[B
.end method

.method public abstract getPinIvCvc3Track2()[B
.end method

.method public abstract getPpseFci()[B
.end method

.method public abstract getRecords()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTrack1ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
.end method

.method public abstract getTrack2ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
.end method

.method public abstract isAlternateAidMchipDataValid()Z
.end method

.method public abstract isMagstripeDataValid()Z
.end method

.method public abstract isPrimaryAidMchipDataValid()Z
.end method

.method public abstract isTransitSupported()Z
.end method

.method public abstract isUsAipMaskSupported()Z
.end method
