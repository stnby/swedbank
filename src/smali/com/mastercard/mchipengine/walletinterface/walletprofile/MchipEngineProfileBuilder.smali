.class public Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
.super Ljava/lang/Object;


# instance fields
.field private mAlternateContactlessPaymentData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;

.field private mCommonData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

.field private mContactlessPaymentData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

.field private mDsrpData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

.field private mRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;",
            ">;"
        }
    .end annotation
.end field

.field private mTrack1ConstructionData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

.field private mTrack2ConstructionData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mRecords:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mAlternateContactlessPaymentData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;

    return-object p0
.end method

.method static synthetic access$100(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mRecords:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$200(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mTrack1ConstructionData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    return-object p0
.end method

.method static synthetic access$300(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mTrack2ConstructionData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    return-object p0
.end method

.method static synthetic access$400(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mContactlessPaymentData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    return-object p0
.end method

.method static synthetic access$500(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mDsrpData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    return-object p0
.end method

.method static synthetic access$600(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mCommonData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

    return-object p0
.end method


# virtual methods
.method public buildProfile()Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$8;

    invoke-direct {v0, p0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$8;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)V

    return-object v0
.end method

.method public withAlternateContactlessPaymentData([B[B[B[B[B[B)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
    .locals 9

    new-instance v8, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$3;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B[B[B[B)V

    iput-object v8, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mAlternateContactlessPaymentData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;

    return-object p0
.end method

.method public withCommonProfileData([B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;Z)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
    .locals 8

    new-instance v7, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;Z)V

    iput-object v7, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mCommonData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

    return-object p0
.end method

.method public withContactlessPaymentData([B[B[B[B[B[B[B[B[B[BZZZZZLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move/from16 v15, p14

    move/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, v1

    new-instance v1, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;

    move-object v0, v1

    move-object/from16 v20, v1

    move-object/from16 v1, v19

    invoke-direct/range {v0 .. v18}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B[B[B[B[B[B[B[BZZZZZLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)V

    move-object/from16 v1, v20

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mContactlessPaymentData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    return-object v0
.end method

.method public withDsrpData([B[B[B[B[B[B[B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
    .locals 15

    move-object v13, p0

    new-instance v14, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p8

    invoke-direct/range {v0 .. v12}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B[B[B[B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;[B)V

    iput-object v14, v13, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mDsrpData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    return-object v13
.end method

.method public withRecords(BB[B)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mRecords:Ljava/util/List;

    new-instance v1, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$4;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;BB[B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public withTrack1ConstructionData([B[B[B)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$5;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$5;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mTrack1ConstructionData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    return-object p0
.end method

.method public withTrack2ConstructionData([B[B[B)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$6;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$6;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->mTrack2ConstructionData:Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    return-object p0
.end method
