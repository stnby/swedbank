.class Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->withContactlessPaymentData([B[B[B[B[B[B[B[B[B[BZZZZZLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

.field final synthetic val$aid:[B

.field final synthetic val$cdCvmModel:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

.field final synthetic val$cdol1RelatedData:[B

.field final synthetic val$cvrMaskAnd:[B

.field final synthetic val$declineCondition:[B

.field final synthetic val$declineConditionOnPpms:[B

.field final synthetic val$gpoResponse:[B

.field final synthetic val$hostUmdConfig:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

.field final synthetic val$isAlternateAidMchipDataValid:Z

.field final synthetic val$isMagstripeDataValid:Z

.field final synthetic val$isPrimaryAidMchipDataValid:Z

.field final synthetic val$isTransitSupported:Z

.field final synthetic val$isUsAipMaskSupported:Z

.field final synthetic val$issuerApplicationData:[B

.field final synthetic val$paymentFci:[B

.field final synthetic val$pinIvCvc3Track2:[B

.field final synthetic val$ppseFci:[B


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B[B[B[B[B[B[B[BZZZZZLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)V
    .locals 2

    move-object v0, p0

    move-object v1, p1

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    move-object v1, p2

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$ppseFci:[B

    move-object v1, p3

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$gpoResponse:[B

    move-object v1, p4

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$paymentFci:[B

    move-object v1, p5

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$cvrMaskAnd:[B

    move-object v1, p6

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$declineCondition:[B

    move-object v1, p7

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$declineConditionOnPpms:[B

    move-object v1, p8

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$aid:[B

    move-object v1, p9

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$issuerApplicationData:[B

    move-object v1, p10

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$pinIvCvc3Track2:[B

    move-object v1, p11

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$cdol1RelatedData:[B

    move v1, p12

    iput-boolean v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isTransitSupported:Z

    move v1, p13

    iput-boolean v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isUsAipMaskSupported:Z

    move/from16 v1, p14

    iput-boolean v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isMagstripeDataValid:Z

    move/from16 v1, p15

    iput-boolean v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isPrimaryAidMchipDataValid:Z

    move/from16 v1, p16

    iput-boolean v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isAlternateAidMchipDataValid:Z

    move-object/from16 v1, p17

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$hostUmdConfig:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$cdCvmModel:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$aid:[B

    return-object v0
.end method

.method public getAlternateContactlessPaymentData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->access$000(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/AlternateContactlessPaymentData;

    move-result-object v0

    return-object v0
.end method

.method public getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$cdCvmModel:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0
.end method

.method public getCdol1RelatedDataLength()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$cdol1RelatedData:[B

    return-object v0
.end method

.method public getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$cvrMaskAnd:[B

    return-object v0
.end method

.method public getDeclineConditions()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$declineCondition:[B

    return-object v0
.end method

.method public getDeclineConditionsOnPpms()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$declineConditionOnPpms:[B

    return-object v0
.end method

.method public getGpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$gpoResponse:[B

    return-object v0
.end method

.method public getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$hostUmdConfig:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    return-object v0
.end method

.method public getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$issuerApplicationData:[B

    return-object v0
.end method

.method public getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$paymentFci:[B

    return-object v0
.end method

.method public getPinIvCvc3Track2()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$pinIvCvc3Track2:[B

    return-object v0
.end method

.method public getPpseFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$ppseFci:[B

    return-object v0
.end method

.method public getRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/Record;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->access$100(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTrack1ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->access$200(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    move-result-object v0

    return-object v0
.end method

.method public getTrack2ConstructionData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->access$300(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/TrackConstructionData;

    move-result-object v0

    return-object v0
.end method

.method public isAlternateAidMchipDataValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isAlternateAidMchipDataValid:Z

    return v0
.end method

.method public isMagstripeDataValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isMagstripeDataValid:Z

    return v0
.end method

.method public isPrimaryAidMchipDataValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isPrimaryAidMchipDataValid:Z

    return v0
.end method

.method public isTransitSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isTransitSupported:Z

    return v0
.end method

.method public isUsAipMaskSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$2;->val$isUsAipMaskSupported:Z

    return v0
.end method
