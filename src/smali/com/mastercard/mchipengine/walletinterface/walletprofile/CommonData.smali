.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;
.end method

.method public abstract getCardCountryCode()[B
.end method

.method public abstract getPan()[B
.end method

.method public abstract getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
.end method

.method public abstract isTransactionIdRequired()Z
.end method
