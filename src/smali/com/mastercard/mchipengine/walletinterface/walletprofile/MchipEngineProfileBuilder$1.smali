.class Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->withCommonProfileData([B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;Z)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

.field final synthetic val$accountType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

.field final synthetic val$cardCountryCode:[B

.field final synthetic val$isTransactionIdRequired:Z

.field final synthetic val$pan:[B

.field final synthetic val$productType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;Z)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    iput-object p2, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$cardCountryCode:[B

    iput-object p3, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$pan:[B

    iput-object p4, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$accountType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    iput-object p5, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$productType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    iput-boolean p6, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$isTransactionIdRequired:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$accountType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    return-object v0
.end method

.method public getCardCountryCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$cardCountryCode:[B

    return-object v0
.end method

.method public getPan()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$pan:[B

    return-object v0
.end method

.method public getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$productType:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0
.end method

.method public isTransactionIdRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$1;->val$isTransactionIdRequired:Z

    return v0
.end method
