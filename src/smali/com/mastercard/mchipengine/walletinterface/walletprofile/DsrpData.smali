.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAip()[B
.end method

.method public abstract getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
.end method

.method public abstract getCvrMaskAnd()[B
.end method

.method public abstract getDeclineConditions()[B
.end method

.method public abstract getExpirationDate()[B
.end method

.method public abstract getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;
.end method

.method public abstract getIssuerApplicationData()[B
.end method

.method public abstract getPanSequenceNumber()[B
.end method

.method public abstract getPaymentAccountReference()[B
.end method

.method public abstract getTrack2EquivalentData()[B
.end method

.method public abstract getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
.end method
