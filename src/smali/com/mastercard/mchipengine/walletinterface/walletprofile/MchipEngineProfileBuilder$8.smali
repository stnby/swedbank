.class Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$8;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->buildProfile()Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$8;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCommonData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$8;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->access$600(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/CommonData;

    move-result-object v0

    return-object v0
.end method

.method public getContactlessProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$8;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->access$400(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/ContactlessPaymentData;

    move-result-object v0

    return-object v0
.end method

.method public getDsrpProfileData()Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$8;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->access$500(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;

    move-result-object v0

    return-object v0
.end method
