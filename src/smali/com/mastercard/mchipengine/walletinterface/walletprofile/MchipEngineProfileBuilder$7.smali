.class Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletprofile/DsrpData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;->withDsrpData([B[B[B[B[B[B[B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;)Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

.field final synthetic val$aip:[B

.field final synthetic val$cdCvmModel:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

.field final synthetic val$cvrMaskAnd:[B

.field final synthetic val$declineConditions:[B

.field final synthetic val$expirationDate:[B

.field final synthetic val$hostUmdConfig:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

.field final synthetic val$issuerApplicationData:[B

.field final synthetic val$panSequenceNumber:[B

.field final synthetic val$paymentAccountReference:[B

.field final synthetic val$track2EquivalentData:[B

.field final synthetic val$ucafVersion:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;[B[B[B[B[B[B[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;[B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->this$0:Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder;

    iput-object p2, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$issuerApplicationData:[B

    iput-object p3, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$cvrMaskAnd:[B

    iput-object p4, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$expirationDate:[B

    iput-object p5, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$track2EquivalentData:[B

    iput-object p6, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$aip:[B

    iput-object p7, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$panSequenceNumber:[B

    iput-object p8, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$declineConditions:[B

    iput-object p9, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$hostUmdConfig:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    iput-object p10, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$ucafVersion:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    iput-object p11, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$cdCvmModel:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    iput-object p12, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$paymentAccountReference:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAip()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$aip:[B

    return-object v0
.end method

.method public getCdCvmModel()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$cdCvmModel:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0
.end method

.method public getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$cvrMaskAnd:[B

    return-object v0
.end method

.method public getDeclineConditions()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$declineConditions:[B

    return-object v0
.end method

.method public getExpirationDate()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$expirationDate:[B

    return-object v0
.end method

.method public getHostUmdConfig()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$hostUmdConfig:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    return-object v0
.end method

.method public getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$issuerApplicationData:[B

    return-object v0
.end method

.method public getPanSequenceNumber()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$panSequenceNumber:[B

    return-object v0
.end method

.method public getPaymentAccountReference()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$paymentAccountReference:[B

    return-object v0
.end method

.method public getTrack2EquivalentData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$track2EquivalentData:[B

    return-object v0
.end method

.method public getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/walletinterface/walletprofile/MchipEngineProfileBuilder$7;->val$ucafVersion:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    return-object v0
.end method
