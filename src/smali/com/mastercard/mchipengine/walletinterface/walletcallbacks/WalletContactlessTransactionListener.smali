.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onContactlessTransactionAbort(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
.end method

.method public abstract onContactlessTransactionCompleted(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
.end method

.method public abstract onContactlessTransactionIncident(Ljava/lang/Exception;)V
.end method
