.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getTimeOfLastAuthentication()J
.end method

.method public abstract isAuthenticated()Z
.end method

.method public abstract isCdCvmBlocked()Z
.end method

.method public abstract isCdCvmEnabled()Z
.end method
