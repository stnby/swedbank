.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;
.super Ljava/lang/Object;


# virtual methods
.method public abstract computeApplicationCryptogram([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B
.end method

.method public abstract computeCvc3([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B
.end method

.method public abstract computeSignedDynamicData([BB[B[B[B[B[B)[B
.end method

.method public abstract setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;
.end method

.method public abstract sha1([B)[B
.end method

.method public abstract sha256([B)[B
.end method
