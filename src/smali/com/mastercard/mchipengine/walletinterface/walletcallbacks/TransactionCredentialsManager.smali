.class public interface abstract Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;
.super Ljava/lang/Object;


# virtual methods
.method public abstract areUmdCredentialsSubjectToCvmFor(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Z
.end method

.method public abstract hasValidCredentialsFor(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Z
.end method
