.class public final Lcom/mastercard/mchipengine/g/b/a;
.super Lcom/mastercard/mchipengine/g/b/c;


# static fields
.field public static c:Lcom/mastercard/mchipengine/g/d/g;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mchipengine/g/b/c;-><init>()V

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    iput-object v0, p0, Lcom/mastercard/mchipengine/g/b/a;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    return-void
.end method

.method public static a(Lcom/mastercard/mchipengine/g/d/g;)V
    .locals 2

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    sget-object v0, Lcom/mastercard/mchipengine/g/b/a;->c:Lcom/mastercard/mchipengine/g/d/g;

    if-nez v0, :cond_0

    sput-object p0, Lcom/mastercard/mchipengine/g/b/a;->c:Lcom/mastercard/mchipengine/g/d/g;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    :cond_0
    return-void
.end method

.method public static b()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mastercard/mchipengine/g/b/a;->c:Lcom/mastercard/mchipengine/g/d/g;

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    return-object v0
.end method
