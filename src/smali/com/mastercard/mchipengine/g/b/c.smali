.class public abstract Lcom/mastercard/mchipengine/g/b/c;
.super Ljava/lang/Object;


# instance fields
.field public g:Lcom/mastercard/mchipengine/a;

.field public h:Lcom/mastercard/mchipengine/d/a;

.field public i:Lcom/mastercard/mchipengine/d/b;

.field public j:Lcom/mastercard/mchipengine/assessment/a;

.field public k:Lcom/mastercard/mchipengine/a/a;

.field public l:Lcom/mastercard/mchipengine/f/c;

.field public m:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

.field public n:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

.field public o:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

.field public p:Lcom/mastercard/mchipengine/c/b;

.field public q:Z

.field public r:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;

.field public s:Lcom/mastercard/mchipengine/b/b;

.field public t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

.field public u:Lcom/mastercard/mchipengine/g/a;

.field protected v:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->v:Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;
.end method

.method public final c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->DSRP:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->HIGH_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    if-nez v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->h()Lcom/mastercard/mchipengine/d/b/a/d;

    move-result-object v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/d;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->i()Lcom/mastercard/mchipengine/d/b/a/l;

    move-result-object v0
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_1 .. :try_end_1} :catch_0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/l;->h()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/l;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :catch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :cond_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->HIGH_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :catch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/j;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/j;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->HIGH_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :cond_4
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;
    :try_end_2
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_2 .. :try_end_2} :catch_2

    return-object v0

    :catch_2
    :cond_5
    :try_start_3
    iget-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->i()Lcom/mastercard/mchipengine/d/b/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/l;->h()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->i()Lcom/mastercard/mchipengine/d/b/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/l;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;
    :try_end_3
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_3 .. :try_end_3} :catch_3

    return-object v0

    :cond_7
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->HIGH_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0

    :catch_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0
.end method
