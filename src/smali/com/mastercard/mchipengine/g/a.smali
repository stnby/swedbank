.class public final Lcom/mastercard/mchipengine/g/a;
.super Ljava/lang/Object;


# instance fields
.field public a:Z

.field private b:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/g/a;->b:Lcom/mastercard/mchipengine/utils/MChipLogger;

    iput-boolean p1, p0, Lcom/mastercard/mchipengine/g/a;->a:Z

    return-void
.end method

.method private static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)[B
    .locals 3

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    const-string v1, "F"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, "F"

    const-string v1, ""

    invoke-virtual {v0, p0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "F"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_1
    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/b/b;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Z)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 14

    move-object/from16 v0, p2

    move-object v1, p0

    iget-boolean v2, v1, Lcom/mastercard/mchipengine/g/a;->a:Z

    const/4 v3, 0x0

    if-nez v2, :cond_0

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, p1

    :try_start_0
    iget-object v2, v2, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v4}, Lcom/mastercard/mchipengine/b/c;->a(BB)Lcom/mastercard/mchipengine/b/g;

    move-result-object v2

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/g;->c:Ljava/util/LinkedHashMap;

    const-string v4, "56"

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mchipengine/d/d;

    iget-object v6, v4, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string v4, "9F6B"

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/d/d;

    iget-object v8, v2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/e/c; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v2, Lcom/mastercard/mchipengine/g/d/c;

    move-object v5, v2

    move-object/from16 v7, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move/from16 v13, p8

    invoke-direct/range {v5 .. v13}, Lcom/mastercard/mchipengine/g/d/c;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Z)V

    iget-object v4, v2, Lcom/mastercard/mchipengine/g/d/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x5e

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x8

    iget-object v6, v2, Lcom/mastercard/mchipengine/g/d/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    iget-object v6, v2, Lcom/mastercard/mchipengine/g/d/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x8

    iget-object v6, v2, Lcom/mastercard/mchipengine/g/d/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    iget-object v6, v2, Lcom/mastercard/mchipengine/g/d/c;->b:Lcom/mastercard/mchipengine/b/h;

    invoke-virtual {v2, v6, v5}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v4

    iget-object v5, v2, Lcom/mastercard/mchipengine/g/d/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x44

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x8

    iget-object v7, v2, Lcom/mastercard/mchipengine/g/d/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "F"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    iget-object v7, v2, Lcom/mastercard/mchipengine/g/d/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x8

    iget-object v7, v2, Lcom/mastercard/mchipengine/g/d/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    iget-object v7, v2, Lcom/mastercard/mchipengine/g/d/c;->d:Lcom/mastercard/mchipengine/b/h;

    invoke-virtual {v2, v7, v5}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "F"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    goto :goto_1

    :cond_1
    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toUtf8String()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :goto_1
    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-interface {v0, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->sha256([B)[B

    move-result-object v4

    invoke-interface {v0, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->sha256([B)[B

    move-result-object v0

    const/16 v2, 0x20

    new-array v2, v2, [B

    array-length v5, v4

    const/16 v6, 0x10

    sub-int/2addr v5, v6

    invoke-static {v4, v5, v2, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v3, v0

    sub-int/2addr v3, v6

    invoke-static {v0, v3, v2, v6, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0

    :catch_0
    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0

    :catch_1
    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/g/a;->a:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    return-object p1

    :cond_0
    :try_start_0
    invoke-static {p2}, Lcom/mastercard/mchipengine/g/a;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)[B

    move-result-object p2

    invoke-virtual {p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p3

    const/4 v0, 0x2

    invoke-static {p3, v0}, Landroid/util/Base64;->decode([BI)[B

    move-result-object p3

    invoke-static {p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p3

    invoke-virtual {p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p3

    invoke-virtual {p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p3

    array-length v0, p2

    array-length v2, p3

    add-int/2addr v0, v2

    new-array v0, v0, [B

    array-length v2, p2

    invoke-static {p2, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p2, p2

    array-length v2, p3

    invoke-static {p3, v1, v0, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-interface {p1, v0}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->sha256([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 2

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/g/a;->a:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    return-object p1

    :cond_0
    :try_start_0
    invoke-static {p2}, Lcom/mastercard/mchipengine/g/a;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p2, p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p2, p4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->sha256([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    return-object p1
.end method
