.class public final Lcom/mastercard/mchipengine/g/c/b;
.super Lcom/mastercard/mchipengine/g/c/c;


# instance fields
.field private a:Lcom/mastercard/mchipengine/g/b/c;

.field private b:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private c:Lcom/mastercard/mchipengine/d/b;

.field private d:Lcom/mastercard/mchipengine/d/a;

.field private e:Lcom/mastercard/mchipengine/b/d;

.field private f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

.field private g:I


# direct methods
.method constructor <init>(Lcom/mastercard/mchipengine/g/b/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/g/c/c;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/c/b;->b:Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void
.end method

.method private a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)Lcom/mastercard/mchipengine/g/d/a;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;->VALID_CONTEXT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->DSRP:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    invoke-interface {v0, v1, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->setContext(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeysContext;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;

    move-result-object v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/e/j; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    new-instance v2, Lcom/mastercard/mchipengine/d/b/b/a;

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/CryptoContext;->getAtc()[B

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/mastercard/mchipengine/d/b/b/a;-><init>([B)V

    iput-object v2, v1, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->p:Lcom/mastercard/mchipengine/c/b;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/c/b;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->MD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    invoke-interface {v1, v2, v3}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->computeApplicationCryptogram([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    invoke-interface {v2, v0, p1}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;->computeApplicationCryptogram([BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)[B

    move-result-object p1

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/b/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/mastercard/mchipengine/c/b;->a([B[B[B)Lcom/mastercard/mchipengine/c/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/c/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    if-ne v2, v3, :cond_0

    invoke-direct {p0, p1, v1}, Lcom/mastercard/mchipengine/g/c/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/c/a/a;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/d;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/mastercard/mchipengine/g/c/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/c/a/a;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    new-instance v1, Lcom/mastercard/mchipengine/g/d/a;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    invoke-direct {v1, v2, p1, v0}, Lcom/mastercard/mchipengine/g/d/a;-><init>(Lcom/mastercard/mchipengine/d/b/b/a;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-object v1

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/e/d;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->as:Lcom/mastercard/mchipengine/e/a;

    const-string v1, "Error during Cryptogram computation"

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mchipengine/e/d;-><init>(Lcom/mastercard/mchipengine/e/a;Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mastercard/mchipengine/c/a; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    new-instance p1, Lcom/mastercard/mchipengine/e/d;

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    const-string v1, "Unable to build DSRP output"

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mchipengine/e/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/String;)V

    throw p1

    :catch_1
    new-instance p1, Lcom/mastercard/mchipengine/e/d;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->aq:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/d;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
.end method

.method private a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/c/a/a;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 20

    move-object/from16 v1, p0

    :try_start_0
    new-instance v0, Lcom/mastercard/mchipengine/g/d/b;

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v5, v2, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/p;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v6

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v7, v2, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    iget v8, v1, Lcom/mastercard/mchipengine/g/c/b;->g:I

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/t;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v9

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v10

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/a;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v11

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v12

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/r;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v13

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/s;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v14

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/k;->l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v15

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v16

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/d;->a:Lcom/mastercard/mchipengine/b/e;

    new-instance v4, Lcom/mastercard/mchipengine/d/d;

    sget-object v3, Lcom/mastercard/mchipengine/b/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/e;->b:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-direct {v4, v3, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    const-class v3, Lcom/mastercard/mchipengine/d/b/a/m;

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;

    move-result-object v18

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v19

    move-object v2, v0

    move-object/from16 v3, p1

    move-object/from16 v17, v4

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v19}, Lcom/mastercard/mchipengine/g/d/b;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/c/a/a;Lcom/mastercard/mchipengine/d/b/b/a;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/b/b/c;ILcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/d/d;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/mastercard/mchipengine/d/d;

    sget-object v4, Lcom/mastercard/mchipengine/g/d/b;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v5, v0, Lcom/mastercard/mchipengine/g/d/b;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v3, v4, v5}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->f:Lcom/mastercard/mchipengine/c/a/a;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/c/a/a;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->g:Lcom/mastercard/mchipengine/d/b/b/a;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/b/a;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->h:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->i:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/b/c;->b()Lcom/mastercard/mchipengine/d/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v3, v0, Lcom/mastercard/mchipengine/g/d/b;->j:I

    sget v4, Lcom/mastercard/mchipengine/assessment/b;->c:I

    if-eq v3, v4, :cond_1

    iget v3, v0, Lcom/mastercard/mchipengine/g/d/b;->j:I

    sget v4, Lcom/mastercard/mchipengine/assessment/b;->f:I

    if-ne v3, v4, :cond_0

    goto :goto_1

    :cond_0
    new-instance v3, Lcom/mastercard/mchipengine/d/d;

    sget-object v4, Lcom/mastercard/mchipengine/g/d/b;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string v5, "3F0002"

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    :goto_0
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    :goto_1
    new-instance v3, Lcom/mastercard/mchipengine/d/d;

    sget-object v4, Lcom/mastercard/mchipengine/g/d/b;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const-string v5, "010002"

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    goto :goto_0

    :goto_2
    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->k:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->l:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->m:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->n:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->o:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->p:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/mastercard/mchipengine/d/d;

    sget-object v4, Lcom/mastercard/mchipengine/g/d/b;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v5, v0, Lcom/mastercard/mchipengine/g/d/b;->q:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v3, v4, v5}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/mastercard/mchipengine/d/d;

    sget-object v4, Lcom/mastercard/mchipengine/g/d/b;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v5, v0, Lcom/mastercard/mchipengine/g/d/b;->r:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v3, v4, v5}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->s:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/d/b;->t:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/d/b;->u:Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/b;->b(Ljava/lang/Iterable;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v2, Lcom/mastercard/mchipengine/c/a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error during DE55 generation: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a/d;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/c/a/a;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 11

    :try_start_0
    new-instance v10, Lcom/mastercard/mchipengine/g/d/h;

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v5, v0, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->f()Lcom/mastercard/mchipengine/d/b/a/t;

    move-result-object v6

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v7

    move-object v0, v10

    move-object v2, p1

    move-object v3, p2

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/mastercard/mchipengine/g/d/h;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/b/b/a;Lcom/mastercard/mchipengine/d/b/a/t;Lcom/mastercard/mchipengine/d/d;Lcom/mastercard/mchipengine/c/a/a;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p2

    and-int/lit8 p2, p2, 0xf

    int-to-byte p2, p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 p4, 0x4

    invoke-virtual {p2, p3, p4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->copyOfRange(II)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/16 v0, 0x8

    invoke-virtual {p2, p4, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->copyOfRange(II)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->e:Lcom/mastercard/mchipengine/d/b/a/t;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/d/b/a/t;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->f:Lcom/mastercard/mchipengine/d/d;

    iget-object p2, p2, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->g:Lcom/mastercard/mchipengine/c/a/a;

    iget-object p2, p2, Lcom/mastercard/mchipengine/c/a/a;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2, p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->g:Lcom/mastercard/mchipengine/c/a/a;

    iget-object p2, p2, Lcom/mastercard/mchipengine/c/a/a;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 p4, 0x1

    invoke-virtual {p2, p4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->i:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    sget-object p4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0_PLUS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    if-ne p2, p4, :cond_0

    iget-object p2, v10, Lcom/mastercard/mchipengine/g/d/h;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2, p3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p2

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/b;->b(Ljava/lang/Iterable;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBase64String()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/mastercard/mchipengine/c/a;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "Error during UCAF generation: "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/a/d;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static e()Lcom/mastercard/mchipengine/g/d/a;
    .locals 2

    new-instance v0, Lcom/mastercard/mchipengine/walletinterface/walletexceptions/TransactionDeclinedException;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/walletinterface/walletexceptions/TransactionDeclinedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->j:Lcom/mastercard/mchipengine/assessment/a;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/assessment/a;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/mastercard/mchipengine/assessment/a;->a(Ljava/util/List;Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    :cond_0
    return v2
.end method

.method public final b()Lcom/mastercard/mchipengine/g/b/c;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    return-object v0
.end method

.method public final d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;
    .locals 20

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    iput-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    iput-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    iput-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/d/e;->getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object v0

    iput-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    iput-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v2, Lcom/mastercard/mchipengine/g/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    :try_start_0
    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getCurrencyCode()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide v5

    long-to-int v3, v5

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getCountryCode()[B

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getCountryCode()[B

    move-result-object v5

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide v5

    long-to-int v5, v5

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getUnpredictableNumber()[B

    move-result-object v6

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v6

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getAmount()[B

    move-result-object v7

    invoke-static {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v7

    invoke-static {v7}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide v7

    new-instance v9, Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getTransactionDate()[B

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/mastercard/mchipengine/utils/MChipDate;-><init>([B)V

    iget-object v10, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v11, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v11, v7}, Lcom/mastercard/mchipengine/d/b/a/b;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v10, v11}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V

    iget-object v7, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v8, Lcom/mastercard/mchipengine/d/b/a/a;

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-direct {v8, v10}, Lcom/mastercard/mchipengine/d/b/a/a;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v7, v8}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V

    iget-object v7, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v8, Lcom/mastercard/mchipengine/d/b/a/m;

    int-to-char v5, v5

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-direct {v8, v5}, Lcom/mastercard/mchipengine/d/b/a/m;-><init>(Ljava/lang/Character;)V

    invoke-virtual {v7, v8}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V

    iget-object v5, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v7, Lcom/mastercard/mchipengine/d/b/a/p;

    invoke-direct {v7}, Lcom/mastercard/mchipengine/d/b/a/p;-><init>()V

    invoke-virtual {v5, v7}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V

    iget-object v5, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v7, Lcom/mastercard/mchipengine/d/b/a/q;

    int-to-char v3, v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v7, v3}, Lcom/mastercard/mchipengine/d/b/a/q;-><init>(Ljava/lang/Character;)V

    invoke-virtual {v5, v7}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v5, Lcom/mastercard/mchipengine/d/b/a/r;

    invoke-direct {v5, v9}, Lcom/mastercard/mchipengine/d/b/a/r;-><init>(Lcom/mastercard/mchipengine/utils/MChipDate;)V

    invoke-virtual {v3, v5}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v5, Lcom/mastercard/mchipengine/d/b/a/s;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getTransactionType()B

    move-result v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-direct {v5, v2}, Lcom/mastercard/mchipengine/d/b/a/s;-><init>(Ljava/lang/Byte;)V

    invoke-virtual {v3, v5}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->d:Lcom/mastercard/mchipengine/d/a;

    new-instance v3, Lcom/mastercard/mchipengine/d/b/a/t;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/mastercard/mchipengine/d/b/a/t;-><init>(Ljava/lang/Integer;)V

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/a;->a(Lcom/mastercard/mchipengine/d/b/a/f;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    new-instance v3, Lcom/mastercard/mchipengine/d/d;

    sget-object v5, Lcom/mastercard/mchipengine/apduprocessing/a/c;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v6, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v6, v6, Lcom/mastercard/mchipengine/b/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/b;->a(Lcom/mastercard/mchipengine/d/d;)V
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/b; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mastercard/mchipengine/d/a/c; {:try_start_1 .. :try_end_1} :catch_0

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/b/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mastercard/mchipengine/d/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b;->b()V

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/f/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    move-result-object v3

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->INTERNATIONAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    if-ne v3, v5, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v4

    iget-object v5, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v5, v5, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    xor-int/2addr v3, v0

    invoke-virtual {v5, v3}, Lcom/mastercard/mchipengine/d/b/b/b;->a(Z)V

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/c;->j:Lcom/mastercard/mchipengine/assessment/a;

    iget-object v5, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-virtual {v3, v5}, Lcom/mastercard/mchipengine/assessment/a;->a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    goto :goto_2

    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v0, :cond_4

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CONSENT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    goto :goto_2

    :cond_4
    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    :goto_2
    sget-object v6, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-ne v5, v6, :cond_5

    invoke-static {}, Lcom/mastercard/mchipengine/a/a;->a()V

    :cond_5
    new-instance v9, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;

    invoke-direct {v9, v5, v3}, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Ljava/util/List;)V

    new-array v3, v0, [Ljava/lang/Object;

    aput-object v9, v3, v4

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/c;->o:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/f/c;->e()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v5

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/f/c;->d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object v2

    invoke-interface {v3, v9, v5, v2}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;->getFinalAssessment(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v2, v0, v4

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v7, v0, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/b/d;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v10

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/b/d;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object v11

    sget-object v12, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->DSRP:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-object v8, v2

    invoke-virtual/range {v7 .. v12}, Lcom/mastercard/mchipengine/f/c;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)I

    move-result v0

    iput v0, v1, Lcom/mastercard/mchipengine/g/c/b;->g:I

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    iget v3, v1, Lcom/mastercard/mchipengine/g/c/b;->g:I

    invoke-virtual {v0, v3, v4}, Lcom/mastercard/mchipengine/d/b/b/b;->a(IZ)V

    iget v0, v1, Lcom/mastercard/mchipengine/g/c/b;->g:I

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/b/d;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mastercard/mchipengine/f/c;->a(ILcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    move-result-object v0

    sget-object v3, Lcom/mastercard/mchipengine/g/c/b$1;->a:[I

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    invoke-static {}, Lcom/mastercard/mchipengine/g/c/b;->e()Lcom/mastercard/mchipengine/g/d/a;

    move-result-object v0

    goto :goto_4

    :pswitch_1
    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/b/c;->c()V

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/b/b;->a()V

    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v2, Lcom/mastercard/mchipengine/g/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object v2

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    sget-object v5, Lcom/mastercard/mchipengine/b/d$1;->a:[I

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_1

    :cond_6
    :pswitch_2
    iget-object v2, v3, Lcom/mastercard/mchipengine/b/d;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_3

    :pswitch_3
    iget-object v2, v3, Lcom/mastercard/mchipengine/b/d;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0_PLUS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    if-ne v2, v5, :cond_7

    iget-object v2, v3, Lcom/mastercard/mchipengine/b/d;->j:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_3

    :cond_7
    iget-object v2, v3, Lcom/mastercard/mchipengine/b/d;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    if-ne v2, v5, :cond_6

    iget-object v2, v3, Lcom/mastercard/mchipengine/b/d;->i:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    :goto_3
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->c:Lcom/mastercard/mchipengine/d/b;

    iget-object v3, v3, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {v3, v2}, Lcom/mastercard/mchipengine/d/b/b/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    :cond_8
    invoke-direct {v1, v0}, Lcom/mastercard/mchipengine/g/c/b;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;)Lcom/mastercard/mchipengine/g/d/a;

    move-result-object v0

    :goto_4
    iget-object v2, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v2, Lcom/mastercard/mchipengine/g/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/k;->l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v3}, Lcom/mastercard/mchipengine/g/c/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/lang/String;

    move-result-object v6

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v3}, Lcom/mastercard/mchipengine/g/c/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/lang/String;

    move-result-object v15

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v3, v3, Lcom/mastercard/mchipengine/b/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v5, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v5, v5, Lcom/mastercard/mchipengine/b/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v7, 0x0

    if-nez v5, :cond_9

    move-object/from16 v17, v7

    goto :goto_5

    :cond_9
    iget-object v5, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v5, v5, Lcom/mastercard/mchipengine/b/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v5

    move-object/from16 v17, v5

    :goto_5
    iget-object v5, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v5, v5, Lcom/mastercard/mchipengine/g/b/c;->u:Lcom/mastercard/mchipengine/g/a;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object v8

    iget-object v9, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v9, v9, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    iget-object v10, v0, Lcom/mastercard/mchipengine/g/d/a;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    sget-object v11, Lcom/mastercard/mchipengine/g/c/b$1;->b:[I

    invoke-virtual {v8}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->ordinal()I

    move-result v8

    aget v8, v11, v8

    packed-switch v8, :pswitch_data_2

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_7

    :pswitch_4
    iget-object v7, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v7, v7, Lcom/mastercard/mchipengine/b/k;->l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v5, v9, v7, v10}, Lcom/mastercard/mchipengine/g/a;->a(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v7

    goto :goto_7

    :pswitch_5
    iget-object v7, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v7, v7, Lcom/mastercard/mchipengine/b/k;->l:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v8, v0, Lcom/mastercard/mchipengine/g/d/a;->a:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v8, v8, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast v8, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v11, v0, Lcom/mastercard/mchipengine/g/d/a;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-boolean v12, v5, Lcom/mastercard/mchipengine/g/a;->a:Z

    if-nez v12, :cond_a

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    :goto_6
    move-object v7, v5

    goto :goto_7

    :cond_a
    invoke-virtual {v5, v9, v7, v8, v11}, Lcom/mastercard/mchipengine/g/a;->a(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    goto :goto_6

    :goto_7
    new-instance v19, Lcom/mastercard/mchipengine/g/d/f;

    invoke-virtual {v3, v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v3

    iget-object v4, v1, Lcom/mastercard/mchipengine/g/c/b;->e:Lcom/mastercard/mchipengine/b/d;

    iget-object v4, v4, Lcom/mastercard/mchipengine/b/d;->a:Lcom/mastercard/mchipengine/b/e;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/b/e;->a()Lcom/mastercard/mchipengine/utils/MChipDate;

    move-result-object v8

    invoke-virtual {v10}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v9

    iget-object v4, v1, Lcom/mastercard/mchipengine/g/c/b;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v4, v4, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v4, v4, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    iget-object v10, v4, Lcom/mastercard/mchipengine/b/d;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getAmount()[B

    move-result-object v11

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getCurrencyCode()[B

    move-result-object v12

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/d/a;->a:Lcom/mastercard/mchipengine/d/b/b/a;

    iget v13, v0, Lcom/mastercard/mchipengine/d/b/b/a;->a:I

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/d/e;->getUnpredictableNumber()[B

    move-result-object v14

    iget-object v0, v1, Lcom/mastercard/mchipengine/g/c/b;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    invoke-virtual {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v18

    move-object/from16 v5, v19

    move v7, v3

    move-object/from16 v16, v0

    invoke-direct/range {v5 .. v18}, Lcom/mastercard/mchipengine/g/d/f;-><init>(Ljava/lang/String;ILcom/mastercard/mchipengine/utils/MChipDate;[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;[B[BI[BLjava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;[B[B)V

    invoke-virtual/range {v19 .. v19}, Lcom/mastercard/mchipengine/g/d/f;->toString()Ljava/lang/String;

    return-object v19

    :catch_0
    new-instance v0, Lcom/mastercard/mchipengine/e/d;

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-direct {v0, v2}, Lcom/mastercard/mchipengine/e/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
