.class public final Lcom/mastercard/mchipengine/g/c/a;
.super Lcom/mastercard/mchipengine/g/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mchipengine/g/c/a$a;
    }
.end annotation


# static fields
.field private static b:I = 0x7d0

.field private static c:J = 0x3e8L


# instance fields
.field public a:Lcom/mastercard/mchipengine/g/c/a$a;

.field private d:Lcom/mastercard/mchipengine/g/b/a;

.field private e:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private f:Lcom/mastercard/mchipengine/utils/f;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/g/b/c;)V
    .locals 6

    invoke-direct {p0}, Lcom/mastercard/mchipengine/g/c/c;-><init>()V

    sget-object v0, Lcom/mastercard/mchipengine/g/c/a$a;->a:Lcom/mastercard/mchipengine/g/c/a$a;

    iput-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->a:Lcom/mastercard/mchipengine/g/c/a$a;

    check-cast p1, Lcom/mastercard/mchipengine/g/b/a;

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/c/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/c/a;->e:Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance p1, Lcom/mastercard/mchipengine/utils/f;

    const-string v1, "BetweenApduTimer"

    sget v2, Lcom/mastercard/mchipengine/g/c/a;->b:I

    sget-wide v3, Lcom/mastercard/mchipengine/g/c/a;->c:J

    new-instance v5, Lcom/mastercard/mchipengine/g/c/a$1;

    invoke-direct {v5, p0}, Lcom/mastercard/mchipengine/g/c/a$1;-><init>(Lcom/mastercard/mchipengine/g/c/a;)V

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/mastercard/mchipengine/utils/f;-><init>(Ljava/lang/String;IJLcom/mastercard/mchipengine/utils/f$a;)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    return-void
.end method

.method static synthetic a(Lcom/mastercard/mchipengine/g/c/a;)Lcom/mastercard/mchipengine/g/b/a;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/g/c/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/c/a$a;)V
    .locals 2

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/c/a;->a:Lcom/mastercard/mchipengine/g/c/a$a;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/c/a$a;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    return-void
.end method

.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final a([B)[B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->b()V

    :cond_0
    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/d;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    invoke-direct {v0, v1, p0}, Lcom/mastercard/mchipengine/apduprocessing/d;-><init>(Lcom/mastercard/mchipengine/g/b/c;Lcom/mastercard/mchipengine/g/c/a;)V

    invoke-static {p1, v0}, Lcom/mastercard/mchipengine/apduprocessing/b;->a([BLcom/mastercard/mchipengine/apduprocessing/d;)Lcom/mastercard/mchipengine/apduprocessing/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mchipengine/apduprocessing/a;->a()Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->a()V

    :cond_1
    iget-object p1, p1, Lcom/mastercard/mchipengine/apduprocessing/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p1

    return-object p1
.end method

.method public final b()Lcom/mastercard/mchipengine/g/b/c;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    return-object v0
.end method

.method public final c()V
    .locals 1

    invoke-super {p0}, Lcom/mastercard/mchipengine/g/c/c;->c()V

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mchipengine/g/c/a;->f:Lcom/mastercard/mchipengine/utils/f;

    :cond_0
    return-void
.end method
