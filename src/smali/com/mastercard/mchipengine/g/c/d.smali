.class public final Lcom/mastercard/mchipengine/g/c/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mchipengine/g/c/d$a;
    }
.end annotation


# instance fields
.field private a:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

.field private b:Lcom/mastercard/mchipengine/g/b;

.field private c:Lcom/mastercard/mchipengine/b/b;

.field private d:Lcom/mastercard/mchipengine/a;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/g/b;Lcom/mastercard/mchipengine/b/b;Lcom/mastercard/mchipengine/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/c/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    iput-object p2, p0, Lcom/mastercard/mchipengine/g/c/d;->b:Lcom/mastercard/mchipengine/g/b;

    iput-object p3, p0, Lcom/mastercard/mchipengine/g/c/d;->c:Lcom/mastercard/mchipengine/b/b;

    iput-object p4, p0, Lcom/mastercard/mchipengine/g/c/d;->d:Lcom/mastercard/mchipengine/a;

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/mastercard/mchipengine/g/c/c;
    .locals 4

    sget-object v0, Lcom/mastercard/mchipengine/g/c/d$1;->a:[I

    add-int/lit8 p1, p1, -0x1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    move-object p1, v0

    goto :goto_0

    :pswitch_0
    new-instance v0, Lcom/mastercard/mchipengine/g/b/b;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/g/b/b;-><init>()V

    new-instance p1, Lcom/mastercard/mchipengine/assessment/d;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/assessment/d;-><init>()V

    iput-object p1, v0, Lcom/mastercard/mchipengine/g/b/c;->j:Lcom/mastercard/mchipengine/assessment/a;

    new-instance p1, Lcom/mastercard/mchipengine/f/b;

    move-object v1, v0

    check-cast v1, Lcom/mastercard/mchipengine/g/b/b;

    invoke-direct {p1, v1}, Lcom/mastercard/mchipengine/f/b;-><init>(Lcom/mastercard/mchipengine/g/b/b;)V

    iput-object p1, v0, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    new-instance p1, Lcom/mastercard/mchipengine/g/c/b;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/g/c/b;-><init>(Lcom/mastercard/mchipengine/g/b/c;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/mastercard/mchipengine/g/b/a;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/g/b/a;-><init>()V

    new-instance p1, Lcom/mastercard/mchipengine/assessment/c;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/assessment/c;-><init>()V

    iput-object p1, v0, Lcom/mastercard/mchipengine/g/b/c;->j:Lcom/mastercard/mchipengine/assessment/a;

    new-instance p1, Lcom/mastercard/mchipengine/f/a;

    move-object v1, v0

    check-cast v1, Lcom/mastercard/mchipengine/g/b/a;

    invoke-direct {p1, v1}, Lcom/mastercard/mchipengine/f/a;-><init>(Lcom/mastercard/mchipengine/g/b/a;)V

    iput-object p1, v0, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    new-instance p1, Lcom/mastercard/mchipengine/g/c/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/g/c/a;-><init>(Lcom/mastercard/mchipengine/g/b/c;)V

    :goto_0
    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/d;->d:Lcom/mastercard/mchipengine/a;

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->g:Lcom/mastercard/mchipengine/a;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/d;->c:Lcom/mastercard/mchipengine/b/b;

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/d;->b:Lcom/mastercard/mchipengine/g/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b;->d:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->r:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;

    new-instance v1, Lcom/mastercard/mchipengine/d/a;

    invoke-direct {v1}, Lcom/mastercard/mchipengine/d/a;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    new-instance v1, Lcom/mastercard/mchipengine/d/b;

    invoke-direct {v1}, Lcom/mastercard/mchipengine/d/b;-><init>()V

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    new-instance v1, Lcom/mastercard/mchipengine/a/a;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/c/d;->b:Lcom/mastercard/mchipengine/g/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b;->b:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/a/a;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;)V

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->k:Lcom/mastercard/mchipengine/a/a;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/d;->b:Lcom/mastercard/mchipengine/g/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b;->c:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->m:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/d;->b:Lcom/mastercard/mchipengine/g/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b;->b:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->n:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/d;->b:Lcom/mastercard/mchipengine/g/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b;->a:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->o:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/c/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->t:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;

    new-instance v1, Lcom/mastercard/mchipengine/c/b;

    iget-object v2, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    iget-object v3, v0, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    invoke-direct {v1, v2, v3}, Lcom/mastercard/mchipengine/c/b;-><init>(Lcom/mastercard/mchipengine/d/a;Lcom/mastercard/mchipengine/d/b;)V

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->p:Lcom/mastercard/mchipengine/c/b;

    new-instance v1, Lcom/mastercard/mchipengine/g/a;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/c/d;->c:Lcom/mastercard/mchipengine/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean v2, v2, Lcom/mastercard/mchipengine/b/j;->f:Z

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/g/a;-><init>(Z)V

    iput-object v1, v0, Lcom/mastercard/mchipengine/g/b/c;->u:Lcom/mastercard/mchipengine/g/a;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
