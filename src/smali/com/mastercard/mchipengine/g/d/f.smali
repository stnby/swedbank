.class public final Lcom/mastercard/mchipengine/g/d/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:[B

.field private d:Lcom/mastercard/mchipengine/utils/MChipDate;

.field private e:[B

.field private f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

.field private g:[B

.field private h:[B

.field private i:I

.field private j:[B

.field private k:Ljava/lang/String;

.field private l:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

.field private m:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/mastercard/mchipengine/utils/MChipDate;[BLcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;[B[BI[BLjava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;[B[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/d/f;->a:Ljava/lang/String;

    iput p2, p0, Lcom/mastercard/mchipengine/g/d/f;->b:I

    iput-object p12, p0, Lcom/mastercard/mchipengine/g/d/f;->c:[B

    iput-object p3, p0, Lcom/mastercard/mchipengine/g/d/f;->d:Lcom/mastercard/mchipengine/utils/MChipDate;

    iput-object p4, p0, Lcom/mastercard/mchipengine/g/d/f;->e:[B

    iput-object p5, p0, Lcom/mastercard/mchipengine/g/d/f;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    iput-object p6, p0, Lcom/mastercard/mchipengine/g/d/f;->g:[B

    iput-object p7, p0, Lcom/mastercard/mchipengine/g/d/f;->h:[B

    iput p8, p0, Lcom/mastercard/mchipengine/g/d/f;->i:I

    iput-object p9, p0, Lcom/mastercard/mchipengine/g/d/f;->j:[B

    iput-object p10, p0, Lcom/mastercard/mchipengine/g/d/f;->k:Ljava/lang/String;

    iput-object p11, p0, Lcom/mastercard/mchipengine/g/d/f;->l:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    iput-object p13, p0, Lcom/mastercard/mchipengine/g/d/f;->m:[B

    return-void
.end method


# virtual methods
.method public final getCryptogramDataType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/f;->l:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-object v0
.end method

.method public final getExpirationDate()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/f;->d:Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipDate;->toHexString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPan()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPanSequenceNumber()[B
    .locals 1

    iget v0, p0, Lcom/mastercard/mchipengine/g/d/f;->b:I

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/b;->a(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getPar()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/f;->c:[B

    return-object v0
.end method

.method public final getTrack2EquivalentData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/f;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTransactionCryptogramData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/f;->e:[B

    return-object v0
.end method

.method public final getTransactionId()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/f;->m:[B

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->a:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget v2, p0, Lcom/mastercard/mchipengine/g/d/f;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->d:Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipDate;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->e:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->g:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->h:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget v2, p0, Lcom/mastercard/mchipengine/g/d/f;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->j:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/f;->k:Ljava/lang/String;

    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/d/f;->l:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    aput-object v1, v0, v3

    const-string v0, "DsrpOutputData"

    return-object v0
.end method
