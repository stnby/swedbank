.class public final Lcom/mastercard/mchipengine/g/d/c;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public final b:Lcom/mastercard/mchipengine/b/h;

.field public final c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public final d:Lcom/mastercard/mchipengine/b/h;

.field private e:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private final f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private final g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private final h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private final i:Z


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/g/d/c;->e:Lcom/mastercard/mchipengine/utils/MChipLogger;

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/d/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p2, p0, Lcom/mastercard/mchipengine/g/d/c;->b:Lcom/mastercard/mchipengine/b/h;

    iput-object p3, p0, Lcom/mastercard/mchipengine/g/d/c;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p4, p0, Lcom/mastercard/mchipengine/g/d/c;->d:Lcom/mastercard/mchipengine/b/h;

    iput-object p5, p0, Lcom/mastercard/mchipengine/g/d/c;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p6, p0, Lcom/mastercard/mchipengine/g/d/c;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p7, p0, Lcom/mastercard/mchipengine/g/d/c;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-boolean p8, p0, Lcom/mastercard/mchipengine/g/d/c;->i:Z

    return-void
.end method

.method private static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v2

    const/4 v3, 0x7

    :goto_1
    if-ltz v3, :cond_1

    invoke-static {v2, v3}, Lcom/mastercard/mchipengine/utils/b;->a(BI)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method private static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;I)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object p0

    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->copyOfRange(II)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;)V
    .locals 5

    :try_start_0
    iget-object p2, p2, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    invoke-static {p2, v0}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p2, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v3

    const/16 v4, 0x31

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(C)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    sub-int/2addr v1, p2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :catch_0
    :cond_2
    return-void
.end method

.method private static b(Lcom/mastercard/mchipengine/utils/MChipByteArray;I)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x10

    invoke-static {p0, v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    return-object p0
.end method

.method private static b(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;)V
    .locals 6

    :try_start_0
    iget-object p2, p2, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    invoke-static {p2, v0}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p2, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v3

    const/16 v4, 0x31

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(C)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    if-ge v1, p2, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v2

    invoke-virtual {p0, p2, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/b/h;Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 11

    iget-object v0, p1, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v0}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)I

    move-result v0

    iget-object v1, p1, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v1}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x1

    add-int/2addr v0, v1

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    const/4 v3, 0x0

    if-ge v2, v0, :cond_0

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p1, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v0}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)I

    move-result v0

    iget-object v2, p1, Lcom/mastercard/mchipengine/b/h;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x10

    invoke-static {v2, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    sub-int/2addr v0, v2

    iget-boolean v2, p0, Lcom/mastercard/mchipengine/g/d/c;->i:Z

    if-eqz v2, :cond_1

    add-int/lit8 v2, v0, 0x5

    rem-int/lit8 v2, v2, 0xa

    goto :goto_0

    :cond_1
    move v2, v0

    :goto_0
    iget-object v5, p1, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v5}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)I

    move-result v5

    iget-object v6, p0, Lcom/mastercard/mchipengine/g/d/c;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v6, v5}, Lcom/mastercard/mchipengine/g/d/c;->b(Lcom/mastercard/mchipengine/utils/MChipByteArray;I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    :try_start_0
    iget-object v6, p1, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v7

    invoke-static {v6, v7}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    :goto_1
    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v9

    if-ge v8, v9, :cond_3

    invoke-virtual {v6, v8}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v9

    const/16 v10, 0x31

    invoke-static {v10}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(C)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v10

    if-ne v9, v10, :cond_2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v10

    if-ge v9, v10, :cond_2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-ge v6, v8, :cond_4

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v5, v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v9

    invoke-virtual {p2, v8, v9}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :catch_0
    :cond_4
    iget-object v5, p0, Lcom/mastercard/mchipengine/g/d/c;->h:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    long-to-double v5, v5

    const-wide/high16 v7, 0x4024000000000000L    # 10.0

    int-to-double v9, v0

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    rem-double/2addr v5, v7

    double-to-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v7, v0, :cond_5

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "%0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-int/2addr v0, v7

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "d%s"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v1

    invoke-static {v0, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    goto :goto_4

    :cond_5
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :goto_4
    invoke-static {p2, v0, p1}, Lcom/mastercard/mchipengine/g/d/c;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;)V

    iget-object v0, p1, Lcom/mastercard/mchipengine/b/h;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;I)B

    move-result v0

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/mastercard/mchipengine/g/d/c;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v4, v0}, Lcom/mastercard/mchipengine/g/d/c;->b(Lcom/mastercard/mchipengine/utils/MChipByteArray;I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-static {p2, v0, p1}, Lcom/mastercard/mchipengine/g/d/c;->b(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/b/h;)V

    :cond_6
    iget-object v0, p1, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    iget-object v4, p1, Lcom/mastercard/mchipengine/b/h;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    sub-int/2addr v0, v1

    invoke-virtual {v4, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v4

    and-int/2addr v4, v1

    if-nez v4, :cond_7

    iget-object p1, p1, Lcom/mastercard/mchipengine/b/h;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p1

    and-int/2addr p1, v1

    if-nez p1, :cond_7

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p1, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p1

    invoke-virtual {p2, v0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    :cond_7
    return-object p2
.end method
