.class public final Lcom/mastercard/mchipengine/g/d/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;


# instance fields
.field public a:[B

.field public b:[B

.field public c:B

.field public d:[B

.field public e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

.field public f:[B

.field public g:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAmount()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/e;->a:[B

    return-object v0
.end method

.method public final getCountryCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/e;->g:[B

    return-object v0
.end method

.method public final getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/e;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    return-object v0
.end method

.method public final getCurrencyCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/e;->b:[B

    return-object v0
.end method

.method public final getTransactionDate()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/e;->f:[B

    return-object v0
.end method

.method public final getTransactionType()B
    .locals 1

    iget-byte v0, p0, Lcom/mastercard/mchipengine/g/d/e;->c:B

    return v0
.end method

.method public final getUnpredictableNumber()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/e;->d:[B

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/e;->a:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/e;->b:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-byte v2, p0, Lcom/mastercard/mchipengine/g/d/e;->c:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/e;->d:[B

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/e;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    new-instance v2, Lcom/mastercard/mchipengine/utils/MChipDate;

    iget-object v4, p0, Lcom/mastercard/mchipengine/g/d/e;->f:[B

    invoke-direct {v2, v4}, Lcom/mastercard/mchipengine/utils/MChipDate;-><init>([B)V

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipDate;->toHexString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/d/e;->g:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/d/e;->g:[B

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    aput-object v1, v0, v3

    const-string v0, "DsrpInputData"

    return-object v0
.end method
