.class public final Lcom/mastercard/mchipengine/g/d/g;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/mastercard/mchipengine/d/b/a/b;

.field public b:Lcom/mastercard/mchipengine/d/b/a/q;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/d/b/a/b;Lcom/mastercard/mchipengine/d/b/a/q;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/d/g;->a:Lcom/mastercard/mchipengine/d/b/a/b;

    iput-object p2, p0, Lcom/mastercard/mchipengine/g/d/g;->b:Lcom/mastercard/mchipengine/d/b/a/q;

    iput-boolean p3, p0, Lcom/mastercard/mchipengine/g/d/g;->c:Z

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/g;->a:Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/b;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/g;->b:Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/q;->toString()Ljava/lang/String;

    iget-boolean v0, p0, Lcom/mastercard/mchipengine/g/d/g;->c:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    const-string v0, "PreviousTapContext"

    return-object v0
.end method
