.class public final Lcom/mastercard/mchipengine/g/d/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;


# instance fields
.field private a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

.field private b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

.field private c:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

.field private d:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/g/d/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    iput-object p2, p0, Lcom/mastercard/mchipengine/g/d/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    iput-object p3, p0, Lcom/mastercard/mchipengine/g/d/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    iput-object p4, p0, Lcom/mastercard/mchipengine/g/d/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method


# virtual methods
.method public final getTerminalInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    return-object v0
.end method

.method public final getTransactionId()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    return-object v0
.end method

.method public final getTransactionOutcome()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/d/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/g/d/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/d/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/d/d;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_3

    :cond_3
    const-string v1, ""

    :goto_3
    aput-object v1, v0, v3

    const-string v0, "MchipContactlessLog"

    return-object v0
.end method
