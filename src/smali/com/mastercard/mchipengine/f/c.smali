.class public abstract Lcom/mastercard/mchipengine/f/c;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/mastercard/mchipengine/g/b/c;

.field protected b:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/g/b/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/f/c;->b:Lcom/mastercard/mchipengine/utils/MChipLogger;

    iput-object p1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    return-void
.end method

.method public static a(ILcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;
    .locals 2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    if-ne p1, v1, :cond_0

    sget v1, Lcom/mastercard/mchipengine/assessment/b;->c:I

    if-eq p0, v1, :cond_0

    sget v1, Lcom/mastercard/mchipengine/assessment/b;->f:I

    if-ne p0, v1, :cond_1

    :cond_0
    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    if-ne p1, p0, :cond_2

    :cond_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;->RANDOM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/KeyType;

    :cond_2
    return-object v0
.end method

.method private f()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    :try_start_0
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->c()Lcom/mastercard/mchipengine/d/b/a/n;

    move-result-object v0
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/a/j;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/a/j;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/n;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/n;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0

    :cond_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0

    :cond_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->NO:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
    :try_end_2
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_2 .. :try_end_2} :catch_1

    return-object v0

    :catch_1
    :cond_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0
.end method

.method private g()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    :try_start_0
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->h()Lcom/mastercard/mchipengine/d/b/a/d;

    move-result-object v1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->c()Lcom/mastercard/mchipengine/d/b/a/n;

    move-result-object v0
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/a/d;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/n;->c()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/n;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0

    :cond_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->NO:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
    :try_end_2
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_2 .. :try_end_2} :catch_1

    return-object v0

    :catch_1
    :cond_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)I
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne p5, v0, :cond_0

    iget-object p5, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object p5, p5, Lcom/mastercard/mchipengine/g/b/c;->n:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    invoke-interface {p5}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;->isCdCvmEnabled()Z

    move-result p5

    if-nez p5, :cond_0

    sget p1, Lcom/mastercard/mchipengine/assessment/b;->a:I

    return p1

    :cond_0
    sget-object p5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    if-ne p3, p5, :cond_5

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    if-eq p4, p1, :cond_2

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    if-ne p4, p1, :cond_1

    goto :goto_0

    :cond_1
    sget p1, Lcom/mastercard/mchipengine/assessment/b;->a:I

    return p1

    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->n:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;->isCdCvmBlocked()Z

    move-result p1

    if-eqz p1, :cond_3

    sget p1, Lcom/mastercard/mchipengine/assessment/b;->e:I

    return p1

    :cond_3
    iget-object p1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->k:Lcom/mastercard/mchipengine/a/a;

    iget-boolean p1, p1, Lcom/mastercard/mchipengine/a/a;->d:Z

    if-nez p1, :cond_4

    sget p1, Lcom/mastercard/mchipengine/assessment/b;->b:I

    return p1

    :cond_4
    sget p1, Lcom/mastercard/mchipengine/assessment/b;->c:I

    return p1

    :cond_5
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p3

    sget-object p4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p3, p4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_6

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p3

    sget-object p4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p3, p4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_7

    :cond_6
    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-ne p1, p3, :cond_7

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getAdvice()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object p1

    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-eq p1, p3, :cond_7

    sget p1, Lcom/mastercard/mchipengine/assessment/b;->f:I

    return p1

    :cond_7
    iget-object p1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->n:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;->isCdCvmBlocked()Z

    move-result p1

    if-eqz p1, :cond_8

    sget p1, Lcom/mastercard/mchipengine/assessment/b;->e:I

    return p1

    :cond_8
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p1

    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    sget p1, Lcom/mastercard/mchipengine/assessment/b;->d:I

    return p1

    :cond_9
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p1

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_a

    iget-object p1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->k:Lcom/mastercard/mchipengine/a/a;

    iget-boolean p1, p1, Lcom/mastercard/mchipengine/a/a;->d:Z

    if-eqz p1, :cond_a

    sget p1, Lcom/mastercard/mchipengine/assessment/b;->c:I

    return p1

    :cond_a
    sget p1, Lcom/mastercard/mchipengine/assessment/b;->b:I

    return p1
.end method

.method public abstract a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;
.end method

.method public abstract b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
.end method

.method public final c()Lcom/mastercard/mchipengine/f/a/a;
    .locals 2

    new-instance v0, Lcom/mastercard/mchipengine/f/a/a;

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/f/a/a;-><init>(Lcom/mastercard/mchipengine/g/b/c;)V

    return-object v0
.end method

.method public final d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;
    .locals 12

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-boolean v2, v2, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/f/c;->f()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    move-result-object v2

    :goto_0
    move-object v7, v2

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v2

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/mastercard/mchipengine/f/c;->g()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    move-result-object v2

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_0

    :goto_1
    const/4 v2, 0x0

    :try_start_0
    const-class v3, Lcom/mastercard/mchipengine/d/b/a/i;

    invoke-virtual {v0, v3}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mchipengine/d/b/a/i;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/i;->f()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mchipengine/utils/MChipByteArray;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    move-object v8, v3

    goto :goto_2

    :catch_0
    move-object v8, v2

    :goto_2
    :try_start_1
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/a/j;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_3

    :cond_2
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->NO:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    move-object v1, v2

    :catch_1
    move-object v5, v1

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v1, v2, :cond_4

    new-instance v1, Lcom/mastercard/mchipengine/f/d;

    const-class v2, Lcom/mastercard/mchipengine/d/b/a/o;

    invoke-virtual {v0, v2}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/o;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/o;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    packed-switch v0, :pswitch_data_2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    :goto_4
    move-object v4, v0

    goto :goto_5

    :pswitch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_2
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_4
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_5
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_6
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_7
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_8
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->MERCHANT_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_9
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_a
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_b
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_UNATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_c
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_d
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_OFFLINE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :pswitch_e
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->BANK_ATTENDED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    goto :goto_4

    :goto_5
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-boolean v0, v0, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->CONTACTLESS_MAGSTRIPE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    :goto_6
    move-object v6, v0

    goto :goto_7

    :cond_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->CONTACTLESS_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    goto :goto_6

    :goto_7
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/b/a;

    iget-object v9, v0, Lcom/mastercard/mchipengine/g/b/a;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/b/a;

    iget-object v10, v0, Lcom/mastercard/mchipengine/g/b/a;->d:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/b/a;

    iget-object v11, v0, Lcom/mastercard/mchipengine/g/b/a;->b:Ljava/util/List;

    move-object v3, v1

    invoke-direct/range {v3 .. v11}, Lcom/mastercard/mchipengine/f/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Ljava/util/List;)V

    return-object v1

    :cond_4
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/d/e;->getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object v0

    new-instance v1, Lcom/mastercard/mchipengine/f/d;

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->CARDHOLDER_OPERATED_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    if-ne v0, v4, :cond_5

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->DSRP_EMV:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    goto :goto_8

    :cond_5
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;->DSRP_UCAF:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    :goto_8
    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/mastercard/mchipengine/f/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;)V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x21
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final e()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;
    .locals 13

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/f/c;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    move-result-object v8

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;->AUTHORIZE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    :try_start_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/f/c;->c()Lcom/mastercard/mchipengine/f/a/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/f/a/a;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    move-result-object v4
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v3, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    iget-object v5, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-boolean v5, v5, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/j;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_1

    :cond_0
    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->NO:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/a;->h()Lcom/mastercard/mchipengine/d/b/a/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/d;->c()Z

    move-result v3
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v3, :cond_0

    goto :goto_0

    :goto_1
    move-object v11, v3

    move-object v7, v4

    goto :goto_2

    :catch_0
    move-object v3, v4

    :catch_1
    move-object v11, v2

    move-object v7, v3

    :goto_2
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    iget-object v3, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    instance-of v3, v3, Lcom/mastercard/mchipengine/g/b/a;

    if-eqz v3, :cond_3

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v1, Lcom/mastercard/mchipengine/f/a;

    iget-boolean v1, v1, Lcom/mastercard/mchipengine/f/a;->a:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_3

    :cond_2
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->NO:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    :goto_3
    iget-object v2, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v2, Lcom/mastercard/mchipengine/g/b/a;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/a;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    move-object v12, v1

    move-object v9, v2

    goto :goto_4

    :cond_3
    move-object v9, v1

    move-object v12, v2

    :goto_4
    new-instance v1, Lcom/mastercard/mchipengine/f/e;

    const-class v2, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {v0, v2}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/a/q;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    const-class v2, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {v0, v2}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/a/b;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    const-class v2, Lcom/mastercard/mchipengine/d/b/a/a;

    invoke-virtual {v0, v2}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/a;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/c;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    move-result-object v6

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/f/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    move-result-object v10

    move-object v2, v1

    invoke-direct/range {v2 .. v12}, Lcom/mastercard/mchipengine/f/e;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;)V

    return-object v1
.end method
