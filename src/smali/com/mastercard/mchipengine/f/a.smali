.class public final Lcom/mastercard/mchipengine/f/a;
.super Lcom/mastercard/mchipengine/f/c;


# instance fields
.field public a:Z

.field private final c:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private d:Lcom/mastercard/mchipengine/g/b/a;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/g/b/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/f/c;-><init>(Lcom/mastercard/mchipengine/g/b/c;)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/f/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/f/a;->c:Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->d()Lcom/mastercard/mchipengine/d/b/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/k;->k:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/a;->a([B)J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/m;->f()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/m;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->INTERNATIONAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    return-object v0

    :cond_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->DOMESTIC:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    return-object v0
.end method

.method public final b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->e()Lcom/mastercard/mchipengine/d/b/a/j;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    iget-boolean v1, v1, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/a;->i()Lcom/mastercard/mchipengine/d/b/a/l;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->forMagstripe(Lcom/mastercard/mchipengine/d/b/a/j;Lcom/mastercard/mchipengine/d/b/a/l;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/a;->d:Lcom/mastercard/mchipengine/g/b/a;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->h()Lcom/mastercard/mchipengine/d/b/a/d;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->forMChip(Lcom/mastercard/mchipengine/d/b/a/d;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    move-result-object v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object v0
.end method
