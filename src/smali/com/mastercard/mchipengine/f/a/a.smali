.class public final Lcom/mastercard/mchipengine/f/a/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/mastercard/mchipengine/g/b/c;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/g/b/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/f/a/a;->a:Lcom/mastercard/mchipengine/g/b/c;

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;
    .locals 6

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/a/a;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/a/a;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    const-class v1, Lcom/mastercard/mchipengine/d/b/a/s;

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/s;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/s;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/mastercard/mchipengine/f/a/a;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    const-class v2, Lcom/mastercard/mchipengine/d/b/a/h;

    invoke-virtual {v1, v2}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mchipengine/d/b/a/h;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/a/a;->a:Lcom/mastercard/mchipengine/g/b/c;

    iget-object v2, v2, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/a;->a()Lcom/mastercard/mchipengine/d/b/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/b/a/b;->f()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/a/h;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/a/a;->a:Lcom/mastercard/mchipengine/g/b/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->DSRP:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/a/a;->a:Lcom/mastercard/mchipengine/g/b/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/d/e;->getTransactionType()B

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :catch_0
    :cond_2
    :goto_0
    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0x20

    if-eq v0, v1, :cond_3

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->CASH:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->PURCHASE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object v0

    :cond_3
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->REFUND:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object v0

    :cond_4
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->PURCHASE_WITH_CASHBACK:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
