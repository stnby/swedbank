.class public final Lcom/mastercard/mchipengine/f/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;


# instance fields
.field private a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

.field private b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

.field private c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

.field private d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

.field private e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/f/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    iput-object p2, p0, Lcom/mastercard/mchipengine/f/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    iput-object p3, p0, Lcom/mastercard/mchipengine/f/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    iput-object p4, p0, Lcom/mastercard/mchipengine/f/d;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mastercard/mchipengine/f/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;)V

    iput-object p6, p0, Lcom/mastercard/mchipengine/f/d;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p7, p0, Lcom/mastercard/mchipengine/f/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p8, p0, Lcom/mastercard/mchipengine/f/d;->h:Ljava/util/List;

    iput-object p5, p0, Lcom/mastercard/mchipengine/f/d;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method


# virtual methods
.method public final getCdolValues()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getDiscretionaryDataByTag(Ljava/lang/String;)[B
    .locals 7

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getTag()[B

    move-result-object v5

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v6

    invoke-static {v5, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result p1

    add-int/2addr p1, v3

    invoke-static {v0, v3, p1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_1
    new-array p1, v2, [B

    return-object p1
.end method

.method public final getMerchantAndLocation()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->e:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0
.end method

.method public final getPdolValues()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTerminalTechnology()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    return-object v0
.end method

.method public final getTerminalType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    return-object v0
.end method

.method public final isCdCvmSupported()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0
.end method

.method public final isTwoTapSupported()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/d;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalType;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/d;->b:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/d;->c:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TerminalTechnology;

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/d;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/d;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_3

    :cond_3
    const-string v1, ""

    :goto_3
    aput-object v1, v0, v3

    const-string v0, "TerminalInformation"

    return-object v0
.end method
