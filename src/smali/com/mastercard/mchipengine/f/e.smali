.class public final Lcom/mastercard/mchipengine/f/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;


# instance fields
.field private a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field private d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

.field private e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

.field private f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

.field private g:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

.field private h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

.field private i:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

.field private j:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/f/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p2, p0, Lcom/mastercard/mchipengine/f/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p3, p0, Lcom/mastercard/mchipengine/f/e;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p4, p0, Lcom/mastercard/mchipengine/f/e;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    iput-object p5, p0, Lcom/mastercard/mchipengine/f/e;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    iput-object p6, p0, Lcom/mastercard/mchipengine/f/e;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    iput-object p7, p0, Lcom/mastercard/mchipengine/f/e;->g:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    iput-object p8, p0, Lcom/mastercard/mchipengine/f/e;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    iput-object p9, p0, Lcom/mastercard/mchipengine/f/e;->i:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    iput-object p10, p0, Lcom/mastercard/mchipengine/f/e;->j:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    return-void
.end method


# virtual methods
.method public final getAuthorizedAmount()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getConditionsOfUse()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    return-object v0
.end method

.method public final getCurrencyCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getExpectedUserActionOnPoi()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    return-object v0
.end method

.method public final getOtherAmount()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPurpose()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->g:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    return-object v0
.end method

.method public final getRichTransactionType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    return-object v0
.end method

.method public final getTransactionRange()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    return-object v0
.end method

.method public final hasTerminalRequestedCdCvm()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->i:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isAlternateAidUsed()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/e;->j:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;->YES:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, ""

    :goto_1
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    const-string v2, ""

    :goto_2
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->d:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    goto :goto_3

    :cond_3
    const-string v2, ""

    :goto_3
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    goto :goto_4

    :cond_4
    const-string v2, ""

    :goto_4
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->f:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    goto :goto_5

    :cond_5
    const-string v2, ""

    :goto_5
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->g:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->g:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Purpose;

    goto :goto_6

    :cond_6
    const-string v2, ""

    :goto_6
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->h:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    goto :goto_7

    :cond_7
    const-string v2, ""

    :goto_7
    aput-object v2, v1, v3

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->i:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/mastercard/mchipengine/f/e;->i:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_8

    :cond_8
    const-string v2, ""

    :goto_8
    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/e;->j:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/e;->j:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExtendedBool;

    goto :goto_9

    :cond_9
    const-string v1, ""

    :goto_9
    aput-object v1, v0, v3

    const-string v0, "TransactionInformation"

    return-object v0
.end method
