.class public final Lcom/mastercard/mchipengine/f/b;
.super Lcom/mastercard/mchipengine/f/c;


# instance fields
.field private a:Lcom/mastercard/mchipengine/g/b/b;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/g/b/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/f/c;-><init>(Lcom/mastercard/mchipengine/g/b/c;)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/f/b;->a:Lcom/mastercard/mchipengine/g/b/b;

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/b;->a:Lcom/mastercard/mchipengine/g/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/d/e;->getCountryCode()[B

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mchipengine/f/b;->a:Lcom/mastercard/mchipengine/g/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/d/e;->getCountryCode()[B

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/f/b;->a:Lcom/mastercard/mchipengine/g/b/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->d()Lcom/mastercard/mchipengine/d/b/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/f/b;->a:Lcom/mastercard/mchipengine/g/b/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/k;->k:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/a;->a([B)J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/m;->f()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/m;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->INTERNATIONAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    return-object v0

    :cond_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->DOMESTIC:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    return-object v0

    :cond_2
    :goto_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    return-object v0
.end method

.method public final b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;
    .locals 1

    invoke-static {}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->forDsrp()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    move-result-object v0

    return-object v0
.end method
