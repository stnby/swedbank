.class public final Lcom/mastercard/mchipengine/assessment/a/b/b;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->DSRP:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v1, v2, :cond_1

    check-cast p1, Lcom/mastercard/mchipengine/g/b/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/d/e;->getCountryCode()[B

    move-result-object p1

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    const/16 v1, 0x10

    invoke-static {p1, v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    const-wide/16 v3, 0x3e7

    cmp-long p1, v1, v3

    if-lez p1, :cond_1

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-object v0
.end method
