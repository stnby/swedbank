.class public final Lcom/mastercard/mchipengine/assessment/a/b/d;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->DSRP:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v1, v2, :cond_0

    check-cast p1, Lcom/mastercard/mchipengine/g/b/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    new-instance v1, Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/d/e;->getTransactionDate()[B

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/mastercard/mchipengine/utils/MChipDate;-><init>([B)V

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipDate;->isValid()Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method
