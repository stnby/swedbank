.class public final Lcom/mastercard/mchipengine/assessment/a/a/g;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method

.method private static a(Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->UNSUPPORTED_TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/f/c;->e()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getRichTransactionType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    if-ne v0, v1, :cond_0

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean p1, p1, Lcom/mastercard/mchipengine/b/j;->g:Z

    :goto_0
    invoke-static {p1}, Lcom/mastercard/mchipengine/assessment/a/a/g;->a(Z)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0
.end method
