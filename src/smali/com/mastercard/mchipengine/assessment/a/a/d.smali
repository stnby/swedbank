.class public final Lcom/mastercard/mchipengine/assessment/a/a/d;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p1, Lcom/mastercard/mchipengine/g/b/c;->r:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    move-result-object v2

    iget-object v3, p1, Lcom/mastercard/mchipengine/g/b/c;->k:Lcom/mastercard/mchipengine/a/a;

    iget-boolean v3, v3, Lcom/mastercard/mchipengine/a/a;->d:Z

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;->areUmdCredentialsSubjectToCvmFor(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Z

    move-result v1

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v2

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v2, v4, :cond_0

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    :goto_0
    invoke-virtual {p1}, Lcom/mastercard/mchipengine/b/k;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    move-result-object p1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/HostUmdConfig;

    if-eq p1, v2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    if-eqz v1, :cond_2

    if-nez v3, :cond_2

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_ACCESSIBLE_WITHOUT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method
