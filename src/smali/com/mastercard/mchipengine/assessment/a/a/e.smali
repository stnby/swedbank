.class public final Lcom/mastercard/mchipengine/assessment/a/a/e;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p1, Lcom/mastercard/mchipengine/g/b/c;->r:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;->hasValidCredentialsFor(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;)Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_AVAILABLE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method
