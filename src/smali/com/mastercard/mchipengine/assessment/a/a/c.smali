.class public final Lcom/mastercard/mchipengine/assessment/a/a/c;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method

.method private static a(Lcom/mastercard/mchipengine/g/b/a;)Z
    .locals 9

    iget-object v0, p0, Lcom/mastercard/mchipengine/g/b/c;->h:Lcom/mastercard/mchipengine/d/a;

    sget-object v1, Lcom/mastercard/mchipengine/g/b/a;->c:Lcom/mastercard/mchipengine/g/d/g;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    :try_start_0
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->a()Lcom/mastercard/mchipengine/d/b/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/b;->f()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/a;->b()Lcom/mastercard/mchipengine/d/b/a/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b/a/q;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object p0, p0, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast p0, Lcom/mastercard/mchipengine/f/a;

    iget-boolean p0, p0, Lcom/mastercard/mchipengine/f/a;->a:Z

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/d/g;->a:Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/b;->f()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v3, v7, v5

    if-nez v3, :cond_0

    iget-object v3, v1, Lcom/mastercard/mchipengine/g/d/g;->b:Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/b/a/q;->f()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v1, v1, Lcom/mastercard/mchipengine/g/d/g;->c:Z

    if-ne v1, p0, :cond_0

    return v2

    :cond_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v4

    aput-object v0, v1, v2

    const/4 v0, 0x2

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    aput-object p0, v1, v0
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return v4

    :cond_1
    return v2
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v1, v2, :cond_0

    check-cast p1, Lcom/mastercard/mchipengine/g/b/a;

    invoke-static {p1}, Lcom/mastercard/mchipengine/assessment/a/a/c;->a(Lcom/mastercard/mchipengine/g/b/a;)Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CONTEXT_NOT_MATCHING:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method
