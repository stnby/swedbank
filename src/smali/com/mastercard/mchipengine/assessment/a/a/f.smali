.class public final Lcom/mastercard/mchipengine/assessment/a/a/f;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method

.method private static a(Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method private static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/b/b/b;)Z
    .locals 4

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast p1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_1

    add-int/lit8 v2, v1, 0x3

    invoke-virtual {p1, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v2

    invoke-virtual {p0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v3

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    iget-object v0, p1, Lcom/mastercard/mchipengine/g/b/c;->i:Lcom/mastercard/mchipengine/d/b;

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    const/4 v3, 0x1

    if-ne v1, v2, :cond_6

    iget-object v1, p1, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    check-cast v1, Lcom/mastercard/mchipengine/f/a;

    iget-boolean v1, v1, Lcom/mastercard/mchipengine/f/a;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/a;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/c;->c:Lcom/mastercard/mchipengine/b/a;

    :goto_0
    iget-object v2, v2, Lcom/mastercard/mchipengine/b/a;->g:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_1

    :cond_0
    iget-object v1, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/b/a;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    iget-object v2, v2, Lcom/mastercard/mchipengine/b/c;->b:Lcom/mastercard/mchipengine/b/a;

    goto :goto_0

    :goto_1
    iget-boolean v4, p1, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    if-eqz v4, :cond_5

    if-eqz v2, :cond_5

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/f/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    move-result-object p1

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;->INTERNATIONAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ConditionsOfUse;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_2

    :cond_1
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_3

    invoke-virtual {v2, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p1

    const/4 v0, 0x4

    and-int/2addr p1, v0

    if-eq p1, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    invoke-static {v1}, Lcom/mastercard/mchipengine/assessment/a/a/f;->a(Z)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_3
    invoke-virtual {v2, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p1

    const/4 v0, 0x2

    and-int/2addr p1, v0

    if-eq p1, v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    invoke-static {v1}, Lcom/mastercard/mchipengine/assessment/a/a/f;->a(Z)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_5
    iget-boolean p1, p1, Lcom/mastercard/mchipengine/g/b/c;->q:Z

    if-nez p1, :cond_7

    if-eqz v1, :cond_7

    invoke-static {v1, v0}, Lcom/mastercard/mchipengine/assessment/a/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/b/b/b;)Z

    move-result p1

    :goto_3
    xor-int/2addr p1, v3

    invoke-static {p1}, Lcom/mastercard/mchipengine/assessment/a/a/f;->a(Z)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_6
    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object p1, p1, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    iget-object p1, p1, Lcom/mastercard/mchipengine/b/d;->f:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz p1, :cond_7

    invoke-static {p1, v0}, Lcom/mastercard/mchipengine/assessment/a/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/d/b/b/b;)Z

    move-result p1

    goto :goto_3

    :cond_7
    invoke-static {v3}, Lcom/mastercard/mchipengine/assessment/a/a/f;->a(Z)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
