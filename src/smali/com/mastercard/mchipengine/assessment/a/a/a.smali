.class public final Lcom/mastercard/mchipengine/assessment/a/a/a;
.super Lcom/mastercard/mchipengine/assessment/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/assessment/a/a;-><init>()V

    return-void
.end method

.method private static b(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/mchipengine/g/b/c;->k:Lcom/mastercard/mchipengine/a/a;

    iget-boolean v1, v1, Lcom/mastercard/mchipengine/a/a;->d:Z

    if-nez v1, :cond_0

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    :goto_0
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_0
    iget-object v1, p0, Lcom/mastercard/mchipengine/g/b/c;->k:Lcom/mastercard/mchipengine/a/a;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/g/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    move-result-object p0

    iget-object v2, v1, Lcom/mastercard/mchipengine/a/a;->c:Lcom/mastercard/mchipengine/g/a/a;

    sget-object v3, Lcom/mastercard/mchipengine/g/a/a$1;->a:[I

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget v2, v2, Lcom/mastercard/mchipengine/g/a/a;->d:I

    :goto_1
    int-to-long v5, v2

    sub-long/2addr v3, v5

    goto :goto_2

    :pswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget v2, v2, Lcom/mastercard/mchipengine/g/a/a;->c:I

    goto :goto_1

    :goto_2
    sget-object v2, Lcom/mastercard/mchipengine/a/a;->a:Lcom/mastercard/mchipengine/a/b;

    iget v2, v2, Lcom/mastercard/mchipengine/a/b;->a:I

    iget-object v1, v1, Lcom/mastercard/mchipengine/a/a;->c:Lcom/mastercard/mchipengine/g/a/a;

    sget-object v5, Lcom/mastercard/mchipengine/g/a/a$1;->a:[I

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    :pswitch_2
    iget v1, v1, Lcom/mastercard/mchipengine/g/a/a;->a:I

    goto :goto_3

    :pswitch_3
    iget v1, v1, Lcom/mastercard/mchipengine/g/a/a;->b:I

    :goto_3
    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ge v2, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_4

    :cond_1
    const/4 v1, 0x0

    :goto_4
    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    if-ne p0, v2, :cond_2

    goto :goto_5

    :cond_2
    if-eqz v1, :cond_3

    sget-wide v1, Lcom/mastercard/mchipengine/a/a;->e:J

    cmp-long p0, v1, v3

    if-ltz p0, :cond_3

    sget-wide v1, Lcom/mastercard/mchipengine/a/a;->e:J

    const-wide/16 v3, 0x0

    cmp-long p0, v1, v3

    if-ltz p0, :cond_3

    const/4 v1, 0x1

    goto :goto_5

    :cond_3
    const/4 v1, 0x0

    :goto_5
    if-nez v1, :cond_4

    sget-object p0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    goto :goto_0

    :cond_4
    :goto_6
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static c(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v1, p0, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/f/c;->c()Lcom/mastercard/mchipengine/f/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/f/a/a;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    move-result-object v1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/e; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    :goto_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/g/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    move-result-object v2

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    if-eq v2, v3, :cond_1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    if-ne v1, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {p0}, Lcom/mastercard/mchipengine/assessment/a/a/a;->b(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_1
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p1, Lcom/mastercard/mchipengine/g/b/c;->k:Lcom/mastercard/mchipengine/a/a;

    iget-object v2, p1, Lcom/mastercard/mchipengine/g/b/c;->s:Lcom/mastercard/mchipengine/b/b;

    iget-object v3, v1, Lcom/mastercard/mchipengine/a/a;->b:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    invoke-interface {v3}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;->isAuthenticated()Z

    move-result v3

    iput-boolean v3, v1, Lcom/mastercard/mchipengine/a/a;->d:Z

    iget-object v1, v1, Lcom/mastercard/mchipengine/a/a;->b:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;

    invoke-interface {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;->getTimeOfLastAuthentication()J

    move-result-wide v3

    const/4 v1, 0x0

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-ltz v5, :cond_0

    sget-wide v5, Lcom/mastercard/mchipengine/a/a;->e:J

    cmp-long v5, v3, v5

    if-lez v5, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    sput-wide v3, Lcom/mastercard/mchipengine/a/a;->e:J

    if-eqz v5, :cond_1

    sget-object v3, Lcom/mastercard/mchipengine/a/a;->a:Lcom/mastercard/mchipengine/a/b;

    iput v1, v3, Lcom/mastercard/mchipengine/a/b;->a:I

    :cond_1
    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->a()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    move-result-object v1

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;->CONTACTLESS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CredentialsScope;

    if-ne v1, v3, :cond_2

    iget-object v1, v2, Lcom/mastercard/mchipengine/b/b;->b:Lcom/mastercard/mchipengine/b/c;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v1, v2, Lcom/mastercard/mchipengine/b/b;->c:Lcom/mastercard/mchipengine/b/d;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/b/d;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v1

    :goto_1
    sget-object v2, Lcom/mastercard/mchipengine/assessment/a/a/a$1;->a:[I

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_3

    :pswitch_0
    invoke-static {p1}, Lcom/mastercard/mchipengine/assessment/a/a/a;->c(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object p1

    goto :goto_2

    :pswitch_1
    invoke-static {p1}, Lcom/mastercard/mchipengine/assessment/a/a/a;->b(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object p1

    :goto_2
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :pswitch_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/b/c;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    move-result-object v2

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->HIGH_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    if-ne v2, v3, :cond_3

    iget-object p1, p1, Lcom/mastercard/mchipengine/g/b/c;->l:Lcom/mastercard/mchipengine/f/c;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/f/c;->b()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    move-result-object p1

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;->NONE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ExpectedUserActionOnPoi;

    if-ne p1, v2, :cond_3

    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_POI_AUTHENTICATION:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_3
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
