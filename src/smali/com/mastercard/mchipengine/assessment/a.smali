.class public abstract Lcom/mastercard/mchipengine/assessment/a;
.super Ljava/lang/Object;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/assessment/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/assessment/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/assessment/a;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/assessment/a;->b:Ljava/util/List;

    new-instance v0, Lcom/mastercard/mchipengine/assessment/a/a/a;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/assessment/a/a/a;-><init>()V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/assessment/a;->b(Lcom/mastercard/mchipengine/assessment/a/a;)V

    new-instance v0, Lcom/mastercard/mchipengine/assessment/a/a/b;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/assessment/a/a/b;-><init>()V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/assessment/a;->b(Lcom/mastercard/mchipengine/assessment/a/a;)V

    new-instance v0, Lcom/mastercard/mchipengine/assessment/a/a/d;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/assessment/a/a/d;-><init>()V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/assessment/a;->b(Lcom/mastercard/mchipengine/assessment/a/a;)V

    new-instance v0, Lcom/mastercard/mchipengine/assessment/a/a/e;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/assessment/a/a/e;-><init>()V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/assessment/a;->b(Lcom/mastercard/mchipengine/assessment/a/a;)V

    new-instance v0, Lcom/mastercard/mchipengine/assessment/a/a/f;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/assessment/a/a/f;-><init>()V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/assessment/a;->b(Lcom/mastercard/mchipengine/assessment/a/a;)V

    new-instance v0, Lcom/mastercard/mchipengine/assessment/a/a/g;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/assessment/a/a/g;-><init>()V

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/assessment/a;->b(Lcom/mastercard/mchipengine/assessment/a/a;)V

    return-void
.end method

.method public static a(Ljava/util/List;Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/assessment/a/a;",
            ">;",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mchipengine/assessment/a/a;

    invoke-virtual {v1, p1}, Lcom/mastercard/mchipengine/assessment/a/a;->a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/g/b/c;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mchipengine/assessment/a;->b:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/mastercard/mchipengine/assessment/a;->a(Ljava/util/List;Lcom/mastercard/mchipengine/g/b/c;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method final a(Lcom/mastercard/mchipengine/assessment/a/a;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/assessment/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method final b(Lcom/mastercard/mchipengine/assessment/a/a;)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/assessment/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
