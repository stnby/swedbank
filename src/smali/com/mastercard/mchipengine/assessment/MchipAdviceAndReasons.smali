.class public Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;


# instance fields
.field private mAdvice:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

.field private mReasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->mAdvice:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    iput-object p2, p0, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->mReasons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getAdvice()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->mAdvice:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    return-object v0
.end method

.method public getReasons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->mReasons:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->mAdvice:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/assessment/MchipAdviceAndReasons;->mReasons:Ljava/util/List;

    aput-object v1, v0, v3

    const-string v0, "MchipAdviceAndReasons"

    return-object v0
.end method
