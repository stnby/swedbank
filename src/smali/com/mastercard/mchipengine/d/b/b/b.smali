.class public final Lcom/mastercard/mchipengine/d/b/b/b;
.super Lcom/mastercard/mchipengine/d/b/b/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/b/d<",
        "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 3

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/b/d;-><init>()V

    const/4 v0, 0x6

    iput v0, p0, Lcom/mastercard/mchipengine/d/b/b/b;->a:I

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    invoke-direct {p0, v2, v1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(IB)V

    new-array p1, v2, [Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[CardVerificationResultsModel="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " bits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    return-void
.end method

.method private a(IB)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v1

    and-int/2addr p2, v1

    int-to-byte p2, p2

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, p1, v1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    :goto_0
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    const/4 v1, 0x1

    shl-int p2, v1, p2

    or-int/2addr p2, v0

    int-to-byte p2, p2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-void
.end method

.method public final a(IZ)V
    .locals 4

    sget-object v0, Lcom/mastercard/mchipengine/d/b/b/b$1;->a:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x2

    const/4 v2, 0x3

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, v1}, Lcom/mastercard/mchipengine/d/b/b/b;->b(Z)V

    invoke-virtual {p0, v3, v3}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array p1, v1, [Ljava/lang/Object;

    iget-object p2, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v3

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v3}, Lcom/mastercard/mchipengine/d/b/b/b;->b(Z)V

    return-void

    :pswitch_2
    invoke-direct {p0, v3}, Lcom/mastercard/mchipengine/d/b/b/b;->b(Z)V

    return-void

    :pswitch_3
    invoke-virtual {p0, v3, v0}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array p1, v1, [Ljava/lang/Object;

    iget-object p2, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v3

    return-void

    :pswitch_4
    invoke-virtual {p0, v3, v0}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    const/4 p1, 0x4

    invoke-virtual {p0, v2, p1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array p1, v1, [Ljava/lang/Object;

    iget-object p2, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v3

    return-void

    :pswitch_5
    if-eqz p2, :cond_0

    invoke-virtual {p0, v2, v2}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    new-array p1, v1, [Ljava/lang/Object;

    iget-object p2, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v3

    return-void

    :cond_0
    invoke-direct {p0, v3}, Lcom/mastercard/mchipengine/d/b/b/b;->b(Z)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/mastercard/mchipengine/d/b/b/b;->a(IB)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    return-void
.end method

.method public final a(Z)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x3

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    invoke-virtual {p0, v1, p1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(II)V

    :goto_0
    new-array p1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    return-void
.end method

.method public final b()V
    .locals 3

    const/4 v0, 0x3

    const/4 v1, -0x2

    invoke-direct {p0, v0, v1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(IB)V

    const/4 v0, 0x5

    const/16 v1, -0x9

    invoke-direct {p0, v0, v1}, Lcom/mastercard/mchipengine/d/b/b/b;->a(IB)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-void
.end method

.method public final c()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, "CardVerificationResultsModel"

    return-object v0
.end method
