.class public final Lcom/mastercard/mchipengine/d/b/b/e;
.super Lcom/mastercard/mchipengine/d/b/b/d;

# interfaces
.implements Lcom/mastercard/mchipengine/utils/Wipeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/b/d<",
        "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
        ">;",
        "Lcom/mastercard/mchipengine/utils/Wipeable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/d/b/b/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    nop

    :array_0
    .array-data 1
        -0x21t
        0x4bt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/b/d;-><init>()V

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/d/d;
    .locals 3

    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/d/b/b/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-object v0
.end method

.method public final a(IB)V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v1

    or-int/2addr p2, v1

    int-to-byte p2, p2

    invoke-virtual {v0, p1, p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-void
.end method

.method public final wipe()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/Wipeable;

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/i;->a(Lcom/mastercard/mchipengine/utils/Wipeable;)V

    return-void
.end method
