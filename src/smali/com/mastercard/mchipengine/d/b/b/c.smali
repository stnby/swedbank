.class public final Lcom/mastercard/mchipengine/d/b/b/c;
.super Lcom/mastercard/mchipengine/d/b/b/d;

# interfaces
.implements Lcom/mastercard/mchipengine/utils/Wipeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/b/d<",
        "Ljava/lang/Byte;",
        ">;",
        "Lcom/mastercard/mchipengine/utils/Wipeable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/d/b/b/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    nop

    :array_0
    .array-data 1
        -0x61t
        0x27t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/b/d;-><init>()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    aput-object v2, v1, v0

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/mastercard/mchipengine/d/d;
    .locals 3

    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/d/b/b/c;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-object v0
.end method

.method public final c()V
    .locals 3

    const/16 v0, -0x80

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-void
.end method

.method public final wipe()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b/b/c;->b:Ljava/lang/Object;

    return-void
.end method
