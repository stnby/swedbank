.class public Lcom/mastercard/mchipengine/d/b/a/j;
.super Lcom/mastercard/mchipengine/d/b/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/a/f<",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/f;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/j;-><init>()V

    return-void
.end method

.method public static a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    const-string v0, "9F7E"

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public static getBuilder()Lcom/mastercard/mchipengine/d/b/a/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mastercard/mchipengine/d/b/a/k<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mastercard/mchipengine/d/b/a/j$1;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/a/j$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mastercard/mchipengine/utils/b;->a(BI)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mastercard/mchipengine/utils/b;->a(BI)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
