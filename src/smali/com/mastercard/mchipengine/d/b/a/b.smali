.class public Lcom/mastercard/mchipengine/d/b/a/b;
.super Lcom/mastercard/mchipengine/d/b/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/a/f<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/f;-><init>()V

    const/4 v0, 0x6

    :try_start_0
    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/b/a/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 2

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/b;-><init>()V

    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/mastercard/mchipengine/utils/a;->a(J)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->padLeading(IB)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    :cond_0
    return-void
.end method

.method public static a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    const-string v0, "9F02"

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public static getBuilder()Lcom/mastercard/mchipengine/d/b/a/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mastercard/mchipengine/d/b/a/k<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mastercard/mchipengine/d/b/a/b$1;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/a/b$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/a;->a([B)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    return-void
.end method
