.class public Lcom/mastercard/mchipengine/d/b/a/l;
.super Lcom/mastercard/mchipengine/d/b/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/a/f<",
        "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/f;-><init>()V

    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/b/a/l;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    const-string v0, "9F33"

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/l;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    shl-int p1, v1, p1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    return v1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public static getBuilder()Lcom/mastercard/mchipengine/d/b/a/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mastercard/mchipengine/d/b/a/k<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mastercard/mchipengine/d/b/a/l$1;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/a/l$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/l;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/b/a/l;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/b/a/l;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/l;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xf8

    const/16 v2, 0x40

    if-ne v0, v2, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/l;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xf8

    const/16 v2, 0x20

    if-ne v0, v2, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/l;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
