.class public abstract Lcom/mastercard/mchipengine/d/b/a/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected b:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 0

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/d/b/a/f;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mastercard/mchipengine/d/b/a/f;->a:Ljava/lang/Object;

    return-void
.end method

.method public final f()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/f;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final g()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/f;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
