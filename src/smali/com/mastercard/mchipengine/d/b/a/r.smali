.class public Lcom/mastercard/mchipengine/d/b/a/r;
.super Lcom/mastercard/mchipengine/d/b/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/a/f<",
        "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/f;-><init>()V

    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/b/a/r;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/r;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipDate;)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/r;-><init>()V

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/b;->a(Lcom/mastercard/mchipengine/utils/MChipDate;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-void
.end method

.method public static a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    const-string v0, "9A"

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public static getBuilder()Lcom/mastercard/mchipengine/d/b/a/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mastercard/mchipengine/d/b/a/k<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mastercard/mchipengine/d/b/a/r$1;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/a/r$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    return-void
.end method
