.class public Lcom/mastercard/mchipengine/d/b/a/d;
.super Lcom/mastercard/mchipengine/d/b/a/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mchipengine/d/b/a/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/a/f<",
        "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/f;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/d;-><init>()V

    return-void
.end method

.method public static a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    const-string v0, "9F34"

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public static getBuilder()Lcom/mastercard/mchipengine/d/b/a/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mastercard/mchipengine/d/b/a/k<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mastercard/mchipengine/d/b/a/d$1;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/a/d$1;-><init>()V

    return-object v0
.end method

.method private h()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/d;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/d;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    const/16 v2, 0x3f

    and-int/2addr v0, v2

    const/16 v3, 0x1f

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/d;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    aget-byte v0, v0, v1

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->h()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 4

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/d;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit8 v0, v0, 0x3f

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/d;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    aget-byte v0, v0, v1

    and-int/lit8 v0, v0, 0x3f

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/d;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    return v1
.end method

.method public final d()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/d;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/d;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    const/16 v1, 0x3f

    and-int/2addr v0, v1

    int-to-byte v0, v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    sget v0, Lcom/mastercard/mchipengine/d/b/a/d$a;->g:I

    return v0

    :pswitch_0
    sget v0, Lcom/mastercard/mchipengine/d/b/a/d$a;->e:I

    return v0

    :pswitch_1
    sget v0, Lcom/mastercard/mchipengine/d/b/a/d$a;->d:I

    return v0

    :pswitch_2
    sget v0, Lcom/mastercard/mchipengine/d/b/a/d$a;->c:I

    return v0

    :pswitch_3
    sget v0, Lcom/mastercard/mchipengine/d/b/a/d$a;->a:I

    return v0

    :cond_0
    sget v0, Lcom/mastercard/mchipengine/d/b/a/d$a;->f:I

    return v0

    :cond_1
    :pswitch_4
    sget v0, Lcom/mastercard/mchipengine/d/b/a/d$a;->b:I

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1e
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
