.class public Lcom/mastercard/mchipengine/d/b/a/n;
.super Lcom/mastercard/mchipengine/d/b/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/a/f<",
        "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/f;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/a/n;-><init>()V

    return-void
.end method

.method public static a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    const-string v0, "9F1D"

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public static getBuilder()Lcom/mastercard/mchipengine/d/b/a/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mastercard/mchipengine/d/b/a/k<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mastercard/mchipengine/d/b/a/n$1;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/a/n$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    invoke-super {p0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/n;->a:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/n;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/n;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b/a/n;->a:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v1

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/n;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b/a/n;->a:Ljava/lang/Object;

    check-cast v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    const/4 v2, 0x4

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v1
.end method
