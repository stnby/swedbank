.class public final Lcom/mastercard/mchipengine/d/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/b/a/k;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/b/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mastercard/mchipengine/d/a;->a:Ljava/util/HashMap;

    invoke-static {}, Lcom/mastercard/mchipengine/d/c;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    :try_start_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    const-string v3, "getBuilder"

    const/4 v4, 0x0

    new-array v5, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/d/b/a/k;

    sget-object v3, Lcom/mastercard/mchipengine/d/a;->a:Ljava/util/HashMap;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/a;->c:Ljava/util/LinkedHashMap;

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/a;->d:Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void
.end method

.method public static c(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/mastercard/mchipengine/d/b/a/f;",
            ">;)TK;"
        }
    .end annotation

    invoke-static {}, Lcom/mastercard/mchipengine/d/c;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    sget-object v0, Lcom/mastercard/mchipengine/d/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/d/b/a/k;

    invoke-interface {p0}, Lcom/mastercard/mchipengine/d/b/a/k;->a()Lcom/mastercard/mchipengine/d/b/a/f;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/d/b/a/b;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/b;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "*>;)TK;"
        }
    .end annotation

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/c;->a(Ljava/lang/Class;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object v0, v1, p1

    new-instance p1, Lcom/mastercard/mchipengine/d/a/e;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/d/a/e;-><init>()V

    throw p1
.end method

.method public final a(Lcom/mastercard/mchipengine/d/b/a/f;)V
    .locals 4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/d/c;->a(Ljava/lang/Class;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/f;->f()Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x2

    aput-object p1, v1, v0

    return-void
.end method

.method public final a(Lcom/mastercard/mchipengine/d/d;)V
    .locals 5

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    sget-object v0, Lcom/mastercard/mchipengine/d/a;->a:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/k;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-interface {v0}, Lcom/mastercard/mchipengine/d/b/a/k;->a()Lcom/mastercard/mchipengine/d/b/a/f;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/d/a;->c:Ljava/util/LinkedHashMap;

    iget-object v3, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v3, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array v0, v2, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    iget-object v3, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    iget-object v3, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/f;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "|"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "|"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, p1}, Lcom/mastercard/mchipengine/d/b/a/f;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    :cond_2
    return-void
.end method

.method public final b()Lcom/mastercard/mchipengine/d/b/a/q;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/q;

    return-object v0
.end method

.method public final b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/mastercard/mchipengine/d/b/a/f;",
            ">;)TK;"
        }
    .end annotation

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/c;->a(Ljava/lang/Class;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object v0, v1, p1

    sget-object p1, Lcom/mastercard/mchipengine/d/a;->a:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/k;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/d/b/a/k;->a()Lcom/mastercard/mchipengine/d/b/a/f;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lcom/mastercard/mchipengine/d/b/a/n;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/n;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/n;

    return-object v0
.end method

.method public final d()Lcom/mastercard/mchipengine/d/b/a/m;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/m;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/m;

    return-object v0
.end method

.method public final d(Ljava/lang/Class;)Lcom/mastercard/mchipengine/d/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/mastercard/mchipengine/d/b/a/f;",
            ">;)",
            "Lcom/mastercard/mchipengine/d/d;"
        }
    .end annotation

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/c;->a(Ljava/lang/Class;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p1, Lcom/mastercard/mchipengine/d/d;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/a;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mchipengine/d/b/a/f;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/d/b/a/f;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-object p1

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object v0, v1, p1

    new-instance p1, Lcom/mastercard/mchipengine/d/a/e;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/d/a/e;-><init>()V

    throw p1
.end method

.method public final e()Lcom/mastercard/mchipengine/d/b/a/j;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/j;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/j;

    return-object v0
.end method

.method public final f()Lcom/mastercard/mchipengine/d/b/a/t;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/t;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/t;

    return-object v0
.end method

.method public final g()Lcom/mastercard/mchipengine/d/b/a/u;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/u;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/u;

    return-object v0
.end method

.method public final h()Lcom/mastercard/mchipengine/d/b/a/d;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/d;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/d;

    return-object v0
.end method

.method public final i()Lcom/mastercard/mchipengine/d/b/a/l;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/d/b/a/l;

    invoke-virtual {p0, v0}, Lcom/mastercard/mchipengine/d/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/b/a/l;

    return-object v0
.end method
