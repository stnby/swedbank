.class public final Lcom/mastercard/mchipengine/d/b;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/mastercard/mchipengine/d/b/b/b;

.field public b:Lcom/mastercard/mchipengine/d/b/b/c;

.field public c:Lcom/mastercard/mchipengine/d/b/b/e;

.field public d:Lcom/mastercard/mchipengine/d/b/b/a;

.field private e:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/mastercard/mchipengine/utils/MChipLogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b;->f:Lcom/mastercard/mchipengine/utils/MChipLogger;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b;->e:Ljava/util/LinkedHashMap;

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/d/d;
    .locals 2

    sget-object v0, Lcom/mastercard/mchipengine/apduprocessing/a/c;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/b;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mchipengine/d/d;

    return-object v0
.end method

.method public final a(ILcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/mastercard/mchipengine/d/b/b/e;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/b/e;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-eq p2, v0, :cond_3

    const/16 v0, 0x8

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    sget-object v5, Lcom/mastercard/mchipengine/d/b$1;->a:[I

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v4, p0, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    invoke-virtual {v4, v3, v0}, Lcom/mastercard/mchipengine/d/b/b/e;->a(IB)V

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v4, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v2

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    invoke-virtual {v4, v3, v1}, Lcom/mastercard/mchipengine/d/b/b/e;->a(IB)V

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v4, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v2

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    invoke-virtual {v4, v3, v3}, Lcom/mastercard/mchipengine/d/b/b/e;->a(IB)V

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v4, v4, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast v4, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v2

    goto :goto_0

    :cond_0
    sget p3, Lcom/mastercard/mchipengine/assessment/b;->c:I

    if-eq p1, p3, :cond_1

    sget p3, Lcom/mastercard/mchipengine/assessment/b;->f:I

    if-ne p1, p3, :cond_2

    :cond_1
    iget-object p1, p0, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    const/16 p3, 0x10

    invoke-virtual {p1, v3, p3}, Lcom/mastercard/mchipengine/d/b/b/e;->a(IB)V

    new-array p3, v3, [Ljava/lang/Object;

    iget-object p1, p1, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast p1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toBinaryString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v2

    :cond_2
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    if-ne p2, p1, :cond_3

    iget-object p1, p0, Lcom/mastercard/mchipengine/d/b;->c:Lcom/mastercard/mchipengine/d/b/b/e;

    iget-object p2, p1, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p2

    aget-byte p2, p2, v3

    and-int/2addr p2, v3

    if-eq p2, v3, :cond_3

    iget-object p2, p1, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p2

    aget-byte p2, p2, v3

    and-int/2addr p2, v1

    if-eq p2, v1, :cond_3

    iget-object p2, p1, Lcom/mastercard/mchipengine/d/b/b/e;->b:Ljava/lang/Object;

    check-cast p2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p2

    aget-byte p2, p2, v3

    and-int/2addr p2, v0

    if-eq p2, v0, :cond_3

    invoke-virtual {p1, v1, v3}, Lcom/mastercard/mchipengine/d/b/b/e;->a(IB)V

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/mastercard/mchipengine/d/d;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/b;->e:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/d/b/b/b;

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/d/b/b/b;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    return-void
.end method

.method public final b()V
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/d/b/b/c;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/d/b/b/c;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/d/b;->b:Lcom/mastercard/mchipengine/d/b/b/c;

    return-void
.end method
