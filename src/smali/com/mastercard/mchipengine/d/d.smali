.class public final Lcom/mastercard/mchipengine/d/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/utils/Wipeable;


# instance fields
.field public a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

.field public c:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/mastercard/mchipengine/utils/b;->a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    iput-object p1, p0, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p1

    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/mastercard/mchipengine/utils/b;->a(J)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/d/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iput-object p2, p0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/d/a/a;

    const-string p2, "Null TLV value"

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/d/a/c;

    invoke-direct {p1}, Lcom/mastercard/mchipengine/d/a/c;-><init>()V

    throw p1
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Ljava/lang/Iterable<",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/b;->a(Ljava/lang/Iterable;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-void
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;[Lcom/mastercard/mchipengine/d/d;)V
    .locals 0

    invoke-static {p2}, Lcom/mastercard/mchipengine/utils/b;->a([Lcom/mastercard/mchipengine/d/d;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-void
.end method

.method public static a([B)Lcom/mastercard/mchipengine/d/d;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/mastercard/mchipengine/d/d;->a([BI)Lcom/mastercard/mchipengine/d/d;

    move-result-object p0

    return-object p0
.end method

.method private static a([BI)Lcom/mastercard/mchipengine/d/d;
    .locals 8

    if-eqz p0, :cond_5

    array-length v0, p0

    if-lez v0, :cond_5

    if-ltz p1, :cond_4

    array-length v0, p0

    if-ge p1, v0, :cond_3

    invoke-static {p0, p1}, Lcom/mastercard/mchipengine/utils/b;->b([BI)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    array-length v1, p0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    add-int/2addr v2, p1

    if-le v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    add-int/2addr v1, p1

    aget-byte v1, p0, v1

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/b;->a(B)I

    move-result v1

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    add-int/2addr v2, p1

    invoke-static {p0, v2}, Lcom/mastercard/mchipengine/utils/b;->a([BI)J

    move-result-wide v2

    const-wide/32 v4, 0x4ffffff7

    cmp-long v4, v2, v4

    if-gtz v4, :cond_1

    array-length v4, p0

    int-to-long v4, v4

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v6

    add-int/2addr v6, p1

    add-int/2addr v6, v1

    int-to-long v6, v6

    add-long/2addr v6, v2

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    long-to-int v2, v2

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v5

    add-int/2addr p1, v5

    add-int/2addr p1, v1

    invoke-virtual {v3, v4, p0, p1, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[BII)V

    new-instance p0, Lcom/mastercard/mchipengine/d/d;

    invoke-direct {p0, v0, v3}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-object p0

    :cond_0
    new-instance p0, Lcom/mastercard/mchipengine/d/a/b;

    const-string p1, "Not enough data in value field"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Lcom/mastercard/mchipengine/d/a/b;

    const-string p1, "The length part represents too big number. Max is 1342177271"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance p0, Lcom/mastercard/mchipengine/d/a/b;

    const-string p1, "Not enough data in length field (no length field)"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_3
    new-instance p0, Lcom/mastercard/mchipengine/d/a/b;

    const-string p1, "Initial offset is beyond the array"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_4
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Initial offset cannot be negative"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_5
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Null or empty input data"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/util/LinkedHashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            ")",
            "Ljava/util/LinkedHashMap<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    invoke-static {v2, v1}, Lcom/mastercard/mchipengine/d/d;->a([BI)Lcom/mastercard/mchipengine/d/d;

    move-result-object v2

    iget-object v3, v2, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/d;->b()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/d;->b()I

    move-result v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/d/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    iget-object v2, p0, Lcom/mastercard/mchipengine/d/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()I
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/d/d;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/d;->b:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, "TlvObject"

    return-object v0
.end method

.method public final wipe()V
    .locals 0

    return-void
.end method
