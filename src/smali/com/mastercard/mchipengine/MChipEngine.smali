.class public Lcom/mastercard/mchipengine/MChipEngine;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mchipengine/MChipEngine$a;
    }
.end annotation


# static fields
.field private static mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;


# instance fields
.field private mChipLogger:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

.field private mIsContactlessSupported:Z

.field private mIsDsrpSupported:Z

.field private mState$75d4c8f6:I

.field private mTransactionEngineFactory:Lcom/mastercard/mchipengine/g/c/d;

.field private mWalletTransactionListener:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;",
            "Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/mastercard/mchipengine/MChipEngine$a;->a:I

    iput v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mChipLogger:Lcom/mastercard/mchipengine/utils/MChipLogger;

    iput-object p6, p0, Lcom/mastercard/mchipengine/MChipEngine;->mWalletTransactionListener:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;

    if-nez p3, :cond_0

    :try_start_0
    new-instance p3, Lcom/mastercard/mchipengine/utils/c;

    invoke-direct {p3}, Lcom/mastercard/mchipengine/utils/c;-><init>()V

    goto :goto_0

    :catch_0
    move-exception p1

    goto/16 :goto_1

    :cond_0
    :goto_0
    if-nez p7, :cond_1

    new-instance p7, Lcom/mastercard/mchipengine/utils/d;

    invoke-direct {p7}, Lcom/mastercard/mchipengine/utils/d;-><init>()V

    :cond_1
    iget-object p6, p0, Lcom/mastercard/mchipengine/MChipEngine;->mWalletTransactionListener:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;

    if-nez p6, :cond_2

    new-instance p6, Lcom/mastercard/mchipengine/utils/e;

    invoke-direct {p6}, Lcom/mastercard/mchipengine/utils/e;-><init>()V

    iput-object p6, p0, Lcom/mastercard/mchipengine/MChipEngine;->mWalletTransactionListener:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;

    :cond_2
    if-eqz p1, :cond_7

    if-eqz p2, :cond_6

    if-eqz p4, :cond_5

    if-eqz p5, :cond_4

    new-instance p6, Lcom/mastercard/mchipengine/b/b;

    invoke-direct {p6, p1, p8, p9}, Lcom/mastercard/mchipengine/b/b;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletprofile/MChipEngineProfile;Ljava/util/List;Ljava/util/List;)V

    iget-object p1, p6, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean p1, p1, Lcom/mastercard/mchipengine/b/j;->c:Z

    iput-boolean p1, p0, Lcom/mastercard/mchipengine/MChipEngine;->mIsContactlessSupported:Z

    iget-object p1, p6, Lcom/mastercard/mchipengine/b/b;->a:Lcom/mastercard/mchipengine/b/j;

    iget-boolean p1, p1, Lcom/mastercard/mchipengine/b/j;->d:Z

    iput-boolean p1, p0, Lcom/mastercard/mchipengine/MChipEngine;->mIsDsrpSupported:Z

    new-instance p1, Lcom/mastercard/mchipengine/g/b;

    invoke-direct {p1, p7, p5, p4, p3}, Lcom/mastercard/mchipengine/g/b;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletCdCvmManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/TransactionCredentialsManager;)V

    sget p3, Lcom/mastercard/mchipengine/MChipEngine$a;->a:I

    iput p3, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    new-instance p3, Lcom/mastercard/mchipengine/g/c/d;

    invoke-direct {p3, p2, p1, p6, p0}, Lcom/mastercard/mchipengine/g/c/d;-><init>(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/McbpCrypto2D;Lcom/mastercard/mchipengine/g/b;Lcom/mastercard/mchipengine/b/b;Lcom/mastercard/mchipengine/a;)V

    iput-object p3, p0, Lcom/mastercard/mchipengine/MChipEngine;->mTransactionEngineFactory:Lcom/mastercard/mchipengine/g/c/d;

    invoke-direct {p0}, Lcom/mastercard/mchipengine/MChipEngine;->initCommandHandlers()V

    sget-object p1, Lcom/mastercard/mchipengine/MChipEngine;->mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;

    if-nez p1, :cond_3

    new-instance p1, Lcom/mastercard/mchipengine/utils/f;

    const-string p3, "AuthenticationTimer"

    const p4, 0x493e0

    const-wide/16 p5, 0x3e8

    new-instance p7, Lcom/mastercard/mchipengine/MChipEngine$1;

    invoke-direct {p7, p0}, Lcom/mastercard/mchipengine/MChipEngine$1;-><init>(Lcom/mastercard/mchipengine/MChipEngine;)V

    move-object p2, p1

    invoke-direct/range {p2 .. p7}, Lcom/mastercard/mchipengine/utils/f;-><init>(Ljava/lang/String;IJLcom/mastercard/mchipengine/utils/f$a;)V

    sput-object p1, Lcom/mastercard/mchipengine/MChipEngine;->mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;

    :cond_3
    return-void

    :cond_4
    new-instance p1, Lcom/mastercard/mchipengine/e/f;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->g:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/f;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_5
    new-instance p1, Lcom/mastercard/mchipengine/e/f;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->g:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/f;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_6
    new-instance p1, Lcom/mastercard/mchipengine/e/f;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->g:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/f;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_7
    new-instance p1, Lcom/mastercard/mchipengine/e/f;

    sget-object p2, Lcom/mastercard/mchipengine/e/a;->an:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/e/f;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/e;->getMessage()Ljava/lang/String;

    move-result-object p4

    aput-object p4, p2, p3

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/e;->c()V

    return-void
.end method

.method static synthetic access$000(Lcom/mastercard/mchipengine/MChipEngine;)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/MChipEngine;->onWaitingForAuthenticationTimedOut()V

    return-void
.end method

.method static synthetic access$100(Lcom/mastercard/mchipengine/MChipEngine;)Lcom/mastercard/mchipengine/utils/MChipLogger;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mChipLogger:Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-object p0
.end method

.method static synthetic access$200(Lcom/mastercard/mchipengine/MChipEngine;)Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;
    .locals 0

    iget-object p0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mWalletTransactionListener:Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;

    return-object p0
.end method

.method private initCommandHandlers()V
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/i;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/i;-><init>()V

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/g;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/g;-><init>()V

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/h;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/h;-><init>()V

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/e;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/e;-><init>()V

    new-instance v0, Lcom/mastercard/mchipengine/apduprocessing/commands/c;

    invoke-direct {v0}, Lcom/mastercard/mchipengine/apduprocessing/commands/c;-><init>()V

    return-void
.end method

.method private onTransactionComplete(Lcom/mastercard/mchipengine/g/d/d;)V
    .locals 1

    sget v0, Lcom/mastercard/mchipengine/MChipEngine$a;->a:I

    iput v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/c/c;->c()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    if-eqz p1, :cond_1

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionCompleteAsync(Lcom/mastercard/mchipengine/g/d/d;)V

    :cond_1
    return-void
.end method

.method private onTransactionCompleteAsync(Lcom/mastercard/mchipengine/g/d/d;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mastercard/mchipengine/MChipEngine$4;

    invoke-direct {v1, p0, p1}, Lcom/mastercard/mchipengine/MChipEngine$4;-><init>(Lcom/mastercard/mchipengine/MChipEngine;Lcom/mastercard/mchipengine/g/d/d;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private onTransactionErrorAsync(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mastercard/mchipengine/MChipEngine$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/mastercard/mchipengine/MChipEngine$3;-><init>(Lcom/mastercard/mchipengine/MChipEngine;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private onTransactionIncidentAsync(Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mastercard/mchipengine/MChipEngine$2;

    invoke-direct {v1, p0, p1}, Lcom/mastercard/mchipengine/MChipEngine$2;-><init>(Lcom/mastercard/mchipengine/MChipEngine;Ljava/lang/Exception;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private declared-synchronized onWaitingForAuthenticationTimedOut()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionComplete(Lcom/mastercard/mchipengine/g/d/d;)V

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->e:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    invoke-direct {p0, v0, v1}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionErrorAsync(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized onNotReceivedApduWithinTimeLimit()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionComplete(Lcom/mastercard/mchipengine/g/d/d;)V

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->TERMINAL_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->R:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    invoke-direct {p0, v0, v1}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionErrorAsync(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onTransactionProcessingFinished(Lcom/mastercard/mchipengine/g/d/d;)V
    .locals 2

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/d/d;->getTransactionOutcome()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->WALLET_ACTION_REQUIRED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/mastercard/mchipengine/MChipEngine;->mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->a()V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/mastercard/mchipengine/MChipEngine;->mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->b()V

    :goto_0
    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionComplete(Lcom/mastercard/mchipengine/g/d/d;)V

    return-void
.end method

.method public declared-synchronized processApdu([B)[B
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mIsContactlessSupported:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/mastercard/mchipengine/MChipEngine;->mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->b()V

    iget v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    sget v1, Lcom/mastercard/mchipengine/MChipEngine$a;->a:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mTransactionEngineFactory:Lcom/mastercard/mchipengine/g/c/d;

    sget v1, Lcom/mastercard/mchipengine/g/c/d$a;->a:I

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/g/c/d;->a(I)Lcom/mastercard/mchipengine/g/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/g/c/c;->a()Z

    sget v0, Lcom/mastercard/mchipengine/MChipEngine$a;->b:I

    iput v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/c/a;

    invoke-virtual {v0, p1}, Lcom/mastercard/mchipengine/g/c/a;->a([B)[B

    move-result-object p1
    :try_end_0
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    sget v1, Lcom/mastercard/mchipengine/MChipEngine$a;->b:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    check-cast v0, Lcom/mastercard/mchipengine/g/c/a;

    invoke-virtual {v0, p1}, Lcom/mastercard/mchipengine/g/c/a;->a([B)[B

    move-result-object p1
    :try_end_1
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_1
    :try_start_2
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->f:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/mastercard/mchipengine/e/b;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->m:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
    :try_end_2
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_3
    instance-of v0, p1, Lcom/mastercard/mchipengine/e/b;

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionComplete(Lcom/mastercard/mchipengine/g/d/d;)V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/e;->d()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionErrorAsync(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionIncidentAsync(Ljava/lang/Exception;)V

    :goto_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/e;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/e;->b()Lcom/mastercard/mchipengine/e/a;

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mchipengine/apduprocessing/f;->a(Lcom/mastercard/mchipengine/e/a;)Lcom/mastercard/mchipengine/apduprocessing/e;

    move-result-object p1

    iget-object p1, p1, Lcom/mastercard/mchipengine/apduprocessing/e;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p1

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized processDsrp(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;)Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;
    .locals 8

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    iget-boolean v3, p0, Lcom/mastercard/mchipengine/MChipEngine;->mIsDsrpSupported:Z

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    sget v4, Lcom/mastercard/mchipengine/MChipEngine$a;->a:I

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mastercard/mchipengine/MChipEngine;->mTransactionEngineFactory:Lcom/mastercard/mchipengine/g/c/d;

    sget v4, Lcom/mastercard/mchipengine/g/c/d$a;->b:I

    invoke-virtual {v3, v4}, Lcom/mastercard/mchipengine/g/c/d;->a(I)Lcom/mastercard/mchipengine/g/c/c;

    move-result-object v3

    iput-object v3, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    iget-object v3, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/g/c/c;->b()Lcom/mastercard/mchipengine/g/b/c;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mchipengine/g/b/b;

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getAmount()[B

    move-result-object v5

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCountryCode()[B

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCountryCode()[B

    move-result-object v5

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    goto :goto_0

    :cond_0
    move-object v5, v2

    :goto_0
    aput-object v5, v4, v1

    const/4 v5, 0x2

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCurrencyCode()[B

    move-result-object v6

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    new-instance v6, Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getTransactionDate()[B

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/mastercard/mchipengine/utils/MChipDate;-><init>([B)V

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getTransactionType()B

    move-result v6

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getUnpredictableNumber()[B

    move-result-object v6

    invoke-static {v6}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v6

    aput-object v6, v4, v5
    :try_end_0
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCurrencyCode()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getUnpredictableNumber()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getAmount()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    new-instance v4, Lcom/mastercard/mchipengine/utils/MChipDate;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getTransactionDate()[B

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/mastercard/mchipengine/utils/MChipDate;-><init>([B)V
    :try_end_1
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v4, Lcom/mastercard/mchipengine/g/d/e;

    invoke-direct {v4}, Lcom/mastercard/mchipengine/g/d/e;-><init>()V

    iput-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    iget-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getAmount()[B

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/mchipengine/g/d/e;->a:[B

    iget-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCurrencyCode()[B

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/mchipengine/g/d/e;->b:[B

    iget-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getTransactionType()B

    move-result v5

    iput-byte v5, v4, Lcom/mastercard/mchipengine/g/d/e;->c:B

    iget-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getUnpredictableNumber()[B

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/mchipengine/g/d/e;->d:[B

    iget-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getTransactionDate()[B

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/mchipengine/g/d/e;->f:[B

    iget-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCountryCode()[B

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/mchipengine/g/d/e;->g:[B

    iget-object v4, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpInputData;->getCryptogramType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    move-result-object p1

    iput-object p1, v4, Lcom/mastercard/mchipengine/g/d/e;->e:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    new-array p1, v1, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/mastercard/mchipengine/g/b/b;->a:Lcom/mastercard/mchipengine/g/d/e;

    aput-object v3, p1, v0

    iget-object p1, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/c/c;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    check-cast p1, Lcom/mastercard/mchipengine/g/c/b;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/g/c/b;->d()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DsrpOutputData;

    move-result-object p1

    iput-object v2, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;
    :try_end_2
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_1
    :try_start_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :catch_0
    move-exception p1

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/a/a;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    new-instance p1, Lcom/mastercard/mchipengine/e/d;

    sget-object v3, Lcom/mastercard/mchipengine/e/a;->a:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v3}, Lcom/mastercard/mchipengine/e/d;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1

    :cond_3
    new-instance p1, Lcom/mastercard/mchipengine/e/d;

    sget-object v3, Lcom/mastercard/mchipengine/e/a;->b:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p1, v3}, Lcom/mastercard/mchipengine/e/d;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p1
    :try_end_3
    .catch Lcom/mastercard/mchipengine/e/e; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected Error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    throw p1

    :catch_2
    move-exception p1

    iput-object v2, p0, Lcom/mastercard/mchipengine/MChipEngine;->mCurrentTransactionEngine:Lcom/mastercard/mchipengine/g/c/c;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/e;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/e/e;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-object v2

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method setPreviousTapContext(Lcom/mastercard/mchipengine/g/d/g;)V
    .locals 0

    invoke-static {p1}, Lcom/mastercard/mchipengine/g/b/a;->a(Lcom/mastercard/mchipengine/g/d/g;)V

    return-void
.end method

.method public declared-synchronized stopContactlessTransaction()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    sget v1, Lcom/mastercard/mchipengine/MChipEngine$a;->b:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    invoke-direct {p0, v2}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionComplete(Lcom/mastercard/mchipengine/g/d/d;)V

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->WALLET_CANCEL_REQUEST:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    new-instance v1, Lcom/mastercard/mchipengine/e/b;

    sget-object v2, Lcom/mastercard/mchipengine/e/a;->e:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {v1, v2}, Lcom/mastercard/mchipengine/e/b;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    invoke-direct {p0, v0, v1}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionErrorAsync(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V

    sget-object v0, Lcom/mastercard/mchipengine/MChipEngine;->mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/mastercard/mchipengine/MChipEngine;->mState$75d4c8f6:I

    sget v1, Lcom/mastercard/mchipengine/MChipEngine$a;->a:I

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/mastercard/mchipengine/g/b/a;->b()V

    invoke-direct {p0, v2}, Lcom/mastercard/mchipengine/MChipEngine;->onTransactionComplete(Lcom/mastercard/mchipengine/g/d/d;)V

    sget-object v0, Lcom/mastercard/mchipengine/MChipEngine;->mWaitingForAuthenticationTimer:Lcom/mastercard/mchipengine/utils/f;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/f;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
