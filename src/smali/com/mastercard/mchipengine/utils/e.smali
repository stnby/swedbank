.class public final Lcom/mastercard/mchipengine/utils/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletContactlessTransactionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onContactlessTransactionAbort(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final onContactlessTransactionCompleted(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
    .locals 0

    return-void
.end method

.method public final onContactlessTransactionIncident(Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method
