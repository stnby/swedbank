.class public final enum Lcom/mastercard/mchipengine/utils/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/utils/b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic a:[Lcom/mastercard/mchipengine/utils/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/mastercard/mchipengine/utils/b;

    sput-object v0, Lcom/mastercard/mchipengine/utils/b;->a:[Lcom/mastercard/mchipengine/utils/b;

    return-void
.end method

.method public static a(B)I
    .locals 2

    and-int/lit16 v0, p0, 0xff

    const/16 v1, 0x84

    if-gt v0, v1, :cond_1

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x1

    return p0

    :pswitch_0
    const/4 p0, 0x5

    return p0

    :pswitch_1
    const/4 p0, 0x4

    return p0

    :pswitch_2
    const/4 p0, 0x3

    return p0

    :pswitch_3
    const/4 p0, 0x2

    return p0

    :cond_0
    new-instance p0, Lcom/mastercard/mchipengine/d/a/b;

    const-string v0, "Incorrect first byte of length. Cannot be 0x80"

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Lcom/mastercard/mchipengine/d/a/b;

    const-string v0, "Incorrect first byte of length. Max value 0x84"

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw p0

    nop

    :pswitch_data_0
    .packed-switch -0x7f
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a([B)J
    .locals 10

    if-eqz p0, :cond_4

    array-length v0, p0

    if-lez v0, :cond_4

    array-length v0, p0

    const/4 v1, 0x5

    const/4 v2, 0x4

    const/16 v3, 0x18

    const/4 v4, 0x3

    const-wide/16 v5, 0xff

    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-lt v0, v1, :cond_0

    aget-byte v0, p0, v9

    int-to-long v0, v0

    and-long/2addr v0, v5

    const/16 v9, 0x20

    shl-long/2addr v0, v9

    aget-byte v8, p0, v8

    int-to-long v8, v8

    and-long/2addr v5, v8

    shl-long/2addr v5, v3

    or-long/2addr v0, v5

    aget-byte v3, p0, v7

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    int-to-long v5, v3

    or-long/2addr v0, v5

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    int-to-long v3, v3

    or-long/2addr v0, v3

    aget-byte p0, p0, v2

    and-int/lit16 p0, p0, 0xff

    int-to-long v2, p0

    or-long/2addr v0, v2

    return-wide v0

    :cond_0
    array-length v0, p0

    if-ne v0, v2, :cond_1

    aget-byte v0, p0, v9

    int-to-long v0, v0

    and-long/2addr v0, v5

    shl-long/2addr v0, v3

    aget-byte v2, p0, v8

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    int-to-long v2, v2

    or-long/2addr v0, v2

    aget-byte v2, p0, v7

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    int-to-long v2, v2

    or-long/2addr v0, v2

    aget-byte p0, p0, v4

    and-int/lit16 p0, p0, 0xff

    int-to-long v2, p0

    or-long/2addr v0, v2

    return-wide v0

    :cond_1
    array-length v0, p0

    if-ne v0, v4, :cond_2

    aget-byte v0, p0, v9

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    aget-byte v1, p0, v8

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    aget-byte p0, p0, v7

    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, v0

    int-to-long v0, p0

    return-wide v0

    :cond_2
    array-length v0, p0

    if-ne v0, v7, :cond_3

    aget-byte v0, p0, v9

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    aget-byte p0, p0, v8

    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, v0

    int-to-long v0, p0

    return-wide v0

    :cond_3
    aget-byte p0, p0, v9

    and-int/lit16 p0, p0, 0xff

    int-to-long v0, p0

    return-wide v0

    :cond_4
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string v0, "Null or empty input"

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a([BI)J
    .locals 7

    if-eqz p0, :cond_7

    array-length v0, p0

    if-lez v0, :cond_7

    array-length v0, p0

    if-ge p1, v0, :cond_6

    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0x84

    if-gt v0, v1, :cond_5

    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0x80

    if-eq v0, v1, :cond_4

    aget-byte v0, p0, p1

    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    packed-switch v0, :pswitch_data_0

    new-array v0, v4, [B

    aget-byte p0, p0, p1

    aput-byte p0, v0, v2

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide p0

    return-wide p0

    :pswitch_0
    add-int/lit8 v0, p1, 0x4

    array-length v5, p0

    if-ge v0, v5, :cond_0

    const/4 v5, 0x4

    new-array v5, v5, [B

    add-int/lit8 v6, p1, 0x1

    aget-byte v6, p0, v6

    aput-byte v6, v5, v2

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    aput-byte v2, v5, v4

    add-int/2addr p1, v1

    aget-byte p1, p0, p1

    aput-byte p1, v5, v3

    aget-byte p0, p0, v0

    aput-byte p0, v5, v1

    invoke-static {v5}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide p0

    return-wide p0

    :cond_0
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Incorrect start offset or not enough data in the array"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_1
    add-int/lit8 v0, p1, 0x3

    array-length v5, p0

    if-ge v0, v5, :cond_1

    new-array v1, v1, [B

    add-int/lit8 v5, p1, 0x1

    aget-byte v5, p0, v5

    aput-byte v5, v1, v2

    add-int/2addr p1, v3

    aget-byte p1, p0, p1

    aput-byte p1, v1, v4

    aget-byte p0, p0, v0

    aput-byte p0, v1, v3

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide p0

    return-wide p0

    :cond_1
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Incorrect start offset or not enough data in the array"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_2
    add-int/lit8 v0, p1, 0x2

    array-length v1, p0

    if-ge v0, v1, :cond_2

    new-array v1, v3, [B

    add-int/2addr p1, v4

    aget-byte p1, p0, p1

    aput-byte p1, v1, v2

    aget-byte p0, p0, v0

    aput-byte p0, v1, v4

    invoke-static {v1}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide p0

    return-wide p0

    :cond_2
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Incorrect start offset or not enough data in the array"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_3
    add-int/2addr p1, v4

    array-length v0, p0

    if-ge p1, v0, :cond_3

    new-array v0, v4, [B

    aget-byte p0, p0, p1

    aput-byte p0, v0, v2

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/b;->a([B)J

    move-result-wide p0

    return-wide p0

    :cond_3
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Incorrect start offset or not enough data in the array"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_4
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Incorrect first byte of length. Cannot be 0x80"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_5
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Incorrect first byte of length. Max value 0x84"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_6
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Offset beyond array bounds"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_7
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Null or empty input"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    nop

    :pswitch_data_0
    .packed-switch -0x7f
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(J)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 18

    move-wide/from16 v0, p0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_5

    const-wide/16 v2, 0x7f

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-wide/16 v5, 0xff

    cmp-long v7, v0, v5

    if-gtz v7, :cond_1

    invoke-static {v4}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v4

    const/16 v7, -0x7f

    invoke-virtual {v4, v3, v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long/2addr v0, v5

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-virtual {v4, v2, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-object v4

    :cond_1
    const-wide/32 v7, 0xffff

    const/16 v9, 0x8

    const-wide/32 v10, 0xff00

    const/4 v12, 0x3

    cmp-long v7, v0, v7

    if-gtz v7, :cond_2

    invoke-static {v12}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v7

    const/16 v8, -0x7e

    invoke-virtual {v7, v3, v8}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long/2addr v10, v0

    shr-long v8, v10, v9

    long-to-int v3, v8

    int-to-byte v3, v3

    invoke-virtual {v7, v2, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long/2addr v0, v5

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-virtual {v7, v4, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-object v7

    :cond_2
    const-wide/32 v7, 0xffffff

    const/16 v13, 0x10

    const-wide/32 v14, 0xff0000

    const/4 v12, 0x4

    cmp-long v7, v0, v7

    if-gtz v7, :cond_3

    invoke-static {v12}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v7

    const/16 v8, -0x7d

    invoke-virtual {v7, v3, v8}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long/2addr v14, v0

    shr-long v12, v14, v13

    long-to-int v3, v12

    int-to-byte v3, v3

    invoke-virtual {v7, v2, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long v2, v0, v10

    shr-long/2addr v2, v9

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v7, v4, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long/2addr v0, v5

    long-to-int v0, v0

    int-to-byte v0, v0

    const/4 v1, 0x3

    invoke-virtual {v7, v1, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-object v7

    :cond_3
    const-wide v7, 0xffffffffL

    cmp-long v7, v0, v7

    if-gtz v7, :cond_4

    const/4 v7, 0x5

    invoke-static {v7}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v7

    const/16 v8, -0x7c

    invoke-virtual {v7, v3, v8}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    const-wide/32 v16, -0x1000000

    and-long v16, v0, v16

    const/16 v3, 0x18

    shr-long v5, v16, v3

    long-to-int v3, v5

    int-to-byte v3, v3

    invoke-virtual {v7, v2, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long v2, v0, v14

    shr-long/2addr v2, v13

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v7, v4, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    and-long v2, v0, v10

    shr-long/2addr v2, v9

    long-to-int v2, v2

    int-to-byte v2, v2

    const/4 v3, 0x3

    invoke-virtual {v7, v3, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-virtual {v7, v12, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-object v7

    :cond_4
    new-instance v0, Lcom/mastercard/mchipengine/d/a/b;

    const-string v1, "The length cannot be bigger than 4 294 967 295"

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Lcom/mastercard/mchipengine/d/a/b;

    const-string v1, "The length cannot be smaller than 0"

    invoke-direct {v0, v1}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/mastercard/mchipengine/utils/MChipDate;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3

    const-string v0, ""

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipDate;->getYear()I

    move-result v1

    add-int/lit16 v1, v1, 0x7d0

    const/16 v2, 0x7da

    if-ge v1, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "0"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    rem-int/lit16 v1, v1, 0x7d0

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipDate;->getMonth()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipDate;->getMonth()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipDate;->getDay()I

    move-result v1

    if-ge v1, v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipDate;->getDay()I

    move-result p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/mastercard/mchipengine/d/d;",
            ">;)",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;"
        }
    .end annotation

    if-eqz p0, :cond_2

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/d/d;->b()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/d/d;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/d/d;->b()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_1

    :cond_1
    return-object v0

    :cond_2
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string v0, "Null data to convert"

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Ljava/util/List;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getTag()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendByte(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static a([Lcom/mastercard/mchipengine/d/d;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 6

    if-eqz p0, :cond_2

    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v4, p0, v2

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/d;->b()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    array-length v2, p0

    const/4 v3, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v4, p0, v1

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/d;->a()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/d/d;->b()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-object v0

    :cond_2
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string v0, "Null data to convert"

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(BI)Z
    .locals 1

    const/4 v0, 0x1

    shl-int p1, v0, p1

    and-int/2addr p0, p1

    if-eqz p0, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static a(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z
    .locals 5

    if-eqz p0, :cond_6

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p0

    const/4 v0, 0x0

    aget-byte v1, p0, v0

    const/16 v2, 0x1f

    and-int/2addr v1, v2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_4

    array-length v1, p0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    array-length v2, p0

    sub-int/2addr v2, v3

    const/16 v4, -0x80

    if-ge v1, v2, :cond_2

    aget-byte v2, p0, v1

    and-int/2addr v2, v4

    if-eq v2, v4, :cond_1

    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    array-length v1, p0

    sub-int/2addr v1, v3

    aget-byte p0, p0, v1

    and-int/2addr p0, v4

    if-ne p0, v4, :cond_3

    return v0

    :cond_3
    return v3

    :cond_4
    array-length p0, p0

    if-le p0, v3, :cond_5

    return v0

    :cond_5
    return v3

    :cond_6
    new-instance p0, Lcom/mastercard/mchipengine/d/a/c;

    const-string v0, "Invalid TLV tag (null or empty)"

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/a/c;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(I)[B
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p0

    return-object p0
.end method

.method public static b([B)I
    .locals 2

    const/4 v0, 0x0

    aget-byte v0, p0, v0

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    aget-byte p0, p0, v1

    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, v0

    const v0, 0xffff

    and-int/2addr p0, v0

    return p0
.end method

.method public static b(Ljava/util/List;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;)J"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;->getLength()B

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public static b(Ljava/lang/Iterable;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
            ">;)",
            "Lcom/mastercard/mchipengine/utils/MChipByteArray;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    :try_start_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v1, v2

    goto :goto_1

    :cond_1
    return-object v0

    :catch_0
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "Implementation error"

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static b([BI)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 5

    if-eqz p0, :cond_7

    array-length v0, p0

    if-lez v0, :cond_7

    array-length v0, p0

    if-ge p1, v0, :cond_6

    aget-byte v0, p0, p1

    const/16 v1, 0x1f

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_5

    aget-byte v0, p0, p1

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    const/4 v2, 0x1

    add-int/2addr p1, v2

    :goto_0
    array-length v3, p0

    const/16 v4, -0x80

    if-ge p1, v3, :cond_0

    aget-byte v3, p0, p1

    invoke-virtual {v0, v3}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendByte(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    aget-byte v3, p0, p1

    and-int/2addr v3, v4

    if-ne v3, v4, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p0

    if-ne p0, v2, :cond_2

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p0

    and-int/2addr p0, v1

    if-eq p0, v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Missing octet"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    :goto_1
    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p0

    if-le p0, v2, :cond_4

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p0

    sub-int/2addr p0, v2

    invoke-virtual {v0, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result p0

    and-int/2addr p0, v4

    if-eq p0, v4, :cond_3

    goto :goto_2

    :cond_3
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Missing octet"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_4
    :goto_2
    return-object v0

    :cond_5
    aget-byte p0, p0, p1

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    return-object p0

    :cond_6
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Offset beyond array bounds"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_7
    new-instance p0, Lcom/mastercard/mchipengine/d/a/a;

    const-string p1, "Null or empty input"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static b(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v2

    const/4 v3, 0x7

    :goto_1
    if-ltz v3, :cond_1

    invoke-static {v2, v3}, Lcom/mastercard/mchipengine/utils/b;->a(BI)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0x31

    goto :goto_2

    :cond_0
    const/16 v4, 0x30

    :goto_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-le v0, v1, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p0

    sub-int/2addr p0, v1

    :goto_3
    if-lez p0, :cond_3

    const/16 v1, 0x20

    invoke-virtual {v0, p0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    add-int/lit8 p0, p0, -0x8

    goto :goto_3

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_4
    return-object p0
.end method

.method public static c([B)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List<",
            "Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    invoke-static {p0, v1}, Lcom/mastercard/mchipengine/utils/b;->b([BI)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    array-length v3, p0

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v4

    add-int/2addr v4, v1

    if-le v3, v4, :cond_1

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v3

    add-int/2addr v3, v1

    aget-byte v3, p0, v3

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/b;->a(B)I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v4

    add-int/2addr v4, v1

    aget-byte v4, p0, v4

    new-instance v5, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object v6

    invoke-direct {v5, v6, v4}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    new-instance p0, Lcom/mastercard/mchipengine/e/g;

    sget-object v0, Lcom/mastercard/mchipengine/e/a;->S:Lcom/mastercard/mchipengine/e/a;

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/e/g;-><init>(Lcom/mastercard/mchipengine/e/a;)V

    throw p0

    :cond_1
    new-instance p0, Lcom/mastercard/mchipengine/d/a/b;

    const-string v0, "Not enough data in length field (no length field)"

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/d/a/b;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    return-object v0
.end method

.method public static d([B)Z
    .locals 5

    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-byte v4, p0, v2

    or-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    if-nez v3, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    return v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/b;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/utils/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/utils/b;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/utils/b;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/utils/b;->a:[Lcom/mastercard/mchipengine/utils/b;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/utils/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/utils/b;

    return-object v0
.end method
