.class public final Lcom/mastercard/mchipengine/utils/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/mchipengine/utils/f$a;
    }
.end annotation


# instance fields
.field final a:Lcom/mastercard/mchipengine/utils/f$a;

.field b:I

.field final c:J

.field private final d:Ljava/lang/String;

.field private e:I

.field private final f:Lcom/mastercard/mchipengine/utils/MChipLogger;

.field private g:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Ljava/lang/String;IJLcom/mastercard/mchipengine/utils/f$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/utils/f;->d:Ljava/lang/String;

    iput p2, p0, Lcom/mastercard/mchipengine/utils/f;->e:I

    iput-object p5, p0, Lcom/mastercard/mchipengine/utils/f;->a:Lcom/mastercard/mchipengine/utils/f$a;

    iput-wide p3, p0, Lcom/mastercard/mchipengine/utils/f;->c:J

    invoke-static {}, Lcom/mastercard/mchipengine/utils/g;->a()Lcom/mastercard/mchipengine/utils/MChipLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/mchipengine/utils/f;->f:Lcom/mastercard/mchipengine/utils/MChipLogger;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/f;->g:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/f;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/mastercard/mchipengine/utils/f;->g:Ljava/util/Timer;

    iget v0, p0, Lcom/mastercard/mchipengine/utils/f;->e:I

    iput v0, p0, Lcom/mastercard/mchipengine/utils/f;->b:I

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/f;->g:Ljava/util/Timer;

    new-instance v2, Lcom/mastercard/mchipengine/utils/f$1;

    invoke-direct {v2, p0}, Lcom/mastercard/mchipengine/utils/f$1;-><init>(Lcom/mastercard/mchipengine/utils/f;)V

    iget-wide v3, p0, Lcom/mastercard/mchipengine/utils/f;->c:J

    iget-wide v5, p0, Lcom/mastercard/mchipengine/utils/f;->c:J

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/f;->g:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/f;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    return-void
.end method
