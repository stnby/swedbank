.class public final enum Lcom/mastercard/mchipengine/utils/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/utils/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic a:[Lcom/mastercard/mchipengine/utils/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/mastercard/mchipengine/utils/a;

    sput-object v0, Lcom/mastercard/mchipengine/utils/a;->a:[Lcom/mastercard/mchipengine/utils/a;

    return-void
.end method

.method public static a([B)J
    .locals 2

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->toHexString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(J)[B
    .locals 10

    const/4 v0, 0x0

    const/4 v3, 0x0

    move-wide v1, p0

    :goto_0
    const-wide/16 v4, 0x0

    const-wide/16 v6, 0xa

    cmp-long v4, v1, v4

    if-eqz v4, :cond_0

    add-int/lit8 v3, v3, 0x1

    div-long/2addr v1, v6

    goto :goto_0

    :cond_0
    rem-int/lit8 v1, v3, 0x2

    if-nez v1, :cond_1

    div-int/lit8 v1, v3, 0x2

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v3, 0x1

    div-int/lit8 v1, v1, 0x2

    :goto_1
    new-array v2, v1, [B

    move-wide v4, p0

    const/4 p0, 0x0

    :goto_2
    if-ge p0, v3, :cond_3

    rem-long v8, v4, v6

    long-to-int p1, v8

    int-to-byte p1, p1

    rem-int/lit8 v8, p0, 0x2

    if-nez v8, :cond_2

    div-int/lit8 v8, p0, 0x2

    aput-byte p1, v2, v8

    goto :goto_3

    :cond_2
    div-int/lit8 v8, p0, 0x2

    aget-byte v9, v2, v8

    shl-int/lit8 p1, p1, 0x4

    int-to-byte p1, p1

    or-int/2addr p1, v9

    int-to-byte p1, p1

    aput-byte p1, v2, v8

    :goto_3
    div-long/2addr v4, v6

    add-int/lit8 p0, p0, 0x1

    goto :goto_2

    :cond_3
    :goto_4
    div-int/lit8 p0, v1, 0x2

    if-ge v0, p0, :cond_4

    aget-byte p0, v2, v0

    sub-int p1, v1, v0

    add-int/lit8 p1, p1, -0x1

    aget-byte v3, v2, p1

    aput-byte v3, v2, v0

    aput-byte p0, v2, p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    return-object v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/a;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/utils/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/utils/a;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/utils/a;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/utils/a;->a:[Lcom/mastercard/mchipengine/utils/a;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/utils/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/utils/a;

    return-object v0
.end method
