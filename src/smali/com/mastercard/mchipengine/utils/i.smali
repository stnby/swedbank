.class public final enum Lcom/mastercard/mchipengine/utils/i;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/mchipengine/utils/i;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic a:[Lcom/mastercard/mchipengine/utils/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/mastercard/mchipengine/utils/i;

    sput-object v0, Lcom/mastercard/mchipengine/utils/i;->a:[Lcom/mastercard/mchipengine/utils/i;

    return-void
.end method

.method public static a(Lcom/mastercard/mchipengine/utils/Wipeable;)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/mastercard/mchipengine/utils/Wipeable;->wipe()V

    :cond_0
    return-void
.end method

.method public static a([B)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    :cond_0
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aput-byte v1, p0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/i;
    .locals 1

    const-class v0, Lcom/mastercard/mchipengine/utils/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/mchipengine/utils/i;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/mchipengine/utils/i;
    .locals 1

    sget-object v0, Lcom/mastercard/mchipengine/utils/i;->a:[Lcom/mastercard/mchipengine/utils/i;

    invoke-virtual {v0}, [Lcom/mastercard/mchipengine/utils/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/mchipengine/utils/i;

    return-object v0
.end method
