.class public final Lcom/mastercard/mchipengine/utils/MChipByteArray;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mchipengine/utils/Wipeable;


# static fields
.field private static final HEX_DIGITS:[C

.field private static final HEX_PREFIX:Ljava/lang/String; = "0x"

.field private static final HEX_REGEX:Ljava/lang/String; = "^([A-Fa-f0-9]{2})+$"


# instance fields
.field private mData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->HEX_DIGITS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method private constructor <init>(B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    iput-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    return-void
.end method

.method private constructor <init>(C)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    const v1, 0xff00

    and-int/2addr v1, p1

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x1

    aput-byte p1, v0, v1

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "0x"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_2

    :cond_0
    const-string v1, "0x"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    rem-int/2addr v1, v2

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    const-string v1, "^([A-Fa-f0-9]{2})+$"

    invoke-static {v1, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v2, v1, 0x2

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    :goto_1
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    div-int/lit8 v3, v0, 0x2

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v5, 0x10

    invoke-static {v0, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    shl-int/lit8 v0, v0, 0x4

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    add-int/2addr v0, v4

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    move v0, v6

    goto :goto_1

    :cond_3
    return-void

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid hex string ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_2
    new-array p1, v0, [B

    iput-object p1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    return-void
.end method

.method private constructor <init>(S)V
    .locals 0

    int-to-char p1, p1

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>(C)V

    return-void
.end method

.method private constructor <init>([BI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, p2, [B

    iput-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public static get(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>(I)V

    return-object v0
.end method

.method public static getRandomByteArray(I)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 2

    new-array p0, p0, [B

    :try_start_0
    const-string v0, "SHA1PRNG"

    invoke-static {v0}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [B

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-virtual {v0, p0}, Ljava/security/SecureRandom;->nextBytes([B)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0, p0}, Ljava/util/Random;->nextBytes([B)V

    :goto_0
    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    return-object p0
.end method

.method public static of(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>(B)V

    return-object v0
.end method

.method public static of(C)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>(C)V

    return-object v0
.end method

.method public static of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p0

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>([BI)V

    return-object v0
.end method

.method public static of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static of(S)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>(S)V

    return-object v0
.end method

.method public static of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 2

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>([BI)V

    return-object v0
.end method

.method public static of([BI)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-direct {v0, p0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>([BI)V

    return-object v0
.end method


# virtual methods
.method public final appendByte(B)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->resize(I)V

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    aput-byte p1, v1, v0

    return-object p0
.end method

.method public final appendBytes(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 0

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->appendBytes([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    return-object p1
.end method

.method public final appendBytes([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v1, v1

    array-length v2, p1

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->resize(I)V

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object p0
.end method

.method public final appendChar(C)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->resize(I)V

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    const v2, 0xff00

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    add-int/lit8 v0, v0, 0x1

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    return-object p0
.end method

.method public final clear()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    :cond_0
    return-void
.end method

.method public final clone()Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 1

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->clone()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    return-object v0
.end method

.method public final copyOfRange(II)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3

    if-ltz p1, :cond_2

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    if-gt p2, v0, :cond_1

    if-gt p1, p2, :cond_0

    sub-int v0, p2, p1

    new-instance v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    invoke-static {v2, p1, p2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    invoke-direct {v1, p1, v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;-><init>([BI)V

    return-object v1

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/d/a/a;

    const-string p2, "The \'from\' is greater than \'to\'"

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/d/a/a;

    const-string p2, "The \'to\' index cannot be beyond"

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/mastercard/mchipengine/d/a/a;

    const-string p2, "The \'from\' index cannot be negative"

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {p0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z

    move-result p1

    return p1

    :cond_0
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final fill(B)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([BB)V

    return-void
.end method

.method public final getByte(I)B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    aget-byte p1, v0, p1

    return p1
.end method

.method public final getBytes()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    return-object v0
.end method

.method public final getLength()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isEqual(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    return p1
.end method

.method public final isZero()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/b;->d([B)Z

    move-result v0

    return v0
.end method

.method public final padLeading(IB)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    if-lt v0, p1, :cond_0

    return-object p0

    :cond_0
    new-array v0, p1, [B

    invoke-static {v0, p2}, Ljava/util/Arrays;->fill([BB)V

    iget-object p2, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v2, v2

    sub-int/2addr p1, v2

    iget-object v2, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v2, v2

    invoke-static {p2, v1, v0, p1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    return-object p0
.end method

.method public final resize(I)V
    .locals 3

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    if-le p1, v0, :cond_0

    new-array p1, p1, [B

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v1, v1

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    :cond_0
    return-void
.end method

.method public final setBit(IIZ)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getByte(I)B

    move-result v0

    const/4 v1, 0x1

    if-eqz p3, :cond_0

    shl-int p2, v1, p2

    or-int/2addr p2, v0

    :goto_0
    int-to-byte p2, p2

    goto :goto_1

    :cond_0
    shl-int p2, v1, p2

    not-int p2, p2

    and-int/2addr p2, v0

    goto :goto_0

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setByte(IB)V

    return-void
.end method

.method public final setByte(IB)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    aput-byte p2, v0, p1

    return-void
.end method

.method public final setBytes(I[B)V
    .locals 3

    array-length v0, p2

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v1, v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v2, p2

    invoke-static {p2, v0, v1, p1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/d/a/a;

    const-string p2, "Not enough space in destination array"

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final setBytes(I[BII)V
    .locals 2

    add-int v0, p1, p4

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v1, v1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    invoke-static {p2, p3, v0, p1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_0
    new-instance p1, Lcom/mastercard/mchipengine/d/a/a;

    const-string p2, "Not enough space in destination array"

    invoke-direct {p1, p2}, Lcom/mastercard/mchipengine/d/a/a;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final toBase64String()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toBinaryString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/b;->b(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toHexString()Ljava/lang/String;
    .locals 7

    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    array-length v3, v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    aget-byte v3, v3, v1

    add-int/lit8 v4, v2, 0x1

    sget-object v5, Lcom/mastercard/mchipengine/utils/MChipByteArray;->HEX_DIGITS:[C

    ushr-int/lit8 v6, v3, 0x4

    and-int/lit8 v6, v6, 0xf

    aget-char v5, v5, v6

    aput-char v5, v0, v2

    add-int/lit8 v2, v4, 0x1

    sget-object v5, Lcom/mastercard/mchipengine/utils/MChipByteArray;->HEX_DIGITS:[C

    and-int/lit8 v3, v3, 0xf

    aget-char v3, v5, v3

    aput-char v3, v0, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1

    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Input"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, "MchipByteArray"

    return-object v0
.end method

.method public final toUtf8String()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mastercard/mchipengine/utils/MChipByteArray;->mData:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method public final wipe()V
    .locals 0

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->clear()V

    return-void
.end method
