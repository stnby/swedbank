.class public final Lcom/mastercard/mchipengine/c/a/a;
.super Lcom/mastercard/mchipengine/d/b/b/d;

# interfaces
.implements Lcom/mastercard/mchipengine/utils/Wipeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mastercard/mchipengine/d/b/b/d<",
        "Lcom/mastercard/mchipengine/utils/MChipByteArray;",
        ">;",
        "Lcom/mastercard/mchipengine/utils/Wipeable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/mastercard/mchipengine/utils/MChipByteArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    sput-object v0, Lcom/mastercard/mchipengine/c/a/a;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    return-void

    nop

    :array_0
    .array-data 1
        -0x61t
        0x10t
    .end array-data
.end method

.method public constructor <init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V
    .locals 0

    invoke-direct {p0}, Lcom/mastercard/mchipengine/d/b/b/d;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/c/a/a;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/mchipengine/d/d;
    .locals 3

    new-instance v0, Lcom/mastercard/mchipengine/d/d;

    sget-object v1, Lcom/mastercard/mchipengine/c/a/a;->a:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/c/a/a;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-static {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Lcom/mastercard/mchipengine/utils/MChipByteArray;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/mastercard/mchipengine/d/d;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-object v0
.end method

.method public final wipe()V
    .locals 0

    return-void
.end method
