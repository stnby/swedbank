.class public final Lcom/mastercard/mchipengine/c/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/mastercard/mchipengine/d/a;

.field private b:Lcom/mastercard/mchipengine/d/b;


# direct methods
.method public constructor <init>(Lcom/mastercard/mchipengine/d/a;Lcom/mastercard/mchipengine/d/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    iput-object p2, p0, Lcom/mastercard/mchipengine/c/b;->b:Lcom/mastercard/mchipengine/d/b;

    return-void
.end method

.method public static a([B[B[B)Lcom/mastercard/mchipengine/c/a/a;
    .locals 3

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    array-length v0, p0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1

    array-length v0, p1

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v2, p0, v2, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[BII)V

    const/4 p0, 0x2

    invoke-virtual {v0, p0, p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[B)V

    const/16 p0, 0x8

    new-array p0, p0, [B

    fill-array-data p0, :array_0

    invoke-static {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of([B)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p0

    const/4 p1, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, p1, p2, v2, v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[BII)V

    const/16 p1, 0xa

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p2

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p0

    invoke-virtual {v0, p1, p2, v2, p0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->setBytes(I[BII)V
    :try_end_0
    .catch Lcom/mastercard/mchipengine/d/a/a; {:try_start_0 .. :try_end_0} :catch_0

    new-instance p0, Lcom/mastercard/mchipengine/c/a/a;

    invoke-direct {p0, v0}, Lcom/mastercard/mchipengine/c/a/a;-><init>(Lcom/mastercard/mchipengine/utils/MChipByteArray;)V

    return-object p0

    :catch_0
    move-exception p0

    new-instance p1, Lcom/mastercard/mchipengine/c/a;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "Issuer Application Data components have incorrect length: "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/d/a/a;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_0
    new-instance p0, Lcom/mastercard/mchipengine/c/a;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "Card Verification Result has incorrect length: "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length p1, p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p1, Lcom/mastercard/mchipengine/c/a;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "Issuer Application Data in Card Profile has incorrect length: "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length p0, p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p0, Lcom/mastercard/mchipengine/c/a;

    const-string p1, "Issuer Application Data input is invalid (null)"

    invoke-direct {p0, p1}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p0

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x1t
    .end array-data
.end method


# virtual methods
.method public final a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;)Lcom/mastercard/mchipengine/utils/MChipByteArray;
    .locals 5

    iget-object v0, p0, Lcom/mastercard/mchipengine/c/b;->b:Lcom/mastercard/mchipengine/d/b;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/d/b;->a()Lcom/mastercard/mchipengine/d/d;

    move-result-object v0

    iget-object v0, v0, Lcom/mastercard/mchipengine/d/d;->c:Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v1, p0, Lcom/mastercard/mchipengine/c/b;->b:Lcom/mastercard/mchipengine/d/b;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b;->d:Lcom/mastercard/mchipengine/d/b/b/a;

    iget-object v1, v1, Lcom/mastercard/mchipengine/d/b/b/a;->b:Ljava/lang/Object;

    check-cast v1, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    iget-object v2, p0, Lcom/mastercard/mchipengine/c/b;->b:Lcom/mastercard/mchipengine/d/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b;->a:Lcom/mastercard/mchipengine/d/b/b/b;

    iget-object v2, v2, Lcom/mastercard/mchipengine/d/b/b/b;->b:Ljava/lang/Object;

    check-cast v2, Lcom/mastercard/mchipengine/utils/MChipByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v3

    if-ne v3, v4, :cond_3

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;->DE55:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CryptogramDataType;

    if-ne p1, v4, :cond_0

    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/b;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/a;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/a;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/a;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/m;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/m;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/m;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/p;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/p;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/p;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/q;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/r;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/r;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/r;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/s;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/s;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/s;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const-class p1, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/a;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/b;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/b;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Lcom/mastercard/mchipengine/d/b/a/a;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/a;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/a;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/a;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Lcom/mastercard/mchipengine/d/b/a/m;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/a;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/m;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/m;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Lcom/mastercard/mchipengine/d/b/a/p;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/a;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/p;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/p;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/a;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/q;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/q;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Lcom/mastercard/mchipengine/d/b/a/r;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/a;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/r;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/r;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Lcom/mastercard/mchipengine/d/b/a/s;

    invoke-static {p1}, Lcom/mastercard/mchipengine/d/a;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :goto_1
    iget-object p1, p0, Lcom/mastercard/mchipengine/c/b;->a:Lcom/mastercard/mchipengine/d/a;

    const-class v4, Lcom/mastercard/mchipengine/d/b/a/t;

    invoke-virtual {p1, v4}, Lcom/mastercard/mchipengine/d/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mastercard/mchipengine/d/b/a/t;

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/d/b/a/t;->g()Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Lcom/mastercard/mchipengine/utils/b;->b(Ljava/lang/Iterable;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    const/16 v1, 0x27

    if-ne v0, v1, :cond_1

    return-object p1

    :cond_1
    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->wipe()V

    new-instance v0, Lcom/mastercard/mchipengine/c/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incomplete Cryptogram Input. Length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", Expected: 39"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance p1, Lcom/mastercard/mchipengine/c/a;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Card VerificationResults has incorrect length: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Lcom/mastercard/mchipengine/c/a;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Application Transaction Counter has incorrect length: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lcom/mastercard/mchipengine/c/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Application Interchange Profile has incorrect length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getLength()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/mastercard/mchipengine/c/a;-><init>(Ljava/lang/String;)V

    throw p1
.end method
