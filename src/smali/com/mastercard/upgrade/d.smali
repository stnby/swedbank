.class public final Lcom/mastercard/upgrade/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/b;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field private b:Lcom/mastercard/upgrade/a;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/upgrade/d;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V
    .locals 29

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    new-instance v0, Lcom/mastercard/upgrade/a;

    iget-object v3, v1, Lcom/mastercard/upgrade/d;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct {v0, v4, v3, v5}, Lcom/mastercard/upgrade/a;-><init>(Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    iput-object v0, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    const-string v0, "SELECT card_id , mobile_keyset_id , mobile_key_type , mobile_key_value FROM mobile_keys"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    const/4 v7, 0x4

    const/4 v8, 0x3

    const/4 v9, 0x2

    const/4 v11, 0x1

    if-nez v0, :cond_0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :cond_0
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v12, v0, [Lcom/mastercard/upgrade/d/d;

    const/4 v13, 0x0

    :cond_1
    :try_start_0
    const-string v0, "card_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v14, "mobile_keyset_id"

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v15, "mobile_key_type"

    invoke-interface {v4, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v4, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const-string v10, "mobile_key_value"

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    iget-object v5, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    invoke-virtual {v5}, Lcom/mastercard/upgrade/a;->a()[B

    move-result-object v5

    invoke-static {v10, v5}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v5

    new-instance v6, Lcom/mastercard/upgrade/d/d;

    invoke-direct {v6, v0, v14, v15, v5}, Lcom/mastercard/upgrade/d/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    aput-object v6, v12, v13
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    const-string v0, "mobile_keys"

    invoke-virtual {v2, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    array-length v4, v12

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_3

    aget-object v0, v12, v5

    :try_start_1
    const-string v6, "INSERT INTO mobile_keys ( mobile_key_value , mobile_keyset_id , card_id , mobile_key_type )  VALUES (?,?,?,?);"

    invoke-virtual {v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    iget-object v13, v0, Lcom/mastercard/upgrade/d/d;->a:[B

    invoke-virtual {v10, v13}, Lcom/mastercard/upgrade/a;->a([B)[B

    move-result-object v10

    invoke-virtual {v6, v11, v10}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    iget-object v10, v0, Lcom/mastercard/upgrade/d/d;->c:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    iget-object v10, v0, Lcom/mastercard/upgrade/d/d;->b:Ljava/lang/String;

    invoke-virtual {v6, v8, v10}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    iget-object v0, v0, Lcom/mastercard/upgrade/d/d;->d:Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v13

    const-wide/16 v16, -0x1

    cmp-long v0, v13, v16

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v6, "Unable to store the mobile key"

    invoke-direct {v0, v6}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    :goto_3
    const-string v0, "SELECT card_id , card_data , card_state , card_pin_state FROM card_profiles_list"

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v5, v0, [Lcom/mastercard/upgrade/d/c;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v6, 0x0

    :cond_4
    :try_start_2
    const-string v0, "card_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const-string v0, "card_data"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    invoke-virtual {v10}, Lcom/mastercard/upgrade/a;->a()[B

    move-result-object v10

    invoke-static {v0, v10}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v20

    const-string v0, "card_state"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    const-string v0, "card_pin_state"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    new-instance v0, Lcom/mastercard/upgrade/d/c;

    move-object/from16 v18, v0

    invoke-direct/range {v18 .. v24}, Lcom/mastercard/upgrade/d/c;-><init>(Ljava/lang/String;[BJJ)V

    aput-object v0, v5, v6
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_2 .. :try_end_2} :catch_2

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    const-string v0, "card_profiles_list"

    invoke-virtual {v2, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    array-length v4, v5

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v4, :cond_6

    aget-object v0, v5, v6

    :try_start_3
    const-string v10, "INSERT INTO card_profiles_list ( card_id , card_data , card_state , card_pin_state )  VALUES (?,?,?,?);"

    invoke-virtual {v2, v10}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v10

    iget-object v12, v0, Lcom/mastercard/upgrade/d/c;->b:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    iget-object v12, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    iget-object v13, v0, Lcom/mastercard/upgrade/d/c;->a:[B

    invoke-virtual {v12, v13}, Lcom/mastercard/upgrade/a;->a([B)[B

    move-result-object v12

    invoke-virtual {v10, v9, v12}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    iget-wide v12, v0, Lcom/mastercard/upgrade/d/c;->c:J

    invoke-virtual {v10, v8, v12, v13}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    iget-wide v12, v0, Lcom/mastercard/upgrade/d/c;->d:J

    invoke-virtual {v10, v7, v12, v13}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v12

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v14, -0x1

    cmp-long v0, v12, v14

    if-eqz v0, :cond_5

    goto :goto_6

    :cond_5
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v10, "Unable to update the database"

    invoke-direct {v0, v10}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_6
    const-string v0, "SELECT suk_info, suk_id, suk_cl_umd, suk_cl_md, suk_rp_umd, suk_rp_md, idn, atc, hash, card_id FROM suk_list"

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v5, v0, [Lcom/mastercard/upgrade/d/e;

    const/4 v6, 0x0

    :cond_7
    :try_start_4
    const-string v0, "suk_info"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v19

    const-string v0, "suk_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const-string v0, "suk_cl_umd"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    invoke-virtual {v10}, Lcom/mastercard/upgrade/a;->a()[B

    move-result-object v10

    invoke-static {v0, v10}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v0

    :cond_8
    move-object/from16 v21, v0

    const-string v0, "suk_cl_md"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    invoke-virtual {v10}, Lcom/mastercard/upgrade/a;->a()[B

    move-result-object v10

    invoke-static {v0, v10}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v0

    :cond_9
    move-object/from16 v22, v0

    const-string v0, "suk_rp_umd"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    invoke-virtual {v10}, Lcom/mastercard/upgrade/a;->a()[B

    move-result-object v10

    invoke-static {v0, v10}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v0

    :cond_a
    move-object/from16 v23, v0

    const-string v0, "suk_rp_md"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    invoke-virtual {v10}, Lcom/mastercard/upgrade/a;->a()[B

    move-result-object v10

    invoke-static {v0, v10}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v0

    :cond_b
    move-object/from16 v24, v0

    const-string v0, "idn"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v25

    const-string v0, "atc"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v26

    const-string v0, "hash"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    const-string v0, "card_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    new-instance v0, Lcom/mastercard/upgrade/d/e;

    move-object/from16 v18, v0

    invoke-direct/range {v18 .. v28}, Lcom/mastercard/upgrade/d/e;-><init>([BLjava/lang/String;[B[B[B[B[B[BLjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v5, v6
    :try_end_4
    .catch Ljava/security/GeneralSecurityException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_4 .. :try_end_4} :catch_4

    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_7
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    const-string v0, "suk_list"

    invoke-virtual {v2, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    array-length v3, v5

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_11

    aget-object v0, v5, v4

    :try_start_5
    const-string v6, "INSERT INTO suk_list ( suk_info , suk_id , suk_cl_umd , suk_cl_md , suk_rp_umd , suk_rp_md , idn , atc , hash , card_id )  VALUES (?,?,?,?,?,?,?,?,?,?);"

    invoke-virtual {v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    iget-object v10, v0, Lcom/mastercard/upgrade/d/e;->a:[B

    invoke-virtual {v6, v11, v10}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    iget-object v10, v0, Lcom/mastercard/upgrade/d/e;->b:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    iget-object v10, v0, Lcom/mastercard/upgrade/d/e;->c:[B

    if-eqz v10, :cond_c

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    iget-object v12, v0, Lcom/mastercard/upgrade/d/e;->c:[B

    invoke-virtual {v10, v12}, Lcom/mastercard/upgrade/a;->a([B)[B

    move-result-object v10

    invoke-virtual {v6, v8, v10}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    :cond_c
    iget-object v10, v0, Lcom/mastercard/upgrade/d/e;->d:[B

    if-eqz v10, :cond_d

    iget-object v10, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    iget-object v12, v0, Lcom/mastercard/upgrade/d/e;->d:[B

    invoke-virtual {v10, v12}, Lcom/mastercard/upgrade/a;->a([B)[B

    move-result-object v10

    invoke-virtual {v6, v7, v10}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    :cond_d
    iget-object v10, v0, Lcom/mastercard/upgrade/d/e;->e:[B

    if-eqz v10, :cond_e

    const/4 v10, 0x5

    iget-object v12, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    iget-object v13, v0, Lcom/mastercard/upgrade/d/e;->e:[B

    invoke-virtual {v12, v13}, Lcom/mastercard/upgrade/a;->a([B)[B

    move-result-object v12

    invoke-virtual {v6, v10, v12}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    :cond_e
    iget-object v10, v0, Lcom/mastercard/upgrade/d/e;->f:[B

    if-eqz v10, :cond_f

    const/4 v10, 0x6

    iget-object v12, v1, Lcom/mastercard/upgrade/d;->b:Lcom/mastercard/upgrade/a;

    iget-object v13, v0, Lcom/mastercard/upgrade/d/e;->f:[B

    invoke-virtual {v12, v13}, Lcom/mastercard/upgrade/a;->a([B)[B

    move-result-object v12

    invoke-virtual {v6, v10, v12}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    :cond_f
    const/4 v10, 0x7

    iget-object v12, v0, Lcom/mastercard/upgrade/d/e;->g:[B

    invoke-virtual {v6, v10, v12}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/16 v10, 0x8

    iget-object v12, v0, Lcom/mastercard/upgrade/d/e;->h:[B

    invoke-virtual {v6, v10, v12}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/16 v10, 0x9

    iget-object v12, v0, Lcom/mastercard/upgrade/d/e;->i:Ljava/lang/String;

    invoke-virtual {v6, v10, v12}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v10, 0xa

    iget-object v0, v0, Lcom/mastercard/upgrade/d/e;->j:Ljava/lang/String;

    invoke-virtual {v6, v10, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v12

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_5
    .catch Ljava/security/GeneralSecurityException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_5 .. :try_end_5} :catch_6

    const-wide/16 v14, -0x1

    cmp-long v0, v12, v14

    if-eqz v0, :cond_10

    goto :goto_a

    :cond_10
    :try_start_6
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v6, "Unable to update database"

    invoke-direct {v0, v6}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Ljava/security/GeneralSecurityException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception v0

    goto :goto_9

    :catch_6
    move-exception v0

    const-wide/16 v14, -0x1

    :goto_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_a
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    :cond_11
    return-void
.end method
