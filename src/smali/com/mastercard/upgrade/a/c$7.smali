.class final Lcom/mastercard/upgrade/a/c$7;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final a:[B

.field final b:[B

.field final c:[B

.field final d:[B

.field final e:[B

.field final synthetic f:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->f:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->f:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {p1}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getU()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->a:[B

    iget-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->f:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {p1}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getP()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->b:[B

    iget-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->f:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {p1}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getQ()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->c:[B

    iget-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->f:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {p1}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getDp()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->d:[B

    iget-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->f:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {p1}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getDq()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$7;->e:[B

    return-void
.end method

.method private f()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->a:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->b:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->c:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->d:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->e:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a()[B
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/upgrade/a/c$7;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->b:[B

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()[B
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/upgrade/a/c$7;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->c:[B

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()[B
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/upgrade/a/c$7;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->d:[B

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()[B
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/upgrade/a/c$7;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->e:[B

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()[B
    .locals 1

    invoke-direct {p0}, Lcom/mastercard/upgrade/a/c$7;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$7;->a:[B

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
