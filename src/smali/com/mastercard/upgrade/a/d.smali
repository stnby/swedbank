.class public final Lcom/mastercard/upgrade/a/d;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)[Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/mastercard/upgrade/d/g;",
            ">;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;",
            ")[",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    const/4 v4, 0x0

    :goto_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v5, v5, Lcom/mastercard/upgrade/d/g;->c:[B

    invoke-interface {v1, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;->encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v11

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v5, v5, Lcom/mastercard/upgrade/d/g;->b:[B

    invoke-interface {v1, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;->encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v9

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v5, v5, Lcom/mastercard/upgrade/d/g;->e:[B

    invoke-interface {v1, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;->encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v12

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v5, v5, Lcom/mastercard/upgrade/d/g;->d:[B

    invoke-interface {v1, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;->encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v10

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v5, v5, Lcom/mastercard/upgrade/d/g;->g:[B

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v14

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v7, v5, Lcom/mastercard/upgrade/d/g;->i:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v8, v5, Lcom/mastercard/upgrade/d/g;->a:[B

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v15, v5, Lcom/mastercard/upgrade/d/g;->h:[B

    :try_start_0
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mastercard/upgrade/d/g;

    iget-object v5, v5, Lcom/mastercard/upgrade/d/g;->f:[B

    const/16 v13, 0x8

    if-eqz v5, :cond_0

    array-length v3, v5

    if-ne v3, v6, :cond_0

    new-array v3, v13, [B

    const/4 v6, 0x0

    invoke-static {v5, v13, v3, v6, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    if-eqz v5, :cond_1

    array-length v3, v5

    if-ne v3, v13, :cond_1

    move-object v3, v5

    :goto_1
    invoke-interface {v1, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;->encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v13
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v3, Lcom/mastercard/upgrade/a/d$1;

    const/4 v5, 0x0

    move-object v6, v3

    invoke-direct/range {v6 .. v15}, Lcom/mastercard/upgrade/a/d$1;-><init>(Ljava/lang/String;[B[B[B[B[B[BI[B)V

    aput-object v3, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid IDN"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid IDN"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v2
.end method
