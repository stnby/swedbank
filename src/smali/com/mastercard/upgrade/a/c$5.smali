.class final Lcom/mastercard/upgrade/a/c$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/ContactlessPaymentData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getAid()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getPpseFci()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getPaymentFci()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final d()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getGpoResponse()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final e()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getCdol1RelatedDataLength()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/b;->a(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final f()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final g()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final h()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getIssuerApplicationData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/mastercard/upgrade/c/b/a/a/f;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getIccPrivateKeyCrtComponents()Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    move-result-object v0

    new-instance v1, Lcom/mastercard/upgrade/a/c$7;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/c$7;-><init>(Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;)V

    return-object v1
.end method

.method public final j()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getPinIvCvc3Track2()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final k()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getCiacDeclineOnPpms()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/mastercard/upgrade/c/b/a/a/a;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getAlternateContactlessPaymentData()Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/upgrade/a/c$8;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/c$8;-><init>(Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;)V

    return-object v1
.end method

.method public final m()[Lcom/mastercard/upgrade/c/b/a/a/j;
    .locals 7

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$5;->a:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->getRecords()[Lcom/mastercard/upgrade/profile/Record;

    move-result-object v0

    array-length v1, v0

    new-array v1, v1, [Lcom/mastercard/upgrade/c/b/a/a/j;

    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v5, v0, v3

    new-instance v6, Lcom/mastercard/upgrade/a/c$9;

    invoke-direct {v6, v5}, Lcom/mastercard/upgrade/a/c$9;-><init>(Lcom/mastercard/upgrade/profile/Record;)V

    aput-object v6, v1, v4

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method
