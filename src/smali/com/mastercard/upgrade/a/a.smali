.class public final Lcom/mastercard/upgrade/a/a;
.super Ljava/lang/Object;


# direct methods
.method static a(Lcom/mastercard/upgrade/c/b/a/a/f;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 2

    invoke-interface {p0}, Lcom/mastercard/upgrade/c/b/a/a/f;->e()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-interface {p0}, Lcom/mastercard/upgrade/c/b/a/a/f;->a()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->append(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-interface {p0}, Lcom/mastercard/upgrade/c/b/a/a/f;->b()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->append(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-interface {p0}, Lcom/mastercard/upgrade/c/b/a/a/f;->c()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->append(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-interface {p0}, Lcom/mastercard/upgrade/c/b/a/a/f;->d()[B

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->append(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    :try_start_0
    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p0

    invoke-interface {p1, p0}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;->encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p0
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    new-instance p0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    const-string p1, ""

    invoke-static {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object p0
.end method
