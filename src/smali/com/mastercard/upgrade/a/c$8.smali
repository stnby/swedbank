.class final Lcom/mastercard/upgrade/a/c$8;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$8;->a:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$8;->a:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getAid()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$8;->a:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getPaymentFci()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$8;->a:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getGpoResponse()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final d()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$8;->a:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final e()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$8;->a:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
