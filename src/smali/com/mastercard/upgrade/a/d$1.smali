.class final Lcom/mastercard/upgrade/a/d$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/upgrade/a/d;->a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)[Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:[B

.field final synthetic c:[B

.field final synthetic d:[B

.field final synthetic e:[B

.field final synthetic f:[B

.field final synthetic g:[B

.field final synthetic h:I

.field final synthetic i:[B


# direct methods
.method constructor <init>(Ljava/lang/String;[B[B[B[B[B[BI[B)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/d$1;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/mastercard/upgrade/a/d$1;->b:[B

    iput-object p3, p0, Lcom/mastercard/upgrade/a/d$1;->c:[B

    iput-object p4, p0, Lcom/mastercard/upgrade/a/d$1;->d:[B

    iput-object p5, p0, Lcom/mastercard/upgrade/a/d$1;->e:[B

    iput-object p6, p0, Lcom/mastercard/upgrade/a/d$1;->f:[B

    iput-object p7, p0, Lcom/mastercard/upgrade/a/d$1;->g:[B

    iput p8, p0, Lcom/mastercard/upgrade/a/d$1;->h:I

    iput-object p9, p0, Lcom/mastercard/upgrade/a/d$1;->i:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAtc()I
    .locals 1

    iget v0, p0, Lcom/mastercard/upgrade/a/d$1;->h:I

    return v0
.end method

.method public final getHash()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->i:[B

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getIdn()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->g:[B

    return-object v0
.end method

.method public final getInfo()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->b:[B

    return-object v0
.end method

.method public final getSessionKeyContactlessMd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->e:[B

    return-object v0
.end method

.method public final getSessionKeyContactlessUmd()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSessionKeyRemotePaymentMd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->f:[B

    return-object v0
.end method

.method public final getSessionKeyRemotePaymentUmd()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSukContactlessUmd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->c:[B

    return-object v0
.end method

.method public final getSukRemotePaymentUmd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/d$1;->d:[B

    return-object v0
.end method

.method public final getTimestamp()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final setStatus(I)V
    .locals 0

    return-void
.end method

.method public final wipe()V
    .locals 0

    return-void
.end method
