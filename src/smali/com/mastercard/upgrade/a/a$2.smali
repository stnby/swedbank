.class final Lcom/mastercard/upgrade/a/a$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/c/b/a/a/d;

.field final synthetic b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/d;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    iput-object p2, p0, Lcom/mastercard/upgrade/a/a$2;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->l()Lcom/mastercard/upgrade/c/b/a/a/a;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/upgrade/a/a$4;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/a$4;-><init>(Lcom/mastercard/upgrade/c/b/a/a/a;)V

    return-object v1
.end method

.method public final getCdol1RelatedDataLength()I
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->e()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->f()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDeclineOnPpms()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->k()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->g()[B

    move-result-object v0

    return-object v0
.end method

.method public final getGpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public final getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->i()Lcom/mastercard/upgrade/c/b/a/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/upgrade/a/a$2;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    invoke-static {v0, v1}, Lcom/mastercard/upgrade/a/a;->a(Lcom/mastercard/upgrade/c/b/a/a/f;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v0

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->h()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->c()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPinIvCvc3Track2()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->j()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPpseFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public final getRecords()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$2;->a:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/d;->m()[Lcom/mastercard/upgrade/c/b/a/a/j;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v0, v3

    new-instance v5, Lcom/mastercard/upgrade/a/a$5;

    invoke-direct {v5, v4}, Lcom/mastercard/upgrade/a/a$5;-><init>(Lcom/mastercard/upgrade/c/b/a/a/j;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final getTrack1ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTrack2ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final isTransitSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isUsAipMaskingSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
