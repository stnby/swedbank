.class public final Lcom/mastercard/upgrade/a/a$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/b/a/e;

.field final synthetic b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;


# direct methods
.method public constructor <init>(Lcom/mastercard/upgrade/b/a/e;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iput-object p2, p0, Lcom/mastercard/upgrade/a/a$1;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCardCountryCode()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iget-object v0, v0, Lcom/mastercard/upgrade/b/a/e;->b:Lcom/mastercard/upgrade/c/b/a/a/i;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/i;->a()Lcom/mastercard/upgrade/c/b/a/a/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/c;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public final getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iget-object v0, v0, Lcom/mastercard/upgrade/b/a/e;->b:Lcom/mastercard/upgrade/c/b/a/a/i;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/i;->b()Lcom/mastercard/upgrade/c/b/a/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/upgrade/a/a$1;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    new-instance v2, Lcom/mastercard/upgrade/a/a$2;

    invoke-direct {v2, v0, v1}, Lcom/mastercard/upgrade/a/a$2;-><init>(Lcom/mastercard/upgrade/c/b/a/a/d;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)V

    return-object v2
.end method

.method public final getDigitizedCardId()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iget-object v0, v0, Lcom/mastercard/upgrade/b/a/e;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/b;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iget-object v0, v0, Lcom/mastercard/upgrade/b/a/e;->b:Lcom/mastercard/upgrade/c/b/a/a/i;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/i;->c()Lcom/mastercard/upgrade/c/b/a/a/k;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/upgrade/a/a$3;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/a$3;-><init>(Lcom/mastercard/upgrade/c/b/a/a/k;)V

    return-object v1
.end method

.method public final getPan()[B
    .locals 3

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iget-object v0, v0, Lcom/mastercard/upgrade/b/a/e;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "F"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "F"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/mastercard/upgrade/utils/b;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    return-object v0
.end method

.method public final getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/a/a$1$1;

    invoke-direct {v0, p0}, Lcom/mastercard/upgrade/a/a$1$1;-><init>(Lcom/mastercard/upgrade/a/a$1;)V

    return-object v0
.end method

.method public final isTransactionIdRequired()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
