.class final Lcom/mastercard/upgrade/a/c$6;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/RemotePaymentData;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/RemotePaymentData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getPan()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getPanSequenceNumber()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getApplicationExpiryDate()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final d()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getAip()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final e()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final f()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final g()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getIssuerApplicationData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final h()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$6;->a:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/RemotePaymentData;->getTrack2EquivalentData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
