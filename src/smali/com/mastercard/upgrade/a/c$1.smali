.class public final Lcom/mastercard/upgrade/a/c$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/DigitizedCardProfile;


# direct methods
.method public constructor <init>(Lcom/mastercard/upgrade/profile/DigitizedCardProfile;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$1;->a:Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$1;->a:Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->getDigitizedCardId()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$1;->a:Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->getMaximumPinTry()I

    move-result v0

    return v0
.end method

.method public final c()Lcom/mastercard/upgrade/c/b/a/a/b;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$1;->a:Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->getBusinessLogicModule()Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    move-result-object v0

    new-instance v1, Lcom/mastercard/upgrade/a/c$2;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/c$2;-><init>(Lcom/mastercard/upgrade/profile/BusinessLogicModule;)V

    return-object v1
.end method

.method public final d()Lcom/mastercard/upgrade/c/b/a/a/i;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$1;->a:Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->getMppLiteModule()Lcom/mastercard/upgrade/profile/MppLiteModule;

    move-result-object v0

    new-instance v1, Lcom/mastercard/upgrade/a/c$3;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/c$3;-><init>(Lcom/mastercard/upgrade/profile/MppLiteModule;)V

    return-object v1
.end method
