.class final Lcom/mastercard/upgrade/a/c$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/CardRiskManagementData;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/CardRiskManagementData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$4;->a:Lcom/mastercard/upgrade/profile/CardRiskManagementData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$4;->a:Lcom/mastercard/upgrade/profile/CardRiskManagementData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CardRiskManagementData;->getAdditionalCheckTable()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$4;->a:Lcom/mastercard/upgrade/profile/CardRiskManagementData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CardRiskManagementData;->getCrmCountryCode()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
