.class final Lcom/mastercard/upgrade/a/a$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/WalletData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/upgrade/a/a$1;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/a/a$1;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/a/a$1;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/a$1$1;->a:Lcom/mastercard/upgrade/a/a$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0
.end method

.method public final getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->MOBILE_PIN:Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    return-object v0
.end method

.method public final getCvmResetTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$1$1;->a:Lcom/mastercard/upgrade/a/a$1;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iget-object v0, v0, Lcom/mastercard/upgrade/b/a/e;->c:Lcom/mastercard/upgrade/c/b/a/a/b;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/b;->a()I

    move-result v0

    return v0
.end method

.method public final getDualTapResetTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$1$1;->a:Lcom/mastercard/upgrade/a/a$1;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/a$1;->a:Lcom/mastercard/upgrade/b/a/e;

    iget-object v0, v0, Lcom/mastercard/upgrade/b/a/e;->c:Lcom/mastercard/upgrade/c/b/a/a/b;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/b;->b()I

    move-result v0

    return v0
.end method

.method public final getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0
.end method
