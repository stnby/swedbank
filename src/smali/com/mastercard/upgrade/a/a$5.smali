.class final Lcom/mastercard/upgrade/a/a$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/Records;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/c/b/a/a/j;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/j;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/a$5;->a:Lcom/mastercard/upgrade/c/b/a/a/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getRecordNumber()B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$5;->a:Lcom/mastercard/upgrade/c/b/a/a/j;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/j;->a()B

    move-result v0

    return v0
.end method

.method public final getRecordValue()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$5;->a:Lcom/mastercard/upgrade/c/b/a/a/j;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/j;->c()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final getSfi()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$5;->a:Lcom/mastercard/upgrade/c/b/a/a/j;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/j;->b()B

    move-result v0

    shl-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
