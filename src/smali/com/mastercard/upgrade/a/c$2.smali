.class final Lcom/mastercard/upgrade/a/c$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/BusinessLogicModule;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getCvmResetTimeout()I

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getDualTapResetTimeout()I

    move-result v0

    return v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getApplicationLifeCycleData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getCardLayoutDescription()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final e()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getSecurityWord()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final f()[Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getCardholderValidators()Lcom/mastercard/upgrade/profile/CardholderValidators;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CardholderValidators;->getCardholderValidators()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    return-object v1
.end method

.method public final g()Lcom/mastercard/upgrade/c/b/a/a/g;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/a/c$2$1;

    invoke-direct {v0, p0}, Lcom/mastercard/upgrade/a/c$2$1;-><init>(Lcom/mastercard/upgrade/a/c$2;)V

    return-object v0
.end method

.method public final h()Lcom/mastercard/upgrade/c/b/a/a/h;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/a/c$2$2;

    invoke-direct {v0, p0}, Lcom/mastercard/upgrade/a/c$2$2;-><init>(Lcom/mastercard/upgrade/a/c$2;)V

    return-object v0
.end method
