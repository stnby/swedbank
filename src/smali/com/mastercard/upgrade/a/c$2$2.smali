.class final Lcom/mastercard/upgrade/a/c$2$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mastercard/upgrade/a/c$2;->h()Lcom/mastercard/upgrade/c/b/a/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/a/c$2;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/a/c$2;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getAckAlwaysRequiredIfCurrencyProvided()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getAckAlwaysRequiredIfCurrencyNotProvided()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getAckAutomaticallyResetByApplication()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getAckPreEntryAllowed()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getPinAlwaysRequiredIfCurrencyNotProvided()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getPinAlwaysRequiredIfCurrencyProvided()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getPinAutomaticallyResetByApplication()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$2$2;->a:Lcom/mastercard/upgrade/a/c$2;

    iget-object v0, v0, Lcom/mastercard/upgrade/a/c$2;->a:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getPinPreEntryAllowed()Z

    move-result v0

    return v0
.end method
