.class final Lcom/mastercard/upgrade/a/c$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/MppLiteModule;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/MppLiteModule;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$3;->a:Lcom/mastercard/upgrade/profile/MppLiteModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/upgrade/c/b/a/a/c;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$3;->a:Lcom/mastercard/upgrade/profile/MppLiteModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/MppLiteModule;->getCardRiskManagementData()Lcom/mastercard/upgrade/profile/CardRiskManagementData;

    move-result-object v0

    new-instance v1, Lcom/mastercard/upgrade/a/c$4;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/c$4;-><init>(Lcom/mastercard/upgrade/profile/CardRiskManagementData;)V

    return-object v1
.end method

.method public final b()Lcom/mastercard/upgrade/c/b/a/a/d;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$3;->a:Lcom/mastercard/upgrade/profile/MppLiteModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/MppLiteModule;->getContactlessPaymentData()Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    move-result-object v0

    new-instance v1, Lcom/mastercard/upgrade/a/c$5;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/c$5;-><init>(Lcom/mastercard/upgrade/profile/ContactlessPaymentData;)V

    return-object v1
.end method

.method public final c()Lcom/mastercard/upgrade/c/b/a/a/k;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$3;->a:Lcom/mastercard/upgrade/profile/MppLiteModule;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/MppLiteModule;->getRemotePaymentData()Lcom/mastercard/upgrade/profile/RemotePaymentData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/mastercard/upgrade/a/c$6;

    invoke-direct {v1, v0}, Lcom/mastercard/upgrade/a/c$6;-><init>(Lcom/mastercard/upgrade/profile/RemotePaymentData;)V

    return-object v1
.end method
