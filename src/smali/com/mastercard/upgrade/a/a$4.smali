.class final Lcom/mastercard/upgrade/a/a$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/c/b/a/a/a;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/a$4;->a:Lcom/mastercard/upgrade/c/b/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$4;->a:Lcom/mastercard/upgrade/c/b/a/a/a;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/a;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$4;->a:Lcom/mastercard/upgrade/c/b/a/a/a;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/a;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$4;->a:Lcom/mastercard/upgrade/c/b/a/a/a;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/a;->e()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$4;->a:Lcom/mastercard/upgrade/c/b/a/a/a;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/a;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public final getgpoResponse()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$4;->a:Lcom/mastercard/upgrade/c/b/a/a/a;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/a;->c()[B

    move-result-object v0

    return-object v0
.end method
