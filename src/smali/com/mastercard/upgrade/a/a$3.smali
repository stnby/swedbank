.class final Lcom/mastercard/upgrade/a/a$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DsrpData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/c/b/a/a/k;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/k;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAip()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/k;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/k;->e()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/k;->f()[B

    move-result-object v0

    return-object v0
.end method

.method public final getExpiryDate()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/k;->c()[B

    move-result-object v0

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/k;->g()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPanSequenceNumber()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/k;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPar()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTrack2EquivalentData()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/a$3;->a:Lcom/mastercard/upgrade/c/b/a/a/k;

    invoke-interface {v0}, Lcom/mastercard/upgrade/c/b/a/a/k;->h()[B

    move-result-object v0

    return-object v0
.end method

.method public final getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;
    .locals 1

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0
.end method

.method public final getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
