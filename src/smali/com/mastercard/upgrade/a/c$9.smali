.class final Lcom/mastercard/upgrade/a/c$9;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/upgrade/profile/Record;


# direct methods
.method constructor <init>(Lcom/mastercard/upgrade/profile/Record;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/a/c$9;->a:Lcom/mastercard/upgrade/profile/Record;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$9;->a:Lcom/mastercard/upgrade/profile/Record;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/Record;->getRecordNumber()B

    move-result v0

    return v0
.end method

.method public final b()B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$9;->a:Lcom/mastercard/upgrade/profile/Record;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/Record;->getSfi()B

    move-result v0

    return v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/a/c$9;->a:Lcom/mastercard/upgrade/profile/Record;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/Record;->getRecordValue()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
