.class public final Lcom/mastercard/upgrade/c;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field private final b:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DATABASE UPGRADE"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/c;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/upgrade/c;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;
    .locals 3

    iget-object v0, p0, Lcom/mastercard/upgrade/c;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrade Case : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/mastercard/upgrade/c$1;->a:[I

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/a/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to upgrade database from unknown version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance p1, Lcom/mastercard/upgrade/h;

    invoke-direct {p1}, Lcom/mastercard/upgrade/h;-><init>()V

    return-object p1

    :pswitch_1
    new-instance p1, Lcom/mastercard/upgrade/f;

    iget-object v0, p0, Lcom/mastercard/upgrade/c;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {p1, v0}, Lcom/mastercard/upgrade/f;-><init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V

    return-object p1

    :pswitch_2
    new-instance p1, Lcom/mastercard/upgrade/i;

    iget-object v0, p0, Lcom/mastercard/upgrade/c;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {p1, v0}, Lcom/mastercard/upgrade/i;-><init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lcom/mastercard/upgrade/e;

    sget v0, Lcom/mastercard/upgrade/utils/a/a;->b:I

    iget-object v1, p0, Lcom/mastercard/upgrade/c;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {p1, v0, v1}, Lcom/mastercard/upgrade/e;-><init>(ILcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V

    return-object p1

    :pswitch_4
    new-instance p1, Lcom/mastercard/upgrade/e;

    sget v0, Lcom/mastercard/upgrade/utils/a/a;->a:I

    iget-object v1, p0, Lcom/mastercard/upgrade/c;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {p1, v0, v1}, Lcom/mastercard/upgrade/e;-><init>(ILcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V

    return-object p1

    :pswitch_5
    new-instance p1, Lcom/mastercard/upgrade/g;

    invoke-direct {p1}, Lcom/mastercard/upgrade/g;-><init>()V

    return-object p1

    :pswitch_6
    new-instance p1, Lcom/mastercard/upgrade/d;

    iget-object v0, p0, Lcom/mastercard/upgrade/c;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {p1, v0}, Lcom/mastercard/upgrade/d;-><init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
