.class public final Lcom/mastercard/upgrade/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/b;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field private b:Lcom/mastercard/upgrade/a;

.field private c:I


# direct methods
.method public constructor <init>(ILcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mastercard/upgrade/e;->c:I

    iput-object p2, p0, Lcom/mastercard/upgrade/e;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V
    .locals 12

    new-instance v0, Lcom/mastercard/upgrade/a;

    iget-object v1, p0, Lcom/mastercard/upgrade/e;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {v0, p2, v1, p3}, Lcom/mastercard/upgrade/a;-><init>(Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/e;->b:Lcom/mastercard/upgrade/a;

    const-string p2, "SELECT card_id , card_data , card_state , card_pin_state FROM card_profiles_list"

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Lcom/mastercard/upgrade/d/c;

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    const/4 v2, 0x0

    :cond_0
    const/4 v3, 0x1

    :try_start_0
    const-string v4, "card_id"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v4, "card_data"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    iget-object v5, p0, Lcom/mastercard/upgrade/e;->b:Lcom/mastercard/upgrade/a;

    invoke-virtual {v5}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v4

    const-string v5, "card_state"

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-string v5, "card_pin_state"

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    sget-object v5, Lcom/mastercard/upgrade/e$1;->a:[I

    iget v7, p0, Lcom/mastercard/upgrade/e;->c:I

    sub-int/2addr v7, v3

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_0

    move-object v7, p3

    goto :goto_1

    :pswitch_0
    invoke-static {v4}, Lcom/mastercard/upgrade/c/a/a/a/e;->a([B)Lcom/mastercard/upgrade/c/a/a/a/e;

    move-result-object v4

    new-instance v5, Lcom/mastercard/upgrade/a/b$1;

    invoke-direct {v5, v4}, Lcom/mastercard/upgrade/a/b$1;-><init>(Lcom/mastercard/upgrade/c/a/a/a/e;)V

    new-instance v4, Lcom/mastercard/upgrade/b/a/e;

    invoke-direct {v4, v5}, Lcom/mastercard/upgrade/b/a/e;-><init>(Lcom/mastercard/upgrade/c/b/a/a/e;)V

    goto :goto_0

    :pswitch_1
    invoke-static {v4}, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->valueOf([B)Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    move-result-object v4

    new-instance v5, Lcom/mastercard/upgrade/a/c$1;

    invoke-direct {v5, v4}, Lcom/mastercard/upgrade/a/c$1;-><init>(Lcom/mastercard/upgrade/profile/DigitizedCardProfile;)V

    new-instance v4, Lcom/mastercard/upgrade/b/a/e;

    invoke-direct {v4, v5}, Lcom/mastercard/upgrade/b/a/e;-><init>(Lcom/mastercard/upgrade/c/b/a/a/e;)V

    :goto_0
    invoke-virtual {v4}, Lcom/mastercard/upgrade/b/a/e;->e()[B

    move-result-object v4

    move-object v7, v4

    :goto_1
    new-instance v4, Lcom/mastercard/upgrade/d/c;

    move-object v5, v4

    invoke-direct/range {v5 .. v11}, Lcom/mastercard/upgrade/d/c;-><init>(Ljava/lang/String;[BJJ)V

    aput-object v4, v0, v2
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    const-string p2, "card_profiles_list"

    invoke-virtual {p1, p2, p3, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    array-length p2, v0

    :goto_3
    if-ge v1, p2, :cond_2

    aget-object p3, v0, v1

    :try_start_1
    const-string v2, "INSERT INTO card_profiles_list ( card_id , card_data , card_state , card_pin_state )  VALUES (?,?,?,?);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iget-object v4, p3, Lcom/mastercard/upgrade/d/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/mastercard/upgrade/e;->b:Lcom/mastercard/upgrade/a;

    iget-object v6, p3, Lcom/mastercard/upgrade/d/c;->a:[B

    invoke-virtual {v5, v6}, Lcom/mastercard/upgrade/a;->a([B)[B

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v4, 0x3

    iget-wide v5, p3, Lcom/mastercard/upgrade/d/c;->c:J

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 v4, 0x4

    iget-wide v5, p3, Lcom/mastercard/upgrade/d/c;->d:J

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v4

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v6, -0x1

    cmp-long p3, v4, v6

    if-eqz p3, :cond_1

    goto :goto_4

    :cond_1
    new-instance p3, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v2, "Unable to update the database"

    invoke-direct {p3, v2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p3
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p3

    invoke-virtual {p3}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
