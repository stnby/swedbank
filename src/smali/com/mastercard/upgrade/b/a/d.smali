.class public Lcom/mastercard/upgrade/b/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/d;
.implements Ljava/io/Serializable;


# instance fields
.field private a:[B

.field private b:[B

.field private c:[B

.field private d:[B

.field private e:[B

.field private f:[B

.field private g:[B

.field private h:[B

.field private i:Lcom/mastercard/upgrade/c/b/a/a/f;

.field private j:[B

.field private k:[B

.field private l:Lcom/mastercard/upgrade/c/b/a/a/a;

.field private m:[Lcom/mastercard/upgrade/c/b/a/a/j;


# direct methods
.method public constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->a()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->a:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->b()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->b:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->c()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->c:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->d()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->d:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->e()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->e:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->f:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->g()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->g:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->h()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->h:[B

    new-instance v0, Lcom/mastercard/upgrade/b/a/f;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->i()Lcom/mastercard/upgrade/c/b/a/a/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/upgrade/b/a/f;-><init>(Lcom/mastercard/upgrade/c/b/a/a/f;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->i:Lcom/mastercard/upgrade/c/b/a/a/f;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->j()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->j:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->k()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->k:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->l()Lcom/mastercard/upgrade/c/b/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mastercard/upgrade/b/a/a;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->l()Lcom/mastercard/upgrade/c/b/a/a/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/upgrade/b/a/a;-><init>(Lcom/mastercard/upgrade/c/b/a/a/a;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->l:Lcom/mastercard/upgrade/c/b/a/a/a;

    :cond_0
    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/d;->m()[Lcom/mastercard/upgrade/c/b/a/a/j;

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/upgrade/b/a/d;->a([Lcom/mastercard/upgrade/c/b/a/a/j;)[Lcom/mastercard/upgrade/b/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/upgrade/b/a/d;->m:[Lcom/mastercard/upgrade/c/b/a/a/j;

    return-void
.end method

.method private static a([Lcom/mastercard/upgrade/c/b/a/a/j;)[Lcom/mastercard/upgrade/b/a/j;
    .locals 4

    array-length v0, p0

    new-array v0, v0, [Lcom/mastercard/upgrade/b/a/j;

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    new-instance v2, Lcom/mastercard/upgrade/b/a/j;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Lcom/mastercard/upgrade/b/a/j;-><init>(Lcom/mastercard/upgrade/c/b/a/a/j;)V

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->a:[B

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->b:[B

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->c:[B

    return-object v0
.end method

.method public final d()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->d:[B

    return-object v0
.end method

.method public final e()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->e:[B

    return-object v0
.end method

.method public final f()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->f:[B

    return-object v0
.end method

.method public final g()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->g:[B

    return-object v0
.end method

.method public final h()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->h:[B

    return-object v0
.end method

.method public final i()Lcom/mastercard/upgrade/c/b/a/a/f;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->i:Lcom/mastercard/upgrade/c/b/a/a/f;

    return-object v0
.end method

.method public final j()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->j:[B

    return-object v0
.end method

.method public final k()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->k:[B

    return-object v0
.end method

.method public final l()Lcom/mastercard/upgrade/c/b/a/a/a;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->l:Lcom/mastercard/upgrade/c/b/a/a/a;

    return-object v0
.end method

.method public final m()[Lcom/mastercard/upgrade/c/b/a/a/j;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/d;->m:[Lcom/mastercard/upgrade/c/b/a/a/j;

    return-object v0
.end method
