.class public Lcom/mastercard/upgrade/b/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/b;
.implements Ljava/io/Serializable;


# instance fields
.field private a:I

.field private b:I

.field private c:[B

.field private d:[B

.field private e:[B

.field private f:[Ljava/lang/String;

.field private g:Lcom/mastercard/upgrade/c/b/a/a/g;

.field private h:Lcom/mastercard/upgrade/c/b/a/a/h;


# direct methods
.method public constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->a()I

    move-result v0

    iput v0, p0, Lcom/mastercard/upgrade/b/a/b;->a:I

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->b()I

    move-result v0

    iput v0, p0, Lcom/mastercard/upgrade/b/a/b;->b:I

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->c()[B

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->c:[B

    :cond_0
    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->d()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->d:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->e()[B

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->e:[B

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->f()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->f:[Ljava/lang/String;

    new-instance v0, Lcom/mastercard/upgrade/b/a/g;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->g()Lcom/mastercard/upgrade/c/b/a/a/g;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/upgrade/b/a/g;-><init>(Lcom/mastercard/upgrade/c/b/a/a/g;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->g:Lcom/mastercard/upgrade/c/b/a/a/g;

    new-instance v0, Lcom/mastercard/upgrade/b/a/h;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/b;->h()Lcom/mastercard/upgrade/c/b/a/a/h;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/mastercard/upgrade/b/a/h;-><init>(Lcom/mastercard/upgrade/c/b/a/a/h;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->h:Lcom/mastercard/upgrade/c/b/a/a/h;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/mastercard/upgrade/b/a/b;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/mastercard/upgrade/b/a/b;->b:I

    return v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->c:[B

    return-object v0
.end method

.method public final d()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->d:[B

    return-object v0
.end method

.method public final e()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->e:[B

    return-object v0
.end method

.method public final f()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/mastercard/upgrade/c/b/a/a/g;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->g:Lcom/mastercard/upgrade/c/b/a/a/g;

    return-object v0
.end method

.method public final h()Lcom/mastercard/upgrade/c/b/a/a/h;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/b;->h:Lcom/mastercard/upgrade/c/b/a/a/h;

    return-object v0
.end method
