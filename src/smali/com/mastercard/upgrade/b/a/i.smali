.class public Lcom/mastercard/upgrade/b/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/i;
.implements Ljava/io/Serializable;


# instance fields
.field private a:Lcom/mastercard/upgrade/c/b/a/a/c;

.field private b:Lcom/mastercard/upgrade/c/b/a/a/d;

.field private c:Lcom/mastercard/upgrade/c/b/a/a/k;


# direct methods
.method public constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/i;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mastercard/upgrade/b/a/c;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/i;->a()Lcom/mastercard/upgrade/c/b/a/a/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/upgrade/b/a/c;-><init>(Lcom/mastercard/upgrade/c/b/a/a/c;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/i;->a:Lcom/mastercard/upgrade/c/b/a/a/c;

    new-instance v0, Lcom/mastercard/upgrade/b/a/d;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/i;->b()Lcom/mastercard/upgrade/c/b/a/a/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/upgrade/b/a/d;-><init>(Lcom/mastercard/upgrade/c/b/a/a/d;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/i;->b:Lcom/mastercard/upgrade/c/b/a/a/d;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/i;->c()Lcom/mastercard/upgrade/c/b/a/a/k;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mastercard/upgrade/b/a/k;

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/i;->c()Lcom/mastercard/upgrade/c/b/a/a/k;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/mastercard/upgrade/b/a/k;-><init>(Lcom/mastercard/upgrade/c/b/a/a/k;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/b/a/i;->c:Lcom/mastercard/upgrade/c/b/a/a/k;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/mastercard/upgrade/c/b/a/a/c;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/i;->a:Lcom/mastercard/upgrade/c/b/a/a/c;

    return-object v0
.end method

.method public final b()Lcom/mastercard/upgrade/c/b/a/a/d;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/i;->b:Lcom/mastercard/upgrade/c/b/a/a/d;

    return-object v0
.end method

.method public final c()Lcom/mastercard/upgrade/c/b/a/a/k;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/b/a/i;->c:Lcom/mastercard/upgrade/c/b/a/a/k;

    return-object v0
.end method
