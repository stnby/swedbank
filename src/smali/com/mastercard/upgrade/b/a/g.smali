.class public Lcom/mastercard/upgrade/b/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/c/b/a/a/g;
.implements Ljava/io/Serializable;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/mastercard/upgrade/c/b/a/a/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->a:Z

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->b:Z

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->c:Z

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->d:Z

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->e:Z

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->f:Z

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->g:Z

    invoke-interface {p1}, Lcom/mastercard/upgrade/c/b/a/a/g;->b()Z

    move-result p1

    iput-boolean p1, p0, Lcom/mastercard/upgrade/b/a/g;->h:Z

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->b:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->h:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->e:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->a:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->d:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->c:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->g:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/b/a/g;->f:Z

    return v0
.end method
