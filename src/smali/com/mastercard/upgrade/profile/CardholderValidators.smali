.class public final Lcom/mastercard/upgrade/profile/CardholderValidators;
.super Ljava/lang/Object;


# instance fields
.field private cardholderValidators:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cardholderValidators"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCardholderValidators()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/CardholderValidators;->cardholderValidators:Ljava/lang/String;

    return-object v0
.end method

.method public final setCardholderValidators(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/CardholderValidators;->cardholderValidators:Ljava/lang/String;

    return-void
.end method
