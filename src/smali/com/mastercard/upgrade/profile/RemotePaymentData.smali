.class public final Lcom/mastercard/upgrade/profile/RemotePaymentData;
.super Ljava/lang/Object;


# instance fields
.field private mAip:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "aip"
    .end annotation
.end field

.field private mApplicationExpiryDate:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "applicationExpiryDate"
    .end annotation
.end field

.field private mCiacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "ciacDecline"
    .end annotation
.end field

.field private mCvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "cvrMaskAnd"
    .end annotation
.end field

.field private mIssuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "issuerApplicationData"
    .end annotation
.end field

.field private mPan:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "pan"
    .end annotation
.end field

.field private mPanSequenceNumber:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "panSequenceNumber"
    .end annotation
.end field

.field private mTrack2EquivalentData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "track2Equivalent"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAip()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mAip:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getApplicationExpiryDate()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mApplicationExpiryDate:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mCiacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mCvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getIssuerApplicationData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mIssuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getPan()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mPan:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getPanSequenceNumber()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mPanSequenceNumber:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getTrack2EquivalentData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mTrack2EquivalentData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final setAip(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mAip:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setApplicationExpiryDate(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mApplicationExpiryDate:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCiacDecline(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mCiacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCvrMaskAnd(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mCvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setIssuerApplicationData(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mIssuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setPan(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mPan:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setPanSequenceNumber(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mPanSequenceNumber:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setTrack2EquivalentData(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/RemotePaymentData;->mTrack2EquivalentData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method
