.class public final Lcom/mastercard/upgrade/profile/MppLiteModule;
.super Ljava/lang/Object;


# instance fields
.field private cardRiskManagementData:Lcom/mastercard/upgrade/profile/CardRiskManagementData;
    .annotation runtime Lflexjson/h;
        a = "cardRiskManagementData"
    .end annotation
.end field

.field private contactlessPaymentData:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;
    .annotation runtime Lflexjson/h;
        a = "contactlessPaymentData"
    .end annotation
.end field

.field private remotePaymentData:Lcom/mastercard/upgrade/profile/RemotePaymentData;
    .annotation runtime Lflexjson/h;
        a = "remotePaymentData"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCardRiskManagementData()Lcom/mastercard/upgrade/profile/CardRiskManagementData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/MppLiteModule;->cardRiskManagementData:Lcom/mastercard/upgrade/profile/CardRiskManagementData;

    return-object v0
.end method

.method public final getContactlessPaymentData()Lcom/mastercard/upgrade/profile/ContactlessPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/MppLiteModule;->contactlessPaymentData:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    return-object v0
.end method

.method public final getRemotePaymentData()Lcom/mastercard/upgrade/profile/RemotePaymentData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/MppLiteModule;->remotePaymentData:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    return-object v0
.end method

.method public final setCardRiskManagementData(Lcom/mastercard/upgrade/profile/CardRiskManagementData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/MppLiteModule;->cardRiskManagementData:Lcom/mastercard/upgrade/profile/CardRiskManagementData;

    return-void
.end method

.method public final setContactlessPaymentData(Lcom/mastercard/upgrade/profile/ContactlessPaymentData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/MppLiteModule;->contactlessPaymentData:Lcom/mastercard/upgrade/profile/ContactlessPaymentData;

    return-void
.end method

.method public final setRemotePaymentData(Lcom/mastercard/upgrade/profile/RemotePaymentData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/MppLiteModule;->remotePaymentData:Lcom/mastercard/upgrade/profile/RemotePaymentData;

    return-void
.end method
