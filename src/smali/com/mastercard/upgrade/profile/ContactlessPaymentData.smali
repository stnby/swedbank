.class public final Lcom/mastercard/upgrade/profile/ContactlessPaymentData;
.super Ljava/lang/Object;


# instance fields
.field private aid:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "aid"
    .end annotation
.end field

.field private alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;
    .annotation runtime Lflexjson/h;
        a = "alternateContactlessPaymentData"
    .end annotation
.end field

.field private cdol1RelatedDataLength:I
    .annotation runtime Lflexjson/h;
        a = "cdol1RelatedDataLength"
    .end annotation
.end field

.field private ciacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "ciacDecline"
    .end annotation
.end field

.field private ciacDeclineOnPpms:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "ciacDeclineOnPpms"
    .end annotation
.end field

.field private cvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "cvrMaskAnd"
    .end annotation
.end field

.field private gpoResponse:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "gpoResponse"
    .end annotation
.end field

.field private iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;
    .annotation runtime Lflexjson/h;
        a = "iccPrivateKeyCrtComponents"
    .end annotation
.end field

.field private issuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "issuerApplicationData"
    .end annotation
.end field

.field private paymentFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "paymentFci"
    .end annotation
.end field

.field private pinIvCvc3Track2:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "pinIvCvc3Track2"
    .end annotation
.end field

.field private ppseFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "ppseFci"
    .end annotation
.end field

.field private records:[Lcom/mastercard/upgrade/profile/Record;
    .annotation runtime Lflexjson/h;
        a = "records"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->aid:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getAlternateContactlessPaymentData()Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    return-object v0
.end method

.method public final getCdol1RelatedDataLength()I
    .locals 1

    iget v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->cdol1RelatedDataLength:I

    return v0
.end method

.method public final getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCiacDeclineOnPpms()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDeclineOnPpms:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->cvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getGpoResponse()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->gpoResponse:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getIccPrivateKeyCrtComponents()Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    return-object v0
.end method

.method public final getIssuerApplicationData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->issuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getPaymentFci()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->paymentFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getPinIvCvc3Track2()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->pinIvCvc3Track2:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getPpseFci()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ppseFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getRecords()[Lcom/mastercard/upgrade/profile/Record;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->records:[Lcom/mastercard/upgrade/profile/Record;

    return-object v0
.end method

.method public final isAlternateAidMchipDataValid()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->isPrimaryAidMchipDataValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isMagstripeDataValid()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->pinIvCvc3Track2:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->pinIvCvc3Track2:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDeclineOnPpms:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDeclineOnPpms:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isPrimaryAidMchipDataValid()Z
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->cvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->cvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->cdol1RelatedDataLength:I

    const/16 v1, 0x2d

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->issuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->issuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getP()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getQ()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getDp()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getDq()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->getU()Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final setAid(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->aid:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setAlternateContactlessPaymentData(Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->alternateContactlessPaymentData:Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;

    return-void
.end method

.method public final setCdol1RelatedDataLength(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->cdol1RelatedDataLength:I

    return-void
.end method

.method public final setCiacDecline(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCiacDeclineOnPpms(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ciacDeclineOnPpms:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCvrMaskAnd(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->cvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setGpoResponse(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->gpoResponse:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setIccPrivateKeyCrtComponents(Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->iccPrivateKeyCrtComponents:Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;

    return-void
.end method

.method public final setIssuerApplicationData(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->issuerApplicationData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setPaymentFci(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->paymentFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setPinIvCvc3Track2(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->pinIvCvc3Track2:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setPpseFci(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->ppseFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setRecords([Lcom/mastercard/upgrade/profile/Record;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/ContactlessPaymentData;->records:[Lcom/mastercard/upgrade/profile/Record;

    return-void
.end method
