.class public final Lcom/mastercard/upgrade/profile/CardRiskManagementData;
.super Ljava/lang/Object;


# instance fields
.field private mAdditionalCheckTable:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "additionalCheckTable"
    .end annotation
.end field

.field private mCrmCountryCode:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "crmCountryCode"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAdditionalCheckTable()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/CardRiskManagementData;->mAdditionalCheckTable:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCrmCountryCode()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/CardRiskManagementData;->mCrmCountryCode:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final setAdditionalCheckTable(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/CardRiskManagementData;->mAdditionalCheckTable:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCrmCountryCode(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/CardRiskManagementData;->mCrmCountryCode:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method
