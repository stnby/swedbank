.class public final Lcom/mastercard/upgrade/profile/CvmIssuerOptions;
.super Ljava/lang/Object;


# instance fields
.field private ackAlwaysRequiredIfCurrencyNotProvided:Z
    .annotation runtime Lflexjson/h;
        a = "ackAlwaysRequiredIfCurrencyNotProvided"
    .end annotation
.end field

.field private ackAlwaysRequiredIfCurrencyProvided:Z
    .annotation runtime Lflexjson/h;
        a = "ackAlwaysRequiredIfCurrencyProvided"
    .end annotation
.end field

.field private ackAutomaticallyResetByApplication:Z
    .annotation runtime Lflexjson/h;
        a = "ackAutomaticallyResetByApplication"
    .end annotation
.end field

.field private ackPreEntryAllowed:Z
    .annotation runtime Lflexjson/h;
        a = "ackPreEntryAllowed"
    .end annotation
.end field

.field private pinAlwaysRequiredIfCurrencyNotProvided:Z
    .annotation runtime Lflexjson/h;
        a = "pinAlwaysRequiredIfCurrencyNotProvided"
    .end annotation
.end field

.field private pinAlwaysRequiredIfCurrencyProvided:Z
    .annotation runtime Lflexjson/h;
        a = "pinAlwaysRequiredIfCurrencyProvided"
    .end annotation
.end field

.field private pinAutomaticallyResetByApplication:Z
    .annotation runtime Lflexjson/h;
        a = "pinAutomaticallyResetByApplication"
    .end annotation
.end field

.field private pinPreEntryAllowed:Z
    .annotation runtime Lflexjson/h;
        a = "pinPreEntryAllowed"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setBit(BI)B
    .locals 1

    const/4 v0, 0x1

    shl-int p1, v0, p1

    or-int/2addr p0, p1

    int-to-byte p0, p0

    return p0
.end method


# virtual methods
.method public final getAckAlwaysRequiredIfCurrencyNotProvided()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackAlwaysRequiredIfCurrencyNotProvided:Z

    return v0
.end method

.method public final getAckAlwaysRequiredIfCurrencyProvided()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackAlwaysRequiredIfCurrencyProvided:Z

    return v0
.end method

.method public final getAckAutomaticallyResetByApplication()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackAutomaticallyResetByApplication:Z

    return v0
.end method

.method public final getAckPreEntryAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackPreEntryAllowed:Z

    return v0
.end method

.method public final getMpaObject()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 3
    .annotation runtime Lflexjson/h;
        b = false
    .end annotation

    invoke-virtual {p0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getAckAlwaysRequiredIfCurrencyProvided()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    invoke-static {v1, v0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->setBit(BI)B

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getAckAlwaysRequiredIfCurrencyNotProvided()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x6

    invoke-static {v0, v2}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->setBit(BI)B

    move-result v0

    :cond_1
    invoke-virtual {p0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getPinAlwaysRequiredIfCurrencyProvided()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->setBit(BI)B

    move-result v0

    :cond_2
    invoke-virtual {p0}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->getPinAlwaysRequiredIfCurrencyNotProvided()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->setBit(BI)B

    move-result v0

    :cond_3
    const/4 v2, 0x1

    new-array v2, v2, [B

    aput-byte v0, v2, v1

    invoke-static {v2}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    return-object v0
.end method

.method public final getPinAlwaysRequiredIfCurrencyNotProvided()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinAlwaysRequiredIfCurrencyNotProvided:Z

    return v0
.end method

.method public final getPinAlwaysRequiredIfCurrencyProvided()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinAlwaysRequiredIfCurrencyProvided:Z

    return v0
.end method

.method public final getPinAutomaticallyResetByApplication()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinAutomaticallyResetByApplication:Z

    return v0
.end method

.method public final getPinPreEntryAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinPreEntryAllowed:Z

    return v0
.end method

.method public final setAckAlwaysRequiredIfCurrencyNotProvided(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackAlwaysRequiredIfCurrencyNotProvided:Z

    return-void
.end method

.method public final setAckAlwaysRequiredIfCurrencyProvided(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackAlwaysRequiredIfCurrencyProvided:Z

    return-void
.end method

.method public final setAckAutomaticallyResetByApplication(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackAutomaticallyResetByApplication:Z

    return-void
.end method

.method public final setAckPreEntryAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->ackPreEntryAllowed:Z

    return-void
.end method

.method public final setPinAlwaysRequiredIfCurrencyNotProvided(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinAlwaysRequiredIfCurrencyNotProvided:Z

    return-void
.end method

.method public final setPinAlwaysRequiredIfCurrencyProvided(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinAlwaysRequiredIfCurrencyProvided:Z

    return-void
.end method

.method public final setPinAutomaticallyResetByApplication(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinAutomaticallyResetByApplication:Z

    return-void
.end method

.method public final setPinPreEntryAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/CvmIssuerOptions;->pinPreEntryAllowed:Z

    return-void
.end method
