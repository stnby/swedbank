.class public Lcom/mastercard/upgrade/profile/Record;
.super Ljava/lang/Object;


# instance fields
.field private mRecordNumber:B
    .annotation runtime Lflexjson/h;
        a = "recordNumber"
    .end annotation
.end field

.field private mRecordValue:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "recordValue"
    .end annotation
.end field

.field private mSfi:B
    .annotation runtime Lflexjson/h;
        a = "sfi"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static valueOf([B)Lcom/mastercard/upgrade/profile/Record;
    .locals 4

    new-instance v0, Lcom/mastercard/upgrade/utils/b/e;

    const-class v1, Lcom/mastercard/upgrade/profile/Record;

    invoke-direct {v0, v1}, Lcom/mastercard/upgrade/utils/b/e;-><init>(Ljava/lang/Class;)V

    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    const-class v2, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    new-instance v3, Lcom/mastercard/upgrade/utils/b/a;

    invoke-direct {v3}, Lcom/mastercard/upgrade/utils/b/a;-><init>()V

    invoke-virtual {p0, v2, v3}, Lflexjson/j;->a(Ljava/lang/Class;Lflexjson/o;)Lflexjson/j;

    move-result-object p0

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    new-instance v3, Lcom/mastercard/upgrade/utils/b/c;

    invoke-direct {v3}, Lcom/mastercard/upgrade/utils/b/c;-><init>()V

    invoke-virtual {p0, v2, v3}, Lflexjson/j;->a(Ljava/lang/Class;Lflexjson/o;)Lflexjson/j;

    move-result-object p0

    iget-object v0, v0, Lcom/mastercard/upgrade/utils/b/e;->a:Ljava/lang/Class;

    invoke-virtual {p0, v1, v0}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/upgrade/profile/Record;

    return-object p0
.end method


# virtual methods
.method public getRecordNumber()B
    .locals 1

    iget-byte v0, p0, Lcom/mastercard/upgrade/profile/Record;->mRecordNumber:B

    return v0
.end method

.method public getRecordValue()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/Record;->mRecordValue:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public getSfi()B
    .locals 1

    iget-byte v0, p0, Lcom/mastercard/upgrade/profile/Record;->mSfi:B

    return v0
.end method

.method public setRecordNumber(B)V
    .locals 0

    iput-byte p1, p0, Lcom/mastercard/upgrade/profile/Record;->mRecordNumber:B

    return-void
.end method

.method public setRecordValue(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/Record;->mRecordValue:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public setSfi(B)V
    .locals 0

    iput-byte p1, p0, Lcom/mastercard/upgrade/profile/Record;->mSfi:B

    return-void
.end method

.method public toJsonString()Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/mastercard/upgrade/utils/b/e;

    const-class v1, Lcom/mastercard/upgrade/profile/Record;

    invoke-direct {v0, v1}, Lcom/mastercard/upgrade/utils/b/e;-><init>(Ljava/lang/Class;)V

    invoke-static {p0}, Lcom/mastercard/upgrade/utils/b/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
