.class public final Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;
.super Ljava/lang/Object;


# instance fields
.field private mAid:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "aid"
    .end annotation
.end field

.field private mCiacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "ciacDecline"
    .end annotation
.end field

.field private mCvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "cvrMaskAnd"
    .end annotation
.end field

.field private mGpoResponse:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "gpoResponse"
    .end annotation
.end field

.field private mPaymentFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "paymentFci"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mAid:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCiacDecline()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mCiacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCvrMaskAnd()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mCvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getGpoResponse()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mGpoResponse:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getPaymentFci()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mPaymentFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final setAid(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mAid:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCiacDecline(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mCiacDecline:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCvrMaskAnd(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mCvrMaskAnd:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setGpoResponse(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mGpoResponse:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setPaymentFci(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/AlternateContactlessPaymentData;->mPaymentFci:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method
