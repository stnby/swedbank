.class public final Lcom/mastercard/upgrade/profile/BusinessLogicModule;
.super Ljava/lang/Object;


# instance fields
.field private mApplicationLifeCycleData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "applicationLifeCycleData"
    .end annotation
.end field

.field private mCardLayoutDescription:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "cardLayoutDescription"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mCardholderValidators:Lcom/mastercard/upgrade/profile/CardholderValidators;
    .annotation runtime Lflexjson/h;
        a = "cardholderValidators"
    .end annotation
.end field

.field private mChipCvmIssuerOptions:Lcom/mastercard/upgrade/profile/CvmIssuerOptions;
    .annotation runtime Lflexjson/h;
        a = "mChipCvmIssuerOptions"
    .end annotation
.end field

.field private mCvmResetTimeout:I
    .annotation runtime Lflexjson/h;
        a = "cvmResetTimeout"
    .end annotation
.end field

.field private mDualTapResetTimeout:I
    .annotation runtime Lflexjson/h;
        a = "dualTapResetTimeout"
    .end annotation
.end field

.field private mMagstripeCvmIssuerOptions:Lcom/mastercard/upgrade/profile/CvmIssuerOptions;
    .annotation runtime Lflexjson/h;
        a = "magstripeCvmIssuerOptions"
    .end annotation
.end field

.field private mSecurityWord:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "securityWord"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getApplicationLifeCycleData()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mApplicationLifeCycleData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCardLayoutDescription()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mCardLayoutDescription:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getCardholderValidators()Lcom/mastercard/upgrade/profile/CardholderValidators;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mCardholderValidators:Lcom/mastercard/upgrade/profile/CardholderValidators;

    return-object v0
.end method

.method public final getCvmResetTimeout()I
    .locals 1

    iget v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mCvmResetTimeout:I

    return v0
.end method

.method public final getDualTapResetTimeout()I
    .locals 1

    iget v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mDualTapResetTimeout:I

    return v0
.end method

.method public final getMChipCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mChipCvmIssuerOptions:Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    return-object v0
.end method

.method public final getMagstripeCvmIssuerOptions()Lcom/mastercard/upgrade/profile/CvmIssuerOptions;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mMagstripeCvmIssuerOptions:Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    return-object v0
.end method

.method public final getSecurityWord()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mSecurityWord:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final setApplicationLifeCycleData(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mApplicationLifeCycleData:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCardLayoutDescription(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mCardLayoutDescription:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setCardholderValidators(Lcom/mastercard/upgrade/profile/CardholderValidators;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mCardholderValidators:Lcom/mastercard/upgrade/profile/CardholderValidators;

    return-void
.end method

.method public final setCvmResetTimeout(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mCvmResetTimeout:I

    return-void
.end method

.method public final setDualTapResetTimeout(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mDualTapResetTimeout:I

    return-void
.end method

.method public final setMChipCvmIssuerOptions(Lcom/mastercard/upgrade/profile/CvmIssuerOptions;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mChipCvmIssuerOptions:Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    return-void
.end method

.method public final setMagstripeCvmIssuerOptions(Lcom/mastercard/upgrade/profile/CvmIssuerOptions;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mMagstripeCvmIssuerOptions:Lcom/mastercard/upgrade/profile/CvmIssuerOptions;

    return-void
.end method

.method public final setSecurityWord(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/BusinessLogicModule;->mSecurityWord:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method
