.class public final Lcom/mastercard/upgrade/profile/DigitizedCardProfile;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lflexjson/h;
    a = "cardProfile"
.end annotation


# instance fields
.field private businessLogicModule:Lcom/mastercard/upgrade/profile/BusinessLogicModule;
    .annotation runtime Lflexjson/h;
        a = "businessLogicModule"
    .end annotation
.end field

.field private cardMetadata:Ljava/lang/String;
    .annotation runtime Lflexjson/h;
        a = "cardMetadata"
    .end annotation
.end field

.field private contactlessSupported:Z
    .annotation runtime Lflexjson/h;
        a = "contactlessSupported"
    .end annotation
.end field

.field private digitizedCardId:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "digitizedCardId"
    .end annotation
.end field

.field private maximumPinTry:I
    .annotation runtime Lflexjson/h;
        a = "maximumPinTry"
    .end annotation
.end field

.field private mobilePinInitialConfiguration:Z
    .annotation runtime Lflexjson/h;
        a = "mobilePinInitialConfiguration"
    .end annotation
.end field

.field private mppLiteModule:Lcom/mastercard/upgrade/profile/MppLiteModule;
    .annotation runtime Lflexjson/h;
        a = "mppLiteModule"
    .end annotation
.end field

.field private remotePaymentSupported:Z
    .annotation runtime Lflexjson/h;
        a = "remoteSupported"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->maximumPinTry:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->mobilePinInitialConfiguration:Z

    iput-boolean v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->contactlessSupported:Z

    iput-boolean v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->remotePaymentSupported:Z

    return-void
.end method

.method public static valueOf([B)Lcom/mastercard/upgrade/profile/DigitizedCardProfile;
    .locals 3

    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance p0, Lflexjson/j;

    invoke-direct {p0}, Lflexjson/j;-><init>()V

    const-class v1, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    new-instance v2, Lcom/mastercard/upgrade/utils/b/a;

    invoke-direct {v2}, Lcom/mastercard/upgrade/utils/b/a;-><init>()V

    invoke-virtual {p0, v1, v2}, Lflexjson/j;->a(Ljava/lang/Class;Lflexjson/o;)Lflexjson/j;

    move-result-object p0

    const-class v1, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    invoke-virtual {p0, v0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;

    return-object p0
.end method


# virtual methods
.method public final getBusinessLogicModule()Lcom/mastercard/upgrade/profile/BusinessLogicModule;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->businessLogicModule:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    return-object v0
.end method

.method public final getCardId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->digitizedCardId:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getCardMetadata()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->cardMetadata:Ljava/lang/String;

    return-object v0
.end method

.method public final getContactlessSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->contactlessSupported:Z

    return v0
.end method

.method public final getDigitizedCardId()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->digitizedCardId:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getMaximumPinTry()I
    .locals 1

    iget v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->maximumPinTry:I

    return v0
.end method

.method public final getMppLiteModule()Lcom/mastercard/upgrade/profile/MppLiteModule;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->mppLiteModule:Lcom/mastercard/upgrade/profile/MppLiteModule;

    return-object v0
.end method

.method public final getRemotePaymentSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->remotePaymentSupported:Z

    return v0
.end method

.method public final isMobilePinInitialConfiguration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->mobilePinInitialConfiguration:Z

    return v0
.end method

.method public final setBusinessLogicModule(Lcom/mastercard/upgrade/profile/BusinessLogicModule;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->businessLogicModule:Lcom/mastercard/upgrade/profile/BusinessLogicModule;

    return-void
.end method

.method public final setCardMetadata(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->cardMetadata:Ljava/lang/String;

    return-void
.end method

.method public final setContactlessSupported(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->contactlessSupported:Z

    return-void
.end method

.method public final setDigitizedCardId(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->digitizedCardId:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setMaximumPinTry(I)V
    .locals 0

    iput p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->maximumPinTry:I

    return-void
.end method

.method public final setMobilePinInitialConfiguration(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->mobilePinInitialConfiguration:Z

    return-void
.end method

.method public final setMppLiteModule(Lcom/mastercard/upgrade/profile/MppLiteModule;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->mppLiteModule:Lcom/mastercard/upgrade/profile/MppLiteModule;

    return-void
.end method

.method public final setRemotePaymentSupported(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mastercard/upgrade/profile/DigitizedCardProfile;->remotePaymentSupported:Z

    return-void
.end method

.method public final toJsonString()Ljava/lang/String;
    .locals 6

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/upgrade/utils/b/b;

    invoke-direct {v1}, Lcom/mastercard/upgrade/utils/b/b;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/upgrade/utils/b/f;

    invoke-direct {v1}, Lcom/mastercard/upgrade/utils/b/f;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
