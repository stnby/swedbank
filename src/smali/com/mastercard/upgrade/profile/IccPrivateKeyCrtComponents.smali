.class public final Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;
.super Ljava/lang/Object;


# instance fields
.field private dp:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "dp"
    .end annotation
.end field

.field private dq:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "dq"
    .end annotation
.end field

.field private p:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "p"
    .end annotation
.end field

.field private q:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "q"
    .end annotation
.end field

.field private u:Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .annotation runtime Lflexjson/h;
        a = "u"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDp()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->dp:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getDq()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->dq:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getP()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->p:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getQ()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->q:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final getU()Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->u:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-object v0
.end method

.method public final setDp(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->dp:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setDq(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->dq:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setP(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->p:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setQ(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->q:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method

.method public final setU(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/profile/IccPrivateKeyCrtComponents;->u:Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    return-void
.end method
