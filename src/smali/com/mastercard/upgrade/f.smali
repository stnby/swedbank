.class public final Lcom/mastercard/upgrade/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/b;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field private final b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

.field private c:Lcom/mastercard/upgrade/a;

.field private d:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

.field private e:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DATABASE UPGRADE"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/f;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/upgrade/f;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    return-void
.end method

.method private a()V
    .locals 8

    const-string v0, "ALTER TABLE mobile_keys ADD COLUMN checksum BLOB;"

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DELETE FROM mobile_keys WHERE mobile_key_type = \'keySetId\'"

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DELETE FROM mobile_keys WHERE mobile_key_type = \'confidentiality_key\'"

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "SELECT * FROM mobile_keys"

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    new-instance v2, Lcom/mastercard/upgrade/d/a/b;

    invoke-direct {v2}, Lcom/mastercard/upgrade/d/a/b;-><init>()V

    const-string v3, "mobile_keyset_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mobile_key_type"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "mobile_key_value"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mobileKeyValue encrypted old: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    new-instance v7, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-direct {v7, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-virtual {v6, v7}, Lcom/mastercard/upgrade/a;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mobileKeyValue plain without MAC old: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v6, v5}, Lcom/mastercard/upgrade/a;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v5

    iput-object v3, v2, Lcom/mastercard/upgrade/d/a/b;->a:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/upgrade/d/a/b;->b:[B

    iput-object v4, v2, Lcom/mastercard/upgrade/d/a/b;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/upgrade/d/a/b;

    iget-object v2, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "UPDATE mobile_keys SET mobile_key_value = ?, checksum = ? WHERE mobile_key_type = ?  ;"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/b;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/b;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/b;->b:[B

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mastercard/upgrade/f;->a(Ljava/lang/String;)[B

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Upgrade MobileKeys | Checksum : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/b;->b:[B

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v3, 0x3

    iget-object v1, v1, Lcom/mastercard/upgrade/d/a/b;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v1, -0x1

    cmp-long v1, v3, v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;)[B
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/upgrade/f;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Ljava/security/GeneralSecurityException;

    invoke-direct {v0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private b()V
    .locals 7

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SELECT * FROM card_profiles_list"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v1, "Update card profile table while empty"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS card_profiles_list"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE card_profiles_lists (card_id TEXT PRIMARY KEY NOT NULL, card_state INTEGER NOT NULL, profile_data BLOB NOT NULL, profile_data_version TEXT NOT NULL, checksum BLOB); "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v2, Lcom/mastercard/upgrade/d/a/a;

    invoke-direct {v2}, Lcom/mastercard/upgrade/d/a/a;-><init>()V

    const-string v4, "card_id"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/mastercard/upgrade/d/a/a;->a:Ljava/lang/String;

    const-string v4, "card_state"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/mastercard/upgrade/d/a/a;->c:Ljava/lang/String;

    const-string v4, "profile_version"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/mastercard/upgrade/d/a/a;->d:Ljava/lang/String;

    const-string v4, "card_data"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    iget-object v5, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    new-instance v6, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-direct {v6, v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-virtual {v5, v6}, Lcom/mastercard/upgrade/a;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CardProfile without MAC old: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v5, v4}, Lcom/mastercard/upgrade/a;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    iput-object v4, v2, Lcom/mastercard/upgrade/d/a/a;->b:[B

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v2, "Update card profile table"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DROP TABLE IF EXISTS card_profiles_list"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "CREATE TABLE card_profiles_lists (card_id TEXT PRIMARY KEY NOT NULL, card_state INTEGER NOT NULL, profile_data BLOB NOT NULL, profile_data_version TEXT NOT NULL, checksum BLOB); "

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/upgrade/d/a/a;

    iget-object v2, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "INSERT INTO card_profiles_lists ( card_id , profile_data , card_state , profile_data_version , checksum )  VALUES (?,?,?,?,?);"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/a;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/a;->b:[B

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mastercard/upgrade/f;->a(Ljava/lang/String;)[B

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Upgrade CardProfile | Checksum : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x2

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/a;->b:[B

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v4, 0x3

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/a;->c:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 v4, 0x4

    iget-object v1, v1, Lcom/mastercard/upgrade/d/a/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v4, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v1, 0x5

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v1, -0x1

    cmp-long v1, v3, v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method

.method private c()V
    .locals 10

    const-string v0, "SELECT * FROM suk_list"

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v1, "Update transaction credentials table while empty"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS suk_list"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE transaction_credentials_list (card_id TEXT NOT NULL, credential_id TEXT NOT NULL, credential_status INTEGER NOT NULL, atc INTEGER, time_stamp TEXT NOT NULL, credential BLOB NOT NULL, credential_data_version TEXT, checksum BLOB, PRIMARY KEY (card_id,atc)); "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    new-instance v4, Lcom/mastercard/upgrade/d/a/c;

    invoke-direct {v4}, Lcom/mastercard/upgrade/d/a/c;-><init>()V

    const-string v5, "card_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/upgrade/d/a/c;->a:Ljava/lang/String;

    const-string v5, "atc"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    long-to-int v5, v5

    iput v5, v4, Lcom/mastercard/upgrade/d/a/c;->e:I

    const-string v5, "suk"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    iget-object v6, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    new-instance v7, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-direct {v7, v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-virtual {v6, v7}, Lcom/mastercard/upgrade/a;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Transaction credentials without MAC old : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v6, v5}, Lcom/mastercard/upgrade/a;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/upgrade/d/a/c;->b:[B

    const-string v5, "time_stamp"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/upgrade/d/a/c;->f:Ljava/lang/String;

    const-string v5, "suk_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/mastercard/upgrade/d/a/c;->c:Ljava/lang/String;

    const-string v5, "suk_status"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    long-to-int v5, v5

    iput v5, v4, Lcom/mastercard/upgrade/d/a/c;->d:I

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v4, "Update transaction credentials table"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "DROP TABLE IF EXISTS suk_list"

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "CREATE TABLE transaction_credentials_list (card_id TEXT NOT NULL, credential_id TEXT NOT NULL, credential_status INTEGER NOT NULL, atc INTEGER, time_stamp TEXT NOT NULL, credential BLOB NOT NULL, credential_data_version TEXT, checksum BLOB, PRIMARY KEY (card_id,atc)); "

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/upgrade/d/a/c;

    iget-object v4, v1, Lcom/mastercard/upgrade/d/a/c;->a:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/String;

    aput-object v4, v6, v3

    iget-object v4, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "SELECT profile_data_version FROM card_profiles_lists WHERE card_id = ?"

    invoke-virtual {v4, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "profile_data_version"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_2
    move-object v6, v2

    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v1, Lcom/mastercard/upgrade/d/a/c;->a:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/mastercard/upgrade/d/a/c;->c:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, v1, Lcom/mastercard/upgrade/d/a/c;->d:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v7, v1, Lcom/mastercard/upgrade/d/a/c;->e:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/mastercard/upgrade/d/a/c;->f:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/mastercard/upgrade/d/a/c;->b:[B

    invoke-static {v7}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mastercard/upgrade/f;->a(Ljava/lang/String;)[B

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Upgrade TransactionCredential | Checksum : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "INSERT INTO transaction_credentials_list ( card_id , credential_id , credential_status , atc , time_stamp , credential , credential_data_version , checksum )  VALUES (?,?,?,?,?,?,?,?);"

    invoke-virtual {v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v7

    iget-object v8, v1, Lcom/mastercard/upgrade/d/a/c;->a:Ljava/lang/String;

    invoke-virtual {v7, v5, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v5, 0x2

    iget-object v8, v1, Lcom/mastercard/upgrade/d/a/c;->c:Ljava/lang/String;

    invoke-virtual {v7, v5, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v5, 0x3

    iget v8, v1, Lcom/mastercard/upgrade/d/a/c;->d:I

    int-to-long v8, v8

    invoke-virtual {v7, v5, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 v5, 0x4

    iget v8, v1, Lcom/mastercard/upgrade/d/a/c;->e:I

    int-to-long v8, v8

    invoke-virtual {v7, v5, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 v5, 0x5

    iget-object v8, v1, Lcom/mastercard/upgrade/d/a/c;->f:Ljava/lang/String;

    invoke-virtual {v7, v5, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v5, 0x6

    iget-object v1, v1, Lcom/mastercard/upgrade/d/a/c;->b:[B

    invoke-virtual {v7, v5, v1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v1, 0x7

    invoke-virtual {v7, v1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v1, 0x8

    invoke-virtual {v7, v1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v4

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_3

    goto/16 :goto_0

    :cond_3
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method private d()V
    .locals 10

    const-string v0, "SELECT * FROM card_transaction_list"

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS card_transaction_list"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE transaction_logs (transaction_log_id INTEGER PRIMARY KEY AUTOINCREMENT, time_stamp INTEGER NOT NULL, transaction_id TEXT, card_id TEXT NOT NULL, log_version TEXT NOT NULL, checksum BLOB, transaction_data BLOB NOT NULL );"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    new-instance v2, Lcom/mastercard/upgrade/d/a/d;

    invoke-direct {v2}, Lcom/mastercard/upgrade/d/a/d;-><init>()V

    const-string v3, "card_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "trans_log_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "transaction_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "time_stamp"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "transaction_data"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    iget-object v8, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    new-instance v9, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-direct {v9, v7}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-virtual {v8, v9}, Lcom/mastercard/upgrade/a;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Transaction Logs without MAC old : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v8, v7}, Lcom/mastercard/upgrade/a;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v7

    iput-object v3, v2, Lcom/mastercard/upgrade/d/a/d;->b:Ljava/lang/String;

    iput-object v4, v2, Lcom/mastercard/upgrade/d/a/d;->a:Ljava/lang/String;

    iput-object v5, v2, Lcom/mastercard/upgrade/d/a/d;->c:Ljava/lang/String;

    iput-object v6, v2, Lcom/mastercard/upgrade/d/a/d;->e:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    iput-object v3, v2, Lcom/mastercard/upgrade/d/a/d;->d:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DROP TABLE IF EXISTS card_transaction_list"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "CREATE TABLE transaction_logs (transaction_log_id INTEGER PRIMARY KEY AUTOINCREMENT, time_stamp INTEGER NOT NULL, transaction_id TEXT, card_id TEXT NOT NULL, log_version TEXT NOT NULL, checksum BLOB, transaction_data BLOB NOT NULL );"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/upgrade/d/a/d;

    iget-object v2, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "INSERT INTO transaction_logs ( card_id , transaction_id , transaction_data , time_stamp , log_version , checksum )  VALUES (?,?,?,?,?,?);"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    const-string v3, "1.0"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/d;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/d;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/d;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/mastercard/upgrade/d/a/d;->d:[B

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mastercard/upgrade/f;->a(Ljava/lang/String;)[B

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Upgrade TransactionLogs | Checksum : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x1

    iget-object v6, v1, Lcom/mastercard/upgrade/d/a/d;->b:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v5, 0x2

    iget-object v6, v1, Lcom/mastercard/upgrade/d/a/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v5, 0x3

    iget-object v6, v1, Lcom/mastercard/upgrade/d/a/d;->d:[B

    invoke-virtual {v2, v5, v6}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v5, 0x4

    iget-object v1, v1, Lcom/mastercard/upgrade/d/a/d;->e:Ljava/lang/String;

    invoke-virtual {v2, v5, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v1, 0x5

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v1, 0x6

    invoke-virtual {v2, v1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v1, -0x1

    cmp-long v1, v3, v1

    if-eqz v1, :cond_2

    goto/16 :goto_0

    :cond_2
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V
    .locals 8

    new-instance v0, Lcom/mastercard/upgrade/a;

    iget-object v1, p0, Lcom/mastercard/upgrade/f;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {v0, p2, v1, p3}, Lcom/mastercard/upgrade/a;-><init>(Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    iput-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p3, p0, Lcom/mastercard/upgrade/f;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    :try_start_0
    const-string p1, "SELECT * FROM environment_container"

    iget-object p2, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    const-wide/16 v0, -0x1

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez p2, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string p2, "Update environment table while empty"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v4}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string p2, "DROP TABLE IF EXISTS environment_container"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string p2, "CREATE TABLE environment_data_container (remote_url BLOB, checksum BLOB); "

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string p2, "remote_url"

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v5, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    invoke-virtual {v5, p2}, Lcom/mastercard/upgrade/a;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->b:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    const-string v5, "Update environment container table"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1, v5, v4}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "DROP TABLE IF EXISTS environment_container"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "CREATE TABLE environment_data_container (remote_url BLOB, checksum BLOB); "

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "INSERT INTO environment_data_container ( remote_url , checksum )  VALUES (?,?);"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p1

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mastercard/upgrade/f;->a(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    invoke-virtual {p1, v3, p2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    cmp-long p1, v4, v0

    if-eqz p1, :cond_5

    :goto_0
    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string p2, "SELECT * FROM card_profiles_list"

    invoke-virtual {p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string p2, "CREATE TABLE wallet_data_container (card_id TEXT PRIMARY KEY NOT NULL, wallet_data_version TEXT NOT NULL, wallet_data BLOB NOT NULL, checksum BLOB); "

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_1
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    new-instance p3, Lcom/mastercard/upgrade/d/a/e;

    invoke-direct {p3}, Lcom/mastercard/upgrade/d/a/e;-><init>()V

    const-string v4, "card_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p3, Lcom/mastercard/upgrade/d/a/e;->a:Ljava/lang/String;

    const-string v4, "wallet_data"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    iget-object v5, p0, Lcom/mastercard/upgrade/f;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v5, v4}, Lcom/mastercard/upgrade/a;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    iput-object v4, p3, Lcom/mastercard/upgrade/d/a/e;->b:[B

    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result p3

    if-nez p3, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    iget-object p1, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string p3, "CREATE TABLE wallet_data_container (card_id TEXT PRIMARY KEY NOT NULL, wallet_data_version TEXT NOT NULL, wallet_data BLOB NOT NULL, checksum BLOB); "

    invoke-virtual {p1, p3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/mastercard/upgrade/d/a/e;

    iget-object p3, p0, Lcom/mastercard/upgrade/f;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "INSERT INTO wallet_data_container ( wallet_data , wallet_data_version , checksum , card_id )  VALUES (?,?,?,?);"

    invoke-virtual {p3, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p3

    const-string v4, "1.0"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p2, Lcom/mastercard/upgrade/d/a/e;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p2, Lcom/mastercard/upgrade/d/a/e;->b:[B

    invoke-static {v6}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mastercard/upgrade/f;->a(Ljava/lang/String;)[B

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Upgrade WalletData | Checksum : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p2, Lcom/mastercard/upgrade/d/a/e;->b:[B

    invoke-virtual {p3, v3, v6}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {p3, v2, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x3

    invoke-virtual {p3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v4, 0x4

    iget-object p2, p2, Lcom/mastercard/upgrade/d/a/e;->a:Ljava/lang/String;

    invoke-virtual {p3, v4, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {p3}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v4

    invoke-virtual {p3}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    cmp-long p2, v4, v0

    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string p2, "Unable to update the database"

    invoke-direct {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/mastercard/upgrade/f;->a()V

    invoke-direct {p0}, Lcom/mastercard/upgrade/f;->b()V

    invoke-direct {p0}, Lcom/mastercard/upgrade/f;->c()V

    invoke-direct {p0}, Lcom/mastercard/upgrade/f;->d()V

    return-void

    :cond_5
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string p2, "Unable to update the database"

    invoke-direct {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/security/GeneralSecurityException;->printStackTrace()V

    return-void
.end method
