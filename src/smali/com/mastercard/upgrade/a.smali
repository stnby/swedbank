.class final Lcom/mastercard/upgrade/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/upgrade/a$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field private final b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

.field private c:Landroid/content/Context;

.field private final d:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DATABASE UPGRADE"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/a;->d:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    iput-object p1, p0, Lcom/mastercard/upgrade/a;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/mastercard/upgrade/a;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    iput-object p3, p0, Lcom/mastercard/upgrade/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    return-void
.end method

.method private a(Ljava/lang/String;)[B
    .locals 6

    iget-object v0, p0, Lcom/mastercard/upgrade/a;->c:Landroid/content/Context;

    const-string v1, "MCBP"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_created"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "storage"

    invoke-static {}, Lcom/mastercard/upgrade/a;->c()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v3, "key_created"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    const-string v1, "deviceID"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "deviceID"

    invoke-interface {v1, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    iget-object v1, p0, Lcom/mastercard/upgrade/a;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->getMpaKey()[B

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mastercard/upgrade/a;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->getMpaKey()[B

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/mastercard/upgrade/a;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->getMpaKey()[B

    move-result-object v1

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/mastercard/mcbp/mpsdk/a;->b:[B

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    :try_start_0
    invoke-static {p1}, Lcom/mastercard/upgrade/a;->d([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    const-string v3, "storage"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v3, p1

    array-length v4, v1

    add-int/2addr v3, v4

    array-length v4, v0

    add-int/2addr v3, v4

    new-array v3, v3, [B

    array-length v4, p1

    invoke-static {p1, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, p1

    array-length v5, v1

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, p1

    array-length v1, v1

    add-int/2addr v4, v1

    array-length v1, v0

    invoke-static {v0, v2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p1}, Lcom/mastercard/upgrade/utils/a;->a([B)V

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/a;->a([B)V

    :try_start_1
    invoke-static {v3}, Lcom/mastercard/upgrade/a;->d([B)[B

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-static {v3}, Lcom/mastercard/upgrade/utils/a;->a([B)V

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to generate the DB key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to generate the DB key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected static a([B[B)[B
    .locals 1

    sget v0, Lcom/mastercard/upgrade/a$a;->b:I

    invoke-static {p0, p1, v0}, Lcom/mastercard/upgrade/a;->a([B[BI)[B

    move-result-object p0

    return-object p0
.end method

.method private static a([B[BI)[B
    .locals 5

    sget v0, Lcom/mastercard/upgrade/a$a;->a:I

    const/16 v1, -0x80

    const/16 v2, 0x10

    const/4 v3, 0x0

    if-ne p2, v0, :cond_0

    array-length p2, p0

    add-int/2addr p2, v2

    array-length v0, p0

    rem-int/2addr v0, v2

    sub-int/2addr p2, v0

    new-array p2, p2, [B

    array-length v0, p0

    invoke-static {p0, v3, p2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p0, p0

    aput-byte v1, p2, p0

    sget p0, Lcom/mastercard/upgrade/a$a;->a:I

    invoke-static {p2, p1, p0}, Lcom/mastercard/upgrade/a;->b([B[BI)[B

    move-result-object p0

    return-object p0

    :cond_0
    sget p2, Lcom/mastercard/upgrade/a$a;->b:I

    invoke-static {p0, p1, p2}, Lcom/mastercard/upgrade/a;->b([B[BI)[B

    move-result-object p0

    if-eqz p0, :cond_4

    array-length p1, p0

    if-lt p1, v2, :cond_4

    array-length p1, p0

    const/4 p2, 0x1

    sub-int/2addr p1, p2

    const/4 v0, 0x0

    :goto_0
    array-length v4, p0

    sub-int/2addr v4, v2

    if-lt p1, v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    aget-byte v4, p0, p1

    if-eqz v4, :cond_1

    aget-byte v4, p0, p1

    if-ne v4, v1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_3

    array-length p1, p0

    sub-int/2addr p1, v0

    new-array p2, p1, [B

    invoke-static {p0, v3, p2, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    :cond_3
    return-object p0

    :cond_4
    new-instance p0, Ljava/security/GeneralSecurityException;

    const-string p1, "Invalid input size"

    invoke-direct {p0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static b([B[BI)[B
    .locals 3

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, p1, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    :try_start_0
    const-string p1, "ECB"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AES/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/NoPadding"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p1

    sget v1, Lcom/mastercard/upgrade/a$a;->a:I

    if-ne p2, v1, :cond_0

    const/4 p2, 0x1

    invoke-virtual {p1, p2, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    invoke-virtual {p1, p2, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    :goto_0
    invoke-virtual {p1, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/security/GeneralSecurityException;

    invoke-virtual {p0}, Ljava/security/GeneralSecurityException;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static c()[B
    .locals 3

    const/16 v0, 0x10

    new-array v0, v0, [B

    :try_start_0
    const-string v1, "SHA1PRNG"

    invoke-static {v1}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [B

    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    :goto_0
    return-object v0
.end method

.method private static d([B)[B
    .locals 1

    :try_start_0
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected final a()[B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a;->c:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/upgrade/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B
    .locals 3

    :try_start_0
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dataFromDatabase unEncryptDataForStorageWithMac(): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dataFromDatabase length unEncryptDataForStorageWithMac(): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v1

    add-int/lit8 v1, v1, -0x40

    invoke-virtual {p1, v0, v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->copyOfRange(II)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v1

    add-int/lit8 v1, v1, -0x40

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->copyOfRange(II)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    iget-object v1, p0, Lcom/mastercard/upgrade/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {v1, v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->isMacValid([B[B)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/mastercard/upgrade/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-direct {v1, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    invoke-interface {p1, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->unencryptStoredDataForUse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "Attempted to unencrypt data for which the MAC was invalid"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a([B)[B
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v0

    :try_start_0
    sget v1, Lcom/mastercard/upgrade/a$a;->a:I

    invoke-static {p1, v0, v1}, Lcom/mastercard/upgrade/a;->a([B[BI)[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/a;->a([B)V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/a;->a([B)V

    throw p1
.end method

.method public final b([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/upgrade/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->encryptDataForStorage([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/upgrade/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->generateMac([B)[B

    move-result-object p1

    array-length v1, v0

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v1, v1, [B

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v0, v0

    array-length v2, p1

    invoke-static {p1, v3, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-direct {p1, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected final b()[B
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/a;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/upgrade/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/mastercard/upgrade/a;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;->encryptDataForStorage([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p1

    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method
