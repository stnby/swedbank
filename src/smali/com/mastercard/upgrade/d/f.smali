.class public final Lcom/mastercard/upgrade/d/f;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[B

.field public final c:[B

.field public final d:B

.field public final e:Z

.field public final f:Z

.field public final g:[B

.field public final h:[B

.field public final i:[B

.field public final j:Ljava/lang/String;

.field private final k:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;[B[BB[BZZ[B[B[BLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/upgrade/d/f;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/mastercard/upgrade/d/f;->b:[B

    iput-object p3, p0, Lcom/mastercard/upgrade/d/f;->c:[B

    iput-byte p4, p0, Lcom/mastercard/upgrade/d/f;->d:B

    const/4 p1, 0x1

    if-eq p4, p1, :cond_1

    const/4 p1, 0x3

    if-ne p4, p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/mastercard/upgrade/d/f;->k:[B

    goto :goto_1

    :cond_1
    :goto_0
    iput-object p5, p0, Lcom/mastercard/upgrade/d/f;->k:[B

    :goto_1
    iput-boolean p6, p0, Lcom/mastercard/upgrade/d/f;->e:Z

    iput-boolean p7, p0, Lcom/mastercard/upgrade/d/f;->f:Z

    iput-object p8, p0, Lcom/mastercard/upgrade/d/f;->g:[B

    iput-object p9, p0, Lcom/mastercard/upgrade/d/f;->h:[B

    iput-object p10, p0, Lcom/mastercard/upgrade/d/f;->i:[B

    iput-object p11, p0, Lcom/mastercard/upgrade/d/f;->j:Ljava/lang/String;

    return-void
.end method
