.class public final Lcom/mastercard/upgrade/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mastercard/upgrade/i$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field private final b:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

.field private c:Lcom/mastercard/upgrade/a;

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

.field private f:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;


# direct methods
.method public constructor <init>(Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/i;->b:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    iput-object p1, p0, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;[BLjava/lang/String;ILjava/lang/String;)J
    .locals 1

    const-string v0, "INSERT INTO suk_list ( card_id , suk_id , suk_status , atc , time_stamp , suk )  VALUES (?,?,?,?,?,?);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 p5, 0x2

    invoke-virtual {p1, p5, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    int-to-long p5, p6

    const/4 p3, 0x3

    invoke-virtual {p1, p3, p5, p6}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    int-to-long p2, p2

    const/4 p5, 0x4

    invoke-virtual {p1, p5, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 p2, 0x5

    invoke-virtual {p1, p2, p7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    iget-object p2, p0, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {p2, p4}, Lcom/mastercard/upgrade/a;->b([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    const/4 p3, 0x6

    invoke-virtual {p1, p3, p2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide p2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    return-wide p2
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v0, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "SELECT card_id, time_stamp, tx_atc, tx_date, transaction_id, trans_log FROM card_transaction_list;"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    const-string v2, "DROP TABLE IF EXISTS card_transaction_list;"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE card_transaction_list (trans_log_id INTEGER PRIMARY KEY AUTOINCREMENT, time_stamp INTEGER NOT NULL, transaction_id TEXT NOT NULL, card_id TEXT NOT NULL, transaction_data BLOB NOT NULL );"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v11, Lcom/mastercard/upgrade/d/h;

    const-string v4, "card_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v4, "time_stamp"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v4, "tx_atc"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v4, "tx_date"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v4, "transaction_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    const-string v4, "trans_log"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    move-object v4, v11

    invoke-direct/range {v4 .. v10}, Lcom/mastercard/upgrade/d/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_1
    const-string v3, "DROP TABLE IF EXISTS card_transaction_list;"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v3, "CREATE TABLE card_transaction_list (trans_log_id INTEGER PRIMARY KEY AUTOINCREMENT, time_stamp INTEGER NOT NULL, transaction_id TEXT NOT NULL, card_id TEXT NOT NULL, transaction_data BLOB NOT NULL );"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mastercard/upgrade/d/h;

    iget-object v4, v3, Lcom/mastercard/upgrade/d/h;->d:[B

    const/16 v5, 0x11

    const/4 v6, 0x0

    invoke-static {v4, v6, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v7

    const/16 v8, 0x15

    invoke-static {v4, v5, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v11

    aget-byte v14, v4, v8

    const/16 v5, 0x16

    aget-byte v5, v4, v5

    const/16 v8, 0x17

    aget-byte v8, v4, v8

    const/16 v9, 0x18

    const/16 v10, 0x1e

    invoke-static {v4, v9, v10}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v12

    const/16 v9, 0x21

    invoke-static {v4, v10, v9}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v13

    new-instance v4, Lcom/mastercard/upgrade/d/i;

    invoke-static {v7}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    const/4 v7, 0x1

    if-ne v5, v7, :cond_2

    const/4 v15, 0x1

    goto :goto_1

    :cond_2
    const/4 v15, 0x0

    :goto_1
    if-ne v8, v7, :cond_3

    const/16 v16, 0x1

    goto :goto_2

    :cond_3
    const/16 v16, 0x0

    :goto_2
    move-object v9, v4

    invoke-direct/range {v9 .. v16}, Lcom/mastercard/upgrade/d/i;-><init>(Ljava/lang/String;[B[B[BBZZ)V

    iget-object v5, v3, Lcom/mastercard/upgrade/d/h;->b:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    iget-object v5, v4, Lcom/mastercard/upgrade/d/i;->c:[B

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iget-object v8, v0, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    iget-object v9, v0, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    iget-object v10, v3, Lcom/mastercard/upgrade/d/h;->a:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    iget-object v10, v4, Lcom/mastercard/upgrade/d/i;->a:[B

    iget-object v13, v4, Lcom/mastercard/upgrade/d/i;->b:[B

    invoke-static {v13}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v13

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    iget-byte v4, v4, Lcom/mastercard/upgrade/d/i;->d:B

    iget-object v5, v3, Lcom/mastercard/upgrade/d/h;->c:[B

    move/from16 v16, v4

    move-object/from16 v17, v5

    invoke-interface/range {v8 .. v17}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->serializeTransactionLog(Ljava/lang/String;[BJJIB[B)[B

    move-result-object v4

    iget-object v5, v0, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v5, v4}, Lcom/mastercard/upgrade/a;->b([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v4

    const-string v5, "INSERT INTO card_transaction_list ( card_id , transaction_id , transaction_data , time_stamp )  VALUES (?,?,?,?);"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    iget-object v8, v0, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    iget-object v9, v3, Lcom/mastercard/upgrade/d/h;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v7, 0x2

    iget-object v8, v3, Lcom/mastercard/upgrade/d/h;->c:[B

    invoke-static {v8}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v7, 0x3

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    invoke-virtual {v5, v7, v4}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    iget-object v3, v3, Lcom/mastercard/upgrade/d/h;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v3

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    goto/16 :goto_0

    :cond_4
    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v2, "Unable to update the database"

    invoke-direct {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)V
    .locals 5

    :cond_0
    :try_start_0
    const-string v0, "card_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UPDATE card_profiles_list SET card_id = \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\' WHERE card_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\';"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "card_data"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iget-object v2, p0, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v2}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/upgrade/b/a/e;->a([B)Lcom/mastercard/upgrade/b/a/e;

    move-result-object v0

    iget-object v2, p0, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->getDatabaseUpgradeCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    move-result-object v2

    new-instance v3, Lcom/mastercard/upgrade/a/a$1;

    invoke-direct {v3, v0, v2}, Lcom/mastercard/upgrade/a/a$1;-><init>(Lcom/mastercard/upgrade/b/a/e;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)V

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;->getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/i;->f:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    new-instance v0, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;-><init>()V

    invoke-virtual {v0, v3}, Lcom/mastercard/mpsdk/card/profile/sdk/DigitizedCardJson;->getContent(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;)[B

    move-result-object v0

    iget-object v2, p0, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v2, v0}, Lcom/mastercard/upgrade/a;->b([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    invoke-static {v2}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "encryptedCardProfile with Mac Hex: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Lcom/mastercard/upgrade/d/a;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/mastercard/upgrade/d/a;-><init>(I)V

    new-instance v3, Lcom/mastercard/upgrade/utils/b/e;

    const-class v4, Lcom/mastercard/upgrade/d/b;

    invoke-direct {v3, v4}, Lcom/mastercard/upgrade/utils/b/e;-><init>(Ljava/lang/Class;)V

    new-instance v3, Lcom/mastercard/upgrade/d/b;

    iget v2, v2, Lcom/mastercard/upgrade/d/a;->a:I

    invoke-direct {v3, v2}, Lcom/mastercard/upgrade/d/b;-><init>(I)V

    invoke-static {v3}, Lcom/mastercard/upgrade/utils/b/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Storing Card Profile Version: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mastercard/upgrade/i;->f:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "UPDATE card_profiles_list SET card_data = ? , wallet_data = ?  , profile_version = ? WHERE  card_id = ? ;"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v0, 0x2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/mastercard/upgrade/i;->f:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v0, 0x4

    invoke-virtual {v3, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List<",
            "Lcom/mastercard/upgrade/i$a;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, v9, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SELECT suk_id, suk_info, suk_cl_umd, suk_cl_md, suk_rp_umd, suk_rp_md, idn, atc, hash, card_id FROM suk_list;"

    const/4 v2, 0x0

    invoke-virtual {v10, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const-string v0, "DROP TABLE IF EXISTS suk_list;"

    invoke-virtual {v10, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE suk_list (card_id TEXT NOT NULL, suk_id TEXT NOT NULL, suk_status INTEGER NOT NULL, atc INTEGER, time_stamp TEXT NOT NULL, suk BLOB NOT NULL, PRIMARY KEY (card_id,atc)); "

    invoke-virtual {v10, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_0
    new-instance v0, Lcom/mastercard/upgrade/d/g;

    const-string v3, "suk_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v3, "suk_info"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    const-string v3, "suk_cl_umd"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iget-object v4, v9, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v4}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v14

    const-string v3, "suk_cl_md"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iget-object v4, v9, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v4}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v15

    const-string v3, "suk_rp_umd"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iget-object v4, v9, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v4}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v16

    const-string v3, "suk_rp_md"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iget-object v4, v9, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v4}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v17

    const-string v3, "idn"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v18

    const-string v3, "atc"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v19

    const-string v3, "hash"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v20

    const-string v3, "card_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object v11, v0

    invoke-direct/range {v11 .. v21}, Lcom/mastercard/upgrade/d/g;-><init>(Ljava/lang/String;[B[B[B[B[B[B[B[BLjava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    const-string v0, "DROP TABLE IF EXISTS suk_list;"

    invoke-virtual {v10, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE suk_list (card_id TEXT NOT NULL, suk_id TEXT NOT NULL, suk_status INTEGER NOT NULL, atc INTEGER, time_stamp TEXT NOT NULL, suk BLOB NOT NULL, PRIMARY KEY (card_id,atc)); "

    invoke-virtual {v10, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v9, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->getDatabaseUpgradeCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mastercard/upgrade/a/d;->a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;)[Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;

    move-result-object v0

    array-length v11, v0

    const/4 v13, 0x0

    :goto_1
    const-wide/16 v14, -0x1

    if-ge v13, v11, :cond_3

    aget-object v1, v0, v13

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getAtc()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0000"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v9, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "000000"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v9, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getInfo()[B

    move-result-object v18

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukContactlessUmd()[B

    move-result-object v19

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSukRemotePaymentUmd()[B

    move-result-object v20

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessMd()[B

    move-result-object v21

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentMd()[B

    move-result-object v22

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyContactlessUmd()[B

    move-result-object v23

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getSessionKeyRemotePaymentUmd()[B

    move-result-object v24

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getIdn()[B

    move-result-object v25

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getHash()[B

    move-result-object v27

    move-object/from16 v16, v2

    move-object/from16 v17, v4

    move/from16 v26, v3

    invoke-interface/range {v16 .. v27}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->serializeTransactionCredential(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)[B

    move-result-object v5

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v7

    new-instance v2, Ljava/util/Date;

    move/from16 v28, v13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-direct {v2, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/TimeUtils;->getFormattedDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    iget-object v2, v9, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/String;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/mastercard/upgrade/i;->a(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;[BLjava/lang/String;ILjava/lang/String;)J

    move-result-wide v1

    cmp-long v1, v1, v14

    if-eqz v1, :cond_2

    add-int/lit8 v13, v28, 0x1

    goto/16 :goto_1

    :cond_2
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mastercard/upgrade/i$a;

    invoke-virtual {v1}, Lcom/mastercard/upgrade/i$a;->a()I

    move-result v2

    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v3

    if-eq v2, v3, :cond_5

    iget v3, v1, Lcom/mastercard/upgrade/i$a;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0000"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/mastercard/upgrade/i$a;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "000000"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v9, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    const/4 v5, 0x1

    new-array v6, v5, [B

    const/4 v11, 0x0

    aput-byte v11, v6, v11

    new-array v7, v5, [B

    aput-byte v11, v7, v11

    new-array v8, v5, [B

    aput-byte v11, v8, v11

    new-array v12, v5, [B

    aput-byte v11, v12, v11

    new-array v13, v5, [B

    aput-byte v11, v13, v11

    new-array v14, v5, [B

    aput-byte v11, v14, v11

    new-array v15, v5, [B

    aput-byte v11, v15, v11

    move-object/from16 v29, v0

    new-array v0, v5, [B

    aput-byte v11, v0, v11

    new-array v5, v5, [B

    aput-byte v11, v5, v11

    move-object/from16 v16, v2

    move-object/from16 v17, v4

    move-object/from16 v18, v6

    move-object/from16 v19, v7

    move-object/from16 v20, v8

    move-object/from16 v21, v12

    move-object/from16 v22, v13

    move-object/from16 v23, v14

    move-object/from16 v24, v15

    move-object/from16 v25, v0

    move/from16 v26, v3

    move-object/from16 v27, v5

    invoke-interface/range {v16 .. v27}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->serializeTransactionCredential(Ljava/lang/String;[B[B[B[B[B[B[B[BI[B)[B

    move-result-object v5

    iget-object v6, v1, Lcom/mastercard/upgrade/i$a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/mastercard/upgrade/i$a;->a()I

    move-result v7

    iget-object v8, v1, Lcom/mastercard/upgrade/i$a;->c:Ljava/lang/String;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/mastercard/upgrade/i;->a(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;[BLjava/lang/String;ILjava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_5
    move-object/from16 v29, v0

    move-wide v2, v14

    const/4 v11, 0x0

    :goto_3
    move-wide v14, v2

    move-object/from16 v0, v29

    goto/16 :goto_2

    :cond_6
    return-void

    :catch_1
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    const-string v0, "SELECT mobile_key_type, mobile_key_value FROM mobile_keys"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void

    :cond_0
    const-string v1, "mobile_key_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mobile_key_value"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Encrypted Key value from older DB : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :try_start_0
    iget-object v3, p0, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v3}, Lcom/mastercard/upgrade/a;->b()[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mastercard/upgrade/a;->a([B[B)[B

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Plain Key value from older DB : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;->getDatabaseUpgradeCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;->encryptDataUsingRemoteKekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Mobile Key with RM Kek Encrypted: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/mastercard/upgrade/a;->b([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Mobile Key with database mac encrypt: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    const-string v3, "UPDATE mobile_keys SET mobile_key_type = ? , mobile_key_value = ?  WHERE  mobile_key_type = ? ;"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "keyValue updateKeysValues: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "keyValue length: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "transport_key"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v4, :cond_1

    const-string v4, "TRANSPORT_KEY"

    invoke-virtual {v3, v7, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v3, v6, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v3, v5, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    :goto_0
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v8, "mac_key"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "MAC_KEY"

    invoke-virtual {v3, v7, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v3, v6, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v3, v5, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v8, "dataencryption_key"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "DEK_KEY"

    invoke-virtual {v3, v7, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v3, v6, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v3, v5, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/database/exception/InvalidInput; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const-string v0, "BEGIN TRANSACTION;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TEMPORARY TABLE mobile_keys_backup(mobile_keyset_id, mobile_key_type, mobile_key_value);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO mobile_keys_backup SELECT mobile_keyset_id, mobile_key_type, mobile_key_value FROM mobile_keys;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE mobile_keys;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE mobile_keys (mobile_keyset_id TEXT, mobile_key_type TEXT NOT NULL, mobile_key_value BLOB NOT NULL, PRIMARY KEY (mobile_keyset_id,mobile_key_type)); "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO mobile_keys SELECT mobile_keyset_id, mobile_key_type, mobile_key_value FROM mobile_keys_backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE mobile_keys_backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "COMMIT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V
    .locals 8

    new-instance v0, Lcom/mastercard/upgrade/a;

    iget-object v1, p0, Lcom/mastercard/upgrade/i;->a:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

    invoke-direct {v0, p2, v1, p3}, Lcom/mastercard/upgrade/a;-><init>(Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    iput-object v0, p0, Lcom/mastercard/upgrade/i;->c:Lcom/mastercard/upgrade/a;

    iput-object p3, p0, Lcom/mastercard/upgrade/i;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    const-string p2, "SELECT token_unique_reference, card_id FROM token_unique_reference_list;"

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "card_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "token_unique_reference"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v2, p0, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    const-string p2, "DROP TABLE IF EXISTS token_unique_reference_list;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_2
    const-string p2, "SELECT * from table_transaction_credential_status"

    invoke-virtual {p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const-string v1, "token_unique_reference"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v1, "transaction_credential_status"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v1, "time_stamp"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v1, "atc"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    new-instance v1, Lcom/mastercard/upgrade/i$a;

    move-object v2, v1

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/mastercard/upgrade/i$a;-><init>(Lcom/mastercard/upgrade/i;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    const-string p2, "DROP TABLE IF EXISTS table_transaction_credential_status;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/mastercard/upgrade/i;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    invoke-direct {p0, p1}, Lcom/mastercard/upgrade/i;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    const-string p2, "ALTER TABLE card_profiles_list ADD COLUMN wallet_data BLOB NULL;"

    const-string v0, "ALTER TABLE card_profiles_list ADD COLUMN profile_version TEXT NULL;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/mastercard/upgrade/i;->d:Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/util/HashMap;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_6

    const-string p2, "SELECT card_id , card_data FROM card_profiles_list"

    invoke-virtual {p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p3

    if-nez p3, :cond_5

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/mastercard/upgrade/i;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)V

    :cond_6
    :goto_0
    const-string p2, "BEGIN TRANSACTION;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "CREATE TEMPORARY TABLE environment_container_backup(cms_mpa_id, remote_url, mpa_fgp, alcd, mno, latitude, longitude, wsp_name, wallet_pin_state);"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "INSERT INTO environment_container_backup SELECT cms_mpa_id, remote_url, mpa_fgp, alcd, mno, latitude, longitude, wsp_name, wallet_pin_state FROM environment_container;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "DROP TABLE environment_container;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "CREATE TABLE environment_container (cms_mpa_id TEXT, cms_provider_id TEXT, remote_url TEXT, mpa_fgp BLOB, alcd TEXT, mno TEXT , latitude DOUBLE , longitude DOUBLE , wsp_name TEXT , wallet_pin_state INTEGER); "

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "INSERT INTO environment_container SELECT cms_mpa_id, null, remote_url, mpa_fgp, alcd, mno, latitude, longitude, wsp_name, wallet_pin_state FROM environment_container_backup;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "DROP TABLE environment_container_backup;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "COMMIT;"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mastercard/upgrade/i;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
