.class public final Lcom/mastercard/upgrade/utils/bytes/ByteArray;
.super Ljava/lang/Object;


# instance fields
.field private mData:[B


# direct methods
.method private constructor <init>(B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    iput-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    return-void
.end method

.method private constructor <init>(C)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    const v1, 0xff00

    and-int/2addr v1, p1

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x1

    aput-byte p1, v0, v1

    return-void
.end method

.method private constructor <init>(IB)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    aput-byte p2, v0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    invoke-static {p1}, Lcom/mastercard/upgrade/utils/b;->b(Ljava/lang/String;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    return-void
.end method

.method private constructor <init>(S)V
    .locals 0

    int-to-char p1, p1

    invoke-direct {p0, p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>(C)V

    return-void
.end method

.method private constructor <init>([BI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, p2, [B

    iput-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public static get(I)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 2

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>(IB)V

    return-object v0
.end method

.method public static of(B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>(B)V

    return-object v0
.end method

.method public static of(C)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>(C)V

    return-object v0
.end method

.method public static of(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 2

    invoke-virtual {p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p0

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>([BI)V

    return-object v0
.end method

.method public static of(Ljava/lang/String;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static of(S)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-direct {v0, p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>(S)V

    return-object v0
.end method

.method public static of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 2

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>([BI)V

    return-object v0
.end method

.method public static of([BI)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 1

    new-instance v0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-direct {v0, p0, p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>([BI)V

    return-object v0
.end method


# virtual methods
.method public final append(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 6

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    if-ne p1, p0, :cond_1

    invoke-static {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p1

    :goto_0
    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v1, v1

    iget-object v2, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v2, v2

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->resize(I)V

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v5

    invoke-static {v2, v3, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-ne p1, p0, :cond_2

    invoke-virtual {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->clear()V

    :cond_2
    :goto_1
    return-object p0
.end method

.method public final appendByte(B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v0, v0

    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->resize(I)V

    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    aput-byte p1, v1, v0

    return-object p0
.end method

.method public final bitWiseAnd(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getLength()I

    move-result v0

    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v1, v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v0, v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    aget-byte v2, v2, v1

    aget-byte v3, p1, v1

    and-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->of([B)Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid AND Mask"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final clear()V
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    :cond_0
    return-void
.end method

.method public final copyOfRange(II)Lcom/mastercard/upgrade/utils/bytes/ByteArray;
    .locals 3

    sub-int v0, p2, p1

    new-instance v1, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    iget-object v2, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    invoke-static {v2, p1, p2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    invoke-direct {v1, p1, v0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;-><init>([BI)V

    return-object v1
.end method

.method public final fill(B)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([BB)V

    return-void
.end method

.method public final getByte(I)B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    aget-byte p1, v0, p1

    return p1
.end method

.method public final getBytes()[B
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    return-object v0
.end method

.method public final getLength()I
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v0, v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final isEqual(Lcom/mastercard/upgrade/utils/bytes/ByteArray;)Z
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    return p1
.end method

.method public final resize(I)V
    .locals 3

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v0, v0

    if-le p1, v0, :cond_0

    new-array p1, p1, [B

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    array-length v1, v1

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    :cond_0
    return-void
.end method

.method public final setByte(IB)V
    .locals 1

    iget-object v0, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    aput-byte p2, v0, p1

    return-void
.end method

.method public final toBase64String()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final toHexString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    invoke-static {v1}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toUtf8String()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->mData:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method
