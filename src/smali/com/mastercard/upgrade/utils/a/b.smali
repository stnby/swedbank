.class public final enum Lcom/mastercard/upgrade/utils/a/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mastercard/upgrade/utils/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mastercard/upgrade/utils/a/b;

.field public static final enum b:Lcom/mastercard/upgrade/utils/a/b;

.field public static final enum c:Lcom/mastercard/upgrade/utils/a/b;

.field public static final enum d:Lcom/mastercard/upgrade/utils/a/b;

.field public static final enum e:Lcom/mastercard/upgrade/utils/a/b;

.field public static final enum f:Lcom/mastercard/upgrade/utils/a/b;

.field public static final enum g:Lcom/mastercard/upgrade/utils/a/b;

.field private static final synthetic h:[Lcom/mastercard/upgrade/utils/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/mastercard/upgrade/utils/a/b;

    const-string v1, "UPGRADE_IMEI_TO_ANDROID_ID"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mastercard/upgrade/utils/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->a:Lcom/mastercard/upgrade/utils/a/b;

    new-instance v0, Lcom/mastercard/upgrade/utils/a/b;

    const-string v1, "UPGRADE_TRANSACTION_LOG"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/mastercard/upgrade/utils/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->b:Lcom/mastercard/upgrade/utils/a/b;

    new-instance v0, Lcom/mastercard/upgrade/utils/a/b;

    const-string v1, "UPGRADE_SEPARATE_MODULE_FOR_ONE_ZERO_SERIES"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/mastercard/upgrade/utils/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->c:Lcom/mastercard/upgrade/utils/a/b;

    new-instance v0, Lcom/mastercard/upgrade/utils/a/b;

    const-string v1, "UPGRADE_SEPARATE_MODULE_FOR_ONE_ONE_SERIES"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/mastercard/upgrade/utils/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->d:Lcom/mastercard/upgrade/utils/a/b;

    new-instance v0, Lcom/mastercard/upgrade/utils/a/b;

    const-string v1, "UPGRADE_TWO_ZERO_STRUCTURE"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/mastercard/upgrade/utils/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->e:Lcom/mastercard/upgrade/utils/a/b;

    new-instance v0, Lcom/mastercard/upgrade/utils/a/b;

    const-string v1, "UPGRADE_TWO_TWO_WITH_TAMPER_DETECTION"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/mastercard/upgrade/utils/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->f:Lcom/mastercard/upgrade/utils/a/b;

    new-instance v0, Lcom/mastercard/upgrade/utils/a/b;

    const-string v1, "UPGRADE_TWO_THREE_ONE"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/mastercard/upgrade/utils/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->g:Lcom/mastercard/upgrade/utils/a/b;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mastercard/upgrade/utils/a/b;

    sget-object v1, Lcom/mastercard/upgrade/utils/a/b;->a:Lcom/mastercard/upgrade/utils/a/b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mastercard/upgrade/utils/a/b;->b:Lcom/mastercard/upgrade/utils/a/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mastercard/upgrade/utils/a/b;->c:Lcom/mastercard/upgrade/utils/a/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mastercard/upgrade/utils/a/b;->d:Lcom/mastercard/upgrade/utils/a/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mastercard/upgrade/utils/a/b;->e:Lcom/mastercard/upgrade/utils/a/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mastercard/upgrade/utils/a/b;->f:Lcom/mastercard/upgrade/utils/a/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mastercard/upgrade/utils/a/b;->g:Lcom/mastercard/upgrade/utils/a/b;

    aput-object v1, v0, v8

    sput-object v0, Lcom/mastercard/upgrade/utils/a/b;->h:[Lcom/mastercard/upgrade/utils/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mastercard/upgrade/utils/a/b;
    .locals 1

    const-class v0, Lcom/mastercard/upgrade/utils/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mastercard/upgrade/utils/a/b;

    return-object p0
.end method

.method public static values()[Lcom/mastercard/upgrade/utils/a/b;
    .locals 1

    sget-object v0, Lcom/mastercard/upgrade/utils/a/b;->h:[Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {v0}, [Lcom/mastercard/upgrade/utils/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mastercard/upgrade/utils/a/b;

    return-object v0
.end method
