.class public final Lcom/mastercard/upgrade/utils/b/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mastercard/upgrade/utils/b/e;->a:Ljava/lang/Class;

    return-void
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Lflexjson/k;

    invoke-direct {v0}, Lflexjson/k;-><init>()V

    const-string v1, "*.class"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/upgrade/utils/b/b;

    invoke-direct {v1}, Lcom/mastercard/upgrade/utils/b/b;-><init>()V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/upgrade/utils/b/d;

    invoke-direct {v1}, Lcom/mastercard/upgrade/utils/b/d;-><init>()V

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    new-instance v1, Lcom/mastercard/upgrade/utils/b/f;

    invoke-direct {v1}, Lcom/mastercard/upgrade/utils/b/f;-><init>()V

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    invoke-virtual {v0, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
