.class public final Lcom/mastercard/upgrade/utils/b/d;
.super Lflexjson/c/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lflexjson/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final isInline()Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final transform(Ljava/lang/Object;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [B

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    invoke-static {v0}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/mastercard/upgrade/utils/b/d;->getContext()Lflexjson/i;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflexjson/i;->c(Ljava/lang/String;)V

    return-void
.end method
