.class public final Lcom/mastercard/upgrade/utils/b/b;
.super Lflexjson/c/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lflexjson/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final isInline()Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final transform(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/mastercard/upgrade/utils/bytes/ByteArray;

    invoke-virtual {p1}, Lcom/mastercard/upgrade/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/mastercard/upgrade/utils/b/b;->getContext()Lflexjson/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lflexjson/i;->c(Ljava/lang/String;)V

    return-void
.end method
