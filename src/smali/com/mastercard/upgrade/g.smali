.class public final Lcom/mastercard/upgrade/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mastercard/upgrade/b;


# instance fields
.field private final a:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mastercard/mpsdk/utils/log/McbpLoggerInstance;->getInstance()Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/g;->a:Lcom/mastercard/mpsdk/componentinterface/McbpLogger;

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V
    .locals 18

    move-object/from16 v0, p1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "SELECT card_id, time_stamp, trans_log FROM card_transaction_list"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const-string v1, "DROP TABLE IF EXISTS card_transaction_list"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE card_transaction_list (card_id TEXT NOT NULL, time_stamp INTEGER  PRIMARY KEY NOT NULL, tx_atc TEXT NOT NULL, tx_date TEXT NOT NULL, transaction_id BLOB, trans_log BLOB NOT NULL  ); "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v3, "trans_log"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const-string v4, "time_stamp"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v4, 0x11

    const/4 v15, 0x0

    invoke-static {v3, v15, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    invoke-static {v5}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    const/16 v5, 0x15

    invoke-static {v3, v4, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v7

    const/16 v4, 0x17

    invoke-static {v3, v5, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v8

    aget-byte v9, v3, v4

    const/16 v4, 0x18

    const/16 v5, 0x20

    invoke-static {v3, v4, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v10

    aget-byte v4, v3, v5

    const/4 v14, 0x1

    if-ne v4, v14, :cond_1

    const/4 v11, 0x1

    goto :goto_0

    :cond_1
    const/4 v11, 0x0

    :goto_0
    const/16 v4, 0x21

    aget-byte v4, v3, v4

    if-ne v4, v14, :cond_2

    const/4 v12, 0x1

    goto :goto_1

    :cond_2
    const/4 v12, 0x0

    :goto_1
    const/16 v4, 0x22

    const/16 v5, 0x25

    invoke-static {v3, v4, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v13

    const/16 v4, 0x2b

    invoke-static {v3, v5, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v17

    const/16 v5, 0x2d

    invoke-static {v3, v4, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    new-instance v4, Lcom/mastercard/upgrade/d/f;

    move-object v5, v4

    move-object/from16 v14, v17

    move-object v15, v3

    invoke-direct/range {v5 .. v16}, Lcom/mastercard/upgrade/d/f;-><init>(Ljava/lang/String;[B[BB[BZZ[B[B[BLjava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const-string v2, "DROP TABLE IF EXISTS card_transaction_list"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE card_transaction_list (card_id TEXT NOT NULL, time_stamp INTEGER  PRIMARY KEY NOT NULL, tx_atc TEXT NOT NULL, tx_date TEXT NOT NULL, transaction_id BLOB, trans_log BLOB NOT NULL  ); "

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mastercard/upgrade/d/f;

    const-string v3, "INSERT INTO card_transaction_list ( card_id , time_stamp , tx_atc , tx_date , transaction_id , trans_log )  VALUES (?,?,?,?,?,?);"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->a:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->j:Ljava/lang/String;

    const/4 v6, 0x2

    invoke-virtual {v3, v6, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x3

    iget-object v7, v2, Lcom/mastercard/upgrade/d/f;->c:[B

    invoke-static {v7}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->g:[B

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/b;->a([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x4

    invoke-virtual {v3, v7, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x5

    const/4 v8, 0x0

    new-array v9, v8, [B

    invoke-virtual {v3, v4, v9}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->a:Ljava/lang/String;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "0x"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "0x"

    invoke-virtual {v4, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :cond_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    invoke-static {v4}, Lcom/mastercard/upgrade/utils/b;->b(Ljava/lang/String;)[B

    move-result-object v4

    goto :goto_3

    :cond_4
    new-array v4, v8, [B

    :goto_3
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->b:[B

    if-nez v4, :cond_5

    new-array v4, v7, [B

    goto :goto_4

    :cond_5
    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->b:[B

    :goto_4
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v4, v5, [B

    iget-byte v7, v2, Lcom/mastercard/upgrade/d/f;->d:B

    aput-byte v7, v4, v8

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v4, v2, Lcom/mastercard/upgrade/d/f;->e:Z

    if-eqz v4, :cond_6

    new-array v4, v5, [B

    aput-byte v5, v4, v8

    goto :goto_5

    :cond_6
    new-array v4, v5, [B

    aput-byte v8, v4, v8

    :goto_5
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v4, v2, Lcom/mastercard/upgrade/d/f;->f:Z

    if-eqz v4, :cond_7

    new-array v4, v5, [B

    aput-byte v5, v4, v8

    goto :goto_6

    :cond_7
    new-array v4, v5, [B

    aput-byte v8, v4, v8

    :goto_6
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->h:[B

    const/4 v7, 0x6

    if-nez v4, :cond_8

    new-array v4, v7, [B

    goto :goto_7

    :cond_8
    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->h:[B

    :goto_7
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, v2, Lcom/mastercard/upgrade/d/f;->i:[B

    if-nez v4, :cond_9

    new-array v2, v6, [B

    goto :goto_8

    :cond_9
    iget-object v2, v2, Lcom/mastercard/upgrade/d/f;->i:[B

    :goto_8
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v9}, Lcom/mastercard/upgrade/utils/a;->a(Ljava/util/List;)[B

    move-result-object v2

    invoke-virtual {v3, v7, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v6

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    const-wide/16 v2, -0x1

    cmp-long v2, v6, v2

    if-eqz v2, :cond_a

    goto/16 :goto_2

    :cond_a
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;

    const-string v1, "Unable to update the database"

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/database/exception/LdeUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    return-void
.end method
