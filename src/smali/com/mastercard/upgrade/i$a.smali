.class final Lcom/mastercard/upgrade/i$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mastercard/upgrade/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:Ljava/lang/String;

.field final synthetic d:Lcom/mastercard/upgrade/i;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/mastercard/upgrade/i;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mastercard/upgrade/i$a;->d:Lcom/mastercard/upgrade/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mastercard/upgrade/i$a;->a:Ljava/lang/String;

    iput p3, p0, Lcom/mastercard/upgrade/i$a;->b:I

    iput-object p4, p0, Lcom/mastercard/upgrade/i$a;->e:Ljava/lang/String;

    iput-object p5, p0, Lcom/mastercard/upgrade/i$a;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lcom/mastercard/upgrade/i$a;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "UNUSED_DISCARDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v1, "UNKNOWN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string v1, "USED_FOR_DSRP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_3
    const-string v1, "UNUSED_ACTIVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_4
    const-string v1, "USED_FOR_CONTACTLESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    :goto_2
    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->getValue()I

    move-result v0

    return v0

    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_ACTIVE:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    goto :goto_2

    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->USED_FOR_DSRP:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    goto :goto_2

    :pswitch_3
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->USED_FOR_CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    goto :goto_2

    :pswitch_4
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;->UNUSED_DISCARDED:Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus$Status;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4250dd7f -> :sswitch_4
        -0x302b2671 -> :sswitch_3
        0x68c0725 -> :sswitch_2
        0x19d1382a -> :sswitch_1
        0x7e619674 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
