.class public final Lcom/mastercard/upgrade/j;
.super Ljava/lang/Object;


# instance fields
.field public a:Landroid/database/sqlite/SQLiteDatabase;

.field public b:Landroid/content/Context;

.field public c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

.field public d:Lcom/mastercard/mpsdk/componentinterface/database/DatabaseUpgradeHelper;

.field public final e:Lcom/mastercard/mpsdk/utils/log/LogUtils;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DATABASE UPGRADE"

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/log/LogUtils;->getInstance(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/log/LogUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/mastercard/upgrade/j;->e:Lcom/mastercard/mpsdk/utils/log/LogUtils;

    return-void
.end method


# virtual methods
.method public final a(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    sget-object v0, Lcom/mastercard/upgrade/utils/a/b;->b:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, v0}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {v0, p2, v1, v2}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    sget-object v0, Lcom/mastercard/upgrade/utils/a/b;->c:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, v0}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object v0

    iget-object v1, p0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {v0, p2, v1, v2}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    invoke-virtual {p0, p1, p2}, Lcom/mastercard/upgrade/j;->b(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1, p2}, Lcom/mastercard/upgrade/j;->c(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public final b(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    sget-object v0, Lcom/mastercard/upgrade/utils/a/b;->e:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, v0}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {p1, p2, v0, v1}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    return-void
.end method

.method public final c(Lcom/mastercard/upgrade/c;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    sget-object v0, Lcom/mastercard/upgrade/utils/a/b;->f:Lcom/mastercard/upgrade/utils/a/b;

    invoke-virtual {p1, v0}, Lcom/mastercard/upgrade/c;->a(Lcom/mastercard/upgrade/utils/a/b;)Lcom/mastercard/upgrade/b;

    move-result-object p1

    iget-object v0, p0, Lcom/mastercard/upgrade/j;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/mastercard/upgrade/j;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;

    invoke-interface {p1, p2, v0, v1}, Lcom/mastercard/upgrade/b;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;)V

    return-void
.end method
