.class public final Lcom/mattprecious/swirl/a$b;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mattprecious/swirl/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_android_font:I = 0x0

.field public static final FontFamilyFont_android_fontStyle:I = 0x2

.field public static final FontFamilyFont_android_fontVariationSettings:I = 0x4

.field public static final FontFamilyFont_android_fontWeight:I = 0x1

.field public static final FontFamilyFont_android_ttcIndex:I = 0x3

.field public static final FontFamilyFont_font:I = 0x5

.field public static final FontFamilyFont_fontStyle:I = 0x6

.field public static final FontFamilyFont_fontVariationSettings:I = 0x7

.field public static final FontFamilyFont_fontWeight:I = 0x8

.field public static final FontFamilyFont_ttcIndex:I = 0x9

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final swirl_Swirl:[I

.field public static final swirl_Swirl_swirl_state:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    .line 247
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mattprecious/swirl/a$b;->CoordinatorLayout:[I

    const/4 v0, 0x7

    .line 250
    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mattprecious/swirl/a$b;->CoordinatorLayout_Layout:[I

    const/4 v0, 0x6

    .line 258
    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mattprecious/swirl/a$b;->FontFamily:[I

    const/16 v0, 0xa

    .line 265
    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/mattprecious/swirl/a$b;->FontFamilyFont:[I

    const/4 v0, 0x1

    .line 276
    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0401ce

    aput v2, v0, v1

    sput-object v0, Lcom/mattprecious/swirl/a$b;->swirl_Swirl:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040116
        0x7f0401c1
    .end array-data

    :array_1
    .array-data 4
        0x10100b3
        0x7f04011b
        0x7f04011c
        0x7f04011d
        0x7f040149
        0x7f040152
        0x7f040153
    .end array-data

    :array_2
    .array-data 4
        0x7f0400df
        0x7f0400e0
        0x7f0400e1
        0x7f0400e2
        0x7f0400e3
        0x7f0400e4
    .end array-data

    :array_3
    .array-data 4
        0x1010532
        0x1010533
        0x101053f
        0x101056f
        0x1010570
        0x7f0400dd
        0x7f0400e5
        0x7f0400e6
        0x7f0400e7
        0x7f040227
    .end array-data
.end method
