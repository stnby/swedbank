.class public final Lcom/mattprecious/swirl/SwirlView;
.super Landroid/widget/ImageView;
.source "SwirlView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mattprecious/swirl/SwirlView$a;
    }
.end annotation


# instance fields
.field private a:Lcom/mattprecious/swirl/SwirlView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, v0}, Lcom/mattprecious/swirl/SwirlView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    sget-object v0, Lcom/mattprecious/swirl/SwirlView$a;->a:Lcom/mattprecious/swirl/SwirlView$a;

    iput-object v0, p0, Lcom/mattprecious/swirl/SwirlView;->a:Lcom/mattprecious/swirl/SwirlView$a;

    .line 33
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 37
    sget-object v0, Lcom/mattprecious/swirl/a$b;->swirl_Swirl:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 38
    sget p2, Lcom/mattprecious/swirl/a$b;->swirl_Swirl_swirl_state:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    if-eq p2, v0, :cond_0

    .line 40
    invoke-static {}, Lcom/mattprecious/swirl/SwirlView$a;->values()[Lcom/mattprecious/swirl/SwirlView$a;

    move-result-object v0

    aget-object p2, v0, p2

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/mattprecious/swirl/SwirlView;->a(Lcom/mattprecious/swirl/SwirlView$a;Z)V

    .line 42
    :cond_0
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    .line 34
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "API 21 required."

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method private static a(Lcom/mattprecious/swirl/SwirlView$a;Lcom/mattprecious/swirl/SwirlView$a;Z)I
    .locals 2

    .line 74
    sget-object v0, Lcom/mattprecious/swirl/SwirlView$1;->a:[I

    invoke-virtual {p1}, Lcom/mattprecious/swirl/SwirlView$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 106
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown state: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_0
    if-eqz p2, :cond_1

    .line 97
    sget-object p1, Lcom/mattprecious/swirl/SwirlView$a;->b:Lcom/mattprecious/swirl/SwirlView$a;

    if-ne p0, p1, :cond_0

    .line 98
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_fingerprint_fp_to_error_state_animation:I

    return p0

    .line 99
    :cond_0
    sget-object p1, Lcom/mattprecious/swirl/SwirlView$a;->a:Lcom/mattprecious/swirl/SwirlView$a;

    if-ne p0, p1, :cond_1

    .line 100
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_error_draw_on_animation:I

    return p0

    .line 104
    :cond_1
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_error:I

    return p0

    :pswitch_1
    if-eqz p2, :cond_3

    .line 87
    sget-object p1, Lcom/mattprecious/swirl/SwirlView$a;->a:Lcom/mattprecious/swirl/SwirlView$a;

    if-ne p0, p1, :cond_2

    .line 88
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_fingerprint_draw_on_animation:I

    return p0

    .line 89
    :cond_2
    sget-object p1, Lcom/mattprecious/swirl/SwirlView$a;->c:Lcom/mattprecious/swirl/SwirlView$a;

    if-ne p0, p1, :cond_3

    .line 90
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_fingerprint_error_state_to_fp_animation:I

    return p0

    .line 94
    :cond_3
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_fingerprint:I

    return p0

    :pswitch_2
    if-eqz p2, :cond_5

    .line 77
    sget-object p1, Lcom/mattprecious/swirl/SwirlView$a;->b:Lcom/mattprecious/swirl/SwirlView$a;

    if-ne p0, p1, :cond_4

    .line 78
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_fingerprint_draw_off_animation:I

    return p0

    .line 79
    :cond_4
    sget-object p1, Lcom/mattprecious/swirl/SwirlView$a;->c:Lcom/mattprecious/swirl/SwirlView$a;

    if-ne p0, p1, :cond_5

    .line 80
    sget p0, Lcom/mattprecious/swirl/a$a;->swirl_error_draw_off_animation:I

    return p0

    :cond_5
    const/4 p0, 0x0

    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(Lcom/mattprecious/swirl/SwirlView$a;Z)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/mattprecious/swirl/SwirlView;->a:Lcom/mattprecious/swirl/SwirlView$a;

    if-ne p1, v0, :cond_0

    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/mattprecious/swirl/SwirlView;->a:Lcom/mattprecious/swirl/SwirlView$a;

    invoke-static {v0, p1, p2}, Lcom/mattprecious/swirl/SwirlView;->a(Lcom/mattprecious/swirl/SwirlView$a;Lcom/mattprecious/swirl/SwirlView$a;Z)I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 54
    invoke-virtual {p0, v1}, Lcom/mattprecious/swirl/SwirlView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 58
    invoke-virtual {p0}, Lcom/mattprecious/swirl/SwirlView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v0}, Landroidx/m/a/a/c;->a(Landroid/content/Context;I)Landroidx/m/a/a/c;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    .line 61
    invoke-virtual {p0}, Lcom/mattprecious/swirl/SwirlView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p0}, Lcom/mattprecious/swirl/SwirlView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-static {p2, v0, v1}, Landroidx/m/a/a/i;->a(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/m/a/a/i;

    move-result-object v1

    .line 63
    :cond_3
    invoke-virtual {p0, v1}, Lcom/mattprecious/swirl/SwirlView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    instance-of p2, v1, Landroid/graphics/drawable/Animatable;

    if-eqz p2, :cond_4

    .line 66
    check-cast v1, Landroid/graphics/drawable/Animatable;

    invoke-interface {v1}, Landroid/graphics/drawable/Animatable;->start()V

    .line 70
    :cond_4
    :goto_0
    iput-object p1, p0, Lcom/mattprecious/swirl/SwirlView;->a:Lcom/mattprecious/swirl/SwirlView$a;

    return-void
.end method

.method public setState(Lcom/mattprecious/swirl/SwirlView$a;)V
    .locals 1

    const/4 v0, 0x1

    .line 46
    invoke-virtual {p0, p1, v0}, Lcom/mattprecious/swirl/SwirlView;->a(Lcom/mattprecious/swirl/SwirlView$a;Z)V

    return-void
.end method
