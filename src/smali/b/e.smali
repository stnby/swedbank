.class public interface abstract Lb/e;
.super Ljava/lang/Object;
.source "BufferedSource.java"

# interfaces
.implements Lb/u;
.implements Ljava/nio/channels/ReadableByteChannel;


# virtual methods
.method public abstract a(Lb/m;)I
.end method

.method public abstract a(B)J
.end method

.method public abstract a(Lb/t;)J
.end method

.method public abstract a(Ljava/nio/charset/Charset;)Ljava/lang/String;
.end method

.method public abstract a(J)V
.end method

.method public abstract a([B)V
.end method

.method public abstract a(JLb/f;)Z
.end method

.method public abstract b(Lb/f;)J
.end method

.method public abstract b(J)Z
.end method

.method public abstract c(Lb/f;)J
.end method

.method public abstract c()Lb/c;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract d()Lb/c;
.end method

.method public abstract d(J)Lb/f;
.end method

.method public abstract f(J)Ljava/lang/String;
.end method

.method public abstract f()Z
.end method

.method public abstract g()Lb/e;
.end method

.method public abstract h(J)[B
.end method

.method public abstract i(J)V
.end method

.method public abstract j()B
.end method

.method public abstract k()S
.end method

.method public abstract l()I
.end method

.method public abstract m()S
.end method

.method public abstract n()I
.end method

.method public abstract o()J
.end method

.method public abstract p()J
.end method

.method public abstract r()Ljava/lang/String;
.end method

.method public abstract s()Ljava/lang/String;
.end method
