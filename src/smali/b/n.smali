.class final Lb/n;
.super Ljava/lang/Object;
.source "PeekSource.java"

# interfaces
.implements Lb/u;


# instance fields
.field private final a:Lb/e;

.field private final b:Lb/c;

.field private c:Lb/q;

.field private d:I

.field private e:Z

.field private f:J


# direct methods
.method constructor <init>(Lb/e;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lb/n;->a:Lb/e;

    .line 41
    invoke-interface {p1}, Lb/e;->c()Lb/c;

    move-result-object p1

    iput-object p1, p0, Lb/n;->b:Lb/c;

    .line 42
    iget-object p1, p0, Lb/n;->b:Lb/c;

    iget-object p1, p1, Lb/c;->a:Lb/q;

    iput-object p1, p0, Lb/n;->c:Lb/q;

    .line 43
    iget-object p1, p0, Lb/n;->c:Lb/q;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lb/n;->c:Lb/q;

    iget p1, p1, Lb/q;->b:I

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    iput p1, p0, Lb/n;->d:I

    return-void
.end method


# virtual methods
.method public a(Lb/c;J)J
    .locals 8

    .line 47
    iget-boolean v0, p0, Lb/n;->e:Z

    if-nez v0, :cond_4

    .line 51
    iget-object v0, p0, Lb/n;->c:Lb/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/n;->c:Lb/q;

    iget-object v1, p0, Lb/n;->b:Lb/c;

    iget-object v1, v1, Lb/c;->a:Lb/q;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lb/n;->d:I

    iget-object v1, p0, Lb/n;->b:Lb/c;

    iget-object v1, v1, Lb/c;->a:Lb/q;

    iget v1, v1, Lb/q;->b:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 53
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Peek source is invalid because upstream source was used"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 56
    :cond_1
    :goto_0
    iget-object v0, p0, Lb/n;->a:Lb/e;

    iget-wide v1, p0, Lb/n;->f:J

    add-long/2addr v1, p2

    invoke-interface {v0, v1, v2}, Lb/e;->b(J)Z

    .line 57
    iget-object v0, p0, Lb/n;->c:Lb/q;

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/n;->b:Lb/c;

    iget-object v0, v0, Lb/c;->a:Lb/q;

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lb/n;->b:Lb/c;

    iget-object v0, v0, Lb/c;->a:Lb/q;

    iput-object v0, p0, Lb/n;->c:Lb/q;

    .line 62
    iget-object v0, p0, Lb/n;->b:Lb/c;

    iget-object v0, v0, Lb/c;->a:Lb/q;

    iget v0, v0, Lb/q;->b:I

    iput v0, p0, Lb/n;->d:I

    .line 65
    :cond_2
    iget-object v0, p0, Lb/n;->b:Lb/c;

    iget-wide v0, v0, Lb/c;->b:J

    iget-wide v2, p0, Lb/n;->f:J

    sub-long/2addr v0, v2

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_3

    const-wide/16 p1, -0x1

    return-wide p1

    .line 68
    :cond_3
    iget-object v2, p0, Lb/n;->b:Lb/c;

    iget-wide v4, p0, Lb/n;->f:J

    move-object v3, p1

    move-wide v6, p2

    invoke-virtual/range {v2 .. v7}, Lb/c;->a(Lb/c;JJ)Lb/c;

    .line 69
    iget-wide v0, p0, Lb/n;->f:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lb/n;->f:J

    return-wide p2

    .line 47
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()Lb/v;
    .locals 1

    .line 74
    iget-object v0, p0, Lb/n;->a:Lb/e;

    invoke-interface {v0}, Lb/e;->a()Lb/v;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    .line 78
    iput-boolean v0, p0, Lb/n;->e:Z

    return-void
.end method
