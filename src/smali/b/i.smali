.class public Lb/i;
.super Lb/v;
.source "ForwardingTimeout.java"


# instance fields
.field private a:Lb/v;


# direct methods
.method public constructor <init>(Lb/v;)V
    .locals 1

    .line 25
    invoke-direct {p0}, Lb/v;-><init>()V

    if-eqz p1, :cond_0

    .line 27
    iput-object p1, p0, Lb/i;->a:Lb/v;

    return-void

    .line 26
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "delegate == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public A_()Lb/v;
    .locals 1

    .line 62
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0}, Lb/v;->A_()Lb/v;

    move-result-object v0

    return-object v0
.end method

.method public B_()J
    .locals 2

    .line 46
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0}, Lb/v;->B_()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lb/v;)Lb/i;
    .locals 1

    if-eqz p1, :cond_0

    .line 37
    iput-object p1, p0, Lb/i;->a:Lb/v;

    return-object p0

    .line 36
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "delegate == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a()Lb/v;
    .locals 1

    .line 32
    iget-object v0, p0, Lb/i;->a:Lb/v;

    return-object v0
.end method

.method public a(J)Lb/v;
    .locals 1

    .line 58
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0, p1, p2}, Lb/v;->a(J)Lb/v;

    move-result-object p1

    return-object p1
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)Lb/v;
    .locals 1

    .line 42
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0, p1, p2, p3}, Lb/v;->a(JLjava/util/concurrent/TimeUnit;)Lb/v;

    move-result-object p1

    return-object p1
.end method

.method public d()J
    .locals 2

    .line 54
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0}, Lb/v;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Lb/v;
    .locals 1

    .line 66
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0}, Lb/v;->f()Lb/v;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 1

    .line 70
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0}, Lb/v;->g()V

    return-void
.end method

.method public z_()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lb/i;->a:Lb/v;

    invoke-virtual {v0}, Lb/v;->z_()Z

    move-result v0

    return v0
.end method
