.class public abstract Landroidx/l/aj;
.super Landroidx/l/n;
.source "Visibility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/l/aj$a;,
        Landroidx/l/aj$b;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "android:visibility:visibility"

    const-string v1, "android:visibility:parent"

    .line 81
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroidx/l/aj;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Landroidx/l/n;-><init>()V

    const/4 v0, 0x3

    .line 98
    iput v0, p0, Landroidx/l/aj;->b:I

    return-void
.end method

.method private a(Landroidx/l/t;Landroidx/l/t;)Landroidx/l/aj$b;
    .locals 6

    .line 193
    new-instance v0, Landroidx/l/aj$b;

    invoke-direct {v0}, Landroidx/l/aj$b;-><init>()V

    const/4 v1, 0x0

    .line 194
    iput-boolean v1, v0, Landroidx/l/aj$b;->a:Z

    .line 195
    iput-boolean v1, v0, Landroidx/l/aj$b;->b:Z

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eqz p1, :cond_0

    .line 196
    iget-object v4, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v5, "android:visibility:visibility"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 197
    iget-object v4, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v5, "android:visibility:visibility"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v0, Landroidx/l/aj$b;->c:I

    .line 198
    iget-object v4, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v5, "android:visibility:parent"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, v0, Landroidx/l/aj$b;->e:Landroid/view/ViewGroup;

    goto :goto_0

    .line 200
    :cond_0
    iput v3, v0, Landroidx/l/aj$b;->c:I

    .line 201
    iput-object v2, v0, Landroidx/l/aj$b;->e:Landroid/view/ViewGroup;

    :goto_0
    if-eqz p2, :cond_1

    .line 203
    iget-object v4, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v5, "android:visibility:visibility"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 204
    iget-object v2, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v3, "android:visibility:visibility"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Landroidx/l/aj$b;->d:I

    .line 205
    iget-object v2, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v3, "android:visibility:parent"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v0, Landroidx/l/aj$b;->f:Landroid/view/ViewGroup;

    goto :goto_1

    .line 207
    :cond_1
    iput v3, v0, Landroidx/l/aj$b;->d:I

    .line 208
    iput-object v2, v0, Landroidx/l/aj$b;->f:Landroid/view/ViewGroup;

    :goto_1
    const/4 v2, 0x1

    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    .line 211
    iget p1, v0, Landroidx/l/aj$b;->c:I

    iget p2, v0, Landroidx/l/aj$b;->d:I

    if-ne p1, p2, :cond_2

    iget-object p1, v0, Landroidx/l/aj$b;->e:Landroid/view/ViewGroup;

    iget-object p2, v0, Landroidx/l/aj$b;->f:Landroid/view/ViewGroup;

    if-ne p1, p2, :cond_2

    return-object v0

    .line 215
    :cond_2
    iget p1, v0, Landroidx/l/aj$b;->c:I

    iget p2, v0, Landroidx/l/aj$b;->d:I

    if-eq p1, p2, :cond_4

    .line 216
    iget p1, v0, Landroidx/l/aj$b;->c:I

    if-nez p1, :cond_3

    .line 217
    iput-boolean v1, v0, Landroidx/l/aj$b;->b:Z

    .line 218
    iput-boolean v2, v0, Landroidx/l/aj$b;->a:Z

    goto :goto_2

    .line 219
    :cond_3
    iget p1, v0, Landroidx/l/aj$b;->d:I

    if-nez p1, :cond_8

    .line 220
    iput-boolean v2, v0, Landroidx/l/aj$b;->b:Z

    .line 221
    iput-boolean v2, v0, Landroidx/l/aj$b;->a:Z

    goto :goto_2

    .line 225
    :cond_4
    iget-object p1, v0, Landroidx/l/aj$b;->f:Landroid/view/ViewGroup;

    if-nez p1, :cond_5

    .line 226
    iput-boolean v1, v0, Landroidx/l/aj$b;->b:Z

    .line 227
    iput-boolean v2, v0, Landroidx/l/aj$b;->a:Z

    goto :goto_2

    .line 228
    :cond_5
    iget-object p1, v0, Landroidx/l/aj$b;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_8

    .line 229
    iput-boolean v2, v0, Landroidx/l/aj$b;->b:Z

    .line 230
    iput-boolean v2, v0, Landroidx/l/aj$b;->a:Z

    goto :goto_2

    :cond_6
    if-nez p1, :cond_7

    .line 234
    iget p1, v0, Landroidx/l/aj$b;->d:I

    if-nez p1, :cond_7

    .line 235
    iput-boolean v2, v0, Landroidx/l/aj$b;->b:Z

    .line 236
    iput-boolean v2, v0, Landroidx/l/aj$b;->a:Z

    goto :goto_2

    :cond_7
    if-nez p2, :cond_8

    .line 237
    iget p1, v0, Landroidx/l/aj$b;->c:I

    if-nez p1, :cond_8

    .line 238
    iput-boolean v1, v0, Landroidx/l/aj$b;->b:Z

    .line 239
    iput-boolean v2, v0, Landroidx/l/aj$b;->a:Z

    :cond_8
    :goto_2
    return-object v0
.end method

.method private a(Landroidx/l/t;)V
    .locals 3

    .line 148
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 149
    iget-object v1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v1, "android:visibility:parent"

    iget-object v2, p1, Landroidx/l/t;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    .line 151
    new-array v0, v0, [I

    .line 152
    iget-object v1, p1, Landroidx/l/t;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 153
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v1, "android:visibility:screenLocation"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public a(Landroid/view/ViewGroup;Landroidx/l/t;ILandroidx/l/t;I)Landroid/animation/Animator;
    .locals 2

    .line 281
    iget p3, p0, Landroidx/l/aj;->b:I

    const/4 p5, 0x1

    and-int/2addr p3, p5

    const/4 v0, 0x0

    if-ne p3, p5, :cond_2

    if-nez p4, :cond_0

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 285
    iget-object p3, p4, Landroidx/l/t;->b:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    const/4 p5, 0x0

    .line 286
    invoke-virtual {p0, p3, p5}, Landroidx/l/aj;->getMatchedTransitionValues(Landroid/view/View;Z)Landroidx/l/t;

    move-result-object v1

    .line 288
    invoke-virtual {p0, p3, p5}, Landroidx/l/aj;->getTransitionValues(Landroid/view/View;Z)Landroidx/l/t;

    move-result-object p3

    .line 290
    invoke-direct {p0, v1, p3}, Landroidx/l/aj;->a(Landroidx/l/t;Landroidx/l/t;)Landroidx/l/aj$b;

    move-result-object p3

    .line 291
    iget-boolean p3, p3, Landroidx/l/aj$b;->a:Z

    if-eqz p3, :cond_1

    return-object v0

    .line 295
    :cond_1
    iget-object p3, p4, Landroidx/l/t;->b:Landroid/view/View;

    invoke-virtual {p0, p1, p3, p2, p4}, Landroidx/l/aj;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    return-object v0
.end method

.method public a(I)V
    .locals 1

    and-int/lit8 v0, p1, -0x4

    if-nez v0, :cond_0

    .line 127
    iput p1, p0, Landroidx/l/aj;->b:I

    return-void

    .line 125
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Only MODE_IN and MODE_OUT flags are allowed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(Landroid/view/ViewGroup;Landroid/view/View;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public b(Landroid/view/ViewGroup;Landroidx/l/t;ILandroidx/l/t;I)Landroid/animation/Animator;
    .locals 6

    .line 338
    iget p3, p0, Landroidx/l/aj;->b:I

    const/4 v0, 0x2

    and-int/2addr p3, v0

    const/4 v1, 0x0

    if-eq p3, v0, :cond_0

    return-object v1

    :cond_0
    if-eqz p2, :cond_1

    .line 342
    iget-object p3, p2, Landroidx/l/t;->b:Landroid/view/View;

    goto :goto_0

    :cond_1
    move-object p3, v1

    :goto_0
    if-eqz p4, :cond_2

    .line 343
    iget-object v2, p4, Landroidx/l/t;->b:Landroid/view/View;

    goto :goto_1

    :cond_2
    move-object v2, v1

    :goto_1
    const/4 v3, 0x1

    if-eqz v2, :cond_7

    .line 346
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-nez v4, :cond_3

    goto :goto_3

    :cond_3
    const/4 v4, 0x4

    if-ne p5, v4, :cond_4

    goto :goto_2

    :cond_4
    if-ne p3, v2, :cond_5

    :goto_2
    move-object p3, v1

    goto/16 :goto_6

    .line 387
    :cond_5
    iget-boolean v2, p0, Landroidx/l/aj;->mCanRemoveViews:Z

    if-eqz v2, :cond_6

    goto :goto_5

    .line 391
    :cond_6
    invoke-virtual {p3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 390
    invoke-static {p1, p3, v2}, Landroidx/l/s;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    move-result-object p3

    goto :goto_4

    :cond_7
    :goto_3
    if-eqz v2, :cond_8

    move-object p3, v2

    :goto_4
    move-object v2, v1

    goto :goto_6

    :cond_8
    if-eqz p3, :cond_c

    .line 354
    invoke-virtual {p3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_9

    :goto_5
    goto :goto_4

    .line 357
    :cond_9
    invoke-virtual {p3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/view/View;

    if-eqz v2, :cond_c

    .line 358
    invoke-virtual {p3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 359
    invoke-virtual {p0, v2, v3}, Landroidx/l/aj;->getTransitionValues(Landroid/view/View;Z)Landroidx/l/t;

    move-result-object v4

    .line 360
    invoke-virtual {p0, v2, v3}, Landroidx/l/aj;->getMatchedTransitionValues(Landroid/view/View;Z)Landroidx/l/t;

    move-result-object v5

    .line 363
    invoke-direct {p0, v4, v5}, Landroidx/l/aj;->a(Landroidx/l/t;Landroidx/l/t;)Landroidx/l/aj$b;

    move-result-object v4

    .line 364
    iget-boolean v4, v4, Landroidx/l/aj$b;->a:Z

    if-nez v4, :cond_a

    .line 365
    invoke-static {p1, p3, v2}, Landroidx/l/s;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    move-result-object p3

    goto :goto_4

    .line 367
    :cond_a
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-nez v4, :cond_b

    .line 368
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_b

    .line 369
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_b

    iget-boolean v2, p0, Landroidx/l/aj;->mCanRemoveViews:Z

    if-eqz v2, :cond_b

    goto :goto_4

    :cond_b
    move-object p3, v1

    goto :goto_4

    :cond_c
    move-object p3, v1

    move-object v2, p3

    :goto_6
    const/4 v4, 0x0

    if-eqz p3, :cond_e

    if-eqz p2, :cond_e

    .line 399
    iget-object p5, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v1, "android:visibility:screenLocation"

    invoke-interface {p5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, [I

    .line 400
    aget v1, p5, v4

    .line 401
    aget p5, p5, v3

    .line 402
    new-array v0, v0, [I

    .line 403
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 404
    aget v2, v0, v4

    sub-int/2addr v1, v2

    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p3, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 405
    aget v0, v0, v3

    sub-int/2addr p5, v0

    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr p5, v0

    invoke-virtual {p3, p5}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 406
    invoke-static {p1}, Landroidx/l/y;->a(Landroid/view/ViewGroup;)Landroidx/l/x;

    move-result-object p5

    .line 407
    invoke-interface {p5, p3}, Landroidx/l/x;->a(Landroid/view/View;)V

    .line 408
    invoke-virtual {p0, p1, p3, p2, p4}, Landroidx/l/aj;->b(Landroid/view/ViewGroup;Landroid/view/View;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;

    move-result-object p1

    if-nez p1, :cond_d

    .line 410
    invoke-interface {p5, p3}, Landroidx/l/x;->b(Landroid/view/View;)V

    goto :goto_7

    .line 413
    :cond_d
    new-instance p2, Landroidx/l/aj$1;

    invoke-direct {p2, p0, p5, p3}, Landroidx/l/aj$1;-><init>(Landroidx/l/aj;Landroidx/l/x;Landroid/view/View;)V

    invoke-virtual {p1, p2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :goto_7
    return-object p1

    :cond_e
    if-eqz v2, :cond_10

    .line 424
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result p3

    .line 425
    invoke-static {v2, v4}, Landroidx/l/ae;->a(Landroid/view/View;I)V

    .line 426
    invoke-virtual {p0, p1, v2, p2, p4}, Landroidx/l/aj;->b(Landroid/view/ViewGroup;Landroid/view/View;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;

    move-result-object p1

    if-eqz p1, :cond_f

    .line 428
    new-instance p2, Landroidx/l/aj$a;

    invoke-direct {p2, v2, p5, v3}, Landroidx/l/aj$a;-><init>(Landroid/view/View;IZ)V

    .line 430
    invoke-virtual {p1, p2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 431
    invoke-static {p1, p2}, Landroidx/l/a;->a(Landroid/animation/Animator;Landroid/animation/AnimatorListenerAdapter;)V

    .line 432
    invoke-virtual {p0, p2}, Landroidx/l/aj;->addListener(Landroidx/l/n$d;)Landroidx/l/n;

    goto :goto_8

    .line 434
    :cond_f
    invoke-static {v2, p3}, Landroidx/l/ae;->a(Landroid/view/View;I)V

    :goto_8
    return-object p1

    :cond_10
    return-object v1
.end method

.method public captureEndValues(Landroidx/l/t;)V
    .locals 0

    .line 163
    invoke-direct {p0, p1}, Landroidx/l/aj;->a(Landroidx/l/t;)V

    return-void
.end method

.method public captureStartValues(Landroidx/l/t;)V
    .locals 0

    .line 158
    invoke-direct {p0, p1}, Landroidx/l/aj;->a(Landroidx/l/t;)V

    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 8

    .line 248
    invoke-direct {p0, p2, p3}, Landroidx/l/aj;->a(Landroidx/l/t;Landroidx/l/t;)Landroidx/l/aj$b;

    move-result-object v0

    .line 249
    iget-boolean v1, v0, Landroidx/l/aj$b;->a:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Landroidx/l/aj$b;->e:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    iget-object v1, v0, Landroidx/l/aj$b;->f:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 251
    :cond_0
    iget-boolean v1, v0, Landroidx/l/aj$b;->b:Z

    if-eqz v1, :cond_1

    .line 252
    iget v5, v0, Landroidx/l/aj$b;->c:I

    iget v7, v0, Landroidx/l/aj$b;->d:I

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-virtual/range {v2 .. v7}, Landroidx/l/aj;->a(Landroid/view/ViewGroup;Landroidx/l/t;ILandroidx/l/t;I)Landroid/animation/Animator;

    move-result-object p1

    return-object p1

    .line 255
    :cond_1
    iget v3, v0, Landroidx/l/aj$b;->c:I

    iget v5, v0, Landroidx/l/aj$b;->d:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroidx/l/aj;->b(Landroid/view/ViewGroup;Landroidx/l/t;ILandroidx/l/t;I)Landroid/animation/Animator;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .line 144
    sget-object v0, Landroidx/l/aj;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public isTransitionRequired(Landroidx/l/t;Landroidx/l/t;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    return v0

    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 467
    iget-object v1, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    .line 468
    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v3, "android:visibility:visibility"

    .line 469
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eq v1, v2, :cond_1

    return v0

    .line 474
    :cond_1
    invoke-direct {p0, p1, p2}, Landroidx/l/aj;->a(Landroidx/l/t;Landroidx/l/t;)Landroidx/l/aj$b;

    move-result-object p1

    .line 475
    iget-boolean p2, p1, Landroidx/l/aj$b;->a:Z

    if-eqz p2, :cond_3

    iget p2, p1, Landroidx/l/aj$b;->c:I

    if-eqz p2, :cond_2

    iget p1, p1, Landroidx/l/aj$b;->d:I

    if-nez p1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method
