.class Landroidx/l/aj$a;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Visibility.java"

# interfaces
.implements Landroidx/l/a$a;
.implements Landroidx/l/n$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/l/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field a:Z

.field private final b:Landroid/view/View;

.field private final c:I

.field private final d:Landroid/view/ViewGroup;

.field private final e:Z

.field private f:Z


# direct methods
.method constructor <init>(Landroid/view/View;IZ)V
    .locals 1

    .line 490
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    const/4 v0, 0x0

    .line 488
    iput-boolean v0, p0, Landroidx/l/aj$a;->a:Z

    .line 491
    iput-object p1, p0, Landroidx/l/aj$a;->b:Landroid/view/View;

    .line 492
    iput p2, p0, Landroidx/l/aj$a;->c:I

    .line 493
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Landroidx/l/aj$a;->d:Landroid/view/ViewGroup;

    .line 494
    iput-boolean p3, p0, Landroidx/l/aj$a;->e:Z

    const/4 p1, 0x1

    .line 496
    invoke-direct {p0, p1}, Landroidx/l/aj$a;->a(Z)V

    return-void
.end method

.method private a()V
    .locals 2

    .line 561
    iget-boolean v0, p0, Landroidx/l/aj$a;->a:Z

    if-nez v0, :cond_0

    .line 563
    iget-object v0, p0, Landroidx/l/aj$a;->b:Landroid/view/View;

    iget v1, p0, Landroidx/l/aj$a;->c:I

    invoke-static {v0, v1}, Landroidx/l/ae;->a(Landroid/view/View;I)V

    .line 564
    iget-object v0, p0, Landroidx/l/aj$a;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Landroidx/l/aj$a;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    :cond_0
    const/4 v0, 0x0

    .line 569
    invoke-direct {p0, v0}, Landroidx/l/aj$a;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .line 573
    iget-boolean v0, p0, Landroidx/l/aj$a;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroidx/l/aj$a;->f:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Landroidx/l/aj$a;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 574
    iput-boolean p1, p0, Landroidx/l/aj$a;->f:Z

    .line 575
    iget-object v0, p0, Landroidx/l/aj$a;->d:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Landroidx/l/y;->a(Landroid/view/ViewGroup;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroidx/l/n;)V
    .locals 0

    return-void
.end method

.method public b(Landroidx/l/n;)V
    .locals 0

    .line 542
    invoke-direct {p0}, Landroidx/l/aj$a;->a()V

    .line 543
    invoke-virtual {p1, p0}, Landroidx/l/n;->removeListener(Landroidx/l/n$d;)Landroidx/l/n;

    return-void
.end method

.method public c(Landroidx/l/n;)V
    .locals 0

    const/4 p1, 0x0

    .line 552
    invoke-direct {p0, p1}, Landroidx/l/aj$a;->a(Z)V

    return-void
.end method

.method public d(Landroidx/l/n;)V
    .locals 0

    const/4 p1, 0x1

    .line 557
    invoke-direct {p0, p1}, Landroidx/l/aj$a;->a(Z)V

    return-void
.end method

.method public e(Landroidx/l/n;)V
    .locals 0

    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    const/4 p1, 0x1

    .line 519
    iput-boolean p1, p0, Landroidx/l/aj$a;->a:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .line 532
    invoke-direct {p0}, Landroidx/l/aj$a;->a()V

    return-void
.end method

.method public onAnimationPause(Landroid/animation/Animator;)V
    .locals 1

    .line 503
    iget-boolean p1, p0, Landroidx/l/aj$a;->a:Z

    if-nez p1, :cond_0

    .line 504
    iget-object p1, p0, Landroidx/l/aj$a;->b:Landroid/view/View;

    iget v0, p0, Landroidx/l/aj$a;->c:I

    invoke-static {p1, v0}, Landroidx/l/ae;->a(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationResume(Landroid/animation/Animator;)V
    .locals 1

    .line 512
    iget-boolean p1, p0, Landroidx/l/aj$a;->a:Z

    if-nez p1, :cond_0

    .line 513
    iget-object p1, p0, Landroidx/l/aj$a;->b:Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroidx/l/ae;->a(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method
