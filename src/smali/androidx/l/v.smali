.class Landroidx/l/v;
.super Landroidx/l/ab;
.source "ViewGroupOverlayApi14.java"

# interfaces
.implements Landroidx/l/x;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroidx/l/ab;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/View;)V

    return-void
.end method

.method static a(Landroid/view/ViewGroup;)Landroidx/l/v;
    .locals 0

    .line 32
    invoke-static {p0}, Landroidx/l/ab;->d(Landroid/view/View;)Landroidx/l/ab;

    move-result-object p0

    check-cast p0, Landroidx/l/v;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 1

    .line 37
    iget-object v0, p0, Landroidx/l/v;->a:Landroidx/l/ab$a;

    invoke-virtual {v0, p1}, Landroidx/l/ab$a;->a(Landroid/view/View;)V

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .line 42
    iget-object v0, p0, Landroidx/l/v;->a:Landroidx/l/ab$a;

    invoke-virtual {v0, p1}, Landroidx/l/ab$a;->b(Landroid/view/View;)V

    return-void
.end method
