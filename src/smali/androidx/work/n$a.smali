.class public final enum Landroidx/work/n$a;
.super Ljava/lang/Enum;
.source "WorkInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/work/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Landroidx/work/n$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Landroidx/work/n$a;

.field public static final enum b:Landroidx/work/n$a;

.field public static final enum c:Landroidx/work/n$a;

.field public static final enum d:Landroidx/work/n$a;

.field public static final enum e:Landroidx/work/n$a;

.field public static final enum f:Landroidx/work/n$a;

.field private static final synthetic g:[Landroidx/work/n$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 136
    new-instance v0, Landroidx/work/n$a;

    const-string v1, "ENQUEUED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroidx/work/n$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/n$a;->a:Landroidx/work/n$a;

    .line 141
    new-instance v0, Landroidx/work/n$a;

    const-string v1, "RUNNING"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Landroidx/work/n$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/n$a;->b:Landroidx/work/n$a;

    .line 148
    new-instance v0, Landroidx/work/n$a;

    const-string v1, "SUCCEEDED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Landroidx/work/n$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/n$a;->c:Landroidx/work/n$a;

    .line 154
    new-instance v0, Landroidx/work/n$a;

    const-string v1, "FAILED"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Landroidx/work/n$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/n$a;->d:Landroidx/work/n$a;

    .line 160
    new-instance v0, Landroidx/work/n$a;

    const-string v1, "BLOCKED"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Landroidx/work/n$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/n$a;->e:Landroidx/work/n$a;

    .line 166
    new-instance v0, Landroidx/work/n$a;

    const-string v1, "CANCELLED"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Landroidx/work/n$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/n$a;->f:Landroidx/work/n$a;

    const/4 v0, 0x6

    .line 130
    new-array v0, v0, [Landroidx/work/n$a;

    sget-object v1, Landroidx/work/n$a;->a:Landroidx/work/n$a;

    aput-object v1, v0, v2

    sget-object v1, Landroidx/work/n$a;->b:Landroidx/work/n$a;

    aput-object v1, v0, v3

    sget-object v1, Landroidx/work/n$a;->c:Landroidx/work/n$a;

    aput-object v1, v0, v4

    sget-object v1, Landroidx/work/n$a;->d:Landroidx/work/n$a;

    aput-object v1, v0, v5

    sget-object v1, Landroidx/work/n$a;->e:Landroidx/work/n$a;

    aput-object v1, v0, v6

    sget-object v1, Landroidx/work/n$a;->f:Landroidx/work/n$a;

    aput-object v1, v0, v7

    sput-object v0, Landroidx/work/n$a;->g:[Landroidx/work/n$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroidx/work/n$a;
    .locals 1

    .line 130
    const-class v0, Landroidx/work/n$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Landroidx/work/n$a;

    return-object p0
.end method

.method public static values()[Landroidx/work/n$a;
    .locals 1

    .line 130
    sget-object v0, Landroidx/work/n$a;->g:[Landroidx/work/n$a;

    invoke-virtual {v0}, [Landroidx/work/n$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroidx/work/n$a;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .line 175
    sget-object v0, Landroidx/work/n$a;->c:Landroidx/work/n$a;

    if-eq p0, v0, :cond_1

    sget-object v0, Landroidx/work/n$a;->d:Landroidx/work/n$a;

    if-eq p0, v0, :cond_1

    sget-object v0, Landroidx/work/n$a;->f:Landroidx/work/n$a;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
