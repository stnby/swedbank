.class public abstract Landroidx/work/RxWorker;
.super Landroidx/work/ListenableWorker;
.source "RxWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/RxWorker$a;
    }
.end annotation


# static fields
.field static final a:Ljava/util/concurrent/Executor;


# instance fields
.field private b:Landroidx/work/RxWorker$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/work/RxWorker$a<",
            "Landroidx/work/ListenableWorker$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 55
    new-instance v0, Landroidx/work/impl/utils/h;

    invoke-direct {v0}, Landroidx/work/impl/utils/h;-><init>()V

    sput-object v0, Landroidx/work/RxWorker;->a:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2}, Landroidx/work/ListenableWorker;-><init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V

    return-void
.end method


# virtual methods
.method protected a()Lio/reactivex/v;
    .locals 1

    .line 95
    invoke-virtual {p0}, Landroidx/work/RxWorker;->k()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/j/a;->a(Ljava/util/concurrent/Executor;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()Lio/reactivex/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Landroidx/work/ListenableWorker$a;",
            ">;"
        }
    .end annotation
.end method

.method public f()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture<",
            "Landroidx/work/ListenableWorker$a;",
            ">;"
        }
    .end annotation

    .line 71
    new-instance v0, Landroidx/work/RxWorker$a;

    invoke-direct {v0}, Landroidx/work/RxWorker$a;-><init>()V

    iput-object v0, p0, Landroidx/work/RxWorker;->b:Landroidx/work/RxWorker$a;

    .line 73
    invoke-virtual {p0}, Landroidx/work/RxWorker;->a()Lio/reactivex/v;

    move-result-object v0

    .line 74
    invoke-virtual {p0}, Landroidx/work/RxWorker;->b()Lio/reactivex/w;

    move-result-object v1

    .line 75
    invoke-virtual {v1, v0}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Landroidx/work/RxWorker;->l()Landroidx/work/impl/utils/b/a;

    move-result-object v1

    invoke-interface {v1}, Landroidx/work/impl/utils/b/a;->c()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/j/a;->a(Ljava/util/concurrent/Executor;)Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v0

    iget-object v1, p0, Landroidx/work/RxWorker;->b:Landroidx/work/RxWorker$a;

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/y;)V

    .line 79
    iget-object v0, p0, Landroidx/work/RxWorker;->b:Landroidx/work/RxWorker$a;

    iget-object v0, v0, Landroidx/work/RxWorker$a;->a:Landroidx/work/impl/utils/a/c;

    return-object v0
.end method

.method public h()V
    .locals 1

    .line 122
    invoke-super {p0}, Landroidx/work/ListenableWorker;->h()V

    .line 123
    iget-object v0, p0, Landroidx/work/RxWorker;->b:Landroidx/work/RxWorker$a;

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {v0}, Landroidx/work/RxWorker$a;->a()V

    const/4 v0, 0x0

    .line 126
    iput-object v0, p0, Landroidx/work/RxWorker;->b:Landroidx/work/RxWorker$a;

    :cond_0
    return-void
.end method
