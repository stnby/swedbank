.class public final Landroidx/work/impl/b/i;
.super Ljava/lang/Object;
.source "WorkNameDao_Impl.java"

# interfaces
.implements Landroidx/work/impl/b/h;


# instance fields
.field private final a:Landroidx/j/e;

.field private final b:Landroidx/j/b;


# direct methods
.method public constructor <init>(Landroidx/j/e;)V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Landroidx/work/impl/b/i;->a:Landroidx/j/e;

    .line 22
    new-instance v0, Landroidx/work/impl/b/i$1;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/b/i$1;-><init>(Landroidx/work/impl/b/i;Landroidx/j/e;)V

    iput-object v0, p0, Landroidx/work/impl/b/i;->b:Landroidx/j/b;

    return-void
.end method


# virtual methods
.method public a(Landroidx/work/impl/b/g;)V
    .locals 1

    .line 46
    iget-object v0, p0, Landroidx/work/impl/b/i;->a:Landroidx/j/e;

    invoke-virtual {v0}, Landroidx/j/e;->f()V

    .line 48
    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/b/i;->b:Landroidx/j/b;

    invoke-virtual {v0, p1}, Landroidx/j/b;->a(Ljava/lang/Object;)V

    .line 49
    iget-object p1, p0, Landroidx/work/impl/b/i;->a:Landroidx/j/e;

    invoke-virtual {p1}, Landroidx/j/e;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    iget-object p1, p0, Landroidx/work/impl/b/i;->a:Landroidx/j/e;

    invoke-virtual {p1}, Landroidx/j/e;->g()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Landroidx/work/impl/b/i;->a:Landroidx/j/e;

    invoke-virtual {v0}, Landroidx/j/e;->g()V

    throw p1
.end method
