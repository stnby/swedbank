.class Landroidx/work/RxWorker$a;
.super Ljava/lang/Object;
.source "RxWorker.java"

# interfaces
.implements Lio/reactivex/y;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/work/RxWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/y<",
        "TT;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Landroidx/work/impl/utils/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/work/impl/utils/a/c<",
            "TT;>;"
        }
    .end annotation
.end field

.field private b:Lio/reactivex/b/c;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    invoke-static {}, Landroidx/work/impl/utils/a/c;->d()Landroidx/work/impl/utils/a/c;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/RxWorker$a;->a:Landroidx/work/impl/utils/a/c;

    .line 139
    iget-object v0, p0, Landroidx/work/RxWorker$a;->a:Landroidx/work/impl/utils/a/c;

    sget-object v1, Landroidx/work/RxWorker;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, p0, v1}, Landroidx/work/impl/utils/a/c;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .line 165
    iget-object v0, p0, Landroidx/work/RxWorker$a;->b:Lio/reactivex/b/c;

    if-eqz v0, :cond_0

    .line 167
    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    :cond_0
    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 0

    .line 144
    iput-object p1, p0, Landroidx/work/RxWorker$a;->b:Lio/reactivex/b/c;

    return-void
.end method

.method public a_(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Landroidx/work/RxWorker$a;->a:Landroidx/work/impl/utils/a/c;

    invoke-virtual {v0, p1}, Landroidx/work/impl/utils/a/c;->a(Ljava/lang/Object;)Z

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 154
    iget-object v0, p0, Landroidx/work/RxWorker$a;->a:Landroidx/work/impl/utils/a/c;

    invoke-virtual {v0, p1}, Landroidx/work/impl/utils/a/c;->a(Ljava/lang/Throwable;)Z

    return-void
.end method

.method public run()V
    .locals 1

    .line 159
    iget-object v0, p0, Landroidx/work/RxWorker$a;->a:Landroidx/work/impl/utils/a/c;

    invoke-virtual {v0}, Landroidx/work/impl/utils/a/c;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0}, Landroidx/work/RxWorker$a;->a()V

    :cond_0
    return-void
.end method
