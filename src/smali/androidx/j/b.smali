.class public abstract Landroidx/j/b;
.super Landroidx/j/i;
.source "EntityInsertionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/j/i;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroidx/j/e;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Landroidx/j/i;-><init>(Landroidx/j/e;)V

    return-void
.end method


# virtual methods
.method protected abstract a(Landroidx/k/a/f;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/k/a/f;",
            "TT;)V"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 61
    invoke-virtual {p0}, Landroidx/j/b;->c()Landroidx/k/a/f;

    move-result-object v0

    .line 63
    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroidx/j/b;->a(Landroidx/k/a/f;Ljava/lang/Object;)V

    .line 64
    invoke-interface {v0}, Landroidx/k/a/f;->b()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    invoke-virtual {p0, v0}, Landroidx/j/b;->a(Landroidx/k/a/f;)V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p0, v0}, Landroidx/j/b;->a(Landroidx/k/a/f;)V

    throw p1
.end method
