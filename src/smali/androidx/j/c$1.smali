.class Landroidx/j/c$1;
.super Ljava/lang/Object;
.source "InvalidationTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/j/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/j/c;


# direct methods
.method constructor <init>(Landroidx/j/c;)V
    .locals 0

    .line 302
    iput-object p1, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Z
    .locals 7

    .line 360
    iget-object v0, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v0, v0, Landroidx/j/c;->e:Landroidx/j/e;

    const-string v1, "SELECT * FROM room_table_modification_log WHERE version  > ? ORDER BY version ASC;"

    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v2, v2, Landroidx/j/c;->c:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroidx/j/e;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 363
    :goto_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 364
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 365
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 367
    iget-object v6, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v6, v6, Landroidx/j/c;->b:[J

    aput-wide v3, v6, v5

    .line 370
    iget-object v5, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iput-wide v3, v5, Landroidx/j/c;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x1

    goto :goto_0

    .line 373
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v3

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 305
    iget-object v0, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v0, v0, Landroidx/j/c;->e:Landroidx/j/e;

    invoke-virtual {v0}, Landroidx/j/e;->a()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    const/4 v1, 0x0

    .line 308
    :try_start_0
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 310
    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    invoke-virtual {v2}, Landroidx/j/c;->a()Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v2, :cond_0

    .line 347
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 314
    :cond_0
    :try_start_1
    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v2, v2, Landroidx/j/c;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-nez v2, :cond_1

    .line 347
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 319
    :cond_1
    :try_start_2
    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v2, v2, Landroidx/j/c;->e:Landroidx/j/e;

    invoke-virtual {v2}, Landroidx/j/e;->j()Z

    move-result v2
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v2, :cond_2

    .line 347
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 326
    :cond_2
    :try_start_3
    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v2, v2, Landroidx/j/c;->g:Landroidx/k/a/f;

    invoke-interface {v2}, Landroidx/k/a/f;->a()I

    .line 327
    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v2, v2, Landroidx/j/c;->c:[Ljava/lang/Object;

    iget-object v3, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-wide v3, v3, Landroidx/j/c;->d:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    .line 328
    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v2, v2, Landroidx/j/c;->e:Landroidx/j/e;

    iget-boolean v2, v2, Landroidx/j/e;->b:Z

    if-eqz v2, :cond_3

    .line 331
    iget-object v2, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v2, v2, Landroidx/j/c;->e:Landroidx/j/e;

    invoke-virtual {v2}, Landroidx/j/e;->b()Landroidx/k/a/c;

    move-result-object v2

    invoke-interface {v2}, Landroidx/k/a/c;->b()Landroidx/k/a/b;

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 333
    :try_start_4
    invoke-interface {v2}, Landroidx/k/a/b;->a()V

    .line 334
    invoke-direct {p0}, Landroidx/j/c$1;->a()Z

    move-result v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 335
    :try_start_5
    invoke-interface {v2}, Landroidx/k/a/b;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 337
    :try_start_6
    invoke-interface {v2}, Landroidx/k/a/b;->b()V
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v1

    move-object v2, v1

    goto :goto_1

    :catchall_0
    move-exception v1

    move v5, v3

    move-object v3, v1

    move v1, v5

    goto :goto_0

    :catchall_1
    move-exception v3

    :goto_0
    :try_start_7
    invoke-interface {v2}, Landroidx/k/a/b;->b()V

    throw v3
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catch_1
    move-exception v2

    move v3, v1

    goto :goto_1

    .line 340
    :cond_3
    :try_start_8
    invoke-direct {p0}, Landroidx/j/c$1;->a()Z

    move-result v3
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v2

    const/4 v3, 0x0

    :goto_1
    :try_start_9
    const-string v1, "ROOM"

    const-string v4, "Cannot run invalidation tracker. Is the db closed?"

    .line 344
    invoke-static {v1, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 347
    :goto_2
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    if-eqz v3, :cond_5

    .line 350
    iget-object v0, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v0, v0, Landroidx/j/c;->h:Landroidx/a/a/b/b;

    monitor-enter v0

    .line 351
    :try_start_a
    iget-object v1, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v1, v1, Landroidx/j/c;->h:Landroidx/a/a/b/b;

    invoke-virtual {v1}, Landroidx/a/a/b/b;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 352
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/j/c$c;

    iget-object v3, p0, Landroidx/j/c$1;->a:Landroidx/j/c;

    iget-object v3, v3, Landroidx/j/c;->b:[J

    invoke-virtual {v2, v3}, Landroidx/j/c$c;->a([J)V

    goto :goto_3

    .line 354
    :cond_4
    monitor-exit v0

    goto :goto_4

    :catchall_3
    move-exception v1

    monitor-exit v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v1

    :cond_5
    :goto_4
    return-void

    .line 347
    :goto_5
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method
