.class public final Landroidx/core/g/e;
.super Ljava/lang/Object;
.source "TextDirectionHeuristicsCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/core/g/e$f;,
        Landroidx/core/g/e$a;,
        Landroidx/core/g/e$b;,
        Landroidx/core/g/e$c;,
        Landroidx/core/g/e$e;,
        Landroidx/core/g/e$d;
    }
.end annotation


# static fields
.field public static final a:Landroidx/core/g/d;

.field public static final b:Landroidx/core/g/d;

.field public static final c:Landroidx/core/g/d;

.field public static final d:Landroidx/core/g/d;

.field public static final e:Landroidx/core/g/d;

.field public static final f:Landroidx/core/g/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 33
    new-instance v0, Landroidx/core/g/e$e;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroidx/core/g/e$e;-><init>(Landroidx/core/g/e$c;Z)V

    sput-object v0, Landroidx/core/g/e;->a:Landroidx/core/g/d;

    .line 39
    new-instance v0, Landroidx/core/g/e$e;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Landroidx/core/g/e$e;-><init>(Landroidx/core/g/e$c;Z)V

    sput-object v0, Landroidx/core/g/e;->b:Landroidx/core/g/d;

    .line 47
    new-instance v0, Landroidx/core/g/e$e;

    sget-object v1, Landroidx/core/g/e$b;->a:Landroidx/core/g/e$b;

    invoke-direct {v0, v1, v2}, Landroidx/core/g/e$e;-><init>(Landroidx/core/g/e$c;Z)V

    sput-object v0, Landroidx/core/g/e;->c:Landroidx/core/g/d;

    .line 55
    new-instance v0, Landroidx/core/g/e$e;

    sget-object v1, Landroidx/core/g/e$b;->a:Landroidx/core/g/e$b;

    invoke-direct {v0, v1, v3}, Landroidx/core/g/e$e;-><init>(Landroidx/core/g/e$c;Z)V

    sput-object v0, Landroidx/core/g/e;->d:Landroidx/core/g/d;

    .line 62
    new-instance v0, Landroidx/core/g/e$e;

    sget-object v1, Landroidx/core/g/e$a;->a:Landroidx/core/g/e$a;

    invoke-direct {v0, v1, v2}, Landroidx/core/g/e$e;-><init>(Landroidx/core/g/e$c;Z)V

    sput-object v0, Landroidx/core/g/e;->e:Landroidx/core/g/d;

    .line 68
    sget-object v0, Landroidx/core/g/e$f;->a:Landroidx/core/g/e$f;

    sput-object v0, Landroidx/core/g/e;->f:Landroidx/core/g/d;

    return-void
.end method

.method static a(I)I
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x2

    return p0

    :pswitch_0
    const/4 p0, 0x0

    return p0

    :pswitch_1
    const/4 p0, 0x1

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static b(I)I
    .locals 0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    const/4 p0, 0x2

    return p0

    :pswitch_0
    const/4 p0, 0x0

    return p0

    :pswitch_1
    const/4 p0, 0x1

    return p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xe
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
