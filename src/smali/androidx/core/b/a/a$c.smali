.class public Landroidx/core/b/a/a$c;
.super Ljava/lang/Object;
.source "FingerprintManagerCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/core/b/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private final a:Ljava/security/Signature;

.field private final b:Ljavax/crypto/Cipher;

.field private final c:Ljavax/crypto/Mac;


# direct methods
.method public constructor <init>(Ljava/security/Signature;)V
    .locals 0

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iput-object p1, p0, Landroidx/core/b/a/a$c;->a:Ljava/security/Signature;

    const/4 p1, 0x0

    .line 202
    iput-object p1, p0, Landroidx/core/b/a/a$c;->b:Ljavax/crypto/Cipher;

    .line 203
    iput-object p1, p0, Landroidx/core/b/a/a$c;->c:Ljavax/crypto/Mac;

    return-void
.end method

.method public constructor <init>(Ljavax/crypto/Cipher;)V
    .locals 0

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    iput-object p1, p0, Landroidx/core/b/a/a$c;->b:Ljavax/crypto/Cipher;

    const/4 p1, 0x0

    .line 209
    iput-object p1, p0, Landroidx/core/b/a/a$c;->a:Ljava/security/Signature;

    .line 210
    iput-object p1, p0, Landroidx/core/b/a/a$c;->c:Ljavax/crypto/Mac;

    return-void
.end method

.method public constructor <init>(Ljavax/crypto/Mac;)V
    .locals 0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p1, p0, Landroidx/core/b/a/a$c;->c:Ljavax/crypto/Mac;

    const/4 p1, 0x0

    .line 215
    iput-object p1, p0, Landroidx/core/b/a/a$c;->b:Ljavax/crypto/Cipher;

    .line 216
    iput-object p1, p0, Landroidx/core/b/a/a$c;->a:Ljava/security/Signature;

    return-void
.end method


# virtual methods
.method public a()Ljava/security/Signature;
    .locals 1

    .line 224
    iget-object v0, p0, Landroidx/core/b/a/a$c;->a:Ljava/security/Signature;

    return-object v0
.end method

.method public b()Ljavax/crypto/Cipher;
    .locals 1

    .line 231
    iget-object v0, p0, Landroidx/core/b/a/a$c;->b:Ljavax/crypto/Cipher;

    return-object v0
.end method

.method public c()Ljavax/crypto/Mac;
    .locals 1

    .line 238
    iget-object v0, p0, Landroidx/core/b/a/a$c;->c:Ljavax/crypto/Mac;

    return-object v0
.end method
