.class final Landroidx/core/b/a/a$1;
.super Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
.source "FingerprintManagerCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/core/b/a/a;->a(Landroidx/core/b/a/a$a;)Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/core/b/a/a$a;


# direct methods
.method constructor <init>(Landroidx/core/b/a/a$a;)V
    .locals 0

    .line 166
    iput-object p1, p0, Landroidx/core/b/a/a$1;->a:Landroidx/core/b/a/a$a;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthenticationError(ILjava/lang/CharSequence;)V
    .locals 1

    .line 169
    iget-object v0, p0, Landroidx/core/b/a/a$1;->a:Landroidx/core/b/a/a$a;

    invoke-virtual {v0, p1, p2}, Landroidx/core/b/a/a$a;->a(ILjava/lang/CharSequence;)V

    return-void
.end method

.method public onAuthenticationFailed()V
    .locals 1

    .line 185
    iget-object v0, p0, Landroidx/core/b/a/a$1;->a:Landroidx/core/b/a/a$a;

    invoke-virtual {v0}, Landroidx/core/b/a/a$a;->a()V

    return-void
.end method

.method public onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .locals 1

    .line 174
    iget-object v0, p0, Landroidx/core/b/a/a$1;->a:Landroidx/core/b/a/a$a;

    invoke-virtual {v0, p1, p2}, Landroidx/core/b/a/a$a;->b(ILjava/lang/CharSequence;)V

    return-void
.end method

.method public onAuthenticationSucceeded(Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .locals 2

    .line 179
    iget-object v0, p0, Landroidx/core/b/a/a$1;->a:Landroidx/core/b/a/a$a;

    new-instance v1, Landroidx/core/b/a/a$b;

    .line 180
    invoke-virtual {p1}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;->getCryptoObject()Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    move-result-object p1

    invoke-static {p1}, Landroidx/core/b/a/a;->a(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;)Landroidx/core/b/a/a$c;

    move-result-object p1

    invoke-direct {v1, p1}, Landroidx/core/b/a/a$b;-><init>(Landroidx/core/b/a/a$c;)V

    .line 179
    invoke-virtual {v0, v1}, Landroidx/core/b/a/a$a;->a(Landroidx/core/b/a/a$b;)V

    return-void
.end method
