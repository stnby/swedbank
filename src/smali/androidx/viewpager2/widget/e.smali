.class final Landroidx/viewpager2/widget/e;
.super Landroidx/recyclerview/widget/RecyclerView$n;
.source "ScrollEventAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/viewpager2/widget/e$a;
    }
.end annotation


# static fields
.field private static final a:Landroid/view/ViewGroup$MarginLayoutParams;


# instance fields
.field private b:Landroidx/viewpager2/widget/ViewPager2$b;

.field private final c:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private d:I

.field private e:I

.field private f:Landroidx/viewpager2/widget/e$a;

.field private g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 51
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    sput-object v0, Landroidx/viewpager2/widget/e;->a:Landroid/view/ViewGroup$MarginLayoutParams;

    .line 52
    sget-object v0, Landroidx/viewpager2/widget/e;->a:Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    return-void
.end method

.method constructor <init>(Landroidx/recyclerview/widget/LinearLayoutManager;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$n;-><init>()V

    .line 84
    iput-object p1, p0, Landroidx/viewpager2/widget/e;->c:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 85
    new-instance p1, Landroidx/viewpager2/widget/e$a;

    invoke-direct {p1}, Landroidx/viewpager2/widget/e$a;-><init>()V

    iput-object p1, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    .line 86
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->e()V

    return-void
.end method

.method private a(I)V
    .locals 2

    .line 387
    iget v0, p0, Landroidx/viewpager2/widget/e;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroidx/viewpager2/widget/e;->e:I

    if-nez v0, :cond_0

    return-void

    .line 391
    :cond_0
    iget v0, p0, Landroidx/viewpager2/widget/e;->e:I

    if-ne v0, p1, :cond_1

    return-void

    .line 395
    :cond_1
    iput p1, p0, Landroidx/viewpager2/widget/e;->e:I

    .line 396
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->b:Landroidx/viewpager2/widget/ViewPager2$b;

    if-eqz v0, :cond_2

    .line 397
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->b:Landroidx/viewpager2/widget/ViewPager2$b;

    invoke-virtual {v0, p1}, Landroidx/viewpager2/widget/ViewPager2$b;->b(I)V

    :cond_2
    return-void
.end method

.method private a(IFI)V
    .locals 1

    .line 408
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->b:Landroidx/viewpager2/widget/ViewPager2$b;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->b:Landroidx/viewpager2/widget/ViewPager2$b;

    invoke-virtual {v0, p1, p2, p3}, Landroidx/viewpager2/widget/ViewPager2$b;->a(IFI)V

    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 2

    .line 261
    iput-boolean p1, p0, Landroidx/viewpager2/widget/e;->k:Z

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 262
    :goto_0
    iput p1, p0, Landroidx/viewpager2/widget/e;->d:I

    .line 263
    iget p1, p0, Landroidx/viewpager2/widget/e;->h:I

    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    .line 266
    iget p1, p0, Landroidx/viewpager2/widget/e;->h:I

    iput p1, p0, Landroidx/viewpager2/widget/e;->g:I

    .line 268
    iput v1, p0, Landroidx/viewpager2/widget/e;->h:I

    goto :goto_1

    .line 269
    :cond_1
    iget p1, p0, Landroidx/viewpager2/widget/e;->g:I

    if-ne p1, v1, :cond_2

    .line 271
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->i()I

    move-result p1

    iput p1, p0, Landroidx/viewpager2/widget/e;->g:I

    .line 273
    :cond_2
    :goto_1
    invoke-direct {p0, v0}, Landroidx/viewpager2/widget/e;->a(I)V

    return-void
.end method

.method private b(I)V
    .locals 1

    .line 402
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->b:Landroidx/viewpager2/widget/ViewPager2$b;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->b:Landroidx/viewpager2/widget/ViewPager2$b;

    invoke-virtual {v0, p1}, Landroidx/viewpager2/widget/ViewPager2$b;->a(I)V

    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    const/4 v0, 0x0

    .line 90
    iput v0, p0, Landroidx/viewpager2/widget/e;->d:I

    .line 91
    iput v0, p0, Landroidx/viewpager2/widget/e;->e:I

    .line 92
    iget-object v1, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    invoke-virtual {v1}, Landroidx/viewpager2/widget/e$a;->a()V

    const/4 v1, -0x1

    .line 93
    iput v1, p0, Landroidx/viewpager2/widget/e;->g:I

    .line 94
    iput v1, p0, Landroidx/viewpager2/widget/e;->h:I

    .line 95
    iput-boolean v0, p0, Landroidx/viewpager2/widget/e;->i:Z

    .line 96
    iput-boolean v0, p0, Landroidx/viewpager2/widget/e;->j:Z

    .line 97
    iput-boolean v0, p0, Landroidx/viewpager2/widget/e;->k:Z

    return-void
.end method

.method private f()V
    .locals 7

    .line 208
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    .line 210
    iget-object v1, p0, Landroidx/viewpager2/widget/e;->c:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->n()I

    move-result v1

    iput v1, v0, Landroidx/viewpager2/widget/e$a;->a:I

    .line 211
    iget v1, v0, Landroidx/viewpager2/widget/e$a;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 212
    invoke-virtual {v0}, Landroidx/viewpager2/widget/e$a;->a()V

    return-void

    .line 215
    :cond_0
    iget-object v1, p0, Landroidx/viewpager2/widget/e;->c:Landroidx/recyclerview/widget/LinearLayoutManager;

    iget v2, v0, Landroidx/viewpager2/widget/e$a;->a:I

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->c(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 217
    invoke-virtual {v0}, Landroidx/viewpager2/widget/e$a;->a()V

    return-void

    .line 223
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    instance-of v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v2, :cond_2

    .line 224
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    goto :goto_0

    :cond_2
    sget-object v2, Landroidx/viewpager2/widget/e;->a:Landroid/view/ViewGroup$MarginLayoutParams;

    .line 227
    :goto_0
    iget-object v3, p0, Landroidx/viewpager2/widget/e;->c:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->g()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v3, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_5

    .line 230
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v6

    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v6

    .line 231
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->g()Z

    move-result v6

    if-nez v6, :cond_4

    .line 232
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v1, v2

    goto :goto_2

    .line 234
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int v1, v3, v1

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    goto :goto_2

    .line 237
    :cond_5
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v3

    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, v6

    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v6

    .line 238
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v1, v2

    :goto_2
    neg-int v1, v1

    .line 241
    iput v1, v0, Landroidx/viewpager2/widget/e$a;->c:I

    .line 242
    iget v1, v0, Landroidx/viewpager2/widget/e$a;->c:I

    if-gez v1, :cond_7

    .line 245
    new-instance v1, Landroidx/viewpager2/widget/a;

    iget-object v2, p0, Landroidx/viewpager2/widget/e;->c:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v1, v2}, Landroidx/viewpager2/widget/a;-><init>(Landroidx/recyclerview/widget/LinearLayoutManager;)V

    invoke-virtual {v1}, Landroidx/viewpager2/widget/a;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 246
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Page(s) contain a ViewGroup with a LayoutTransition (or animateLayoutChanges=\"true\"), which interferes with the scrolling animation. Make sure to call getLayoutTransition().setAnimateParentHierarchy(false) on all ViewGroups with a LayoutTransition before an animation is started."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_6
    new-instance v1, Ljava/lang/IllegalStateException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v3, v5, [Ljava/lang/Object;

    iget v0, v0, Landroidx/viewpager2/widget/e$a;->c:I

    .line 255
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const-string v0, "Page can only be offset by a positive amount, not by %d"

    .line 254
    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    if-nez v3, :cond_8

    const/4 v1, 0x0

    goto :goto_3

    .line 257
    :cond_8
    iget v1, v0, Landroidx/viewpager2/widget/e$a;->c:I

    int-to-float v1, v1

    int-to-float v2, v3

    div-float/2addr v1, v2

    :goto_3
    iput v1, v0, Landroidx/viewpager2/widget/e$a;->b:F

    return-void
.end method

.method private g()Z
    .locals 2

    .line 323
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->c:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->v()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private h()Z
    .locals 3

    .line 363
    iget v0, p0, Landroidx/viewpager2/widget/e;->d:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Landroidx/viewpager2/widget/e;->d:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private i()I
    .locals 1

    .line 414
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->c:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->n()I

    move-result v0

    return v0
.end method


# virtual methods
.method a()I
    .locals 1

    .line 331
    iget v0, p0, Landroidx/viewpager2/widget/e;->e:I

    return v0
.end method

.method a(IZ)V
    .locals 1

    const/4 v0, 0x2

    if-eqz p2, :cond_0

    const/4 p2, 0x2

    goto :goto_0

    :cond_0
    const/4 p2, 0x3

    .line 280
    :goto_0
    iput p2, p0, Landroidx/viewpager2/widget/e;->d:I

    .line 283
    iget p2, p0, Landroidx/viewpager2/widget/e;->h:I

    if-eq p2, p1, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    .line 284
    :goto_1
    iput p1, p0, Landroidx/viewpager2/widget/e;->h:I

    .line 285
    invoke-direct {p0, v0}, Landroidx/viewpager2/widget/e;->a(I)V

    if-eqz p2, :cond_2

    .line 287
    invoke-direct {p0, p1}, Landroidx/viewpager2/widget/e;->b(I)V

    :cond_2
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;I)V
    .locals 2

    .line 107
    iget p1, p0, Landroidx/viewpager2/widget/e;->d:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    if-ne p2, v1, :cond_0

    .line 109
    invoke-direct {p0, v0}, Landroidx/viewpager2/widget/e;->a(Z)V

    return-void

    .line 115
    :cond_0
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->h()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x2

    if-ne p2, p1, :cond_2

    .line 117
    iget-boolean p2, p0, Landroidx/viewpager2/widget/e;->j:Z

    if-eqz p2, :cond_1

    .line 118
    invoke-direct {p0, p1}, Landroidx/viewpager2/widget/e;->a(I)V

    .line 120
    iput-boolean v1, p0, Landroidx/viewpager2/widget/e;->i:Z

    :cond_1
    return-void

    .line 126
    :cond_2
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->h()Z

    move-result p1

    if-eqz p1, :cond_6

    if-nez p2, :cond_6

    .line 128
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->f()V

    .line 129
    iget-boolean p1, p0, Landroidx/viewpager2/widget/e;->j:Z

    if-nez p1, :cond_3

    .line 134
    iget-object p1, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p1, p1, Landroidx/viewpager2/widget/e$a;->a:I

    const/4 p2, -0x1

    if-eq p1, p2, :cond_5

    .line 135
    iget-object p1, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p1, p1, Landroidx/viewpager2/widget/e$a;->a:I

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroidx/viewpager2/widget/e;->a(IFI)V

    goto :goto_0

    .line 138
    :cond_3
    iget-object p1, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p1, p1, Landroidx/viewpager2/widget/e$a;->c:I

    if-nez p1, :cond_4

    .line 146
    iget p1, p0, Landroidx/viewpager2/widget/e;->g:I

    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->a:I

    if-eq p1, p2, :cond_5

    .line 147
    iget-object p1, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p1, p1, Landroidx/viewpager2/widget/e$a;->a:I

    invoke-direct {p0, p1}, Landroidx/viewpager2/widget/e;->b(I)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :cond_5
    :goto_0
    if-eqz v1, :cond_6

    .line 153
    invoke-direct {p0, v0}, Landroidx/viewpager2/widget/e;->a(I)V

    .line 154
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->e()V

    :cond_6
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 2

    const/4 p1, 0x1

    .line 165
    iput-boolean p1, p0, Landroidx/viewpager2/widget/e;->j:Z

    .line 166
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->f()V

    .line 168
    iget-boolean v0, p0, Landroidx/viewpager2/widget/e;->i:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 170
    iput-boolean v1, p0, Landroidx/viewpager2/widget/e;->i:Z

    if-gtz p3, :cond_2

    if-nez p3, :cond_1

    if-gez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 171
    :goto_0
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->g()Z

    move-result p3

    if-ne p2, p3, :cond_1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p2, 0x1

    :goto_2
    if-eqz p2, :cond_3

    .line 175
    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->c:I

    if-eqz p2, :cond_3

    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->a:I

    add-int/2addr p2, p1

    goto :goto_3

    :cond_3
    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->a:I

    :goto_3
    iput p2, p0, Landroidx/viewpager2/widget/e;->h:I

    .line 177
    iget p2, p0, Landroidx/viewpager2/widget/e;->g:I

    iget p3, p0, Landroidx/viewpager2/widget/e;->h:I

    if-eq p2, p3, :cond_5

    .line 178
    iget p2, p0, Landroidx/viewpager2/widget/e;->h:I

    invoke-direct {p0, p2}, Landroidx/viewpager2/widget/e;->b(I)V

    goto :goto_4

    .line 180
    :cond_4
    iget p2, p0, Landroidx/viewpager2/widget/e;->d:I

    if-nez p2, :cond_5

    .line 183
    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->a:I

    invoke-direct {p0, p2}, Landroidx/viewpager2/widget/e;->b(I)V

    .line 186
    :cond_5
    :goto_4
    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->a:I

    iget-object p3, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p3, p3, Landroidx/viewpager2/widget/e$a;->b:F

    iget-object v0, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget v0, v0, Landroidx/viewpager2/widget/e$a;->c:I

    invoke-direct {p0, p2, p3, v0}, Landroidx/viewpager2/widget/e;->a(IFI)V

    .line 190
    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->a:I

    iget p3, p0, Landroidx/viewpager2/widget/e;->h:I

    if-eq p2, p3, :cond_6

    iget p2, p0, Landroidx/viewpager2/widget/e;->h:I

    const/4 p3, -0x1

    if-ne p2, p3, :cond_7

    :cond_6
    iget-object p2, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget p2, p2, Landroidx/viewpager2/widget/e$a;->c:I

    if-nez p2, :cond_7

    iget p2, p0, Landroidx/viewpager2/widget/e;->e:I

    if-eq p2, p1, :cond_7

    .line 198
    invoke-direct {p0, v1}, Landroidx/viewpager2/widget/e;->a(I)V

    .line 199
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->e()V

    :cond_7
    return-void
.end method

.method a(Landroidx/viewpager2/widget/ViewPager2$b;)V
    .locals 0

    .line 327
    iput-object p1, p0, Landroidx/viewpager2/widget/e;->b:Landroidx/viewpager2/widget/ViewPager2$b;

    return-void
.end method

.method b()Z
    .locals 1

    .line 338
    iget v0, p0, Landroidx/viewpager2/widget/e;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method c()Z
    .locals 1

    .line 354
    iget-boolean v0, p0, Landroidx/viewpager2/widget/e;->k:Z

    return v0
.end method

.method d()F
    .locals 2

    .line 378
    invoke-direct {p0}, Landroidx/viewpager2/widget/e;->f()V

    .line 379
    iget-object v0, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget v0, v0, Landroidx/viewpager2/widget/e$a;->a:I

    int-to-float v0, v0

    iget-object v1, p0, Landroidx/viewpager2/widget/e;->f:Landroidx/viewpager2/widget/e$a;

    iget v1, v1, Landroidx/viewpager2/widget/e$a;->b:F

    add-float/2addr v0, v1

    return v0
.end method
