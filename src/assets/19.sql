ALTER TABLE customer_data RENAME TO customer_data_
CREATE TABLE IF NOT EXISTS customer_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, customer_id TEXT UNIQUE, name TEXT DEFAULT '', mobile_agreement_id TEXT DEFAULT NULL, business INTEGER DEFAULT 0, logged_in INTEGER DEFAULT 0, selected INTEGER DEFAULT 0)
INSERT INTO customer_data (local_id,customer_id,name,mobile_agreement_id,logged_in,selected) SELECT local_id,customer_id,name,mobile_agreement_id,logged_in,selected FROM customer_data_
DROP TABLE IF EXISTS customer_data_
