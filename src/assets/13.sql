ALTER TABLE card_data ADD COLUMN internet_shopping_enabled INTEGER DEFAULT 0
DROP TABLE IF EXISTS active_session
CREATE TABLE IF NOT EXISTS active_session (local_id INTEGER PRIMARY KEY AUTOINCREMENT, access_token TEXT DEFAULT '', refresh_token TEXT DEFAULT '', id_token TEXT DEFAULT NULL, expires_in INTEGER DEFAULT 0, created INTEGER DEFAULT 0)