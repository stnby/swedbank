ALTER TABLE customer_data RENAME TO customer_data_
CREATE TABLE IF NOT EXISTS customer_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, customer_id TEXT UNIQUE, name TEXT DEFAULT '', mobile_agreement_id TEXT DEFAULT NULL, type INTEGER DEFAULT 0, legal_code TEXT DEFAULT NULL, swedbank_employee INTEGER DEFAULT 0, logged_in INTEGER DEFAULT 0, selected INTEGER DEFAULT 0)
INSERT INTO customer_data (local_id,customer_id,name,mobile_agreement_id,swedbank_employee,logged_in,selected) SELECT local_id,customer_id,name,mobile_agreement_id,swedbank_employee,logged_in,selected FROM customer_data_
UPDATE customer_data SET type = 1 WHERE customer_id IN (SELECT customer_id FROM customer_data_ WHERE self_employed = 1)
UPDATE customer_data SET type = 2 WHERE customer_id IN (SELECT customer_id FROM customer_data_ WHERE business = 1)
DROP TABLE IF EXISTS customer_data_
