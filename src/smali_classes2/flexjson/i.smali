.class public Lflexjson/i;
.super Ljava/lang/Object;
.source "JSONContext.java"


# static fields
.field private static a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lflexjson/i;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lflexjson/p;

.field private d:Z

.field private e:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lflexjson/v;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Lflexjson/c/s;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lflexjson/q;",
            "Lflexjson/c/q;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lflexjson/r;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lflexjson/s;

.field private k:Lflexjson/e;

.field private l:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lflexjson/q;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lflexjson/i$1;

    invoke-direct {v0}, Lflexjson/i$1;-><init>()V

    sput-object v0, Lflexjson/i;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 32
    iput-boolean v0, p0, Lflexjson/i;->d:Z

    .line 33
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lflexjson/i;->e:Ljava/util/Stack;

    .line 35
    iput v0, p0, Lflexjson/i;->f:I

    .line 40
    sget-object v0, Lflexjson/s;->b:Lflexjson/s;

    iput-object v0, p0, Lflexjson/i;->j:Lflexjson/s;

    .line 42
    new-instance v0, Lflexjson/e;

    sget-object v1, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    invoke-direct {v0, v1}, Lflexjson/e;-><init>(Ljava/util/Set;)V

    iput-object v0, p0, Lflexjson/i;->k:Lflexjson/e;

    .line 43
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lflexjson/i;->l:Ljava/util/LinkedList;

    .line 45
    new-instance v0, Lflexjson/q;

    invoke-direct {v0}, Lflexjson/q;-><init>()V

    iput-object v0, p0, Lflexjson/i;->m:Lflexjson/q;

    return-void
.end method

.method private a(C)V
    .locals 5

    .line 352
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, "\\u"

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    const v2, 0xf000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0xc

    .line 356
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    sget-object v4, Lflexjson/k;->a:[C

    aget-char v2, v4, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    shl-int/2addr p1, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Ljava/lang/Object;)Lflexjson/c/q;
    .locals 1

    .line 107
    iget-object v0, p0, Lflexjson/i;->g:Lflexjson/c/s;

    invoke-virtual {v0, p1}, Lflexjson/c/s;->a(Ljava/lang/Object;)Lflexjson/c/q;

    move-result-object p1

    return-object p1
.end method

.method public static j()Lflexjson/i;
    .locals 1

    .line 369
    sget-object v0, Lflexjson/i;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflexjson/i;

    return-object v0
.end method

.method public static k()V
    .locals 1

    .line 376
    sget-object v0, Lflexjson/i;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    return-void
.end method

.method private p()Lflexjson/c/q;
    .locals 2

    .line 103
    iget-object v0, p0, Lflexjson/i;->h:Ljava/util/Map;

    iget-object v1, p0, Lflexjson/i;->m:Lflexjson/q;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflexjson/c/q;

    return-object v0
.end method

.method private q()V
    .locals 1

    const/4 v0, 0x0

    .line 243
    iput-boolean v0, p0, Lflexjson/i;->n:Z

    return-void
.end method

.method private r()V
    .locals 2

    .line 247
    iget-boolean v0, p0, Lflexjson/i;->n:Z

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, ","

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 249
    iget-boolean v0, p0, Lflexjson/i;->d:Z

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, "\n"

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    :cond_0
    const/4 v0, 0x0

    .line 252
    iput-boolean v0, p0, Lflexjson/i;->n:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lflexjson/c;Ljava/lang/Object;)Lflexjson/c/q;
    .locals 1

    .line 86
    invoke-direct {p0}, Lflexjson/i;->p()Lflexjson/c/q;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 90
    invoke-virtual {p1}, Lflexjson/c;->l()Lflexjson/c/q;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    .line 94
    invoke-direct {p0, p2}, Lflexjson/i;->b(Ljava/lang/Object;)Lflexjson/c/q;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public a()V
    .locals 1

    .line 144
    iget-object v0, p0, Lflexjson/i;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    return-void
.end method

.method public a(Lflexjson/c/s;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lflexjson/i;->g:Lflexjson/c/s;

    return-void
.end method

.method public a(Lflexjson/e;)V
    .locals 0

    .line 386
    iput-object p1, p0, Lflexjson/i;->k:Lflexjson/e;

    return-void
.end method

.method public a(Lflexjson/p;)V
    .locals 0

    .line 162
    iput-object p1, p0, Lflexjson/i;->c:Lflexjson/p;

    return-void
.end method

.method public a(Lflexjson/s;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lflexjson/i;->j:Lflexjson/s;

    return-void
.end method

.method public a(Lflexjson/v;)V
    .locals 1

    .line 140
    iget-object v0, p0, Lflexjson/i;->e:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    .line 66
    invoke-direct {p0}, Lflexjson/i;->p()Lflexjson/c/q;

    move-result-object v0

    if-nez v0, :cond_0

    .line 69
    invoke-direct {p0, p1}, Lflexjson/i;->b(Ljava/lang/Object;)Lflexjson/c/q;

    move-result-object v0

    .line 72
    :cond_0
    invoke-interface {v0, p1}, Lflexjson/c/q;->transform(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 181
    invoke-direct {p0}, Lflexjson/i;->r()V

    .line 183
    invoke-virtual {p0}, Lflexjson/i;->b()Lflexjson/v;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0}, Lflexjson/v;->a()Lflexjson/a;

    move-result-object v0

    sget-object v1, Lflexjson/a;->b:Lflexjson/a;

    if-ne v0, v1, :cond_0

    .line 186
    invoke-virtual {p0}, Lflexjson/i;->i()V

    .line 188
    :cond_0
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    invoke-interface {v0, p1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lflexjson/r;",
            ">;)V"
        }
    .end annotation

    .line 406
    iput-object p1, p0, Lflexjson/i;->i:Ljava/util/List;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lflexjson/q;",
            "Lflexjson/c/q;",
            ">;)V"
        }
    .end annotation

    .line 125
    iput-object p1, p0, Lflexjson/i;->h:Ljava/util/Map;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 136
    iput-boolean p1, p0, Lflexjson/i;->d:Z

    return-void
.end method

.method public a(Lflexjson/c;)Z
    .locals 4

    .line 410
    iget-object v0, p0, Lflexjson/i;->i:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflexjson/i;->b(Ljava/util/List;)Lflexjson/r;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {v0}, Lflexjson/r;->b()Z

    move-result p1

    return p1

    .line 415
    :cond_0
    invoke-virtual {p1}, Lflexjson/c;->g()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 417
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    .line 420
    :cond_1
    invoke-virtual {p1}, Lflexjson/c;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    return v1

    .line 422
    :cond_2
    iget-object v0, p0, Lflexjson/i;->j:Lflexjson/s;

    sget-object v2, Lflexjson/s;->b:Lflexjson/s;

    const/4 v3, 0x1

    if-ne v0, v2, :cond_4

    .line 423
    invoke-virtual {p1}, Lflexjson/c;->d()Ljava/lang/Class;

    move-result-object p1

    .line 424
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_3

    const-class v0, Ljava/lang/Iterable;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_3

    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-nez p1, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1

    :cond_4
    return v3
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .line 432
    iget-object p1, p0, Lflexjson/i;->i:Ljava/util/List;

    invoke-virtual {p0, p1}, Lflexjson/i;->b(Ljava/util/List;)Lflexjson/r;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 434
    invoke-virtual {p1}, Lflexjson/r;->b()Z

    move-result p1

    return p1

    .line 437
    :cond_0
    sget-object p1, Lflexjson/i;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {p1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflexjson/i;

    invoke-virtual {p1}, Lflexjson/i;->n()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    if-eqz p2, :cond_4

    .line 447
    iget-object v1, p0, Lflexjson/i;->j:Lflexjson/s;

    sget-object v2, Lflexjson/s;->b:Lflexjson/s;

    if-ne v1, v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v1, p0, Lflexjson/i;->m:Lflexjson/q;

    invoke-virtual {v1}, Lflexjson/q;->c()I

    move-result v1

    if-gt v1, v0, :cond_2

    :cond_1
    iget-object v1, p0, Lflexjson/i;->j:Lflexjson/s;

    sget-object v2, Lflexjson/s;->b:Lflexjson/s;

    if-ne v1, v2, :cond_4

    if-nez p1, :cond_4

    .line 451
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    .line 452
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result p2

    if-nez p2, :cond_3

    const-class p2, Ljava/lang/Iterable;

    invoke-virtual {p2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_4
    return v0
.end method

.method protected b(Ljava/util/List;)Lflexjson/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lflexjson/r;",
            ">;)",
            "Lflexjson/r;"
        }
    .end annotation

    .line 460
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflexjson/r;

    .line 461
    iget-object v1, p0, Lflexjson/i;->m:Lflexjson/q;

    invoke-virtual {v0, v1}, Lflexjson/r;->a(Lflexjson/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public b()Lflexjson/v;
    .locals 1

    .line 148
    iget-object v0, p0, Lflexjson/i;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lflexjson/i;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflexjson/v;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 227
    invoke-direct {p0}, Lflexjson/i;->r()V

    .line 229
    iget-boolean v0, p0, Lflexjson/i;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflexjson/i;->i()V

    :cond_0
    if-eqz p1, :cond_1

    .line 231
    invoke-virtual {p0, p1}, Lflexjson/i;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string p1, "null"

    .line 233
    invoke-virtual {p0, p1}, Lflexjson/i;->a(Ljava/lang/String;)V

    .line 234
    :goto_0
    iget-object p1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v0, ":"

    invoke-interface {p1, v0}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 235
    iget-boolean p1, p0, Lflexjson/i;->d:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v0, " "

    invoke-interface {p1, v0}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    :cond_2
    return-void
.end method

.method public c()Lflexjson/p;
    .locals 1

    .line 171
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 5

    .line 303
    invoke-direct {p0}, Lflexjson/i;->r()V

    .line 305
    iget-boolean v0, p0, Lflexjson/i;->d:Z

    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {p0}, Lflexjson/i;->b()Lflexjson/v;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    invoke-virtual {v0}, Lflexjson/v;->a()Lflexjson/a;

    move-result-object v0

    sget-object v1, Lflexjson/a;->b:Lflexjson/a;

    if-ne v0, v1, :cond_0

    .line 309
    invoke-virtual {p0}, Lflexjson/i;->i()V

    .line 313
    :cond_0
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, "\""

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 315
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_d

    .line 317
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x22

    if-ne v3, v4, :cond_1

    .line 319
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\u0022"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    :cond_1
    const/16 v4, 0x26

    if-ne v3, v4, :cond_2

    .line 321
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\u0026"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    :cond_2
    const/16 v4, 0x27

    if-ne v3, v4, :cond_3

    .line 323
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\u0027"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    :cond_3
    const/16 v4, 0x3c

    if-ne v3, v4, :cond_4

    .line 325
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\u003c"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    :cond_4
    const/16 v4, 0x3e

    if-ne v3, v4, :cond_5

    .line 327
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\u003e"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_5
    const/16 v4, 0x5c

    if-ne v3, v4, :cond_6

    .line 329
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\\\"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_6
    const/16 v4, 0x8

    if-ne v3, v4, :cond_7

    .line 331
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\b"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_7
    const/16 v4, 0xc

    if-ne v3, v4, :cond_8

    .line 333
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\f"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_8
    const/16 v4, 0xa

    if-ne v3, v4, :cond_9

    .line 335
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\n"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_9
    const/16 v4, 0xd

    if-ne v3, v4, :cond_a

    .line 337
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\r"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_a
    const/16 v4, 0x9

    if-ne v3, v4, :cond_b

    .line 339
    iget-object v3, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v4, "\\t"

    invoke-interface {v3, p1, v2, v1, v4}, Lflexjson/p;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 340
    :cond_b
    invoke-static {v3}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 341
    iget-object v4, p0, Lflexjson/i;->c:Lflexjson/p;

    invoke-interface {v4, p1, v2, v1}, Lflexjson/p;->a(Ljava/lang/String;II)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 342
    invoke-direct {p0, v3}, Lflexjson/i;->a(C)V

    :cond_c
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 345
    :cond_d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 346
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v0, p1, v2, v1}, Lflexjson/p;->a(Ljava/lang/String;II)I

    .line 348
    :cond_e
    iget-object p1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v0, "\""

    invoke-interface {p1, v0}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    return-void
.end method

.method public d()Lflexjson/v;
    .locals 3

    .line 193
    invoke-direct {p0}, Lflexjson/i;->r()V

    .line 195
    iget-boolean v0, p0, Lflexjson/i;->d:Z

    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {p0}, Lflexjson/i;->b()Lflexjson/v;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {v0}, Lflexjson/v;->a()Lflexjson/a;

    move-result-object v0

    sget-object v1, Lflexjson/a;->b:Lflexjson/a;

    if-ne v0, v1, :cond_0

    .line 199
    invoke-virtual {p0}, Lflexjson/i;->i()V

    .line 202
    :cond_0
    new-instance v0, Lflexjson/v;

    sget-object v1, Lflexjson/a;->a:Lflexjson/a;

    invoke-direct {v0, v1}, Lflexjson/v;-><init>(Lflexjson/a;)V

    .line 203
    invoke-virtual {p0, v0}, Lflexjson/i;->a(Lflexjson/v;)V

    .line 204
    iget-object v1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v2, "{"

    invoke-interface {v1, v2}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 205
    iget-boolean v1, p0, Lflexjson/i;->d:Z

    if-eqz v1, :cond_1

    .line 206
    iget v1, p0, Lflexjson/i;->f:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lflexjson/i;->f:I

    .line 207
    iget-object v1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v2, "\n"

    invoke-interface {v1, v2}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    :cond_1
    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .line 398
    iput-object p1, p0, Lflexjson/i;->b:Ljava/lang/String;

    return-void
.end method

.method public e()V
    .locals 2

    .line 214
    invoke-direct {p0}, Lflexjson/i;->q()V

    .line 216
    iget-boolean v0, p0, Lflexjson/i;->d:Z

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, "\n"

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 218
    iget v0, p0, Lflexjson/i;->f:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lflexjson/i;->f:I

    .line 219
    invoke-virtual {p0}, Lflexjson/i;->i()V

    .line 221
    :cond_0
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, "}"

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 222
    invoke-virtual {p0}, Lflexjson/i;->a()V

    return-void
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x1

    .line 239
    iput-boolean v0, p0, Lflexjson/i;->n:Z

    return-void
.end method

.method public g()Lflexjson/v;
    .locals 3

    .line 258
    invoke-direct {p0}, Lflexjson/i;->r()V

    .line 260
    iget-boolean v0, p0, Lflexjson/i;->d:Z

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lflexjson/i;->b()Lflexjson/v;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v0}, Lflexjson/v;->a()Lflexjson/a;

    move-result-object v0

    sget-object v1, Lflexjson/a;->b:Lflexjson/a;

    if-ne v0, v1, :cond_0

    .line 264
    invoke-virtual {p0}, Lflexjson/i;->i()V

    .line 267
    :cond_0
    new-instance v0, Lflexjson/v;

    sget-object v1, Lflexjson/a;->b:Lflexjson/a;

    invoke-direct {v0, v1}, Lflexjson/v;-><init>(Lflexjson/a;)V

    .line 268
    invoke-virtual {p0, v0}, Lflexjson/i;->a(Lflexjson/v;)V

    .line 269
    iget-object v1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v2, "["

    invoke-interface {v1, v2}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 270
    iget-boolean v1, p0, Lflexjson/i;->d:Z

    if-eqz v1, :cond_1

    .line 271
    iget v1, p0, Lflexjson/i;->f:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lflexjson/i;->f:I

    .line 272
    iget-object v1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v2, "\n"

    invoke-interface {v1, v2}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    :cond_1
    return-object v0
.end method

.method public h()V
    .locals 2

    .line 279
    invoke-direct {p0}, Lflexjson/i;->q()V

    .line 281
    iget-boolean v0, p0, Lflexjson/i;->d:Z

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, "\n"

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 283
    iget v0, p0, Lflexjson/i;->f:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lflexjson/i;->f:I

    .line 284
    invoke-virtual {p0}, Lflexjson/i;->i()V

    .line 286
    :cond_0
    iget-object v0, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v1, "]"

    invoke-interface {v0, v1}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    .line 287
    invoke-virtual {p0}, Lflexjson/i;->a()V

    return-void
.end method

.method public i()V
    .locals 3

    const/4 v0, 0x0

    .line 291
    :goto_0
    iget v1, p0, Lflexjson/i;->f:I

    if-ge v0, v1, :cond_0

    .line 292
    iget-object v1, p0, Lflexjson/i;->c:Lflexjson/p;

    const-string v2, " "

    invoke-interface {v1, v2}, Lflexjson/p;->a(Ljava/lang/String;)Lflexjson/p;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public l()Lflexjson/e;
    .locals 1

    .line 382
    iget-object v0, p0, Lflexjson/i;->k:Lflexjson/e;

    return-object v0
.end method

.method public m()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 390
    iget-object v0, p0, Lflexjson/i;->l:Ljava/util/LinkedList;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .line 394
    iget-object v0, p0, Lflexjson/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public o()Lflexjson/q;
    .locals 1

    .line 402
    iget-object v0, p0, Lflexjson/i;->m:Lflexjson/q;

    return-object v0
.end method
