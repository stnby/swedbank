.class public Lflexjson/c/r;
.super Lflexjson/c/a;
.source "TransformerWrapper.java"


# instance fields
.field protected a:Lflexjson/c/q;

.field protected b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lflexjson/c/q;)V
    .locals 1

    .line 13
    invoke-direct {p0}, Lflexjson/c/a;-><init>()V

    .line 11
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lflexjson/c/r;->b:Ljava/lang/Boolean;

    .line 14
    iput-object p1, p0, Lflexjson/c/r;->a:Lflexjson/c/q;

    return-void
.end method


# virtual methods
.method public isInline()Ljava/lang/Boolean;
    .locals 1

    .line 31
    iget-object v0, p0, Lflexjson/c/r;->a:Lflexjson/c/q;

    instance-of v0, v0, Lflexjson/c/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflexjson/c/r;->a:Lflexjson/c/q;

    check-cast v0, Lflexjson/c/j;

    invoke-interface {v0}, Lflexjson/c/j;->isInline()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public transform(Ljava/lang/Object;)V
    .locals 1

    .line 20
    invoke-virtual {p0}, Lflexjson/c/r;->getContext()Lflexjson/i;

    move-result-object v0

    invoke-virtual {v0}, Lflexjson/i;->m()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 22
    iget-object v0, p0, Lflexjson/c/r;->a:Lflexjson/c/q;

    invoke-interface {v0, p1}, Lflexjson/c/q;->transform(Ljava/lang/Object;)V

    .line 25
    invoke-virtual {p0}, Lflexjson/c/r;->getContext()Lflexjson/i;

    move-result-object p1

    invoke-virtual {p1}, Lflexjson/i;->m()Ljava/util/LinkedList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    return-void
.end method
