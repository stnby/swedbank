.class public Lflexjson/c/s;
.super Ljava/util/concurrent/ConcurrentHashMap;
.source "TypeTransformerMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflexjson/c/s$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/ConcurrentHashMap<",
        "Ljava/lang/Class;",
        "Lflexjson/c/q;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Z

.field private b:Lflexjson/c/s;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    return-void
.end method

.method public constructor <init>(Lflexjson/c/s;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 36
    iput-object p1, p0, Lflexjson/c/s;->b:Lflexjson/c/s;

    return-void
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Class;Lflexjson/c/s$a;)Lflexjson/c/q;
    .locals 4

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 69
    :cond_0
    invoke-virtual {p0, p1}, Lflexjson/c/s;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    if-eq p1, p2, :cond_1

    .line 74
    invoke-virtual {p3, v1}, Lflexjson/c/s$a;->a(Z)V

    .line 76
    :cond_1
    invoke-virtual {p0, p1}, Lflexjson/c/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflexjson/c/q;

    return-object p1

    .line 82
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 87
    invoke-virtual {p3, v1}, Lflexjson/c/s$a;->a(Z)V

    .line 88
    const-class p1, Ljava/util/Arrays;

    invoke-virtual {p0, p1}, Lflexjson/c/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflexjson/c/q;

    return-object p1

    .line 92
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_5

    aget-object v3, v0, v1

    .line 93
    invoke-direct {p0, v3, p2, p3}, Lflexjson/c/s;->a(Ljava/lang/Class;Ljava/lang/Class;Lflexjson/c/s$a;)Lflexjson/c/q;

    move-result-object v3

    if-eqz v3, :cond_4

    return-object v3

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Lflexjson/c/s;->a(Ljava/lang/Class;Ljava/lang/Class;Lflexjson/c/s$a;)Lflexjson/c/q;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;
    .locals 1

    .line 104
    iget-boolean v0, p0, Lflexjson/c/s;->a:Z

    if-nez v0, :cond_0

    .line 105
    invoke-virtual {p0, p1, p2}, Lflexjson/c/s;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p2
.end method

.method public a(Ljava/lang/Object;)Lflexjson/c/q;
    .locals 4

    .line 42
    new-instance v0, Lflexjson/c/s$a;

    invoke-direct {v0, p0}, Lflexjson/c/s$a;-><init>(Lflexjson/c/s;)V

    if-nez p1, :cond_0

    .line 43
    sget-object v1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 45
    :goto_0
    invoke-direct {p0, v1, v1, v0}, Lflexjson/c/s;->a(Ljava/lang/Class;Ljava/lang/Class;Lflexjson/c/s$a;)Lflexjson/c/q;

    move-result-object v2

    if-nez v2, :cond_2

    .line 47
    iget-object v3, p0, Lflexjson/c/s;->b:Lflexjson/c/s;

    if-eqz v3, :cond_2

    .line 50
    iget-object v2, p0, Lflexjson/c/s;->b:Lflexjson/c/s;

    invoke-virtual {v2, p1}, Lflexjson/c/s;->a(Ljava/lang/Object;)Lflexjson/c/q;

    move-result-object v2

    if-eqz v2, :cond_2

    if-nez p1, :cond_1

    .line 52
    sget-object p1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    :goto_1
    invoke-virtual {p0, p1, v2}, Lflexjson/c/s;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 55
    :cond_2
    invoke-virtual {v0}, Lflexjson/c/s$a;->a()Z

    move-result p1

    if-nez p1, :cond_3

    .line 58
    invoke-virtual {p0, v1, v2}, Lflexjson/c/s;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    :cond_3
    return-object v2
.end method
