.class public Lflexjson/k;
.super Ljava/lang/Object;
.source "JSONSerializer.java"


# static fields
.field public static final a:[C


# instance fields
.field private b:Lflexjson/c/s;

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lflexjson/q;",
            "Lflexjson/c/q;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lflexjson/r;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "0123456789ABCDEF"

    .line 190
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lflexjson/k;->a:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflexjson/k;->c:Ljava/util/Map;

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflexjson/k;->d:Ljava/util/List;

    .line 201
    new-instance v0, Lflexjson/c/s;

    invoke-static {}, Lflexjson/u;->a()Lflexjson/c/s;

    move-result-object v1

    invoke-direct {v0, v1}, Lflexjson/c/s;-><init>(Lflexjson/c/s;)V

    iput-object v0, p0, Lflexjson/k;->b:Lflexjson/c/s;

    return-void
.end method


# virtual methods
.method public varargs a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;
    .locals 4

    .line 437
    new-instance v0, Lflexjson/c/r;

    invoke-direct {v0, p1}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    .line 439
    array-length p1, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    aget-object v2, p2, v1

    .line 440
    iget-object v3, p0, Lflexjson/k;->b:Lflexjson/c/s;

    invoke-virtual {v3, v2, v0}, Lflexjson/c/s;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lflexjson/k;
    .locals 0

    .line 230
    iput-object p1, p0, Lflexjson/k;->f:Ljava/lang/String;

    return-object p0
.end method

.method public varargs a([Ljava/lang/String;)Lflexjson/k;
    .locals 3

    .line 483
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    .line 484
    invoke-virtual {p0, v2}, Lflexjson/k;->b(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .line 243
    sget-object v0, Lflexjson/s;->b:Lflexjson/s;

    new-instance v1, Lflexjson/t;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {v1, v2}, Lflexjson/t;-><init>(Ljava/lang/StringBuilder;)V

    invoke-virtual {p0, p1, v0, v1}, Lflexjson/k;->a(Ljava/lang/Object;Lflexjson/s;Lflexjson/p;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected a(Ljava/lang/Object;Lflexjson/s;Lflexjson/p;)Ljava/lang/String;
    .locals 2

    .line 372
    invoke-static {}, Lflexjson/i;->j()Lflexjson/i;

    move-result-object v0

    .line 373
    iget-object v1, p0, Lflexjson/k;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflexjson/i;->d(Ljava/lang/String;)V

    .line 374
    iget-boolean v1, p0, Lflexjson/k;->e:Z

    invoke-virtual {v0, v1}, Lflexjson/i;->a(Z)V

    .line 375
    invoke-virtual {v0, p3}, Lflexjson/i;->a(Lflexjson/p;)V

    .line 376
    invoke-virtual {v0, p2}, Lflexjson/i;->a(Lflexjson/s;)V

    .line 377
    iget-object p2, p0, Lflexjson/k;->b:Lflexjson/c/s;

    invoke-virtual {v0, p2}, Lflexjson/i;->a(Lflexjson/c/s;)V

    .line 378
    iget-object p2, p0, Lflexjson/k;->c:Ljava/util/Map;

    invoke-virtual {v0, p2}, Lflexjson/i;->a(Ljava/util/Map;)V

    .line 379
    iget-object p2, p0, Lflexjson/k;->d:Ljava/util/List;

    invoke-virtual {v0, p2}, Lflexjson/i;->a(Ljava/util/List;)V

    .line 383
    :try_start_0
    invoke-virtual {v0}, Lflexjson/i;->n()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 384
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    const-string v1, ""

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 387
    :cond_0
    invoke-virtual {v0}, Lflexjson/i;->d()Lflexjson/v;

    .line 388
    invoke-virtual {v0, p2}, Lflexjson/i;->b(Ljava/lang/String;)V

    .line 389
    invoke-virtual {v0, p1}, Lflexjson/i;->a(Ljava/lang/Object;)V

    .line 390
    invoke-virtual {v0}, Lflexjson/i;->e()V

    goto :goto_1

    .line 385
    :cond_1
    :goto_0
    invoke-virtual {v0, p1}, Lflexjson/i;->a(Ljava/lang/Object;)V

    .line 393
    :goto_1
    invoke-virtual {v0}, Lflexjson/i;->c()Lflexjson/p;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    invoke-static {}, Lflexjson/i;->k()V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {}, Lflexjson/i;->k()V

    throw p1
.end method

.method public varargs b([Ljava/lang/String;)Lflexjson/k;
    .locals 3

    .line 505
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    .line 506
    invoke-virtual {p0, v2}, Lflexjson/k;->c(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 4

    const/16 v0, 0x2e

    .line 449
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    .line 451
    new-instance v2, Lflexjson/r;

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lflexjson/r;-><init>(Ljava/lang/String;Z)V

    .line 452
    invoke-virtual {v2}, Lflexjson/r;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 453
    iget-object v0, p0, Lflexjson/k;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456
    :cond_0
    iget-object v0, p0, Lflexjson/k;->d:Ljava/util/List;

    new-instance v2, Lflexjson/r;

    invoke-direct {v2, p1, v1}, Lflexjson/r;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 3

    .line 460
    iget-object v0, p0, Lflexjson/k;->d:Ljava/util/List;

    new-instance v1, Lflexjson/r;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lflexjson/r;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
