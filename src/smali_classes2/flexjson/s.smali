.class public final enum Lflexjson/s;
.super Ljava/lang/Enum;
.source "SerializationType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lflexjson/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflexjson/s;

.field public static final enum b:Lflexjson/s;

.field private static final synthetic c:[Lflexjson/s;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 19
    new-instance v0, Lflexjson/s;

    const-string v1, "DEEP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lflexjson/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflexjson/s;->a:Lflexjson/s;

    new-instance v0, Lflexjson/s;

    const-string v1, "SHALLOW"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lflexjson/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflexjson/s;->b:Lflexjson/s;

    const/4 v0, 0x2

    .line 18
    new-array v0, v0, [Lflexjson/s;

    sget-object v1, Lflexjson/s;->a:Lflexjson/s;

    aput-object v1, v0, v2

    sget-object v1, Lflexjson/s;->b:Lflexjson/s;

    aput-object v1, v0, v3

    sput-object v0, Lflexjson/s;->c:[Lflexjson/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflexjson/s;
    .locals 1

    .line 18
    const-class v0, Lflexjson/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lflexjson/s;

    return-object p0
.end method

.method public static values()[Lflexjson/s;
    .locals 1

    .line 18
    sget-object v0, Lflexjson/s;->c:[Lflexjson/s;

    invoke-virtual {v0}, [Lflexjson/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflexjson/s;

    return-object v0
.end method
