.class public Lflexjson/l;
.super Ljava/lang/Object;
.source "JSONTokener.java"


# instance fields
.field private a:I

.field private b:Ljava/io/Reader;

.field private c:C

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-virtual {p1}, Ljava/io/Reader;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object p1, v0

    :goto_0
    iput-object p1, p0, Lflexjson/l;->b:Ljava/io/Reader;

    const/4 p1, 0x0

    .line 59
    iput-boolean p1, p0, Lflexjson/l;->d:Z

    .line 60
    iput p1, p0, Lflexjson/l;->a:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 70
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lflexjson/l;-><init>(Ljava/io/Reader;)V

    return-void
.end method

.method private a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_1

    .line 495
    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 498
    :cond_0
    new-instance p1, Lflexjson/JSONException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Duplicate key \""

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\""

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lflexjson/JSONException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const-string v0, ""

    .line 549
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    :cond_0
    const-string v0, "true"

    .line 552
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1

    :cond_1
    const-string v0, "false"

    .line 555
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 556
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    :cond_2
    const-string v0, "null"

    .line 558
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 p1, 0x0

    return-object p1

    .line 562
    :cond_3
    invoke-direct {p0, p1}, Lflexjson/l;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 563
    new-instance v0, Lflexjson/m;

    invoke-direct {v0, p1}, Lflexjson/m;-><init>(Ljava/lang/String;)V

    return-object v0

    :cond_4
    return-object p1
.end method

.method private c(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 570
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 571
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 572
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-nez v3, :cond_0

    const/16 v3, 0x2d

    if-eq v2, v3, :cond_0

    const/16 v3, 0x2e

    if-eq v2, v3, :cond_0

    const/16 v3, 0x2b

    if-eq v2, v3, :cond_0

    const/16 v3, 0x65

    if-eq v2, v3, :cond_0

    const/16 v3, 0x45

    if-eq v2, v3, :cond_0

    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method private f()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 442
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 444
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v1

    const/16 v2, 0x7b

    if-ne v1, v2, :cond_8

    .line 448
    :goto_0
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v1

    if-eqz v1, :cond_7

    const/16 v2, 0x7d

    if-eq v1, v2, :cond_6

    .line 455
    invoke-virtual {p0}, Lflexjson/l;->a()V

    .line 456
    invoke-virtual {p0}, Lflexjson/l;->d()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 463
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v3

    const/16 v4, 0x3d

    if-ne v3, v4, :cond_0

    .line 465
    invoke-virtual {p0}, Lflexjson/l;->b()C

    move-result v3

    const/16 v4, 0x3e

    if-eq v3, v4, :cond_1

    .line 466
    invoke-virtual {p0}, Lflexjson/l;->a()V

    goto :goto_1

    :cond_0
    const/16 v4, 0x3a

    if-ne v3, v4, :cond_5

    .line 471
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lflexjson/l;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v1, v3}, Lflexjson/l;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 477
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v1

    const/16 v3, 0x2c

    if-eq v1, v3, :cond_3

    const/16 v3, 0x3b

    if-eq v1, v3, :cond_3

    if-ne v1, v2, :cond_2

    return-object v0

    :cond_2
    const-string v0, "Expected a \',\' or \'}\'"

    .line 488
    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0

    .line 480
    :cond_3
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v1

    if-ne v1, v2, :cond_4

    return-object v0

    .line 483
    :cond_4
    invoke-virtual {p0}, Lflexjson/l;->a()V

    goto :goto_0

    :cond_5
    const-string v0, "Expected a \':\' after a key"

    .line 469
    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0

    :cond_6
    return-object v0

    :cond_7
    const-string v0, "A JSONObject text must end with \'}\'"

    .line 451
    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0

    :cond_8
    const-string v0, "A JSONObject text must begin with \'{\'"

    .line 445
    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lflexjson/JSONException;
    .locals 2

    .line 425
    new-instance v0, Lflexjson/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lflexjson/l;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lflexjson/JSONException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public a(C)Ljava/lang/String;
    .locals 5

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    :goto_0
    invoke-virtual {p0}, Lflexjson/l;->b()C

    move-result v1

    if-eqz v1, :cond_7

    const/16 v2, 0xa

    if-eq v1, v2, :cond_7

    const/16 v3, 0xd

    if-eq v1, v3, :cond_7

    const/16 v4, 0x5c

    if-eq v1, v4, :cond_1

    if-ne v1, p1, :cond_0

    .line 285
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 287
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 256
    :cond_1
    invoke-virtual {p0}, Lflexjson/l;->b()C

    move-result v1

    const/16 v4, 0x62

    if-eq v1, v4, :cond_6

    const/16 v4, 0x66

    if-eq v1, v4, :cond_5

    const/16 v4, 0x6e

    if-eq v1, v4, :cond_4

    const/16 v2, 0x72

    if-eq v1, v2, :cond_3

    const/16 v2, 0x78

    const/16 v3, 0x10

    if-eq v1, v2, :cond_2

    packed-switch v1, :pswitch_data_0

    .line 280
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x4

    .line 274
    invoke-virtual {p0, v1}, Lflexjson/l;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const/16 v1, 0x9

    .line 262
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    .line 277
    invoke-virtual {p0, v1}, Lflexjson/l;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 271
    :cond_3
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 265
    :cond_4
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_5
    const/16 v1, 0xc

    .line 268
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_6
    const/16 v1, 0x8

    .line 259
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_7
    const-string p1, "Unterminated string"

    .line 254
    invoke-virtual {p0, p1}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object p1

    throw p1

    :pswitch_data_0
    .packed-switch 0x74
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(I)Ljava/lang/String;
    .locals 5

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 190
    :cond_0
    new-array v0, p1, [C

    .line 193
    iget-boolean v1, p0, Lflexjson/l;->d:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 194
    iput-boolean v3, p0, Lflexjson/l;->d:Z

    .line 195
    iget-char v1, p0, Lflexjson/l;->c:C

    aput-char v1, v0, v3

    const/4 v3, 0x1

    :cond_1
    :goto_0
    if-ge v3, p1, :cond_2

    .line 201
    :try_start_0
    iget-object v1, p0, Lflexjson/l;->b:Ljava/io/Reader;

    sub-int v4, p1, v3

    invoke-virtual {v1, v0, v3, v4}, Ljava/io/Reader;->read([CII)I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, -0x1

    if-eq v1, v4, :cond_2

    add-int/2addr v3, v1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 205
    new-instance v0, Lflexjson/JSONException;

    invoke-direct {v0, p1}, Lflexjson/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 207
    :cond_2
    iget v1, p0, Lflexjson/l;->a:I

    add-int/2addr v1, v3

    iput v1, p0, Lflexjson/l;->a:I

    if-lt v3, p1, :cond_3

    sub-int/2addr p1, v2

    .line 213
    aget-char p1, v0, p1

    iput-char p1, p0, Lflexjson/l;->c:C

    .line 214
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/String;-><init>([C)V

    return-object p1

    :cond_3
    const-string p1, "Substring bounds error"

    .line 210
    invoke-virtual {p0, p1}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object p1

    throw p1
.end method

.method public a()V
    .locals 2

    .line 82
    iget-boolean v0, p0, Lflexjson/l;->d:Z

    if-nez v0, :cond_0

    iget v0, p0, Lflexjson/l;->a:I

    if-lez v0, :cond_0

    .line 85
    iget v0, p0, Lflexjson/l;->a:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lflexjson/l;->a:I

    .line 86
    iput-boolean v1, p0, Lflexjson/l;->d:Z

    return-void

    .line 83
    :cond_0
    new-instance v0, Lflexjson/JSONException;

    const-string v1, "Stepping back two steps is not supported"

    invoke-direct {v0, v1}, Lflexjson/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()C
    .locals 2

    .line 135
    iget-boolean v0, p0, Lflexjson/l;->d:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 136
    iput-boolean v1, p0, Lflexjson/l;->d:Z

    .line 137
    iget-char v0, p0, Lflexjson/l;->c:C

    if-eqz v0, :cond_0

    .line 138
    iget v0, p0, Lflexjson/l;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflexjson/l;->a:I

    .line 140
    :cond_0
    iget-char v0, p0, Lflexjson/l;->c:C

    return v0

    .line 144
    :cond_1
    :try_start_0
    iget-object v0, p0, Lflexjson/l;->b:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->read()I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-gtz v0, :cond_2

    .line 150
    iput-char v1, p0, Lflexjson/l;->c:C

    return v1

    .line 153
    :cond_2
    iget v1, p0, Lflexjson/l;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflexjson/l;->a:I

    int-to-char v0, v0

    .line 154
    iput-char v0, p0, Lflexjson/l;->c:C

    .line 155
    iget-char v0, p0, Lflexjson/l;->c:C

    return v0

    :catch_0
    move-exception v0

    .line 146
    new-instance v1, Lflexjson/JSONException;

    invoke-direct {v1, v0}, Lflexjson/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public c()C
    .locals 2

    .line 226
    :cond_0
    invoke-virtual {p0}, Lflexjson/l;->b()C

    move-result v0

    if-eqz v0, :cond_1

    const/16 v1, 0x20

    if-le v0, v1, :cond_0

    :cond_1
    return v0
.end method

.method public d()Ljava/lang/Object;
    .locals 3

    .line 348
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v0

    const/16 v1, 0x22

    if-eq v0, v1, :cond_4

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_2

    packed-switch v0, :pswitch_data_0

    .line 373
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    const/16 v2, 0x20

    if-lt v0, v2, :cond_0

    const-string v2, ",:]}/\\\"[{;=#"

    .line 374
    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-gez v2, :cond_0

    .line 375
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 376
    invoke-virtual {p0}, Lflexjson/l;->b()C

    move-result v0

    goto :goto_0

    .line 378
    :cond_0
    invoke-virtual {p0}, Lflexjson/l;->a()V

    .line 380
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 381
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 384
    invoke-direct {p0, v0}, Lflexjson/l;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "Missing value"

    .line 382
    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0

    .line 356
    :cond_2
    invoke-virtual {p0}, Lflexjson/l;->a()V

    .line 357
    invoke-direct {p0}, Lflexjson/l;->f()Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 360
    :cond_3
    :pswitch_0
    invoke-virtual {p0}, Lflexjson/l;->a()V

    .line 361
    invoke-virtual {p0}, Lflexjson/l;->e()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 354
    :cond_4
    :pswitch_1
    invoke-virtual {p0, v0}, Lflexjson/l;->a(C)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x27
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public e()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 504
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 506
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v1

    const/16 v2, 0x29

    const/16 v3, 0x5d

    const/16 v4, 0x5b

    if-ne v1, v4, :cond_0

    const/16 v1, 0x5d

    goto :goto_0

    :cond_0
    const/16 v4, 0x28

    if-ne v1, v4, :cond_8

    const/16 v1, 0x29

    .line 515
    :goto_0
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v4

    if-ne v4, v3, :cond_1

    return-object v0

    .line 518
    :cond_1
    invoke-virtual {p0}, Lflexjson/l;->a()V

    .line 520
    :goto_1
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v4

    const/16 v5, 0x2c

    if-ne v4, v5, :cond_2

    .line 521
    invoke-virtual {p0}, Lflexjson/l;->a()V

    const/4 v4, 0x0

    .line 522
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 524
    :cond_2
    invoke-virtual {p0}, Lflexjson/l;->a()V

    .line 525
    invoke-virtual {p0}, Lflexjson/l;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527
    :goto_2
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v4

    if-eq v4, v2, :cond_6

    if-eq v4, v5, :cond_4

    const/16 v5, 0x3b

    if-eq v4, v5, :cond_4

    if-ne v4, v3, :cond_3

    goto :goto_3

    :cond_3
    const-string v0, "Expected a \',\' or \']\'"

    .line 543
    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0

    .line 531
    :cond_4
    invoke-virtual {p0}, Lflexjson/l;->c()C

    move-result v4

    if-ne v4, v3, :cond_5

    return-object v0

    .line 534
    :cond_5
    invoke-virtual {p0}, Lflexjson/l;->a()V

    goto :goto_1

    :cond_6
    :goto_3
    if-ne v1, v4, :cond_7

    return-object v0

    .line 539
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0

    :cond_8
    const-string v0, "A JSONArray text must start with \'[\'"

    .line 513
    invoke-virtual {p0, v0}, Lflexjson/l;->a(Ljava/lang/String;)Lflexjson/JSONException;

    move-result-object v0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " at character "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lflexjson/l;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
