.class public Lflexjson/j;
.super Ljava/lang/Object;
.source "JSONDeserializer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lflexjson/o;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lflexjson/q;",
            "Lflexjson/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflexjson/j;->a:Ljava/util/Map;

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflexjson/j;->b:Ljava/util/Map;

    return-void
.end method

.method private a()Lflexjson/n;
    .locals 4

    .line 379
    new-instance v0, Lflexjson/n;

    invoke-direct {v0}, Lflexjson/n;-><init>()V

    .line 380
    iget-object v1, p0, Lflexjson/j;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 381
    iget-object v3, p0, Lflexjson/j;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflexjson/o;

    invoke-virtual {v0, v2, v3}, Lflexjson/n;->a(Ljava/lang/Class;Lflexjson/o;)Lflexjson/n;

    goto :goto_0

    .line 383
    :cond_0
    iget-object v1, p0, Lflexjson/j;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflexjson/q;

    .line 384
    iget-object v3, p0, Lflexjson/j;->b:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflexjson/o;

    invoke-virtual {v0, v2, v3}, Lflexjson/n;->a(Lflexjson/q;Lflexjson/o;)Lflexjson/n;

    goto :goto_1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public varargs a(Lflexjson/o;[Ljava/lang/String;)Lflexjson/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflexjson/o;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lflexjson/j<",
            "TT;>;"
        }
    .end annotation

    .line 372
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    .line 373
    invoke-virtual {p0, v2, p1}, Lflexjson/j;->a(Ljava/lang/String;Lflexjson/o;)Lflexjson/j;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Class;Lflexjson/o;)Lflexjson/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Lflexjson/o;",
            ")",
            "Lflexjson/j<",
            "TT;>;"
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lflexjson/j;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const-class v0, Ljava/lang/Boolean;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 356
    :cond_0
    const-class v0, Ljava/lang/Integer;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 357
    :cond_1
    const-class v0, Ljava/lang/Short;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 358
    :cond_2
    const-class v0, Ljava/lang/Long;

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 359
    :cond_3
    const-class v0, Ljava/lang/Byte;

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 360
    :cond_4
    const-class v0, Ljava/lang/Float;

    if-ne p1, v0, :cond_5

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 361
    :cond_5
    const-class v0, Ljava/lang/Double;

    if-ne p1, v0, :cond_6

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 362
    :cond_6
    const-class v0, Ljava/lang/Character;

    if-ne p1, v0, :cond_7

    iget-object p1, p0, Lflexjson/j;->a:Ljava/util/Map;

    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    :goto_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Lflexjson/f;)Lflexjson/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lflexjson/f;",
            ")",
            "Lflexjson/j<",
            "TT;>;"
        }
    .end annotation

    .line 345
    iget-object v0, p0, Lflexjson/j;->b:Ljava/util/Map;

    invoke-static {p1}, Lflexjson/q;->b(Ljava/lang/String;)Lflexjson/q;

    move-result-object p1

    new-instance v1, Lflexjson/a/h;

    invoke-direct {v1, p2}, Lflexjson/a/h;-><init>(Lflexjson/f;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public a(Ljava/lang/String;Lflexjson/o;)Lflexjson/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lflexjson/o;",
            ")",
            "Lflexjson/j<",
            "TT;>;"
        }
    .end annotation

    .line 367
    iget-object v0, p0, Lflexjson/j;->b:Ljava/util/Map;

    invoke-static {p1}, Lflexjson/q;->b(Ljava/lang/String;)Lflexjson/q;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            "Ljava/lang/Class;",
            ")TT;"
        }
    .end annotation

    .line 196
    invoke-direct {p0}, Lflexjson/j;->a()Lflexjson/n;

    move-result-object v0

    .line 197
    new-instance v1, Lflexjson/l;

    invoke-direct {v1, p1}, Lflexjson/l;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v1}, Lflexjson/l;->d()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lflexjson/n;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class;",
            ")TT;"
        }
    .end annotation

    .line 183
    invoke-direct {p0}, Lflexjson/j;->a()Lflexjson/n;

    move-result-object v0

    .line 184
    new-instance v1, Lflexjson/l;

    invoke-direct {v1, p1}, Lflexjson/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflexjson/l;->d()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lflexjson/n;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/Class;)Lflexjson/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class;",
            ")",
            "Lflexjson/j<",
            "TT;>;"
        }
    .end annotation

    .line 350
    new-instance v0, Lflexjson/b/a;

    invoke-direct {v0, p2}, Lflexjson/b/a;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, p1, v0}, Lflexjson/j;->a(Ljava/lang/String;Lflexjson/f;)Lflexjson/j;

    move-result-object p1

    return-object p1
.end method
