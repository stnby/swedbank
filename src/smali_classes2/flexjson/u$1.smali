.class final Lflexjson/u$1;
.super Lflexjson/c/s;
.source "TransformerUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflexjson/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 3

    .line 14
    invoke-direct {p0}, Lflexjson/c/s;-><init>()V

    .line 17
    new-instance v0, Lflexjson/c/m;

    invoke-direct {v0}, Lflexjson/c/m;-><init>()V

    .line 18
    sget-object v1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 20
    new-instance v0, Lflexjson/c/o;

    invoke-direct {v0}, Lflexjson/c/o;-><init>()V

    .line 21
    const-class v1, Ljava/lang/Object;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 23
    new-instance v0, Lflexjson/c/f;

    invoke-direct {v0}, Lflexjson/c/f;-><init>()V

    .line 24
    const-class v1, Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 26
    new-instance v0, Lflexjson/c/d;

    invoke-direct {v0}, Lflexjson/c/d;-><init>()V

    .line 27
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 28
    const-class v1, Ljava/lang/Boolean;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 30
    new-instance v0, Lflexjson/c/n;

    invoke-direct {v0}, Lflexjson/c/n;-><init>()V

    .line 31
    const-class v1, Ljava/lang/Number;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 33
    const-class v1, Ljava/lang/Integer;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 34
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 36
    const-class v1, Ljava/lang/Long;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 37
    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 39
    const-class v1, Ljava/lang/Double;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 40
    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 42
    const-class v1, Ljava/lang/Float;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 43
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 45
    const-class v1, Ljava/math/BigDecimal;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 46
    const-class v1, Ljava/math/BigInteger;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 48
    new-instance v0, Lflexjson/c/p;

    invoke-direct {v0}, Lflexjson/c/p;-><init>()V

    .line 49
    const-class v1, Ljava/lang/String;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 51
    new-instance v0, Lflexjson/c/e;

    invoke-direct {v0}, Lflexjson/c/e;-><init>()V

    .line 52
    const-class v1, Ljava/lang/Character;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 53
    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 55
    new-instance v0, Lflexjson/c/c;

    invoke-direct {v0}, Lflexjson/c/c;-><init>()V

    .line 56
    const-class v1, Ljava/util/Date;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 58
    new-instance v0, Lflexjson/c/g;

    invoke-direct {v0}, Lflexjson/c/g;-><init>()V

    .line 59
    const-class v1, Ljava/util/Calendar;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 61
    new-instance v0, Lflexjson/c/h;

    invoke-direct {v0}, Lflexjson/c/h;-><init>()V

    .line 62
    const-class v1, Ljava/lang/Enum;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 64
    new-instance v0, Lflexjson/c/k;

    invoke-direct {v0}, Lflexjson/c/k;-><init>()V

    .line 65
    const-class v1, Ljava/lang/Iterable;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 67
    new-instance v0, Lflexjson/c/l;

    invoke-direct {v0}, Lflexjson/c/l;-><init>()V

    .line 68
    const-class v1, Ljava/util/Map;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    .line 70
    new-instance v0, Lflexjson/c/b;

    invoke-direct {v0}, Lflexjson/c/b;-><init>()V

    .line 71
    const-class v1, Ljava/util/Arrays;

    new-instance v2, Lflexjson/c/r;

    invoke-direct {v2, v0}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v1, v2}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;

    :try_start_0
    const-string v0, "org.hibernate.proxy.HibernateProxy"

    .line 74
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 75
    new-instance v1, Lflexjson/c/r;

    new-instance v2, Lflexjson/c/i;

    invoke-direct {v2}, Lflexjson/c/i;-><init>()V

    invoke-direct {v1, v2}, Lflexjson/c/r;-><init>(Lflexjson/c/q;)V

    invoke-virtual {p0, v0, v1}, Lflexjson/u$1;->a(Ljava/lang/Class;Lflexjson/c/q;)Lflexjson/c/q;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x1

    .line 80
    iput-boolean v0, p0, Lflexjson/u$1;->a:Z

    return-void
.end method
