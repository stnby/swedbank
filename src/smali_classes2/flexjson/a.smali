.class public final enum Lflexjson/a;
.super Ljava/lang/Enum;
.source "BasicType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lflexjson/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflexjson/a;

.field public static final enum b:Lflexjson/a;

.field private static final synthetic c:[Lflexjson/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 9
    new-instance v0, Lflexjson/a;

    const-string v1, "OBJECT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lflexjson/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflexjson/a;->a:Lflexjson/a;

    new-instance v0, Lflexjson/a;

    const-string v1, "ARRAY"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lflexjson/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflexjson/a;->b:Lflexjson/a;

    const/4 v0, 0x2

    .line 8
    new-array v0, v0, [Lflexjson/a;

    sget-object v1, Lflexjson/a;->a:Lflexjson/a;

    aput-object v1, v0, v2

    sget-object v1, Lflexjson/a;->b:Lflexjson/a;

    aput-object v1, v0, v3

    sput-object v0, Lflexjson/a;->c:[Lflexjson/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflexjson/a;
    .locals 1

    .line 8
    const-class v0, Lflexjson/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lflexjson/a;

    return-object p0
.end method

.method public static values()[Lflexjson/a;
    .locals 1

    .line 8
    sget-object v0, Lflexjson/a;->c:[Lflexjson/a;

    invoke-virtual {v0}, [Lflexjson/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflexjson/a;

    return-object v0
.end method
