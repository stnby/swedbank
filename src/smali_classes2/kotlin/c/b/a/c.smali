.class public abstract Lkotlin/c/b/a/c;
.super Lkotlin/c/b/a/a;
.source "ContinuationImpl.kt"


# instance fields
.field private transient a:Lkotlin/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/c/c<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/c/e;


# virtual methods
.method public a()Lkotlin/c/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 105
    iget-object v0, p0, Lkotlin/c/b/a/c;->b:Lkotlin/c/e;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/e/b/j;->a()V

    :cond_0
    return-object v0
.end method

.method protected b()V
    .locals 3

    .line 116
    iget-object v0, p0, Lkotlin/c/b/a/c;->a:Lkotlin/c/c;

    if-eqz v0, :cond_1

    .line 117
    move-object v1, p0

    check-cast v1, Lkotlin/c/b/a/c;

    if-eq v0, v1, :cond_1

    .line 118
    invoke-virtual {p0}, Lkotlin/c/b/a/c;->a()Lkotlin/c/e;

    move-result-object v1

    sget-object v2, Lkotlin/c/d;->a:Lkotlin/c/d$b;

    check-cast v2, Lkotlin/c/e$c;

    invoke-interface {v1, v2}, Lkotlin/c/e;->get(Lkotlin/c/e$c;)Lkotlin/c/e$b;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/e/b/j;->a()V

    :cond_0
    check-cast v1, Lkotlin/c/d;

    invoke-interface {v1, v0}, Lkotlin/c/d;->b(Lkotlin/c/c;)V

    .line 120
    :cond_1
    sget-object v0, Lkotlin/c/b/a/b;->a:Lkotlin/c/b/a/b;

    check-cast v0, Lkotlin/c/c;

    iput-object v0, p0, Lkotlin/c/b/a/c;->a:Lkotlin/c/c;

    return-void
.end method

.method public final e()Lkotlin/c/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/c/c<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 111
    iget-object v0, p0, Lkotlin/c/b/a/c;->a:Lkotlin/c/c;

    if-eqz v0, :cond_0

    goto :goto_1

    .line 112
    :cond_0
    invoke-virtual {p0}, Lkotlin/c/b/a/c;->a()Lkotlin/c/e;

    move-result-object v0

    sget-object v1, Lkotlin/c/d;->a:Lkotlin/c/d$b;

    check-cast v1, Lkotlin/c/e$c;

    invoke-interface {v0, v1}, Lkotlin/c/e;->get(Lkotlin/c/e$c;)Lkotlin/c/e$b;

    move-result-object v0

    check-cast v0, Lkotlin/c/d;

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lkotlin/c/c;

    invoke-interface {v0, v1}, Lkotlin/c/d;->a(Lkotlin/c/c;)Lkotlin/c/c;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, p0

    check-cast v0, Lkotlin/c/c;

    .line 113
    :goto_0
    iput-object v0, p0, Lkotlin/c/b/a/c;->a:Lkotlin/c/c;

    :goto_1
    return-object v0
.end method
