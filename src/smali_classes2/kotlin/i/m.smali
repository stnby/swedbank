.class public final Lkotlin/i/m;
.super Ljava/lang/Object;
.source "Sequences.kt"

# interfaces
.implements Lkotlin/i/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/i/e<",
        "TR;>;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/i/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/i/e<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/i/e;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/i/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/i/e<",
            "+TT;>;",
            "Lkotlin/e/a/b<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transformer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/i/m;->a:Lkotlin/i/e;

    iput-object p2, p0, Lkotlin/i/m;->b:Lkotlin/e/a/b;

    return-void
.end method

.method public static final synthetic a(Lkotlin/i/m;)Lkotlin/e/a/b;
    .locals 0

    .line 167
    iget-object p0, p0, Lkotlin/i/m;->b:Lkotlin/e/a/b;

    return-object p0
.end method

.method public static final synthetic b(Lkotlin/i/m;)Lkotlin/i/e;
    .locals 0

    .line 167
    iget-object p0, p0, Lkotlin/i/m;->a:Lkotlin/i/e;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 169
    new-instance v0, Lkotlin/i/m$a;

    invoke-direct {v0, p0}, Lkotlin/i/m$a;-><init>(Lkotlin/i/m;)V

    check-cast v0, Ljava/util/Iterator;

    return-object v0
.end method
