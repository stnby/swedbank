.class public final Lkotlin/i/l;
.super Ljava/lang/Object;
.source "Sequences.kt"

# interfaces
.implements Lkotlin/i/a;
.implements Lkotlin/i/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/i/a<",
        "TT;>;",
        "Lkotlin/i/e<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/i/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/i/e<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Lkotlin/i/e;I)V
    .locals 1
    .param p1    # Lkotlin/i/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/i/e<",
            "+TT;>;I)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/i/l;->a:Lkotlin/i/e;

    iput p2, p0, Lkotlin/i/l;->b:I

    .line 356
    iget p1, p0, Lkotlin/i/l;->b:I

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    return-void

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "count must be non-negative, but was "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lkotlin/i/l;->b:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p2, 0x2e

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public static final synthetic a(Lkotlin/i/l;)I
    .locals 0

    .line 350
    iget p0, p0, Lkotlin/i/l;->b:I

    return p0
.end method

.method public static final synthetic b(Lkotlin/i/l;)Lkotlin/i/e;
    .locals 0

    .line 350
    iget-object p0, p0, Lkotlin/i/l;->a:Lkotlin/i/e;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 362
    new-instance v0, Lkotlin/i/l$a;

    invoke-direct {v0, p0}, Lkotlin/i/l$a;-><init>(Lkotlin/i/l;)V

    check-cast v0, Ljava/util/Iterator;

    return-object v0
.end method

.method public a(I)Lkotlin/i/e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lkotlin/i/e<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 360
    iget v0, p0, Lkotlin/i/l;->b:I

    if-lt p1, v0, :cond_0

    move-object p1, p0

    check-cast p1, Lkotlin/i/e;

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/i/l;

    iget-object v1, p0, Lkotlin/i/l;->a:Lkotlin/i/e;

    invoke-direct {v0, v1, p1}, Lkotlin/i/l;-><init>(Lkotlin/i/e;I)V

    move-object p1, v0

    check-cast p1, Lkotlin/i/e;

    :goto_0
    return-object p1
.end method
