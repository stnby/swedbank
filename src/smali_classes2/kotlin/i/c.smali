.class public final Lkotlin/i/c;
.super Ljava/lang/Object;
.source "Sequences.kt"

# interfaces
.implements Lkotlin/i/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/i/e<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/i/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/i/e<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final c:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/i/e;ZLkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/i/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/i/e<",
            "+TT;>;Z",
            "Lkotlin/e/a/b<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predicate"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/i/c;->a:Lkotlin/i/e;

    iput-boolean p2, p0, Lkotlin/i/c;->b:Z

    iput-object p3, p0, Lkotlin/i/c;->c:Lkotlin/e/a/b;

    return-void
.end method

.method public static final synthetic a(Lkotlin/i/c;)Z
    .locals 0

    .line 119
    iget-boolean p0, p0, Lkotlin/i/c;->b:Z

    return p0
.end method

.method public static final synthetic b(Lkotlin/i/c;)Lkotlin/e/a/b;
    .locals 0

    .line 119
    iget-object p0, p0, Lkotlin/i/c;->c:Lkotlin/e/a/b;

    return-object p0
.end method

.method public static final synthetic c(Lkotlin/i/c;)Lkotlin/i/e;
    .locals 0

    .line 119
    iget-object p0, p0, Lkotlin/i/c;->a:Lkotlin/i/e;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 125
    new-instance v0, Lkotlin/i/c$a;

    invoke-direct {v0, p0}, Lkotlin/i/c$a;-><init>(Lkotlin/i/c;)V

    check-cast v0, Ljava/util/Iterator;

    return-object v0
.end method
