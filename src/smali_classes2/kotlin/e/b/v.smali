.class public Lkotlin/e/b/v;
.super Ljava/lang/Object;
.source "Reflection.java"


# static fields
.field private static final a:Lkotlin/e/b/w;

.field private static final b:[Lkotlin/h/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "kotlin.reflect.jvm.internal.ReflectionFactoryImpl"

    .line 22
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/e/b/w;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-eqz v0, :cond_0

    goto :goto_1

    .line 30
    :cond_0
    new-instance v0, Lkotlin/e/b/w;

    invoke-direct {v0}, Lkotlin/e/b/w;-><init>()V

    :goto_1
    sput-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    const/4 v0, 0x0

    .line 35
    new-array v0, v0, [Lkotlin/h/b;

    sput-object v0, Lkotlin/e/b/v;->b:[Lkotlin/h/b;

    return-void
.end method

.method public static a(Lkotlin/e/b/k;)Ljava/lang/String;
    .locals 1

    .line 69
    sget-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    invoke-virtual {v0, p0}, Lkotlin/e/b/w;->a(Lkotlin/e/b/k;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Class;)Lkotlin/h/b;
    .locals 1

    .line 50
    sget-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    invoke-virtual {v0, p0}, Lkotlin/e/b/w;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;
    .locals 1

    .line 46
    sget-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    invoke-virtual {v0, p0, p1}, Lkotlin/e/b/w;->a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlin/e/b/i;)Lkotlin/h/d;
    .locals 1

    .line 80
    sget-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    invoke-virtual {v0, p0}, Lkotlin/e/b/w;->a(Lkotlin/e/b/i;)Lkotlin/h/d;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlin/e/b/m;)Lkotlin/h/f;
    .locals 1

    .line 98
    sget-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    invoke-virtual {v0, p0}, Lkotlin/e/b/w;->a(Lkotlin/e/b/m;)Lkotlin/h/f;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlin/e/b/q;)Lkotlin/h/h;
    .locals 1

    .line 86
    sget-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    invoke-virtual {v0, p0}, Lkotlin/e/b/w;->a(Lkotlin/e/b/q;)Lkotlin/h/h;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlin/e/b/s;)Lkotlin/h/i;
    .locals 1

    .line 94
    sget-object v0, Lkotlin/e/b/v;->a:Lkotlin/e/b/w;

    invoke-virtual {v0, p0}, Lkotlin/e/b/w;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object p0

    return-object p0
.end method
