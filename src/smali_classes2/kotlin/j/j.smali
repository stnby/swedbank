.class final Lkotlin/j/j;
.super Ljava/lang/Object;
.source "Regex.kt"

# interfaces
.implements Lkotlin/j/i;


# instance fields
.field private final a:Lkotlin/j/g;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/util/regex/Matcher;

.field private final c:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/util/regex/Matcher;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/util/regex/Matcher;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "matcher"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/j/j;->b:Ljava/util/regex/Matcher;

    iput-object p2, p0, Lkotlin/j/j;->c:Ljava/lang/CharSequence;

    .line 274
    new-instance p1, Lkotlin/j/j$a;

    invoke-direct {p1, p0}, Lkotlin/j/j$a;-><init>(Lkotlin/j/j;)V

    check-cast p1, Lkotlin/j/g;

    iput-object p1, p0, Lkotlin/j/j;->a:Lkotlin/j/g;

    return-void
.end method

.method public static final synthetic a(Lkotlin/j/j;)Ljava/util/regex/MatchResult;
    .locals 0

    .line 267
    invoke-direct {p0}, Lkotlin/j/j;->b()Ljava/util/regex/MatchResult;

    move-result-object p0

    return-object p0
.end method

.method private final b()Ljava/util/regex/MatchResult;
    .locals 1

    .line 268
    iget-object v0, p0, Lkotlin/j/j;->b:Ljava/util/regex/Matcher;

    check-cast v0, Ljava/util/regex/MatchResult;

    return-object v0
.end method


# virtual methods
.method public a()Lkotlin/g/d;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 270
    invoke-direct {p0}, Lkotlin/j/j;->b()Ljava/util/regex/MatchResult;

    move-result-object v0

    invoke-static {v0}, Lkotlin/j/l;->a(Ljava/util/regex/MatchResult;)Lkotlin/g/d;

    move-result-object v0

    return-object v0
.end method
