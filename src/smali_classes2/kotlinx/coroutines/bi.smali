.class public final Lkotlinx/coroutines/bi;
.super Lkotlinx/coroutines/r;
.source "Unconfined.kt"


# static fields
.field public static final b:Lkotlinx/coroutines/bi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lkotlinx/coroutines/bi;

    invoke-direct {v0}, Lkotlinx/coroutines/bi;-><init>()V

    sput-object v0, Lkotlinx/coroutines/bi;->b:Lkotlinx/coroutines/bi;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lkotlinx/coroutines/r;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/c/e;Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Lkotlin/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Runnable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "block"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public a(Lkotlin/c/e;)Z
    .locals 1
    .param p1    # Lkotlin/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "Unconfined"

    return-object v0
.end method
