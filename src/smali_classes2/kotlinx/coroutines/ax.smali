.class public final Lkotlinx/coroutines/ax;
.super Ljava/lang/Object;
.source "JobSupport.kt"


# static fields
.field private static final a:Lkotlinx/coroutines/a/p;

.field private static final b:Lkotlinx/coroutines/af;

.field private static final c:Lkotlinx/coroutines/af;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1174
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "SEALED"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/ax;->a:Lkotlinx/coroutines/a/p;

    .line 1176
    new-instance v0, Lkotlinx/coroutines/af;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlinx/coroutines/af;-><init>(Z)V

    sput-object v0, Lkotlinx/coroutines/ax;->b:Lkotlinx/coroutines/af;

    .line 1178
    new-instance v0, Lkotlinx/coroutines/af;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlinx/coroutines/af;-><init>(Z)V

    sput-object v0, Lkotlinx/coroutines/ax;->c:Lkotlinx/coroutines/af;

    return-void
.end method

.method public static final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-static {p0}, Lkotlinx/coroutines/ax;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a()Lkotlinx/coroutines/a/p;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/ax;->a:Lkotlinx/coroutines/a/p;

    return-object v0
.end method

.method private static final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 1159
    instance-of v0, p0, Lkotlinx/coroutines/al;

    if-eqz v0, :cond_0

    new-instance v0, Lkotlinx/coroutines/am;

    check-cast p0, Lkotlinx/coroutines/al;

    invoke-direct {v0, p0}, Lkotlinx/coroutines/am;-><init>(Lkotlinx/coroutines/al;)V

    move-object p0, v0

    :cond_0
    return-object p0
.end method

.method public static final synthetic b()Lkotlinx/coroutines/af;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/ax;->c:Lkotlinx/coroutines/af;

    return-object v0
.end method

.method public static final synthetic c()Lkotlinx/coroutines/af;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/ax;->b:Lkotlinx/coroutines/af;

    return-object v0
.end method
