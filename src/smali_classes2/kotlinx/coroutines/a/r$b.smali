.class final Lkotlinx/coroutines/a/r$b;
.super Lkotlin/e/b/k;
.source "ThreadContext.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlinx/coroutines/a/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lkotlinx/coroutines/be<",
        "*>;",
        "Lkotlin/c/e$b;",
        "Lkotlinx/coroutines/be<",
        "*>;>;"
    }
.end annotation


# static fields
.field public static final a:Lkotlinx/coroutines/a/r$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlinx/coroutines/a/r$b;

    invoke-direct {v0}, Lkotlinx/coroutines/a/r$b;-><init>()V

    sput-object v0, Lkotlinx/coroutines/a/r$b;->a:Lkotlinx/coroutines/a/r$b;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/be;

    check-cast p2, Lkotlin/c/e$b;

    invoke-virtual {p0, p1, p2}, Lkotlinx/coroutines/a/r$b;->a(Lkotlinx/coroutines/be;Lkotlin/c/e$b;)Lkotlinx/coroutines/be;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lkotlinx/coroutines/be;Lkotlin/c/e$b;)Lkotlinx/coroutines/be;
    .locals 1
    .param p1    # Lkotlinx/coroutines/be;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lkotlin/c/e$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/be<",
            "*>;",
            "Lkotlin/c/e$b;",
            ")",
            "Lkotlinx/coroutines/be<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "element"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    return-object p1

    .line 38
    :cond_0
    instance-of p1, p2, Lkotlinx/coroutines/be;

    if-nez p1, :cond_1

    const/4 p2, 0x0

    :cond_1
    check-cast p2, Lkotlinx/coroutines/be;

    return-object p2
.end method
