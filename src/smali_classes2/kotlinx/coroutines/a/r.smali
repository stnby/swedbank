.class public final Lkotlinx/coroutines/a/r;
.super Ljava/lang/Object;
.source "ThreadContext.kt"


# static fields
.field private static final a:Lkotlinx/coroutines/a/p;

.field private static final b:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "Ljava/lang/Object;",
            "Lkotlin/c/e$b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "Lkotlinx/coroutines/be<",
            "*>;",
            "Lkotlin/c/e$b;",
            "Lkotlinx/coroutines/be<",
            "*>;>;"
        }
    .end annotation
.end field

.field private static final d:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "Lkotlinx/coroutines/a/u;",
            "Lkotlin/c/e$b;",
            "Lkotlinx/coroutines/a/u;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "Lkotlinx/coroutines/a/u;",
            "Lkotlin/c/e$b;",
            "Lkotlinx/coroutines/a/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "ZERO"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/a/r;->a:Lkotlinx/coroutines/a/p;

    .line 26
    sget-object v0, Lkotlinx/coroutines/a/r$a;->a:Lkotlinx/coroutines/a/r$a;

    check-cast v0, Lkotlin/e/a/m;

    sput-object v0, Lkotlinx/coroutines/a/r;->b:Lkotlin/e/a/m;

    .line 36
    sget-object v0, Lkotlinx/coroutines/a/r$b;->a:Lkotlinx/coroutines/a/r$b;

    check-cast v0, Lkotlin/e/a/m;

    sput-object v0, Lkotlinx/coroutines/a/r;->c:Lkotlin/e/a/m;

    .line 43
    sget-object v0, Lkotlinx/coroutines/a/r$d;->a:Lkotlinx/coroutines/a/r$d;

    check-cast v0, Lkotlin/e/a/m;

    sput-object v0, Lkotlinx/coroutines/a/r;->d:Lkotlin/e/a/m;

    .line 52
    sget-object v0, Lkotlinx/coroutines/a/r$c;->a:Lkotlinx/coroutines/a/r$c;

    check-cast v0, Lkotlin/e/a/m;

    sput-object v0, Lkotlinx/coroutines/a/r;->e:Lkotlin/e/a/m;

    return-void
.end method

.method public static final a(Lkotlin/c/e;)Ljava/lang/Object;
    .locals 2
    .param p0    # Lkotlin/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 60
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lkotlinx/coroutines/a/r;->b:Lkotlin/e/a/m;

    invoke-interface {p0, v0, v1}, Lkotlin/c/e;->fold(Ljava/lang/Object;Lkotlin/e/a/m;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/e/b/j;->a()V

    :cond_0
    return-object p0
.end method

.method public static final a(Lkotlin/c/e;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lkotlin/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    invoke-static {p0}, Lkotlinx/coroutines/a/r;->a(Lkotlin/c/e;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    const/4 v0, 0x0

    .line 68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-ne p1, v0, :cond_1

    sget-object p0, Lkotlinx/coroutines/a/r;->a:Lkotlinx/coroutines/a/p;

    goto :goto_1

    .line 70
    :cond_1
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 72
    new-instance v0, Lkotlinx/coroutines/a/u;

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-direct {v0, p0, p1}, Lkotlinx/coroutines/a/u;-><init>(Lkotlin/c/e;I)V

    sget-object p1, Lkotlinx/coroutines/a/r;->d:Lkotlin/e/a/m;

    invoke-interface {p0, v0, p1}, Lkotlin/c/e;->fold(Ljava/lang/Object;Lkotlin/e/a/m;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    .line 77
    check-cast p1, Lkotlinx/coroutines/be;

    .line 78
    invoke-interface {p1, p0}, Lkotlinx/coroutines/be;->b(Lkotlin/c/e;)Ljava/lang/Object;

    move-result-object p0

    :goto_1
    return-object p0

    .line 77
    :cond_3
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final b(Lkotlin/c/e;Ljava/lang/Object;)V
    .locals 2
    .param p0    # Lkotlin/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    sget-object v0, Lkotlinx/coroutines/a/r;->a:Lkotlinx/coroutines/a/p;

    if-ne p1, v0, :cond_0

    return-void

    .line 86
    :cond_0
    instance-of v0, p1, Lkotlinx/coroutines/a/u;

    if-eqz v0, :cond_1

    .line 88
    move-object v0, p1

    check-cast v0, Lkotlinx/coroutines/a/u;

    invoke-virtual {v0}, Lkotlinx/coroutines/a/u;->b()V

    .line 89
    sget-object v0, Lkotlinx/coroutines/a/r;->e:Lkotlin/e/a/m;

    invoke-interface {p0, p1, v0}, Lkotlin/c/e;->fold(Ljava/lang/Object;Lkotlin/e/a/m;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 94
    sget-object v1, Lkotlinx/coroutines/a/r;->c:Lkotlin/e/a/m;

    invoke-interface {p0, v0, v1}, Lkotlin/c/e;->fold(Ljava/lang/Object;Lkotlin/e/a/m;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lkotlinx/coroutines/be;

    .line 95
    invoke-interface {v0, p0, p1}, Lkotlinx/coroutines/be;->a(Lkotlin/c/e;Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 94
    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
