.class final Lkotlinx/coroutines/a/r$c;
.super Lkotlin/e/b/k;
.source "ThreadContext.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlinx/coroutines/a/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lkotlinx/coroutines/a/u;",
        "Lkotlin/c/e$b;",
        "Lkotlinx/coroutines/a/u;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlinx/coroutines/a/r$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlinx/coroutines/a/r$c;

    invoke-direct {v0}, Lkotlinx/coroutines/a/r$c;-><init>()V

    sput-object v0, Lkotlinx/coroutines/a/r$c;->a:Lkotlinx/coroutines/a/r$c;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/a/u;

    check-cast p2, Lkotlin/c/e$b;

    invoke-virtual {p0, p1, p2}, Lkotlinx/coroutines/a/r$c;->a(Lkotlinx/coroutines/a/u;Lkotlin/c/e$b;)Lkotlinx/coroutines/a/u;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lkotlinx/coroutines/a/u;Lkotlin/c/e$b;)Lkotlinx/coroutines/a/u;
    .locals 2
    .param p1    # Lkotlinx/coroutines/a/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/c/e$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "element"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of v0, p2, Lkotlinx/coroutines/be;

    if-eqz v0, :cond_0

    .line 55
    check-cast p2, Lkotlinx/coroutines/be;

    invoke-virtual {p1}, Lkotlinx/coroutines/a/u;->c()Lkotlin/c/e;

    move-result-object v0

    invoke-virtual {p1}, Lkotlinx/coroutines/a/u;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lkotlinx/coroutines/be;->a(Lkotlin/c/e;Ljava/lang/Object;)V

    :cond_0
    return-object p1
.end method
