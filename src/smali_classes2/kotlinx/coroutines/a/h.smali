.class public final Lkotlinx/coroutines/a/h;
.super Ljava/lang/Object;
.source "LockFreeLinkedList.kt"


# static fields
.field private static final a:Ljava/lang/Object;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final b:Ljava/lang/Object;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:Ljava/lang/Object;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final d:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "CONDITION_FALSE"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/a/h;->a:Ljava/lang/Object;

    .line 25
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "ALREADY_REMOVED"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/a/h;->b:Ljava/lang/Object;

    .line 28
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "LIST_EMPTY"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/a/h;->c:Ljava/lang/Object;

    .line 30
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "REMOVE_PREPARED"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/a/h;->d:Ljava/lang/Object;

    return-void
.end method

.method public static final a()Ljava/lang/Object;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    sget-object v0, Lkotlinx/coroutines/a/h;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public static final a(Ljava/lang/Object;)Lkotlinx/coroutines/a/i;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "receiver$0"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 663
    instance-of v0, p0, Lkotlinx/coroutines/a/n;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move-object v0, p0

    :goto_0
    check-cast v0, Lkotlinx/coroutines/a/n;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lkotlinx/coroutines/a/n;->a:Lkotlinx/coroutines/a/i;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, p0

    check-cast v0, Lkotlinx/coroutines/a/i;

    :goto_1
    return-object v0
.end method
