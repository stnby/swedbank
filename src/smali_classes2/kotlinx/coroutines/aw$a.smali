.class final Lkotlinx/coroutines/aw$a;
.super Lkotlinx/coroutines/av;
.source "JobSupport.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlinx/coroutines/aw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlinx/coroutines/av<",
        "Lkotlinx/coroutines/aq;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlinx/coroutines/aw;

.field private final g:Lkotlinx/coroutines/aw$b;

.field private final h:Lkotlinx/coroutines/j;

.field private final i:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/aw;Lkotlinx/coroutines/aw$b;Lkotlinx/coroutines/j;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lkotlinx/coroutines/aw;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlinx/coroutines/aw$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlinx/coroutines/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "child"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1028
    iget-object v0, p3, Lkotlinx/coroutines/j;->a:Lkotlinx/coroutines/k;

    check-cast v0, Lkotlinx/coroutines/aq;

    invoke-direct {p0, v0}, Lkotlinx/coroutines/av;-><init>(Lkotlinx/coroutines/aq;)V

    iput-object p1, p0, Lkotlinx/coroutines/aw$a;->a:Lkotlinx/coroutines/aw;

    iput-object p2, p0, Lkotlinx/coroutines/aw$a;->g:Lkotlinx/coroutines/aw$b;

    iput-object p3, p0, Lkotlinx/coroutines/aw$a;->h:Lkotlinx/coroutines/j;

    iput-object p4, p0, Lkotlinx/coroutines/aw$a;->i:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)V
    .locals 3
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 1030
    iget-object p1, p0, Lkotlinx/coroutines/aw$a;->a:Lkotlinx/coroutines/aw;

    iget-object v0, p0, Lkotlinx/coroutines/aw$a;->g:Lkotlinx/coroutines/aw$b;

    iget-object v1, p0, Lkotlinx/coroutines/aw$a;->h:Lkotlinx/coroutines/j;

    iget-object v2, p0, Lkotlinx/coroutines/aw$a;->i:Ljava/lang/Object;

    invoke-static {p1, v0, v1, v2}, Lkotlinx/coroutines/aw;->a(Lkotlinx/coroutines/aw;Lkotlinx/coroutines/aw$b;Lkotlinx/coroutines/j;Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1023
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lkotlinx/coroutines/aw$a;->a(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1033
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChildCompletion["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lkotlinx/coroutines/aw$a;->h:Lkotlinx/coroutines/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lkotlinx/coroutines/aw$a;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
