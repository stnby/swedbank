.class public final Lkotlinx/coroutines/j;
.super Lkotlinx/coroutines/ar;
.source "JobSupport.kt"

# interfaces
.implements Lkotlinx/coroutines/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlinx/coroutines/ar<",
        "Lkotlinx/coroutines/aw;",
        ">;",
        "Lkotlinx/coroutines/i;"
    }
.end annotation


# instance fields
.field public final a:Lkotlinx/coroutines/k;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/aw;Lkotlinx/coroutines/k;)V
    .locals 1
    .param p1    # Lkotlinx/coroutines/aw;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlinx/coroutines/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "childJob"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1323
    check-cast p1, Lkotlinx/coroutines/aq;

    invoke-direct {p0, p1}, Lkotlinx/coroutines/ar;-><init>(Lkotlinx/coroutines/aq;)V

    iput-object p2, p0, Lkotlinx/coroutines/j;->a:Lkotlinx/coroutines/k;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 1324
    iget-object p1, p0, Lkotlinx/coroutines/j;->a:Lkotlinx/coroutines/k;

    iget-object v0, p0, Lkotlinx/coroutines/j;->b:Lkotlinx/coroutines/aq;

    check-cast v0, Lkotlinx/coroutines/bc;

    invoke-interface {p1, v0}, Lkotlinx/coroutines/k;->a(Lkotlinx/coroutines/bc;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1320
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lkotlinx/coroutines/j;->a(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public b(Ljava/lang/Throwable;)Z
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cause"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1325
    iget-object v0, p0, Lkotlinx/coroutines/j;->b:Lkotlinx/coroutines/aq;

    check-cast v0, Lkotlinx/coroutines/aw;

    invoke-virtual {v0, p1}, Lkotlinx/coroutines/aw;->b(Ljava/lang/Throwable;)Z

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChildHandle["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lkotlinx/coroutines/j;->a:Lkotlinx/coroutines/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
