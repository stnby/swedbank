.class public interface abstract Lkotlinx/coroutines/s;
.super Ljava/lang/Object;
.source "CoroutineExceptionHandler.kt"

# interfaces
.implements Lkotlin/c/e$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlinx/coroutines/s$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlinx/coroutines/s$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lkotlinx/coroutines/s$a;->a:Lkotlinx/coroutines/s$a;

    sput-object v0, Lkotlinx/coroutines/s;->a:Lkotlinx/coroutines/s$a;

    return-void
.end method


# virtual methods
.method public abstract handleException(Lkotlin/c/e;Ljava/lang/Throwable;)V
    .param p1    # Lkotlin/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method
