.class public abstract Lkotlinx/coroutines/ah$a;
.super Ljava/lang/Object;
.source "EventLoop.kt"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;
.implements Lkotlinx/coroutines/a/t;
.implements Lkotlinx/coroutines/ae;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlinx/coroutines/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lkotlinx/coroutines/ah$a;",
        ">;",
        "Ljava/lang/Runnable;",
        "Lkotlinx/coroutines/a/t;",
        "Lkotlinx/coroutines/ae;"
    }
.end annotation


# instance fields
.field public final a:J

.field private b:Ljava/lang/Object;

.field private c:I


# virtual methods
.method public final declared-synchronized a(Lkotlinx/coroutines/a/s;Lkotlinx/coroutines/ah;)I
    .locals 2
    .param p1    # Lkotlinx/coroutines/a/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlinx/coroutines/ah;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/a/s<",
            "Lkotlinx/coroutines/ah$a;",
            ">;",
            "Lkotlinx/coroutines/ah;",
            ")I"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "delayed"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventLoop"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lkotlinx/coroutines/ah$a;->b:Ljava/lang/Object;

    invoke-static {}, Lkotlinx/coroutines/ai;->b()Lkotlinx/coroutines/a/p;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-ne v0, v1, :cond_0

    const/4 p1, 0x2

    monitor-exit p0

    return p1

    .line 270
    :cond_0
    :try_start_1
    move-object v0, p0

    check-cast v0, Lkotlinx/coroutines/a/t;

    .line 342
    monitor-enter p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 270
    :try_start_2
    invoke-static {p2}, Lkotlinx/coroutines/ah;->a(Lkotlinx/coroutines/ah;)Z

    move-result p2

    const/4 v1, 0x1

    xor-int/2addr p2, v1

    if-eqz p2, :cond_1

    .line 344
    invoke-virtual {p1, v0}, Lkotlinx/coroutines/a/s;->b(Lkotlinx/coroutines/a/t;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 348
    :goto_0
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    xor-int/lit8 p1, p2, 0x1

    .line 270
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p2

    .line 348
    :try_start_4
    monitor-exit p1

    throw p2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    .line 268
    monitor-exit p0

    throw p1
.end method

.method public a(Lkotlinx/coroutines/ah$a;)I
    .locals 4
    .param p1    # Lkotlinx/coroutines/ah$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    iget-wide v0, p0, Lkotlinx/coroutines/ah$a;->a:J

    iget-wide v2, p1, Lkotlinx/coroutines/ah$a;->a:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    if-gez p1, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    .line 278
    :try_start_0
    iget-object v0, p0, Lkotlinx/coroutines/ah$a;->b:Ljava/lang/Object;

    .line 279
    invoke-static {}, Lkotlinx/coroutines/ai;->b()Lkotlinx/coroutines/a/p;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    monitor-exit p0

    return-void

    .line 281
    :cond_0
    :try_start_1
    instance-of v1, v0, Lkotlinx/coroutines/a/s;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    check-cast v0, Lkotlinx/coroutines/a/s;

    if-eqz v0, :cond_2

    move-object v1, p0

    check-cast v1, Lkotlinx/coroutines/a/t;

    invoke-virtual {v0, v1}, Lkotlinx/coroutines/a/s;->a(Lkotlinx/coroutines/a/t;)Z

    .line 282
    :cond_2
    invoke-static {}, Lkotlinx/coroutines/ai;->b()Lkotlinx/coroutines/a/p;

    move-result-object v0

    iput-object v0, p0, Lkotlinx/coroutines/ah$a;->b:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 277
    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .locals 0

    .line 252
    iput p1, p0, Lkotlinx/coroutines/ah$a;->c:I

    return-void
.end method

.method public a(Lkotlinx/coroutines/a/s;)V
    .locals 2
    .param p1    # Lkotlinx/coroutines/a/s;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/a/s<",
            "*>;)V"
        }
    .end annotation

    .line 248
    iget-object v0, p0, Lkotlinx/coroutines/ah$a;->b:Ljava/lang/Object;

    invoke-static {}, Lkotlinx/coroutines/ai;->b()Lkotlinx/coroutines/a/p;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 249
    iput-object p1, p0, Lkotlinx/coroutines/ah$a;->b:Ljava/lang/Object;

    return-void

    .line 248
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final a(J)Z
    .locals 2

    .line 265
    iget-wide v0, p0, Lkotlinx/coroutines/ah$a;->a:J

    sub-long/2addr p1, v0

    const-wide/16 v0, 0x0

    cmp-long p1, p1, v0

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()Lkotlinx/coroutines/a/s;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/a/s<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 246
    iget-object v0, p0, Lkotlinx/coroutines/ah$a;->b:Ljava/lang/Object;

    instance-of v1, v0, Lkotlinx/coroutines/a/s;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlinx/coroutines/a/s;

    return-object v0
.end method

.method public c()I
    .locals 1

    .line 252
    iget v0, p0, Lkotlinx/coroutines/ah$a;->c:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 240
    check-cast p1, Lkotlinx/coroutines/ah$a;

    invoke-virtual {p0, p1}, Lkotlinx/coroutines/ah$a;->a(Lkotlinx/coroutines/ah$a;)I

    move-result p1

    return p1
.end method

.method public final d()V
    .locals 1

    .line 274
    sget-object v0, Lkotlinx/coroutines/y;->b:Lkotlinx/coroutines/y;

    invoke-virtual {v0, p0}, Lkotlinx/coroutines/y;->a(Lkotlinx/coroutines/ah$a;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Delayed[nanos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lkotlinx/coroutines/ah$a;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
