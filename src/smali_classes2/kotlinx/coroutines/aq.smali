.class public interface abstract Lkotlinx/coroutines/aq;
.super Ljava/lang/Object;
.source "Job.kt"

# interfaces
.implements Lkotlin/c/e$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlinx/coroutines/aq$a;,
        Lkotlinx/coroutines/aq$b;
    }
.end annotation


# static fields
.field public static final a:Lkotlinx/coroutines/aq$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lkotlinx/coroutines/aq$b;->a:Lkotlinx/coroutines/aq$b;

    sput-object v0, Lkotlinx/coroutines/aq;->a:Lkotlinx/coroutines/aq$b;

    return-void
.end method


# virtual methods
.method public abstract a(ZZLkotlin/e/a/b;)Lkotlinx/coroutines/ae;
    .param p3    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/s;",
            ">;)",
            "Lkotlinx/coroutines/ae;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a(Lkotlinx/coroutines/k;)Lkotlinx/coroutines/i;
    .param p1    # Lkotlinx/coroutines/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a()Z
.end method

.method public abstract a(Ljava/lang/Throwable;)Z
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
.end method

.method public abstract b()Ljava/util/concurrent/CancellationException;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract c()Z
.end method

.method public abstract d()V
.end method
