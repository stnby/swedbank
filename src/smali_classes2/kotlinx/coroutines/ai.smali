.class public final Lkotlinx/coroutines/ai;
.super Ljava/lang/Object;
.source "EventLoop.kt"


# static fields
.field private static final a:Lkotlinx/coroutines/a/p;

.field private static final b:Lkotlinx/coroutines/a/p;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "REMOVED_TASK"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/ai;->a:Lkotlinx/coroutines/a/p;

    .line 31
    new-instance v0, Lkotlinx/coroutines/a/p;

    const-string v1, "CLOSED_EMPTY"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/ai;->b:Lkotlinx/coroutines/a/p;

    return-void
.end method

.method public static final a()Lkotlinx/coroutines/ag;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 316
    new-instance v0, Lkotlinx/coroutines/b;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "Thread.currentThread()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lkotlinx/coroutines/b;-><init>(Ljava/lang/Thread;)V

    check-cast v0, Lkotlinx/coroutines/ag;

    return-object v0
.end method

.method public static final synthetic b()Lkotlinx/coroutines/a/p;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/ai;->a:Lkotlinx/coroutines/a/p;

    return-object v0
.end method

.method public static final synthetic c()Lkotlinx/coroutines/a/p;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/ai;->b:Lkotlinx/coroutines/a/p;

    return-object v0
.end method
