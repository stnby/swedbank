.class public final Lkotlinx/coroutines/be$a;
.super Ljava/lang/Object;
.source "ThreadContextElement.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlinx/coroutines/be;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lkotlinx/coroutines/be;Ljava/lang/Object;Lkotlin/e/a/m;)Ljava/lang/Object;
    .locals 1
    .param p2    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/be<",
            "TS;>;TR;",
            "Lkotlin/e/a/m<",
            "-TR;-",
            "Lkotlin/c/e$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    const-string v0, "operation"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lkotlin/c/e$b;

    invoke-static {p0, p1, p2}, Lkotlin/c/e$b$a;->a(Lkotlin/c/e$b;Ljava/lang/Object;Lkotlin/e/a/m;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlinx/coroutines/be;Lkotlin/c/e$c;)Lkotlin/c/e$b;
    .locals 1
    .param p1    # Lkotlin/c/e$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E::",
            "Lkotlin/c/e$b;",
            ">(",
            "Lkotlinx/coroutines/be<",
            "TS;>;",
            "Lkotlin/c/e$c<",
            "TE;>;)TE;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lkotlin/c/e$b;

    invoke-static {p0, p1}, Lkotlin/c/e$b$a;->a(Lkotlin/c/e$b;Lkotlin/c/e$c;)Lkotlin/c/e$b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlinx/coroutines/be;Lkotlin/c/e;)Lkotlin/c/e;
    .locals 1
    .param p1    # Lkotlin/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/be<",
            "TS;>;",
            "Lkotlin/c/e;",
            ")",
            "Lkotlin/c/e;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lkotlin/c/e$b;

    invoke-static {p0, p1}, Lkotlin/c/e$b$a;->a(Lkotlin/c/e$b;Lkotlin/c/e;)Lkotlin/c/e;

    move-result-object p0

    return-object p0
.end method

.method public static b(Lkotlinx/coroutines/be;Lkotlin/c/e$c;)Lkotlin/c/e;
    .locals 1
    .param p1    # Lkotlin/c/e$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/be<",
            "TS;>;",
            "Lkotlin/c/e$c<",
            "*>;)",
            "Lkotlin/c/e;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lkotlin/c/e$b;

    invoke-static {p0, p1}, Lkotlin/c/e$b$a;->b(Lkotlin/c/e$b;Lkotlin/c/e$c;)Lkotlin/c/e;

    move-result-object p0

    return-object p0
.end method
