.class public abstract Lkotlinx/coroutines/av;
.super Lkotlinx/coroutines/p;
.source "JobSupport.kt"

# interfaces
.implements Lkotlinx/coroutines/ae;
.implements Lkotlinx/coroutines/al;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<J::",
        "Lkotlinx/coroutines/aq;",
        ">",
        "Lkotlinx/coroutines/p;",
        "Lkotlinx/coroutines/ae;",
        "Lkotlinx/coroutines/al;"
    }
.end annotation


# instance fields
.field public final b:Lkotlinx/coroutines/aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TJ;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/aq;)V
    .locals 1
    .param p1    # Lkotlinx/coroutines/aq;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TJ;)V"
        }
    .end annotation

    const-string v0, "job"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1201
    invoke-direct {p0}, Lkotlinx/coroutines/p;-><init>()V

    iput-object p1, p0, Lkotlinx/coroutines/av;->b:Lkotlinx/coroutines/aq;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 1204
    iget-object v0, p0, Lkotlinx/coroutines/av;->b:Lkotlinx/coroutines/aq;

    if-eqz v0, :cond_0

    check-cast v0, Lkotlinx/coroutines/aw;

    invoke-virtual {v0, p0}, Lkotlinx/coroutines/aw;->a(Lkotlinx/coroutines/av;)V

    return-void

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlinx.coroutines.JobSupport"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public x_()Lkotlinx/coroutines/az;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
