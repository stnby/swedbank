.class public final Lokhttp3/internal/c/a;
.super Ljava/lang/Object;
.source "Http1ExchangeCodec.java"

# interfaces
.implements Lokhttp3/internal/b/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lokhttp3/internal/c/a$f;,
        Lokhttp3/internal/c/a$c;,
        Lokhttp3/internal/c/a$d;,
        Lokhttp3/internal/c/a$a;,
        Lokhttp3/internal/c/a$b;,
        Lokhttp3/internal/c/a$e;
    }
.end annotation


# instance fields
.field private final a:Lokhttp3/x;

.field private final b:Lokhttp3/internal/connection/e;

.field private final c:Lb/e;

.field private final d:Lb/d;

.field private e:I

.field private f:J

.field private g:Lokhttp3/s;


# direct methods
.method public constructor <init>(Lokhttp3/x;Lokhttp3/internal/connection/e;Lb/e;Lb/d;)V
    .locals 2

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 84
    iput v0, p0, Lokhttp3/internal/c/a;->e:I

    const-wide/32 v0, 0x40000

    .line 85
    iput-wide v0, p0, Lokhttp3/internal/c/a;->f:J

    .line 95
    iput-object p1, p0, Lokhttp3/internal/c/a;->a:Lokhttp3/x;

    .line 96
    iput-object p2, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    .line 97
    iput-object p3, p0, Lokhttp3/internal/c/a;->c:Lb/e;

    .line 98
    iput-object p4, p0, Lokhttp3/internal/c/a;->d:Lb/d;

    return-void
.end method

.method static synthetic a(Lokhttp3/internal/c/a;I)I
    .locals 0

    .line 66
    iput p1, p0, Lokhttp3/internal/c/a;->e:I

    return p1
.end method

.method static synthetic a(Lokhttp3/internal/c/a;)Lb/d;
    .locals 0

    .line 66
    iget-object p0, p0, Lokhttp3/internal/c/a;->d:Lb/d;

    return-object p0
.end method

.method private a(J)Lb/u;
    .locals 2

    .line 266
    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    .line 267
    iput v0, p0, Lokhttp3/internal/c/a;->e:I

    .line 268
    new-instance v0, Lokhttp3/internal/c/a$d;

    invoke-direct {v0, p0, p1, p2}, Lokhttp3/internal/c/a$d;-><init>(Lokhttp3/internal/c/a;J)V

    return-object v0

    .line 266
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "state: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Lokhttp3/t;)Lb/u;
    .locals 2

    .line 272
    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    .line 273
    iput v0, p0, Lokhttp3/internal/c/a;->e:I

    .line 274
    new-instance v0, Lokhttp3/internal/c/a$c;

    invoke-direct {v0, p0, p1}, Lokhttp3/internal/c/a$c;-><init>(Lokhttp3/internal/c/a;Lokhttp3/t;)V

    return-object v0

    .line 272
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lokhttp3/internal/c/a;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic a(Lokhttp3/internal/c/a;Lokhttp3/s;)Lokhttp3/s;
    .locals 0

    .line 66
    iput-object p1, p0, Lokhttp3/internal/c/a;->g:Lokhttp3/s;

    return-object p1
.end method

.method private a(Lb/i;)V
    .locals 2

    .line 290
    invoke-virtual {p1}, Lb/i;->a()Lb/v;

    move-result-object v0

    .line 291
    sget-object v1, Lb/v;->c:Lb/v;

    invoke-virtual {p1, v1}, Lb/i;->a(Lb/v;)Lb/i;

    .line 292
    invoke-virtual {v0}, Lb/v;->f()Lb/v;

    .line 293
    invoke-virtual {v0}, Lb/v;->A_()Lb/v;

    return-void
.end method

.method static synthetic a(Lokhttp3/internal/c/a;Lb/i;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lokhttp3/internal/c/a;->a(Lb/i;)V

    return-void
.end method

.method static synthetic b(Lokhttp3/internal/c/a;)Lb/e;
    .locals 0

    .line 66
    iget-object p0, p0, Lokhttp3/internal/c/a;->c:Lb/e;

    return-object p0
.end method

.method static synthetic c(Lokhttp3/internal/c/a;)Lokhttp3/internal/connection/e;
    .locals 0

    .line 66
    iget-object p0, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    return-object p0
.end method

.method static synthetic d(Lokhttp3/internal/c/a;)I
    .locals 0

    .line 66
    iget p0, p0, Lokhttp3/internal/c/a;->e:I

    return p0
.end method

.method private e()Ljava/lang/String;
    .locals 5

    .line 238
    iget-object v0, p0, Lokhttp3/internal/c/a;->c:Lb/e;

    iget-wide v1, p0, Lokhttp3/internal/c/a;->f:J

    invoke-interface {v0, v1, v2}, Lb/e;->f(J)Ljava/lang/String;

    move-result-object v0

    .line 239
    iget-wide v1, p0, Lokhttp3/internal/c/a;->f:J

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v3, v3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lokhttp3/internal/c/a;->f:J

    return-object v0
.end method

.method static synthetic e(Lokhttp3/internal/c/a;)Lokhttp3/s;
    .locals 0

    .line 66
    invoke-direct {p0}, Lokhttp3/internal/c/a;->f()Lokhttp3/s;

    move-result-object p0

    return-object p0
.end method

.method private f()Lokhttp3/s;
    .locals 3

    .line 245
    new-instance v0, Lokhttp3/s$a;

    invoke-direct {v0}, Lokhttp3/s$a;-><init>()V

    .line 247
    :goto_0
    invoke-direct {p0}, Lokhttp3/internal/c/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 248
    sget-object v2, Lokhttp3/internal/a;->a:Lokhttp3/internal/a;

    invoke-virtual {v2, v0, v1}, Lokhttp3/internal/a;->a(Lokhttp3/s$a;Ljava/lang/String;)V

    goto :goto_0

    .line 250
    :cond_0
    invoke-virtual {v0}, Lokhttp3/s$a;->a()Lokhttp3/s;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lokhttp3/internal/c/a;)Lokhttp3/x;
    .locals 0

    .line 66
    iget-object p0, p0, Lokhttp3/internal/c/a;->a:Lokhttp3/x;

    return-object p0
.end method

.method private g()Lb/t;
    .locals 3

    .line 254
    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    .line 255
    iput v0, p0, Lokhttp3/internal/c/a;->e:I

    .line 256
    new-instance v0, Lokhttp3/internal/c/a$b;

    invoke-direct {v0, p0}, Lokhttp3/internal/c/a$b;-><init>(Lokhttp3/internal/c/a;)V

    return-object v0

    .line 254
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lokhttp3/internal/c/a;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic g(Lokhttp3/internal/c/a;)Lokhttp3/s;
    .locals 0

    .line 66
    iget-object p0, p0, Lokhttp3/internal/c/a;->g:Lokhttp3/s;

    return-object p0
.end method

.method private h()Lb/t;
    .locals 3

    .line 260
    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    .line 261
    iput v0, p0, Lokhttp3/internal/c/a;->e:I

    .line 262
    new-instance v0, Lokhttp3/internal/c/a$e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lokhttp3/internal/c/a$e;-><init>(Lokhttp3/internal/c/a;Lokhttp3/internal/c/a$1;)V

    return-object v0

    .line 260
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lokhttp3/internal/c/a;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private i()Lb/u;
    .locals 3

    .line 278
    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    .line 279
    iput v0, p0, Lokhttp3/internal/c/a;->e:I

    .line 280
    iget-object v0, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    invoke-virtual {v0}, Lokhttp3/internal/connection/e;->a()V

    .line 281
    new-instance v0, Lokhttp3/internal/c/a$f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lokhttp3/internal/c/a$f;-><init>(Lokhttp3/internal/c/a;Lokhttp3/internal/c/a$1;)V

    return-object v0

    .line 278
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lokhttp3/internal/c/a;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lokhttp3/ac;)J
    .locals 2

    .line 145
    invoke-static {p1}, Lokhttp3/internal/b/e;->d(Lokhttp3/ac;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    const-string v0, "chunked"

    const-string v1, "Transfer-Encoding"

    .line 149
    invoke-virtual {p1, v1}, Lokhttp3/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    return-wide v0

    .line 153
    :cond_1
    invoke-static {p1}, Lokhttp3/internal/b/e;->a(Lokhttp3/ac;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lokhttp3/aa;J)Lb/t;
    .locals 2

    .line 106
    invoke-virtual {p1}, Lokhttp3/aa;->d()Lokhttp3/ab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lokhttp3/aa;->d()Lokhttp3/ab;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ab;->c()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 107
    :cond_0
    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Duplex connections are not supported for HTTP/1"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    const-string v0, "chunked"

    const-string v1, "Transfer-Encoding"

    .line 110
    invoke-virtual {p1, v1}, Lokhttp3/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 112
    invoke-direct {p0}, Lokhttp3/internal/c/a;->g()Lb/t;

    move-result-object p1

    return-object p1

    :cond_2
    const-wide/16 v0, -0x1

    cmp-long p1, p2, v0

    if-eqz p1, :cond_3

    .line 117
    invoke-direct {p0}, Lokhttp3/internal/c/a;->h()Lb/t;

    move-result-object p1

    return-object p1

    .line 120
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Z)Lokhttp3/ac$a;
    .locals 4

    .line 208
    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 209
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lokhttp3/internal/c/a;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 213
    :cond_1
    :goto_0
    :try_start_0
    invoke-direct {p0}, Lokhttp3/internal/c/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/internal/b/k;->a(Ljava/lang/String;)Lokhttp3/internal/b/k;

    move-result-object v0

    .line 215
    new-instance v2, Lokhttp3/ac$a;

    invoke-direct {v2}, Lokhttp3/ac$a;-><init>()V

    iget-object v3, v0, Lokhttp3/internal/b/k;->a:Lokhttp3/y;

    .line 216
    invoke-virtual {v2, v3}, Lokhttp3/ac$a;->a(Lokhttp3/y;)Lokhttp3/ac$a;

    move-result-object v2

    iget v3, v0, Lokhttp3/internal/b/k;->b:I

    .line 217
    invoke-virtual {v2, v3}, Lokhttp3/ac$a;->a(I)Lokhttp3/ac$a;

    move-result-object v2

    iget-object v3, v0, Lokhttp3/internal/b/k;->c:Ljava/lang/String;

    .line 218
    invoke-virtual {v2, v3}, Lokhttp3/ac$a;->a(Ljava/lang/String;)Lokhttp3/ac$a;

    move-result-object v2

    .line 219
    invoke-direct {p0}, Lokhttp3/internal/c/a;->f()Lokhttp3/s;

    move-result-object v3

    invoke-virtual {v2, v3}, Lokhttp3/ac$a;->a(Lokhttp3/s;)Lokhttp3/ac$a;

    move-result-object v2

    const/16 v3, 0x64

    if-eqz p1, :cond_2

    .line 221
    iget p1, v0, Lokhttp3/internal/b/k;->b:I

    if-ne p1, v3, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 223
    :cond_2
    iget p1, v0, Lokhttp3/internal/b/k;->b:I

    if-ne p1, v3, :cond_3

    .line 224
    iput v1, p0, Lokhttp3/internal/c/a;->e:I

    return-object v2

    :cond_3
    const/4 p1, 0x4

    .line 228
    iput p1, p0, Lokhttp3/internal/c/a;->e:I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception p1

    .line 232
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected end of stream on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    .line 233
    invoke-virtual {v2}, Lokhttp3/internal/connection/e;->b()Lokhttp3/ae;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/ae;->a()Lokhttp3/a;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/a;->a()Lokhttp3/t;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/t;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public a()Lokhttp3/internal/connection/e;
    .locals 1

    .line 102
    iget-object v0, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    return-object v0
.end method

.method public a(Lokhttp3/aa;)V
    .locals 1

    .line 139
    iget-object v0, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    .line 140
    invoke-virtual {v0}, Lokhttp3/internal/connection/e;->b()Lokhttp3/ae;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ae;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    .line 139
    invoke-static {p1, v0}, Lokhttp3/internal/b/i;->a(Lokhttp3/aa;Ljava/net/Proxy$Type;)Ljava/lang/String;

    move-result-object v0

    .line 141
    invoke-virtual {p1}, Lokhttp3/aa;->c()Lokhttp3/s;

    move-result-object p1

    invoke-virtual {p0, p1, v0}, Lokhttp3/internal/c/a;->a(Lokhttp3/s;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lokhttp3/s;Ljava/lang/String;)V
    .locals 3

    .line 195
    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    if-nez v0, :cond_1

    .line 196
    iget-object v0, p0, Lokhttp3/internal/c/a;->d:Lb/d;

    invoke-interface {v0, p2}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    move-result-object p2

    const-string v0, "\r\n"

    invoke-interface {p2, v0}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    const/4 p2, 0x0

    .line 197
    invoke-virtual {p1}, Lokhttp3/s;->a()I

    move-result v0

    :goto_0
    if-ge p2, v0, :cond_0

    .line 198
    iget-object v1, p0, Lokhttp3/internal/c/a;->d:Lb/d;

    invoke-virtual {p1, p2}, Lokhttp3/s;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    move-result-object v1

    const-string v2, ": "

    .line 199
    invoke-interface {v1, v2}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    move-result-object v1

    .line 200
    invoke-virtual {p1, p2}, Lokhttp3/s;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    move-result-object v1

    const-string v2, "\r\n"

    .line 201
    invoke-interface {v1, v2}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 203
    :cond_0
    iget-object p1, p0, Lokhttp3/internal/c/a;->d:Lb/d;

    const-string p2, "\r\n"

    invoke-interface {p1, p2}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    const/4 p1, 0x1

    .line 204
    iput p1, p0, Lokhttp3/internal/c/a;->e:I

    return-void

    .line 195
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "state: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lokhttp3/internal/c/a;->e:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(Lokhttp3/ac;)Lb/u;
    .locals 4

    .line 157
    invoke-static {p1}, Lokhttp3/internal/b/e;->d(Lokhttp3/ac;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 158
    invoke-direct {p0, v0, v1}, Lokhttp3/internal/c/a;->a(J)Lb/u;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v0, "chunked"

    const-string v1, "Transfer-Encoding"

    .line 161
    invoke-virtual {p1, v1}, Lokhttp3/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {p1}, Lokhttp3/ac;->a()Lokhttp3/aa;

    move-result-object p1

    invoke-virtual {p1}, Lokhttp3/aa;->a()Lokhttp3/t;

    move-result-object p1

    invoke-direct {p0, p1}, Lokhttp3/internal/c/a;->a(Lokhttp3/t;)Lb/u;

    move-result-object p1

    return-object p1

    .line 165
    :cond_1
    invoke-static {p1}, Lokhttp3/internal/b/e;->a(Lokhttp3/ac;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-eqz p1, :cond_2

    .line 167
    invoke-direct {p0, v0, v1}, Lokhttp3/internal/c/a;->a(J)Lb/u;

    move-result-object p1

    return-object p1

    .line 170
    :cond_2
    invoke-direct {p0}, Lokhttp3/internal/c/a;->i()Lb/u;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 1

    .line 186
    iget-object v0, p0, Lokhttp3/internal/c/a;->d:Lb/d;

    invoke-interface {v0}, Lb/d;->flush()V

    return-void
.end method

.method public c()V
    .locals 1

    .line 190
    iget-object v0, p0, Lokhttp3/internal/c/a;->d:Lb/d;

    invoke-interface {v0}, Lb/d;->flush()V

    return-void
.end method

.method public c(Lokhttp3/ac;)V
    .locals 4

    .line 301
    invoke-static {p1}, Lokhttp3/internal/b/e;->a(Lokhttp3/ac;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    return-void

    .line 303
    :cond_0
    invoke-direct {p0, v0, v1}, Lokhttp3/internal/c/a;->a(J)Lb/u;

    move-result-object p1

    const v0, 0x7fffffff

    .line 304
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1, v0, v1}, Lokhttp3/internal/c;->b(Lb/u;ILjava/util/concurrent/TimeUnit;)Z

    .line 305
    invoke-interface {p1}, Lb/u;->close()V

    return-void
.end method

.method public d()V
    .locals 1

    .line 125
    iget-object v0, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lokhttp3/internal/c/a;->b:Lokhttp3/internal/connection/e;

    invoke-virtual {v0}, Lokhttp3/internal/connection/e;->c()V

    :cond_0
    return-void
.end method
