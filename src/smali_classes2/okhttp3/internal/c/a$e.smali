.class final Lokhttp3/internal/c/a$e;
.super Ljava/lang/Object;
.source "Http1ExchangeCodec.java"

# interfaces
.implements Lb/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lokhttp3/internal/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lokhttp3/internal/c/a;

.field private final b:Lb/i;

.field private c:Z


# direct methods
.method private constructor <init>(Lokhttp3/internal/c/a;)V
    .locals 1

    .line 309
    iput-object p1, p0, Lokhttp3/internal/c/a$e;->a:Lokhttp3/internal/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    new-instance p1, Lb/i;

    iget-object v0, p0, Lokhttp3/internal/c/a$e;->a:Lokhttp3/internal/c/a;

    invoke-static {v0}, Lokhttp3/internal/c/a;->a(Lokhttp3/internal/c/a;)Lb/d;

    move-result-object v0

    invoke-interface {v0}, Lb/d;->a()Lb/v;

    move-result-object v0

    invoke-direct {p1, v0}, Lb/i;-><init>(Lb/v;)V

    iput-object p1, p0, Lokhttp3/internal/c/a$e;->b:Lb/i;

    return-void
.end method

.method synthetic constructor <init>(Lokhttp3/internal/c/a;Lokhttp3/internal/c/a$1;)V
    .locals 0

    .line 309
    invoke-direct {p0, p1}, Lokhttp3/internal/c/a$e;-><init>(Lokhttp3/internal/c/a;)V

    return-void
.end method


# virtual methods
.method public a()Lb/v;
    .locals 1

    .line 314
    iget-object v0, p0, Lokhttp3/internal/c/a$e;->b:Lb/i;

    return-object v0
.end method

.method public a_(Lb/c;J)V
    .locals 7

    .line 318
    iget-boolean v0, p0, Lokhttp3/internal/c/a$e;->c:Z

    if-nez v0, :cond_0

    .line 319
    invoke-virtual {p1}, Lb/c;->b()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Lokhttp3/internal/c;->a(JJJ)V

    .line 320
    iget-object v0, p0, Lokhttp3/internal/c/a$e;->a:Lokhttp3/internal/c/a;

    invoke-static {v0}, Lokhttp3/internal/c/a;->a(Lokhttp3/internal/c/a;)Lb/d;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lb/d;->a_(Lb/c;J)V

    return-void

    .line 318
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public close()V
    .locals 2

    .line 329
    iget-boolean v0, p0, Lokhttp3/internal/c/a$e;->c:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 330
    iput-boolean v0, p0, Lokhttp3/internal/c/a$e;->c:Z

    .line 331
    iget-object v0, p0, Lokhttp3/internal/c/a$e;->a:Lokhttp3/internal/c/a;

    iget-object v1, p0, Lokhttp3/internal/c/a$e;->b:Lb/i;

    invoke-static {v0, v1}, Lokhttp3/internal/c/a;->a(Lokhttp3/internal/c/a;Lb/i;)V

    .line 332
    iget-object v0, p0, Lokhttp3/internal/c/a$e;->a:Lokhttp3/internal/c/a;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lokhttp3/internal/c/a;->a(Lokhttp3/internal/c/a;I)I

    return-void
.end method

.method public flush()V
    .locals 1

    .line 324
    iget-boolean v0, p0, Lokhttp3/internal/c/a$e;->c:Z

    if-eqz v0, :cond_0

    return-void

    .line 325
    :cond_0
    iget-object v0, p0, Lokhttp3/internal/c/a$e;->a:Lokhttp3/internal/c/a;

    invoke-static {v0}, Lokhttp3/internal/c/a;->a(Lokhttp3/internal/c/a;)Lb/d;

    move-result-object v0

    invoke-interface {v0}, Lb/d;->flush()V

    return-void
.end method
