.class final Lokhttp3/internal/connection/d;
.super Ljava/lang/Object;
.source "ExchangeFinder.java"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lokhttp3/internal/connection/i;

.field private final c:Lokhttp3/a;

.field private final d:Lokhttp3/internal/connection/f;

.field private final e:Lokhttp3/e;

.field private final f:Lokhttp3/p;

.field private g:Lokhttp3/internal/connection/h$a;

.field private final h:Lokhttp3/internal/connection/h;

.field private i:Lokhttp3/internal/connection/e;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    const-class v0, Lokhttp3/internal/connection/d;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lokhttp3/internal/connection/d;->a:Z

    return-void
.end method

.method constructor <init>(Lokhttp3/internal/connection/i;Lokhttp3/internal/connection/f;Lokhttp3/a;Lokhttp3/e;Lokhttp3/p;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    .line 70
    iput-object p2, p0, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    .line 71
    iput-object p3, p0, Lokhttp3/internal/connection/d;->c:Lokhttp3/a;

    .line 72
    iput-object p4, p0, Lokhttp3/internal/connection/d;->e:Lokhttp3/e;

    .line 73
    iput-object p5, p0, Lokhttp3/internal/connection/d;->f:Lokhttp3/p;

    .line 74
    new-instance p1, Lokhttp3/internal/connection/h;

    iget-object p2, p2, Lokhttp3/internal/connection/f;->a:Lokhttp3/internal/connection/g;

    invoke-direct {p1, p3, p2, p4, p5}, Lokhttp3/internal/connection/h;-><init>(Lokhttp3/a;Lokhttp3/internal/connection/g;Lokhttp3/e;Lokhttp3/p;)V

    iput-object p1, p0, Lokhttp3/internal/connection/d;->h:Lokhttp3/internal/connection/h;

    return-void
.end method

.method private a(IIIIZ)Lokhttp3/internal/connection/e;
    .locals 18

    move-object/from16 v1, p0

    .line 139
    iget-object v2, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    monitor-enter v2

    .line 140
    :try_start_0
    iget-object v0, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    invoke-virtual {v0}, Lokhttp3/internal/connection/i;->i()Z

    move-result v0

    if-nez v0, :cond_11

    const/4 v0, 0x0

    .line 141
    iput-boolean v0, v1, Lokhttp3/internal/connection/d;->j:Z

    .line 143
    invoke-direct/range {p0 .. p0}, Lokhttp3/internal/connection/d;->e()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    .line 144
    iget-object v3, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v3, v3, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    invoke-virtual {v3}, Lokhttp3/internal/connection/e;->b()Lokhttp3/ae;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v4

    .line 149
    :goto_0
    iget-object v5, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v5, v5, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    .line 150
    iget-object v6, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v6, v6, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    if-eqz v6, :cond_1

    iget-object v6, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v6, v6, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    iget-boolean v6, v6, Lokhttp3/internal/connection/e;->b:Z

    if-eqz v6, :cond_1

    .line 151
    iget-object v6, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    invoke-virtual {v6}, Lokhttp3/internal/connection/i;->d()Ljava/net/Socket;

    move-result-object v6

    goto :goto_1

    :cond_1
    move-object v6, v4

    .line 154
    :goto_1
    iget-object v7, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v7, v7, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    if-eqz v7, :cond_2

    .line 156
    iget-object v5, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v5, v5, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    move-object v7, v4

    goto :goto_2

    :cond_2
    move-object v7, v5

    move-object v5, v4

    :goto_2
    const/4 v8, 0x1

    if-nez v5, :cond_4

    .line 162
    iget-object v9, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    iget-object v10, v1, Lokhttp3/internal/connection/d;->c:Lokhttp3/a;

    iget-object v11, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    invoke-virtual {v9, v10, v11, v4, v0}, Lokhttp3/internal/connection/f;->a(Lokhttp3/a;Lokhttp3/internal/connection/i;Ljava/util/List;Z)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 164
    iget-object v3, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v5, v3, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    move-object v9, v4

    const/4 v3, 0x1

    goto :goto_4

    :cond_3
    move-object v9, v3

    goto :goto_3

    :cond_4
    move-object v9, v4

    :goto_3
    const/4 v3, 0x0

    .line 169
    :goto_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 170
    invoke-static {v6}, Lokhttp3/internal/c;->a(Ljava/net/Socket;)V

    if-eqz v7, :cond_5

    .line 173
    iget-object v2, v1, Lokhttp3/internal/connection/d;->f:Lokhttp3/p;

    iget-object v6, v1, Lokhttp3/internal/connection/d;->e:Lokhttp3/e;

    invoke-virtual {v2, v6, v7}, Lokhttp3/p;->b(Lokhttp3/e;Lokhttp3/i;)V

    :cond_5
    if-eqz v3, :cond_6

    .line 176
    iget-object v2, v1, Lokhttp3/internal/connection/d;->f:Lokhttp3/p;

    iget-object v6, v1, Lokhttp3/internal/connection/d;->e:Lokhttp3/e;

    invoke-virtual {v2, v6, v5}, Lokhttp3/p;->a(Lokhttp3/e;Lokhttp3/i;)V

    :cond_6
    if-eqz v5, :cond_7

    return-object v5

    :cond_7
    if-nez v9, :cond_9

    .line 185
    iget-object v2, v1, Lokhttp3/internal/connection/d;->g:Lokhttp3/internal/connection/h$a;

    if-eqz v2, :cond_8

    iget-object v2, v1, Lokhttp3/internal/connection/d;->g:Lokhttp3/internal/connection/h$a;

    invoke-virtual {v2}, Lokhttp3/internal/connection/h$a;->a()Z

    move-result v2

    if-nez v2, :cond_9

    .line 187
    :cond_8
    iget-object v2, v1, Lokhttp3/internal/connection/d;->h:Lokhttp3/internal/connection/h;

    invoke-virtual {v2}, Lokhttp3/internal/connection/h;->b()Lokhttp3/internal/connection/h$a;

    move-result-object v2

    iput-object v2, v1, Lokhttp3/internal/connection/d;->g:Lokhttp3/internal/connection/h$a;

    const/4 v2, 0x1

    goto :goto_5

    :cond_9
    const/4 v2, 0x0

    .line 191
    :goto_5
    iget-object v6, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    monitor-enter v6

    .line 192
    :try_start_1
    iget-object v7, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    invoke-virtual {v7}, Lokhttp3/internal/connection/i;->i()Z

    move-result v7

    if-nez v7, :cond_10

    if-eqz v2, :cond_a

    .line 197
    iget-object v2, v1, Lokhttp3/internal/connection/d;->g:Lokhttp3/internal/connection/h$a;

    invoke-virtual {v2}, Lokhttp3/internal/connection/h$a;->c()Ljava/util/List;

    move-result-object v2

    .line 198
    iget-object v7, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    iget-object v10, v1, Lokhttp3/internal/connection/d;->c:Lokhttp3/a;

    iget-object v11, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    invoke-virtual {v7, v10, v11, v2, v0}, Lokhttp3/internal/connection/f;->a(Lokhttp3/a;Lokhttp3/internal/connection/i;Ljava/util/List;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 201
    iget-object v0, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v5, v0, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    const/4 v3, 0x1

    goto :goto_6

    :cond_a
    move-object v2, v4

    :cond_b
    :goto_6
    if-nez v3, :cond_d

    if-nez v9, :cond_c

    .line 207
    iget-object v0, v1, Lokhttp3/internal/connection/d;->g:Lokhttp3/internal/connection/h$a;

    invoke-virtual {v0}, Lokhttp3/internal/connection/h$a;->b()Lokhttp3/ae;

    move-result-object v9

    .line 212
    :cond_c
    new-instance v5, Lokhttp3/internal/connection/e;

    iget-object v0, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    invoke-direct {v5, v0, v9}, Lokhttp3/internal/connection/e;-><init>(Lokhttp3/internal/connection/f;Lokhttp3/ae;)V

    .line 213
    iput-object v5, v1, Lokhttp3/internal/connection/d;->i:Lokhttp3/internal/connection/e;

    .line 215
    :cond_d
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_e

    .line 219
    iget-object v0, v1, Lokhttp3/internal/connection/d;->f:Lokhttp3/p;

    iget-object v2, v1, Lokhttp3/internal/connection/d;->e:Lokhttp3/e;

    invoke-virtual {v0, v2, v5}, Lokhttp3/p;->a(Lokhttp3/e;Lokhttp3/i;)V

    return-object v5

    .line 224
    :cond_e
    iget-object v0, v1, Lokhttp3/internal/connection/d;->e:Lokhttp3/e;

    iget-object v3, v1, Lokhttp3/internal/connection/d;->f:Lokhttp3/p;

    move-object v10, v5

    move/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p3

    move/from16 v14, p4

    move/from16 v15, p5

    move-object/from16 v16, v0

    move-object/from16 v17, v3

    invoke-virtual/range {v10 .. v17}, Lokhttp3/internal/connection/e;->a(IIIIZLokhttp3/e;Lokhttp3/p;)V

    .line 226
    iget-object v0, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    iget-object v0, v0, Lokhttp3/internal/connection/f;->a:Lokhttp3/internal/connection/g;

    invoke-virtual {v5}, Lokhttp3/internal/connection/e;->b()Lokhttp3/ae;

    move-result-object v3

    invoke-virtual {v0, v3}, Lokhttp3/internal/connection/g;->b(Lokhttp3/ae;)V

    .line 229
    iget-object v3, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    monitor-enter v3

    .line 230
    :try_start_2
    iput-object v4, v1, Lokhttp3/internal/connection/d;->i:Lokhttp3/internal/connection/e;

    .line 233
    iget-object v0, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    iget-object v6, v1, Lokhttp3/internal/connection/d;->c:Lokhttp3/a;

    iget-object v7, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    invoke-virtual {v0, v6, v7, v2, v8}, Lokhttp3/internal/connection/f;->a(Lokhttp3/a;Lokhttp3/internal/connection/i;Ljava/util/List;Z)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 235
    iput-boolean v8, v5, Lokhttp3/internal/connection/e;->b:Z

    .line 236
    invoke-virtual {v5}, Lokhttp3/internal/connection/e;->d()Ljava/net/Socket;

    move-result-object v4

    .line 237
    iget-object v0, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v5, v0, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    goto :goto_7

    .line 239
    :cond_f
    iget-object v0, v1, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    invoke-virtual {v0, v5}, Lokhttp3/internal/connection/f;->a(Lokhttp3/internal/connection/e;)V

    .line 240
    iget-object v0, v1, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    invoke-virtual {v0, v5}, Lokhttp3/internal/connection/i;->a(Lokhttp3/internal/connection/e;)V

    .line 242
    :goto_7
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 243
    invoke-static {v4}, Lokhttp3/internal/c;->a(Ljava/net/Socket;)V

    .line 245
    iget-object v0, v1, Lokhttp3/internal/connection/d;->f:Lokhttp3/p;

    iget-object v2, v1, Lokhttp3/internal/connection/d;->e:Lokhttp3/e;

    invoke-virtual {v0, v2, v5}, Lokhttp3/p;->a(Lokhttp3/e;Lokhttp3/i;)V

    return-object v5

    :catchall_0
    move-exception v0

    .line 242
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 192
    :cond_10
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_1
    move-exception v0

    .line 215
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 140
    :cond_11
    :try_start_5
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Canceled"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_2
    move-exception v0

    .line 169
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method private a(IIIIZZ)Lokhttp3/internal/connection/e;
    .locals 3

    .line 107
    :goto_0
    invoke-direct/range {p0 .. p5}, Lokhttp3/internal/connection/d;->a(IIIIZ)Lokhttp3/internal/connection/e;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    monitor-enter v1

    .line 112
    :try_start_0
    iget v2, v0, Lokhttp3/internal/connection/e;->d:I

    if-nez v2, :cond_0

    .line 113
    monitor-exit v1

    return-object v0

    .line 115
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    invoke-virtual {v0, p6}, Lokhttp3/internal/connection/e;->a(Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    invoke-virtual {v0}, Lokhttp3/internal/connection/e;->a()V

    goto :goto_0

    :cond_1
    return-object v0

    :catchall_0
    move-exception p1

    .line 115
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private e()Z
    .locals 2

    .line 283
    iget-object v0, p0, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v0, v0, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v0, v0, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    iget v0, v0, Lokhttp3/internal/connection/e;->c:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lokhttp3/internal/connection/d;->b:Lokhttp3/internal/connection/i;

    iget-object v0, v0, Lokhttp3/internal/connection/i;->a:Lokhttp3/internal/connection/e;

    .line 285
    invoke-virtual {v0}, Lokhttp3/internal/connection/e;->b()Lokhttp3/ae;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ae;->a()Lokhttp3/a;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/a;->a()Lokhttp3/t;

    move-result-object v0

    iget-object v1, p0, Lokhttp3/internal/connection/d;->c:Lokhttp3/a;

    invoke-virtual {v1}, Lokhttp3/a;->a()Lokhttp3/t;

    move-result-object v1

    invoke-static {v0, v1}, Lokhttp3/internal/c;->a(Lokhttp3/t;Lokhttp3/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public a(Lokhttp3/x;Lokhttp3/u$a;Z)Lokhttp3/internal/b/c;
    .locals 7

    .line 80
    invoke-interface {p2}, Lokhttp3/u$a;->b()I

    move-result v1

    .line 81
    invoke-interface {p2}, Lokhttp3/u$a;->c()I

    move-result v2

    .line 82
    invoke-interface {p2}, Lokhttp3/u$a;->d()I

    move-result v3

    .line 83
    invoke-virtual {p1}, Lokhttp3/x;->e()I

    move-result v4

    .line 84
    invoke-virtual {p1}, Lokhttp3/x;->t()Z

    move-result v5

    move-object v0, p0

    move v6, p3

    .line 87
    :try_start_0
    invoke-direct/range {v0 .. v6}, Lokhttp3/internal/connection/d;->a(IIIIZZ)Lokhttp3/internal/connection/e;

    move-result-object p3

    .line 89
    invoke-virtual {p3, p1, p2}, Lokhttp3/internal/connection/e;->a(Lokhttp3/x;Lokhttp3/u$a;)Lokhttp3/internal/b/c;

    move-result-object p1
    :try_end_0
    .catch Lokhttp3/internal/connection/RouteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 94
    invoke-virtual {p0}, Lokhttp3/internal/connection/d;->b()V

    .line 95
    new-instance p2, Lokhttp3/internal/connection/RouteException;

    invoke-direct {p2, p1}, Lokhttp3/internal/connection/RouteException;-><init>(Ljava/io/IOException;)V

    throw p2

    :catch_1
    move-exception p1

    .line 91
    invoke-virtual {p0}, Lokhttp3/internal/connection/d;->b()V

    .line 92
    throw p1
.end method

.method a()Lokhttp3/internal/connection/e;
    .locals 1

    .line 250
    sget-boolean v0, Lokhttp3/internal/connection/d;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 251
    :cond_1
    :goto_0
    iget-object v0, p0, Lokhttp3/internal/connection/d;->i:Lokhttp3/internal/connection/e;

    return-object v0
.end method

.method b()V
    .locals 2

    .line 255
    sget-boolean v0, Lokhttp3/internal/connection/d;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 256
    :cond_1
    :goto_0
    iget-object v0, p0, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    monitor-enter v0

    const/4 v1, 0x1

    .line 257
    :try_start_0
    iput-boolean v1, p0, Lokhttp3/internal/connection/d;->j:Z

    .line 258
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method c()Z
    .locals 2

    .line 263
    iget-object v0, p0, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    monitor-enter v0

    .line 264
    :try_start_0
    iget-boolean v1, p0, Lokhttp3/internal/connection/d;->j:Z

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 265
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method d()Z
    .locals 2

    .line 270
    iget-object v0, p0, Lokhttp3/internal/connection/d;->d:Lokhttp3/internal/connection/f;

    monitor-enter v0

    .line 271
    :try_start_0
    invoke-direct {p0}, Lokhttp3/internal/connection/d;->e()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lokhttp3/internal/connection/d;->g:Lokhttp3/internal/connection/h$a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lokhttp3/internal/connection/d;->g:Lokhttp3/internal/connection/h$a;

    .line 272
    invoke-virtual {v1}, Lokhttp3/internal/connection/h$a;->a()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iget-object v1, p0, Lokhttp3/internal/connection/d;->h:Lokhttp3/internal/connection/h;

    .line 273
    invoke-virtual {v1}, Lokhttp3/internal/connection/h;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 274
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
