.class public final Lokhttp3/internal/b/g;
.super Ljava/lang/Object;
.source "RealInterceptorChain.java"

# interfaces
.implements Lokhttp3/u$a;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/u;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lokhttp3/internal/connection/i;

.field private final c:Lokhttp3/internal/connection/c;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:I

.field private final e:Lokhttp3/aa;

.field private final f:Lokhttp3/e;

.field private final g:I

.field private final h:I

.field private final i:I

.field private j:I


# direct methods
.method public constructor <init>(Ljava/util/List;Lokhttp3/internal/connection/i;Lokhttp3/internal/connection/c;ILokhttp3/aa;Lokhttp3/e;III)V
    .locals 0
    .param p3    # Lokhttp3/internal/connection/c;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lokhttp3/u;",
            ">;",
            "Lokhttp3/internal/connection/i;",
            "Lokhttp3/internal/connection/c;",
            "I",
            "Lokhttp3/aa;",
            "Lokhttp3/e;",
            "III)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lokhttp3/internal/b/g;->a:Ljava/util/List;

    .line 55
    iput-object p2, p0, Lokhttp3/internal/b/g;->b:Lokhttp3/internal/connection/i;

    .line 56
    iput-object p3, p0, Lokhttp3/internal/b/g;->c:Lokhttp3/internal/connection/c;

    .line 57
    iput p4, p0, Lokhttp3/internal/b/g;->d:I

    .line 58
    iput-object p5, p0, Lokhttp3/internal/b/g;->e:Lokhttp3/aa;

    .line 59
    iput-object p6, p0, Lokhttp3/internal/b/g;->f:Lokhttp3/e;

    .line 60
    iput p7, p0, Lokhttp3/internal/b/g;->g:I

    .line 61
    iput p8, p0, Lokhttp3/internal/b/g;->h:I

    .line 62
    iput p9, p0, Lokhttp3/internal/b/g;->i:I

    return-void
.end method


# virtual methods
.method public a()Lokhttp3/aa;
    .locals 1

    .line 113
    iget-object v0, p0, Lokhttp3/internal/b/g;->e:Lokhttp3/aa;

    return-object v0
.end method

.method public a(Lokhttp3/aa;)Lokhttp3/ac;
    .locals 2

    .line 117
    iget-object v0, p0, Lokhttp3/internal/b/g;->b:Lokhttp3/internal/connection/i;

    iget-object v1, p0, Lokhttp3/internal/b/g;->c:Lokhttp3/internal/connection/c;

    invoke-virtual {p0, p1, v0, v1}, Lokhttp3/internal/b/g;->a(Lokhttp3/aa;Lokhttp3/internal/connection/i;Lokhttp3/internal/connection/c;)Lokhttp3/ac;

    move-result-object p1

    return-object p1
.end method

.method public a(Lokhttp3/aa;Lokhttp3/internal/connection/i;Lokhttp3/internal/connection/c;)Lokhttp3/ac;
    .locals 12
    .param p3    # Lokhttp3/internal/connection/c;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 122
    iget v0, p0, Lokhttp3/internal/b/g;->d:I

    iget-object v1, p0, Lokhttp3/internal/b/g;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 124
    iget v0, p0, Lokhttp3/internal/b/g;->j:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lokhttp3/internal/b/g;->j:I

    .line 127
    iget-object v0, p0, Lokhttp3/internal/b/g;->c:Lokhttp3/internal/connection/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lokhttp3/internal/b/g;->c:Lokhttp3/internal/connection/c;

    invoke-virtual {v0}, Lokhttp3/internal/connection/c;->a()Lokhttp3/internal/connection/e;

    move-result-object v0

    invoke-virtual {p1}, Lokhttp3/aa;->a()Lokhttp3/t;

    move-result-object v2

    invoke-virtual {v0, v2}, Lokhttp3/internal/connection/e;->a(Lokhttp3/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 128
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "network interceptor "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lokhttp3/internal/b/g;->a:Ljava/util/List;

    iget v0, p0, Lokhttp3/internal/b/g;->d:I

    sub-int/2addr v0, v1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, " must retain the same host and port"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 133
    :cond_1
    :goto_0
    iget-object v0, p0, Lokhttp3/internal/b/g;->c:Lokhttp3/internal/connection/c;

    if-eqz v0, :cond_3

    iget v0, p0, Lokhttp3/internal/b/g;->j:I

    if-gt v0, v1, :cond_2

    goto :goto_1

    .line 134
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "network interceptor "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lokhttp3/internal/b/g;->a:Ljava/util/List;

    iget v0, p0, Lokhttp3/internal/b/g;->d:I

    sub-int/2addr v0, v1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, " must call proceed() exactly once"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 139
    :cond_3
    :goto_1
    new-instance v0, Lokhttp3/internal/b/g;

    iget-object v3, p0, Lokhttp3/internal/b/g;->a:Ljava/util/List;

    iget v2, p0, Lokhttp3/internal/b/g;->d:I

    add-int/lit8 v6, v2, 0x1

    iget-object v8, p0, Lokhttp3/internal/b/g;->f:Lokhttp3/e;

    iget v9, p0, Lokhttp3/internal/b/g;->g:I

    iget v10, p0, Lokhttp3/internal/b/g;->h:I

    iget v11, p0, Lokhttp3/internal/b/g;->i:I

    move-object v2, v0

    move-object v4, p2

    move-object v5, p3

    move-object v7, p1

    invoke-direct/range {v2 .. v11}, Lokhttp3/internal/b/g;-><init>(Ljava/util/List;Lokhttp3/internal/connection/i;Lokhttp3/internal/connection/c;ILokhttp3/aa;Lokhttp3/e;III)V

    .line 141
    iget-object p1, p0, Lokhttp3/internal/b/g;->a:Ljava/util/List;

    iget p2, p0, Lokhttp3/internal/b/g;->d:I

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lokhttp3/u;

    .line 142
    invoke-interface {p1, v0}, Lokhttp3/u;->a(Lokhttp3/u$a;)Lokhttp3/ac;

    move-result-object p2

    if-eqz p3, :cond_5

    .line 145
    iget p3, p0, Lokhttp3/internal/b/g;->d:I

    add-int/2addr p3, v1

    iget-object v2, p0, Lokhttp3/internal/b/g;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p3, v2, :cond_5

    iget p3, v0, Lokhttp3/internal/b/g;->j:I

    if-ne p3, v1, :cond_4

    goto :goto_2

    .line 146
    :cond_4
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "network interceptor "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " must call proceed() exactly once"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_5
    :goto_2
    if-eqz p2, :cond_7

    .line 155
    invoke-virtual {p2}, Lokhttp3/ac;->h()Lokhttp3/ad;

    move-result-object p3

    if-eqz p3, :cond_6

    return-object p2

    .line 156
    :cond_6
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "interceptor "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " returned a response with no body"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 152
    :cond_7
    new-instance p2, Ljava/lang/NullPointerException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "interceptor "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " returned null"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 122
    :cond_8
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public b()I
    .locals 1

    .line 70
    iget v0, p0, Lokhttp3/internal/b/g;->g:I

    return v0
.end method

.method public c()I
    .locals 1

    .line 80
    iget v0, p0, Lokhttp3/internal/b/g;->h:I

    return v0
.end method

.method public d()I
    .locals 1

    .line 90
    iget v0, p0, Lokhttp3/internal/b/g;->i:I

    return v0
.end method

.method public e()Lokhttp3/internal/connection/i;
    .locals 1

    .line 100
    iget-object v0, p0, Lokhttp3/internal/b/g;->b:Lokhttp3/internal/connection/i;

    return-object v0
.end method

.method public f()Lokhttp3/internal/connection/c;
    .locals 1

    .line 104
    iget-object v0, p0, Lokhttp3/internal/b/g;->c:Lokhttp3/internal/connection/c;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lokhttp3/internal/b/g;->c:Lokhttp3/internal/connection/c;

    return-object v0

    .line 104
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
