.class public final Lio/reactivex/i/d;
.super Ljava/lang/Object;
.source "Observables.kt"


# static fields
.field public static final a:Lio/reactivex/i/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lio/reactivex/i/d;

    invoke-direct {v0}, Lio/reactivex/i/d;-><init>()V

    sput-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/o<",
            "TT1;>;",
            "Lio/reactivex/o<",
            "TT2;>;)",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "TT1;TT2;>;>;"
        }
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p1, Lio/reactivex/s;

    check-cast p2, Lio/reactivex/s;

    .line 29
    sget-object v0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d$a;

    check-cast v0, Lio/reactivex/c/c;

    .line 28
    invoke-static {p1, p2, v0}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/o;Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/o<",
            "TT1;>;",
            "Lio/reactivex/o<",
            "TT2;>;",
            "Lio/reactivex/o<",
            "TT3;>;)",
            "Lio/reactivex/o<",
            "Lkotlin/p<",
            "TT1;TT2;TT3;>;>;"
        }
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    check-cast p1, Lio/reactivex/s;

    check-cast p2, Lio/reactivex/s;

    check-cast p3, Lio/reactivex/s;

    .line 44
    sget-object v0, Lio/reactivex/i/d$b;->a:Lio/reactivex/i/d$b;

    check-cast v0, Lio/reactivex/c/i;

    .line 43
    invoke-static {p1, p2, p3, v0}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/i;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/o<",
            "TT1;>;",
            "Lio/reactivex/o<",
            "TT2;>;)",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "TT1;TT2;>;>;"
        }
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    check-cast p1, Lio/reactivex/s;

    check-cast p2, Lio/reactivex/s;

    .line 116
    sget-object v0, Lio/reactivex/i/d$c;->a:Lio/reactivex/i/d$c;

    check-cast v0, Lio/reactivex/c/c;

    .line 115
    invoke-static {p1, p2, v0}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
