.class public final Lio/reactivex/i/e;
.super Ljava/lang/Object;
.source "Singles.kt"


# static fields
.field public static final a:Lio/reactivex/i/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lio/reactivex/i/e;

    invoke-direct {v0}, Lio/reactivex/i/e;-><init>()V

    sput-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lio/reactivex/aa;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/aa;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/aa<",
            "TT;>;",
            "Lio/reactivex/aa<",
            "TU;>;)",
            "Lio/reactivex/w<",
            "Lkotlin/k<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lio/reactivex/i/e$a;->a:Lio/reactivex/i/e$a;

    check-cast v0, Lio/reactivex/c/c;

    invoke-static {p1, p2, v0}, Lio/reactivex/w;->a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/c/c;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "Single.zip(s1, s2, BiFun\u2026on { t, u -> Pair(t,u) })"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lio/reactivex/aa;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/aa;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/aa;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/aa<",
            "TT1;>;",
            "Lio/reactivex/aa<",
            "TT2;>;",
            "Lio/reactivex/aa<",
            "TT3;>;)",
            "Lio/reactivex/w<",
            "Lkotlin/p<",
            "TT1;TT2;TT3;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lio/reactivex/i/e$b;->a:Lio/reactivex/i/e$b;

    check-cast v0, Lio/reactivex/c/i;

    invoke-static {p1, p2, p3, v0}, Lio/reactivex/w;->a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/c/i;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "Single.zip(s1, s2, s3, F\u2026t3 -> Triple(t1,t2,t3) })"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
