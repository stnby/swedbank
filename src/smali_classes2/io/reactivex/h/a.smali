.class public abstract Lio/reactivex/h/a;
.super Lio/reactivex/h;
.source "FlowableProcessor.java"

# interfaces
.implements Lio/reactivex/i;
.implements Lorg/reactivestreams/Processor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/h<",
        "TT;>;",
        "Lio/reactivex/i<",
        "TT;>;",
        "Lorg/reactivestreams/Processor<",
        "TT;TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Lio/reactivex/h;-><init>()V

    return-void
.end method


# virtual methods
.method public final g()Lio/reactivex/h/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h/a<",
            "TT;>;"
        }
    .end annotation

    .line 74
    instance-of v0, p0, Lio/reactivex/h/b;

    if-eqz v0, :cond_0

    return-object p0

    .line 77
    :cond_0
    new-instance v0, Lio/reactivex/h/b;

    invoke-direct {v0, p0}, Lio/reactivex/h/b;-><init>(Lio/reactivex/h/a;)V

    return-object v0
.end method
