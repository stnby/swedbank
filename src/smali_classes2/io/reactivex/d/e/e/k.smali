.class public final Lio/reactivex/d/e/e/k;
.super Lio/reactivex/j;
.source "ObservableElementAtMaybe.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/j<",
        "TT;>;",
        "Lio/reactivex/d/c/c<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/s<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:J


# direct methods
.method public constructor <init>(Lio/reactivex/s;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;J)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Lio/reactivex/j;-><init>()V

    .line 26
    iput-object p1, p0, Lio/reactivex/d/e/e/k;->a:Lio/reactivex/s;

    .line 27
    iput-wide p2, p0, Lio/reactivex/d/e/e/k;->b:J

    return-void
.end method


# virtual methods
.method public b(Lio/reactivex/l;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/l<",
            "-TT;>;)V"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lio/reactivex/d/e/e/k;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/k$a;

    iget-wide v2, p0, Lio/reactivex/d/e/e/k;->b:J

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/e/k$a;-><init>(Lio/reactivex/l;J)V

    invoke-interface {v0, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method

.method public w_()Lio/reactivex/o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .line 37
    new-instance v6, Lio/reactivex/d/e/e/j;

    iget-object v1, p0, Lio/reactivex/d/e/e/k;->a:Lio/reactivex/s;

    iget-wide v2, p0, Lio/reactivex/d/e/e/k;->b:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/e/j;-><init>(Lio/reactivex/s;JLjava/lang/Object;Z)V

    invoke-static {v6}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
