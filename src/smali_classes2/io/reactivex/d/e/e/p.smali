.class public final Lio/reactivex/d/e/e/p;
.super Lio/reactivex/d/e/e/a;
.source "ObservableFlatMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/p$a;,
        Lio/reactivex/d/e/e/p$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TU;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TU;>;>;"
        }
    .end annotation
.end field

.field final c:Z

.field final d:I

.field final e:I


# direct methods
.method public constructor <init>(Lio/reactivex/s;Lio/reactivex/c/h;ZII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TU;>;>;ZII)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 42
    iput-object p2, p0, Lio/reactivex/d/e/e/p;->b:Lio/reactivex/c/h;

    .line 43
    iput-boolean p3, p0, Lio/reactivex/d/e/e/p;->c:Z

    .line 44
    iput p4, p0, Lio/reactivex/d/e/e/p;->d:I

    .line 45
    iput p5, p0, Lio/reactivex/d/e/e/p;->e:I

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TU;>;)V"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lio/reactivex/d/e/e/p;->a:Lio/reactivex/s;

    iget-object v1, p0, Lio/reactivex/d/e/e/p;->b:Lio/reactivex/c/h;

    invoke-static {v0, p1, v1}, Lio/reactivex/d/e/e/aq;->a(Lio/reactivex/s;Lio/reactivex/u;Lio/reactivex/c/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/e/p;->a:Lio/reactivex/s;

    new-instance v7, Lio/reactivex/d/e/e/p$b;

    iget-object v3, p0, Lio/reactivex/d/e/e/p;->b:Lio/reactivex/c/h;

    iget-boolean v4, p0, Lio/reactivex/d/e/e/p;->c:Z

    iget v5, p0, Lio/reactivex/d/e/e/p;->d:I

    iget v6, p0, Lio/reactivex/d/e/e/p;->e:I

    move-object v1, v7

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lio/reactivex/d/e/e/p$b;-><init>(Lio/reactivex/u;Lio/reactivex/c/h;ZII)V

    invoke-interface {v0, v7}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method
