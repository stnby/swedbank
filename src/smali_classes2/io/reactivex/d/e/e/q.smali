.class public final Lio/reactivex/d/e/e/q;
.super Lio/reactivex/d/e/e/a;
.source "ObservableFlatMapCompletable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/s;Lio/reactivex/c/h;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;Z)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 42
    iput-object p2, p0, Lio/reactivex/d/e/e/q;->b:Lio/reactivex/c/h;

    .line 43
    iput-boolean p3, p0, Lio/reactivex/d/e/e/q;->c:Z

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lio/reactivex/d/e/e/q;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/q$a;

    iget-object v2, p0, Lio/reactivex/d/e/e/q;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/e/q;->c:Z

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/e/q$a;-><init>(Lio/reactivex/u;Lio/reactivex/c/h;Z)V

    invoke-interface {v0, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method
