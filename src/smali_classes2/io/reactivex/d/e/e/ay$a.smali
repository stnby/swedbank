.class final Lio/reactivex/d/e/e/ay$a;
.super Ljava/lang/Object;
.source "ObservableSwitchIfEmpty.java"

# interfaces
.implements Lio/reactivex/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/e/ay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/u<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/s<",
            "+TT;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/d/a/g;

.field d:Z


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;",
            "Lio/reactivex/s<",
            "+TT;>;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lio/reactivex/d/e/e/ay$a;->a:Lio/reactivex/u;

    .line 43
    iput-object p2, p0, Lio/reactivex/d/e/e/ay$a;->b:Lio/reactivex/s;

    const/4 p1, 0x1

    .line 44
    iput-boolean p1, p0, Lio/reactivex/d/e/e/ay$a;->d:Z

    .line 45
    new-instance p1, Lio/reactivex/d/a/g;

    invoke-direct {p1}, Lio/reactivex/d/a/g;-><init>()V

    iput-object p1, p0, Lio/reactivex/d/e/e/ay$a;->c:Lio/reactivex/d/a/g;

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/b/c;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lio/reactivex/d/e/e/ay$a;->c:Lio/reactivex/d/a/g;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/g;->a(Lio/reactivex/b/c;)Z

    return-void
.end method

.method public onComplete()V
    .locals 1

    .line 68
    iget-boolean v0, p0, Lio/reactivex/d/e/e/ay$a;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 69
    iput-boolean v0, p0, Lio/reactivex/d/e/e/ay$a;->d:Z

    .line 70
    iget-object v0, p0, Lio/reactivex/d/e/e/ay$a;->b:Lio/reactivex/s;

    invoke-interface {v0, p0}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    goto :goto_0

    .line 72
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/e/ay$a;->a:Lio/reactivex/u;

    invoke-interface {v0}, Lio/reactivex/u;->onComplete()V

    :goto_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lio/reactivex/d/e/e/ay$a;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 55
    iget-boolean v0, p0, Lio/reactivex/d/e/e/ay$a;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 56
    iput-boolean v0, p0, Lio/reactivex/d/e/e/ay$a;->d:Z

    .line 58
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/e/ay$a;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V

    return-void
.end method
