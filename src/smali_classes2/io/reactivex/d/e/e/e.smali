.class public final Lio/reactivex/d/e/e/e;
.super Lio/reactivex/d/e/e/a;
.source "ObservableDebounceTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/e$a;,
        Lio/reactivex/d/e/e/e$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/v;


# direct methods
.method public constructor <init>(Lio/reactivex/s;JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/v;",
            ")V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 33
    iput-wide p2, p0, Lio/reactivex/d/e/e/e;->b:J

    .line 34
    iput-object p4, p0, Lio/reactivex/d/e/e/e;->c:Ljava/util/concurrent/TimeUnit;

    .line 35
    iput-object p5, p0, Lio/reactivex/d/e/e/e;->d:Lio/reactivex/v;

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lio/reactivex/d/e/e/e;->a:Lio/reactivex/s;

    new-instance v7, Lio/reactivex/d/e/e/e$b;

    new-instance v2, Lio/reactivex/f/c;

    invoke-direct {v2, p1}, Lio/reactivex/f/c;-><init>(Lio/reactivex/u;)V

    iget-wide v3, p0, Lio/reactivex/d/e/e/e;->b:J

    iget-object v5, p0, Lio/reactivex/d/e/e/e;->c:Ljava/util/concurrent/TimeUnit;

    iget-object p1, p0, Lio/reactivex/d/e/e/e;->d:Lio/reactivex/v;

    .line 42
    invoke-virtual {p1}, Lio/reactivex/v;->a()Lio/reactivex/v$b;

    move-result-object v6

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lio/reactivex/d/e/e/e$b;-><init>(Lio/reactivex/u;JLjava/util/concurrent/TimeUnit;Lio/reactivex/v$b;)V

    .line 40
    invoke-interface {v0, v7}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method
