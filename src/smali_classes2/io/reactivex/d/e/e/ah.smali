.class public final Lio/reactivex/d/e/e/ah;
.super Lio/reactivex/d/e/e/a;
.source "ObservableObserveOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/ah$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/v;

.field final c:Z

.field final d:I


# direct methods
.method public constructor <init>(Lio/reactivex/s;Lio/reactivex/v;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;",
            "Lio/reactivex/v;",
            "ZI)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 33
    iput-object p2, p0, Lio/reactivex/d/e/e/ah;->b:Lio/reactivex/v;

    .line 34
    iput-boolean p3, p0, Lio/reactivex/d/e/e/ah;->c:Z

    .line 35
    iput p4, p0, Lio/reactivex/d/e/e/ah;->d:I

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lio/reactivex/d/e/e/ah;->b:Lio/reactivex/v;

    instance-of v0, v0, Lio/reactivex/d/g/n;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lio/reactivex/d/e/e/ah;->a:Lio/reactivex/s;

    invoke-interface {v0, p1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    goto :goto_0

    .line 43
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/e/ah;->b:Lio/reactivex/v;

    invoke-virtual {v0}, Lio/reactivex/v;->a()Lio/reactivex/v$b;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lio/reactivex/d/e/e/ah;->a:Lio/reactivex/s;

    new-instance v2, Lio/reactivex/d/e/e/ah$a;

    iget-boolean v3, p0, Lio/reactivex/d/e/e/ah;->c:Z

    iget v4, p0, Lio/reactivex/d/e/e/ah;->d:I

    invoke-direct {v2, p1, v0, v3, v4}, Lio/reactivex/d/e/e/ah$a;-><init>(Lio/reactivex/u;Lio/reactivex/v$b;ZI)V

    invoke-interface {v1, v2}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    :goto_0
    return-void
.end method
