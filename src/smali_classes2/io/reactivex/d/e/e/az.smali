.class public final Lio/reactivex/d/e/e/az;
.super Lio/reactivex/d/e/e/a;
.source "ObservableSwitchMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/az$a;,
        Lio/reactivex/d/e/e/az$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TR;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:Z


# direct methods
.method public constructor <init>(Lio/reactivex/s;Lio/reactivex/c/h;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TR;>;>;IZ)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 39
    iput-object p2, p0, Lio/reactivex/d/e/e/az;->b:Lio/reactivex/c/h;

    .line 40
    iput p3, p0, Lio/reactivex/d/e/e/az;->c:I

    .line 41
    iput-boolean p4, p0, Lio/reactivex/d/e/e/az;->d:Z

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TR;>;)V"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lio/reactivex/d/e/e/az;->a:Lio/reactivex/s;

    iget-object v1, p0, Lio/reactivex/d/e/e/az;->b:Lio/reactivex/c/h;

    invoke-static {v0, p1, v1}, Lio/reactivex/d/e/e/aq;->a(Lio/reactivex/s;Lio/reactivex/u;Lio/reactivex/c/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/e/az;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/az$b;

    iget-object v2, p0, Lio/reactivex/d/e/e/az;->b:Lio/reactivex/c/h;

    iget v3, p0, Lio/reactivex/d/e/e/az;->c:I

    iget-boolean v4, p0, Lio/reactivex/d/e/e/az;->d:Z

    invoke-direct {v1, p1, v2, v3, v4}, Lio/reactivex/d/e/e/az$b;-><init>(Lio/reactivex/u;Lio/reactivex/c/h;IZ)V

    invoke-interface {v0, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method
