.class public final Lio/reactivex/d/e/e/r;
.super Lio/reactivex/b;
.source "ObservableFlatMapCompletableCompletable.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/b;",
        "Lio/reactivex/d/c/c<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/s<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/s;Lio/reactivex/c/h;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;Z)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Lio/reactivex/b;-><init>()V

    .line 42
    iput-object p1, p0, Lio/reactivex/d/e/e/r;->a:Lio/reactivex/s;

    .line 43
    iput-object p2, p0, Lio/reactivex/d/e/e/r;->b:Lio/reactivex/c/h;

    .line 44
    iput-boolean p3, p0, Lio/reactivex/d/e/e/r;->c:Z

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/d;)V
    .locals 4

    .line 49
    iget-object v0, p0, Lio/reactivex/d/e/e/r;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/r$a;

    iget-object v2, p0, Lio/reactivex/d/e/e/r;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/e/r;->c:Z

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/e/r$a;-><init>(Lio/reactivex/d;Lio/reactivex/c/h;Z)V

    invoke-interface {v0, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method

.method public w_()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .line 54
    new-instance v0, Lio/reactivex/d/e/e/q;

    iget-object v1, p0, Lio/reactivex/d/e/e/r;->a:Lio/reactivex/s;

    iget-object v2, p0, Lio/reactivex/d/e/e/r;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/e/r;->c:Z

    invoke-direct {v0, v1, v2, v3}, Lio/reactivex/d/e/e/q;-><init>(Lio/reactivex/s;Lio/reactivex/c/h;Z)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
