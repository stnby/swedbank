.class final Lio/reactivex/d/e/e/az$a;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableSwitchMap.java"

# interfaces
.implements Lio/reactivex/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/e/az;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference<",
        "Lio/reactivex/b/c;",
        ">;",
        "Lio/reactivex/u<",
        "TR;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/e/e/az$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/e/az$b<",
            "TT;TR;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:I

.field volatile d:Lio/reactivex/d/c/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/c/i<",
            "TR;>;"
        }
    .end annotation
.end field

.field volatile e:Z


# direct methods
.method constructor <init>(Lio/reactivex/d/e/e/az$b;JI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/e/e/az$b<",
            "TT;TR;>;JI)V"
        }
    .end annotation

    .line 338
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 339
    iput-object p1, p0, Lio/reactivex/d/e/e/az$a;->a:Lio/reactivex/d/e/e/az$b;

    .line 340
    iput-wide p2, p0, Lio/reactivex/d/e/e/az$a;->b:J

    .line 341
    iput p4, p0, Lio/reactivex/d/e/e/az$a;->c:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 392
    invoke-static {p0}, Lio/reactivex/d/a/c;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 2

    .line 346
    invoke-static {p0, p1}, Lio/reactivex/d/a/c;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/c;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 347
    instance-of v0, p1, Lio/reactivex/d/c/d;

    if-eqz v0, :cond_1

    .line 349
    check-cast p1, Lio/reactivex/d/c/d;

    const/4 v0, 0x7

    .line 351
    invoke-interface {p1, v0}, Lio/reactivex/d/c/d;->a(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 353
    iput-object p1, p0, Lio/reactivex/d/e/e/az$a;->d:Lio/reactivex/d/c/i;

    .line 354
    iput-boolean v1, p0, Lio/reactivex/d/e/e/az$a;->e:Z

    .line 355
    iget-object p1, p0, Lio/reactivex/d/e/e/az$a;->a:Lio/reactivex/d/e/e/az$b;

    invoke-virtual {p1}, Lio/reactivex/d/e/e/az$b;->d()V

    return-void

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 359
    iput-object p1, p0, Lio/reactivex/d/e/e/az$a;->d:Lio/reactivex/d/c/i;

    return-void

    .line 364
    :cond_1
    new-instance p1, Lio/reactivex/d/f/c;

    iget v0, p0, Lio/reactivex/d/e/e/az$a;->c:I

    invoke-direct {p1, v0}, Lio/reactivex/d/f/c;-><init>(I)V

    iput-object p1, p0, Lio/reactivex/d/e/e/az$a;->d:Lio/reactivex/d/c/i;

    :cond_2
    return-void
.end method

.method public onComplete()V
    .locals 4

    .line 385
    iget-wide v0, p0, Lio/reactivex/d/e/e/az$a;->b:J

    iget-object v2, p0, Lio/reactivex/d/e/e/az$a;->a:Lio/reactivex/d/e/e/az$b;

    iget-wide v2, v2, Lio/reactivex/d/e/e/az$b;->k:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 386
    iput-boolean v0, p0, Lio/reactivex/d/e/e/az$a;->e:Z

    .line 387
    iget-object v0, p0, Lio/reactivex/d/e/e/az$a;->a:Lio/reactivex/d/e/e/az$b;

    invoke-virtual {v0}, Lio/reactivex/d/e/e/az$b;->d()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 380
    iget-object v0, p0, Lio/reactivex/d/e/e/az$a;->a:Lio/reactivex/d/e/e/az$b;

    invoke-virtual {v0, p0, p1}, Lio/reactivex/d/e/e/az$b;->a(Lio/reactivex/d/e/e/az$a;Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .line 370
    iget-wide v0, p0, Lio/reactivex/d/e/e/az$a;->b:J

    iget-object v2, p0, Lio/reactivex/d/e/e/az$a;->a:Lio/reactivex/d/e/e/az$b;

    iget-wide v2, v2, Lio/reactivex/d/e/e/az$b;->k:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 372
    iget-object v0, p0, Lio/reactivex/d/e/e/az$a;->d:Lio/reactivex/d/c/i;

    invoke-interface {v0, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    .line 374
    :cond_0
    iget-object p1, p0, Lio/reactivex/d/e/e/az$a;->a:Lio/reactivex/d/e/e/az$b;

    invoke-virtual {p1}, Lio/reactivex/d/e/e/az$b;->d()V

    :cond_1
    return-void
.end method
