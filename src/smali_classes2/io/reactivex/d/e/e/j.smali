.class public final Lio/reactivex/d/e/e/j;
.super Lio/reactivex/d/e/e/a;
.source "ObservableElementAt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final d:Z


# direct methods
.method public constructor <init>(Lio/reactivex/s;JLjava/lang/Object;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;JTT;Z)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 30
    iput-wide p2, p0, Lio/reactivex/d/e/e/j;->b:J

    .line 31
    iput-object p4, p0, Lio/reactivex/d/e/e/j;->c:Ljava/lang/Object;

    .line 32
    iput-boolean p5, p0, Lio/reactivex/d/e/e/j;->d:Z

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lio/reactivex/d/e/e/j;->a:Lio/reactivex/s;

    new-instance v7, Lio/reactivex/d/e/e/j$a;

    iget-wide v3, p0, Lio/reactivex/d/e/e/j;->b:J

    iget-object v5, p0, Lio/reactivex/d/e/e/j;->c:Ljava/lang/Object;

    iget-boolean v6, p0, Lio/reactivex/d/e/e/j;->d:Z

    move-object v1, v7

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lio/reactivex/d/e/e/j$a;-><init>(Lio/reactivex/u;JLjava/lang/Object;Z)V

    invoke-interface {v0, v7}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method
