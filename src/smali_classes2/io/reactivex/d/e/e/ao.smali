.class public final Lio/reactivex/d/e/e/ao;
.super Lio/reactivex/d/e/e/a;
.source "ObservableRetryPredicate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/ao$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/k<",
            "-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field final c:J


# direct methods
.method public constructor <init>(Lio/reactivex/o;JLio/reactivex/c/k;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "TT;>;J",
            "Lio/reactivex/c/k<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 31
    iput-object p4, p0, Lio/reactivex/d/e/e/ao;->b:Lio/reactivex/c/k;

    .line 32
    iput-wide p2, p0, Lio/reactivex/d/e/e/ao;->c:J

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 37
    new-instance v5, Lio/reactivex/d/a/g;

    invoke-direct {v5}, Lio/reactivex/d/a/g;-><init>()V

    .line 38
    invoke-interface {p1, v5}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 40
    new-instance v7, Lio/reactivex/d/e/e/ao$a;

    iget-wide v2, p0, Lio/reactivex/d/e/e/ao;->c:J

    iget-object v4, p0, Lio/reactivex/d/e/e/ao;->b:Lio/reactivex/c/k;

    iget-object v6, p0, Lio/reactivex/d/e/e/ao;->a:Lio/reactivex/s;

    move-object v0, v7

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/e/ao$a;-><init>(Lio/reactivex/u;JLio/reactivex/c/k;Lio/reactivex/d/a/g;Lio/reactivex/s;)V

    .line 41
    invoke-virtual {v7}, Lio/reactivex/d/e/e/ao$a;->a()V

    return-void
.end method
