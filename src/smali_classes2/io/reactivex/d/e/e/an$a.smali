.class final Lio/reactivex/d/e/e/an$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableRepeatWhen.java"

# interfaces
.implements Lio/reactivex/b/c;
.implements Lio/reactivex/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/e/an;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/an$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/c;",
        "Lio/reactivex/u<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field final c:Lio/reactivex/d/j/b;

.field final d:Lio/reactivex/k/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/f<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lio/reactivex/d/e/e/an$a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/e/an$a<",
            "TT;>.a;"
        }
    .end annotation
.end field

.field final f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lio/reactivex/b/c;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lio/reactivex/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/s<",
            "TT;>;"
        }
    .end annotation
.end field

.field volatile h:Z


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/k/f;Lio/reactivex/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;",
            "Lio/reactivex/k/f<",
            "Ljava/lang/Object;",
            ">;",
            "Lio/reactivex/s<",
            "TT;>;)V"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 84
    iput-object p1, p0, Lio/reactivex/d/e/e/an$a;->a:Lio/reactivex/u;

    .line 85
    iput-object p2, p0, Lio/reactivex/d/e/e/an$a;->d:Lio/reactivex/k/f;

    .line 86
    iput-object p3, p0, Lio/reactivex/d/e/e/an$a;->g:Lio/reactivex/s;

    .line 87
    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object p1, p0, Lio/reactivex/d/e/e/an$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 88
    new-instance p1, Lio/reactivex/d/j/b;

    invoke-direct {p1}, Lio/reactivex/d/j/b;-><init>()V

    iput-object p1, p0, Lio/reactivex/d/e/e/an$a;->c:Lio/reactivex/d/j/b;

    .line 89
    new-instance p1, Lio/reactivex/d/e/e/an$a$a;

    invoke-direct {p1, p0}, Lio/reactivex/d/e/e/an$a$a;-><init>(Lio/reactivex/d/e/e/an$a;)V

    iput-object p1, p0, Lio/reactivex/d/e/e/an$a;->e:Lio/reactivex/d/e/e/an$a$a;

    .line 90
    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lio/reactivex/d/e/e/an$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 123
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lio/reactivex/d/a/c;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 124
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->e:Lio/reactivex/d/e/e/an$a$a;

    invoke-static {v0}, Lio/reactivex/d/a/c;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1

    .line 95
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0, p1}, Lio/reactivex/d/a/c;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/c;)Z

    return-void
.end method

.method a(Ljava/lang/Throwable;)V
    .locals 2

    .line 132
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lio/reactivex/d/a/c;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 133
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->a:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/e/an$a;->c:Lio/reactivex/d/j/b;

    invoke-static {v0, p1, p0, v1}, Lio/reactivex/d/j/i;->a(Lio/reactivex/u;Ljava/lang/Throwable;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/b;)V

    return-void
.end method

.method public b()Z
    .locals 1

    .line 118
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/c;

    invoke-static {v0}, Lio/reactivex/d/a/c;->a(Lio/reactivex/b/c;)Z

    move-result v0

    return v0
.end method

.method c()V
    .locals 0

    .line 128
    invoke-virtual {p0}, Lio/reactivex/d/e/e/an$a;->e()V

    return-void
.end method

.method d()V
    .locals 2

    .line 137
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lio/reactivex/d/a/c;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 138
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->a:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/e/an$a;->c:Lio/reactivex/d/j/b;

    invoke-static {v0, p0, v1}, Lio/reactivex/d/j/i;->a(Lio/reactivex/u;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/b;)V

    return-void
.end method

.method e()V
    .locals 1

    .line 142
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_3

    .line 145
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/e/an$a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 149
    :cond_1
    iget-boolean v0, p0, Lio/reactivex/d/e/e/an$a;->h:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 150
    iput-boolean v0, p0, Lio/reactivex/d/e/e/an$a;->h:Z

    .line 151
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->g:Lio/reactivex/s;

    invoke-interface {v0, p0}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    .line 153
    :cond_2
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    return-void
.end method

.method public onComplete()V
    .locals 2

    .line 111
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lio/reactivex/d/a/c;->c(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/c;)Z

    const/4 v0, 0x0

    .line 112
    iput-boolean v0, p0, Lio/reactivex/d/e/e/an$a;->h:Z

    .line 113
    iget-object v1, p0, Lio/reactivex/d/e/e/an$a;->d:Lio/reactivex/k/f;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/k/f;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .line 105
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->e:Lio/reactivex/d/e/e/an$a$a;

    invoke-static {v0}, Lio/reactivex/d/a/c;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 106
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->a:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/e/an$a;->c:Lio/reactivex/d/j/b;

    invoke-static {v0, p1, p0, v1}, Lio/reactivex/d/j/i;->a(Lio/reactivex/u;Ljava/lang/Throwable;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/b;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lio/reactivex/d/e/e/an$a;->a:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/e/an$a;->c:Lio/reactivex/d/j/b;

    invoke-static {v0, p1, p0, v1}, Lio/reactivex/d/j/i;->a(Lio/reactivex/u;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/b;)V

    return-void
.end method
