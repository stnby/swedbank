.class public final Lio/reactivex/d/e/e/am;
.super Lio/reactivex/o;
.source "ObservableRefCount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/am$b;,
        Lio/reactivex/d/e/e/am$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/o<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/e/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/e/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:I

.field final c:J

.field final d:Ljava/util/concurrent/TimeUnit;

.field final e:Lio/reactivex/v;

.field f:Lio/reactivex/d/e/e/am$a;


# direct methods
.method public constructor <init>(Lio/reactivex/e/a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/e/a<",
            "TT;>;)V"
        }
    .end annotation

    .line 48
    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v2, 0x1

    const-wide/16 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/e/am;-><init>(Lio/reactivex/e/a;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V

    return-void
.end method

.method public constructor <init>(Lio/reactivex/e/a;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/e/a<",
            "TT;>;IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/v;",
            ")V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    .line 53
    iput-object p1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    .line 54
    iput p2, p0, Lio/reactivex/d/e/e/am;->b:I

    .line 55
    iput-wide p3, p0, Lio/reactivex/d/e/e/am;->c:J

    .line 56
    iput-object p5, p0, Lio/reactivex/d/e/e/am;->d:Ljava/util/concurrent/TimeUnit;

    .line 57
    iput-object p6, p0, Lio/reactivex/d/e/e/am;->e:Lio/reactivex/v;

    return-void
.end method


# virtual methods
.method a(Lio/reactivex/d/e/e/am$a;)V
    .locals 5

    .line 93
    monitor-enter p0

    .line 94
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    if-eq v0, p1, :cond_0

    goto :goto_1

    .line 97
    :cond_0
    iget-wide v0, p1, Lio/reactivex/d/e/e/am$a;->c:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    .line 98
    iput-wide v0, p1, Lio/reactivex/d/e/e/am$a;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 99
    iget-boolean v0, p1, Lio/reactivex/d/e/e/am$a;->d:Z

    if-nez v0, :cond_1

    goto :goto_0

    .line 102
    :cond_1
    iget-wide v0, p0, Lio/reactivex/d/e/e/am;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 103
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/e/am;->c(Lio/reactivex/d/e/e/am$a;)V

    .line 104
    monitor-exit p0

    return-void

    .line 106
    :cond_2
    new-instance v0, Lio/reactivex/d/a/g;

    invoke-direct {v0}, Lio/reactivex/d/a/g;-><init>()V

    .line 107
    iput-object v0, p1, Lio/reactivex/d/e/e/am$a;->b:Lio/reactivex/b/c;

    .line 108
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    iget-object v1, p0, Lio/reactivex/d/e/e/am;->e:Lio/reactivex/v;

    iget-wide v2, p0, Lio/reactivex/d/e/e/am;->c:J

    iget-object v4, p0, Lio/reactivex/d/e/e/am;->d:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, v2, v3, v4}, Lio/reactivex/v;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/g;->b(Lio/reactivex/b/c;)Z

    return-void

    .line 100
    :cond_3
    :goto_0
    :try_start_1
    monitor-exit p0

    return-void

    .line 95
    :cond_4
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 108
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected a(Lio/reactivex/u;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 66
    monitor-enter p0

    .line 67
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lio/reactivex/d/e/e/am$a;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/am$a;-><init>(Lio/reactivex/d/e/e/am;)V

    .line 70
    iput-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    .line 73
    :cond_0
    iget-wide v1, v0, Lio/reactivex/d/e/e/am$a;->c:J

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    .line 74
    iget-object v3, v0, Lio/reactivex/d/e/e/am$a;->b:Lio/reactivex/b/c;

    if-eqz v3, :cond_1

    .line 75
    iget-object v3, v0, Lio/reactivex/d/e/e/am$a;->b:Lio/reactivex/b/c;

    invoke-interface {v3}, Lio/reactivex/b/c;->a()V

    :cond_1
    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    .line 77
    iput-wide v1, v0, Lio/reactivex/d/e/e/am$a;->c:J

    .line 78
    iget-boolean v3, v0, Lio/reactivex/d/e/e/am$a;->d:Z

    const/4 v4, 0x1

    if-nez v3, :cond_2

    iget v3, p0, Lio/reactivex/d/e/e/am;->b:I

    int-to-long v5, v3

    cmp-long v1, v1, v5

    if-nez v1, :cond_2

    .line 80
    iput-boolean v4, v0, Lio/reactivex/d/e/e/am$a;->d:Z

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    .line 82
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    iget-object v1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    new-instance v2, Lio/reactivex/d/e/e/am$b;

    invoke-direct {v2, p1, p0, v0}, Lio/reactivex/d/e/e/am$b;-><init>(Lio/reactivex/u;Lio/reactivex/d/e/e/am;Lio/reactivex/d/e/e/am$a;)V

    invoke-virtual {v1, v2}, Lio/reactivex/e/a;->d(Lio/reactivex/u;)V

    if-eqz v4, :cond_3

    .line 87
    iget-object p1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    invoke-virtual {p1, v0}, Lio/reactivex/e/a;->d(Lio/reactivex/c/g;)V

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    .line 82
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method b(Lio/reactivex/d/e/e/am$a;)V
    .locals 4

    .line 114
    monitor-enter p0

    .line 115
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    .line 116
    iput-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    .line 117
    iget-object v0, p1, Lio/reactivex/d/e/e/am$a;->b:Lio/reactivex/b/c;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p1, Lio/reactivex/d/e/e/am$a;->b:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 121
    :cond_0
    iget-wide v0, p1, Lio/reactivex/d/e/e/am$a;->c:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p1, Lio/reactivex/d/e/e/am$a;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 122
    iget-object v0, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    instance-of v0, v0, Lio/reactivex/b/c;

    if-eqz v0, :cond_1

    .line 123
    iget-object p1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    check-cast p1, Lio/reactivex/b/c;

    invoke-interface {p1}, Lio/reactivex/b/c;->a()V

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    instance-of v0, v0, Lio/reactivex/d/a/f;

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    check-cast v0, Lio/reactivex/d/a/f;

    invoke-virtual {p1}, Lio/reactivex/d/e/e/am$a;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/b/c;

    invoke-interface {v0, p1}, Lio/reactivex/d/a/f;->a(Lio/reactivex/b/c;)V

    .line 128
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method c(Lio/reactivex/d/e/e/am$a;)V
    .locals 4

    .line 132
    monitor-enter p0

    .line 133
    :try_start_0
    iget-wide v0, p1, Lio/reactivex/d/e/e/am$a;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x0

    .line 134
    iput-object v0, p0, Lio/reactivex/d/e/e/am;->f:Lio/reactivex/d/e/e/am$a;

    .line 135
    invoke-virtual {p1}, Lio/reactivex/d/e/e/am$a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/c;

    .line 136
    invoke-static {p1}, Lio/reactivex/d/a/c;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 138
    iget-object v1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    instance-of v1, v1, Lio/reactivex/b/c;

    if-eqz v1, :cond_0

    .line 139
    iget-object p1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    check-cast p1, Lio/reactivex/b/c;

    invoke-interface {p1}, Lio/reactivex/b/c;->a()V

    goto :goto_0

    .line 140
    :cond_0
    iget-object v1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    instance-of v1, v1, Lio/reactivex/d/a/f;

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 142
    iput-boolean v0, p1, Lio/reactivex/d/e/e/am$a;->e:Z

    goto :goto_0

    .line 144
    :cond_1
    iget-object p1, p0, Lio/reactivex/d/e/e/am;->a:Lio/reactivex/e/a;

    check-cast p1, Lio/reactivex/d/a/f;

    invoke-interface {p1, v0}, Lio/reactivex/d/a/f;->a(Lio/reactivex/b/c;)V

    .line 148
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
