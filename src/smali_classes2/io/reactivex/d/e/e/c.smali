.class public final Lio/reactivex/d/e/e/c;
.super Lio/reactivex/d/e/e/a;
.source "ObservableConcatMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/c$a;,
        Lio/reactivex/d/e/e/c$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TU;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TU;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:Lio/reactivex/d/j/g;


# direct methods
.method public constructor <init>(Lio/reactivex/s;Lio/reactivex/c/h;ILio/reactivex/d/j/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TU;>;>;I",
            "Lio/reactivex/d/j/g;",
            ")V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 39
    iput-object p2, p0, Lio/reactivex/d/e/e/c;->b:Lio/reactivex/c/h;

    .line 40
    iput-object p4, p0, Lio/reactivex/d/e/e/c;->d:Lio/reactivex/d/j/g;

    const/16 p1, 0x8

    .line 41
    invoke-static {p1, p3}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lio/reactivex/d/e/e/c;->c:I

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TU;>;)V"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lio/reactivex/d/e/e/c;->a:Lio/reactivex/s;

    iget-object v1, p0, Lio/reactivex/d/e/e/c;->b:Lio/reactivex/c/h;

    invoke-static {v0, p1, v1}, Lio/reactivex/d/e/e/aq;->a(Lio/reactivex/s;Lio/reactivex/u;Lio/reactivex/c/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/e/c;->d:Lio/reactivex/d/j/g;

    sget-object v1, Lio/reactivex/d/j/g;->a:Lio/reactivex/d/j/g;

    if-ne v0, v1, :cond_1

    .line 52
    new-instance v0, Lio/reactivex/f/c;

    invoke-direct {v0, p1}, Lio/reactivex/f/c;-><init>(Lio/reactivex/u;)V

    .line 53
    iget-object p1, p0, Lio/reactivex/d/e/e/c;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/c$b;

    iget-object v2, p0, Lio/reactivex/d/e/e/c;->b:Lio/reactivex/c/h;

    iget v3, p0, Lio/reactivex/d/e/e/c;->c:I

    invoke-direct {v1, v0, v2, v3}, Lio/reactivex/d/e/e/c$b;-><init>(Lio/reactivex/u;Lio/reactivex/c/h;I)V

    invoke-interface {p1, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    goto :goto_1

    .line 55
    :cond_1
    iget-object v0, p0, Lio/reactivex/d/e/e/c;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/c$a;

    iget-object v2, p0, Lio/reactivex/d/e/e/c;->b:Lio/reactivex/c/h;

    iget v3, p0, Lio/reactivex/d/e/e/c;->c:I

    iget-object v4, p0, Lio/reactivex/d/e/e/c;->d:Lio/reactivex/d/j/g;

    sget-object v5, Lio/reactivex/d/j/g;->c:Lio/reactivex/d/j/g;

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v1, p1, v2, v3, v4}, Lio/reactivex/d/e/e/c$a;-><init>(Lio/reactivex/u;Lio/reactivex/c/h;IZ)V

    invoke-interface {v0, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    :goto_1
    return-void
.end method
