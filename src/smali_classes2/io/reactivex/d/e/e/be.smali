.class public final Lio/reactivex/d/e/e/be;
.super Lio/reactivex/d/e/e/a;
.source "ObservableTimeoutTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/be$d;,
        Lio/reactivex/d/e/e/be$a;,
        Lio/reactivex/d/e/e/be$b;,
        Lio/reactivex/d/e/e/be$e;,
        Lio/reactivex/d/e/e/be$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/v;

.field final e:Lio/reactivex/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/s<",
            "+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/o;JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;Lio/reactivex/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/v;",
            "Lio/reactivex/s<",
            "+TT;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 35
    iput-wide p2, p0, Lio/reactivex/d/e/e/be;->b:J

    .line 36
    iput-object p4, p0, Lio/reactivex/d/e/e/be;->c:Ljava/util/concurrent/TimeUnit;

    .line 37
    iput-object p5, p0, Lio/reactivex/d/e/e/be;->d:Lio/reactivex/v;

    .line 38
    iput-object p6, p0, Lio/reactivex/d/e/e/be;->e:Lio/reactivex/s;

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lio/reactivex/d/e/e/be;->e:Lio/reactivex/s;

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lio/reactivex/d/e/e/be$c;

    iget-wide v5, p0, Lio/reactivex/d/e/e/be;->b:J

    iget-object v7, p0, Lio/reactivex/d/e/e/be;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lio/reactivex/d/e/e/be;->d:Lio/reactivex/v;

    invoke-virtual {v3}, Lio/reactivex/v;->a()Lio/reactivex/v$b;

    move-result-object v8

    move-object v3, v0

    move-object v4, p1

    invoke-direct/range {v3 .. v8}, Lio/reactivex/d/e/e/be$c;-><init>(Lio/reactivex/u;JLjava/util/concurrent/TimeUnit;Lio/reactivex/v$b;)V

    .line 45
    invoke-interface {p1, v0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 46
    invoke-virtual {v0, v1, v2}, Lio/reactivex/d/e/e/be$c;->a(J)V

    .line 47
    iget-object p1, p0, Lio/reactivex/d/e/e/be;->a:Lio/reactivex/s;

    invoke-interface {p1, v0}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Lio/reactivex/d/e/e/be$b;

    iget-wide v5, p0, Lio/reactivex/d/e/e/be;->b:J

    iget-object v7, p0, Lio/reactivex/d/e/e/be;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lio/reactivex/d/e/e/be;->d:Lio/reactivex/v;

    invoke-virtual {v3}, Lio/reactivex/v;->a()Lio/reactivex/v$b;

    move-result-object v8

    iget-object v9, p0, Lio/reactivex/d/e/e/be;->e:Lio/reactivex/s;

    move-object v3, v0

    move-object v4, p1

    invoke-direct/range {v3 .. v9}, Lio/reactivex/d/e/e/be$b;-><init>(Lio/reactivex/u;JLjava/util/concurrent/TimeUnit;Lio/reactivex/v$b;Lio/reactivex/s;)V

    .line 50
    invoke-interface {p1, v0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 51
    invoke-virtual {v0, v1, v2}, Lio/reactivex/d/e/e/be$b;->a(J)V

    .line 52
    iget-object p1, p0, Lio/reactivex/d/e/e/be;->a:Lio/reactivex/s;

    invoke-interface {p1, v0}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    :goto_0
    return-void
.end method
