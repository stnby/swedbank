.class final Lio/reactivex/d/e/e/am$b;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source "ObservableRefCount.java"

# interfaces
.implements Lio/reactivex/b/c;
.implements Lio/reactivex/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/e/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "Lio/reactivex/b/c;",
        "Lio/reactivex/u<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/d/e/e/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/e/am<",
            "TT;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/d/e/e/am$a;

.field d:Lio/reactivex/b/c;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/d/e/e/am;Lio/reactivex/d/e/e/am$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;",
            "Lio/reactivex/d/e/e/am<",
            "TT;>;",
            "Lio/reactivex/d/e/e/am$a;",
            ")V"
        }
    .end annotation

    .line 199
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 200
    iput-object p1, p0, Lio/reactivex/d/e/e/am$b;->a:Lio/reactivex/u;

    .line 201
    iput-object p2, p0, Lio/reactivex/d/e/e/am$b;->b:Lio/reactivex/d/e/e/am;

    .line 202
    iput-object p3, p0, Lio/reactivex/d/e/e/am$b;->c:Lio/reactivex/d/e/e/am$a;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 230
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->d:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 231
    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/e/am$b;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->b:Lio/reactivex/d/e/e/am;

    iget-object v1, p0, Lio/reactivex/d/e/e/am$b;->c:Lio/reactivex/d/e/e/am$a;

    invoke-virtual {v0, v1}, Lio/reactivex/d/e/e/am;->a(Lio/reactivex/d/e/e/am$a;)V

    :cond_0
    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1

    .line 243
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->d:Lio/reactivex/b/c;

    invoke-static {v0, p1}, Lio/reactivex/d/a/c;->a(Lio/reactivex/b/c;Lio/reactivex/b/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iput-object p1, p0, Lio/reactivex/d/e/e/am$b;->d:Lio/reactivex/b/c;

    .line 246
    iget-object p1, p0, Lio/reactivex/d/e/e/am$b;->a:Lio/reactivex/u;

    invoke-interface {p1, p0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .line 238
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->d:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->b()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 222
    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/e/am$b;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->b:Lio/reactivex/d/e/e/am;

    iget-object v1, p0, Lio/reactivex/d/e/e/am$b;->c:Lio/reactivex/d/e/e/am$a;

    invoke-virtual {v0, v1}, Lio/reactivex/d/e/e/am;->b(Lio/reactivex/d/e/e/am$a;)V

    .line 224
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->a:Lio/reactivex/u;

    invoke-interface {v0}, Lio/reactivex/u;->onComplete()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 212
    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/e/am$b;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->b:Lio/reactivex/d/e/e/am;

    iget-object v1, p0, Lio/reactivex/d/e/e/am$b;->c:Lio/reactivex/d/e/e/am$a;

    invoke-virtual {v0, v1}, Lio/reactivex/d/e/e/am;->b(Lio/reactivex/d/e/e/am$a;)V

    .line 214
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 216
    :cond_0
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 207
    iget-object v0, p0, Lio/reactivex/d/e/e/am$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V

    return-void
.end method
