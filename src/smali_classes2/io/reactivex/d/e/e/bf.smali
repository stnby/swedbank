.class public final Lio/reactivex/d/e/e/bf;
.super Lio/reactivex/o;
.source "ObservableTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/bf$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/o<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/v;

.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    .line 28
    iput-wide p1, p0, Lio/reactivex/d/e/e/bf;->b:J

    .line 29
    iput-object p3, p0, Lio/reactivex/d/e/e/bf;->c:Ljava/util/concurrent/TimeUnit;

    .line 30
    iput-object p4, p0, Lio/reactivex/d/e/e/bf;->a:Lio/reactivex/v;

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 35
    new-instance v0, Lio/reactivex/d/e/e/bf$a;

    invoke-direct {v0, p1}, Lio/reactivex/d/e/e/bf$a;-><init>(Lio/reactivex/u;)V

    .line 36
    invoke-interface {p1, v0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 38
    iget-object p1, p0, Lio/reactivex/d/e/e/bf;->a:Lio/reactivex/v;

    iget-wide v1, p0, Lio/reactivex/d/e/e/bf;->b:J

    iget-object v3, p0, Lio/reactivex/d/e/e/bf;->c:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2, v3}, Lio/reactivex/v;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/c;

    move-result-object p1

    .line 40
    invoke-virtual {v0, p1}, Lio/reactivex/d/e/e/bf$a;->a(Lio/reactivex/b/c;)V

    return-void
.end method
