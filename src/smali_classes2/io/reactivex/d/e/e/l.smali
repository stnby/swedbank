.class public final Lio/reactivex/d/e/e/l;
.super Lio/reactivex/w;
.source "ObservableElementAtSingle.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/w<",
        "TT;>;",
        "Lio/reactivex/d/c/c<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/s<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/s;JLjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;JTT;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Lio/reactivex/w;-><init>()V

    .line 30
    iput-object p1, p0, Lio/reactivex/d/e/e/l;->a:Lio/reactivex/s;

    .line 31
    iput-wide p2, p0, Lio/reactivex/d/e/e/l;->b:J

    .line 32
    iput-object p4, p0, Lio/reactivex/d/e/e/l;->c:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public b(Lio/reactivex/y;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/y<",
            "-TT;>;)V"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lio/reactivex/d/e/e/l;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/l$a;

    iget-wide v2, p0, Lio/reactivex/d/e/e/l;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/e/l;->c:Ljava/lang/Object;

    invoke-direct {v1, p1, v2, v3, v4}, Lio/reactivex/d/e/e/l$a;-><init>(Lio/reactivex/y;JLjava/lang/Object;)V

    invoke-interface {v0, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    return-void
.end method

.method public w_()Lio/reactivex/o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .line 42
    new-instance v6, Lio/reactivex/d/e/e/j;

    iget-object v1, p0, Lio/reactivex/d/e/e/l;->a:Lio/reactivex/s;

    iget-wide v2, p0, Lio/reactivex/d/e/e/l;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/e/l;->c:Ljava/lang/Object;

    const/4 v5, 0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/e/j;-><init>(Lio/reactivex/s;JLjava/lang/Object;Z)V

    invoke-static {v6}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
