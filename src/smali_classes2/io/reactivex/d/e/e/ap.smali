.class public final Lio/reactivex/d/e/e/ap;
.super Lio/reactivex/d/e/e/a;
.source "ObservableSampleWithObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/ap$a;,
        Lio/reactivex/d/e/e/ap$b;,
        Lio/reactivex/d/e/e/ap$d;,
        Lio/reactivex/d/e/e/ap$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/e/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/s<",
            "*>;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/s;Lio/reactivex/s;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/s<",
            "TT;>;",
            "Lio/reactivex/s<",
            "*>;Z)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/s;)V

    .line 31
    iput-object p2, p0, Lio/reactivex/d/e/e/ap;->b:Lio/reactivex/s;

    .line 32
    iput-boolean p3, p0, Lio/reactivex/d/e/e/ap;->c:Z

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/u;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    .line 37
    new-instance v0, Lio/reactivex/f/c;

    invoke-direct {v0, p1}, Lio/reactivex/f/c;-><init>(Lio/reactivex/u;)V

    .line 38
    iget-boolean p1, p0, Lio/reactivex/d/e/e/ap;->c:Z

    if-eqz p1, :cond_0

    .line 39
    iget-object p1, p0, Lio/reactivex/d/e/e/ap;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/ap$a;

    iget-object v2, p0, Lio/reactivex/d/e/e/ap;->b:Lio/reactivex/s;

    invoke-direct {v1, v0, v2}, Lio/reactivex/d/e/e/ap$a;-><init>(Lio/reactivex/u;Lio/reactivex/s;)V

    invoke-interface {p1, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    goto :goto_0

    .line 41
    :cond_0
    iget-object p1, p0, Lio/reactivex/d/e/e/ap;->a:Lio/reactivex/s;

    new-instance v1, Lio/reactivex/d/e/e/ap$b;

    iget-object v2, p0, Lio/reactivex/d/e/e/ap;->b:Lio/reactivex/s;

    invoke-direct {v1, v0, v2}, Lio/reactivex/d/e/e/ap$b;-><init>(Lio/reactivex/u;Lio/reactivex/s;)V

    invoke-interface {p1, v1}, Lio/reactivex/s;->d(Lio/reactivex/u;)V

    :goto_0
    return-void
.end method
