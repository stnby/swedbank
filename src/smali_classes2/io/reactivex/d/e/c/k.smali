.class public final Lio/reactivex/d/e/c/k;
.super Lio/reactivex/j;
.source "MaybeFromAction.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/j<",
        "TT;>;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/c/a;


# direct methods
.method public constructor <init>(Lio/reactivex/c/a;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lio/reactivex/j;-><init>()V

    .line 34
    iput-object p1, p0, Lio/reactivex/d/e/c/k;->a:Lio/reactivex/c/a;

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/l<",
            "-TT;>;)V"
        }
    .end annotation

    .line 39
    invoke-static {}, Lio/reactivex/b/d;->a()Lio/reactivex/b/c;

    move-result-object v0

    .line 40
    invoke-interface {p1, v0}, Lio/reactivex/l;->a(Lio/reactivex/b/c;)V

    .line 42
    invoke-interface {v0}, Lio/reactivex/b/c;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 45
    :try_start_0
    iget-object v1, p0, Lio/reactivex/d/e/c/k;->a:Lio/reactivex/c/a;

    invoke-interface {v1}, Lio/reactivex/c/a;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    invoke-interface {v0}, Lio/reactivex/b/c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    invoke-interface {p1}, Lio/reactivex/l;->onComplete()V

    goto :goto_1

    :catch_0
    move-exception v1

    .line 47
    invoke-static {v1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 48
    invoke-interface {v0}, Lio/reactivex/b/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    invoke-interface {p1, v1}, Lio/reactivex/l;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 51
    :cond_0
    invoke-static {v1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_1
    :goto_1
    return-void
.end method

.method public call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lio/reactivex/d/e/c/k;->a:Lio/reactivex/c/a;

    invoke-interface {v0}, Lio/reactivex/c/a;->b()V

    const/4 v0, 0x0

    return-object v0
.end method
