.class public final Lio/reactivex/d/e/c/r;
.super Lio/reactivex/d/e/c/a;
.source "MaybeOnErrorNext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/c/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/c/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-",
            "Ljava/lang/Throwable;",
            "+",
            "Lio/reactivex/n<",
            "+TT;>;>;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/n;Lio/reactivex/c/h;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/n<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-",
            "Ljava/lang/Throwable;",
            "+",
            "Lio/reactivex/n<",
            "+TT;>;>;Z)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0, p1}, Lio/reactivex/d/e/c/a;-><init>(Lio/reactivex/n;)V

    .line 40
    iput-object p2, p0, Lio/reactivex/d/e/c/r;->b:Lio/reactivex/c/h;

    .line 41
    iput-boolean p3, p0, Lio/reactivex/d/e/c/r;->c:Z

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/l;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/l<",
            "-TT;>;)V"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lio/reactivex/d/e/c/r;->a:Lio/reactivex/n;

    new-instance v1, Lio/reactivex/d/e/c/r$a;

    iget-object v2, p0, Lio/reactivex/d/e/c/r;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/c/r;->c:Z

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/c/r$a;-><init>(Lio/reactivex/l;Lio/reactivex/c/h;Z)V

    invoke-interface {v0, v1}, Lio/reactivex/n;->a(Lio/reactivex/l;)V

    return-void
.end method
