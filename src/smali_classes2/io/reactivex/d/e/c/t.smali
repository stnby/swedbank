.class public final Lio/reactivex/d/e/c/t;
.super Lio/reactivex/d/e/c/a;
.source "MaybePeek.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/c/t$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/c/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/g<",
            "-",
            "Lio/reactivex/b/c;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/c/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/g<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final d:Lio/reactivex/c/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/g<",
            "-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lio/reactivex/c/a;

.field final f:Lio/reactivex/c/a;

.field final g:Lio/reactivex/c/a;


# direct methods
.method public constructor <init>(Lio/reactivex/n;Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/n<",
            "TT;>;",
            "Lio/reactivex/c/g<",
            "-",
            "Lio/reactivex/b/c;",
            ">;",
            "Lio/reactivex/c/g<",
            "-TT;>;",
            "Lio/reactivex/c/g<",
            "-",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/c/a;",
            "Lio/reactivex/c/a;",
            "Lio/reactivex/c/a;",
            ")V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1}, Lio/reactivex/d/e/c/a;-><init>(Lio/reactivex/n;)V

    .line 46
    iput-object p2, p0, Lio/reactivex/d/e/c/t;->b:Lio/reactivex/c/g;

    .line 47
    iput-object p3, p0, Lio/reactivex/d/e/c/t;->c:Lio/reactivex/c/g;

    .line 48
    iput-object p4, p0, Lio/reactivex/d/e/c/t;->d:Lio/reactivex/c/g;

    .line 49
    iput-object p5, p0, Lio/reactivex/d/e/c/t;->e:Lio/reactivex/c/a;

    .line 50
    iput-object p6, p0, Lio/reactivex/d/e/c/t;->f:Lio/reactivex/c/a;

    .line 51
    iput-object p7, p0, Lio/reactivex/d/e/c/t;->g:Lio/reactivex/c/a;

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/l<",
            "-TT;>;)V"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lio/reactivex/d/e/c/t;->a:Lio/reactivex/n;

    new-instance v1, Lio/reactivex/d/e/c/t$a;

    invoke-direct {v1, p1, p0}, Lio/reactivex/d/e/c/t$a;-><init>(Lio/reactivex/l;Lio/reactivex/d/e/c/t;)V

    invoke-interface {v0, v1}, Lio/reactivex/n;->a(Lio/reactivex/l;)V

    return-void
.end method
