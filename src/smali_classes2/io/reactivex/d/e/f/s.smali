.class public final Lio/reactivex/d/e/f/s;
.super Lio/reactivex/w;
.source "SingleSubscribeOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/f/s$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/w<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/aa<",
            "+TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/v;


# direct methods
.method public constructor <init>(Lio/reactivex/aa;Lio/reactivex/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/aa<",
            "+TT;>;",
            "Lio/reactivex/v;",
            ")V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Lio/reactivex/w;-><init>()V

    .line 28
    iput-object p1, p0, Lio/reactivex/d/e/f/s;->a:Lio/reactivex/aa;

    .line 29
    iput-object p2, p0, Lio/reactivex/d/e/f/s;->b:Lio/reactivex/v;

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/y;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/y<",
            "-TT;>;)V"
        }
    .end annotation

    .line 34
    new-instance v0, Lio/reactivex/d/e/f/s$a;

    iget-object v1, p0, Lio/reactivex/d/e/f/s;->a:Lio/reactivex/aa;

    invoke-direct {v0, p1, v1}, Lio/reactivex/d/e/f/s$a;-><init>(Lio/reactivex/y;Lio/reactivex/aa;)V

    .line 35
    invoke-interface {p1, v0}, Lio/reactivex/y;->a(Lio/reactivex/b/c;)V

    .line 37
    iget-object p1, p0, Lio/reactivex/d/e/f/s;->b:Lio/reactivex/v;

    invoke-virtual {p1, v0}, Lio/reactivex/v;->a(Ljava/lang/Runnable;)Lio/reactivex/b/c;

    move-result-object p1

    .line 39
    iget-object v0, v0, Lio/reactivex/d/e/f/s$a;->b:Lio/reactivex/d/a/g;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/g;->b(Lio/reactivex/b/c;)Z

    return-void
.end method
