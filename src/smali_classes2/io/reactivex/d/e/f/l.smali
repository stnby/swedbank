.class public final Lio/reactivex/d/e/f/l;
.super Lio/reactivex/j;
.source "SingleFlatMapMaybe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/f/l$a;,
        Lio/reactivex/d/e/f/l$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/j<",
        "TR;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/aa<",
            "+TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/n<",
            "+TR;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/aa<",
            "+TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/n<",
            "+TR;>;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Lio/reactivex/j;-><init>()V

    .line 35
    iput-object p2, p0, Lio/reactivex/d/e/f/l;->b:Lio/reactivex/c/h;

    .line 36
    iput-object p1, p0, Lio/reactivex/d/e/f/l;->a:Lio/reactivex/aa;

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/l;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/l<",
            "-TR;>;)V"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lio/reactivex/d/e/f/l;->a:Lio/reactivex/aa;

    new-instance v1, Lio/reactivex/d/e/f/l$b;

    iget-object v2, p0, Lio/reactivex/d/e/f/l;->b:Lio/reactivex/c/h;

    invoke-direct {v1, p1, v2}, Lio/reactivex/d/e/f/l$b;-><init>(Lio/reactivex/l;Lio/reactivex/c/h;)V

    invoke-interface {v0, v1}, Lio/reactivex/aa;->a(Lio/reactivex/y;)V

    return-void
.end method
