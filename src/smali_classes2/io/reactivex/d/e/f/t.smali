.class public final Lio/reactivex/d/e/f/t;
.super Lio/reactivex/w;
.source "SingleTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/f/t$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/w<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final a:J

.field final b:Ljava/util/concurrent/TimeUnit;

.field final c:Lio/reactivex/v;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lio/reactivex/w;-><init>()V

    .line 33
    iput-wide p1, p0, Lio/reactivex/d/e/f/t;->a:J

    .line 34
    iput-object p3, p0, Lio/reactivex/d/e/f/t;->b:Ljava/util/concurrent/TimeUnit;

    .line 35
    iput-object p4, p0, Lio/reactivex/d/e/f/t;->c:Lio/reactivex/v;

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/y;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/y<",
            "-",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 40
    new-instance v0, Lio/reactivex/d/e/f/t$a;

    invoke-direct {v0, p1}, Lio/reactivex/d/e/f/t$a;-><init>(Lio/reactivex/y;)V

    .line 41
    invoke-interface {p1, v0}, Lio/reactivex/y;->a(Lio/reactivex/b/c;)V

    .line 42
    iget-object p1, p0, Lio/reactivex/d/e/f/t;->c:Lio/reactivex/v;

    iget-wide v1, p0, Lio/reactivex/d/e/f/t;->a:J

    iget-object v3, p0, Lio/reactivex/d/e/f/t;->b:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2, v3}, Lio/reactivex/v;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/d/e/f/t$a;->a(Lio/reactivex/b/c;)V

    return-void
.end method
