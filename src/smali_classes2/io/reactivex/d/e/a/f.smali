.class public final Lio/reactivex/d/e/a/f;
.super Lio/reactivex/b;
.source "CompletableFromAction.java"


# instance fields
.field final a:Lio/reactivex/c/a;


# direct methods
.method public constructor <init>(Lio/reactivex/c/a;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lio/reactivex/b;-><init>()V

    .line 27
    iput-object p1, p0, Lio/reactivex/d/e/a/f;->a:Lio/reactivex/c/a;

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/d;)V
    .locals 2

    .line 32
    invoke-static {}, Lio/reactivex/b/d;->a()Lio/reactivex/b/c;

    move-result-object v0

    .line 33
    invoke-interface {p1, v0}, Lio/reactivex/d;->a(Lio/reactivex/b/c;)V

    .line 35
    :try_start_0
    iget-object v1, p0, Lio/reactivex/d/e/a/f;->a:Lio/reactivex/c/a;

    invoke-interface {v1}, Lio/reactivex/c/a;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    invoke-interface {v0}, Lio/reactivex/b/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    invoke-interface {p1}, Lio/reactivex/d;->onComplete()V

    :cond_0
    return-void

    :catch_0
    move-exception v1

    .line 37
    invoke-static {v1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 38
    invoke-interface {v0}, Lio/reactivex/b/c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 39
    invoke-interface {p1, v1}, Lio/reactivex/d;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 41
    :cond_1
    invoke-static {v1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
