.class final Lio/reactivex/d/e/a/r$a;
.super Lio/reactivex/d/d/c;
.source "CompletableToObservable.java"

# interfaces
.implements Lio/reactivex/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/a/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/d/d/c<",
        "Ljava/lang/Void;",
        ">;",
        "Lio/reactivex/d;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "*>;"
        }
    .end annotation
.end field

.field b:Lio/reactivex/b/c;


# direct methods
.method constructor <init>(Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "*>;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Lio/reactivex/d/d/c;-><init>()V

    .line 47
    iput-object p1, p0, Lio/reactivex/d/e/a/r$a;->a:Lio/reactivex/u;

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 0

    and-int/lit8 p1, p1, 0x2

    return p1
.end method

.method public a()V
    .locals 1

    .line 90
    iget-object v0, p0, Lio/reactivex/d/e/a/r$a;->b:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lio/reactivex/d/e/a/r$a;->b:Lio/reactivex/b/c;

    invoke-static {v0, p1}, Lio/reactivex/d/a/c;->a(Lio/reactivex/b/c;Lio/reactivex/b/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iput-object p1, p0, Lio/reactivex/d/e/a/r$a;->b:Lio/reactivex/b/c;

    .line 64
    iget-object p1, p0, Lio/reactivex/d/e/a/r$a;->a:Lio/reactivex/u;

    invoke-interface {p1, p0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .line 95
    iget-object v0, p0, Lio/reactivex/d/e/a/r$a;->b:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->b()Z

    move-result v0

    return v0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lio/reactivex/d/e/a/r$a;->f()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public f()Ljava/lang/Void;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onComplete()V
    .locals 1

    .line 52
    iget-object v0, p0, Lio/reactivex/d/e/a/r$a;->a:Lio/reactivex/u;

    invoke-interface {v0}, Lio/reactivex/u;->onComplete()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lio/reactivex/d/e/a/r$a;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V

    return-void
.end method
