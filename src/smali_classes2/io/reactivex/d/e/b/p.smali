.class public final Lio/reactivex/d/e/b/p;
.super Lio/reactivex/h;
.source "FlowableTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/b/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/h<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/v;

.field final c:J

.field final d:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lio/reactivex/h;-><init>()V

    .line 32
    iput-wide p1, p0, Lio/reactivex/d/e/b/p;->c:J

    .line 33
    iput-object p3, p0, Lio/reactivex/d/e/b/p;->d:Ljava/util/concurrent/TimeUnit;

    .line 34
    iput-object p4, p0, Lio/reactivex/d/e/b/p;->b:Lio/reactivex/v;

    return-void
.end method


# virtual methods
.method public a(Lorg/reactivestreams/Subscriber;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 39
    new-instance v0, Lio/reactivex/d/e/b/p$a;

    invoke-direct {v0, p1}, Lio/reactivex/d/e/b/p$a;-><init>(Lorg/reactivestreams/Subscriber;)V

    .line 40
    invoke-interface {p1, v0}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    .line 42
    iget-object p1, p0, Lio/reactivex/d/e/b/p;->b:Lio/reactivex/v;

    iget-wide v1, p0, Lio/reactivex/d/e/b/p;->c:J

    iget-object v3, p0, Lio/reactivex/d/e/b/p;->d:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2, v3}, Lio/reactivex/v;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/c;

    move-result-object p1

    .line 44
    invoke-virtual {v0, p1}, Lio/reactivex/d/e/b/p$a;->a(Lio/reactivex/b/c;)V

    return-void
.end method
