.class final Lio/reactivex/d/e/b/d$b;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "FlowableFlatMap.java"

# interfaces
.implements Lio/reactivex/i;
.implements Lorg/reactivestreams/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/b/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/i<",
        "TT;>;",
        "Lorg/reactivestreams/Subscription;"
    }
.end annotation


# static fields
.field static final k:[Lio/reactivex/d/e/b/d$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lio/reactivex/d/e/b/d$a<",
            "**>;"
        }
    .end annotation
.end field

.field static final l:[Lio/reactivex/d/e/b/d$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lio/reactivex/d/e/b/d$a<",
            "**>;"
        }
    .end annotation
.end field


# instance fields
.field final a:Lorg/reactivestreams/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/reactivestreams/Subscriber<",
            "-TU;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lorg/reactivestreams/Publisher<",
            "+TU;>;>;"
        }
    .end annotation
.end field

.field final c:Z

.field final d:I

.field final e:I

.field volatile f:Lio/reactivex/d/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/c/h<",
            "TU;>;"
        }
    .end annotation
.end field

.field volatile g:Z

.field final h:Lio/reactivex/d/j/b;

.field volatile i:Z

.field final j:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "[",
            "Lio/reactivex/d/e/b/d$a<",
            "**>;>;"
        }
    .end annotation
.end field

.field final m:Ljava/util/concurrent/atomic/AtomicLong;

.field n:Lorg/reactivestreams/Subscription;

.field o:J

.field p:J

.field q:I

.field r:I

.field final s:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    .line 82
    new-array v1, v0, [Lio/reactivex/d/e/b/d$a;

    sput-object v1, Lio/reactivex/d/e/b/d$b;->k:[Lio/reactivex/d/e/b/d$a;

    .line 84
    new-array v0, v0, [Lio/reactivex/d/e/b/d$a;

    sput-object v0, Lio/reactivex/d/e/b/d$b;->l:[Lio/reactivex/d/e/b/d$a;

    return-void
.end method

.method constructor <init>(Lorg/reactivestreams/Subscriber;Lio/reactivex/c/h;ZII)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-TU;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lorg/reactivestreams/Publisher<",
            "+TU;>;>;ZII)V"
        }
    .end annotation

    .line 98
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 76
    new-instance v0, Lio/reactivex/d/j/b;

    invoke-direct {v0}, Lio/reactivex/d/j/b;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    .line 80
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    .line 86
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    .line 99
    iput-object p1, p0, Lio/reactivex/d/e/b/d$b;->a:Lorg/reactivestreams/Subscriber;

    .line 100
    iput-object p2, p0, Lio/reactivex/d/e/b/d$b;->b:Lio/reactivex/c/h;

    .line 101
    iput-boolean p3, p0, Lio/reactivex/d/e/b/d$b;->c:Z

    .line 102
    iput p4, p0, Lio/reactivex/d/e/b/d$b;->d:I

    .line 103
    iput p5, p0, Lio/reactivex/d/e/b/d$b;->e:I

    const/4 p1, 0x1

    shr-int/lit8 p2, p4, 0x1

    .line 104
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lio/reactivex/d/e/b/d$b;->s:I

    .line 105
    iget-object p1, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object p2, Lio/reactivex/d/e/b/d$b;->k:[Lio/reactivex/d/e/b/d$a;

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method a()Lio/reactivex/d/c/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/d/c/i<",
            "TU;>;"
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->f:Lio/reactivex/d/c/h;

    if-nez v0, :cond_1

    .line 219
    iget v0, p0, Lio/reactivex/d/e/b/d$b;->d:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 220
    new-instance v0, Lio/reactivex/d/f/c;

    iget v1, p0, Lio/reactivex/d/e/b/d$b;->e:I

    invoke-direct {v0, v1}, Lio/reactivex/d/f/c;-><init>(I)V

    goto :goto_0

    .line 222
    :cond_0
    new-instance v0, Lio/reactivex/d/f/b;

    iget v1, p0, Lio/reactivex/d/e/b/d$b;->d:I

    invoke-direct {v0, v1}, Lio/reactivex/d/f/b;-><init>(I)V

    .line 224
    :goto_0
    iput-object v0, p0, Lio/reactivex/d/e/b/d$b;->f:Lio/reactivex/d/c/h;

    :cond_1
    return-object v0
.end method

.method a(Lio/reactivex/d/e/b/d$a;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/e/b/d$a<",
            "TT;TU;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 598
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0, p2}, Lio/reactivex/d/j/b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p2, 0x1

    .line 599
    iput-boolean p2, p1, Lio/reactivex/d/e/b/d$a;->e:Z

    .line 600
    iget-boolean p1, p0, Lio/reactivex/d/e/b/d$b;->c:Z

    if-nez p1, :cond_0

    .line 601
    iget-object p1, p0, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    invoke-interface {p1}, Lorg/reactivestreams/Subscription;->cancel()V

    .line 602
    iget-object p1, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object p2, Lio/reactivex/d/e/b/d$b;->l:[Lio/reactivex/d/e/b/d$a;

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lio/reactivex/d/e/b/d$a;

    array-length p2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget-object v1, p1, v0

    .line 603
    invoke-virtual {v1}, Lio/reactivex/d/e/b/d$a;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 606
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->b()V

    goto :goto_1

    .line 608
    :cond_1
    invoke-static {p2}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method a(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;)V"
        }
    .end annotation

    .line 230
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->get()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lio/reactivex/d/e/b/d$b;->compareAndSet(II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 231
    iget-object v2, p0, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 232
    iget-object v4, p0, Lio/reactivex/d/e/b/d$b;->f:Lio/reactivex/d/c/h;

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-eqz v5, :cond_2

    if-eqz v4, :cond_0

    .line 233
    invoke-interface {v4}, Lio/reactivex/d/c/i;->d()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 234
    :cond_0
    iget-object v4, p0, Lio/reactivex/d/e/b/d$b;->a:Lorg/reactivestreams/Subscriber;

    invoke-interface {v4, p1}, Lorg/reactivestreams/Subscriber;->onNext(Ljava/lang/Object;)V

    const-wide v4, 0x7fffffffffffffffL

    cmp-long p1, v2, v4

    if-eqz p1, :cond_1

    .line 236
    iget-object p1, p0, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicLong;->decrementAndGet()J

    .line 238
    :cond_1
    iget p1, p0, Lio/reactivex/d/e/b/d$b;->d:I

    const v2, 0x7fffffff

    if-eq p1, v2, :cond_4

    iget-boolean p1, p0, Lio/reactivex/d/e/b/d$b;->i:Z

    if-nez p1, :cond_4

    iget p1, p0, Lio/reactivex/d/e/b/d$b;->r:I

    add-int/2addr p1, v0

    iput p1, p0, Lio/reactivex/d/e/b/d$b;->r:I

    iget v0, p0, Lio/reactivex/d/e/b/d$b;->s:I

    if-ne p1, v0, :cond_4

    .line 240
    iput v1, p0, Lio/reactivex/d/e/b/d$b;->r:I

    .line 241
    iget-object p1, p0, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    iget v0, p0, Lio/reactivex/d/e/b/d$b;->s:I

    int-to-long v0, v0

    invoke-interface {p1, v0, v1}, Lorg/reactivestreams/Subscription;->request(J)V

    goto :goto_0

    :cond_2
    if-nez v4, :cond_3

    .line 245
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->a()Lio/reactivex/d/c/i;

    move-result-object v4

    .line 247
    :cond_3
    invoke-interface {v4, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 248
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Scalar queue full?!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lio/reactivex/d/e/b/d$b;->onError(Ljava/lang/Throwable;)V

    return-void

    .line 252
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->decrementAndGet()I

    move-result p1

    if-nez p1, :cond_7

    return-void

    .line 256
    :cond_5
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->a()Lio/reactivex/d/c/i;

    move-result-object v0

    .line 257
    invoke-interface {v0, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 258
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Scalar queue full?!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lio/reactivex/d/e/b/d$b;->onError(Ljava/lang/Throwable;)V

    return-void

    .line 261
    :cond_6
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->getAndIncrement()I

    move-result p1

    if-eqz p1, :cond_7

    return-void

    .line 265
    :cond_7
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->c()V

    return-void
.end method

.method a(Ljava/lang/Object;Lio/reactivex/d/e/b/d$a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;",
            "Lio/reactivex/d/e/b/d$a<",
            "TT;TU;>;)V"
        }
    .end annotation

    .line 278
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->get()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/b/d$b;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 279
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    .line 280
    iget-object v2, p2, Lio/reactivex/d/e/b/d$a;->f:Lio/reactivex/d/c/i;

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_0

    .line 281
    invoke-interface {v2}, Lio/reactivex/d/c/i;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 282
    :cond_0
    iget-object v2, p0, Lio/reactivex/d/e/b/d$b;->a:Lorg/reactivestreams/Subscriber;

    invoke-interface {v2, p1}, Lorg/reactivestreams/Subscriber;->onNext(Ljava/lang/Object;)V

    const-wide v2, 0x7fffffffffffffffL

    cmp-long p1, v0, v2

    if-eqz p1, :cond_1

    .line 284
    iget-object p1, p0, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicLong;->decrementAndGet()J

    :cond_1
    const-wide/16 v0, 0x1

    .line 286
    invoke-virtual {p2, v0, v1}, Lio/reactivex/d/e/b/d$a;->a(J)V

    goto :goto_0

    :cond_2
    if-nez v2, :cond_3

    .line 289
    invoke-virtual {p0, p2}, Lio/reactivex/d/e/b/d$b;->c(Lio/reactivex/d/e/b/d$a;)Lio/reactivex/d/c/i;

    move-result-object v2

    .line 291
    :cond_3
    invoke-interface {v2, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 292
    new-instance p1, Lio/reactivex/exceptions/MissingBackpressureException;

    const-string p2, "Inner queue full?!"

    invoke-direct {p1, p2}, Lio/reactivex/exceptions/MissingBackpressureException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lio/reactivex/d/e/b/d$b;->onError(Ljava/lang/Throwable;)V

    return-void

    .line 296
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->decrementAndGet()I

    move-result p1

    if-nez p1, :cond_8

    return-void

    .line 300
    :cond_5
    iget-object v0, p2, Lio/reactivex/d/e/b/d$a;->f:Lio/reactivex/d/c/i;

    if-nez v0, :cond_6

    .line 302
    new-instance v0, Lio/reactivex/d/f/b;

    iget v1, p0, Lio/reactivex/d/e/b/d$b;->e:I

    invoke-direct {v0, v1}, Lio/reactivex/d/f/b;-><init>(I)V

    .line 303
    iput-object v0, p2, Lio/reactivex/d/e/b/d$a;->f:Lio/reactivex/d/c/i;

    .line 305
    :cond_6
    invoke-interface {v0, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 306
    new-instance p1, Lio/reactivex/exceptions/MissingBackpressureException;

    const-string p2, "Inner queue full?!"

    invoke-direct {p1, p2}, Lio/reactivex/exceptions/MissingBackpressureException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lio/reactivex/d/e/b/d$b;->onError(Ljava/lang/Throwable;)V

    return-void

    .line 309
    :cond_7
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->getAndIncrement()I

    move-result p1

    if-eqz p1, :cond_8

    return-void

    .line 313
    :cond_8
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->c()V

    return-void
.end method

.method a(Lio/reactivex/d/e/b/d$a;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/e/b/d$a<",
            "TT;TU;>;)Z"
        }
    .end annotation

    .line 170
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/d/e/b/d$a;

    .line 171
    sget-object v1, Lio/reactivex/d/e/b/d$b;->l:[Lio/reactivex/d/e/b/d$a;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 172
    invoke-virtual {p1}, Lio/reactivex/d/e/b/d$a;->a()V

    return v2

    .line 175
    :cond_1
    array-length v1, v0

    add-int/lit8 v3, v1, 0x1

    .line 176
    new-array v3, v3, [Lio/reactivex/d/e/b/d$a;

    .line 177
    invoke-static {v0, v2, v3, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    aput-object p1, v3, v1

    .line 179
    iget-object v1, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1
.end method

.method b()V
    .locals 1

    .line 365
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->c()V

    :cond_0
    return-void
.end method

.method b(Lio/reactivex/d/e/b/d$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/e/b/d$a<",
            "TT;TU;>;)V"
        }
    .end annotation

    .line 187
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/d/e/b/d$a;

    .line 188
    array-length v1, v0

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_3

    .line 194
    aget-object v5, v0, v4

    if-ne v5, p1, :cond_2

    move v2, v4

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    if-gez v2, :cond_4

    return-void

    :cond_4
    const/4 v4, 0x1

    if-ne v1, v4, :cond_5

    .line 204
    sget-object v1, Lio/reactivex/d/e/b/d$b;->k:[Lio/reactivex/d/e/b/d$a;

    goto :goto_2

    :cond_5
    add-int/lit8 v5, v1, -0x1

    .line 206
    new-array v5, v5, [Lio/reactivex/d/e/b/d$a;

    .line 207
    invoke-static {v0, v3, v5, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v2, 0x1

    sub-int/2addr v1, v2

    sub-int/2addr v1, v4

    .line 208
    invoke-static {v0, v3, v5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v1, v5

    .line 210
    :goto_2
    iget-object v2, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method c(Lio/reactivex/d/e/b/d$a;)Lio/reactivex/d/c/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/e/b/d$a<",
            "TT;TU;>;)",
            "Lio/reactivex/d/c/i<",
            "TU;>;"
        }
    .end annotation

    .line 269
    iget-object v0, p1, Lio/reactivex/d/e/b/d$a;->f:Lio/reactivex/d/c/i;

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Lio/reactivex/d/f/b;

    iget v1, p0, Lio/reactivex/d/e/b/d$b;->e:I

    invoke-direct {v0, v1}, Lio/reactivex/d/f/b;-><init>(I)V

    .line 272
    iput-object v0, p1, Lio/reactivex/d/e/b/d$a;->f:Lio/reactivex/d/c/i;

    :cond_0
    return-object v0
.end method

.method c()V
    .locals 23

    move-object/from16 v1, p0

    .line 371
    iget-object v2, v1, Lio/reactivex/d/e/b/d$b;->a:Lorg/reactivestreams/Subscriber;

    const/4 v4, 0x1

    .line 374
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lio/reactivex/d/e/b/d$b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 377
    :cond_1
    iget-object v0, v1, Lio/reactivex/d/e/b/d$b;->f:Lio/reactivex/d/c/h;

    .line 379
    iget-object v5, v1, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    const-wide v7, 0x7fffffffffffffffL

    cmp-long v10, v5, v7

    if-nez v10, :cond_2

    const/4 v10, 0x1

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    :goto_1
    const-wide/16 v12, 0x1

    const-wide/16 v14, 0x0

    if-eqz v0, :cond_9

    move-wide/from16 v16, v14

    :goto_2
    move-wide v7, v14

    const/16 v18, 0x0

    :goto_3
    cmp-long v19, v5, v14

    if-eqz v19, :cond_5

    .line 389
    invoke-interface {v0}, Lio/reactivex/d/c/h;->c()Ljava/lang/Object;

    move-result-object v9

    .line 391
    invoke-virtual/range {p0 .. p0}, Lio/reactivex/d/e/b/d$b;->d()Z

    move-result v18

    if-eqz v18, :cond_3

    return-void

    :cond_3
    if-nez v9, :cond_4

    move-object/from16 v18, v9

    goto :goto_4

    .line 398
    :cond_4
    invoke-interface {v2, v9}, Lorg/reactivestreams/Subscriber;->onNext(Ljava/lang/Object;)V

    add-long v16, v16, v12

    add-long/2addr v7, v12

    sub-long/2addr v5, v12

    move-object/from16 v18, v9

    goto :goto_3

    :cond_5
    :goto_4
    cmp-long v9, v7, v14

    if-eqz v9, :cond_7

    if-eqz v10, :cond_6

    const-wide v5, 0x7fffffffffffffffL

    goto :goto_5

    .line 408
    :cond_6
    iget-object v5, v1, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    neg-long v6, v7

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v5

    :cond_7
    :goto_5
    cmp-long v7, v5, v14

    if-eqz v7, :cond_a

    if-nez v18, :cond_8

    goto :goto_6

    :cond_8
    const-wide v7, 0x7fffffffffffffffL

    goto :goto_2

    :cond_9
    move-wide/from16 v16, v14

    .line 417
    :cond_a
    :goto_6
    iget-boolean v0, v1, Lio/reactivex/d/e/b/d$b;->g:Z

    .line 418
    iget-object v7, v1, Lio/reactivex/d/e/b/d$b;->f:Lio/reactivex/d/c/h;

    .line 419
    iget-object v8, v1, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lio/reactivex/d/e/b/d$a;

    .line 420
    array-length v9, v8

    if-eqz v0, :cond_e

    if-eqz v7, :cond_b

    .line 422
    invoke-interface {v7}, Lio/reactivex/d/c/h;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_b
    if-nez v9, :cond_e

    .line 423
    iget-object v0, v1, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object v0

    .line 424
    sget-object v3, Lio/reactivex/d/j/h;->a:Ljava/lang/Throwable;

    if-eq v0, v3, :cond_d

    if-nez v0, :cond_c

    .line 426
    invoke-interface {v2}, Lorg/reactivestreams/Subscriber;->onComplete()V

    goto :goto_7

    .line 428
    :cond_c
    invoke-interface {v2, v0}, Lorg/reactivestreams/Subscriber;->onError(Ljava/lang/Throwable;)V

    :cond_d
    :goto_7
    return-void

    :cond_e
    if-eqz v9, :cond_27

    move/from16 v20, v4

    .line 436
    iget-wide v3, v1, Lio/reactivex/d/e/b/d$b;->p:J

    .line 437
    iget v0, v1, Lio/reactivex/d/e/b/d$b;->q:I

    if-le v9, v0, :cond_f

    .line 439
    aget-object v7, v8, v0

    iget-wide v11, v7, Lio/reactivex/d/e/b/d$a;->a:J

    cmp-long v7, v11, v3

    if-eqz v7, :cond_14

    :cond_f
    if-gt v9, v0, :cond_10

    const/4 v0, 0x0

    :cond_10
    move v7, v0

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v9, :cond_13

    .line 445
    aget-object v11, v8, v7

    iget-wide v11, v11, Lio/reactivex/d/e/b/d$a;->a:J

    cmp-long v11, v11, v3

    if-nez v11, :cond_11

    goto :goto_9

    :cond_11
    add-int/lit8 v7, v7, 0x1

    if-ne v7, v9, :cond_12

    const/4 v7, 0x0

    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 454
    :cond_13
    :goto_9
    iput v7, v1, Lio/reactivex/d/e/b/d$b;->q:I

    .line 455
    aget-object v0, v8, v7

    iget-wide v3, v0, Lio/reactivex/d/e/b/d$a;->a:J

    iput-wide v3, v1, Lio/reactivex/d/e/b/d$b;->p:J

    move v0, v7

    :cond_14
    move v4, v0

    const/4 v0, 0x0

    const/4 v3, 0x0

    :goto_a
    if-ge v3, v9, :cond_26

    .line 461
    invoke-virtual/range {p0 .. p0}, Lio/reactivex/d/e/b/d$b;->d()Z

    move-result v7

    if-eqz v7, :cond_15

    return-void

    .line 466
    :cond_15
    aget-object v7, v8, v4

    const/4 v11, 0x0

    .line 470
    :goto_b
    invoke-virtual/range {p0 .. p0}, Lio/reactivex/d/e/b/d$b;->d()Z

    move-result v12

    if-eqz v12, :cond_16

    return-void

    .line 473
    :cond_16
    iget-object v12, v7, Lio/reactivex/d/e/b/d$a;->f:Lio/reactivex/d/c/i;

    if-nez v12, :cond_17

    move-object/from16 v21, v8

    move/from16 v22, v9

    goto/16 :goto_f

    :cond_17
    move-object/from16 v21, v8

    move/from16 v22, v9

    move-wide v8, v14

    :goto_c
    cmp-long v13, v5, v14

    if-eqz v13, :cond_1c

    .line 481
    :try_start_0
    invoke-interface {v12}, Lio/reactivex/d/c/i;->c()Ljava/lang/Object;

    move-result-object v11
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v11, :cond_18

    goto :goto_d

    .line 501
    :cond_18
    invoke-interface {v2, v11}, Lorg/reactivestreams/Subscriber;->onNext(Ljava/lang/Object;)V

    .line 503
    invoke-virtual/range {p0 .. p0}, Lio/reactivex/d/e/b/d$b;->d()Z

    move-result v13

    if-eqz v13, :cond_19

    return-void

    :cond_19
    const/4 v13, 0x0

    const-wide/16 v18, 0x1

    sub-long v5, v5, v18

    add-long v8, v8, v18

    goto :goto_c

    :catch_0
    move-exception v0

    move-object v8, v0

    .line 483
    invoke-static {v8}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 484
    invoke-virtual {v7}, Lio/reactivex/d/e/b/d$a;->a()V

    .line 485
    iget-object v0, v1, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0, v8}, Lio/reactivex/d/j/b;->a(Ljava/lang/Throwable;)Z

    .line 486
    iget-boolean v0, v1, Lio/reactivex/d/e/b/d$b;->c:Z

    if-nez v0, :cond_1a

    .line 487
    iget-object v0, v1, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    invoke-interface {v0}, Lorg/reactivestreams/Subscription;->cancel()V

    .line 489
    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lio/reactivex/d/e/b/d$b;->d()Z

    move-result v0

    if-eqz v0, :cond_1b

    return-void

    .line 492
    :cond_1b
    invoke-virtual {v1, v7}, Lio/reactivex/d/e/b/d$b;->b(Lio/reactivex/d/e/b/d$a;)V

    add-int/lit8 v3, v3, 0x1

    move/from16 v11, v22

    const/4 v0, 0x1

    const-wide/16 v7, 0x1

    goto :goto_11

    :cond_1c
    :goto_d
    cmp-long v12, v8, v14

    if-eqz v12, :cond_1e

    if-nez v10, :cond_1d

    .line 512
    iget-object v5, v1, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    neg-long v12, v8

    invoke-virtual {v5, v12, v13}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v5

    goto :goto_e

    :cond_1d
    const-wide v5, 0x7fffffffffffffffL

    .line 516
    :goto_e
    invoke-virtual {v7, v8, v9}, Lio/reactivex/d/e/b/d$a;->a(J)V

    :cond_1e
    cmp-long v8, v5, v14

    if-eqz v8, :cond_20

    if-nez v11, :cond_1f

    goto :goto_f

    :cond_1f
    move-object/from16 v8, v21

    move/from16 v9, v22

    goto :goto_b

    .line 522
    :cond_20
    :goto_f
    iget-boolean v8, v7, Lio/reactivex/d/e/b/d$a;->e:Z

    .line 523
    iget-object v9, v7, Lio/reactivex/d/e/b/d$a;->f:Lio/reactivex/d/c/i;

    if-eqz v8, :cond_23

    if-eqz v9, :cond_21

    .line 524
    invoke-interface {v9}, Lio/reactivex/d/c/i;->d()Z

    move-result v8

    if-eqz v8, :cond_23

    .line 525
    :cond_21
    invoke-virtual {v1, v7}, Lio/reactivex/d/e/b/d$b;->b(Lio/reactivex/d/e/b/d$a;)V

    .line 526
    invoke-virtual/range {p0 .. p0}, Lio/reactivex/d/e/b/d$b;->d()Z

    move-result v0

    if-eqz v0, :cond_22

    return-void

    :cond_22
    const/4 v0, 0x0

    const-wide/16 v7, 0x1

    add-long v16, v16, v7

    const/4 v0, 0x1

    goto :goto_10

    :cond_23
    const-wide/16 v7, 0x1

    :goto_10
    cmp-long v9, v5, v14

    if-nez v9, :cond_24

    goto :goto_12

    :cond_24
    add-int/lit8 v9, v4, 0x1

    move/from16 v11, v22

    if-ne v9, v11, :cond_25

    const/4 v4, 0x0

    goto :goto_11

    :cond_25
    move v4, v9

    :goto_11
    const/4 v9, 0x1

    add-int/2addr v3, v9

    move v9, v11

    move-object/from16 v8, v21

    goto/16 :goto_a

    :cond_26
    move-object/from16 v21, v8

    :goto_12
    const/4 v9, 0x1

    .line 541
    iput v4, v1, Lio/reactivex/d/e/b/d$b;->q:I

    .line 542
    aget-object v3, v21, v4

    iget-wide v3, v3, Lio/reactivex/d/e/b/d$a;->a:J

    iput-wide v3, v1, Lio/reactivex/d/e/b/d$b;->p:J

    move-wide/from16 v3, v16

    goto :goto_13

    :cond_27
    move/from16 v20, v4

    const/4 v9, 0x1

    move-wide/from16 v3, v16

    const/4 v0, 0x0

    :goto_13
    cmp-long v5, v3, v14

    if-eqz v5, :cond_28

    .line 545
    iget-boolean v5, v1, Lio/reactivex/d/e/b/d$b;->i:Z

    if-nez v5, :cond_28

    .line 546
    iget-object v5, v1, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    invoke-interface {v5, v3, v4}, Lorg/reactivestreams/Subscription;->request(J)V

    :cond_28
    if-eqz v0, :cond_29

    move/from16 v4, v20

    goto/16 :goto_0

    :cond_29
    move/from16 v3, v20

    neg-int v0, v3

    .line 551
    invoke-virtual {v1, v0}, Lio/reactivex/d/e/b/d$b;->addAndGet(I)I

    move-result v4

    if-nez v4, :cond_0

    return-void
.end method

.method public cancel()V
    .locals 1

    .line 351
    iget-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 352
    iput-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->i:Z

    .line 353
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    invoke-interface {v0}, Lorg/reactivestreams/Subscription;->cancel()V

    .line 354
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->f()V

    .line 355
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 356
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->f:Lio/reactivex/d/c/h;

    if-eqz v0, :cond_0

    .line 358
    invoke-interface {v0}, Lio/reactivex/d/c/i;->e()V

    :cond_0
    return-void
.end method

.method d()Z
    .locals 3

    .line 559
    iget-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->i:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 560
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->e()V

    return v1

    .line 563
    :cond_0
    iget-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0}, Lio/reactivex/d/j/b;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 564
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->e()V

    .line 565
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object v0

    .line 566
    sget-object v2, Lio/reactivex/d/j/h;->a:Ljava/lang/Throwable;

    if-eq v0, v2, :cond_1

    .line 567
    iget-object v2, p0, Lio/reactivex/d/e/b/d$b;->a:Lorg/reactivestreams/Subscriber;

    invoke-interface {v2, v0}, Lorg/reactivestreams/Subscriber;->onError(Ljava/lang/Throwable;)V

    :cond_1
    return v1

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method e()V
    .locals 1

    .line 575
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->f:Lio/reactivex/d/c/h;

    if-eqz v0, :cond_0

    .line 577
    invoke-interface {v0}, Lio/reactivex/d/c/i;->e()V

    :cond_0
    return-void
.end method

.method f()V
    .locals 4

    .line 582
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/d/e/b/d$a;

    .line 583
    sget-object v1, Lio/reactivex/d/e/b/d$b;->l:[Lio/reactivex/d/e/b/d$a;

    if-eq v0, v1, :cond_1

    .line 584
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->j:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lio/reactivex/d/e/b/d$b;->l:[Lio/reactivex/d/e/b/d$a;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/d/e/b/d$a;

    .line 585
    sget-object v1, Lio/reactivex/d/e/b/d$b;->l:[Lio/reactivex/d/e/b/d$a;

    if-eq v0, v1, :cond_1

    .line 586
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 587
    invoke-virtual {v3}, Lio/reactivex/d/e/b/d$a;->a()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 589
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 590
    sget-object v1, Lio/reactivex/d/j/h;->a:Ljava/lang/Throwable;

    if-eq v0, v1, :cond_1

    .line 591
    invoke-static {v0}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public onComplete()V
    .locals 1

    .line 334
    iget-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->g:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 337
    iput-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->g:Z

    .line 338
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->b()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 319
    iget-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->g:Z

    if-eqz v0, :cond_0

    .line 320
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0, p1}, Lio/reactivex/d/j/b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    .line 324
    iput-boolean p1, p0, Lio/reactivex/d/e/b/d$b;->g:Z

    .line 325
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->b()V

    goto :goto_0

    .line 327
    :cond_1
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 127
    iget-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->g:Z

    if-eqz v0, :cond_0

    return-void

    .line 132
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->b:Lio/reactivex/c/h;

    invoke-interface {v0, p1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "The mapper returned a null Publisher"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/reactivestreams/Publisher;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 139
    instance-of v0, p1, Ljava/util/concurrent/Callable;

    if-eqz v0, :cond_2

    .line 143
    :try_start_1
    check-cast p1, Ljava/util/concurrent/Callable;

    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz p1, :cond_1

    .line 152
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/b/d$b;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 154
    :cond_1
    iget p1, p0, Lio/reactivex/d/e/b/d$b;->d:I

    const v0, 0x7fffffff

    if-eq p1, v0, :cond_3

    iget-boolean p1, p0, Lio/reactivex/d/e/b/d$b;->i:Z

    if-nez p1, :cond_3

    iget p1, p0, Lio/reactivex/d/e/b/d$b;->r:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lio/reactivex/d/e/b/d$b;->r:I

    iget v0, p0, Lio/reactivex/d/e/b/d$b;->s:I

    if-ne p1, v0, :cond_3

    const/4 p1, 0x0

    .line 156
    iput p1, p0, Lio/reactivex/d/e/b/d$b;->r:I

    .line 157
    iget-object p1, p0, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    iget v0, p0, Lio/reactivex/d/e/b/d$b;->s:I

    int-to-long v0, v0

    invoke-interface {p1, v0, v1}, Lorg/reactivestreams/Subscription;->request(J)V

    goto :goto_0

    :catch_0
    move-exception p1

    .line 145
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 146
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->h:Lio/reactivex/d/j/b;

    invoke-virtual {v0, p1}, Lio/reactivex/d/j/b;->a(Ljava/lang/Throwable;)Z

    .line 147
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->b()V

    return-void

    .line 161
    :cond_2
    new-instance v0, Lio/reactivex/d/e/b/d$a;

    iget-wide v1, p0, Lio/reactivex/d/e/b/d$b;->o:J

    const-wide/16 v3, 0x1

    add-long/2addr v3, v1

    iput-wide v3, p0, Lio/reactivex/d/e/b/d$b;->o:J

    invoke-direct {v0, p0, v1, v2}, Lio/reactivex/d/e/b/d$a;-><init>(Lio/reactivex/d/e/b/d$b;J)V

    .line 162
    invoke-virtual {p0, v0}, Lio/reactivex/d/e/b/d$b;->a(Lio/reactivex/d/e/b/d$a;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 163
    invoke-interface {p1, v0}, Lorg/reactivestreams/Publisher;->subscribe(Lorg/reactivestreams/Subscriber;)V

    :cond_3
    :goto_0
    return-void

    :catch_1
    move-exception p1

    .line 134
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 135
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    invoke-interface {v0}, Lorg/reactivestreams/Subscription;->cancel()V

    .line 136
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/b/d$b;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onSubscribe(Lorg/reactivestreams/Subscription;)V
    .locals 2

    .line 110
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    invoke-static {v0, p1}, Lio/reactivex/d/i/f;->a(Lorg/reactivestreams/Subscription;Lorg/reactivestreams/Subscription;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    iput-object p1, p0, Lio/reactivex/d/e/b/d$b;->n:Lorg/reactivestreams/Subscription;

    .line 112
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->a:Lorg/reactivestreams/Subscriber;

    invoke-interface {v0, p0}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    .line 113
    iget-boolean v0, p0, Lio/reactivex/d/e/b/d$b;->i:Z

    if-nez v0, :cond_1

    .line 114
    iget v0, p0, Lio/reactivex/d/e/b/d$b;->d:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    .line 115
    invoke-interface {p1, v0, v1}, Lorg/reactivestreams/Subscription;->request(J)V

    goto :goto_0

    .line 117
    :cond_0
    iget v0, p0, Lio/reactivex/d/e/b/d$b;->d:I

    int-to-long v0, v0

    invoke-interface {p1, v0, v1}, Lorg/reactivestreams/Subscription;->request(J)V

    :cond_1
    :goto_0
    return-void
.end method

.method public request(J)V
    .locals 1

    .line 343
    invoke-static {p1, p2}, Lio/reactivex/d/i/f;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lio/reactivex/d/e/b/d$b;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v0, p1, p2}, Lio/reactivex/d/j/c;->a(Ljava/util/concurrent/atomic/AtomicLong;J)J

    .line 345
    invoke-virtual {p0}, Lio/reactivex/d/e/b/d$b;->b()V

    :cond_0
    return-void
.end method
