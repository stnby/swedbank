.class public final Lio/reactivex/d/e/b/j;
.super Lio/reactivex/d/e/b/a;
.source "FlowableRepeatWhen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/b/j$a;,
        Lio/reactivex/d/e/b/j$c;,
        Lio/reactivex/d/e/b/j$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/b/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-",
            "Lio/reactivex/h<",
            "Ljava/lang/Object;",
            ">;+",
            "Lorg/reactivestreams/Publisher<",
            "*>;>;"
        }
    .end annotation
.end field


# virtual methods
.method public a(Lorg/reactivestreams/Subscriber;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 40
    new-instance v0, Lio/reactivex/l/a;

    invoke-direct {v0, p1}, Lio/reactivex/l/a;-><init>(Lorg/reactivestreams/Subscriber;)V

    const/16 v1, 0x8

    .line 42
    invoke-static {v1}, Lio/reactivex/h/c;->a(I)Lio/reactivex/h/c;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/h/c;->g()Lio/reactivex/h/a;

    move-result-object v1

    .line 47
    :try_start_0
    iget-object v2, p0, Lio/reactivex/d/e/b/j;->c:Lio/reactivex/c/h;

    invoke-interface {v2, v1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "handler returned a null Publisher"

    invoke-static {v2, v3}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/reactivestreams/Publisher;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    new-instance v3, Lio/reactivex/d/e/b/j$b;

    iget-object v4, p0, Lio/reactivex/d/e/b/j;->b:Lio/reactivex/h;

    invoke-direct {v3, v4}, Lio/reactivex/d/e/b/j$b;-><init>(Lorg/reactivestreams/Publisher;)V

    .line 56
    new-instance v4, Lio/reactivex/d/e/b/j$a;

    invoke-direct {v4, v0, v1, v3}, Lio/reactivex/d/e/b/j$a;-><init>(Lorg/reactivestreams/Subscriber;Lio/reactivex/h/a;Lorg/reactivestreams/Subscription;)V

    .line 58
    iput-object v4, v3, Lio/reactivex/d/e/b/j$b;->d:Lio/reactivex/d/e/b/j$c;

    .line 60
    invoke-interface {p1, v4}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    .line 62
    invoke-interface {v2, v3}, Lorg/reactivestreams/Publisher;->subscribe(Lorg/reactivestreams/Subscriber;)V

    const/4 p1, 0x0

    .line 64
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, p1}, Lio/reactivex/d/e/b/j$b;->onNext(Ljava/lang/Object;)V

    return-void

    :catch_0
    move-exception v0

    .line 49
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 50
    invoke-static {v0, p1}, Lio/reactivex/d/i/c;->a(Ljava/lang/Throwable;Lorg/reactivestreams/Subscriber;)V

    return-void
.end method
