.class public final Lio/reactivex/d/e/b/d;
.super Lio/reactivex/d/e/b/a;
.source "FlowableFlatMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/b/d$a;,
        Lio/reactivex/d/e/b/d$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/b/a<",
        "TT;TU;>;"
    }
.end annotation


# instance fields
.field final c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lorg/reactivestreams/Publisher<",
            "+TU;>;>;"
        }
    .end annotation
.end field

.field final d:Z

.field final e:I

.field final f:I


# direct methods
.method public constructor <init>(Lio/reactivex/h;Lio/reactivex/c/h;ZII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/h<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lorg/reactivestreams/Publisher<",
            "+TU;>;>;ZII)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1}, Lio/reactivex/d/e/b/a;-><init>(Lio/reactivex/h;)V

    .line 42
    iput-object p2, p0, Lio/reactivex/d/e/b/d;->c:Lio/reactivex/c/h;

    .line 43
    iput-boolean p3, p0, Lio/reactivex/d/e/b/d;->d:Z

    .line 44
    iput p4, p0, Lio/reactivex/d/e/b/d;->e:I

    .line 45
    iput p5, p0, Lio/reactivex/d/e/b/d;->f:I

    return-void
.end method

.method public static a(Lorg/reactivestreams/Subscriber;Lio/reactivex/c/h;ZII)Lio/reactivex/i;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/reactivestreams/Subscriber<",
            "-TU;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lorg/reactivestreams/Publisher<",
            "+TU;>;>;ZII)",
            "Lio/reactivex/i<",
            "TT;>;"
        }
    .end annotation

    .line 59
    new-instance v6, Lio/reactivex/d/e/b/d$b;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/b/d$b;-><init>(Lorg/reactivestreams/Subscriber;Lio/reactivex/c/h;ZII)V

    return-object v6
.end method


# virtual methods
.method protected a(Lorg/reactivestreams/Subscriber;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-TU;>;)V"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lio/reactivex/d/e/b/d;->b:Lio/reactivex/h;

    iget-object v1, p0, Lio/reactivex/d/e/b/d;->c:Lio/reactivex/c/h;

    invoke-static {v0, p1, v1}, Lio/reactivex/d/e/b/m;->a(Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Subscriber;Lio/reactivex/c/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/b/d;->b:Lio/reactivex/h;

    iget-object v1, p0, Lio/reactivex/d/e/b/d;->c:Lio/reactivex/c/h;

    iget-boolean v2, p0, Lio/reactivex/d/e/b/d;->d:Z

    iget v3, p0, Lio/reactivex/d/e/b/d;->e:I

    iget v4, p0, Lio/reactivex/d/e/b/d;->f:I

    invoke-static {p1, v1, v2, v3, v4}, Lio/reactivex/d/e/b/d;->a(Lorg/reactivestreams/Subscriber;Lio/reactivex/c/h;ZII)Lio/reactivex/i;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/h;->a(Lio/reactivex/i;)V

    return-void
.end method
