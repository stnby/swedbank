.class public final Lio/reactivex/d/e/b/k;
.super Lio/reactivex/d/e/b/a;
.source "FlowableRetryPredicate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/b/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/b/a<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final c:Lio/reactivex/c/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/k<",
            "-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field final d:J


# direct methods
.method public constructor <init>(Lio/reactivex/h;JLio/reactivex/c/k;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/h<",
            "TT;>;J",
            "Lio/reactivex/c/k<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1}, Lio/reactivex/d/e/b/a;-><init>(Lio/reactivex/h;)V

    .line 32
    iput-object p4, p0, Lio/reactivex/d/e/b/k;->c:Lio/reactivex/c/k;

    .line 33
    iput-wide p2, p0, Lio/reactivex/d/e/b/k;->d:J

    return-void
.end method


# virtual methods
.method public a(Lorg/reactivestreams/Subscriber;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 38
    new-instance v5, Lio/reactivex/d/i/e;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Lio/reactivex/d/i/e;-><init>(Z)V

    .line 39
    invoke-interface {p1, v5}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    .line 41
    new-instance v7, Lio/reactivex/d/e/b/k$a;

    iget-wide v2, p0, Lio/reactivex/d/e/b/k;->d:J

    iget-object v4, p0, Lio/reactivex/d/e/b/k;->c:Lio/reactivex/c/k;

    iget-object v6, p0, Lio/reactivex/d/e/b/k;->b:Lio/reactivex/h;

    move-object v0, v7

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/b/k$a;-><init>(Lorg/reactivestreams/Subscriber;JLio/reactivex/c/k;Lio/reactivex/d/i/e;Lorg/reactivestreams/Publisher;)V

    .line 42
    invoke-virtual {v7}, Lio/reactivex/d/e/b/k$a;->a()V

    return-void
.end method
