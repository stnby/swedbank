.class final Lio/reactivex/d/e/d/e;
.super Ljava/lang/Object;
.source "ScalarXMapZHelper.java"


# direct methods
.method static a(Ljava/lang/Object;Lio/reactivex/c/h;Lio/reactivex/d;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;",
            "Lio/reactivex/d;",
            ")Z"
        }
    .end annotation

    .line 53
    instance-of v0, p0, Ljava/util/concurrent/Callable;

    if-eqz v0, :cond_2

    .line 55
    check-cast p0, Ljava/util/concurrent/Callable;

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 58
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 60
    invoke-interface {p1, p0}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    const-string p1, "The mapper returned a null CompletableSource"

    invoke-static {p0, p1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Lio/reactivex/f;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    if-nez v0, :cond_1

    .line 69
    invoke-static {p2}, Lio/reactivex/d/a/d;->a(Lio/reactivex/d;)V

    goto :goto_0

    .line 71
    :cond_1
    invoke-interface {v0, p2}, Lio/reactivex/f;->a(Lio/reactivex/d;)V

    :goto_0
    return v1

    :catch_0
    move-exception p0

    .line 63
    invoke-static {p0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 64
    invoke-static {p0, p2}, Lio/reactivex/d/a/d;->a(Ljava/lang/Throwable;Lio/reactivex/d;)V

    return v1

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method static a(Ljava/lang/Object;Lio/reactivex/c/h;Lio/reactivex/u;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/aa<",
            "+TR;>;>;",
            "Lio/reactivex/u<",
            "-TR;>;)Z"
        }
    .end annotation

    .line 131
    instance-of v0, p0, Ljava/util/concurrent/Callable;

    if-eqz v0, :cond_2

    .line 133
    check-cast p0, Ljava/util/concurrent/Callable;

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 136
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 138
    invoke-interface {p1, p0}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    const-string p1, "The mapper returned a null SingleSource"

    invoke-static {p0, p1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Lio/reactivex/aa;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    if-nez v0, :cond_1

    .line 147
    invoke-static {p2}, Lio/reactivex/d/a/d;->a(Lio/reactivex/u;)V

    goto :goto_0

    .line 149
    :cond_1
    invoke-static {p2}, Lio/reactivex/d/e/f/v;->b(Lio/reactivex/u;)Lio/reactivex/y;

    move-result-object p0

    invoke-interface {v0, p0}, Lio/reactivex/aa;->a(Lio/reactivex/y;)V

    :goto_0
    return v1

    :catch_0
    move-exception p0

    .line 141
    invoke-static {p0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 142
    invoke-static {p0, p2}, Lio/reactivex/d/a/d;->a(Ljava/lang/Throwable;Lio/reactivex/u;)V

    return v1

    :cond_2
    const/4 p0, 0x0

    return p0
.end method
