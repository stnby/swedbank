.class final Lio/reactivex/d/e/d/c$a;
.super Ljava/lang/Object;
.source "ObservableSwitchMapCompletable.java"

# interfaces
.implements Lio/reactivex/b/c;
.implements Lio/reactivex/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/c$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/c;",
        "Lio/reactivex/u<",
        "TT;>;"
    }
.end annotation


# static fields
.field static final f:Lio/reactivex/d/e/d/c$a$a;


# instance fields
.field final a:Lio/reactivex/d;

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;"
        }
    .end annotation
.end field

.field final c:Z

.field final d:Lio/reactivex/d/j/b;

.field final e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lio/reactivex/d/e/d/c$a$a;",
            ">;"
        }
    .end annotation
.end field

.field volatile g:Z

.field h:Lio/reactivex/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 69
    new-instance v0, Lio/reactivex/d/e/d/c$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lio/reactivex/d/e/d/c$a$a;-><init>(Lio/reactivex/d/e/d/c$a;)V

    sput-object v0, Lio/reactivex/d/e/d/c$a;->f:Lio/reactivex/d/e/d/c$a$a;

    return-void
.end method

.method constructor <init>(Lio/reactivex/d;Lio/reactivex/c/h;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;Z)V"
        }
    .end annotation

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    .line 78
    iput-object p2, p0, Lio/reactivex/d/e/d/c$a;->b:Lio/reactivex/c/h;

    .line 79
    iput-boolean p3, p0, Lio/reactivex/d/e/d/c$a;->c:Z

    .line 80
    new-instance p1, Lio/reactivex/d/j/b;

    invoke-direct {p1}, Lio/reactivex/d/j/b;-><init>()V

    iput-object p1, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    .line 81
    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 161
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->h:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 162
    invoke-virtual {p0}, Lio/reactivex/d/e/d/c$a;->c()V

    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1

    .line 86
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->h:Lio/reactivex/b/c;

    invoke-static {v0, p1}, Lio/reactivex/d/a/c;->a(Lio/reactivex/b/c;Lio/reactivex/b/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iput-object p1, p0, Lio/reactivex/d/e/d/c$a;->h:Lio/reactivex/b/c;

    .line 88
    iget-object p1, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {p1, p0}, Lio/reactivex/d;->a(Lio/reactivex/b/c;)V

    :cond_0
    return-void
.end method

.method a(Lio/reactivex/d/e/d/c$a$a;)V
    .locals 2

    .line 192
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 193
    iget-boolean p1, p0, Lio/reactivex/d/e/d/c$a;->g:Z

    if-eqz p1, :cond_1

    .line 194
    iget-object p1, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    invoke-virtual {p1}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object p1

    if-nez p1, :cond_0

    .line 196
    iget-object p1, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {p1}, Lio/reactivex/d;->onComplete()V

    goto :goto_0

    .line 198
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {v0, p1}, Lio/reactivex/d;->onError(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method a(Lio/reactivex/d/e/d/c$a$a;Ljava/lang/Throwable;)V
    .locals 2

    .line 171
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 172
    iget-object p1, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    invoke-virtual {p1, p2}, Lio/reactivex/d/j/b;->a(Ljava/lang/Throwable;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 173
    iget-boolean p1, p0, Lio/reactivex/d/e/d/c$a;->c:Z

    if-eqz p1, :cond_0

    .line 174
    iget-boolean p1, p0, Lio/reactivex/d/e/d/c$a;->g:Z

    if-eqz p1, :cond_1

    .line 175
    iget-object p1, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    invoke-virtual {p1}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object p1

    .line 176
    iget-object p2, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {p2, p1}, Lio/reactivex/d;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 179
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/d/c$a;->a()V

    .line 180
    iget-object p1, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    invoke-virtual {p1}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object p1

    .line 181
    sget-object p2, Lio/reactivex/d/j/h;->a:Ljava/lang/Throwable;

    if-eq p1, p2, :cond_1

    .line 182
    iget-object p2, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {p2, p1}, Lio/reactivex/d;->onError(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void

    .line 188
    :cond_2
    invoke-static {p2}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public b()Z
    .locals 2

    .line 167
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lio/reactivex/d/e/d/c$a;->f:Lio/reactivex/d/e/d/c$a$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method c()V
    .locals 2

    .line 153
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lio/reactivex/d/e/d/c$a;->f:Lio/reactivex/d/e/d/c$a$a;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/c$a$a;

    if-eqz v0, :cond_0

    .line 154
    sget-object v1, Lio/reactivex/d/e/d/c$a;->f:Lio/reactivex/d/e/d/c$a$a;

    if-eq v0, v1, :cond_0

    .line 155
    invoke-virtual {v0}, Lio/reactivex/d/e/d/c$a$a;->a()V

    :cond_0
    return-void
.end method

.method public onComplete()V
    .locals 2

    const/4 v0, 0x1

    .line 141
    iput-boolean v0, p0, Lio/reactivex/d/e/d/c$a;->g:Z

    .line 142
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    invoke-virtual {v0}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {v0}, Lio/reactivex/d;->onComplete()V

    goto :goto_0

    .line 147
    :cond_0
    iget-object v1, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {v1, v0}, Lio/reactivex/d;->onError(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    invoke-virtual {v0, p1}, Lio/reactivex/d/j/b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    iget-boolean p1, p0, Lio/reactivex/d/e/d/c$a;->c:Z

    if-eqz p1, :cond_0

    .line 126
    invoke-virtual {p0}, Lio/reactivex/d/e/d/c$a;->onComplete()V

    goto :goto_0

    .line 128
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/d/c$a;->c()V

    .line 129
    iget-object p1, p0, Lio/reactivex/d/e/d/c$a;->d:Lio/reactivex/d/j/b;

    invoke-virtual {p1}, Lio/reactivex/d/j/b;->a()Ljava/lang/Throwable;

    move-result-object p1

    .line 130
    sget-object v0, Lio/reactivex/d/j/h;->a:Ljava/lang/Throwable;

    if-eq p1, v0, :cond_2

    .line 131
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->a:Lio/reactivex/d;

    invoke-interface {v0, p1}, Lio/reactivex/d;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 135
    :cond_1
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 97
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->b:Lio/reactivex/c/h;

    invoke-interface {v0, p1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "The mapper returned a null CompletableSource"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    new-instance v0, Lio/reactivex/d/e/d/c$a$a;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/c$a$a;-><init>(Lio/reactivex/d/e/d/c$a;)V

    .line 108
    :cond_0
    iget-object v1, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/d/e/d/c$a$a;

    .line 109
    sget-object v2, Lio/reactivex/d/e/d/c$a;->f:Lio/reactivex/d/e/d/c$a$a;

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 112
    :cond_1
    iget-object v2, p0, Lio/reactivex/d/e/d/c$a;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_2

    .line 114
    invoke-virtual {v1}, Lio/reactivex/d/e/d/c$a$a;->a()V

    .line 116
    :cond_2
    invoke-interface {p1, v0}, Lio/reactivex/f;->a(Lio/reactivex/d;)V

    :goto_0
    return-void

    :catch_0
    move-exception p1

    .line 99
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 100
    iget-object v0, p0, Lio/reactivex/d/e/d/c$a;->h:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 101
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/d/c$a;->onError(Ljava/lang/Throwable;)V

    return-void
.end method
