.class public final Lio/reactivex/d/e/d/d;
.super Lio/reactivex/o;
.source "ObservableSwitchMapSingle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/o<",
        "TR;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/aa<",
            "+TR;>;>;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/o;Lio/reactivex/c/h;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/aa<",
            "+TR;>;>;Z)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    .line 47
    iput-object p1, p0, Lio/reactivex/d/e/d/d;->a:Lio/reactivex/o;

    .line 48
    iput-object p2, p0, Lio/reactivex/d/e/d/d;->b:Lio/reactivex/c/h;

    .line 49
    iput-boolean p3, p0, Lio/reactivex/d/e/d/d;->c:Z

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TR;>;)V"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lio/reactivex/d/e/d/d;->a:Lio/reactivex/o;

    iget-object v1, p0, Lio/reactivex/d/e/d/d;->b:Lio/reactivex/c/h;

    invoke-static {v0, v1, p1}, Lio/reactivex/d/e/d/e;->a(Ljava/lang/Object;Lio/reactivex/c/h;Lio/reactivex/u;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lio/reactivex/d/e/d/d;->a:Lio/reactivex/o;

    new-instance v1, Lio/reactivex/d/e/d/d$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/d;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/d/d;->c:Z

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/d/d$a;-><init>(Lio/reactivex/u;Lio/reactivex/c/h;Z)V

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/u;)V

    :cond_0
    return-void
.end method
