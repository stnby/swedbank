.class public final Lio/reactivex/d/e/d/f;
.super Lio/reactivex/o;
.source "SingleFlatMapObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/o<",
        "TR;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/aa<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TR;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/aa<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TR;>;>;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    .line 41
    iput-object p1, p0, Lio/reactivex/d/e/d/f;->a:Lio/reactivex/aa;

    .line 42
    iput-object p2, p0, Lio/reactivex/d/e/d/f;->b:Lio/reactivex/c/h;

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TR;>;)V"
        }
    .end annotation

    .line 47
    new-instance v0, Lio/reactivex/d/e/d/f$a;

    iget-object v1, p0, Lio/reactivex/d/e/d/f;->b:Lio/reactivex/c/h;

    invoke-direct {v0, p1, v1}, Lio/reactivex/d/e/d/f$a;-><init>(Lio/reactivex/u;Lio/reactivex/c/h;)V

    .line 48
    invoke-interface {p1, v0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 49
    iget-object p1, p0, Lio/reactivex/d/e/d/f;->a:Lio/reactivex/aa;

    invoke-interface {p1, v0}, Lio/reactivex/aa;->a(Lio/reactivex/y;)V

    return-void
.end method
