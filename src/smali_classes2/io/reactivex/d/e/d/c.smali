.class public final Lio/reactivex/d/e/d/c;
.super Lio/reactivex/b;
.source "ObservableSwitchMapCompletable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/b;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/o;Lio/reactivex/c/h;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "TT;>;",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;Z)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Lio/reactivex/b;-><init>()V

    .line 45
    iput-object p1, p0, Lio/reactivex/d/e/d/c;->a:Lio/reactivex/o;

    .line 46
    iput-object p2, p0, Lio/reactivex/d/e/d/c;->b:Lio/reactivex/c/h;

    .line 47
    iput-boolean p3, p0, Lio/reactivex/d/e/d/c;->c:Z

    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/d;)V
    .locals 4

    .line 52
    iget-object v0, p0, Lio/reactivex/d/e/d/c;->a:Lio/reactivex/o;

    iget-object v1, p0, Lio/reactivex/d/e/d/c;->b:Lio/reactivex/c/h;

    invoke-static {v0, v1, p1}, Lio/reactivex/d/e/d/e;->a(Ljava/lang/Object;Lio/reactivex/c/h;Lio/reactivex/d;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lio/reactivex/d/e/d/c;->a:Lio/reactivex/o;

    new-instance v1, Lio/reactivex/d/e/d/c$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/c;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/d/c;->c:Z

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/d/c$a;-><init>(Lio/reactivex/d;Lio/reactivex/c/h;Z)V

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/u;)V

    :cond_0
    return-void
.end method
