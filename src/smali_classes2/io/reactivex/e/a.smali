.class public abstract Lio/reactivex/e/a;
.super Lio/reactivex/o;
.source "ConnectableObservable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/o<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/b/c;
    .locals 1

    .line 64
    new-instance v0, Lio/reactivex/d/j/e;

    invoke-direct {v0}, Lio/reactivex/d/j/e;-><init>()V

    .line 65
    invoke-virtual {p0, v0}, Lio/reactivex/e/a;->d(Lio/reactivex/c/g;)V

    .line 66
    iget-object v0, v0, Lio/reactivex/d/j/e;->a:Lio/reactivex/b/c;

    return-object v0
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .line 86
    new-instance v0, Lio/reactivex/d/e/e/am;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/am;-><init>(Lio/reactivex/e/a;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public abstract d(Lio/reactivex/c/g;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g<",
            "-",
            "Lio/reactivex/b/c;",
            ">;)V"
        }
    .end annotation
.end method
