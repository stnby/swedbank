.class public final Lio/reactivex/j/a;
.super Ljava/lang/Object;
.source "Schedulers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/j/a$b;,
        Lio/reactivex/j/a$h;,
        Lio/reactivex/j/a$f;,
        Lio/reactivex/j/a$c;,
        Lio/reactivex/j/a$e;,
        Lio/reactivex/j/a$d;,
        Lio/reactivex/j/a$a;,
        Lio/reactivex/j/a$g;
    }
.end annotation


# static fields
.field static final a:Lio/reactivex/v;

.field static final b:Lio/reactivex/v;

.field static final c:Lio/reactivex/v;

.field static final d:Lio/reactivex/v;

.field static final e:Lio/reactivex/v;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 75
    new-instance v0, Lio/reactivex/j/a$h;

    invoke-direct {v0}, Lio/reactivex/j/a$h;-><init>()V

    invoke-static {v0}, Lio/reactivex/g/a;->d(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    sput-object v0, Lio/reactivex/j/a;->a:Lio/reactivex/v;

    .line 77
    new-instance v0, Lio/reactivex/j/a$b;

    invoke-direct {v0}, Lio/reactivex/j/a$b;-><init>()V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    sput-object v0, Lio/reactivex/j/a;->b:Lio/reactivex/v;

    .line 79
    new-instance v0, Lio/reactivex/j/a$c;

    invoke-direct {v0}, Lio/reactivex/j/a$c;-><init>()V

    invoke-static {v0}, Lio/reactivex/g/a;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    sput-object v0, Lio/reactivex/j/a;->c:Lio/reactivex/v;

    .line 81
    invoke-static {}, Lio/reactivex/d/g/n;->c()Lio/reactivex/d/g/n;

    move-result-object v0

    sput-object v0, Lio/reactivex/j/a;->d:Lio/reactivex/v;

    .line 83
    new-instance v0, Lio/reactivex/j/a$f;

    invoke-direct {v0}, Lio/reactivex/j/a$f;-><init>()V

    invoke-static {v0}, Lio/reactivex/g/a;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    sput-object v0, Lio/reactivex/j/a;->e:Lio/reactivex/v;

    return-void
.end method

.method public static a()Lio/reactivex/v;
    .locals 1

    .line 136
    sget-object v0, Lio/reactivex/j/a;->b:Lio/reactivex/v;

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;)Lio/reactivex/v;
    .locals 2

    .line 346
    new-instance v0, Lio/reactivex/d/g/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/reactivex/d/g/d;-><init>(Ljava/util/concurrent/Executor;Z)V

    return-object v0
.end method

.method public static b()Lio/reactivex/v;
    .locals 1

    .line 181
    sget-object v0, Lio/reactivex/j/a;->c:Lio/reactivex/v;

    invoke-static {v0}, Lio/reactivex/g/a;->b(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lio/reactivex/v;
    .locals 1

    .line 200
    sget-object v0, Lio/reactivex/j/a;->d:Lio/reactivex/v;

    return-object v0
.end method

.method public static d()Lio/reactivex/v;
    .locals 1

    .line 289
    sget-object v0, Lio/reactivex/j/a;->a:Lio/reactivex/v;

    invoke-static {v0}, Lio/reactivex/g/a;->c(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
