.class public abstract Lio/reactivex/b;
.super Ljava/lang/Object;
.source "Completable.java"

# interfaces
.implements Lio/reactivex/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lio/reactivex/b;
    .locals 1

    .line 172
    sget-object v0, Lio/reactivex/d/e/a/d;->a:Lio/reactivex/b;

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b;
    .locals 1

    .line 918
    invoke-static {}, Lio/reactivex/j/a;->a()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lio/reactivex/b;->a(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)Lio/reactivex/b;
    .locals 1

    const-string v0, "unit is null"

    .line 939
    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "scheduler is null"

    .line 940
    invoke-static {p3, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 941
    new-instance v0, Lio/reactivex/d/e/a/p;

    invoke-direct {v0, p0, p1, p2, p3}, Lio/reactivex/d/e/a/p;-><init>(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lio/reactivex/c/a;)Lio/reactivex/b;
    .locals 1

    const-string v0, "run is null"

    .line 426
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 427
    new-instance v0, Lio/reactivex/d/e/a/f;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/f;-><init>(Lio/reactivex/c/a;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method private a(Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;)Lio/reactivex/b;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g<",
            "-",
            "Lio/reactivex/b/c;",
            ">;",
            "Lio/reactivex/c/g<",
            "-",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/c/a;",
            "Lio/reactivex/c/a;",
            "Lio/reactivex/c/a;",
            "Lio/reactivex/c/a;",
            ")",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "onSubscribe is null"

    .line 1591
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onError is null"

    .line 1592
    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onComplete is null"

    .line 1593
    invoke-static {p3, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onTerminate is null"

    .line 1594
    invoke-static {p4, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onAfterTerminate is null"

    .line 1595
    invoke-static {p5, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onDispose is null"

    .line 1596
    invoke-static {p6, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1597
    new-instance v0, Lio/reactivex/d/e/a/m;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v1 .. v8}, Lio/reactivex/d/e/a/m;-><init>(Lio/reactivex/f;Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public static a(Lio/reactivex/e;)Lio/reactivex/b;
    .locals 1

    const-string v0, "source is null"

    .line 309
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 310
    new-instance v0, Lio/reactivex/d/e/a/b;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/b;-><init>(Lio/reactivex/e;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Iterable;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lio/reactivex/f;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "sources is null"

    .line 685
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 686
    new-instance v0, Lio/reactivex/d/e/a/j;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/j;-><init>(Ljava/lang/Iterable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Throwable;)Lio/reactivex/b;
    .locals 1

    const-string v0, "error is null"

    .line 398
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 399
    new-instance v0, Lio/reactivex/d/e/a/e;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/e;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable<",
            "*>;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "callable is null"

    .line 453
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 454
    new-instance v0, Lio/reactivex/d/e/a/g;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/g;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lorg/reactivestreams/Publisher;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/reactivestreams/Publisher<",
            "TT;>;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "publisher is null"

    .line 587
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 588
    new-instance v0, Lio/reactivex/d/e/a/h;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/h;-><init>(Lorg/reactivestreams/Publisher;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method private static b(Ljava/lang/Throwable;)Ljava/lang/NullPointerException;
    .locals 2

    .line 950
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Actually not, but can\'t pass out an exception otherwise..."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 951
    invoke-virtual {v0, p0}, Ljava/lang/NullPointerException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    return-object v0
.end method


# virtual methods
.method public final a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/a;",
            "Lio/reactivex/c/g<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/b/c;"
        }
    .end annotation

    const-string v0, "onError is null"

    .line 2379
    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onComplete is null"

    .line 2380
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2382
    new-instance v0, Lio/reactivex/d/d/g;

    invoke-direct {v0, p2, p1}, Lio/reactivex/d/d/g;-><init>(Lio/reactivex/c/g;Lio/reactivex/c/a;)V

    .line 2383
    invoke-virtual {p0, v0}, Lio/reactivex/b;->a(Lio/reactivex/d;)V

    return-object v0
.end method

.method public final a(Lio/reactivex/c/g;)Lio/reactivex/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .line 1541
    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v1

    sget-object v3, Lio/reactivex/d/b/a;->c:Lio/reactivex/c/a;

    sget-object v4, Lio/reactivex/d/b/a;->c:Lio/reactivex/c/a;

    sget-object v5, Lio/reactivex/d/b/a;->c:Lio/reactivex/c/a;

    sget-object v6, Lio/reactivex/d/b/a;->c:Lio/reactivex/c/a;

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/b;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/c/h;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h<",
            "-",
            "Ljava/lang/Throwable;",
            "+",
            "Lio/reactivex/f;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "errorMapper is null"

    .line 1951
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1952
    new-instance v0, Lio/reactivex/d/e/a/n;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/a/n;-><init>(Lio/reactivex/f;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/c/k;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/k<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "predicate is null"

    .line 1928
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1930
    new-instance v0, Lio/reactivex/d/e/a/l;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/a/l;-><init>(Lio/reactivex/f;Lio/reactivex/c/k;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/f;)Lio/reactivex/b;
    .locals 1

    const-string v0, "next is null"

    .line 1181
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1182
    new-instance v0, Lio/reactivex/d/e/a/a;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/a/a;-><init>(Lio/reactivex/f;Lio/reactivex/f;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/v;)Lio/reactivex/b;
    .locals 1

    const-string v0, "scheduler is null"

    .line 1890
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1891
    new-instance v0, Lio/reactivex/d/e/a/k;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/a/k;-><init>(Lio/reactivex/f;Lio/reactivex/v;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/n;)Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n<",
            "TT;>;)",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "next is null"

    .line 1159
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1160
    new-instance v0, Lio/reactivex/d/e/c/d;

    invoke-direct {v0, p1, p0}, Lio/reactivex/d/e/c/d;-><init>(Lio/reactivex/n;Lio/reactivex/f;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/j;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/s;)Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/s<",
            "TT;>;)",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "next is null"

    .line 1083
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1084
    new-instance v0, Lio/reactivex/d/e/d/a;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/f;Lio/reactivex/s;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/aa;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/aa<",
            "TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "next is null"

    .line 1135
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1136
    new-instance v0, Lio/reactivex/d/e/f/d;

    invoke-direct {v0, p1, p0}, Lio/reactivex/d/e/f/d;-><init>(Lio/reactivex/aa;Lio/reactivex/f;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/lang/Object;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "completionValue is null"

    .line 2714
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2715
    new-instance v0, Lio/reactivex/d/e/a/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lio/reactivex/d/e/a/s;-><init>(Lio/reactivex/f;Ljava/util/concurrent/Callable;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/d;)V
    .locals 1

    const-string v0, "observer is null"

    .line 2302
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2305
    :try_start_0
    invoke-static {p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/b;Lio/reactivex/d;)Lio/reactivex/d;

    move-result-object p1

    const-string v0, "The RxJavaPlugins.onSubscribe hook returned a null CompletableObserver. Please check the handler provided to RxJavaPlugins.setOnCompletableSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins"

    .line 2307
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2309
    invoke-virtual {p0, p1}, Lio/reactivex/b;->b(Lio/reactivex/d;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 2313
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 2314
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 2315
    invoke-static {p1}, Lio/reactivex/b;->b(Ljava/lang/Throwable;)Ljava/lang/NullPointerException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    .line 2311
    throw p1
.end method

.method public final b(Lio/reactivex/c/a;)Lio/reactivex/b;
    .locals 7

    .line 1499
    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v2

    sget-object v4, Lio/reactivex/d/b/a;->c:Lio/reactivex/c/a;

    sget-object v5, Lio/reactivex/d/b/a;->c:Lio/reactivex/c/a;

    sget-object v6, Lio/reactivex/d/b/a;->c:Lio/reactivex/c/a;

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/b;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lio/reactivex/c/h;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h<",
            "-",
            "Lio/reactivex/h<",
            "Ljava/lang/Throwable;",
            ">;+",
            "Lorg/reactivestreams/Publisher<",
            "*>;>;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .line 2190
    invoke-virtual {p0}, Lio/reactivex/b;->e()Lio/reactivex/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/h;->b(Lio/reactivex/c/h;)Lio/reactivex/h;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/b;->a(Lorg/reactivestreams/Publisher;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lio/reactivex/v;)Lio/reactivex/b;
    .locals 1

    const-string v0, "scheduler is null"

    .line 2431
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2433
    new-instance v0, Lio/reactivex/d/e/a/o;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/a/o;-><init>(Lio/reactivex/f;Lio/reactivex/v;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final b(Ljava/util/concurrent/Callable;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "+TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "completionValueSupplier is null"

    .line 2692
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2693
    new-instance v0, Lio/reactivex/d/e/a/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lio/reactivex/d/e/a/s;-><init>(Lio/reactivex/f;Ljava/util/concurrent/Callable;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final b()V
    .locals 1

    .line 1225
    new-instance v0, Lio/reactivex/d/d/f;

    invoke-direct {v0}, Lio/reactivex/d/d/f;-><init>()V

    .line 1226
    invoke-virtual {p0, v0}, Lio/reactivex/b;->a(Lio/reactivex/d;)V

    .line 1227
    invoke-virtual {v0}, Lio/reactivex/d/d/f;->b()Ljava/lang/Object;

    return-void
.end method

.method protected abstract b(Lio/reactivex/d;)V
.end method

.method public final c()Lio/reactivex/b;
    .locals 1

    .line 1908
    invoke-static {}, Lio/reactivex/d/b/a;->c()Lio/reactivex/c/k;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/k;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lio/reactivex/c/a;)Lio/reactivex/b;
    .locals 1

    const-string v0, "onFinally is null"

    .line 1690
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1691
    new-instance v0, Lio/reactivex/d/e/a/c;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/a/c;-><init>(Lio/reactivex/f;Lio/reactivex/c/a;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final d(Lio/reactivex/c/a;)Lio/reactivex/b/c;
    .locals 1

    const-string v0, "onComplete is null"

    .line 2407
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2409
    new-instance v0, Lio/reactivex/d/d/g;

    invoke-direct {v0, p1}, Lio/reactivex/d/d/g;-><init>(Lio/reactivex/c/a;)V

    .line 2410
    invoke-virtual {p0, v0}, Lio/reactivex/b;->a(Lio/reactivex/d;)V

    return-object v0
.end method

.method public final d()Lio/reactivex/b;
    .locals 1

    .line 2062
    invoke-virtual {p0}, Lio/reactivex/b;->e()Lio/reactivex/h;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/b;->a(Lorg/reactivestreams/Publisher;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lio/reactivex/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/h<",
            "TT;>;"
        }
    .end annotation

    .line 2623
    instance-of v0, p0, Lio/reactivex/d/c/a;

    if-eqz v0, :cond_0

    .line 2624
    move-object v0, p0

    check-cast v0, Lio/reactivex/d/c/a;

    invoke-interface {v0}, Lio/reactivex/d/c/a;->v_()Lio/reactivex/h;

    move-result-object v0

    return-object v0

    .line 2626
    :cond_0
    new-instance v0, Lio/reactivex/d/e/a/q;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/q;-><init>(Lio/reactivex/f;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/h;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .line 2646
    instance-of v0, p0, Lio/reactivex/d/c/b;

    if-eqz v0, :cond_0

    .line 2647
    move-object v0, p0

    check-cast v0, Lio/reactivex/d/c/b;

    invoke-interface {v0}, Lio/reactivex/d/c/b;->a()Lio/reactivex/j;

    move-result-object v0

    return-object v0

    .line 2649
    :cond_0
    new-instance v0, Lio/reactivex/d/e/c/m;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/c/m;-><init>(Lio/reactivex/f;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/j;)Lio/reactivex/j;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .line 2668
    instance-of v0, p0, Lio/reactivex/d/c/c;

    if-eqz v0, :cond_0

    .line 2669
    move-object v0, p0

    check-cast v0, Lio/reactivex/d/c/c;

    invoke-interface {v0}, Lio/reactivex/d/c/c;->w_()Lio/reactivex/o;

    move-result-object v0

    return-object v0

    .line 2671
    :cond_0
    new-instance v0, Lio/reactivex/d/e/a/r;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/r;-><init>(Lio/reactivex/f;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
