.class public abstract Lio/reactivex/w;
.super Ljava/lang/Object;
.source "Single.java"

# interfaces
.implements Lio/reactivex/aa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/aa<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1318
    invoke-static {}, Lio/reactivex/j/a;->a()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lio/reactivex/w;->a(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static a(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/v;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const-string v0, "unit is null"

    .line 1342
    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "scheduler is null"

    .line 1343
    invoke-static {p3, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1344
    new-instance v0, Lio/reactivex/d/e/f/t;

    invoke-direct {v0, p0, p1, p2, p3}, Lio/reactivex/d/e/f/t;-><init>(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/c/i;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/aa<",
            "+TT1;>;",
            "Lio/reactivex/aa<",
            "+TT2;>;",
            "Lio/reactivex/aa<",
            "+TT3;>;",
            "Lio/reactivex/c/i<",
            "-TT1;-TT2;-TT3;+TR;>;)",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "source1 is null"

    .line 1591
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "source2 is null"

    .line 1592
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "source3 is null"

    .line 1593
    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1594
    invoke-static {p3}, Lio/reactivex/d/b/a;->a(Lio/reactivex/c/i;)Lio/reactivex/c/h;

    move-result-object p3

    const/4 v0, 0x3

    new-array v0, v0, [Lio/reactivex/aa;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    const/4 p0, 0x2

    aput-object p2, v0, p0

    invoke-static {p3, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/c/c;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/aa<",
            "+TT1;>;",
            "Lio/reactivex/aa<",
            "+TT2;>;",
            "Lio/reactivex/c/c<",
            "-TT1;-TT2;+TR;>;)",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "source1 is null"

    .line 1551
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "source2 is null"

    .line 1552
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1553
    invoke-static {p2}, Lio/reactivex/d/b/a;->a(Lio/reactivex/c/c;)Lio/reactivex/c/h;

    move-result-object p2

    const/4 v0, 0x2

    new-array v0, v0, [Lio/reactivex/aa;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    invoke-static {p2, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static varargs a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h<",
            "-[",
            "Ljava/lang/Object;",
            "+TR;>;[",
            "Lio/reactivex/aa<",
            "+TT;>;)",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "zipper is null"

    .line 1969
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "sources is null"

    .line 1970
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1971
    array-length v0, p1

    if-nez v0, :cond_0

    .line 1972
    new-instance p0, Ljava/util/NoSuchElementException;

    invoke-direct {p0}, Ljava/util/NoSuchElementException;-><init>()V

    invoke-static {p0}, Lio/reactivex/w;->a(Ljava/lang/Throwable;)Lio/reactivex/w;

    move-result-object p0

    return-object p0

    .line 1974
    :cond_0
    new-instance v0, Lio/reactivex/d/e/f/w;

    invoke-direct {v0, p1, p0}, Lio/reactivex/d/e/f/w;-><init>([Lio/reactivex/aa;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lio/reactivex/h;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/h<",
            "TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    .line 4101
    new-instance v0, Lio/reactivex/d/e/b/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/reactivex/d/e/b/o;-><init>(Lio/reactivex/h;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lio/reactivex/z;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/z<",
            "TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "source is null"

    .line 518
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 519
    new-instance v0, Lio/reactivex/d/e/f/b;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/b;-><init>(Lio/reactivex/z;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "+",
            "Lio/reactivex/aa<",
            "+TT;>;>;",
            "Lio/reactivex/c/h<",
            "-[",
            "Ljava/lang/Object;",
            "+TR;>;)",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "zipper is null"

    .line 1515
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "sources is null"

    .line 1516
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1517
    new-instance v0, Lio/reactivex/d/e/f/x;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/x;-><init>(Ljava/lang/Iterable;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Throwable;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "exception is null"

    .line 587
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 588
    invoke-static {p0}, Lio/reactivex/d/b/a;->a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object p0

    invoke-static {p0}, Lio/reactivex/w;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "+",
            "Lio/reactivex/aa<",
            "+TT;>;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "singleSupplier is null"

    .line 540
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 541
    new-instance v0, Lio/reactivex/d/e/f/c;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/c;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/Object;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "item is null"

    .line 837
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 838
    new-instance v0, Lio/reactivex/d/e/f/n;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/n;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/util/concurrent/Callable;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "errorSupplier is null"

    .line 561
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 562
    new-instance v0, Lio/reactivex/d/e/f/i;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/i;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "+TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "callable is null"

    .line 621
    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 622
    new-instance v0, Lio/reactivex/d/e/f/m;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/m;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g<",
            "-TT;>;",
            "Lio/reactivex/c/g<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/b/c;"
        }
    .end annotation

    const-string v0, "onSuccess is null"

    .line 3585
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onError is null"

    .line 3586
    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3588
    new-instance v0, Lio/reactivex/d/d/h;

    invoke-direct {v0, p1, p2}, Lio/reactivex/d/d/h;-><init>(Lio/reactivex/c/g;Lio/reactivex/c/g;)V

    .line 3589
    invoke-virtual {p0, v0}, Lio/reactivex/w;->a(Lio/reactivex/y;)V

    return-object v0
.end method

.method public final a(Lio/reactivex/c/k;)Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/k<",
            "-TT;>;)",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "predicate is null"

    .line 2632
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2633
    new-instance v0, Lio/reactivex/d/e/c/h;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/c/h;-><init>(Lio/reactivex/aa;Lio/reactivex/c/k;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/j;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    .line 2082
    new-instance v0, Lio/reactivex/d/e/f/a;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/a;-><init>(Lio/reactivex/aa;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/c/a;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/a;",
            ")",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "onDispose is null"

    .line 2607
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2608
    new-instance v0, Lio/reactivex/d/e/f/e;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/e;-><init>(Lio/reactivex/aa;Lio/reactivex/c/a;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/c/b;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/b<",
            "-TT;-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "onEvent is null"

    .line 2562
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2563
    new-instance v0, Lio/reactivex/d/e/f/f;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/f;-><init>(Lio/reactivex/aa;Lio/reactivex/c/b;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/c/g;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g<",
            "-",
            "Lio/reactivex/b/c;",
            ">;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "onSubscribe is null"

    .line 2494
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2495
    new-instance v0, Lio/reactivex/d/e/f/g;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/g;-><init>(Lio/reactivex/aa;Lio/reactivex/c/g;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/c/h;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/aa<",
            "+TR;>;>;)",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "mapper is null"

    .line 2656
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2657
    new-instance v0, Lio/reactivex/d/e/f/j;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/j;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/v;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/v;",
            ")",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "scheduler is null"

    .line 3119
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3120
    new-instance v0, Lio/reactivex/d/e/f/p;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/p;-><init>(Lio/reactivex/aa;Lio/reactivex/v;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lio/reactivex/y;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/y<",
            "-TT;>;)V"
        }
    .end annotation

    const-string v0, "observer is null"

    .line 3596
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3598
    invoke-static {p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/w;Lio/reactivex/y;)Lio/reactivex/y;

    move-result-object p1

    const-string v0, "The RxJavaPlugins.onSubscribe hook returned a null SingleObserver. Please check the handler provided to RxJavaPlugins.setOnSingleSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins"

    .line 3600
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3603
    :try_start_0
    invoke-virtual {p0, p1}, Lio/reactivex/w;->b(Lio/reactivex/y;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 3607
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 3608
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "subscribeActual failed"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 3609
    invoke-virtual {v0, p1}, Ljava/lang/NullPointerException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 3610
    throw v0

    :catch_1
    move-exception p1

    .line 3605
    throw p1
.end method

.method public final b(Lio/reactivex/c/h;)Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/n<",
            "+TR;>;>;)",
            "Lio/reactivex/j<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "mapper is null"

    .line 2680
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2681
    new-instance v0, Lio/reactivex/d/e/f/l;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/l;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/j;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lio/reactivex/c/g;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g<",
            "-TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "onSuccess is null"

    .line 2543
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2544
    new-instance v0, Lio/reactivex/d/e/f/h;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/h;-><init>(Lio/reactivex/aa;Lio/reactivex/c/g;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lio/reactivex/v;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/v;",
            ")",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "scheduler is null"

    .line 3674
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3675
    new-instance v0, Lio/reactivex/d/e/f/s;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/s;-><init>(Lio/reactivex/aa;Lio/reactivex/v;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 2833
    new-instance v0, Lio/reactivex/d/d/f;

    invoke-direct {v0}, Lio/reactivex/d/d/f;-><init>()V

    .line 2834
    invoke-virtual {p0, v0}, Lio/reactivex/w;->a(Lio/reactivex/y;)V

    .line 2835
    invoke-virtual {v0}, Lio/reactivex/d/d/f;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(Lio/reactivex/y;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/y<",
            "-TT;>;)V"
        }
    .end annotation
.end method

.method public final c(Lio/reactivex/c/g;)Lio/reactivex/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g<",
            "-TT;>;)",
            "Lio/reactivex/b/c;"
        }
    .end annotation

    .line 3559
    sget-object v0, Lio/reactivex/d/b/a;->f:Lio/reactivex/c/g;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lio/reactivex/b;
    .locals 1

    .line 3919
    new-instance v0, Lio/reactivex/d/e/a/i;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/i;-><init>(Lio/reactivex/aa;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lio/reactivex/c/h;)Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/s<",
            "+TR;>;>;)",
            "Lio/reactivex/o<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "mapper is null"

    .line 2788
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2789
    new-instance v0, Lio/reactivex/d/e/d/f;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/d/f;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public final c(Ljava/lang/Object;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "value is null"

    .line 3173
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3174
    new-instance v0, Lio/reactivex/d/e/f/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lio/reactivex/d/e/f/q;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final c(Lio/reactivex/y;)Lio/reactivex/y;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/reactivex/y<",
            "-TT;>;>(TE;)TE;"
        }
    .end annotation

    .line 3650
    invoke-virtual {p0, p1}, Lio/reactivex/w;->a(Lio/reactivex/y;)V

    return-object p1
.end method

.method public final d(Lio/reactivex/c/h;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h<",
            "-TT;+",
            "Lio/reactivex/f;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "mapper is null"

    .line 2813
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2814
    new-instance v0, Lio/reactivex/d/e/f/k;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/k;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final d()Lio/reactivex/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h<",
            "TT;>;"
        }
    .end annotation

    .line 3940
    instance-of v0, p0, Lio/reactivex/d/c/a;

    if-eqz v0, :cond_0

    .line 3941
    move-object v0, p0

    check-cast v0, Lio/reactivex/d/c/a;

    invoke-interface {v0}, Lio/reactivex/d/c/a;->v_()Lio/reactivex/h;

    move-result-object v0

    return-object v0

    .line 3943
    :cond_0
    new-instance v0, Lio/reactivex/d/e/f/u;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/u;-><init>(Lio/reactivex/aa;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/h;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .line 3979
    instance-of v0, p0, Lio/reactivex/d/c/b;

    if-eqz v0, :cond_0

    .line 3980
    move-object v0, p0

    check-cast v0, Lio/reactivex/d/c/b;

    invoke-interface {v0}, Lio/reactivex/d/c/b;->a()Lio/reactivex/j;

    move-result-object v0

    return-object v0

    .line 3982
    :cond_0
    new-instance v0, Lio/reactivex/d/e/c/n;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/c/n;-><init>(Lio/reactivex/aa;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/j;)Lio/reactivex/j;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lio/reactivex/c/h;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h<",
            "-TT;+TR;>;)",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "mapper is null"

    .line 3007
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3008
    new-instance v0, Lio/reactivex/d/e/f/o;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/o;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final f()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .line 3999
    instance-of v0, p0, Lio/reactivex/d/c/c;

    if-eqz v0, :cond_0

    .line 4000
    move-object v0, p0

    check-cast v0, Lio/reactivex/d/c/c;

    invoke-interface {v0}, Lio/reactivex/d/c/c;->w_()Lio/reactivex/o;

    move-result-object v0

    return-object v0

    .line 4002
    :cond_0
    new-instance v0, Lio/reactivex/d/e/f/v;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/f/v;-><init>(Lio/reactivex/aa;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lio/reactivex/c/h;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h<",
            "Ljava/lang/Throwable;",
            "+TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "resumeFunction is null"

    .line 3153
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3154
    new-instance v0, Lio/reactivex/d/e/f/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lio/reactivex/d/e/f/q;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final g(Lio/reactivex/c/h;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h<",
            "-",
            "Ljava/lang/Throwable;",
            "+",
            "Lio/reactivex/aa<",
            "+TT;>;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "resumeFunctionInCaseOfError is null"

    .line 3245
    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3246
    new-instance v0, Lio/reactivex/d/e/f/r;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/f/r;-><init>(Lio/reactivex/aa;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final h(Lio/reactivex/c/h;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h<",
            "-",
            "Lio/reactivex/h<",
            "Ljava/lang/Throwable;",
            ">;+",
            "Lorg/reactivestreams/Publisher<",
            "*>;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    .line 3489
    invoke-virtual {p0}, Lio/reactivex/w;->d()Lio/reactivex/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/h;->b(Lio/reactivex/c/h;)Lio/reactivex/h;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->a(Lio/reactivex/h;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
