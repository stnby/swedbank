.class final Lretrofit2/adapter/rxjava2/b$a;
.super Ljava/lang/Object;
.source "CallEnqueueObservable.java"

# interfaces
.implements Lio/reactivex/b/c;
.implements Lretrofit2/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lretrofit2/adapter/rxjava2/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/c;",
        "Lretrofit2/d<",
        "TT;>;"
    }
.end annotation


# instance fields
.field a:Z

.field private final b:Lretrofit2/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/b<",
            "*>;"
        }
    .end annotation
.end field

.field private final c:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-",
            "Lretrofit2/q<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private volatile d:Z


# direct methods
.method constructor <init>(Lretrofit2/b;Lio/reactivex/u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/b<",
            "*>;",
            "Lio/reactivex/u<",
            "-",
            "Lretrofit2/q<",
            "TT;>;>;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lretrofit2/adapter/rxjava2/b$a;->a:Z

    .line 52
    iput-object p1, p0, Lretrofit2/adapter/rxjava2/b$a;->b:Lretrofit2/b;

    .line 53
    iput-object p2, p0, Lretrofit2/adapter/rxjava2/b$a;->c:Lio/reactivex/u;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    .line 93
    iput-boolean v0, p0, Lretrofit2/adapter/rxjava2/b$a;->d:Z

    .line 94
    iget-object v0, p0, Lretrofit2/adapter/rxjava2/b$a;->b:Lretrofit2/b;

    invoke-interface {v0}, Lretrofit2/b;->b()V

    return-void
.end method

.method public a(Lretrofit2/b;Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/b<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 82
    invoke-interface {p1}, Lretrofit2/b;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 85
    :cond_0
    :try_start_0
    iget-object p1, p0, Lretrofit2/adapter/rxjava2/b$a;->c:Lio/reactivex/u;

    invoke-interface {p1, p2}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 87
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 88
    new-instance v0, Lio/reactivex/exceptions/CompositeException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Throwable;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 p2, 0x1

    aput-object p1, v1, p2

    invoke-direct {v0, v1}, Lio/reactivex/exceptions/CompositeException;-><init>([Ljava/lang/Throwable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public a(Lretrofit2/b;Lretrofit2/q;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/b<",
            "TT;>;",
            "Lretrofit2/q<",
            "TT;>;)V"
        }
    .end annotation

    .line 57
    iget-boolean p1, p0, Lretrofit2/adapter/rxjava2/b$a;->d:Z

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x1

    .line 60
    :try_start_0
    iget-object v0, p0, Lretrofit2/adapter/rxjava2/b$a;->c:Lio/reactivex/u;

    invoke-interface {v0, p2}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V

    .line 62
    iget-boolean p2, p0, Lretrofit2/adapter/rxjava2/b$a;->d:Z

    if-nez p2, :cond_2

    .line 63
    iput-boolean p1, p0, Lretrofit2/adapter/rxjava2/b$a;->a:Z

    .line 64
    iget-object p2, p0, Lretrofit2/adapter/rxjava2/b$a;->c:Lio/reactivex/u;

    invoke-interface {p2}, Lio/reactivex/u;->onComplete()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 67
    invoke-static {p2}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 68
    iget-boolean v0, p0, Lretrofit2/adapter/rxjava2/b$a;->a:Z

    if-eqz v0, :cond_1

    .line 69
    invoke-static {p2}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 70
    :cond_1
    iget-boolean v0, p0, Lretrofit2/adapter/rxjava2/b$a;->d:Z

    if-nez v0, :cond_2

    .line 72
    :try_start_1
    iget-object v0, p0, Lretrofit2/adapter/rxjava2/b$a;->c:Lio/reactivex/u;

    invoke-interface {v0, p2}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    .line 74
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 75
    new-instance v1, Lio/reactivex/exceptions/CompositeException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Throwable;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    aput-object v0, v2, p1

    invoke-direct {v1, v2}, Lio/reactivex/exceptions/CompositeException;-><init>([Ljava/lang/Throwable;)V

    invoke-static {v1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public b()Z
    .locals 1

    .line 98
    iget-boolean v0, p0, Lretrofit2/adapter/rxjava2/b$a;->d:Z

    return v0
.end method
