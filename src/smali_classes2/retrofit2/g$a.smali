.class final Lretrofit2/g$a;
.super Ljava/lang/Object;
.source "DefaultCallAdapterFactory.java"

# interfaces
.implements Lretrofit2/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lretrofit2/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/b<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/Executor;

.field final b:Lretrofit2/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/b<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Lretrofit2/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lretrofit2/b<",
            "TT;>;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lretrofit2/g$a;->a:Ljava/util/concurrent/Executor;

    .line 69
    iput-object p2, p0, Lretrofit2/g$a;->b:Lretrofit2/b;

    return-void
.end method


# virtual methods
.method public a()Lretrofit2/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/q<",
            "TT;>;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lretrofit2/g$a;->b:Lretrofit2/b;

    invoke-interface {v0}, Lretrofit2/b;->a()Lretrofit2/q;

    move-result-object v0

    return-object v0
.end method

.method public a(Lretrofit2/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/d<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "callback == null"

    .line 73
    invoke-static {p1, v0}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lretrofit2/g$a;->b:Lretrofit2/b;

    new-instance v1, Lretrofit2/g$a$1;

    invoke-direct {v1, p0, p1}, Lretrofit2/g$a$1;-><init>(Lretrofit2/g$a;Lretrofit2/d;)V

    invoke-interface {v0, v1}, Lretrofit2/b;->a(Lretrofit2/d;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 108
    iget-object v0, p0, Lretrofit2/g$a;->b:Lretrofit2/b;

    invoke-interface {v0}, Lretrofit2/b;->b()V

    return-void
.end method

.method public c()Z
    .locals 1

    .line 112
    iget-object v0, p0, Lretrofit2/g$a;->b:Lretrofit2/b;

    invoke-interface {v0}, Lretrofit2/b;->c()Z

    move-result v0

    return v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 63
    invoke-virtual {p0}, Lretrofit2/g$a;->d()Lretrofit2/b;

    move-result-object v0

    return-object v0
.end method

.method public d()Lretrofit2/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/b<",
            "TT;>;"
        }
    .end annotation

    .line 117
    new-instance v0, Lretrofit2/g$a;

    iget-object v1, p0, Lretrofit2/g$a;->a:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lretrofit2/g$a;->b:Lretrofit2/b;

    invoke-interface {v2}, Lretrofit2/b;->d()Lretrofit2/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lretrofit2/g$a;-><init>(Ljava/util/concurrent/Executor;Lretrofit2/b;)V

    return-object v0
.end method

.method public e()Lokhttp3/aa;
    .locals 1

    .line 121
    iget-object v0, p0, Lretrofit2/g$a;->b:Lretrofit2/b;

    invoke-interface {v0}, Lretrofit2/b;->e()Lokhttp3/aa;

    move-result-object v0

    return-object v0
.end method
