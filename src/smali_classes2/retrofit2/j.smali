.class public final Lretrofit2/j;
.super Ljava/lang/Object;
.source "KotlinExtensions.kt"


# direct methods
.method public static final a(Lretrofit2/b;Lkotlin/c/c;)Ljava/lang/Object;
    .locals 3
    .param p0    # Lretrofit2/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/b<",
            "TT;>;",
            "Lkotlin/c/c<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 100
    new-instance v0, Lkotlinx/coroutines/f;

    invoke-static {p1}, Lkotlin/c/a/b;->a(Lkotlin/c/c;)Lkotlin/c/c;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lkotlinx/coroutines/f;-><init>(Lkotlin/c/c;I)V

    .line 104
    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/e;

    .line 29
    new-instance v2, Lretrofit2/j$a;

    invoke-direct {v2, p0}, Lretrofit2/j$a;-><init>(Lretrofit2/b;)V

    check-cast v2, Lkotlin/e/a/b;

    invoke-interface {v1, v2}, Lkotlinx/coroutines/e;->a(Lkotlin/e/a/b;)V

    .line 32
    new-instance v2, Lretrofit2/j$c;

    invoke-direct {v2, v1}, Lretrofit2/j$c;-><init>(Lkotlinx/coroutines/e;)V

    check-cast v2, Lretrofit2/d;

    invoke-interface {p0, v2}, Lretrofit2/b;->a(Lretrofit2/d;)V

    .line 105
    invoke-virtual {v0}, Lkotlinx/coroutines/f;->g()Ljava/lang/Object;

    move-result-object p0

    .line 99
    invoke-static {}, Lkotlin/c/a/b;->a()Ljava/lang/Object;

    move-result-object v0

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lkotlin/c/b/a/g;->b(Lkotlin/c/c;)V

    :cond_0
    return-object p0
.end method

.method public static final b(Lretrofit2/b;Lkotlin/c/c;)Ljava/lang/Object;
    .locals 3
    .param p0    # Lretrofit2/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/b<",
            "TT;>;",
            "Lkotlin/c/c<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 108
    new-instance v0, Lkotlinx/coroutines/f;

    invoke-static {p1}, Lkotlin/c/a/b;->a(Lkotlin/c/c;)Lkotlin/c/c;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lkotlinx/coroutines/f;-><init>(Lkotlin/c/c;I)V

    .line 112
    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/e;

    .line 63
    new-instance v2, Lretrofit2/j$b;

    invoke-direct {v2, p0}, Lretrofit2/j$b;-><init>(Lretrofit2/b;)V

    check-cast v2, Lkotlin/e/a/b;

    invoke-interface {v1, v2}, Lkotlinx/coroutines/e;->a(Lkotlin/e/a/b;)V

    .line 66
    new-instance v2, Lretrofit2/j$d;

    invoke-direct {v2, v1}, Lretrofit2/j$d;-><init>(Lkotlinx/coroutines/e;)V

    check-cast v2, Lretrofit2/d;

    invoke-interface {p0, v2}, Lretrofit2/b;->a(Lretrofit2/d;)V

    .line 113
    invoke-virtual {v0}, Lkotlinx/coroutines/f;->g()Ljava/lang/Object;

    move-result-object p0

    .line 107
    invoke-static {}, Lkotlin/c/a/b;->a()Ljava/lang/Object;

    move-result-object v0

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lkotlin/c/b/a/g;->b(Lkotlin/c/c;)V

    :cond_0
    return-object p0
.end method

.method public static final c(Lretrofit2/b;Lkotlin/c/c;)Ljava/lang/Object;
    .locals 3
    .param p0    # Lretrofit2/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/b<",
            "TT;>;",
            "Lkotlin/c/c<",
            "-",
            "Lretrofit2/q<",
            "TT;>;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 116
    new-instance v0, Lkotlinx/coroutines/f;

    invoke-static {p1}, Lkotlin/c/a/b;->a(Lkotlin/c/c;)Lkotlin/c/c;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lkotlinx/coroutines/f;-><init>(Lkotlin/c/c;I)V

    .line 120
    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/e;

    .line 84
    new-instance v2, Lretrofit2/j$e;

    invoke-direct {v2, p0}, Lretrofit2/j$e;-><init>(Lretrofit2/b;)V

    check-cast v2, Lkotlin/e/a/b;

    invoke-interface {v1, v2}, Lkotlinx/coroutines/e;->a(Lkotlin/e/a/b;)V

    .line 87
    new-instance v2, Lretrofit2/j$f;

    invoke-direct {v2, v1}, Lretrofit2/j$f;-><init>(Lkotlinx/coroutines/e;)V

    check-cast v2, Lretrofit2/d;

    invoke-interface {p0, v2}, Lretrofit2/b;->a(Lretrofit2/d;)V

    .line 121
    invoke-virtual {v0}, Lkotlinx/coroutines/f;->g()Ljava/lang/Object;

    move-result-object p0

    .line 115
    invoke-static {}, Lkotlin/c/a/b;->a()Ljava/lang/Object;

    move-result-object v0

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lkotlin/c/b/a/g;->b(Lkotlin/c/c;)V

    :cond_0
    return-object p0
.end method
