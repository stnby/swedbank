.class final Lretrofit2/h$b;
.super Lretrofit2/h;
.source "HttpServiceMethod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lretrofit2/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResponseT:",
        "Ljava/lang/Object;",
        ">",
        "Lretrofit2/h<",
        "TResponseT;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lretrofit2/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/c<",
            "TResponseT;",
            "Lretrofit2/b<",
            "TResponseT;>;>;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method constructor <init>(Lretrofit2/p;Lokhttp3/e$a;Lretrofit2/f;Lretrofit2/c;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/p;",
            "Lokhttp3/e$a;",
            "Lretrofit2/f<",
            "Lokhttp3/ad;",
            "TResponseT;>;",
            "Lretrofit2/c<",
            "TResponseT;",
            "Lretrofit2/b<",
            "TResponseT;>;>;Z)V"
        }
    .end annotation

    .line 179
    invoke-direct {p0, p1, p2, p3}, Lretrofit2/h;-><init>(Lretrofit2/p;Lokhttp3/e$a;Lretrofit2/f;)V

    .line 180
    iput-object p4, p0, Lretrofit2/h$b;->a:Lretrofit2/c;

    .line 181
    iput-boolean p5, p0, Lretrofit2/h$b;->b:Z

    return-void
.end method


# virtual methods
.method protected a(Lretrofit2/b;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/b<",
            "TResponseT;>;[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lretrofit2/h$b;->a:Lretrofit2/c;

    invoke-interface {v0, p1}, Lretrofit2/c;->a(Lretrofit2/b;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lretrofit2/b;

    .line 188
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    aget-object p2, p2, v0

    check-cast p2, Lkotlin/c/c;

    .line 189
    iget-boolean v0, p0, Lretrofit2/h$b;->b:Z

    if-eqz v0, :cond_0

    .line 190
    invoke-static {p1, p2}, Lretrofit2/j;->b(Lretrofit2/b;Lkotlin/c/c;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 191
    :cond_0
    invoke-static {p1, p2}, Lretrofit2/j;->a(Lretrofit2/b;Lkotlin/c/c;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    return-object p1
.end method
