.class final Lretrofit2/k;
.super Ljava/lang/Object;
.source "OkHttpCall.java"

# interfaces
.implements Lretrofit2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lretrofit2/k$a;,
        Lretrofit2/k$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/b<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lretrofit2/p;

.field private final b:[Ljava/lang/Object;

.field private final c:Lokhttp3/e$a;

.field private final d:Lretrofit2/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/f<",
            "Lokhttp3/ad;",
            "TT;>;"
        }
    .end annotation
.end field

.field private volatile e:Z

.field private f:Lokhttp3/e;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Ljava/lang/Throwable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lretrofit2/p;[Ljava/lang/Object;Lokhttp3/e$a;Lretrofit2/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/p;",
            "[",
            "Ljava/lang/Object;",
            "Lokhttp3/e$a;",
            "Lretrofit2/f<",
            "Lokhttp3/ad;",
            "TT;>;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lretrofit2/k;->a:Lretrofit2/p;

    .line 50
    iput-object p2, p0, Lretrofit2/k;->b:[Ljava/lang/Object;

    .line 51
    iput-object p3, p0, Lretrofit2/k;->c:Lokhttp3/e$a;

    .line 52
    iput-object p4, p0, Lretrofit2/k;->d:Lretrofit2/f;

    return-void
.end method

.method private g()Lokhttp3/e;
    .locals 3

    .line 192
    iget-object v0, p0, Lretrofit2/k;->c:Lokhttp3/e$a;

    iget-object v1, p0, Lretrofit2/k;->a:Lretrofit2/p;

    iget-object v2, p0, Lretrofit2/k;->b:[Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lretrofit2/p;->a([Ljava/lang/Object;)Lokhttp3/aa;

    move-result-object v1

    invoke-interface {v0, v1}, Lokhttp3/e$a;->a(Lokhttp3/aa;)Lokhttp3/e;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 194
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Call.Factory returned null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()Lretrofit2/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/q<",
            "TT;>;"
        }
    .end annotation

    .line 158
    monitor-enter p0

    .line 159
    :try_start_0
    iget-boolean v0, p0, Lretrofit2/k;->h:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 160
    iput-boolean v0, p0, Lretrofit2/k;->h:Z

    .line 162
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/io/IOException;

    if-nez v0, :cond_1

    .line 165
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 168
    :cond_0
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 164
    :cond_1
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 172
    :cond_2
    iget-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    .line 175
    :try_start_1
    invoke-direct {p0}, Lretrofit2/k;->g()Lokhttp3/e;

    move-result-object v0

    iput-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 177
    :try_start_2
    invoke-static {v0}, Lretrofit2/v;->a(Ljava/lang/Throwable;)V

    .line 178
    iput-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    .line 179
    throw v0

    .line 182
    :cond_3
    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 184
    iget-boolean v1, p0, Lretrofit2/k;->e:Z

    if-eqz v1, :cond_4

    .line 185
    invoke-interface {v0}, Lokhttp3/e;->c()V

    .line 188
    :cond_4
    invoke-interface {v0}, Lokhttp3/e;->b()Lokhttp3/ac;

    move-result-object v0

    invoke-virtual {p0, v0}, Lretrofit2/k;->a(Lokhttp3/ac;)Lretrofit2/q;

    move-result-object v0

    return-object v0

    .line 159
    :cond_5
    :try_start_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already executed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    .line 182
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method a(Lokhttp3/ac;)Lretrofit2/q;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/ac;",
            ")",
            "Lretrofit2/q<",
            "TT;>;"
        }
    .end annotation

    .line 200
    invoke-virtual {p1}, Lokhttp3/ac;->h()Lokhttp3/ad;

    move-result-object v0

    .line 203
    invoke-virtual {p1}, Lokhttp3/ac;->i()Lokhttp3/ac$a;

    move-result-object p1

    new-instance v1, Lretrofit2/k$b;

    .line 204
    invoke-virtual {v0}, Lokhttp3/ad;->a()Lokhttp3/v;

    move-result-object v2

    invoke-virtual {v0}, Lokhttp3/ad;->b()J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lretrofit2/k$b;-><init>(Lokhttp3/v;J)V

    invoke-virtual {p1, v1}, Lokhttp3/ac$a;->a(Lokhttp3/ad;)Lokhttp3/ac$a;

    move-result-object p1

    .line 205
    invoke-virtual {p1}, Lokhttp3/ac$a;->a()Lokhttp3/ac;

    move-result-object p1

    .line 207
    invoke-virtual {p1}, Lokhttp3/ac;->c()I

    move-result v1

    const/16 v2, 0xc8

    if-lt v1, v2, :cond_3

    const/16 v2, 0x12c

    if-lt v1, v2, :cond_0

    goto :goto_1

    :cond_0
    const/16 v2, 0xcc

    if-eq v1, v2, :cond_2

    const/16 v2, 0xcd

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 223
    :cond_1
    new-instance v1, Lretrofit2/k$a;

    invoke-direct {v1, v0}, Lretrofit2/k$a;-><init>(Lokhttp3/ad;)V

    .line 225
    :try_start_0
    iget-object v0, p0, Lretrofit2/k;->d:Lretrofit2/f;

    invoke-interface {v0, v1}, Lretrofit2/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 226
    invoke-static {v0, p1}, Lretrofit2/q;->a(Ljava/lang/Object;Lokhttp3/ac;)Lretrofit2/q;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 230
    invoke-virtual {v1}, Lretrofit2/k$a;->e()V

    .line 231
    throw p1

    .line 219
    :cond_2
    :goto_0
    invoke-virtual {v0}, Lokhttp3/ad;->close()V

    const/4 v0, 0x0

    .line 220
    invoke-static {v0, p1}, Lretrofit2/q;->a(Ljava/lang/Object;Lokhttp3/ac;)Lretrofit2/q;

    move-result-object p1

    return-object p1

    .line 211
    :cond_3
    :goto_1
    :try_start_1
    invoke-static {v0}, Lretrofit2/v;->a(Lokhttp3/ad;)Lokhttp3/ad;

    move-result-object v1

    .line 212
    invoke-static {v1, p1}, Lretrofit2/q;->a(Lokhttp3/ad;Lokhttp3/ac;)Lretrofit2/q;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    invoke-virtual {v0}, Lokhttp3/ad;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Lokhttp3/ad;->close()V

    throw p1
.end method

.method public a(Lretrofit2/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/d<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "callback == null"

    .line 87
    invoke-static {p1, v0}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 92
    monitor-enter p0

    .line 93
    :try_start_0
    iget-boolean v0, p0, Lretrofit2/k;->h:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 94
    iput-boolean v0, p0, Lretrofit2/k;->h:Z

    .line 96
    iget-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;

    .line 97
    iget-object v1, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 100
    :try_start_1
    invoke-direct {p0}, Lretrofit2/k;->g()Lokhttp3/e;

    move-result-object v2

    iput-object v2, p0, Lretrofit2/k;->f:Lokhttp3/e;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    .line 102
    :try_start_2
    invoke-static {v1}, Lretrofit2/v;->a(Ljava/lang/Throwable;)V

    .line 103
    iput-object v1, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    .line 106
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    .line 109
    invoke-interface {p1, p0, v1}, Lretrofit2/d;->a(Lretrofit2/b;Ljava/lang/Throwable;)V

    return-void

    .line 113
    :cond_1
    iget-boolean v1, p0, Lretrofit2/k;->e:Z

    if-eqz v1, :cond_2

    .line 114
    invoke-interface {v0}, Lokhttp3/e;->c()V

    .line 117
    :cond_2
    new-instance v1, Lretrofit2/k$1;

    invoke-direct {v1, p0, p1}, Lretrofit2/k$1;-><init>(Lretrofit2/k;Lretrofit2/d;)V

    invoke-interface {v0, v1}, Lokhttp3/e;->a(Lokhttp3/f;)V

    return-void

    .line 93
    :cond_3
    :try_start_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Already executed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 106
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    .line 236
    iput-boolean v0, p0, Lretrofit2/k;->e:Z

    .line 239
    monitor-enter p0

    .line 240
    :try_start_0
    iget-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;

    .line 241
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 243
    invoke-interface {v0}, Lokhttp3/e;->c()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    .line 241
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 2

    .line 248
    iget-boolean v0, p0, Lretrofit2/k;->e:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 251
    :cond_0
    monitor-enter p0

    .line 252
    :try_start_0
    iget-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;

    invoke-interface {v0}, Lokhttp3/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    .line 253
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lretrofit2/k;->f()Lretrofit2/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lretrofit2/b;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lretrofit2/k;->f()Lretrofit2/k;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized e()Lokhttp3/aa;
    .locals 3

    monitor-enter p0

    .line 61
    :try_start_0
    iget-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;

    if-eqz v0, :cond_0

    .line 63
    invoke-interface {v0}, Lokhttp3/e;->a()Lokhttp3/aa;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 65
    :cond_0
    :try_start_1
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/io/IOException;

    if-nez v0, :cond_2

    .line 68
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 71
    :cond_1
    iget-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 67
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to create request."

    iget-object v2, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    invoke-direct {v0, v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lretrofit2/k;->g()Lokhttp3/e;

    move-result-object v0

    iput-object v0, p0, Lretrofit2/k;->f:Lokhttp3/e;

    invoke-interface {v0}, Lokhttp3/e;->a()Lokhttp3/aa;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    .line 81
    :try_start_3
    iput-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    .line 82
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to create request."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    .line 77
    invoke-static {v0}, Lretrofit2/v;->a(Ljava/lang/Throwable;)V

    .line 78
    iput-object v0, p0, Lretrofit2/k;->g:Ljava/lang/Throwable;

    .line 79
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    .line 60
    monitor-exit p0

    throw v0
.end method

.method public f()Lretrofit2/k;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/k<",
            "TT;>;"
        }
    .end annotation

    .line 57
    new-instance v0, Lretrofit2/k;

    iget-object v1, p0, Lretrofit2/k;->a:Lretrofit2/p;

    iget-object v2, p0, Lretrofit2/k;->b:[Ljava/lang/Object;

    iget-object v3, p0, Lretrofit2/k;->c:Lokhttp3/e$a;

    iget-object v4, p0, Lretrofit2/k;->d:Lretrofit2/f;

    invoke-direct {v0, v1, v2, v3, v4}, Lretrofit2/k;-><init>(Lretrofit2/p;[Ljava/lang/Object;Lokhttp3/e$a;Lretrofit2/f;)V

    return-object v0
.end method
