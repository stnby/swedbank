.class public final Lretrofit2/r$a;
.super Ljava/lang/Object;
.source "Retrofit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lretrofit2/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lretrofit2/n;

.field private b:Lokhttp3/e$a;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Lokhttp3/t;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lretrofit2/f$a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lretrofit2/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/concurrent/Executor;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 408
    invoke-static {}, Lretrofit2/n;->a()Lretrofit2/n;

    move-result-object v0

    invoke-direct {p0, v0}, Lretrofit2/r$a;-><init>(Lretrofit2/n;)V

    return-void
.end method

.method constructor <init>(Lretrofit2/n;)V
    .locals 1

    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lretrofit2/r$a;->d:Ljava/util/List;

    .line 399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lretrofit2/r$a;->e:Ljava/util/List;

    .line 404
    iput-object p1, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    return-void
.end method

.method constructor <init>(Lretrofit2/r;)V
    .locals 4

    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lretrofit2/r$a;->d:Ljava/util/List;

    .line 399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lretrofit2/r$a;->e:Ljava/util/List;

    .line 412
    invoke-static {}, Lretrofit2/n;->a()Lretrofit2/n;

    move-result-object v0

    iput-object v0, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    .line 413
    iget-object v0, p1, Lretrofit2/r;->a:Lokhttp3/e$a;

    iput-object v0, p0, Lretrofit2/r$a;->b:Lokhttp3/e$a;

    .line 414
    iget-object v0, p1, Lretrofit2/r;->b:Lokhttp3/t;

    iput-object v0, p0, Lretrofit2/r$a;->c:Lokhttp3/t;

    .line 418
    iget-object v0, p1, Lretrofit2/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    invoke-virtual {v1}, Lretrofit2/n;->e()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x1

    :goto_0
    if-ge v1, v0, :cond_0

    .line 420
    iget-object v2, p0, Lretrofit2/r$a;->d:Ljava/util/List;

    iget-object v3, p1, Lretrofit2/r;->c:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 425
    iget-object v1, p1, Lretrofit2/r;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    invoke-virtual {v2}, Lretrofit2/n;->c()I

    move-result v2

    sub-int/2addr v1, v2

    :goto_1
    if-ge v0, v1, :cond_1

    .line 427
    iget-object v2, p0, Lretrofit2/r$a;->e:Ljava/util/List;

    iget-object v3, p1, Lretrofit2/r;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 430
    :cond_1
    iget-object v0, p1, Lretrofit2/r;->e:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lretrofit2/r$a;->f:Ljava/util/concurrent/Executor;

    .line 431
    iget-boolean p1, p1, Lretrofit2/r;->f:Z

    iput-boolean p1, p0, Lretrofit2/r$a;->g:Z

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lretrofit2/f$a;",
            ">;"
        }
    .end annotation

    .line 567
    iget-object v0, p0, Lretrofit2/r$a;->d:Ljava/util/List;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lretrofit2/r$a;
    .locals 1

    const-string v0, "baseUrl == null"

    .line 469
    invoke-static {p1, v0}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 470
    invoke-static {p1}, Lokhttp3/t;->g(Ljava/lang/String;)Lokhttp3/t;

    move-result-object p1

    invoke-virtual {p0, p1}, Lretrofit2/r$a;->a(Lokhttp3/t;)Lretrofit2/r$a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lokhttp3/e$a;)Lretrofit2/r$a;
    .locals 1

    const-string v0, "factory == null"

    .line 449
    invoke-static {p1, v0}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lokhttp3/e$a;

    iput-object p1, p0, Lretrofit2/r$a;->b:Lokhttp3/e$a;

    return-object p0
.end method

.method public a(Lokhttp3/t;)Lretrofit2/r$a;
    .locals 3

    const-string v0, "baseUrl == null"

    .line 524
    invoke-static {p1, v0}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 525
    invoke-virtual {p1}, Lokhttp3/t;->j()Ljava/util/List;

    move-result-object v0

    const-string v1, ""

    .line 526
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    iput-object p1, p0, Lretrofit2/r$a;->c:Lokhttp3/t;

    return-object p0

    .line 527
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "baseUrl must end in /: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lokhttp3/x;)Lretrofit2/r$a;
    .locals 1

    const-string v0, "client == null"

    .line 440
    invoke-static {p1, v0}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lokhttp3/e$a;

    invoke-virtual {p0, p1}, Lretrofit2/r$a;->a(Lokhttp3/e$a;)Lretrofit2/r$a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lretrofit2/c$a;)Lretrofit2/r$a;
    .locals 2

    .line 544
    iget-object v0, p0, Lretrofit2/r$a;->e:Ljava/util/List;

    const-string v1, "factory == null"

    invoke-static {p1, v1}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lretrofit2/f$a;)Lretrofit2/r$a;
    .locals 2

    .line 535
    iget-object v0, p0, Lretrofit2/r$a;->d:Ljava/util/List;

    const-string v1, "factory == null"

    invoke-static {p1, v1}, Lretrofit2/v;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public b()Lretrofit2/r;
    .locals 9

    .line 586
    iget-object v0, p0, Lretrofit2/r$a;->c:Lokhttp3/t;

    if-eqz v0, :cond_2

    .line 590
    iget-object v0, p0, Lretrofit2/r$a;->b:Lokhttp3/e$a;

    if-nez v0, :cond_0

    .line 592
    new-instance v0, Lokhttp3/x;

    invoke-direct {v0}, Lokhttp3/x;-><init>()V

    :cond_0
    move-object v2, v0

    .line 595
    iget-object v0, p0, Lretrofit2/r$a;->f:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_1

    .line 597
    iget-object v0, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    invoke-virtual {v0}, Lretrofit2/n;->b()Ljava/util/concurrent/Executor;

    move-result-object v0

    :cond_1
    move-object v6, v0

    .line 601
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lretrofit2/r$a;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 602
    iget-object v1, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    invoke-virtual {v1, v6}, Lretrofit2/n;->a(Ljava/util/concurrent/Executor;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 605
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lretrofit2/r$a;->d:Ljava/util/List;

    .line 606
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    invoke-virtual {v4}, Lretrofit2/n;->e()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 610
    new-instance v3, Lretrofit2/a;

    invoke-direct {v3}, Lretrofit2/a;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 611
    iget-object v3, p0, Lretrofit2/r$a;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 612
    iget-object v3, p0, Lretrofit2/r$a;->a:Lretrofit2/n;

    invoke-virtual {v3}, Lretrofit2/n;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 614
    new-instance v8, Lretrofit2/r;

    iget-object v3, p0, Lretrofit2/r$a;->c:Lokhttp3/t;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 615
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iget-boolean v7, p0, Lretrofit2/r$a;->g:Z

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lretrofit2/r;-><init>(Lokhttp3/e$a;Lokhttp3/t;Ljava/util/List;Ljava/util/List;Ljava/util/concurrent/Executor;Z)V

    return-object v8

    .line 587
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Base URL required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
