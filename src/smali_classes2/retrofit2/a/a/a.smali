.class public final Lretrofit2/a/a/a;
.super Lretrofit2/f$a;
.source "MoshiConverterFactory.java"


# instance fields
.field private final a:Lcom/squareup/moshi/n;

.field private final b:Z

.field private final c:Z

.field private final d:Z


# direct methods
.method private constructor <init>(Lcom/squareup/moshi/n;ZZZ)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lretrofit2/f$a;-><init>()V

    .line 65
    iput-object p1, p0, Lretrofit2/a/a/a;->a:Lcom/squareup/moshi/n;

    .line 66
    iput-boolean p2, p0, Lretrofit2/a/a/a;->b:Z

    .line 67
    iput-boolean p3, p0, Lretrofit2/a/a/a;->c:Z

    .line 68
    iput-boolean p4, p0, Lretrofit2/a/a/a;->d:Z

    return-void
.end method

.method private static a([Ljava/lang/annotation/Annotation;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Ljava/util/Set<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .line 121
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget-object v3, p0, v2

    .line 122
    invoke-interface {v3}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/squareup/moshi/f;

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v1, :cond_0

    .line 123
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 124
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    .line 127
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    goto :goto_1

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method public static a(Lcom/squareup/moshi/n;)Lretrofit2/a/a/a;
    .locals 2

    if-eqz p0, :cond_0

    .line 55
    new-instance v0, Lretrofit2/a/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v1, v1}, Lretrofit2/a/a/a;-><init>(Lcom/squareup/moshi/n;ZZZ)V

    return-object v0

    .line 54
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v0, "moshi == null"

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lretrofit2/r;)Lretrofit2/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lretrofit2/r;",
            ")",
            "Lretrofit2/f<",
            "Lokhttp3/ad;",
            "*>;"
        }
    .end annotation

    .line 91
    iget-object p3, p0, Lretrofit2/a/a/a;->a:Lcom/squareup/moshi/n;

    invoke-static {p2}, Lretrofit2/a/a/a;->a([Ljava/lang/annotation/Annotation;)Ljava/util/Set;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 92
    iget-boolean p2, p0, Lretrofit2/a/a/a;->b:Z

    if-eqz p2, :cond_0

    .line 93
    invoke-virtual {p1}, Lcom/squareup/moshi/JsonAdapter;->e()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 95
    :cond_0
    iget-boolean p2, p0, Lretrofit2/a/a/a;->c:Z

    if-eqz p2, :cond_1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/moshi/JsonAdapter;->f()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 98
    :cond_1
    iget-boolean p2, p0, Lretrofit2/a/a/a;->d:Z

    if-eqz p2, :cond_2

    .line 99
    invoke-virtual {p1}, Lcom/squareup/moshi/JsonAdapter;->c()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 101
    :cond_2
    new-instance p2, Lretrofit2/a/a/c;

    invoke-direct {p2, p1}, Lretrofit2/a/a/c;-><init>(Lcom/squareup/moshi/JsonAdapter;)V

    return-object p2
.end method

.method public a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lretrofit2/r;)Lretrofit2/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lretrofit2/r;",
            ")",
            "Lretrofit2/f<",
            "*",
            "Lokhttp3/ab;",
            ">;"
        }
    .end annotation

    .line 106
    iget-object p3, p0, Lretrofit2/a/a/a;->a:Lcom/squareup/moshi/n;

    invoke-static {p2}, Lretrofit2/a/a/a;->a([Ljava/lang/annotation/Annotation;)Ljava/util/Set;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 107
    iget-boolean p2, p0, Lretrofit2/a/a/a;->b:Z

    if-eqz p2, :cond_0

    .line 108
    invoke-virtual {p1}, Lcom/squareup/moshi/JsonAdapter;->e()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 110
    :cond_0
    iget-boolean p2, p0, Lretrofit2/a/a/a;->c:Z

    if-eqz p2, :cond_1

    .line 111
    invoke-virtual {p1}, Lcom/squareup/moshi/JsonAdapter;->f()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 113
    :cond_1
    iget-boolean p2, p0, Lretrofit2/a/a/a;->d:Z

    if-eqz p2, :cond_2

    .line 114
    invoke-virtual {p1}, Lcom/squareup/moshi/JsonAdapter;->c()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 116
    :cond_2
    new-instance p2, Lretrofit2/a/a/b;

    invoke-direct {p2, p1}, Lretrofit2/a/a/b;-><init>(Lcom/squareup/moshi/JsonAdapter;)V

    return-object p2
.end method
