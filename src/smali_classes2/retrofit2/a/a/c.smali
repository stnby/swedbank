.class final Lretrofit2/a/a/c;
.super Ljava/lang/Object;
.source "MoshiResponseBodyConverter.java"

# interfaces
.implements Lretrofit2/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/f<",
        "Lokhttp3/ad;",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final a:Lb/f;


# instance fields
.field private final b:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "EFBBBF"

    .line 28
    invoke-static {v0}, Lb/f;->c(Ljava/lang/String;)Lb/f;

    move-result-object v0

    sput-object v0, Lretrofit2/a/a/c;->a:Lb/f;

    return-void
.end method

.method constructor <init>(Lcom/squareup/moshi/JsonAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/JsonAdapter<",
            "TT;>;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lretrofit2/a/a/c;->b:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lokhttp3/ad;

    invoke-virtual {p0, p1}, Lretrofit2/a/a/c;->a(Lokhttp3/ad;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Lokhttp3/ad;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/ad;",
            ")TT;"
        }
    .end annotation

    .line 37
    invoke-virtual {p1}, Lokhttp3/ad;->c()Lb/e;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 41
    :try_start_0
    sget-object v3, Lretrofit2/a/a/c;->a:Lb/f;

    invoke-interface {v0, v1, v2, v3}, Lb/e;->a(JLb/f;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    sget-object v1, Lretrofit2/a/a/c;->a:Lb/f;

    invoke-virtual {v1}, Lb/f;->h()I

    move-result v1

    int-to-long v1, v1

    invoke-interface {v0, v1, v2}, Lb/e;->i(J)V

    .line 44
    :cond_0
    invoke-static {v0}, Lcom/squareup/moshi/g;->a(Lb/e;)Lcom/squareup/moshi/g;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lretrofit2/a/a/c;->b:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v1

    .line 46
    invoke-virtual {v0}, Lcom/squareup/moshi/g;->h()Lcom/squareup/moshi/g$b;

    move-result-object v0

    sget-object v2, Lcom/squareup/moshi/g$b;->j:Lcom/squareup/moshi/g$b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_1

    .line 51
    invoke-virtual {p1}, Lokhttp3/ad;->close()V

    return-object v1

    .line 47
    :cond_1
    :try_start_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    const-string v1, "JSON document was not fully consumed."

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    .line 51
    invoke-virtual {p1}, Lokhttp3/ad;->close()V

    throw v0
.end method
