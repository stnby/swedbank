.class public interface abstract Lcom/meawallet/mtp/MeaContactlessTransactionListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onAuthenticationRequired(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
.end method

.method public abstract onContactlessPaymentFailure(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
.end method

.method public abstract onContactlessPaymentSubmitted(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
.end method
