.class Lcom/meawallet/mtp/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "aw"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;
    .locals 0

    .line 20
    invoke-static {p0}, Lcom/meawallet/mtp/aw;->b(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lcom/meawallet/mtp/MeaCryptoException;)Lcom/meawallet/mtp/cy;
    .locals 2

    .line 60
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->isRootedDeviceDetected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x7d1

    .line 63
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getRootedReasonErrorCode()Lcom/meawallet/mtp/MeaCryptoException$a;

    move-result-object p0

    .line 65
    sget-object v1, Lcom/meawallet/mtp/aw$1;->a:[I

    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException$a;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x7e1

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x7e0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x7df

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x7de

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x7dd

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x7dc

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x7db

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x7d9

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x7d8

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x7d7

    goto :goto_0

    :pswitch_a
    const/16 v0, 0x7d6

    goto :goto_0

    :pswitch_b
    const/16 v0, 0x7d5

    goto :goto_0

    :pswitch_c
    const/16 v0, 0x7d4

    goto :goto_0

    :pswitch_d
    const/16 v0, 0x7d3

    goto :goto_0

    :pswitch_e
    const/16 v0, 0x7d2

    goto :goto_0

    :pswitch_f
    const/16 v0, 0x7da

    .line 1162
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/di;->c()V

    .line 1163
    invoke-static {}, Lcom/meawallet/mtp/di;->d()V

    .line 133
    new-instance p0, Lcom/meawallet/mtp/cy;

    invoke-direct {p0, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object p0

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getErrorCodes()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->m:Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 136
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getErrorCodes()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->aL:Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 137
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getErrorCodes()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->bk:Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_2

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getErrorCodes()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cM:Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3162
    invoke-static {}, Lcom/meawallet/mtp/di;->c()V

    .line 3163
    invoke-static {}, Lcom/meawallet/mtp/di;->d()V

    .line 147
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x130

    invoke-direct {p0, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object p0

    .line 149
    :cond_2
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getErrorCodes()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cu:Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 150
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getErrorCodes()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->aX:Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 157
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x12d

    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object v0

    .line 4162
    :cond_4
    :goto_1
    invoke-static {}, Lcom/meawallet/mtp/di;->c()V

    .line 4163
    invoke-static {}, Lcom/meawallet/mtp/di;->d()V

    .line 154
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x132

    invoke-direct {p0, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object p0

    .line 2162
    :cond_5
    :goto_2
    invoke-static {}, Lcom/meawallet/mtp/di;->c()V

    .line 2163
    invoke-static {}, Lcom/meawallet/mtp/di;->d()V

    .line 141
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x12f

    invoke-direct {p0, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static a(Ljava/lang/Exception;Lcom/meawallet/mtp/MeaCoreListener;)V
    .locals 0

    .line 24
    invoke-static {p0}, Lcom/meawallet/mtp/aw;->b(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object p0

    if-eqz p1, :cond_0

    .line 28
    invoke-interface {p1, p0}, Lcom/meawallet/mtp/MeaCoreListener;->onFailure(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static b(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;
    .locals 2

    .line 33
    sget-object v0, Lcom/meawallet/mtp/aw;->a:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 35
    instance-of v0, p0, Lcom/meawallet/mtp/MeaCryptoException;

    if-eqz v0, :cond_0

    .line 37
    check-cast p0, Lcom/meawallet/mtp/MeaCryptoException;

    invoke-static {p0}, Lcom/meawallet/mtp/aw;->a(Lcom/meawallet/mtp/MeaCryptoException;)Lcom/meawallet/mtp/cy;

    move-result-object p0

    return-object p0

    .line 38
    :cond_0
    instance-of v0, p0, Ljava/security/GeneralSecurityException;

    if-eqz v0, :cond_1

    .line 40
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x12d

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object v0

    .line 41
    :cond_1
    instance-of v0, p0, Lcom/meawallet/mtp/bv;

    if-nez v0, :cond_4

    instance-of v0, p0, Lcom/meawallet/mtp/bn;

    if-nez v0, :cond_4

    instance-of v0, p0, Lcom/meawallet/mtp/cs;

    if-nez v0, :cond_4

    instance-of v0, p0, Lcom/meawallet/mtp/NotInitializedException;

    if-nez v0, :cond_4

    instance-of v0, p0, Lcom/meawallet/mtp/bm;

    if-nez v0, :cond_4

    instance-of v0, p0, Lcom/meawallet/mtp/bl;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 49
    :cond_2
    instance-of v0, p0, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_3

    .line 51
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f9

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object v0

    .line 54
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f5

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object v0

    .line 48
    :cond_4
    :goto_0
    check-cast p0, Lcom/meawallet/mtp/cz;

    invoke-interface {p0}, Lcom/meawallet/mtp/cz;->getMeaError()Lcom/meawallet/mtp/MeaError;

    move-result-object p0

    return-object p0
.end method
