.class Lcom/meawallet/mtp/MeaCryptoException;
.super Ljava/security/GeneralSecurityException;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/cz;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaCryptoException$a;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MeaCryptoException"


# instance fields
.field private final mErrorCodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/meawallet/mtp/MeaCryptoException$a;",
            ">;"
        }
    .end annotation
.end field

.field private mRootReasons:[Lcom/meawallet/mtp/MeaCryptoException$a;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 3

    .line 30
    invoke-direct {p0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/MeaCryptoException;->mErrorCodes:Ljava/util/ArrayList;

    const/16 v0, 0x10

    .line 430
    new-array v0, v0, [Lcom/meawallet/mtp/MeaCryptoException$a;

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->aT:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->ch:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cg:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cf:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->ce:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cd:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cj:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->ci:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cc:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cv:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cw:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cx:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->cQ:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->fd:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->fe:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->ff:Lcom/meawallet/mtp/MeaCryptoException$a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/meawallet/mtp/MeaCryptoException;->mRootReasons:[Lcom/meawallet/mtp/MeaCryptoException$a;

    .line 32
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/MeaCryptoException;->parseErrorCodes(Ljava/lang/String;)V

    return-void
.end method

.method private concatenateNativeErrorCodeMessages()Ljava/lang/String;
    .locals 3

    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Exception in native library: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 484
    iget-object v1, p0, Lcom/meawallet/mtp/MeaCryptoException;->mErrorCodes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/MeaCryptoException$a;

    if-eqz v2, :cond_0

    .line 486
    invoke-virtual {v2}, Lcom/meawallet/mtp/MeaCryptoException$a;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 490
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRootedReasonList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCryptoException$a;",
            ">;"
        }
    .end annotation

    .line 518
    iget-object v0, p0, Lcom/meawallet/mtp/MeaCryptoException;->mRootReasons:[Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private parseErrorCodes(Ljava/lang/String;)V
    .locals 7

    .line 450
    sget-object v0, Lcom/meawallet/mtp/MeaCryptoException;->TAG:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const/16 v3, 0x12d

    invoke-static {v0, v3, p1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "\\|"

    .line 451
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 453
    array-length v0, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, p1, v2

    .line 455
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 457
    iget-object v4, p0, Lcom/meawallet/mtp/MeaCryptoException;->mErrorCodes:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/meawallet/mtp/MeaCryptoException$a;->a(I)Lcom/meawallet/mtp/MeaCryptoException$a;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    .line 459
    sget-object v4, Lcom/meawallet/mtp/MeaCryptoException;->TAG:Ljava/lang/String;

    const-string v5, "Failed to convert String error code from native lib to Integer value"

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v4, v3, v5, v6}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method getErrorCodes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/meawallet/mtp/MeaCryptoException$a;",
            ">;"
        }
    .end annotation

    .line 465
    iget-object v0, p0, Lcom/meawallet/mtp/MeaCryptoException;->mErrorCodes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic getMeaError()Lcom/meawallet/mtp/MeaError;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getMeaError()Lcom/meawallet/mtp/cy;

    move-result-object v0

    return-object v0
.end method

.method public getMeaError()Lcom/meawallet/mtp/cy;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 477
    invoke-super {p0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getRootedReasonErrorCode()Lcom/meawallet/mtp/MeaCryptoException$a;
    .locals 4

    .line 501
    iget-object v0, p0, Lcom/meawallet/mtp/MeaCryptoException;->mErrorCodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 503
    invoke-direct {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getRootedReasonList()Ljava/util/List;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lcom/meawallet/mtp/MeaCryptoException;->mErrorCodes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/MeaCryptoException$a;

    .line 507
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    .line 514
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/MeaCryptoException$a;->fT:Lcom/meawallet/mtp/MeaCryptoException$a;

    return-object v0
.end method

.method isRootedDeviceDetected()Z
    .locals 2

    .line 495
    iget-object v0, p0, Lcom/meawallet/mtp/MeaCryptoException;->mErrorCodes:Ljava/util/ArrayList;

    sget-object v1, Lcom/meawallet/mtp/MeaCryptoException$a;->bo:Lcom/meawallet/mtp/MeaCryptoException$a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getRootedReasonList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCryptoException;->getRootedReasonErrorCode()Lcom/meawallet/mtp/MeaCryptoException$a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method
