.class Lcom/meawallet/mtp/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String; = "l"


# instance fields
.field final a:Lcom/meawallet/mtp/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/meawallet/mtp/l;->a:Lcom/meawallet/mtp/ci;

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I
    .locals 5

    const/4 v0, 0x1

    .line 122
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 124
    new-instance v1, Lcom/meawallet/mtp/ct;

    invoke-direct {v1, p1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0, v1, p2}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object v3

    if-nez v3, :cond_0

    const/16 p1, 0x3f2

    return p1

    .line 132
    :cond_0
    sget-object v4, Lcom/meawallet/mtp/MeaCardState;->ACTIVE:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v4, v3}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 134
    invoke-virtual {v3}, Lcom/meawallet/mtp/MeaCardState;->getRules()Lcom/meawallet/mtp/ev;

    move-result-object v3

    invoke-interface {v3}, Lcom/meawallet/mtp/ev;->a()I

    move-result v3

    .line 136
    invoke-static {v3}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v4

    if-eqz v4, :cond_1

    return v3

    .line 141
    :cond_1
    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 145
    invoke-interface {p2, v3}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->activateCard(Lcom/mastercard/mpsdk/componentinterface/Card;)V

    .line 147
    new-array p2, v0, [Ljava/lang/Object;

    aput-object p1, p2, v2

    .line 149
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p1

    .line 1162
    iget-object p1, p1, Lcom/meawallet/mtp/ax;->f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    if-eqz p1, :cond_3

    .line 153
    new-instance p2, Lcom/meawallet/mtp/l$1;

    invoke-direct {p2, p0, p1, v1}, Lcom/meawallet/mtp/l$1;-><init>(Lcom/meawallet/mtp/l;Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;Lcom/meawallet/mtp/MeaCard;)V

    invoke-static {p2}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 167
    :cond_2
    new-array p2, v0, [Ljava/lang/Object;

    aput-object p1, p2, v2

    :cond_3
    :goto_0
    const/4 p1, -0x1

    return p1
.end method

.method final a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;
    .locals 4

    const/4 v0, 0x1

    .line 24
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 27
    :try_start_0
    iget-object v1, p0, Lcom/meawallet/mtp/l;->a:Lcom/meawallet/mtp/ci;

    invoke-static {p1, v1}, Lcom/meawallet/mtp/i;->c(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object v1

    .line 29
    new-array v3, v0, [Ljava/lang/Object;

    aput-object v1, v3, v2

    if-eqz v1, :cond_2

    .line 31
    sget-object v3, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v3, v1}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v3, v1}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 36
    :cond_0
    invoke-static {p1, p2}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    if-nez p1, :cond_1

    return-object v1

    .line 43
    :cond_1
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardState()Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    move-result-object p1

    .line 45
    new-array p2, v0, [Ljava/lang/Object;

    aput-object p1, p2, v2

    .line 47
    sget-object p2, Lcom/meawallet/mtp/l$4;->a:[I

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 58
    :pswitch_0
    sget-object p1, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    return-object p1

    .line 55
    :pswitch_1
    sget-object p1, Lcom/meawallet/mtp/MeaCardState;->SUSPENDED:Lcom/meawallet/mtp/MeaCardState;

    return-object p1

    .line 52
    :pswitch_2
    sget-object p1, Lcom/meawallet/mtp/MeaCardState;->ACTIVE:Lcom/meawallet/mtp/MeaCardState;

    return-object p1

    .line 49
    :pswitch_3
    sget-object p1, Lcom/meawallet/mtp/MeaCardState;->PROVISIONED:Lcom/meawallet/mtp/MeaCardState;
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_2
    :goto_0
    return-object v1

    :catch_0
    move-exception p1

    .line 61
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    .line 64
    :goto_1
    sget-object p1, Lcom/meawallet/mtp/MeaCardState;->UNKNOWN:Lcom/meawallet/mtp/MeaCardState;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method final a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardState;Lcom/meawallet/mtp/ci;)V
    .locals 3

    .line 351
    invoke-static {p1, p3}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v0

    .line 353
    invoke-interface {p3, v0}, Lcom/meawallet/mtp/ci;->d(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object p3

    const/4 v1, 0x2

    .line 355
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 357
    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1, p3}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 358
    iget-object p3, p0, Lcom/meawallet/mtp/l;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {p3, v0, p2}, Lcom/meawallet/mtp/ci;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardState;)V

    .line 360
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p3

    .line 5162
    iget-object p3, p3, Lcom/meawallet/mtp/ax;->f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    if-eqz p3, :cond_0

    .line 364
    new-instance v0, Lcom/meawallet/mtp/l$3;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/meawallet/mtp/l$3;-><init>(Lcom/meawallet/mtp/l;Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardState;)V

    invoke-static {v0}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method final a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;",
            "Lcom/mastercard/mpsdk/componentinterface/CardManager;",
            "Lcom/meawallet/mtp/k;",
            ")V"
        }
    .end annotation

    .line 316
    invoke-static {p1, p2}, Lcom/meawallet/mtp/i;->b(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V

    .line 1327
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/MeaCard;

    const/16 v2, 0x3f2

    const/4 v3, 0x1

    .line 2293
    :try_start_0
    new-array v4, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 2294
    invoke-virtual {p0, v1, p2}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object v4

    const/4 v6, -0x1

    if-nez v4, :cond_1

    const/16 v6, 0x3f2

    goto :goto_1

    .line 2301
    :cond_1
    invoke-virtual {v4}, Lcom/meawallet/mtp/MeaCardState;->getRules()Lcom/meawallet/mtp/ev;

    .line 2303
    invoke-static {v6}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    .line 2308
    :cond_2
    sget-object v4, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    iget-object v7, p0, Lcom/meawallet/mtp/l;->a:Lcom/meawallet/mtp/ci;

    invoke-virtual {p0, v1, v4, v7}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardState;Lcom/meawallet/mtp/ci;)V

    .line 1332
    :goto_1
    invoke-static {v6}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1333
    sget-object v1, Lcom/meawallet/mtp/l;->b:Ljava/lang/String;

    const-string v4, "markCardForDeletionInLde() result = %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    invoke-static {v1, v6, v4, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1336
    invoke-static {v1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v3

    .line 1338
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v3

    if-ne v2, v3, :cond_3

    goto :goto_0

    .line 1342
    :cond_3
    throw v1

    .line 318
    :cond_4
    invoke-static {p1}, Lcom/meawallet/mtp/k;->a(Ljava/util/List;)V

    .line 4166
    iget-object v0, p3, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    if-eqz v0, :cond_6

    .line 3286
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v0

    .line 3288
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/MeaCard;

    if-eqz v2, :cond_5

    .line 3290
    invoke-interface {v2}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 3292
    invoke-interface {v2}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    .line 3293
    invoke-virtual {p3, v2}, Lcom/meawallet/mtp/k;->a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;

    goto :goto_2

    .line 320
    :cond_6
    invoke-static {p1, p2}, Lcom/meawallet/mtp/i;->a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V

    return-void
.end method

.method final b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I
    .locals 3

    .line 71
    invoke-virtual {p0, p1, p2}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object p1

    const/4 p2, 0x1

    .line 73
    new-array v0, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    if-nez p1, :cond_0

    const/16 p1, 0x3f2

    return p1

    .line 80
    :cond_0
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaCardState;->getRules()Lcom/meawallet/mtp/ev;

    move-result-object v0

    invoke-interface {v0}, Lcom/meawallet/mtp/ev;->c()I

    move-result v0

    const/4 v2, 0x2

    .line 82
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, p2

    return v0
.end method

.method final c(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I
    .locals 3

    .line 88
    invoke-virtual {p0, p1, p2}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object p1

    const/4 p2, 0x1

    .line 90
    new-array v0, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    if-nez p1, :cond_0

    const/16 p1, 0x3f2

    return p1

    .line 97
    :cond_0
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaCardState;->getRules()Lcom/meawallet/mtp/ev;

    move-result-object v0

    invoke-interface {v0}, Lcom/meawallet/mtp/ev;->e()I

    move-result v0

    const/4 v2, 0x2

    .line 99
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, p2

    return v0
.end method

.method final d(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I
    .locals 3

    .line 105
    invoke-virtual {p0, p1, p2}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object p1

    const/4 p2, 0x1

    .line 107
    new-array v0, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    if-nez p1, :cond_0

    const/16 p1, 0x3f2

    return p1

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaCardState;->getRules()Lcom/meawallet/mtp/ev;

    move-result-object v0

    invoke-interface {v0}, Lcom/meawallet/mtp/ev;->d()I

    move-result v0

    const/4 v2, 0x2

    .line 116
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, p2

    return v0
.end method
