.class final enum Lcom/meawallet/mtp/WspErrorResponseData$Category;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/WspErrorResponseData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/WspErrorResponseData$Category;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Category;

.field public static final enum AUTHENTICATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

.field public static final enum CRYPTOGRAPHY:Lcom/meawallet/mtp/WspErrorResponseData$Category;

.field public static final enum DIGITIZATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

.field public static final enum INTERNAL:Lcom/meawallet/mtp/WspErrorResponseData$Category;

.field public static final enum REGISTRATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

.field public static final enum VALIDATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

.field public static final enum WORKFLOW:Lcom/meawallet/mtp/WspErrorResponseData$Category;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 46
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const-string v1, "VALIDATION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/WspErrorResponseData$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->VALIDATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 47
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const-string v1, "WORKFLOW"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/WspErrorResponseData$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->WORKFLOW:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 48
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const-string v1, "REGISTRATION"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/WspErrorResponseData$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->REGISTRATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 49
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const-string v1, "DIGITIZATION"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/WspErrorResponseData$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->DIGITIZATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 50
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const-string v1, "CRYPTOGRAPHY"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/WspErrorResponseData$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->CRYPTOGRAPHY:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 51
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const-string v1, "INTERNAL"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/WspErrorResponseData$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->INTERNAL:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 52
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const-string v1, "AUTHENTICATION"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/WspErrorResponseData$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->AUTHENTICATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const/4 v0, 0x7

    .line 44
    new-array v0, v0, [Lcom/meawallet/mtp/WspErrorResponseData$Category;

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Category;->VALIDATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Category;->WORKFLOW:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Category;->REGISTRATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Category;->DIGITIZATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Category;->CRYPTOGRAPHY:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Category;->INTERNAL:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Category;->AUTHENTICATION:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    aput-object v1, v0, v8

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->$VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/WspErrorResponseData$Category;
    .locals 1

    .line 44
    const-class v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/WspErrorResponseData$Category;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/WspErrorResponseData$Category;
    .locals 1

    .line 44
    sget-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Category;->$VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Category;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/WspErrorResponseData$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/WspErrorResponseData$Category;

    return-object v0
.end method
