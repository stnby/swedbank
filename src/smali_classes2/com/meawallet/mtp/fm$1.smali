.class final Lcom/meawallet/mtp/fm$1;
.super Lcom/meawallet/mtp/fk;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/fm;->c()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/MeaCard;

.field final synthetic b:Lcom/meawallet/mtp/fm;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/fm;Lcom/meawallet/mtp/do;Lcom/meawallet/mtp/MeaCard;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/meawallet/mtp/fm$1;->b:Lcom/meawallet/mtp/fm;

    iput-object p3, p0, Lcom/meawallet/mtp/fm$1;->a:Lcom/meawallet/mtp/MeaCard;

    invoke-direct {p0, p2}, Lcom/meawallet/mtp/fk;-><init>(Lcom/meawallet/mtp/do;)V

    return-void
.end method


# virtual methods
.method public final onDeleteCardCompleted(Ljava/lang/String;)Z
    .locals 3

    .line 92
    invoke-static {}, Lcom/meawallet/mtp/fm;->a()Ljava/lang/String;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 94
    iget-object v1, p0, Lcom/meawallet/mtp/fm$1;->a:Lcom/meawallet/mtp/MeaCard;

    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/meawallet/mtp/fm$1;->b()V

    .line 98
    iget-object v0, p0, Lcom/meawallet/mtp/fm$1;->b:Lcom/meawallet/mtp/fm;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/fm;->a(Lcom/meawallet/mtp/fm;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/fm;->a()Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    :goto_0
    return v2
.end method

.method public final onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 3

    .line 109
    invoke-static {}, Lcom/meawallet/mtp/fm;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, p4, p3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-object p4, p0, Lcom/meawallet/mtp/fm$1;->a:Lcom/meawallet/mtp/MeaCard;

    invoke-interface {p4}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/meawallet/mtp/fm$1;->b()V

    .line 115
    iget-object p4, p0, Lcom/meawallet/mtp/fm$1;->b:Lcom/meawallet/mtp/fm;

    invoke-static {p4, p1, p2, p3}, Lcom/meawallet/mtp/fm;->a(Lcom/meawallet/mtp/fm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/fm;->a()Ljava/lang/String;

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    aput-object p1, p2, v1

    :goto_0
    return v1
.end method
