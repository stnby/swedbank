.class public final enum Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaAuthenticationMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

.field public static final enum CARDHOLDER_TO_CALL_AUTOMATED_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

.field public static final enum CARDHOLDER_TO_CALL_MANNED_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

.field public static final enum CARDHOLDER_TO_USE_ISSUER_MOBILE_APP:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

.field public static final enum CARDHOLDER_TO_VISIT_WEBSITE:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

.field public static final enum EMAIL_TO_CARDHOLDER_ADDRESS:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

.field public static final enum ISSUER_TO_CALL_CARDHOLDER_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

.field public static final enum TEXT_TO_CARDHOLDER_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 63
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const-string v1, "TEXT_TO_CARDHOLDER_NUMBER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->TEXT_TO_CARDHOLDER_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    .line 64
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const-string v1, "EMAIL_TO_CARDHOLDER_ADDRESS"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->EMAIL_TO_CARDHOLDER_ADDRESS:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    .line 65
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const-string v1, "CARDHOLDER_TO_CALL_AUTOMATED_NUMBER"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_CALL_AUTOMATED_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    .line 66
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const-string v1, "CARDHOLDER_TO_CALL_MANNED_NUMBER"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_CALL_MANNED_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    .line 67
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const-string v1, "CARDHOLDER_TO_VISIT_WEBSITE"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_VISIT_WEBSITE:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    .line 68
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const-string v1, "CARDHOLDER_TO_USE_ISSUER_MOBILE_APP"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_USE_ISSUER_MOBILE_APP:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    .line 69
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const-string v1, "ISSUER_TO_CALL_CARDHOLDER_NUMBER"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->ISSUER_TO_CALL_CARDHOLDER_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    const/4 v0, 0x7

    .line 61
    new-array v0, v0, [Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->TEXT_TO_CARDHOLDER_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->EMAIL_TO_CARDHOLDER_ADDRESS:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_CALL_AUTOMATED_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_CALL_MANNED_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_VISIT_WEBSITE:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->CARDHOLDER_TO_USE_ISSUER_MOBILE_APP:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->ISSUER_TO_CALL_CARDHOLDER_NUMBER:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    aput-object v1, v0, v8

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->$VALUES:[Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;
    .locals 1

    .line 61
    const-class v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;
    .locals 1

    .line 61
    sget-object v0, Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->$VALUES:[Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    return-object v0
.end method
