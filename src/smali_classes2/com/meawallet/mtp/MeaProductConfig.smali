.class public Lcom/meawallet/mtp/MeaProductConfig;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;,
        Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "MeaProductConfig"


# instance fields
.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "brandLogoAssetId"
    .end annotation
.end field

.field private c:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "isCoBranded"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "coBrandName"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "coBrandLogoAssetId"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cardBackgroundCombinedAssetId"
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "foregroundColor"
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "issuerName"
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "shortDescription"
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "longDescription"
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "issuerLogoAssetId"
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "iconAssetId"
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "customerServiceUrl"
    .end annotation
.end field

.field private n:Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "issuerMobileApp"
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "termsAndConditionsUrl"
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "privacyPolicyUrl"
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "issuerProductConfigCode"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .line 48
    sget-object v0, Lcom/meawallet/mtp/MeaProductConfig;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getBrandLogoAssetId()Ljava/lang/String;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getCardBackgroundCombinedAssetId()Ljava/lang/String;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getCoBrandLogoAssetId()Ljava/lang/String;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getCoBrandName()Ljava/lang/String;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomerServiceUrl()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getForegroundColor()Ljava/lang/String;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getIconAssetId()Ljava/lang/String;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getIsCoBranded()Ljava/lang/Boolean;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getIssuerLogoAssetId()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getIssuerMobileApp()Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->n:Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;

    return-object v0
.end method

.method public getIssuerName()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getIssuerProductConfigCode()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getLongDescription()Ljava/lang/String;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivacyPolicyUrl()Ljava/lang/String;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getShortDescription()Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getTermsAndConditionsUrl()Ljava/lang/String;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig;->o:Ljava/lang/String;

    return-object v0
.end method

.method validate()V
    .locals 0

    return-void
.end method
