.class final enum Lcom/meawallet/mtp/MeaHttpMethod;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaHttpMethod;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum GET:Lcom/meawallet/mtp/MeaHttpMethod;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum POST:Lcom/meawallet/mtp/MeaHttpMethod;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaHttpMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 15
    new-instance v0, Lcom/meawallet/mtp/MeaHttpMethod;

    const-string v1, "POST"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaHttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    .line 17
    new-instance v0, Lcom/meawallet/mtp/MeaHttpMethod;

    const-string v1, "GET"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaHttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->GET:Lcom/meawallet/mtp/MeaHttpMethod;

    const/4 v0, 0x2

    .line 14
    new-array v0, v0, [Lcom/meawallet/mtp/MeaHttpMethod;

    sget-object v1, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaHttpMethod;->GET:Lcom/meawallet/mtp/MeaHttpMethod;

    aput-object v1, v0, v3

    sput-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->a:[Lcom/meawallet/mtp/MeaHttpMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 14
    const-class v0, Lcom/meawallet/mtp/MeaHttpMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaHttpMethod;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 14
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->a:[Lcom/meawallet/mtp/MeaHttpMethod;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaHttpMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method
