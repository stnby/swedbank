.class final Lcom/meawallet/mtp/dl;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaError;
    .locals 3

    const/16 v0, 0x1f5

    if-nez p0, :cond_0

    .line 32
    new-instance p0, Lcom/meawallet/mtp/cy;

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    :cond_0
    const/4 v1, -0x1

    .line 35
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "PROCESSING"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_1
    const-string v2, "CANCELED"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_2
    const-string v2, "NETWORK_ERROR"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_3
    const-string v2, "INCORRECT_MOBILE_PIN"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_4
    const-string v2, "SDK_COMMUNICATION_ERROR"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_5
    const-string v2, "SDK_CRYPTO_OPERATION_FAILED"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_6
    const-string v2, "SDK_INVALID_INPUTS"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x4

    :cond_1
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 63
    new-instance p0, Lcom/meawallet/mtp/cy;

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    :pswitch_0
    const-string p0, "Wallet pin is not set"

    .line 57
    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 58
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 p1, 0x38f

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object p0

    :pswitch_1
    const-string p0, "Another request is already in process"

    .line 52
    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 53
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 p1, 0xd1

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object p0

    .line 66
    :cond_2
    new-instance p0, Lcom/meawallet/mtp/cy;

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    .line 49
    :pswitch_2
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x1f9

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    .line 46
    :pswitch_3
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0xc9

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    .line 43
    :pswitch_4
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x12d

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    .line 40
    :pswitch_5
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x6d

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    .line 37
    :pswitch_6
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x263

    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69ef23ea -> :sswitch_6
        -0x60fcff32 -> :sswitch_5
        -0x394a3f86 -> :sswitch_4
        -0x3687deae -> :sswitch_3
        -0x34711f89 -> :sswitch_2
        0x274e7499 -> :sswitch_1
        0x36141b13 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
