.class final enum Lcom/meawallet/mtp/WspErrorResponseData$Code;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/WspErrorResponseData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Code"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/WspErrorResponseData$Code;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum API_IMPLEMENTATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum BAD_REQUEST:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum CARD_DATA:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum CONFIGURATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum DUPLICATE_REQUEST:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum EXCEEDED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum EXPIRED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum GATEWAY_TIMEOUT:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum INVALID_FIELD_VALUE:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum INVALID_OPERATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum MISSING_FIELD:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum NOT_MAPPED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum OPERATION_FAILED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum TEMPORARY_UNAVAILABLE:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum UNAUTHORIZED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum UNSUPPORTED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

.field public static final enum VELOCITY_CHECK_REACHED:Lcom/meawallet/mtp/WspErrorResponseData$Code;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 57
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "INVALID_FIELD_VALUE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->INVALID_FIELD_VALUE:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 58
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "MISSING_FIELD"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->MISSING_FIELD:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 59
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "EXPIRED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->EXPIRED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 60
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "CARD_DATA"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->CARD_DATA:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 61
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "BAD_REQUEST"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->BAD_REQUEST:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 62
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "INVALID_OPERATION"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->INVALID_OPERATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 63
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "DUPLICATE_REQUEST"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->DUPLICATE_REQUEST:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 64
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "UNSUPPORTED"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->UNSUPPORTED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 65
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "TEMPORARY_UNAVAILABLE"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->TEMPORARY_UNAVAILABLE:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 66
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "EXCEEDED"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->EXCEEDED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 67
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "CONFIGURATION"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->CONFIGURATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 68
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "OPERATION_FAILED"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->OPERATION_FAILED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 69
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "GATEWAY_TIMEOUT"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->GATEWAY_TIMEOUT:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 70
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "NOT_MAPPED"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->NOT_MAPPED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 71
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "API_IMPLEMENTATION"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->API_IMPLEMENTATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 72
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "UNAUTHORIZED"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->UNAUTHORIZED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 73
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const-string v1, "VELOCITY_CHECK_REACHED"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/WspErrorResponseData$Code;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->VELOCITY_CHECK_REACHED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const/16 v0, 0x11

    .line 55
    new-array v0, v0, [Lcom/meawallet/mtp/WspErrorResponseData$Code;

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->INVALID_FIELD_VALUE:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->MISSING_FIELD:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->EXPIRED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->CARD_DATA:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->BAD_REQUEST:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->INVALID_OPERATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->DUPLICATE_REQUEST:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v8

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->UNSUPPORTED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v9

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->TEMPORARY_UNAVAILABLE:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v10

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->EXCEEDED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v11

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->CONFIGURATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v12

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->OPERATION_FAILED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v13

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->GATEWAY_TIMEOUT:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    aput-object v1, v0, v14

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->NOT_MAPPED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->API_IMPLEMENTATION:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->UNAUTHORIZED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Code;->VELOCITY_CHECK_REACHED:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->$VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Code;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/WspErrorResponseData$Code;
    .locals 1

    .line 55
    const-class v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/WspErrorResponseData$Code;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/WspErrorResponseData$Code;
    .locals 1

    .line 55
    sget-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Code;->$VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Code;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/WspErrorResponseData$Code;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/WspErrorResponseData$Code;

    return-object v0
.end method
