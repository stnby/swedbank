.class final Lcom/meawallet/mtp/et;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

.field private d:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/meawallet/mtp/et;->a:Ljava/lang/String;

    .line 16
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;-><init>([B)V

    iput-object p1, p0, Lcom/meawallet/mtp/et;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    .line 17
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;-><init>([B)V

    iput-object p1, p0, Lcom/meawallet/mtp/et;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    .line 18
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    invoke-virtual {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;-><init>([B)V

    iput-object p1, p0, Lcom/meawallet/mtp/et;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    return-void
.end method


# virtual methods
.method public final getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/meawallet/mtp/et;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    return-object v0
.end method

.method public final getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/meawallet/mtp/et;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    return-object v0
.end method

.method public final getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/meawallet/mtp/et;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    return-object v0
.end method

.method public final getKeySetId()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/meawallet/mtp/et;->a:Ljava/lang/String;

    return-object v0
.end method
