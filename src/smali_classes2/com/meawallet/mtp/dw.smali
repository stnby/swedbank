.class Lcom/meawallet/mtp/dw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "dw"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()I
    .locals 5

    .line 1383
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "VERSION"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->c(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x2ac

    const/4 v3, 0x1

    if-nez v0, :cond_0

    .line 1388
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v4, "VERSION"

    invoke-virtual {v0, v4, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;I)Z

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    if-ge v0, v2, :cond_2

    .line 1402
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v4, "VERSION"

    invoke-virtual {v0, v4, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x0

    if-nez v0, :cond_3

    const/16 v0, 0x1f7

    .line 21
    invoke-static {v0, v2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    .line 26
    :cond_3
    invoke-static {}, Lcom/meawallet/mtp/dw;->b()Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0x6a

    .line 30
    invoke-static {v0, v2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    .line 2074
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v4, Lcom/meawallet/mtp/BuildConfig;->MIN_SDK_VERSION:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lt v0, v4, :cond_7

    .line 2086
    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/meawallet/mtp/n;->l()Z

    invoke-static {}, Lcom/meawallet/mtp/n;->m()Z

    .line 2092
    invoke-static {}, Lcom/meawallet/mtp/n;->k()Z

    .line 2093
    invoke-static {}, Lcom/meawallet/mtp/n;->j()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_6

    goto :goto_3

    :cond_6
    const/4 v1, 0x1

    :cond_7
    :goto_3
    if-nez v1, :cond_8

    const/16 v0, 0x66

    .line 39
    invoke-static {v0, v2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    .line 2212
    :cond_8
    invoke-static {}, Lcom/meawallet/mtp/ar;->a()Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0x6b

    .line 48
    invoke-static {v0, v2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    :cond_9
    const/4 v0, -0x1

    return v0
.end method

.method static a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;)I
    .locals 2

    const/4 v0, 0x1

    .line 282
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 284
    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    return v1

    .line 291
    :cond_0
    sget-object v0, Lcom/meawallet/mtp/dw$1;->a:[I

    invoke-virtual {p1}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 303
    :pswitch_0
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->g(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_1

    const/16 p0, 0x386

    return p0

    .line 295
    :pswitch_1
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->h(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_1

    const/16 p0, 0x387

    return p0

    :cond_1
    :goto_0
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(ILcom/meawallet/mtp/MeaCoreListener;)V
    .locals 3

    const/4 v0, 0x1

    .line 412
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 414
    new-instance v0, Lcom/meawallet/mtp/cy;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method static a(Landroid/content/Context;)Z
    .locals 0

    .line 106
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->e(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const/16 p0, 0x1f5

    .line 231
    invoke-static {p0, p2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    .line 236
    :cond_0
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;)I

    move-result p1

    const/4 v1, 0x1

    .line 238
    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 240
    invoke-static {p1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v2

    if-nez v2, :cond_1

    return v1

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 249
    :pswitch_0
    invoke-static {}, Lcom/meawallet/mtp/n;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 252
    sget-object p1, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;)I

    move-result p0

    .line 254
    new-array p1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v0

    .line 256
    invoke-static {p0}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 257
    invoke-static {p0, p2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    .line 262
    :cond_2
    invoke-static {p1, p2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    .line 271
    :pswitch_1
    invoke-static {p1, p2}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    return v0

    :cond_3
    :goto_0
    return v1

    :pswitch_data_0
    .packed-switch 0x386
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static a(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 0

    if-nez p0, :cond_1

    if-eqz p1, :cond_0

    const/16 p0, 0x1f9

    .line 62
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    return p0

    .line 64
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Context is null."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method static a(Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 1

    .line 111
    invoke-static {}, Lcom/meawallet/mtp/dd;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/16 v0, 0x67

    .line 118
    invoke-static {v0, p0}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    return p0
.end method

.method static b()Z
    .locals 1

    .line 180
    invoke-static {}, Lcom/meawallet/mtp/ar;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method static b(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    .line 193
    invoke-static {p0, v0}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p0

    return p0
.end method

.method static b(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 0

    .line 125
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->c(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    const/16 p0, 0x69

    .line 128
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method static b(Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 1

    .line 168
    invoke-static {}, Lcom/meawallet/mtp/dw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/16 v0, 0x6a

    .line 173
    invoke-static {v0, p0}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    return p0
.end method

.method static c(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 0

    .line 140
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->b(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/16 p0, 0x68

    .line 147
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    return p0
.end method

.method static c(Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 3

    .line 2352
    invoke-static {}, Lcom/meawallet/mtp/n;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/meawallet/mtp/n;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2354
    :cond_1
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/br;->a()Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    :goto_1
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 319
    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    const/4 p0, 0x0

    goto :goto_2

    :cond_2
    const/4 p0, 0x1

    .line 324
    :goto_2
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    return p0
.end method

.method static d(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 1

    .line 154
    const-class v0, Lcom/meawallet/mtp/MeaHceService;

    invoke-static {p0, v0}, Lcom/meawallet/mtp/ar;->a(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/16 p0, 0x6e

    .line 161
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    return p0
.end method

.method static e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 0

    .line 198
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->d(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/16 p0, 0x6d

    .line 205
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    return p0
.end method

.method static f(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z
    .locals 2

    .line 331
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    .line 332
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/16 p0, 0x6f

    .line 342
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(ILcom/meawallet/mtp/MeaCoreListener;)V

    const/4 p0, 0x0

    .line 345
    :goto_0
    new-array p1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p1, v0

    return p0
.end method
