.class interface abstract Lcom/meawallet/mtp/ci;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Ljava/lang/String;)Lcom/meawallet/mtp/bt;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/bt;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(I)V
.end method

.method public abstract a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V
.end method

.method public abstract a(Lcom/meawallet/mtp/LdePinState;)V
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaTransactionLimit;)V
.end method

.method public abstract a(Lcom/meawallet/mtp/bt;)V
.end method

.method public abstract a(Lcom/meawallet/mtp/cb;)V
.end method

.method public abstract a(Lcom/meawallet/mtp/cc;)V
.end method

.method public abstract a(Ljava/lang/Integer;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/meawallet/mtp/LdePinState;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardState;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/util/Currency;)V
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b(Ljava/util/Currency;)Lcom/meawallet/mtp/MeaTransactionLimit;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;
.end method

.method public abstract b(Lcom/meawallet/mtp/bt;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(Ljava/lang/String;)Lcom/meawallet/mtp/bt;
.end method

.method public abstract c()V
.end method

.method public abstract d(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardState;
.end method

.method public abstract d()Z
.end method

.method public abstract e()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
.end method

.method public abstract e(Ljava/lang/String;)V
.end method

.method public abstract f()Ljava/lang/Integer;
.end method

.method public abstract f(Ljava/lang/String;)V
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()Lcom/meawallet/mtp/cc;
.end method

.method public abstract i()V
.end method

.method public abstract j()Landroid/content/Context;
.end method

.method public abstract k()Lcom/meawallet/mtp/ai;
.end method

.method public abstract l()Lcom/meawallet/mtp/LdePinState;
.end method

.method public abstract m()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract n()Ljava/lang/Integer;
.end method

.method public abstract o()V
.end method

.method public abstract p()Lcom/meawallet/mtp/cg;
.end method

.method public abstract q()V
.end method
