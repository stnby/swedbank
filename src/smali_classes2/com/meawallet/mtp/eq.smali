.class final Lcom/meawallet/mtp/eq;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 0

    .line 31
    invoke-direct/range {p0 .. p8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 5

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/eq;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    .line 82
    invoke-static {v0}, Lcom/meawallet/mtp/eq;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    sget-object p1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v0, "MOBILE_KEYS_MISSING"

    const-string v1, "Failed to execute request session command."

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/meawallet/mtp/eq;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 92
    :cond_0
    new-instance v1, Lcom/meawallet/mtp/er;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/meawallet/mtp/eq;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 93
    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;->getPaymentAppInstanceId()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/meawallet/mtp/eq;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 94
    invoke-interface {v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;->getPaymentAppProviderId()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    .line 95
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/meawallet/mtp/er;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3024
    invoke-virtual {p1, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/eq;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 119
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 121
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/eq;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 113
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 114
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/eq;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 107
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute request session command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/eq;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 100
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute Session HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/eq;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 61
    iput-object p2, p0, Lcom/meawallet/mtp/eq;->c:Ljava/lang/String;

    .line 63
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 2317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 65
    iput-object p1, p0, Lcom/meawallet/mtp/eq;->h:Lcom/meawallet/mtp/ae;

    .line 67
    iget-object p1, p0, Lcom/meawallet/mtp/eq;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lcom/meawallet/mtp/eq;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->b:Lcom/meawallet/mtp/ae;

    if-ne p1, v0, :cond_1

    .line 70
    :cond_0
    sget-object p1, Lcom/meawallet/mtp/ae;->c:Lcom/meawallet/mtp/ae;

    iput-object p1, p0, Lcom/meawallet/mtp/eq;->h:Lcom/meawallet/mtp/ae;

    .line 72
    iget-object p1, p0, Lcom/meawallet/mtp/eq;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p1, p2, p3, p4}, Lcom/meawallet/mtp/ad;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_1
    return-void
.end method

.method final a(Lcom/meawallet/mtp/s;)V
    .locals 0

    .line 53
    sget-object p1, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 55
    sget-object p1, Lcom/meawallet/mtp/ah;->a:Lcom/meawallet/mtp/ah;

    iput-object p1, p0, Lcom/meawallet/mtp/eq;->i:Lcom/meawallet/mtp/ah;

    .line 57
    iget-object p1, p0, Lcom/meawallet/mtp/eq;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p1}, Lcom/meawallet/mtp/ad;->b()V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 0

    .line 151
    instance-of p1, p1, Lcom/meawallet/mtp/eq;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/meawallet/mtp/eq;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getRemoteManagementServiceUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/paymentapp/1/0/requestSession"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/requestSession"

    return-object v0
.end method

.method public final j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()V
    .locals 4

    .line 128
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 3317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 129
    iget-object v0, p0, Lcom/meawallet/mtp/eq;->d:Lcom/meawallet/mtp/ad;

    const-string v1, "CANCELED"

    const-string v2, "Request Session command cancelled"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/meawallet/mtp/ad;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 134
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/eq;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 139
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
