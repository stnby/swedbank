.class final Lcom/meawallet/mtp/at$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DsrpData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/at;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAip()[B
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;->aip:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;->ciacDecline:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;->cvrMaskAnd:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getExpiryDate()[B
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;->applicationExpiryDate:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;->issuerApplicationData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPanSequenceNumber()[B
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;->panSequenceNumber:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPar()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTrack2EquivalentData()[B
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/meawallet/mtp/at$3;->a:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;->track2Equivalent:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;
    .locals 1

    .line 223
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0
.end method

.method public final getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
