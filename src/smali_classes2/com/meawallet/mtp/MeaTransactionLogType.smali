.class public final enum Lcom/meawallet/mtp/MeaTransactionLogType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaTransactionLogType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/meawallet/mtp/MeaTransactionLogType;

.field public static final enum CONTACTLESS:Lcom/meawallet/mtp/MeaTransactionLogType;

.field public static final enum FAILED:Lcom/meawallet/mtp/MeaTransactionLogType;

.field public static final enum REMOTE_DSRP:Lcom/meawallet/mtp/MeaTransactionLogType;


# instance fields
.field private mCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 12
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionLogType;

    const-string v1, "CONTACTLESS"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/meawallet/mtp/MeaTransactionLogType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionLogType;->CONTACTLESS:Lcom/meawallet/mtp/MeaTransactionLogType;

    .line 13
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionLogType;

    const-string v1, "REMOTE_DSRP"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v3, v4}, Lcom/meawallet/mtp/MeaTransactionLogType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionLogType;->REMOTE_DSRP:Lcom/meawallet/mtp/MeaTransactionLogType;

    .line 14
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionLogType;

    const-string v1, "FAILED"

    const/4 v5, 0x2

    const/4 v6, 0x4

    invoke-direct {v0, v1, v5, v6}, Lcom/meawallet/mtp/MeaTransactionLogType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionLogType;->FAILED:Lcom/meawallet/mtp/MeaTransactionLogType;

    .line 9
    new-array v0, v4, [Lcom/meawallet/mtp/MeaTransactionLogType;

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionLogType;->CONTACTLESS:Lcom/meawallet/mtp/MeaTransactionLogType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionLogType;->REMOTE_DSRP:Lcom/meawallet/mtp/MeaTransactionLogType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionLogType;->FAILED:Lcom/meawallet/mtp/MeaTransactionLogType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionLogType;->$VALUES:[Lcom/meawallet/mtp/MeaTransactionLogType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Lcom/meawallet/mtp/MeaTransactionLogType;->mCode:I

    return-void
.end method

.method public static getByCode(I)Lcom/meawallet/mtp/MeaTransactionLogType;
    .locals 5

    .line 28
    invoke-static {}, Lcom/meawallet/mtp/MeaTransactionLogType;->values()[Lcom/meawallet/mtp/MeaTransactionLogType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 30
    invoke-virtual {v3}, Lcom/meawallet/mtp/MeaTransactionLogType;->getCode()I

    move-result v4

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaTransactionLogType;
    .locals 1

    .line 9
    const-class v0, Lcom/meawallet/mtp/MeaTransactionLogType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaTransactionLogType;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaTransactionLogType;
    .locals 1

    .line 9
    sget-object v0, Lcom/meawallet/mtp/MeaTransactionLogType;->$VALUES:[Lcom/meawallet/mtp/MeaTransactionLogType;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaTransactionLogType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaTransactionLogType;

    return-object v0
.end method


# virtual methods
.method public final getCode()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/meawallet/mtp/MeaTransactionLogType;->mCode:I

    return v0
.end method
