.class Lcom/meawallet/mtp/cu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String; = "cu"


# instance fields
.field final b:Lcom/meawallet/mtp/dm;

.field final c:Lcom/meawallet/mtp/l;

.field final d:Lcom/meawallet/mtp/ci;

.field private final e:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

.field private final f:Lcom/meawallet/mtp/fu;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/dm;Lcom/meawallet/mtp/l;Lcom/meawallet/mtp/ci;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Lcom/meawallet/mtp/fu;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    .line 36
    iput-object p2, p0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 37
    iput-object p3, p0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    .line 38
    iput-object p4, p0, Lcom/meawallet/mtp/cu;->e:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    .line 39
    iput-object p5, p0, Lcom/meawallet/mtp/cu;->f:Lcom/meawallet/mtp/fu;

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/cu;Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)Lcom/meawallet/mtp/MeaError;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/meawallet/mtp/cu;->a(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)Lcom/meawallet/mtp/MeaError;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/k;
    .locals 0

    .line 16055
    iget-object p0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    .line 16289
    iget-object p0, p0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    return-object p0
.end method

.method static synthetic a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/k;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 0

    .line 20
    invoke-static {p0, p1, p2, p3}, Lcom/meawallet/mtp/cu;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/k;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/MeaCardListener;)V

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/cu;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2, p3}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V

    return-void
.end method

.method static synthetic b(Lcom/meawallet/mtp/cu;)Lcom/mastercard/mpsdk/componentinterface/CardManager;
    .locals 0

    .line 17051
    iget-object p0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {p0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .line 20
    sget-object v0, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/k;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 3

    const/4 v0, 0x1

    .line 294
    new-array v0, v0, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 296
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/meawallet/mtp/k;->a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V

    .line 298
    invoke-static {p3, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardListener;Lcom/meawallet/mtp/MeaCard;)V

    return-void
.end method

.method static synthetic c(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/cv;
    .locals 0

    .line 17059
    iget-object p0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    .line 17284
    iget-object p0, p0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    return-object p0
.end method

.method static synthetic d(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/l;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    return-object p0
.end method

.method static synthetic e(Lcom/meawallet/mtp/cu;)Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/meawallet/mtp/cu;->e:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    return-object p0
.end method

.method static synthetic f(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/ci;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    return-object p0
.end method


# virtual methods
.method final a(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)Lcom/meawallet/mtp/MeaError;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;",
            "Lcom/meawallet/mtp/MeaListener;",
            ")",
            "Lcom/meawallet/mtp/MeaError;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 795
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 796
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 798
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/meawallet/mtp/MeaCard;

    .line 800
    invoke-static {v3}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 803
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 14051
    :cond_1
    iget-object v5, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v5}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v5

    .line 807
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v5

    if-eqz v5, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-nez v4, :cond_0

    .line 810
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 819
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_4

    .line 820
    new-array p1, v4, [Ljava/lang/Object;

    .line 821
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v0

    .line 822
    iget-object p1, p0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {v1, p1}, Lcom/meawallet/mtp/i;->a(Ljava/util/List;Lcom/meawallet/mtp/ci;)V

    .line 826
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_5

    .line 827
    new-array p1, v4, [Ljava/lang/Object;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    .line 14844
    new-array p1, v4, [Ljava/lang/Object;

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 14846
    invoke-static {v2}, Lcom/meawallet/mtp/i;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 14848
    iget-object p1, p0, Lcom/meawallet/mtp/cu;->f:Lcom/meawallet/mtp/fu;

    new-instance v1, Lcom/meawallet/mtp/cu$7;

    invoke-direct {v1, p0, v2, p2}, Lcom/meawallet/mtp/cu$7;-><init>(Lcom/meawallet/mtp/cu;Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_1

    .line 15210
    :try_start_1
    new-instance p2, Lcom/meawallet/mtp/fw;

    iget-object v5, p1, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v6, p1, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v7, p1, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    move-object v3, p2

    move-object v8, v1

    invoke-direct/range {v3 .. v8}, Lcom/meawallet/mtp/fw;-><init>(Ljava/util/List;Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {p2}, Lcom/meawallet/mtp/fw;->a()V
    :try_end_1
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/cs; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 15213
    :try_start_2
    new-instance p2, Lcom/meawallet/mtp/ft;

    invoke-direct {p2}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance p2, Lcom/meawallet/mtp/fs;

    invoke-direct {p2}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p1}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    goto :goto_2

    .line 831
    :cond_5
    invoke-static {p2}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaListener;)V
    :try_end_2
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/meawallet/mtp/bn; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/meawallet/mtp/bv; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/meawallet/mtp/cs; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    const/4 p1, 0x0

    return-object p1

    :catch_1
    move-exception p1

    .line 835
    sget-object p2, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const-string v1, "Not provisioned cards deletion failed."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2, p1, v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 837
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object p1

    return-object p1
.end method

.method final a(Lcom/meawallet/mtp/MeaCard;)Ljava/lang/Boolean;
    .locals 3

    const/4 v0, 0x1

    .line 82
    new-array v0, v0, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 1051
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 85
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 89
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->isContactlessSupported()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 91
    :cond_0
    sget-object p1, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v0, 0x3f2

    const-string v1, "isContactlessPaymentsSupported failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v0, v1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 94
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 625
    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/cu;->a(Z)Ljava/util/List;

    move-result-object v0

    .line 627
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-nez v0, :cond_0

    return-object v1

    .line 634
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/MeaCard;

    .line 637
    sget-object v3, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    iget-object v4, p0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 13051
    iget-object v5, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v5}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v5

    .line 637
    invoke-virtual {v4, v2, v5}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 638
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method final a(Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    .line 183
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/ci;Z)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method final a(Landroid/content/Context;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 2

    const/4 v0, 0x1

    .line 669
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 671
    new-instance v0, Lcom/meawallet/mtp/c;

    invoke-direct {v0}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cu$5;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/meawallet/mtp/cu$5;-><init>(Lcom/meawallet/mtp/cu;Lcom/meawallet/mtp/MeaCard;Landroid/content/Context;Lcom/meawallet/mtp/MeaCardListener;)V

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V

    return-void
.end method

.method final a(Landroid/content/Context;Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    .line 716
    new-instance v0, Lcom/meawallet/mtp/c;

    invoke-direct {v0}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cu$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/meawallet/mtp/cu$6;-><init>(Lcom/meawallet/mtp/cu;Landroid/content/Context;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 4

    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 7051
    iget-object v1, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    .line 221
    invoke-virtual {v0, p1, v1}, Lcom/meawallet/mtp/l;->b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v0

    .line 223
    invoke-static {v0}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 224
    sget-object p1, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const-string v1, "selectForContactlessPayments failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v0, v1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 226
    new-instance p1, Lcom/meawallet/mtp/cy;

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 8051
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    .line 231
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v1

    if-nez v1, :cond_1

    .line 234
    sget-object p1, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v1, 0x3f2

    const-string v3, "selectForContactlessPayments failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 236
    new-instance p1, Lcom/meawallet/mtp/cy;

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 241
    :cond_1
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/Card;->isContactlessSupported()Z

    move-result v0

    const/4 v3, 0x1

    if-nez v0, :cond_2

    .line 242
    new-array v0, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 244
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x3f0

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 249
    :cond_2
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getNumberOfAvailableCredentials()I

    move-result v0

    if-lez v0, :cond_3

    .line 8055
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    .line 8289
    iget-object v0, v0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    .line 9051
    iget-object v1, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    .line 250
    invoke-static {p1, v0, v1, p2}, Lcom/meawallet/mtp/cu;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/k;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/MeaCardListener;)V

    return-void

    .line 255
    :cond_3
    new-array v0, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 257
    new-instance v0, Lcom/meawallet/mtp/dk;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/meawallet/mtp/dk;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/cu;)V

    .line 259
    new-instance v1, Lcom/meawallet/mtp/cu$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/meawallet/mtp/cu$1;-><init>(Lcom/meawallet/mtp/cu;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V

    .line 10031
    iput-object v1, v0, Lcom/meawallet/mtp/dk;->a:Lcom/meawallet/mtp/en;

    .line 283
    invoke-virtual {v0}, Lcom/meawallet/mtp/dk;->a()V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 286
    invoke-static {p1, p2}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V
    .locals 8

    .line 12051
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 453
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v4

    .line 456
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    .line 12284
    iget-object v0, v0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    .line 456
    invoke-virtual {v0, v4}, Lcom/meawallet/mtp/cv;->isCdCvmSuccessful(Lcom/mastercard/mpsdk/componentinterface/Card;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    .line 458
    invoke-interface {p3, p1, p2}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onAuthenticationRequired(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;)V

    :cond_0
    return-void

    .line 464
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/c;

    invoke-direct {v0}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cu$3;

    move-object v2, v1

    move-object v3, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, Lcom/meawallet/mtp/cu$3;-><init>(Lcom/meawallet/mtp/cu;Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;)V

    .line 465
    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p2

    .line 522
    invoke-static {p2}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object p2

    invoke-static {p3, p1, p2}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method final b(Lcom/meawallet/mtp/MeaCard;)Ljava/lang/Boolean;
    .locals 3

    const/4 v0, 0x1

    .line 101
    new-array v0, v0, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 2051
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 104
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 108
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->isDsrpSupported()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 110
    :cond_0
    sget-object p1, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v0, 0x3f2

    const-string v1, "isRemotePaymentsSupported failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v0, v1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 113
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    .line 116
    :goto_0
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1
.end method

.method final c(Lcom/meawallet/mtp/MeaCard;)Ljava/lang/Boolean;
    .locals 4

    const/4 v0, 0x1

    .line 120
    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 122
    iget-object v1, p0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 3051
    iget-object v2, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    .line 122
    invoke-virtual {v1, p1, v2}, Lcom/meawallet/mtp/l;->b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v1

    .line 124
    invoke-static {v1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    new-array p1, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v3

    .line 127
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    .line 4051
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 131
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 135
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->stopContactlessTransaction()V

    .line 137
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1

    .line 139
    :cond_1
    sget-object p1, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v0, 0x3f2

    const-string v1, "stopContactlessTransaction failed."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 142
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    .line 145
    :goto_0
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1
.end method

.method final d(Lcom/meawallet/mtp/MeaCard;)I
    .locals 4

    const/4 v0, 0x1

    .line 187
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 193
    :try_start_0
    iget-object v1, p0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 5051
    iget-object v3, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v3}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v3

    .line 193
    invoke-virtual {v1, p1, v3}, Lcom/meawallet/mtp/l;->b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v1

    .line 195
    invoke-static {v1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 196
    new-array p1, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    return v2

    .line 6051
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    .line 201
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 204
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getNumberOfAvailableCredentials()I

    move-result p1

    goto :goto_0

    .line 206
    :cond_1
    new-array p1, v0, [Ljava/lang/Object;

    const/16 v1, 0x3f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v2
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 p1, 0x0

    .line 209
    :goto_0
    :try_start_1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2
    :try_end_1
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    move v2, p1

    move-object p1, v0

    goto :goto_1

    :catch_1
    move-exception p1

    .line 211
    :goto_1
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move p1, v2

    :goto_2
    return p1
.end method

.method final e(Lcom/meawallet/mtp/MeaCard;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/MeaCard;",
            ")",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLog;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 352
    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 354
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 357
    :try_start_0
    iget-object v2, p0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 10051
    iget-object v4, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v4}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v4

    .line 357
    invoke-virtual {v2, p1, v4}, Lcom/meawallet/mtp/l;->b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v2

    .line 359
    invoke-static {v2}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 360
    new-array p1, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v3

    return-object v1

    .line 11051
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 365
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 369
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getTransactionLog()Ljava/util/Iterator;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 373
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 374
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;

    .line 376
    new-instance v2, Lcom/meawallet/mtp/MeaTransactionLog;

    invoke-direct {v2, v0}, Lcom/meawallet/mtp/MeaTransactionLog;-><init>(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)V

    .line 378
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 382
    :cond_1
    sget-object p1, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v0, 0x3f2

    const-string v2, "getTransactionLog failed."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 385
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    :cond_2
    :goto_1
    return-object v1
.end method
