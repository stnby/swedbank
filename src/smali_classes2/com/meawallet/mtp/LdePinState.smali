.class final enum Lcom/meawallet/mtp/LdePinState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/LdePinState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum PIN_NOT_SET:Lcom/meawallet/mtp/LdePinState;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum PIN_SET:Lcom/meawallet/mtp/LdePinState;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic b:[Lcom/meawallet/mtp/LdePinState;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 13
    new-instance v0, Lcom/meawallet/mtp/LdePinState;

    const-string v1, "PIN_NOT_SET"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/meawallet/mtp/LdePinState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/meawallet/mtp/LdePinState;->PIN_NOT_SET:Lcom/meawallet/mtp/LdePinState;

    .line 19
    new-instance v0, Lcom/meawallet/mtp/LdePinState;

    const-string v1, "PIN_SET"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/meawallet/mtp/LdePinState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    const/4 v0, 0x2

    .line 8
    new-array v0, v0, [Lcom/meawallet/mtp/LdePinState;

    sget-object v1, Lcom/meawallet/mtp/LdePinState;->PIN_NOT_SET:Lcom/meawallet/mtp/LdePinState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/meawallet/mtp/LdePinState;->b:[Lcom/meawallet/mtp/LdePinState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lcom/meawallet/mtp/LdePinState;->a:I

    return-void
.end method

.method static a(I)Lcom/meawallet/mtp/LdePinState;
    .locals 5

    .line 49
    invoke-static {}, Lcom/meawallet/mtp/LdePinState;->values()[Lcom/meawallet/mtp/LdePinState;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 1037
    iget v4, v3, Lcom/meawallet/mtp/LdePinState;->a:I

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    :cond_1
    sget-object p0, Lcom/meawallet/mtp/LdePinState;->PIN_NOT_SET:Lcom/meawallet/mtp/LdePinState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/LdePinState;
    .locals 1

    .line 8
    const-class v0, Lcom/meawallet/mtp/LdePinState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/LdePinState;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/LdePinState;
    .locals 1

    .line 8
    sget-object v0, Lcom/meawallet/mtp/LdePinState;->b:[Lcom/meawallet/mtp/LdePinState;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/LdePinState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/LdePinState;

    return-object v0
.end method
