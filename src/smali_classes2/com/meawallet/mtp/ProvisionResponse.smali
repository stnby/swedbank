.class public Lcom/meawallet/mtp/ProvisionResponse;
.super Lcom/meawallet/mtp/s;
.source "SourceFile"


# instance fields
.field public cardProfile:Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lflexjson/h;
        a = "cardProfile"
    .end annotation
.end field

.field public iccKek:Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lflexjson/h;
        a = "iccKek"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/meawallet/mtp/s;-><init>()V

    return-void
.end method

.method public static valueOf(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/ProvisionResponse;
    .locals 6

    .line 52
    new-instance p0, Ljava/io/InputStreamReader;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/4 v0, 0x0

    .line 53
    :try_start_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string p1, "version"

    .line 56
    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 57
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "cardProfile"

    .line 58
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v1, "version"

    .line 59
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 62
    new-instance p1, Lflexjson/j;

    invoke-direct {p1}, Lflexjson/j;-><init>()V

    const-string v1, "cardProfile"

    const-class v2, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    .line 63
    invoke-virtual {p1, v1, v2}, Lflexjson/j;->b(Ljava/lang/String;Ljava/lang/Class;)Lflexjson/j;

    move-result-object p1

    const-class v1, Lcom/meawallet/mtp/ProvisionResponse;

    .line 64
    invoke-virtual {p1, p0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/ProvisionResponse;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    return-object p1

    .line 67
    :cond_0
    :try_start_2
    new-instance p1, Lorg/json/JSONException;

    const-string v1, "Profile version not supported"

    invoke-direct {p1, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 70
    :cond_1
    new-instance p1, Lflexjson/j;

    invoke-direct {p1}, Lflexjson/j;-><init>()V

    const-string v1, "cardProfile"

    const-class v2, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    .line 71
    invoke-virtual {p1, v1, v2}, Lflexjson/j;->b(Ljava/lang/String;Ljava/lang/Class;)Lflexjson/j;

    move-result-object p1

    const-class v1, Lcom/meawallet/mtp/ProvisionResponse;

    .line 72
    invoke-virtual {p1, p0, v1}, Lflexjson/j;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/ProvisionResponse;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 81
    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    return-object p1

    :catch_0
    move-exception p1

    .line 76
    :try_start_3
    const-class v1, Lcom/meawallet/mtp/ProvisionResponse;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error in parsing Provision Response %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 77
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_2
    const-string v5, ""

    :goto_0
    aput-object v5, v3, v4

    .line 76
    invoke-static {v1, p1, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 81
    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    return-object v0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    move-object v0, p1

    .line 52
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    if-eqz v0, :cond_3

    .line 81
    :try_start_5
    invoke-virtual {p0}, Ljava/io/Reader;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :catch_2
    move-exception p0

    invoke-virtual {v0, p0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    :goto_2
    throw p1
.end method


# virtual methods
.method public getCardProfile()Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/meawallet/mtp/ProvisionResponse;->cardProfile:Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;

    return-object v0
.end method

.method public getIccKek()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/meawallet/mtp/ProvisionResponse;->iccKek:Ljava/lang/String;

    return-object v0
.end method

.method public setCardProfile(Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/meawallet/mtp/ProvisionResponse;->cardProfile:Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;

    return-void
.end method

.method public setIccKek(Ljava/lang/String;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/meawallet/mtp/ProvisionResponse;->iccKek:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
