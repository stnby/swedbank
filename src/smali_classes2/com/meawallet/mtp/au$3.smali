.class final Lcom/meawallet/mtp/au$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/au;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)V
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/meawallet/mtp/au$3;->a:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNAtc()[B
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/meawallet/mtp/au$3;->a:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->nAtc:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPCvc3()[B
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/meawallet/mtp/au$3;->a:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->pCvc3:Ljava/lang/String;

    .line 272
    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPUnAtc()[B
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/meawallet/mtp/au$3;->a:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->pUnAtc:Ljava/lang/String;

    .line 277
    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getTrackData()[B
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/meawallet/mtp/au$3;->a:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;->trackData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
