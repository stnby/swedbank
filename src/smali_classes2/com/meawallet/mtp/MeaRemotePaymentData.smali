.class public Lcom/meawallet/mtp/MeaRemotePaymentData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;


# instance fields
.field private a:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionAmount"
    .end annotation
.end field

.field private b:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currency"
    .end annotation
.end field

.field private c:B
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionType"
    .end annotation
.end field

.field private d:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cryptogramType"
    .end annotation
.end field

.field private e:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "countryCode"
    .end annotation
.end field

.field private f:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "unpredictableNumber"
    .end annotation
.end field

.field private g:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionDay"
    .end annotation
.end field

.field private h:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionMonth"
    .end annotation
.end field

.field private i:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionYear"
    .end annotation
.end field

.field private j:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 2

    .line 55
    :try_start_0
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    const-class v1, Lcom/meawallet/mtp/MeaRemotePaymentData;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaRemotePaymentData;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public getCountryCode()I
    .locals 1

    .line 101
    iget v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->e:I

    return v0
.end method

.method public getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->d:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    return-object v0
.end method

.method public getCurrencyCode()I
    .locals 1

    .line 79
    iget v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->b:I

    return v0
.end method

.method public getOptionalUnpredictableNumber()I
    .locals 1

    .line 167
    iget v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->f:I

    return v0
.end method

.method public getTransactionAmount()J
    .locals 2

    .line 63
    iget-wide v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->a:J

    return-wide v0
.end method

.method public getTransactionDate()Ljava/util/Date;
    .locals 4

    .line 190
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->j:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 191
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 193
    iget v1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->i:I

    iget v2, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->h:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->g:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 195
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->j:Ljava/util/Date;

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->j:Ljava/util/Date;

    return-object v0
.end method

.method public getTransactionType()B
    .locals 1

    .line 123
    iget-byte v0, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->c:B

    return v0
.end method

.method public isValid()Z
    .locals 4

    .line 245
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getTransactionAmount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setCountryCode(I)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 111
    iput p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->e:I

    return-object p0
.end method

.method public setCryptogramType(Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->d:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    return-object p0
.end method

.method public setCurrencyCode(I)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 89
    iput p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->b:I

    return-object p0
.end method

.method public setOptionalUnpredictableNumber(I)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 177
    iput p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->f:I

    return-object p0
.end method

.method public setTransactionAmount(J)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 67
    iput-wide p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->a:J

    return-object p0
.end method

.method public setTransactionDay(I)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 209
    iput p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->g:I

    return-object p0
.end method

.method public setTransactionMonth(I)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 221
    iput p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->h:I

    return-object p0
.end method

.method public setTransactionType(B)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 133
    iput-byte p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->c:B

    return-object p0
.end method

.method public setTransactionYear(I)Lcom/meawallet/mtp/MeaRemotePaymentData;
    .locals 0

    .line 233
    iput p1, p0, Lcom/meawallet/mtp/MeaRemotePaymentData;->i:I

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MeaRemotePaymentData[\n\ttransactionAmount: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getTransactionAmount()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\n\tcurrencyCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getCurrencyCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttransactionType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getTransactionType()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\n\tunpredictableNumber: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getOptionalUnpredictableNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\n\tcryptogramType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttransactionDate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getTransactionDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tcountryCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getCountryCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
