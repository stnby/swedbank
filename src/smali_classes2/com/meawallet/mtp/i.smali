.class Lcom/meawallet/mtp/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "i"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;
    .locals 1

    .line 115
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, p0}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Ljava/lang/String;
    .locals 1

    .line 20
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getEligibilityReceipt()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0, p1}, Lcom/meawallet/mtp/i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;
    .locals 1

    .line 69
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    invoke-interface {p1, p0}, Lcom/meawallet/mtp/ci;->a(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    .line 2114
    iget-object p1, p0, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    if-eqz p1, :cond_0

    .line 3114
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 74
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 25
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v3, 0x1

    aput-object p1, v1, v3

    .line 27
    invoke-static {p0, p1, p2}, Lcom/meawallet/mtp/i;->b(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 1046
    iget-object p0, p2, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    return-object p0

    .line 31
    :cond_0
    new-instance p2, Lcom/meawallet/mtp/cs;

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v2

    aput-object p1, v0, v3

    const-string p0, "Card not found. cardId = %s, eligibilityReceipt = %s"

    .line 32
    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const/16 p1, 0x3f2

    invoke-direct {p2, p0, p1}, Lcom/meawallet/mtp/cs;-><init>(Ljava/lang/String;I)V

    throw p2
.end method

.method static a(Lcom/meawallet/mtp/ci;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/ci;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 131
    invoke-interface {p0, p1}, Lcom/meawallet/mtp/ci;->a(Z)Ljava/util/List;

    move-result-object p0

    .line 135
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/bt;

    .line 136
    new-instance v1, Lcom/meawallet/mtp/ct;

    invoke-direct {v1, p1}, Lcom/meawallet/mtp/ct;-><init>(Lcom/meawallet/mtp/bt;)V

    .line 137
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 353
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 354
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 356
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/MeaCard;

    .line 358
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 359
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method

.method static a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 1

    if-eqz p2, :cond_1

    if-eqz p0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6041
    iget-object p0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 181
    invoke-static {p2, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 184
    :cond_0
    invoke-interface {p2, p1}, Lcom/meawallet/mtp/MeaCardListener;->onSuccess(Lcom/meawallet/mtp/MeaCard;)V

    return-void

    :cond_1
    if-eqz p0, :cond_3

    .line 188
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p2

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/meawallet/mtp/ax;->e(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardListener;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 191
    invoke-virtual {p0}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7041
    iget-object p0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 193
    invoke-static {p2, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    .line 195
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p0

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/ax;->f(Ljava/lang/String;)V

    return-void

    .line 199
    :cond_2
    invoke-interface {p2, p1}, Lcom/meawallet/mtp/MeaCardListener;->onSuccess(Lcom/meawallet/mtp/MeaCard;)V

    .line 200
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p0

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/ax;->f(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method static a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    if-eqz p1, :cond_1

    if-eqz p0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8041
    iget-object p0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 211
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 214
    :cond_0
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaListener;->onSuccess()V

    return-void

    :cond_1
    if-eqz p0, :cond_3

    .line 218
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p1

    .line 8154
    iget-object p1, p1, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    if-eqz p1, :cond_3

    .line 222
    invoke-virtual {p0}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 9041
    iget-object p0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 224
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    .line 226
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p0

    .line 9158
    iput-object v1, p0, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    return-void

    .line 230
    :cond_2
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaListener;->onSuccess()V

    .line 231
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p0

    .line 10158
    iput-object v1, p0, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    :cond_3
    return-void
.end method

.method static a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;",
            "Lcom/mastercard/mpsdk/componentinterface/CardManager;",
            ")V"
        }
    .end annotation

    .line 146
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/MeaCard;

    if-eqz v0, :cond_0

    .line 148
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 150
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Card;->deleteTransactionCredentials()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static a(Ljava/util/List;Lcom/meawallet/mtp/ci;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;",
            "Lcom/meawallet/mtp/ci;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 326
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 328
    invoke-static {p0}, Lcom/meawallet/mtp/k;->a(Ljava/util/List;)V

    .line 330
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/MeaCard;

    .line 333
    :try_start_0
    invoke-static {v0, p1}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v0

    .line 334
    invoke-interface {p1, v0}, Lcom/meawallet/mtp/ci;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 337
    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v1

    const/16 v2, 0x3f2

    .line 339
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    if-ne v2, v1, :cond_0

    goto :goto_0

    .line 343
    :cond_0
    new-instance p0, Lcom/meawallet/mtp/cs;

    invoke-virtual {v0}, Lcom/meawallet/mtp/cs;->getMeaError()Lcom/meawallet/mtp/MeaError;

    move-result-object p1

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lcom/meawallet/mtp/cs;->getMeaError()Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    invoke-interface {v0}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/meawallet/mtp/cs;-><init>(Ljava/lang/String;I)V

    throw p0

    :cond_1
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaCard;)Z
    .locals 0

    .line 123
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;
    .locals 1

    .line 41
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getEligibilityReceipt()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0, p1}, Lcom/meawallet/mtp/i;->b(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    return-object p0
.end method

.method static b(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;
    .locals 3

    const/4 v0, 0x2

    .line 47
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v2, 0x1

    aput-object p1, v0, v2

    .line 51
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    invoke-interface {p2, p0}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 56
    invoke-interface {p2, p1}, Lcom/meawallet/mtp/ci;->c(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_2

    .line 60
    new-array p1, v2, [Ljava/lang/Object;

    .line 2046
    iget-object p2, p0, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    aput-object p2, p1, v1

    :cond_2
    return-object p0
.end method

.method static b(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;",
            "Lcom/mastercard/mpsdk/componentinterface/CardManager;",
            ")V"
        }
    .end annotation

    .line 160
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/MeaCard;

    if-eqz v0, :cond_0

    .line 163
    :try_start_0
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 164
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Card;->stopContactlessTransaction()V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 171
    sget-object v1, Lcom/meawallet/mtp/i;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Z
    .locals 3

    .line 314
    invoke-static {p0}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const/4 v0, 0x2

    .line 319
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    aput-object p0, v0, v1

    return p1
.end method

.method static c(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaCardState;
    .locals 1

    .line 83
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getEligibilityReceipt()Ljava/lang/String;

    move-result-object p0

    .line 4090
    invoke-static {v0, p0, p1}, Lcom/meawallet/mtp/i;->b(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 5054
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static d(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/LdePinState;
    .locals 0

    .line 103
    invoke-static {p0, p1}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 5063
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->c:Lcom/meawallet/mtp/LdePinState;

    return-object p0

    .line 109
    :cond_0
    sget-object p0, Lcom/meawallet/mtp/LdePinState;->PIN_NOT_SET:Lcom/meawallet/mtp/LdePinState;

    return-object p0
.end method

.method static e(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaDigitizationDecision;
    .locals 3

    const/4 v0, 0x1

    .line 239
    new-array v0, v0, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 241
    invoke-static {p0, p1}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 11071
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->e:Lcom/meawallet/mtp/MeaDigitizationDecision;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static f(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)[Lcom/meawallet/mtp/MeaAuthenticationMethod;
    .locals 4

    const/4 v0, 0x1

    .line 253
    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 255
    invoke-static {p0, p1}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 11080
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->f:[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    return-object p0

    .line 260
    :cond_0
    new-array p0, v0, [Ljava/lang/Object;

    const/16 p1, 0x3f2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p0, v3

    const/4 p0, 0x0

    return-object p0
.end method

.method static g(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaProductConfig;
    .locals 4

    const/4 v0, 0x1

    .line 268
    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 270
    invoke-static {p0, p1}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 11097
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->g:Lcom/meawallet/mtp/MeaProductConfig;

    return-object p0

    .line 275
    :cond_0
    new-array p0, v0, [Ljava/lang/Object;

    const/16 p1, 0x3f2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p0, v3

    const/4 p0, 0x0

    return-object p0
.end method

.method static h(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaTokenInfo;
    .locals 4

    const/4 v0, 0x1

    .line 283
    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 285
    invoke-static {p0, p1}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 11106
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->h:Lcom/meawallet/mtp/MeaTokenInfo;

    return-object p0

    .line 290
    :cond_0
    new-array p0, v0, [Ljava/lang/Object;

    const/16 p1, 0x3f2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p0, v3

    const/4 p0, 0x0

    return-object p0
.end method

.method static i(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/PaymentNetwork;
    .locals 4

    const/4 v0, 0x1

    .line 298
    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 300
    invoke-static {p0, p1}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 11122
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    return-object p0

    .line 305
    :cond_0
    new-array p0, v0, [Ljava/lang/Object;

    const/16 p1, 0x3f2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p0, v3

    const/4 p0, 0x0

    return-object p0
.end method
