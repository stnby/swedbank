.class Lcom/meawallet/mtp/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;
.implements Lcom/meawallet/mtp/ad;
.implements Lcom/meawallet/mtp/af;


# static fields
.field private static final d:Ljava/lang/String; = "x"

.field private static e:Lcom/google/gson/Gson;


# instance fields
.field a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

.field b:Lcom/meawallet/mtp/t;

.field c:Lcom/meawallet/mtp/u;

.field private f:Lcom/meawallet/mtp/fi;

.field private g:Lcom/meawallet/mtp/w;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field private j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field private k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field private l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    invoke-static {}, Lcom/meawallet/mtp/JsonUtil;->a()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/x;->e:Lcom/google/gson/Gson;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/meawallet/mtp/ee;)V
    .locals 3

    .line 245
    iget-object v0, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    .line 8344
    iget-object v0, v0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {v0}, Lcom/meawallet/mtp/u$a;->size()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    .line 246
    iget-object v0, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    .line 9214
    iget-object v0, v0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    const/4 v1, 0x1

    .line 248
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const-string v0, "NULL"

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/meawallet/mtp/ee;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v1, v2

    .line 250
    sget-object v0, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/ee;->a(Lcom/meawallet/mtp/ag;)V

    .line 251
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->l()V

    return-void

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/ee;)Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/v;)Lcom/meawallet/mtp/aa;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 261
    :try_start_0
    iget-object v2, p0, Lcom/meawallet/mtp/x;->g:Lcom/meawallet/mtp/w;

    sget-object v3, Lcom/meawallet/mtp/x;->e:Lcom/google/gson/Gson;

    invoke-virtual {v2, v3, p1}, Lcom/meawallet/mtp/w;->a(Lcom/google/gson/Gson;Lcom/meawallet/mtp/v;)Lcom/meawallet/mtp/ab;

    move-result-object p1

    .line 263
    iget-object v2, p0, Lcom/meawallet/mtp/x;->f:Lcom/meawallet/mtp/fi;

    invoke-virtual {v2, p1}, Lcom/meawallet/mtp/fi;->a(Lcom/meawallet/mtp/ab;)V

    .line 265
    iget-object v2, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    .line 10090
    new-array v3, v0, [Ljava/lang/Object;

    iget-object v4, v2, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    .line 10092
    invoke-virtual {v2}, Lcom/meawallet/mtp/u;->a()V

    .line 10094
    new-array v3, v0, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    .line 267
    invoke-virtual {p1}, Lcom/meawallet/mtp/ab;->a()Lcom/meawallet/mtp/aa;

    move-result-object p1
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 273
    sget-object v2, Lcom/meawallet/mtp/x;->d:Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const-string v1, "Unable to parse push notification data: %s SDK_INVALID_INPUTS"

    invoke-static {v2, p1, v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 275
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse push notification data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "SDK_INVALID_INPUTS"

    invoke-direct {v0, p1, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception p1

    .line 269
    sget-object v0, Lcom/meawallet/mtp/x;->d:Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Unable to parse push notification data."

    invoke-static {v0, p1, v2, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse push notification data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
.end method

.method public final a()V
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthCompleted()V

    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V
    .locals 12

    .line 361
    iget-object v0, p0, Lcom/meawallet/mtp/x;->h:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    .line 363
    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    .line 365
    iget-object p1, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    iget-object v4, p0, Lcom/meawallet/mtp/x;->f:Lcom/meawallet/mtp/fi;

    const-string v9, "SUCCESS"

    .line 11087
    new-instance v11, Lcom/meawallet/mtp/du;

    invoke-virtual {p1}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p1, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v5, p1, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p1, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v7, p1, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p1, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v8, p2

    invoke-direct/range {v0 .. v10}, Lcom/meawallet/mtp/du;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    .line 368
    iget-object p1, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    invoke-virtual {p1, v11}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/ee;)Ljava/lang/String;

    return-void
.end method

.method public final a(Lcom/meawallet/mtp/ee;)V
    .locals 2

    .line 325
    check-cast p1, Lcom/meawallet/mtp/du;

    .line 10145
    iget-object p1, p1, Lcom/meawallet/mtp/du;->j:Ljava/lang/String;

    const/4 v0, 0x1

    .line 328
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 330
    iget-object v0, p0, Lcom/meawallet/mtp/x;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;

    if-eqz v0, :cond_0

    .line 335
    iget-object v1, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v1, v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lcom/meawallet/mtp/x;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 338
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ProvisionedCardId in CmsDRemoteCommunicationManager is not as received in onNotifyProvisioningResultCommandCompleted() or DigitizedCardProfile is missing."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/meawallet/mtp/ee;Ljava/lang/String;)V
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p2, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 292
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->a()Ljava/lang/String;

    move-result-object v2

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceived(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 422
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    .line 481
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 483
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    const/4 p2, 0x1

    .line 353
    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    .line 355
    iget-object p2, p0, Lcom/meawallet/mtp/x;->h:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    const/4 p2, 0x1

    .line 379
    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    .line 381
    iget-object p2, p0, Lcom/meawallet/mtp/x;->h:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;)V"
        }
    .end annotation

    .line 400
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public final b()V
    .locals 1

    .line 387
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionSuccess()V

    return-void
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 463
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 408
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->a()Ljava/lang/String;

    move-result-object v2

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeCardPinSucceeded(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 435
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final c()V
    .locals 1

    .line 442
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetWalletPinSucceeded()V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .line 429
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onSetCardPinSucceeded(Ljava/lang/String;)V

    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public cancelPendingRequests()Z
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    invoke-virtual {v0}, Lcom/meawallet/mtp/u;->b()V

    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    .line 456
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletPinSucceeded()V

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 488
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 489
    iget-object v0, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onChangeWalletMobilePinStarted(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic e()Lcom/meawallet/mtp/ee;
    .locals 11

    .line 11472
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    .line 12047
    new-instance v10, Lcom/meawallet/mtp/eq;

    invoke-virtual {v0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v4, v0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v5, v0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    iget-object v6, v0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v7, v0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v8, v0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v9, v0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/meawallet/mtp/eq;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    const/4 v0, 0x1

    .line 11474
    new-array v0, v0, [Ljava/lang/Object;

    .line 12085
    iget-object v1, v10, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v10
.end method

.method public getCurrentRequestId()Ljava/lang/String;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    invoke-virtual {v0}, Lcom/meawallet/mtp/u;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegistrationRequestData([BLcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RegistrationRequestParameters;
    .locals 3

    .line 498
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->generateEncryptedRgk()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    .line 500
    iget-object v0, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->createSignedRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 506
    iget-object v0, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    iget-object v2, p0, Lcom/meawallet/mtp/x;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 509
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object p2

    .line 506
    invoke-interface {v0, v1, v2, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 514
    :goto_0
    new-instance v0, Lcom/meawallet/mtp/x$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/meawallet/mtp/x$1;-><init>(Lcom/meawallet/mtp/x;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    .line 525
    new-instance p2, Ljava/security/GeneralSecurityException;

    const-string v0, "Failed to get registration request parameters SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p2, v0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public getSetPinRequestData(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;
    .locals 3

    .line 534
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/x;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v1, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v2, p0, Lcom/meawallet/mtp/x;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 544
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object p1

    .line 542
    invoke-interface {v1, v0, v2, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    move-result-object p1

    return-object p1

    .line 538
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "Communication parameters are missing, Mobile keys are missing. ErrorCode.MOBILE_KEYS_MISSING"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 547
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "Failed to get registration request parameters SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {v0, v1, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public getWalletIdentificationDataProvider()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/meawallet/mtp/x;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    return-object v0
.end method

.method public initialize(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 8

    .line 69
    new-instance v0, Lcom/meawallet/mtp/fi;

    invoke-direct {v0}, Lcom/meawallet/mtp/fi;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/x;->f:Lcom/meawallet/mtp/fi;

    .line 71
    new-instance v0, Lcom/meawallet/mtp/u;

    iget-object v1, p0, Lcom/meawallet/mtp/x;->f:Lcom/meawallet/mtp/fi;

    sget-object v2, Lcom/meawallet/mtp/x;->e:Lcom/google/gson/Gson;

    invoke-direct {v0, p0, v1, v2}, Lcom/meawallet/mtp/u;-><init>(Lcom/meawallet/mtp/af;Lcom/meawallet/mtp/fi;Lcom/google/gson/Gson;)V

    iput-object v0, p0, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/x;->h:Ljava/util/Map;

    .line 74
    iput-object p4, p0, Lcom/meawallet/mtp/x;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    .line 75
    iput-object p1, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 76
    iput-object p5, p0, Lcom/meawallet/mtp/x;->i:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 77
    iput-object p3, p0, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    .line 79
    new-instance p3, Lcom/meawallet/mtp/w;

    invoke-direct {p3, p1, p4}, Lcom/meawallet/mtp/w;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;)V

    iput-object p3, p0, Lcom/meawallet/mtp/x;->g:Lcom/meawallet/mtp/w;

    .line 81
    new-instance p3, Lcom/meawallet/mtp/t;

    iget-object v4, p0, Lcom/meawallet/mtp/x;->f:Lcom/meawallet/mtp/fi;

    move-object v0, p3

    move-object v1, p2

    move-object v2, p1

    move-object v3, p0

    move-object v5, p5

    move-object v6, p4

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/meawallet/mtp/t;-><init>(Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/ad;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    iput-object p3, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    return-void
.end method

.method public processNotificationData(Ljava/lang/String;)V
    .locals 2

    .line 204
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string v0, "This method should not be used, use instead processNotificationData(CmsDPushNotification cmsDPushNotification)"

    const-string v1, ""

    invoke-direct {p1, v0, v1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1
.end method

.method public requestChangePin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    invoke-virtual {v0, p1, p2}, Lcom/meawallet/mtp/t;->b(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/meawallet/mtp/q;

    move-result-object p1

    .line 131
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    .line 4085
    iget-object p1, p1, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object p1
.end method

.method public requestChangeWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/meawallet/mtp/t;->b(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/meawallet/mtp/q;

    move-result-object p1

    .line 152
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    .line 6085
    iget-object p1, p1, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object p1
.end method

.method public requestDeleteCard(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;
    .locals 13

    .line 161
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    .line 6114
    new-instance v12, Lcom/meawallet/mtp/an;

    .line 6115
    invoke-virtual {v0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v4, v0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v5, v0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    iget-object v6, v0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v7, v0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v8, v0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v11, v0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v1, v12

    move-object v9, p1

    move-object v10, p2

    invoke-direct/range {v1 .. v11}, Lcom/meawallet/mtp/an;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    .line 163
    invoke-direct {p0, v12}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    .line 7085
    iget-object p1, v12, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object p1
.end method

.method public requestReplenish(Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Ljava/lang/String;
    .locals 13

    .line 108
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    .line 2070
    new-instance v12, Lcom/meawallet/mtp/ReplenishCommand;

    invoke-virtual {v0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v4, v0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v5, v0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    iget-object v6, v0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v7, v0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v8, v0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v11, v0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v1, v12

    move-object v9, p1

    move-object v10, p2

    invoke-direct/range {v1 .. v11}, Lcom/meawallet/mtp/ReplenishCommand;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    .line 110
    invoke-direct {p0, v12}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    .line 2085
    iget-object p1, v12, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object p1
.end method

.method public requestSetPin(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    invoke-virtual {v0, p1, p2}, Lcom/meawallet/mtp/t;->a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/meawallet/mtp/fj;

    move-result-object p1

    .line 120
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    .line 3085
    iget-object p1, p1, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object p1
.end method

.method public requestSetWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/meawallet/mtp/t;->a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/meawallet/mtp/fj;

    move-result-object p1

    .line 141
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    .line 5085
    iget-object p1, p1, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object p1
.end method

.method public requestSystemHealth()Ljava/lang/String;
    .locals 11

    .line 172
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    .line 7128
    new-instance v10, Lcom/meawallet/mtp/bb;

    invoke-virtual {v0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v4, v0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v5, v0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    iget-object v6, v0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v7, v0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v8, v0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v9, v0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/meawallet/mtp/bb;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    .line 174
    invoke-direct {p0, v10}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    .line 8085
    iget-object v0, v10, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public requestTaskStatusUpdate(Ljava/lang/String;)V
    .locals 12

    .line 95
    iget-object v0, p0, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    iget-object v5, p0, Lcom/meawallet/mtp/x;->f:Lcom/meawallet/mtp/fi;

    .line 1102
    new-instance v11, Lcom/meawallet/mtp/bc;

    invoke-virtual {v0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v4, v0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v6, v0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v7, v0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v8, v0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, v0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v1, v11

    move-object v9, p1

    invoke-direct/range {v1 .. v10}, Lcom/meawallet/mtp/bc;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    .line 96
    invoke-direct {p0, v11}, Lcom/meawallet/mtp/x;->b(Lcom/meawallet/mtp/ee;)V

    return-void
.end method

.method public setRegistrationResponseData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;Ljava/lang/String;)V
    .locals 6

    .line 554
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v2

    .line 556
    iget-object v0, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 559
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v5

    .line 561
    iget-object v0, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v3

    .line 563
    iget-object v0, p0, Lcom/meawallet/mtp/x;->k:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v1, p0, Lcom/meawallet/mtp/x;->j:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    .line 564
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v4

    .line 566
    new-instance p1, Lcom/meawallet/mtp/x$2;

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/x$2;-><init>(Lcom/meawallet/mtp/x;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    .line 584
    iget-object v0, p0, Lcom/meawallet/mtp/x;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0, p1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->setCommunicationParameters(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 587
    new-instance p2, Ljava/security/GeneralSecurityException;

    const-string v0, "Failed to get registration request parameters SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p2, v0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    .line 590
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "Unable to retrieve RGK SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
