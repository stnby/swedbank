.class Lcom/meawallet/mtp/fm;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "fm"


# instance fields
.field private b:Landroid/app/Application;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/app/Application;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/meawallet/mtp/fm;->b:Landroid/app/Application;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .line 18
    sget-object v0, Lcom/meawallet/mtp/fm;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/meawallet/mtp/fm;Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    .line 3193
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->e()Lcom/meawallet/mtp/ci;

    move-result-object v1

    const/4 v2, 0x1

    .line 3208
    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 3210
    :try_start_1
    invoke-static {p1, v0, v1}, Lcom/meawallet/mtp/i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v3

    .line 3211
    invoke-static {v3, v1}, Lcom/meawallet/mtp/i;->a(Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v5

    if-eqz v3, :cond_0

    .line 3214
    invoke-interface {v1, v3}, Lcom/meawallet/mtp/ci;->f(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    .line 3217
    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v4

    aput-object v5, v1, v2

    .line 3220
    :goto_0
    invoke-direct {p0, p1, v5}, Lcom/meawallet/mtp/fm;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/meawallet/mtp/cs; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_0
    move-exception v1

    .line 3237
    :try_start_2
    sget-object v2, Lcom/meawallet/mtp/fm;->a:Ljava/lang/String;

    const-string v3, "deleteCardsInLde(): failed"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception v1

    .line 3224
    invoke-static {v1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v2

    const/16 v3, 0x3f2

    .line 3226
    invoke-interface {v2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v2

    if-ne v3, v2, :cond_1

    .line 3229
    invoke-direct {p0, p1, v0}, Lcom/meawallet/mtp/fm;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 3232
    :cond_1
    sget-object v2, Lcom/meawallet/mtp/fm;->a:Ljava/lang/String;

    const-string v3, "deleteCardsInLde(): failed"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3198
    :goto_1
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-direct {p0}, Lcom/meawallet/mtp/fm;->c()Z
    :try_end_2
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_2 .. :try_end_2} :catch_2

    return-void

    :catch_2
    move-exception v1

    .line 3201
    sget-object v2, Lcom/meawallet/mtp/fm;->a:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3203
    invoke-virtual {v1}, Lcom/meawallet/mtp/NotInitializedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/meawallet/mtp/fm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/fm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/meawallet/mtp/fm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 260
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/ax;->e(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 264
    new-instance v1, Lcom/meawallet/mtp/fm$2;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/meawallet/mtp/fm$2;-><init>(Lcom/meawallet/mtp/fm;Lcom/meawallet/mtp/MeaCardListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 284
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/ax;->e(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardListener;

    move-result-object v0

    const/16 v1, 0x3f3

    if-eqz v0, :cond_1

    const-string v2, "CANCELED"

    .line 290
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "Another request is already in process"

    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 292
    new-instance p2, Lcom/meawallet/mtp/cy;

    const/16 v2, 0xd1

    invoke-direct {p2, v2, p3}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 295
    :cond_0
    new-instance p2, Lcom/meawallet/mtp/cy;

    invoke-direct {p2, v1, p3}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 298
    :goto_0
    new-instance v2, Lcom/meawallet/mtp/fm$3;

    invoke-direct {v2, p0, v0, p2, p1}, Lcom/meawallet/mtp/fm$3;-><init>(Lcom/meawallet/mtp/fm;Lcom/meawallet/mtp/MeaCardListener;Lcom/meawallet/mtp/MeaError;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 308
    :cond_1
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p1

    .line 3154
    iget-object p1, p1, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    .line 310
    new-instance p2, Lcom/meawallet/mtp/cy;

    invoke-direct {p2, v1, p3}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {p1, p2}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/meawallet/mtp/ci;)Z
    .locals 3

    const/4 v0, 0x1

    .line 242
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v1, 0x0

    .line 245
    :try_start_0
    invoke-static {p0, v1, p1}, Lcom/meawallet/mtp/i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    .line 247
    new-array p1, v0, [Ljava/lang/Object;

    aput-object p0, p1, v2
    :try_end_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 253
    :catch_0
    new-array p1, v0, [Ljava/lang/Object;

    aput-object p0, p1, v2

    return v2
.end method

.method private varargs b()Ljava/lang/Boolean;
    .locals 2

    .line 38
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/meawallet/mtp/fm;->b:Landroid/app/Application;

    invoke-static {v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->initialize(Landroid/app/Application;)V

    .line 46
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-direct {p0}, Lcom/meawallet/mtp/fm;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/InitializationFailedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 49
    sget-object v1, Lcom/meawallet/mtp/fm;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method private c()Z
    .locals 8

    const/4 v0, 0x1

    .line 63
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v1

    .line 64
    invoke-static {}, Lcom/meawallet/mtp/dd;->e()Lcom/meawallet/mtp/ci;

    move-result-object v2

    .line 66
    invoke-virtual {v1}, Lcom/meawallet/mtp/cu;->a()Ljava/util/List;

    move-result-object v1

    .line 68
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 69
    invoke-static {}, Lcom/meawallet/mtp/fm;->d()V

    return v0

    .line 74
    :cond_0
    iget-object v3, p0, Lcom/meawallet/mtp/fm;->b:Landroid/app/Application;

    invoke-virtual {v3}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_1

    return v4

    .line 79
    :cond_1
    new-array v3, v0, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1165
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1167
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/meawallet/mtp/MeaCard;

    .line 1169
    invoke-interface {v5}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 1171
    invoke-interface {v5}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/meawallet/mtp/fm;->a(Ljava/lang/String;Lcom/meawallet/mtp/ci;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1175
    new-array v6, v0, [Ljava/lang/Object;

    aput-object v5, v6, v4

    .line 1176
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1180
    :cond_3
    new-array v6, v0, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    .line 1182
    invoke-static {v5, v2}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Lcom/meawallet/mtp/ci;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 85
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/MeaCard;

    .line 87
    invoke-static {}, Lcom/meawallet/mtp/dd;->a()Lcom/meawallet/mtp/dm;

    move-result-object v2

    .line 1303
    iget-object v2, v2, Lcom/meawallet/mtp/dm;->e:Lcom/meawallet/mtp/do;

    .line 88
    new-instance v3, Lcom/meawallet/mtp/fm$1;

    invoke-direct {v3, p0, v2, v1}, Lcom/meawallet/mtp/fm$1;-><init>(Lcom/meawallet/mtp/fm;Lcom/meawallet/mtp/do;Lcom/meawallet/mtp/MeaCard;)V

    .line 125
    invoke-virtual {v3}, Lcom/meawallet/mtp/fk;->a()V

    .line 127
    invoke-static {}, Lcom/meawallet/mtp/dd;->a()Lcom/meawallet/mtp/dm;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    .line 129
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 133
    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->deleteCard(Lcom/mastercard/mpsdk/componentinterface/Card;)Ljava/lang/String;

    goto :goto_1

    .line 137
    :cond_5
    new-array v2, v0, [Ljava/lang/Object;

    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    goto :goto_1

    .line 141
    :cond_6
    invoke-static {}, Lcom/meawallet/mtp/fm;->d()V
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return v0

    :catch_0
    move-exception v1

    .line 152
    sget-object v2, Lcom/meawallet/mtp/fm;->a:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return v0
.end method

.method private static d()V
    .locals 1

    .line 277
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 2154
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    .line 279
    invoke-static {v0}, Lcom/meawallet/mtp/ft;->b(Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    const/4 v0, 0x1

    .line 57
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/meawallet/mtp/fm;->b()Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/fm;->a(Ljava/lang/Boolean;)V

    return-void
.end method
