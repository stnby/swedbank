.class Lcom/meawallet/mtp/dp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;


# static fields
.field private static final a:Ljava/lang/String; = "dp"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/meawallet/mtp/cv;

.field private final d:Lcom/meawallet/mtp/de;

.field private final e:Lcom/meawallet/mtp/k;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/meawallet/mtp/de;Lcom/meawallet/mtp/cv;Lcom/meawallet/mtp/k;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/meawallet/mtp/dp;->b:Landroid/content/Context;

    .line 41
    iput-object p3, p0, Lcom/meawallet/mtp/dp;->c:Lcom/meawallet/mtp/cv;

    .line 42
    iput-object p2, p0, Lcom/meawallet/mtp/dp;->d:Lcom/meawallet/mtp/de;

    .line 43
    iput-object p4, p0, Lcom/meawallet/mtp/dp;->e:Lcom/meawallet/mtp/k;

    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 1

    if-nez p0, :cond_0

    .line 432
    sget-object p0, Lcom/meawallet/mtp/dp;->a:Ljava/lang/String;

    const/16 p1, 0x1f5

    const-string p2, "Transaction completed broadcast was not sent because context is null."

    const/4 p3, 0x0

    new-array p3, p3, [Ljava/lang/Object;

    invoke-static {p0, p1, p2, p3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 437
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 438
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    invoke-static {p3}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "error_code"

    .line 441
    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "error_message"

    .line 442
    invoke-virtual {v0, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    const-string p1, "card_id"

    .line 445
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p5, :cond_2

    const-string p1, "contactless_transaction_data"

    .line 448
    invoke-virtual {v0, p1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 451
    :cond_2
    invoke-static {p0}, Landroidx/i/a/a;->a(Landroid/content/Context;)Landroidx/i/a/a;

    move-result-object p0

    invoke-virtual {p0, v0}, Landroidx/i/a/a;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method private a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    .locals 6

    const-string v3, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 349
    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 8

    const/4 v0, 0x4

    .line 419
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 420
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    .line 422
    iget-object v2, p0, Lcom/meawallet/mtp/dp;->b:Landroid/content/Context;

    const-string v3, "com.meawallet.mtp.intent.ON_TRANSACTION_FAILURE_MESSAGE"

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-static/range {v2 .. v7}, Lcom/meawallet/mtp/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    .locals 11

    .line 357
    sget-object v0, Lcom/meawallet/mtp/dp;->a:Ljava/lang/String;

    const-string v1, "handleTransactionFailure(cardId = %s, errorCode = %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    move v3, p2

    invoke-static {v0, p2, v1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    if-eqz p5, :cond_0

    .line 361
    new-instance v1, Lcom/meawallet/mtp/dp$2;

    move-object v4, v1

    move-object v5, p0

    move-object/from16 v6, p5

    move-object v7, p1

    move v8, p2

    move-object v9, p3

    move-object v10, p4

    invoke-direct/range {v4 .. v10}, Lcom/meawallet/mtp/dp$2;-><init>(Lcom/meawallet/mtp/dp;Lcom/meawallet/mtp/MeaContactlessTransactionListener;Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    invoke-static {v1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 373
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 8

    const/4 v0, 0x2

    .line 397
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 399
    iget-object v2, p0, Lcom/meawallet/mtp/dp;->b:Landroid/content/Context;

    const-string v3, "com.meawallet.mtp.intent.ON_AUTHENTICATION_REQUIRED_MESSAGE"

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v4, p1

    move-object v7, p2

    invoke-static/range {v2 .. v7}, Lcom/meawallet/mtp/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    .locals 1

    if-eqz p3, :cond_0

    .line 331
    new-instance v0, Lcom/meawallet/mtp/dp$1;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/meawallet/mtp/dp$1;-><init>(Lcom/meawallet/mtp/dp;Lcom/meawallet/mtp/MeaContactlessTransactionListener;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    invoke-static {v0}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 342
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/meawallet/mtp/dp;->b(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    return-void
.end method

.method private b(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 8

    const/4 v0, 0x2

    .line 408
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 410
    iget-object v2, p0, Lcom/meawallet/mtp/dp;->b:Landroid/content/Context;

    const-string v3, "com.meawallet.mtp.intent.ON_TRANSACTION_SUBMITTED_MESSAGE"

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v4, p1

    move-object v7, p2

    invoke-static/range {v2 .. v7}, Lcom/meawallet/mtp/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    return-void
.end method

.method private b(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    .locals 2

    const/4 v0, 0x2

    .line 379
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    if-eqz p3, :cond_0

    .line 383
    new-instance v0, Lcom/meawallet/mtp/dp$3;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/meawallet/mtp/dp$3;-><init>(Lcom/meawallet/mtp/dp;Lcom/meawallet/mtp/MeaContactlessTransactionListener;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    invoke-static {v0}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 393
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized onContactlessPaymentAborted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;Ljava/lang/Exception;)V
    .locals 12

    monitor-enter p0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 252
    :try_start_0
    sget-object p1, Lcom/meawallet/mtp/dp;->a:Ljava/lang/String;

    const/16 p2, 0x1f5

    const-string p3, "Card is null receiving onContactlessPaymentAborted() callback."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2, p3, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto/16 :goto_5

    .line 257
    :cond_0
    :try_start_1
    sget-object v1, Lcom/meawallet/mtp/dp;->a:Ljava/lang/String;

    const-string v2, "onContactlessPaymentAborted(cardId = %s, abortReason = %s)"

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-static {v1, p3, v2, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->TERMINAL_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-virtual {v1, p2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p3, :cond_1

    const-string v1, "TERMINAL_INACTIVITY_TIMEOUT"

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 262
    :cond_1
    iget-object v1, p0, Lcom/meawallet/mtp/dp;->c:Lcom/meawallet/mtp/cv;

    if-eqz v1, :cond_2

    .line 263
    iget-object v1, p0, Lcom/meawallet/mtp/dp;->c:Lcom/meawallet/mtp/cv;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cv;->a()V

    .line 266
    :cond_2
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v1

    .line 267
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/ax;->c(Ljava/lang/String;)Lcom/meawallet/mtp/MeaContactlessTransactionListener;

    move-result-object v11

    const-string v1, ""

    .line 272
    sget-object v2, Lcom/meawallet/mtp/dp$4;->c:[I

    invoke-virtual {p2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->ordinal()I

    move-result p2

    aget p2, v2, p2

    packed-switch p2, :pswitch_data_0

    const/16 p2, 0x44d

    move-object v9, v1

    const/16 v8, 0x44d

    goto/16 :goto_4

    :pswitch_0
    const/16 p2, 0x450

    if-eqz p3, :cond_3

    .line 313
    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p3

    move-object v9, p3

    goto :goto_0

    :cond_3
    move-object v9, v1

    :goto_0
    const/16 v8, 0x450

    goto/16 :goto_4

    :pswitch_1
    const/16 p2, 0x44f

    if-eqz p3, :cond_9

    .line 280
    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 281
    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p3

    const/4 v1, -0x1

    .line 283
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v4, -0x7c4a85c3

    if-eq v2, v4, :cond_7

    const v3, -0x3f967a99

    if-eq v2, v3, :cond_6

    const v3, 0xb9c78e9

    if-eq v2, v3, :cond_5

    const v0, 0x1c5c224d

    if-eq v2, v0, :cond_4

    goto :goto_1

    :cond_4
    const-string v0, "COMMAND_INCOMPATIBLE"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x3

    goto :goto_2

    :cond_5
    const-string v2, "PERSISTENT_TRANSACTION_CONTEXT_NOT_SUPPORTED"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    goto :goto_2

    :cond_6
    const-string v0, "CONTACTLESS_TRANSACTION_NOT_POSSIBLE_BECAUSE_TRANSACTION_MANAGER_IS_BUSY"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_2

    :cond_7
    const-string v0, "ERROR_MISSING_ICC_PRIVATE_KEY"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x2

    goto :goto_2

    :cond_8
    :goto_1
    const/4 v0, -0x1

    :goto_2
    packed-switch v0, :pswitch_data_1

    goto :goto_3

    :pswitch_2
    const/16 p2, 0x45f

    goto :goto_3

    :pswitch_3
    const/16 p2, 0x45e

    move-object v9, p3

    const/16 v8, 0x45e

    goto :goto_4

    :pswitch_4
    const/16 p2, 0x45d

    move-object v9, p3

    const/16 v8, 0x45d

    goto :goto_4

    :pswitch_5
    const/16 p2, 0x457

    move-object v9, p3

    const/16 v8, 0x457

    goto :goto_4

    :goto_3
    move v8, p2

    move-object v9, p3

    goto :goto_4

    :cond_9
    move-object v9, v1

    const/16 v8, 0x44f

    goto :goto_4

    :pswitch_6
    const/16 p2, 0x44e

    move-object v9, v1

    const/16 v8, 0x44e

    .line 321
    :goto_4
    new-instance v10, Lcom/meawallet/mtp/cw;

    .line 322
    invoke-static {}, Lcom/meawallet/mtp/h;->a()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p2

    invoke-static {p2}, Lcom/meawallet/mtp/h;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v10, p2}, Lcom/meawallet/mtp/cw;-><init>(Ljava/lang/String;)V

    .line 324
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v7

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 325
    monitor-exit p0

    return-void

    .line 250
    :goto_5
    monitor-exit p0

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized onContactlessPaymentCompleted(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
    .locals 12

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 64
    :try_start_0
    sget-object p1, Lcom/meawallet/mtp/dp;->a:Ljava/lang/String;

    const/16 p2, 0x1f5

    const-string v2, "Card is null receiving onContactlessPaymentCompleted() callback."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, p2, v2, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 66
    iget-object p1, p0, Lcom/meawallet/mtp/dp;->e:Lcom/meawallet/mtp/k;

    invoke-virtual {p1, v0}, Lcom/meawallet/mtp/k;->a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto/16 :goto_3

    .line 71
    :cond_0
    :try_start_1
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionOutcome()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    move-result-object v2

    const/4 v3, 0x2

    .line 73
    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x1

    aput-object v2, v3, v4

    .line 75
    iget-object v3, p0, Lcom/meawallet/mtp/dp;->c:Lcom/meawallet/mtp/cv;

    if-eqz v3, :cond_1

    .line 76
    iget-object v3, p0, Lcom/meawallet/mtp/dp;->c:Lcom/meawallet/mtp/cv;

    invoke-virtual {v3, v2}, Lcom/meawallet/mtp/cv;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;)V

    .line 79
    :cond_1
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v3

    .line 80
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/meawallet/mtp/ax;->c(Ljava/lang/String;)Lcom/meawallet/mtp/MeaContactlessTransactionListener;

    move-result-object v11

    .line 82
    new-instance v10, Lcom/meawallet/mtp/cw;

    invoke-direct {v10, p1, p2}, Lcom/meawallet/mtp/cw;-><init>(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V

    .line 84
    iget-object v3, p0, Lcom/meawallet/mtp/dp;->d:Lcom/meawallet/mtp/de;

    invoke-interface {v3}, Lcom/meawallet/mtp/de;->a()Lcom/meawallet/mtp/cq;

    move-result-object v3

    .line 86
    new-array v5, v4, [Ljava/lang/Object;

    aput-object v3, v5, v1

    .line 88
    sget-object v1, Lcom/meawallet/mtp/dp$4;->b:[I

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 215
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2

    :pswitch_0
    if-nez v3, :cond_2

    .line 175
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x451

    const-string v9, "Mea advice clarification is undefined."

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    .line 181
    :cond_2
    :try_start_2
    sget-object p2, Lcom/meawallet/mtp/dp$4;->a:[I

    invoke-virtual {v3}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v0

    aget p2, p2, v0

    if-eq p2, v4, :cond_3

    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 205
    :pswitch_1
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x38d

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 211
    :goto_0
    monitor-exit p0

    return-void

    .line 190
    :pswitch_2
    :try_start_3
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x454

    const-string v9, "Consent check failed."

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    monitor-exit p0

    return-void

    .line 185
    :pswitch_3
    :try_start_4
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x3ed

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 186
    monitor-exit p0

    return-void

    .line 200
    :cond_3
    :pswitch_4
    :try_start_5
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v10, v11}, Lcom/meawallet/mtp/dp;->b(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 201
    monitor-exit p0

    return-void

    .line 167
    :pswitch_5
    :try_start_6
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x452

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 169
    monitor-exit p0

    return-void

    .line 157
    :pswitch_6
    :try_start_7
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object p2

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getRichTransactionType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    move-result-object p2

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->REFUND:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    if-ne p2, v0, :cond_4

    .line 160
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    monitor-exit p0

    return-void

    .line 162
    :cond_4
    :try_start_8
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x453

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 164
    monitor-exit p0

    return-void

    :pswitch_7
    if-nez v3, :cond_5

    .line 101
    :try_start_9
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x454

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    monitor-exit p0

    return-void

    .line 106
    :cond_5
    :try_start_a
    sget-object p2, Lcom/meawallet/mtp/dp$4;->a:[I

    invoke-virtual {v3}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v0

    aget p2, p2, v0

    packed-switch p2, :pswitch_data_2

    goto :goto_1

    .line 146
    :pswitch_8
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x45c

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V

    goto :goto_1

    .line 138
    :pswitch_9
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x45b

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 142
    monitor-exit p0

    return-void

    .line 130
    :pswitch_a
    :try_start_b
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x45a

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 134
    monitor-exit p0

    return-void

    .line 122
    :pswitch_b
    :try_start_c
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x459

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 126
    monitor-exit p0

    return-void

    .line 114
    :pswitch_c
    :try_start_d
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x456

    invoke-direct {p0, p1, p2, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 109
    :pswitch_d
    :try_start_e
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v10, v11}, Lcom/meawallet/mtp/dp;->b(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 110
    monitor-exit p0

    return-void

    .line 154
    :goto_1
    monitor-exit p0

    return-void

    .line 91
    :pswitch_e
    :try_start_f
    iget-object p2, p0, Lcom/meawallet/mtp/dp;->e:Lcom/meawallet/mtp/k;

    invoke-virtual {p2, v0}, Lcom/meawallet/mtp/k;->a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;

    .line 93
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v10, v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    :goto_2
    const/16 v8, 0x451

    :try_start_10
    const-string v9, "Transaction outcome is undefined."

    move-object v6, p0

    .line 215
    invoke-direct/range {v6 .. v11}, Lcom/meawallet/mtp/dp;->a(Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 221
    monitor-exit p0

    return-void

    .line 62
    :goto_3
    monitor-exit p0

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method public onContactlessPaymentIncident(Lcom/mastercard/mpsdk/componentinterface/Card;Ljava/lang/Exception;)V
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 234
    sget-object p1, Lcom/meawallet/mtp/dp;->a:Ljava/lang/String;

    const/16 p2, 0x1f5

    const-string v1, "Card is null receiving onContactlessPaymentIncident() callback."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2, v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 239
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/dp;->a:Ljava/lang/String;

    const-string v2, "onContactlessPaymentIncident(cardId = %s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v0

    invoke-static {v1, p2, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onTransactionStopped()V
    .locals 0

    return-void
.end method
