.class final Lcom/meawallet/mtp/au$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DsrpData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/au;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;)V
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAip()[B
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->aip:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 4

    .line 319
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->cvmModel:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 321
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->cvmModel:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x66c3a89a

    if-eq v2, v3, :cond_3

    const v3, -0x5e1f552b

    if-eq v2, v3, :cond_2

    const v3, 0x7473b8c5

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "FLEXIBLE_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "CDCVM_ALWAYS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const-string v2, "CARD_LIKE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 333
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 330
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CARD_LIKE:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 328
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 326
    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getExpiryDate()[B
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->expirationDate:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->issuerApplicationData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPanSequenceNumber()[B
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->panSequenceNumber:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPar()[B
    .locals 1

    .line 372
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->par:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->par:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTrack2EquivalentData()[B
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->track2Equivalent:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getUcafVersion()Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;
    .locals 4

    .line 355
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->ucafVersion:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 357
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->ucafVersion:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/16 v3, 0xa9a

    if-eq v2, v3, :cond_2

    const v3, 0x1ca0c41f

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "V0_PLUS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "V0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    :cond_3
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 367
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    .line 364
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0_PLUS:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    .line 362
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;->V0:Lcom/mastercard/mpsdk/componentinterface/CardUcafVersion;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 4

    .line 337
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->umdGeneration:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 339
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$4;->a:Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DsrpDataV2Json;->umdGeneration:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0xc95c30a

    if-eq v2, v3, :cond_3

    const v3, 0x42a6720a

    if-eq v2, v3, :cond_2

    const v3, 0x42ec93be

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "ALWAYS_VALIDATED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v2, "ALWAYS_GENERATE_RANDOM_UMD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    goto :goto_0

    :cond_3
    const-string v2, "VALIDATED_ON_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 351
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 348
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 346
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 344
    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
