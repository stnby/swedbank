.class public interface abstract Lcom/meawallet/mtp/MeaAuthenticationListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/MeaCoreListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/MeaCoreListener<",
        "Lcom/meawallet/mtp/MeaError;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract onCardPinRequired(Lcom/meawallet/mtp/MeaCard;)V
.end method

.method public abstract onDeviceUnlockRequired()V
.end method

.method public abstract onFingerprintRequired()V
.end method

.method public abstract onWalletPinRequired()V
.end method
