.class Lcom/meawallet/mtp/hb;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# instance fields
.field a:Lcom/google/gson/JsonElement;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field

.field b:[Lcom/meawallet/mtp/WspErrorResponseData;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "errors"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "responseId"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/meawallet/mtp/hb;->b:[Lcom/meawallet/mtp/WspErrorResponseData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method validate()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/meawallet/mtp/hb;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Response id is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
