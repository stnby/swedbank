.class final Lcom/meawallet/mtp/dz;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/g<",
        "Lcom/meawallet/mtp/ProvisionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private j:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 9

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    .line 39
    invoke-direct/range {v0 .. v8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object v1, p1

    .line 48
    iput-object v1, v0, Lcom/meawallet/mtp/dz;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final synthetic a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    .line 9058
    invoke-static {p1, p2}, Lcom/meawallet/mtp/ProvisionResponse;->valueOf(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/ProvisionResponse;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 3

    .line 123
    :try_start_0
    new-instance v0, Lcom/meawallet/mtp/ea;

    iget-object v1, p0, Lcom/meawallet/mtp/dz;->j:Ljava/lang/String;

    .line 2085
    iget-object v2, p0, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    .line 123
    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ea;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3019
    invoke-static {}, Lcom/meawallet/mtp/JsonUtil;->a()Lcom/google/gson/Gson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/dz;->a(Ljava/lang/String;)Lcom/meawallet/mtp/y;

    move-result-object v0

    .line 3034
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/dz;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 147
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 149
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 147
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/dz;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 143
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/dz;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 136
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute provision command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/dz;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 129
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute Provision HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/dz;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 107
    iput-object p2, p0, Lcom/meawallet/mtp/dz;->c:Ljava/lang/String;

    .line 108
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 109
    iput-object p1, p0, Lcom/meawallet/mtp/dz;->h:Lcom/meawallet/mtp/ae;

    .line 111
    iget-object p1, p0, Lcom/meawallet/mtp/dz;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-ne p1, v0, :cond_0

    .line 112
    iget-object p1, p0, Lcom/meawallet/mtp/dz;->d:Lcom/meawallet/mtp/ad;

    iget-object v0, p0, Lcom/meawallet/mtp/dz;->j:Ljava/lang/String;

    invoke-interface {p1, v0, p2, p3, p4}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final synthetic a(Lcom/meawallet/mtp/s;)V
    .locals 4

    .line 24
    check-cast p1, Lcom/meawallet/mtp/ProvisionResponse;

    .line 5064
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/dz;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    .line 5066
    invoke-static {v0}, Lcom/meawallet/mtp/dz;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 5068
    sget-object p1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v0, "MOBILE_KEYS_MISSING"

    const-string v1, "Failed to decrypt provision response."

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/meawallet/mtp/dz;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 5076
    :cond_0
    invoke-virtual {p1}, Lcom/meawallet/mtp/ProvisionResponse;->getIccKek()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 5079
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 5080
    new-instance v3, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    invoke-static {v1}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    goto :goto_0

    :cond_1
    move-object v3, v2

    .line 5083
    :goto_0
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    .line 5086
    invoke-virtual {p1}, Lcom/meawallet/mtp/ProvisionResponse;->getCardProfile()Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;

    move-result-object p1

    iget-object v1, p0, Lcom/meawallet/mtp/dz;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    if-eqz p1, :cond_3

    .line 6044
    sput-object v3, Lcom/meawallet/mtp/as;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    .line 6045
    sput-object v0, Lcom/meawallet/mtp/as;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    .line 6046
    sput-object v1, Lcom/meawallet/mtp/as;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 6048
    invoke-interface {p1}, Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;->getProfileVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V1:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_2

    .line 6049
    check-cast p1, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    .line 7030
    new-instance v2, Lcom/meawallet/mtp/at$1;

    invoke-direct {v2, p1}, Lcom/meawallet/mtp/at$1;-><init>(Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;)V

    goto :goto_1

    .line 6052
    :cond_2
    invoke-interface {p1}, Lcom/mastercard/mpsdk/card/profile/DigitizedCardProfile;->getProfileVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;->V2:Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    if-ne v0, v1, :cond_3

    .line 6053
    check-cast p1, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    .line 8030
    new-instance v2, Lcom/meawallet/mtp/au$1;

    invoke-direct {v2, p1}, Lcom/meawallet/mtp/au$1;-><init>(Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)V

    .line 5091
    :cond_3
    :goto_1
    sget-object p1, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 8317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 5093
    sget-object p1, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    iput-object p1, p0, Lcom/meawallet/mtp/dz;->i:Lcom/meawallet/mtp/ah;

    .line 5095
    iget-object p1, p0, Lcom/meawallet/mtp/dz;->d:Lcom/meawallet/mtp/ad;

    iget-object v0, p0, Lcom/meawallet/mtp/dz;->j:Ljava/lang/String;

    invoke-interface {p1, v2, v0}, Lcom/meawallet/mtp/ad;->a(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 5099
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Unable to decrypt mobile keys"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/dz;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/provision"

    return-object v0
.end method

.method public final l()V
    .locals 5

    .line 3230
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 156
    sget-object v1, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/meawallet/mtp/dz;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/dz;->j:Ljava/lang/String;

    const-string v3, "CANCELED"

    const-string v4, "Too many commands already in queue."

    invoke-interface {v0, v1, v3, v4, v2}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 4230
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 163
    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_1

    .line 164
    iget-object v0, p0, Lcom/meawallet/mtp/dz;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/dz;->j:Ljava/lang/String;

    const-string v3, "CANCELED"

    const-string v4, "Duplicate Request"

    invoke-interface {v0, v1, v3, v4, v2}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 172
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 4317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 173
    iget-object v0, p0, Lcom/meawallet/mtp/dz;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/dz;->j:Ljava/lang/String;

    const-string v3, "CANCELED"

    const-string v4, "Provision command cancelled or Could not get a valid session from CMS-D"

    invoke-interface {v0, v1, v3, v4, v2}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 179
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/dz;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 185
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
