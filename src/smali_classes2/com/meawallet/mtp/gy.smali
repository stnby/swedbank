.class final Lcom/meawallet/mtp/gy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/meawallet/mtp/ep;

.field static final b:Lcom/meawallet/mtp/ep;

.field static final c:Lcom/meawallet/mtp/ep;

.field static final d:Lcom/meawallet/mtp/ep;

.field static final e:Lcom/meawallet/mtp/ep;

.field static final f:Lcom/meawallet/mtp/ep;

.field static final g:Lcom/meawallet/mtp/ep;

.field static final h:Lcom/meawallet/mtp/ep;

.field static final i:Lcom/meawallet/mtp/ep;

.field static final j:Lcom/meawallet/mtp/ep;

.field static final k:Lcom/meawallet/mtp/ep;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 10
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "register"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->a:Lcom/meawallet/mtp/ep;

    .line 12
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "initializeDigitizationWithEncryptedPan"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->b:Lcom/meawallet/mtp/ep;

    .line 14
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "initializeDigitizationWithPan"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->c:Lcom/meawallet/mtp/ep;

    .line 16
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "initializeDigitizationWithSecret"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->d:Lcom/meawallet/mtp/ep;

    .line 18
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "completeDigitization"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->e:Lcom/meawallet/mtp/ep;

    .line 20
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "initializeAuthentication"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->f:Lcom/meawallet/mtp/ep;

    .line 22
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "completeAuthentication"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->g:Lcom/meawallet/mtp/ep;

    .line 24
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "getAsset"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->h:Lcom/meawallet/mtp/ep;

    .line 26
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "updateDeviceInfo"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->i:Lcom/meawallet/mtp/ep;

    .line 28
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "getTransactionHistory"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->j:Lcom/meawallet/mtp/ep;

    .line 30
    new-instance v0, Lcom/meawallet/mtp/ep;

    const-string v1, "deleteCards"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ep;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/meawallet/mtp/gy;->k:Lcom/meawallet/mtp/ep;

    return-void
.end method
