.class Lcom/meawallet/mtp/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "w"


# instance fields
.field private b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/meawallet/mtp/w;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 26
    iput-object p2, p0, Lcom/meawallet/mtp/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    return-void
.end method


# virtual methods
.method final a(Lcom/google/gson/Gson;Lcom/meawallet/mtp/v;)Lcom/meawallet/mtp/ab;
    .locals 4

    .line 1049
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/w;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1051
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1052
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1053
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1054
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1061
    iget-object v1, p0, Lcom/meawallet/mtp/w;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 1062
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v2

    .line 1063
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    .line 2026
    iget-object p2, p2, Lcom/meawallet/mtp/v;->b:Ljava/lang/String;

    .line 1064
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    const/4 v3, 0x2

    invoke-static {p2, v3}, Landroid/util/Base64;->decode([BI)[B

    move-result-object p2

    .line 1061
    invoke-interface {v1, v2, v0, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->decryptPushedRemoteData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)[B

    move-result-object p2

    const/16 v0, 0x10

    .line 1066
    array-length v1, p2

    invoke-static {p2, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p2

    .line 35
    invoke-static {p1, p2}, Lcom/meawallet/mtp/aa;->a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/aa;

    move-result-object p1

    const/4 p2, 0x1

    .line 37
    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/meawallet/mtp/aa;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    .line 39
    new-instance p2, Lcom/meawallet/mtp/ab;

    invoke-direct {p2, p1}, Lcom/meawallet/mtp/ab;-><init>(Lcom/meawallet/mtp/aa;)V

    return-object p2

    .line 1056
    :cond_0
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string p2, "Failed to decrypt push notification data, Mobile keys are missing."

    const-string v0, "MOBILE_KEYS_MISSING"

    invoke-direct {p1, p2, v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 42
    new-instance p2, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string v0, "Failed to decrypt push notification data"

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    invoke-direct {p2, v0, v1, p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method
