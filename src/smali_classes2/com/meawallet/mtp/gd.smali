.class Lcom/meawallet/mtp/gd;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# instance fields
.field a:Lcom/meawallet/mtp/MeaDigitizationDecision;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "decision"
    .end annotation
.end field

.field b:[Lcom/meawallet/mtp/MeaAuthenticationMethod;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "authenticationMethods"
    .end annotation
.end field

.field c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenUniqueReference"
    .end annotation
.end field

.field d:Lcom/meawallet/mtp/MeaProductConfig;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "productConfig"
    .end annotation
.end field

.field e:Lcom/meawallet/mtp/MeaTokenInfo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenInfo"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method


# virtual methods
.method validate()V
    .locals 4

    .line 47
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->a:Lcom/meawallet/mtp/MeaDigitizationDecision;

    if-eqz v0, :cond_7

    .line 49
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->a:Lcom/meawallet/mtp/MeaDigitizationDecision;

    sget-object v1, Lcom/meawallet/mtp/MeaDigitizationDecision;->APPROVED:Lcom/meawallet/mtp/MeaDigitizationDecision;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/gd;->a:Lcom/meawallet/mtp/MeaDigitizationDecision;

    sget-object v1, Lcom/meawallet/mtp/MeaDigitizationDecision;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaDigitizationDecision;

    if-ne v0, v1, :cond_1

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 54
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->d:Lcom/meawallet/mtp/MeaProductConfig;

    if-eqz v0, :cond_5

    .line 56
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->d:Lcom/meawallet/mtp/MeaProductConfig;

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaProductConfig;->validate()V

    .line 58
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->e:Lcom/meawallet/mtp/MeaTokenInfo;

    if-eqz v0, :cond_4

    .line 60
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->e:Lcom/meawallet/mtp/MeaTokenInfo;

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaTokenInfo;->validate()V

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->a:Lcom/meawallet/mtp/MeaDigitizationDecision;

    sget-object v1, Lcom/meawallet/mtp/MeaDigitizationDecision;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaDigitizationDecision;

    if-ne v0, v1, :cond_3

    .line 64
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->b:[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    if-eqz v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/meawallet/mtp/gd;->b:[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, v0, v2

    .line 67
    invoke-virtual {v3}, Lcom/meawallet/mtp/MeaAuthenticationMethod;->validate()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 64
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Authentication methods are null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void

    .line 58
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Token info is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_5
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Product config is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_6
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Token unique reference is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_7
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Digitization decision is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
