.class Lcom/meawallet/mtp/fr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String; = "fr"


# instance fields
.field a:Lcom/meawallet/mtp/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/fr;)Lcom/meawallet/mtp/ci;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    return-object p0
.end method


# virtual methods
.method final a(Ljava/util/Currency;I)Z
    .locals 4

    const/4 v0, 0x2

    .line 62
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 64
    iget-object v0, p0, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {v0, p1}, Lcom/meawallet/mtp/ci;->b(Ljava/util/Currency;)Lcom/meawallet/mtp/MeaTransactionLimit;

    move-result-object p1

    .line 66
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    if-eqz p1, :cond_1

    .line 70
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaTransactionLimit;->getMaxAmount()I

    move-result p1

    if-gt p2, p1, :cond_0

    return v3

    :cond_0
    return v2

    .line 73
    :cond_1
    iget-object p1, p0, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {p1}, Lcom/meawallet/mtp/ci;->n()Ljava/lang/Integer;

    move-result-object p1

    .line 75
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    if-eqz p1, :cond_3

    .line 77
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-gt p2, p1, :cond_2

    goto :goto_0

    :cond_2
    return v2

    :cond_3
    :goto_0
    return v3
.end method
