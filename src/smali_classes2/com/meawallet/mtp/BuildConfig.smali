.class public final Lcom/meawallet/mtp/BuildConfig;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final AMEX_ENABLED:Ljava/lang/Boolean;

.field public static final APDU_LOGGING:Ljava/lang/Boolean;

.field public static final APDU_LOGGING_DEBUG_MODE:Ljava/lang/Boolean;

.field public static final APPLICATION_ID:Ljava/lang/String; = "com.meawallet.mtp"

.field public static final BUILD_TYPE:Ljava/lang/String; = "release"

.field public static final CMS_URL:Ljava/lang/String; = "https://cmsd.meawallet.com"

.field public static final CMS_URL_PATH:Ljava/lang/String; = "/paymentapp/1/0/"

.field public static final CURRENCY_TRANSACTION_LIMITS:Ljava/lang/Boolean;

.field public static final DEBUG:Z = false

.field public static final DEBUGGER_ALLOWED:Ljava/lang/Boolean;

.field public static final DEBUG_MODE:Ljava/lang/Boolean;

.field public static final DEVICE_UNLOCK_REQ_FOR_EACH_TXS:Ljava/lang/Boolean;

.field public static final FALLBACK_TO_DEVICE_UNLOCK_ALLOWED:Ljava/lang/Boolean;

.field public static final FALLBACK_TO_MOBILE_PIN_ALLOWED:Ljava/lang/Boolean;

.field public static final FALLBACK_TO_MOBILE_PIN_ALLOWED_IF_LOCKSCREEN_AVAILABLE_BUT_NOT_ENABLED:Ljava/lang/Boolean;

.field public static final FALLBACK_WHEN_FINGERPRINT_NOT_READY:Ljava/lang/Boolean;

.field public static final FLAVOR:Ljava/lang/String; = "_swedbank_lt_prod"

.field public static final FLAVOR_VERSION_NAME:Ljava/lang/String; = "mtp-swedbank-lt-prod-2.0.8-684"

.field public static final FLAVOR_customerConfig:Ljava/lang/String; = "_swedbank_lt"

.field public static final FLAVOR_environment:Ljava/lang/String; = "_prod"

.field public static final FORCE_TLS_PROTOCOL:[Ljava/lang/String;

.field public static final LIBMTP_VERSION:Ljava/lang/String; = "1.8.1"

.field public static final LOST_OR_STOLEN:Ljava/lang/Boolean;

.field public static final MAX_TRANSACTIONS_WITHOUT_AUTHENTICATION:I = 0x3

.field public static final MC_ENABLED:Ljava/lang/Boolean;

.field public static final MIN_SDK_VERSION:Ljava/lang/Integer;

.field public static final MOBILE_PUBLIC_KEY_FINGERPRINT:Ljava/lang/String; = "2DC6385290121D2B7A6DB7FAAE1EB56C2C9F2D8858687346A68FF8908E4035DE"

.field public static final MPSDK_LOGGING_INFO:Ljava/lang/Boolean;

.field public static final NUMBER_OF_PROLONGED_TXS:I = 0x1

.field public static final PAYMENT_APP_ID:Ljava/lang/String; = "SWEDBANKLIETUVA"

.field public static final PROLONGED_AUTH_SECONDS:I = 0x3c

.field public static final REMOTE_REQUEST_TIMEOUT:Ljava/lang/Integer;

.field public static final REPLENISH_THRESHOLD:I = 0x5

.field public static final RNS_DATA_MESSAGE_TAG:Ljava/lang/String; = "mea_data"

.field public static final RNS_TOKEN_STATUS_MESSAGE_TAG:Ljava/lang/String; = "mea_token_status"

.field public static final RNS_TRANSACTION_MESSAGE_TAG:Ljava/lang/String; = "mea_transaction"

.field public static final SAVE_AUTH_WHEN_LOCKED:Ljava/lang/Boolean;

.field public static final SCREEN_OFF_ALLOWED:Z = false

.field public static final SDK_VERSION:Ljava/lang/String; = "2.0.8"

.field public static final STORAGE_VERSION:Ljava/lang/String; = "1.0"

.field public static final SUPPORTED_CDCVMS:B = 0x4t

.field public static final USER_MODULE_ENABLED:Ljava/lang/Boolean;

.field public static final USR_URL:Ljava/lang/String; = "https://usr.meawallet.com"

.field public static final USR_VERSION:Ljava/lang/Integer;

.field public static final VERSION_CODE:I = 0x2ac

.field public static final VERSION_NAME:Ljava/lang/String; = "2.0.8"

.field public static final WALLET_CVM_MODEL:Ljava/lang/String; = "ALWAYS_CDCVM"

.field public static final WALLET_PUBLIC_KEY_FINGERPRINT:Ljava/lang/String; = "F397AEC7A252FB813AB80E7F0085CBD8071D515E3CD8C9FC4167E95AB91F7B6D"

.field public static final WSP_URL:Ljava/lang/String; = "https://wsp.meawallet.com"

.field public static final WSP_URL_PATH:Ljava/lang/String; = "/wsp/wallet"

.field public static final WSP_VERSION:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->APDU_LOGGING:Ljava/lang/Boolean;

    .line 22
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->APDU_LOGGING_DEBUG_MODE:Ljava/lang/Boolean;

    .line 23
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->DEBUGGER_ALLOWED:Ljava/lang/Boolean;

    .line 24
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->DEBUG_MODE:Ljava/lang/Boolean;

    .line 26
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->AMEX_ENABLED:Ljava/lang/Boolean;

    .line 27
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->CURRENCY_TRANSACTION_LIMITS:Ljava/lang/Boolean;

    .line 28
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->DEVICE_UNLOCK_REQ_FOR_EACH_TXS:Ljava/lang/Boolean;

    .line 29
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_TO_DEVICE_UNLOCK_ALLOWED:Ljava/lang/Boolean;

    .line 30
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_TO_MOBILE_PIN_ALLOWED:Ljava/lang/Boolean;

    .line 31
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_WHEN_FINGERPRINT_NOT_READY:Ljava/lang/Boolean;

    .line 32
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->MC_ENABLED:Ljava/lang/Boolean;

    .line 35
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->SAVE_AUTH_WHEN_LOCKED:Ljava/lang/Boolean;

    .line 38
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->USER_MODULE_ENABLED:Ljava/lang/Boolean;

    .line 46
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_TO_MOBILE_PIN_ALLOWED_IF_LOCKSCREEN_AVAILABLE_BUT_NOT_ENABLED:Ljava/lang/Boolean;

    const-string v0, "TLSv1.2"

    .line 47
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->FORCE_TLS_PROTOCOL:[Ljava/lang/String;

    .line 49
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->LOST_OR_STOLEN:Ljava/lang/Boolean;

    const/16 v0, 0x13

    .line 51
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->MIN_SDK_VERSION:Ljava/lang/Integer;

    .line 52
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->MPSDK_LOGGING_INFO:Ljava/lang/Boolean;

    const v0, 0x9c40

    .line 54
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->REMOTE_REQUEST_TIMEOUT:Ljava/lang/Integer;

    const/4 v0, 0x1

    .line 60
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->USR_VERSION:Ljava/lang/Integer;

    const/4 v0, 0x2

    .line 62
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/BuildConfig;->WSP_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
