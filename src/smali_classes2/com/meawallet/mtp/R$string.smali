.class public final Lcom/meawallet/mtp/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final aidGroupDescription:I = 0x7f110027

.field public static final apduServiceDescription:I = 0x7f110028

.field public static final app_name:I = 0x7f110029

.field public static final auth_title:I = 0x7f11002e

.field public static final cancel:I = 0x7f110056

.field public static final cirrus:I = 0x7f1100e1

.field public static final common_google_play_services_enable_button:I = 0x7f1100e9

.field public static final common_google_play_services_enable_text:I = 0x7f1100ea

.field public static final common_google_play_services_enable_title:I = 0x7f1100eb

.field public static final common_google_play_services_install_button:I = 0x7f1100ec

.field public static final common_google_play_services_install_text:I = 0x7f1100ed

.field public static final common_google_play_services_install_title:I = 0x7f1100ee

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1100ef

.field public static final common_google_play_services_notification_ticker:I = 0x7f1100f0

.field public static final common_google_play_services_unknown_issue:I = 0x7f1100f1

.field public static final common_google_play_services_unsupported_text:I = 0x7f1100f2

.field public static final common_google_play_services_update_button:I = 0x7f1100f3

.field public static final common_google_play_services_update_text:I = 0x7f1100f4

.field public static final common_google_play_services_update_title:I = 0x7f1100f5

.field public static final common_google_play_services_updating_text:I = 0x7f1100f6

.field public static final common_google_play_services_wear_update_text:I = 0x7f1100f7

.field public static final common_open_on_phone:I = 0x7f1100f8

.field public static final common_signin_button_text:I = 0x7f1100f9

.field public static final common_signin_button_text_long:I = 0x7f1100fa

.field public static final fingerprint_auth_not_ready:I = 0x7f11012e

.field public static final fingerprint_description:I = 0x7f11012f

.field public static final fingerprint_hint:I = 0x7f110130

.field public static final maestro_belgium:I = 0x7f110156

.field public static final maestro_debit:I = 0x7f110157

.field public static final maestro_uk:I = 0x7f110158

.field public static final maestro_us:I = 0x7f110159

.field public static final mastercard_credit:I = 0x7f11015a

.field public static final mastercard_paypass:I = 0x7f11015b

.field public static final ppse:I = 0x7f1101e2

.field public static final status_bar_notification_info_overflow:I = 0x7f11028f

.field public static final use_device_unlock:I = 0x7f1102c3

.field public static final visa:I = 0x7f1102c6

.field public static final visa_credit:I = 0x7f1102c7

.field public static final visa_debit:I = 0x7f1102c8

.field public static final visa_debit_credit_classic:I = 0x7f1102c9

.field public static final visa_electron:I = 0x7f1102ca

.field public static final visa_interlink:I = 0x7f1102cb

.field public static final visa_plus:I = 0x7f1102cc

.field public static final visa_v_pay:I = 0x7f1102cd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
