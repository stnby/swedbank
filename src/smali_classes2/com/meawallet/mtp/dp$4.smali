.class final synthetic Lcom/meawallet/mtp/dp$4;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/dp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I

.field static final synthetic c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 272
    invoke-static {}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/meawallet/mtp/dp$4;->c:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/dp$4;->c:[I

    sget-object v2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->WALLET_CANCEL_REQUEST:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-virtual {v2}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/meawallet/mtp/dp$4;->c:[I

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->CARD_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/meawallet/mtp/dp$4;->c:[I

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->TERMINAL_ERROR:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AbortReason;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 88
    :catch_2
    invoke-static {}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/meawallet/mtp/dp$4;->b:[I

    :try_start_3
    sget-object v3, Lcom/meawallet/mtp/dp$4;->b:[I

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->AUTHORIZE_ONLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/meawallet/mtp/dp$4;->b:[I

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->DECLINE_BY_CARD:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/meawallet/mtp/dp$4;->b:[I

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->DECLINE_BY_TERMINAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    const/4 v3, 0x4

    :try_start_6
    sget-object v4, Lcom/meawallet/mtp/dp$4;->b:[I

    sget-object v5, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->AUTHENTICATE_OFFLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    invoke-virtual {v5}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    const/4 v4, 0x5

    :try_start_7
    sget-object v5, Lcom/meawallet/mtp/dp$4;->b:[I

    sget-object v6, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->WALLET_ACTION_REQUIRED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;

    invoke-virtual {v6}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result v6

    aput v4, v5, v6
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    .line 106
    :catch_7
    invoke-static {}, Lcom/meawallet/mtp/cq;->values()[Lcom/meawallet/mtp/cq;

    move-result-object v5

    array-length v5, v5

    new-array v5, v5, [I

    sput-object v5, Lcom/meawallet/mtp/dp$4;->a:[I

    :try_start_8
    sget-object v5, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v6, Lcom/meawallet/mtp/cq;->d:Lcom/meawallet/mtp/cq;

    invoke-virtual {v6}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v6

    aput v0, v5, v6
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v5, Lcom/meawallet/mtp/cq;->f:Lcom/meawallet/mtp/cq;

    invoke-virtual {v5}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v5

    aput v1, v0, v5
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->g:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->j:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->h:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    aput v4, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->i:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->c:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->b:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->a:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v0, Lcom/meawallet/mtp/dp$4;->a:[I

    sget-object v1, Lcom/meawallet/mtp/cq;->e:Lcom/meawallet/mtp/cq;

    invoke-virtual {v1}, Lcom/meawallet/mtp/cq;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    return-void
.end method
