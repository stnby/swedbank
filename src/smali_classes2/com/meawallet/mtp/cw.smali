.class final Lcom/meawallet/mtp/cw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/MeaContactlessTransactionData;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/Date;

.field private c:Ljava/lang/Double;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/meawallet/mtp/MeaRichTransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    const-class v0, Lcom/meawallet/mtp/MeaContactlessTransactionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/cw;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;)V
    .locals 3

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getCurrencyCode()[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getCurrencyCode()[B

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/a;->a([B)Ljava/util/Currency;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cw;->d:Ljava/lang/String;

    .line 59
    :cond_0
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getAuthorizedAmount()[B

    move-result-object v0

    if-eqz v0, :cond_1

    .line 60
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getAuthorizedAmount()[B

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/a;->c([B)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cw;->c:Ljava/lang/Double;

    .line 63
    :cond_1
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getRichTransactionType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/cw;->a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;)Lcom/meawallet/mtp/MeaRichTransactionType;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cw;->h:Lcom/meawallet/mtp/MeaRichTransactionType;

    .line 65
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionId()[B

    move-result-object v0

    if-eqz v0, :cond_3

    .line 66
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionId()[B

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cw;->e:Ljava/lang/String;

    if-eqz p1, :cond_3

    .line 70
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getTransactionLog()Ljava/util/Iterator;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 74
    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;

    .line 77
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getTransactionId()[B

    move-result-object v1

    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTransactionId()[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getDate()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/meawallet/mtp/al;->c(J)Ljava/util/Date;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/cw;->b:Ljava/util/Date;

    .line 88
    :cond_3
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/ContactlessLog;->getTerminalInformation()Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 91
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;->getMerchantAndLocation()[B

    move-result-object p2

    if-eqz p2, :cond_4

    .line 92
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;->getMerchantAndLocation()[B

    move-result-object p2

    invoke-static {p2}, Lcom/meawallet/mtp/h;->b([B)Z

    move-result p2

    if-nez p2, :cond_4

    .line 95
    :try_start_0
    new-instance p2, Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;->getMerchantAndLocation()[B

    move-result-object p1

    const-string v0, "UTF-8"

    invoke-direct {p2, p1, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object p2, p0, Lcom/meawallet/mtp/cw;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const/4 p2, 0x1

    .line 98
    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p2, v0

    .line 102
    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/meawallet/mtp/cw;->c:Ljava/lang/Double;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/meawallet/mtp/cw;->d:Ljava/lang/String;

    if-eqz p1, :cond_5

    .line 103
    iget-object p1, p0, Lcom/meawallet/mtp/cw;->c:Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p1

    invoke-virtual {p0}, Lcom/meawallet/mtp/cw;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/meawallet/mtp/a;->a(DLjava/util/Currency;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/cw;->g:Ljava/lang/String;

    :cond_5
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/meawallet/mtp/cw;->e:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;)Lcom/meawallet/mtp/MeaRichTransactionType;
    .locals 6

    if-eqz p0, :cond_1

    .line 110
    invoke-static {}, Lcom/meawallet/mtp/MeaRichTransactionType;->values()[Lcom/meawallet/mtp/MeaRichTransactionType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 112
    invoke-virtual {v3}, Lcom/meawallet/mtp/MeaRichTransactionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/RichTransactionType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    :cond_1
    sget-object p0, Lcom/meawallet/mtp/MeaRichTransactionType;->UNKNOWN:Lcom/meawallet/mtp/MeaRichTransactionType;

    return-object p0
.end method


# virtual methods
.method public final getAmount()Ljava/lang/Double;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->c:Ljava/lang/Double;

    return-object v0
.end method

.method public final getCurrency()Ljava/util/Currency;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getDate()Ljava/util/Date;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->b:Ljava/util/Date;

    return-object v0
.end method

.method public final getDisplayableAmountAndCurrency()Ljava/lang/String;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final getMerchantAndLocation()Ljava/lang/String;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final getTransactionIdHexString()Ljava/lang/String;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final getTransactionType()Lcom/meawallet/mtp/MeaRichTransactionType;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/meawallet/mtp/cw;->h:Lcom/meawallet/mtp/MeaRichTransactionType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/cw;->h:Lcom/meawallet/mtp/MeaRichTransactionType;

    return-object v0

    :cond_0
    sget-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->UNKNOWN:Lcom/meawallet/mtp/MeaRichTransactionType;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
