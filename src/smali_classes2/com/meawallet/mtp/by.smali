.class Lcom/meawallet/mtp/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/bw;


# static fields
.field private static final a:Ljava/lang/String; = "by"


# instance fields
.field private b:Lcom/meawallet/mtp/bq;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/meawallet/mtp/ai;Lcom/google/gson/Gson;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/az;->a(Landroid/content/Context;)V

    .line 40
    invoke-static {p2, p3}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/ai;Lcom/google/gson/Gson;)Lcom/meawallet/mtp/bq;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    return-void
.end method

.method private a(Lcom/meawallet/mtp/bz;)V
    .locals 1

    .line 908
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bz;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 911
    :cond_0
    new-instance p1, Lcom/meawallet/mtp/cl;

    const-string v0, "LDE update operation failed."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cl;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private p()Lcom/meawallet/mtp/cg;
    .locals 2

    .line 784
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->e:Lcom/meawallet/mtp/bp;

    .line 785
    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/cg;

    if-nez v0, :cond_0

    .line 788
    new-instance v0, Lcom/meawallet/mtp/cg;

    invoke-direct {v0}, Lcom/meawallet/mtp/cg;-><init>()V

    :cond_0
    return-object v0
.end method

.method private q()Lcom/meawallet/mtp/ck;
    .locals 2

    .line 841
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->d:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ck;

    if-nez v0, :cond_0

    .line 844
    new-instance v0, Lcom/meawallet/mtp/ck;

    invoke-direct {v0}, Lcom/meawallet/mtp/ck;-><init>()V

    :cond_0
    return-object v0
.end method

.method private r()V
    .locals 2

    .line 893
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->d:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->b(Lcom/meawallet/mtp/bp;)Z

    return-void
.end method

.method private s()V
    .locals 2

    .line 900
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->e:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->b(Lcom/meawallet/mtp/bp;)Z

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    const/4 v0, 0x1

    .line 186
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 188
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ca;

    if-eqz v0, :cond_0

    .line 5103
    iput p1, v0, Lcom/meawallet/mtp/ca;->f:I

    .line 197
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void

    .line 192
    :cond_0
    new-instance p1, Lcom/meawallet/mtp/bv;

    const-string v0, "LDE container is not initialized."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bv;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 145
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 150
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/meawallet/mtp/h;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 152
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ca;

    if-eqz v0, :cond_0

    .line 5095
    iput-object p1, v0, Lcom/meawallet/mtp/ca;->e:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 161
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void

    .line 156
    :cond_0
    new-instance p1, Lcom/meawallet/mtp/bv;

    const-string v0, "LDE container is not initialized."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bv;-><init>(Ljava/lang/String;)V

    throw p1

    .line 147
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid sessionId."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/meawallet/mtp/LdePinState;)V
    .locals 2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 223
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 225
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "WALLET_PIN_STATE"

    .line 6037
    iget p1, p1, Lcom/meawallet/mtp/LdePinState;->a:I

    .line 225
    invoke-virtual {v0, v1, p1}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 228
    :cond_0
    new-instance p1, Lcom/meawallet/mtp/cl;

    const-string v0, "Unable to update the database."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cl;-><init>(Ljava/lang/String;)V

    throw p1

    .line 220
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid pin state."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/meawallet/mtp/bt;)V
    .locals 9

    if-eqz p1, :cond_7

    .line 7114
    iget-object v0, p1, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    if-eqz v0, :cond_6

    .line 350
    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 354
    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 8088
    iget-object v2, p1, Lcom/meawallet/mtp/bt;->d:Ljava/lang/String;

    .line 358
    iget-object v3, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v4, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    invoke-virtual {v3, v4}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v3

    check-cast v3, Lcom/meawallet/mtp/bu;

    if-nez v3, :cond_0

    .line 361
    new-instance v3, Lcom/meawallet/mtp/bu;

    invoke-direct {v3}, Lcom/meawallet/mtp/bu;-><init>()V

    .line 9019
    :cond_0
    iget-object v4, v3, Lcom/meawallet/mtp/bu;->a:Ljava/util/List;

    .line 365
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/meawallet/mtp/bt;

    .line 9114
    iget-object v6, v5, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v6, :cond_3

    .line 10114
    iget-object v6, v5, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 367
    invoke-virtual {v6}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_1

    .line 368
    :cond_2
    new-instance p1, Lcom/meawallet/mtp/av;

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v0, v2, v7

    const-string v0, "Card with eligibility receipt: %s already exists."

    .line 369
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/meawallet/mtp/av;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-eqz v2, :cond_1

    .line 11088
    iget-object v5, v5, Lcom/meawallet/mtp/bt;->d:Ljava/lang/String;

    .line 372
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    goto :goto_0

    .line 373
    :cond_4
    new-instance p1, Lcom/meawallet/mtp/av;

    new-array v0, v8, [Ljava/lang/Object;

    aput-object v2, v0, v7

    const-string v2, "Card with token unique reference: %s already exists."

    .line 374
    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/meawallet/mtp/av;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1

    .line 378
    :cond_5
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 12050
    iput-object v0, p1, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    .line 379
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->DIGITIZATION_STARTED:Lcom/meawallet/mtp/MeaCardState;

    .line 12058
    iput-object v0, p1, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 380
    sget-object v0, Lcom/meawallet/mtp/LdePinState;->PIN_NOT_SET:Lcom/meawallet/mtp/LdePinState;

    .line 12067
    iput-object v0, p1, Lcom/meawallet/mtp/bt;->c:Lcom/meawallet/mtp/LdePinState;

    .line 382
    invoke-virtual {v3, p1}, Lcom/meawallet/mtp/bu;->a(Lcom/meawallet/mtp/bt;)V

    .line 384
    invoke-direct {p0, v3}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void

    .line 351
    :cond_6
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid eligibility receipt."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1

    .line 345
    :cond_7
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid card."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/meawallet/mtp/cb;)V
    .locals 2

    if-eqz p1, :cond_2

    .line 1044
    iget-object v0, p1, Lcom/meawallet/mtp/cb;->b:Ljava/lang/String;

    .line 2039
    iget-object v1, p1, Lcom/meawallet/mtp/cb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1033
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 1034
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 59
    invoke-virtual {p0}, Lcom/meawallet/mtp/by;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    new-instance v0, Lcom/meawallet/mtp/ca;

    invoke-direct {v0}, Lcom/meawallet/mtp/ca;-><init>()V

    .line 3039
    iget-object v1, p1, Lcom/meawallet/mtp/cb;->a:Ljava/lang/String;

    .line 3062
    iput-object v1, v0, Lcom/meawallet/mtp/ca;->a:Ljava/lang/String;

    .line 4044
    iget-object p1, p1, Lcom/meawallet/mtp/cb;->b:Ljava/lang/String;

    .line 4079
    iput-object p1, v0, Lcom/meawallet/mtp/ca;->c:Ljava/lang/String;

    const/4 p1, 0x2

    .line 4087
    iput p1, v0, Lcom/meawallet/mtp/ca;->d:I

    .line 68
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void

    .line 60
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bs;

    const-string v0, "LDE is already initialized."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bs;-><init>(Ljava/lang/String;)V

    throw p1

    .line 56
    :cond_2
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid init params."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/meawallet/mtp/cc;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 315
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void

    .line 312
    :cond_0
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid key container."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Ljava/lang/Integer;)V
    .locals 2

    const/4 v0, 0x1

    .line 824
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 826
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->q()Lcom/meawallet/mtp/ck;

    move-result-object v0

    .line 21042
    iput-object p1, v0, Lcom/meawallet/mtp/ck;->b:Ljava/lang/Integer;

    .line 830
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    .line 661
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    if-eqz p1, :cond_3

    .line 663
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 668
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/bu;

    if-eqz v0, :cond_2

    .line 672
    new-instance v1, Lcom/meawallet/mtp/bu;

    invoke-direct {v1}, Lcom/meawallet/mtp/bu;-><init>()V

    .line 19019
    iget-object v0, v0, Lcom/meawallet/mtp/bu;->a:Ljava/util/List;

    .line 674
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/bt;

    .line 19046
    iget-object v3, v2, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    .line 676
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 677
    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/bu;->a(Lcom/meawallet/mtp/bt;)V

    goto :goto_0

    .line 681
    :cond_1
    invoke-direct {p0, v1}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    :cond_2
    return-void

    .line 665
    :cond_3
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid Card UUID."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Ljava/lang/String;Lcom/meawallet/mtp/LdePinState;)V
    .locals 4

    if-eqz p1, :cond_5

    .line 693
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz p2, :cond_4

    const/4 v0, 0x2

    .line 703
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v2, 0x1

    aput-object p2, v0, v2

    .line 707
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v2, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v2}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/bu;

    if-eqz v0, :cond_2

    .line 20019
    iget-object v1, v0, Lcom/meawallet/mtp/bu;->a:Ljava/util/List;

    .line 711
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/bt;

    .line 20046
    iget-object v3, v2, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    .line 713
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 20067
    iput-object p2, v2, Lcom/meawallet/mtp/bt;->c:Lcom/meawallet/mtp/LdePinState;

    .line 720
    :cond_1
    iget-object p1, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    invoke-virtual {p1, v0}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bz;)Z

    move-result v1

    :cond_2
    if-eqz v1, :cond_3

    return-void

    .line 725
    :cond_3
    new-instance p1, Lcom/meawallet/mtp/cl;

    const-string p2, "Unable to update the database."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/cl;-><init>(Ljava/lang/String;)V

    throw p1

    .line 700
    :cond_4
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string p2, "Invalid PIN state."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1

    .line 695
    :cond_5
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string p2, "Invalid Card UUID."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardState;)V
    .locals 6

    const/4 v0, 0x2

    .line 575
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v2, 0x1

    aput-object p2, v0, v2

    if-eqz p1, :cond_5

    .line 577
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz p2, :cond_4

    .line 588
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v2, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v2}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/bu;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 18019
    iget-object v3, v0, Lcom/meawallet/mtp/bu;->a:Ljava/util/List;

    .line 593
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/meawallet/mtp/bt;

    .line 18046
    iget-object v5, v4, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    .line 595
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 18058
    iput-object p2, v4, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    move-object v2, v4

    :cond_1
    if-eqz v2, :cond_2

    .line 604
    iget-object p1, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    invoke-virtual {p1, v0}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bz;)Z

    move-result v1

    :cond_2
    if-eqz v1, :cond_3

    return-void

    .line 610
    :cond_3
    new-instance p1, Lcom/meawallet/mtp/cl;

    const-string p2, "Unable to update the database."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/cl;-><init>(Ljava/lang/String;)V

    throw p1

    .line 584
    :cond_4
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string p2, "Invalid card state."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1

    .line 579
    :cond_5
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string p2, "Invalid card UUID."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x4

    .line 764
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    .line 767
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->p()Lcom/meawallet/mtp/cg;

    move-result-object v0

    .line 21019
    iget-object v1, v0, Lcom/meawallet/mtp/cg;->a:Ljava/util/List;

    .line 770
    new-instance v2, Lcom/meawallet/mtp/cf;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/meawallet/mtp/cf;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 773
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;)V"
        }
    .end annotation

    .line 809
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->q()Lcom/meawallet/mtp/ck;

    move-result-object v0

    if-eqz p1, :cond_1

    .line 811
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 21029
    :cond_0
    iput-object p1, v0, Lcom/meawallet/mtp/ck;->a:Ljava/util/List;

    goto :goto_1

    .line 813
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/meawallet/mtp/ck;->b()V

    .line 819
    :goto_1
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void
.end method

.method public final a()Z
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->c(Lcom/meawallet/mtp/bp;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/meawallet/mtp/bt;)V
    .locals 5

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    .line 398
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 13046
    iget-object v2, p1, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 14046
    iget-object v0, p1, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 402
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 14114
    iget-object v1, p1, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 406
    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 408
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 412
    iget-object v1, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v2, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/bu;

    if-nez v1, :cond_0

    .line 415
    new-instance v1, Lcom/meawallet/mtp/bu;

    invoke-direct {v1}, Lcom/meawallet/mtp/bu;-><init>()V

    .line 15019
    :cond_0
    iget-object v2, v1, Lcom/meawallet/mtp/bu;->a:Ljava/util/List;

    .line 418
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/meawallet/mtp/bt;

    .line 15046
    iget-object v4, v3, Lcom/meawallet/mtp/bt;->a:Ljava/lang/String;

    .line 420
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 15088
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->d:Ljava/lang/String;

    .line 15092
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->d:Ljava/lang/String;

    .line 15122
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    .line 15126
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    .line 15130
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->k:Ljava/lang/String;

    .line 15134
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->k:Ljava/lang/String;

    .line 16071
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->e:Lcom/meawallet/mtp/MeaDigitizationDecision;

    .line 16075
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->e:Lcom/meawallet/mtp/MeaDigitizationDecision;

    .line 16080
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->f:[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    .line 16084
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->f:[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    .line 16097
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->g:Lcom/meawallet/mtp/MeaProductConfig;

    .line 16101
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->g:Lcom/meawallet/mtp/MeaProductConfig;

    .line 16106
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->h:Lcom/meawallet/mtp/MeaTokenInfo;

    .line 16110
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->h:Lcom/meawallet/mtp/MeaTokenInfo;

    .line 17054
    iget-object v4, p1, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 17058
    iput-object v4, v3, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    goto :goto_0

    .line 433
    :cond_2
    invoke-direct {p0, v1}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void

    .line 409
    :cond_3
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid eligibility receipt."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1

    .line 403
    :cond_4
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid card UUID."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1

    .line 395
    :cond_5
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "updateLdeCard(): Invalid card."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 279
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 284
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 286
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ca;

    if-eqz v0, :cond_0

    .line 7070
    iput-object p1, v0, Lcom/meawallet/mtp/ca;->b:Ljava/lang/String;

    .line 295
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/by;->a(Lcom/meawallet/mtp/bz;)V

    return-void

    .line 290
    :cond_0
    new-instance p1, Lcom/meawallet/mtp/bv;

    const-string v0, "LDE container is not initialized."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bv;-><init>(Ljava/lang/String;)V

    throw p1

    .line 281
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string v0, "Invalid URL."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b()Z
    .locals 2

    .line 86
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "IS_REGISTERED"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 3

    .line 96
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "IS_REGISTERED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 99
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/cl;

    const-string v1, "Unable to update the database."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cl;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Lcom/meawallet/mtp/cc;
    .locals 2

    .line 326
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->c:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/cc;

    return-object v0
.end method

.method public final e()Lcom/meawallet/mtp/bu;
    .locals 2

    .line 651
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/bu;

    return-object v0
.end method

.method public final f()V
    .locals 2

    .line 21886
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->b(Lcom/meawallet/mtp/bp;)Z

    .line 22872
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->c:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->b(Lcom/meawallet/mtp/bp;)Z

    .line 22879
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->b(Lcom/meawallet/mtp/bp;)Z

    .line 864
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->r()V

    .line 865
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->s()V

    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ca;

    if-eqz v0, :cond_0

    .line 6075
    iget-object v0, v0, Lcom/meawallet/mtp/ca;->c:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ca;

    if-eqz v0, :cond_0

    .line 5058
    iget-object v0, v0, Lcom/meawallet/mtp/ca;->a:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ca;

    if-eqz v0, :cond_0

    .line 5091
    iget-object v0, v0, Lcom/meawallet/mtp/ca;->e:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    return-object v0

    .line 134
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bv;

    const-string v1, "LDE container is not initialized."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bv;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final j()Ljava/lang/Integer;
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/meawallet/mtp/by;->b:Lcom/meawallet/mtp/bq;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/bq;->a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/ca;

    if-eqz v0, :cond_0

    .line 5099
    iget v0, v0, Lcom/meawallet/mtp/ca;->f:I

    .line 178
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 175
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bv;

    const-string v1, "LDE container is not initialized."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bv;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final k()Lcom/meawallet/mtp/LdePinState;
    .locals 2

    .line 209
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "WALLET_PIN_STATE"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->c(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/meawallet/mtp/LdePinState;->a(I)Lcom/meawallet/mtp/LdePinState;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/meawallet/mtp/ck;
    .locals 1

    .line 802
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->q()Lcom/meawallet/mtp/ck;

    move-result-object v0

    return-object v0
.end method

.method public final m()V
    .locals 0

    .line 837
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->r()V

    return-void
.end method

.method public final n()Lcom/meawallet/mtp/cg;
    .locals 1

    .line 758
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->p()Lcom/meawallet/mtp/cg;

    move-result-object v0

    return-object v0
.end method

.method public final o()V
    .locals 0

    .line 780
    invoke-direct {p0}, Lcom/meawallet/mtp/by;->s()V

    return-void
.end method
