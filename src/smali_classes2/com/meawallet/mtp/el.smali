.class abstract Lcom/meawallet/mtp/el;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Ljava/lang/Object;",
        "R:",
        "Lcom/meawallet/mtp/eh;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "el"


# instance fields
.field final a:Lcom/meawallet/mtp/ef;

.field b:Lcom/meawallet/mtp/bf;

.field final c:Ljavax/net/ssl/X509TrustManager;

.field final d:Lcom/meawallet/mtp/eg;

.field protected final e:Lcom/google/gson/Gson;

.field final f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/eg;Ljavax/net/ssl/X509TrustManager;Lcom/meawallet/mtp/ef;Lcom/meawallet/mtp/bf;Lcom/google/gson/Gson;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/eg;",
            "Ljavax/net/ssl/X509TrustManager;",
            "Lcom/meawallet/mtp/ef;",
            "Lcom/meawallet/mtp/bf;",
            "Lcom/google/gson/Gson;",
            "T",
            "L;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/meawallet/mtp/el;->d:Lcom/meawallet/mtp/eg;

    .line 33
    iput-object p2, p0, Lcom/meawallet/mtp/el;->c:Ljavax/net/ssl/X509TrustManager;

    .line 34
    iput-object p3, p0, Lcom/meawallet/mtp/el;->a:Lcom/meawallet/mtp/ef;

    .line 35
    iput-object p4, p0, Lcom/meawallet/mtp/el;->b:Lcom/meawallet/mtp/bf;

    .line 36
    iput-object p5, p0, Lcom/meawallet/mtp/el;->e:Lcom/google/gson/Gson;

    .line 37
    iput-object p6, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/MeaError;)Lcom/meawallet/mtp/MeaError;
    .locals 6

    if-eqz p0, :cond_0

    .line 1103
    new-instance v0, Lcom/meawallet/mtp/cy;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaError;->getRemoteResponseId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 1105
    sget-object v1, Lcom/meawallet/mtp/el;->g:Ljava/lang/String;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p0

    const-string v2, "Remote request was not successful code: %s, message: %s, remoteResponseId: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1107
    invoke-virtual {v0}, Lcom/meawallet/mtp/cy;->getCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/meawallet/mtp/cy;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Lcom/meawallet/mtp/cy;->getRemoteResponseId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1105
    invoke-static {v1, p0, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .line 15
    sget-object v0, Lcom/meawallet/mtp/el;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "TR;>;"
        }
    .end annotation
.end method

.method final a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const/4 v0, 0x2

    .line 90
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 91
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 94
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/el;->e:Lcom/google/gson/Gson;

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 96
    new-instance p2, Lcom/meawallet/mtp/bl;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to parse response. "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/gson/JsonSyntaxException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method final a()V
    .locals 2

    .line 45
    new-instance v0, Lcom/meawallet/mtp/c;

    invoke-direct {v0}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/el$1;

    invoke-direct {v1, p0, p0}, Lcom/meawallet/mtp/el$1;-><init>(Lcom/meawallet/mtp/el;Lcom/meawallet/mtp/el;)V

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fn;)V

    return-void
.end method

.method abstract a(Lcom/meawallet/mtp/d;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/d<",
            "TR;>;)V"
        }
    .end annotation
.end method

.method abstract b()Lcom/meawallet/mtp/MeaHttpMethod;
.end method

.method abstract c()Ljava/lang/String;
.end method

.method abstract d()Lcom/meawallet/mtp/ep;
.end method

.method e()V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/meawallet/mtp/el;->d:Lcom/meawallet/mtp/eg;

    if-eqz v0, :cond_3

    .line 177
    iget-object v0, p0, Lcom/meawallet/mtp/el;->d:Lcom/meawallet/mtp/eg;

    invoke-virtual {v0}, Lcom/meawallet/mtp/eg;->b()V

    .line 179
    iget-object v0, p0, Lcom/meawallet/mtp/el;->c:Ljavax/net/ssl/X509TrustManager;

    if-eqz v0, :cond_2

    .line 181
    iget-object v0, p0, Lcom/meawallet/mtp/el;->a:Lcom/meawallet/mtp/ef;

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/meawallet/mtp/el;->b:Lcom/meawallet/mtp/bf;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Response code wrapper is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Remote request authorization is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Public key manager is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Remote request data is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0
.end method
