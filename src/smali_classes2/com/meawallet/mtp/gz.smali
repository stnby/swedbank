.class Lcom/meawallet/mtp/gz;
.super Lcom/meawallet/mtp/eg;
.source "SourceFile"


# static fields
.field private static final transient b:Ljava/lang/String; = "gz"


# instance fields
.field transient a:Lcom/meawallet/mtp/gx;

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "requestId"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sdkVersion"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentAppId"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentAppInstanceId"
    .end annotation
.end field

.field private g:Lcom/meawallet/mtp/PaymentNetwork;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentNetwork"
    .end annotation
.end field

.field private h:Lcom/google/gson/JsonElement;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/meawallet/mtp/gx;)V
    .locals 1

    .line 45
    invoke-direct {p0}, Lcom/meawallet/mtp/eg;-><init>()V

    .line 46
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/gz;->c:Ljava/lang/String;

    const-string v0, "2.0.8"

    .line 47
    iput-object v0, p0, Lcom/meawallet/mtp/gz;->d:Ljava/lang/String;

    const-string v0, "SWEDBANKLIETUVA"

    .line 48
    iput-object v0, p0, Lcom/meawallet/mtp/gz;->e:Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/meawallet/mtp/gz;->f:Ljava/lang/String;

    .line 1070
    iget-object p1, p2, Lcom/meawallet/mtp/gx;->b:Lcom/meawallet/mtp/PaymentNetwork;

    .line 50
    iput-object p1, p0, Lcom/meawallet/mtp/gz;->g:Lcom/meawallet/mtp/PaymentNetwork;

    .line 52
    iput-object p2, p0, Lcom/meawallet/mtp/gz;->a:Lcom/meawallet/mtp/gx;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 12

    .line 59
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->a:Lcom/meawallet/mtp/gx;

    invoke-virtual {v0}, Lcom/meawallet/mtp/gx;->a()V

    .line 61
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->a:Lcom/meawallet/mtp/gx;

    .line 2035
    invoke-static {}, Lcom/meawallet/mtp/JsonUtil;->a()Lcom/google/gson/Gson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/gson/Gson;->toJsonTree(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v1

    .line 2037
    iget-boolean v2, v0, Lcom/meawallet/mtp/gx;->d:Z

    if-eqz v2, :cond_7

    .line 2038
    iget-object v2, v0, Lcom/meawallet/mtp/gx;->c:Lcom/meawallet/mtp/ec;

    if-eqz v2, :cond_6

    .line 2043
    iget-object v0, v0, Lcom/meawallet/mtp/gx;->c:Lcom/meawallet/mtp/ec;

    const/4 v2, 0x1

    .line 3037
    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/meawallet/mtp/JsonUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 3039
    iget-object v3, v0, Lcom/meawallet/mtp/ec;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v3}, Lcom/meawallet/mtp/ci;->h()Lcom/meawallet/mtp/cc;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 4043
    iget-object v3, v3, Lcom/meawallet/mtp/cc;->b:Lcom/meawallet/mtp/cn;

    .line 3050
    invoke-virtual {v1}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5023
    iget-object v4, v3, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 6023
    iget-object v8, v4, Lcom/meawallet/mtp/cm;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 7023
    iget-object v4, v3, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 7027
    iget-object v9, v4, Lcom/meawallet/mtp/cm;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 3052
    iget-object v4, v0, Lcom/meawallet/mtp/ec;->b:Lcom/meawallet/mtp/ci;

    .line 3053
    invoke-interface {v4}, Lcom/meawallet/mtp/ci;->e()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v10

    .line 7134
    iget-object v4, v0, Lcom/meawallet/mtp/ec;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v4}, Lcom/meawallet/mtp/ci;->f()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 7136
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_0

    goto :goto_0

    .line 7139
    :cond_0
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 7141
    iget-object v4, v0, Lcom/meawallet/mtp/ec;->b:Lcom/meawallet/mtp/ci;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v4, v6}, Lcom/meawallet/mtp/ci;->a(I)V

    goto :goto_1

    .line 7137
    :cond_1
    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 7144
    :goto_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-eqz v1, :cond_3

    .line 7162
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_2

    goto :goto_2

    .line 7167
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    .line 7170
    invoke-virtual {v0}, Lcom/meawallet/mtp/ec;->a()Lcom/meawallet/mtp/ai;

    move-result-object v6

    move-object v7, v1

    invoke-interface/range {v6 .. v11}, Lcom/meawallet/mtp/ai;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;I)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    .line 7172
    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    .line 7174
    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toBase64String()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_4

    .line 3062
    new-instance v1, Lcom/google/gson/JsonPrimitive;

    invoke-direct {v1, v0}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    .line 8023
    iget-object v0, v3, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 3064
    invoke-virtual {v0}, Lcom/meawallet/mtp/cm;->a()V

    goto :goto_4

    .line 3057
    :cond_4
    sget-object v0, Lcom/meawallet/mtp/ec;->a:Ljava/lang/String;

    const/16 v1, 0x12d

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Failed to encrypt request JSON data, encrypted data is null."

    invoke-static {v0, v1, v3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 3059
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Encrypted data is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3042
    :cond_5
    sget-object v0, Lcom/meawallet/mtp/ec;->a:Ljava/lang/String;

    const/16 v1, 0x12e

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Failed to encrypt request JSON data, LDE key container is null."

    invoke-static {v0, v1, v3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 3044
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "LDE key container is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2040
    :cond_6
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Remote data cryptor is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_7
    :goto_4
    iput-object v1, p0, Lcom/meawallet/mtp/gz;->h:Lcom/google/gson/JsonElement;

    return-void
.end method

.method final b()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 68
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 70
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->a:Lcom/meawallet/mtp/gx;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/meawallet/mtp/gz;->a:Lcom/meawallet/mtp/gx;

    invoke-virtual {v0}, Lcom/meawallet/mtp/gx;->b()V

    return-void

    .line 74
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Wsp data data is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Payment app instance id is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Payment app id is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Sdk version is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Request is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
