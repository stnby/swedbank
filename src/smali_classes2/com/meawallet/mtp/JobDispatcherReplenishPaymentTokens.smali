.class public Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;
.super Lcom/firebase/jobdispatcher/r;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "JobDispatcherReplenishPaymentTokens"


# instance fields
.field private b:Lcom/meawallet/mtp/fo;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Lcom/firebase/jobdispatcher/r;-><init>()V

    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;->b:Lcom/meawallet/mtp/fo;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .line 12
    sget-object v0, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onStartJob(Lcom/firebase/jobdispatcher/q;)Z
    .locals 2

    .line 21
    new-instance v0, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens$1;

    invoke-virtual {p0}, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens$1;-><init>(Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;Landroid/app/Application;Lcom/firebase/jobdispatcher/q;)V

    iput-object v0, p0, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;->b:Lcom/meawallet/mtp/fo;

    .line 31
    iget-object p1, p0, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;->b:Lcom/meawallet/mtp/fo;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/meawallet/mtp/fo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 p1, 0x1

    return p1
.end method

.method public onStopJob(Lcom/firebase/jobdispatcher/q;)Z
    .locals 1

    .line 40
    iget-object p1, p0, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;->b:Lcom/meawallet/mtp/fo;

    if-eqz p1, :cond_0

    .line 41
    iget-object p1, p0, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;->b:Lcom/meawallet/mtp/fo;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/meawallet/mtp/fo;->cancel(Z)Z

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
