.class final Lcom/meawallet/mtp/gq;
.super Lcom/meawallet/mtp/gx;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cardId"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cardSecret"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "bin"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 22
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/gx;-><init>(Z)V

    .line 24
    iput-object p1, p0, Lcom/meawallet/mtp/gq;->a:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/meawallet/mtp/gq;->e:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/meawallet/mtp/gq;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final b()V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/meawallet/mtp/gq;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 33
    iget-object v0, p0, Lcom/meawallet/mtp/gq;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x40

    if-gt v0, v1, :cond_4

    .line 35
    iget-object v0, p0, Lcom/meawallet/mtp/gq;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 37
    iget-object v0, p0, Lcom/meawallet/mtp/gq;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v1, :cond_2

    .line 39
    iget-object v0, p0, Lcom/meawallet/mtp/gq;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/meawallet/mtp/gq;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/gq;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Card bin range is wrong."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void

    .line 37
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Card secret is too long."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Card secret is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Card Id is too long."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_5
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Card id is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
