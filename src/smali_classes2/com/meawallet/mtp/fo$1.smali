.class final Lcom/meawallet/mtp/fo$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/en;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/fo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/fo;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/fo;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/meawallet/mtp/fo$1;->a:Lcom/meawallet/mtp/fo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/meawallet/mtp/MeaError;)V
    .locals 5

    .line 105
    invoke-static {}, Lcom/meawallet/mtp/fo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    const-string v2, "onReplenishFailure(error = %s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .line 87
    invoke-static {}, Lcom/meawallet/mtp/fo;->a()Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 88
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    if-lez p2, :cond_0

    .line 93
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->b()Lcom/meawallet/mtp/l;

    move-result-object p2

    .line 95
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->a()Lcom/meawallet/mtp/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lcom/meawallet/mtp/l;->a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 98
    invoke-static {}, Lcom/meawallet/mtp/fo;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method
