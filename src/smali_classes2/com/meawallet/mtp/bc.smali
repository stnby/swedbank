.class final Lcom/meawallet/mtp/bc;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/g<",
        "Lcom/meawallet/mtp/be;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    .line 34
    invoke-direct/range {v0 .. v8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object/from16 v1, p8

    .line 43
    iput-object v1, v0, Lcom/meawallet/mtp/bc;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final bridge synthetic a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    .line 7052
    invoke-static {p1, p2}, Lcom/meawallet/mtp/be;->a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/be;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 3

    .line 74
    :try_start_0
    sget-object v0, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    .line 2317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 76
    new-instance v0, Lcom/meawallet/mtp/bd;

    .line 3085
    iget-object v1, p0, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    .line 76
    iget-object v2, p0, Lcom/meawallet/mtp/bc;->j:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/bd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4020
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/bc;->a(Ljava/lang/String;)Lcom/meawallet/mtp/y;

    move-result-object v0

    .line 4034
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/bc;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 101
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 103
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 101
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bc;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 95
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 96
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bc;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 89
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute set  Get Task Status command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bc;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 82
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute Get Task Status HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bc;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 62
    iput-object p2, p0, Lcom/meawallet/mtp/bc;->c:Ljava/lang/String;

    .line 63
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 64
    iput-object p1, p0, Lcom/meawallet/mtp/bc;->h:Lcom/meawallet/mtp/ae;

    .line 66
    iget-object p1, p0, Lcom/meawallet/mtp/bc;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-ne p1, v0, :cond_0

    .line 67
    iget-object p1, p0, Lcom/meawallet/mtp/bc;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p1, p2, p3, p4}, Lcom/meawallet/mtp/ad;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final bridge synthetic a(Lcom/meawallet/mtp/s;)V
    .locals 1

    .line 20
    check-cast p1, Lcom/meawallet/mtp/be;

    .line 6056
    sget-object v0, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 6317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 6057
    sget-object v0, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    iput-object v0, p0, Lcom/meawallet/mtp/bc;->i:Lcom/meawallet/mtp/ah;

    .line 6058
    iget-object v0, p0, Lcom/meawallet/mtp/bc;->d:Lcom/meawallet/mtp/ad;

    .line 7017
    iget-object p1, p1, Lcom/meawallet/mtp/be;->a:Ljava/lang/String;

    .line 6058
    invoke-interface {v0, p1}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 0

    .line 144
    instance-of p1, p1, Lcom/meawallet/mtp/bc;

    return p1
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/getTaskStatus"

    return-object v0
.end method

.method public final l()V
    .locals 4

    .line 4230
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 109
    sget-object v1, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/meawallet/mtp/bc;->d:Lcom/meawallet/mtp/ad;

    const-string v1, "CANCELED"

    const-string v3, "Too many commands already in queue."

    invoke-interface {v0, v1, v3, v2}, Lcom/meawallet/mtp/ad;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 5230
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 116
    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_1

    .line 117
    iget-object v0, p0, Lcom/meawallet/mtp/bc;->d:Lcom/meawallet/mtp/ad;

    const-string v1, "CANCELED"

    const-string v3, "Duplicate Request"

    invoke-interface {v0, v1, v3, v2}, Lcom/meawallet/mtp/ad;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 123
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 5317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 124
    iget-object v0, p0, Lcom/meawallet/mtp/bc;->d:Lcom/meawallet/mtp/ad;

    const-string v1, "CANCELED"

    const-string v3, "Get Task Status command cancelled or Could not get a valid session from CMS-D"

    invoke-interface {v0, v1, v3, v2}, Lcom/meawallet/mtp/ad;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 130
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/bc;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 135
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
