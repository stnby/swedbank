.class Lcom/meawallet/mtp/gr;
.super Lcom/meawallet/mtp/fv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/fv<",
        "Lcom/meawallet/mtp/MeaListener;",
        "Lcom/meawallet/mtp/gt;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "gr"


# instance fields
.field private final h:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

.field private final i:Lcom/meawallet/mtp/dq;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;Lcom/meawallet/mtp/ap;Lcom/meawallet/mtp/CvmMethod;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 15

    move-object v6, p0

    .line 37
    new-instance v4, Lcom/meawallet/mtp/gs;

    move-object v7, v4

    move-object/from16 v8, p2

    move-object/from16 v9, p1

    move-object/from16 v10, p4

    move-object/from16 v11, p7

    move-object/from16 v12, p5

    move-object/from16 v13, p6

    move-object/from16 v14, p8

    invoke-direct/range {v7 .. v14}, Lcom/meawallet/mtp/gs;-><init>(Lcom/meawallet/mtp/dq;Lcom/meawallet/mtp/ci;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;Ljava/lang/String;Lcom/meawallet/mtp/ap;Lcom/meawallet/mtp/CvmMethod;Ljava/lang/String;)V

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/fv;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V

    move-object/from16 v0, p4

    .line 50
    iput-object v0, v6, Lcom/meawallet/mtp/gr;->h:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

    move-object/from16 v0, p2

    .line 51
    iput-object v0, v6, Lcom/meawallet/mtp/gr;->i:Lcom/meawallet/mtp/dq;

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/d;)V
    .locals 1

    .line 124
    new-instance v0, Lcom/meawallet/mtp/ft;

    invoke-direct {v0}, Lcom/meawallet/mtp/ft;-><init>()V

    .line 15147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 124
    check-cast v0, Lcom/meawallet/mtp/MeaCoreListener;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method

.method final b()Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 56
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method

.method final b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/gt;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 67
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 70
    :try_start_0
    const-class v0, Lcom/meawallet/mtp/gt;

    .line 71
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/gr;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/gt;

    if-eqz p1, :cond_1

    .line 77
    invoke-virtual {p1}, Lcom/meawallet/mtp/gt;->validate()V

    .line 79
    invoke-virtual {p0}, Lcom/meawallet/mtp/gr;->h()Lcom/meawallet/mtp/ci;

    move-result-object v0

    .line 1070
    iget-object v1, p1, Lcom/meawallet/mtp/gt;->d:Ljava/lang/String;

    .line 79
    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ci;->e(Ljava/lang/String;)V

    .line 2036
    new-instance v0, Lcom/meawallet/mtp/cc;

    invoke-direct {v0}, Lcom/meawallet/mtp/cc;-><init>()V

    .line 2038
    new-instance v1, Lcom/meawallet/mtp/cd;

    iget-object v2, p1, Lcom/meawallet/mtp/gt;->e:Lcom/meawallet/mtp/gu;

    .line 3026
    iget-object v2, v2, Lcom/meawallet/mtp/gu;->a:Ljava/lang/String;

    .line 2040
    invoke-static {v2}, Lcom/meawallet/mtp/h;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    iget-object v3, p1, Lcom/meawallet/mtp/gt;->e:Lcom/meawallet/mtp/gu;

    .line 3030
    iget-object v3, v3, Lcom/meawallet/mtp/gu;->b:Ljava/lang/String;

    .line 2041
    invoke-static {v3}, Lcom/meawallet/mtp/h;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    iget-object v4, p1, Lcom/meawallet/mtp/gt;->e:Lcom/meawallet/mtp/gu;

    .line 3034
    iget-object v4, v4, Lcom/meawallet/mtp/gu;->c:Ljava/lang/String;

    .line 2042
    invoke-static {v4}, Lcom/meawallet/mtp/h;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/meawallet/mtp/cd;-><init>(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V

    .line 2044
    new-instance v2, Lcom/meawallet/mtp/ce;

    iget-object v3, p1, Lcom/meawallet/mtp/gt;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lcom/meawallet/mtp/ce;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/cd;)V

    .line 3039
    iput-object v2, v0, Lcom/meawallet/mtp/cc;->a:Lcom/meawallet/mtp/ce;

    .line 2046
    new-instance v1, Lcom/meawallet/mtp/cm;

    iget-object v2, p1, Lcom/meawallet/mtp/gt;->f:Lcom/meawallet/mtp/gu;

    .line 4026
    iget-object v2, v2, Lcom/meawallet/mtp/gu;->a:Ljava/lang/String;

    .line 2048
    invoke-static {v2}, Lcom/meawallet/mtp/h;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    iget-object v3, p1, Lcom/meawallet/mtp/gt;->f:Lcom/meawallet/mtp/gu;

    .line 4030
    iget-object v3, v3, Lcom/meawallet/mtp/gu;->b:Ljava/lang/String;

    .line 2049
    invoke-static {v3}, Lcom/meawallet/mtp/h;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    iget-object v4, p1, Lcom/meawallet/mtp/gt;->f:Lcom/meawallet/mtp/gu;

    .line 4034
    iget-object v4, v4, Lcom/meawallet/mtp/gu;->c:Ljava/lang/String;

    .line 2050
    invoke-static {v4}, Lcom/meawallet/mtp/h;->c(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/meawallet/mtp/cm;-><init>(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V

    .line 2052
    new-instance v2, Lcom/meawallet/mtp/cn;

    iget-object v3, p1, Lcom/meawallet/mtp/gt;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lcom/meawallet/mtp/cn;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/cm;)V

    .line 4047
    iput-object v2, v0, Lcom/meawallet/mtp/cc;->b:Lcom/meawallet/mtp/cn;

    .line 5035
    iget-object v1, v0, Lcom/meawallet/mtp/cc;->a:Lcom/meawallet/mtp/ce;

    .line 84
    new-instance v2, Lcom/meawallet/mtp/et;

    .line 6019
    iget-object v3, v1, Lcom/meawallet/mtp/ce;->a:Ljava/lang/String;

    .line 6023
    iget-object v4, v1, Lcom/meawallet/mtp/ce;->b:Lcom/meawallet/mtp/cd;

    .line 6028
    iget-object v4, v4, Lcom/meawallet/mtp/cd;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 7023
    iget-object v5, v1, Lcom/meawallet/mtp/ce;->b:Lcom/meawallet/mtp/cd;

    .line 7024
    iget-object v5, v5, Lcom/meawallet/mtp/cd;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 8023
    iget-object v6, v1, Lcom/meawallet/mtp/ce;->b:Lcom/meawallet/mtp/cd;

    .line 8032
    iget-object v6, v6, Lcom/meawallet/mtp/cd;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 88
    invoke-direct {v2, v3, v4, v5, v6}, Lcom/meawallet/mtp/et;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V

    .line 90
    iget-object v3, p0, Lcom/meawallet/mtp/gr;->h:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

    .line 8070
    iget-object v4, p1, Lcom/meawallet/mtp/gt;->d:Ljava/lang/String;

    .line 90
    invoke-interface {v3, v2, v4}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;->setRegistrationResponseData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedMobileKeys;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/meawallet/mtp/gr;->g()Lcom/meawallet/mtp/gx;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/gs;

    .line 94
    iget-object v3, p0, Lcom/meawallet/mtp/gr;->i:Lcom/meawallet/mtp/dq;

    .line 8132
    iget-object v2, v2, Lcom/meawallet/mtp/gs;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 9043
    iget-object v4, v0, Lcom/meawallet/mtp/cc;->b:Lcom/meawallet/mtp/cn;

    .line 10023
    iget-object v4, v4, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 10052
    iget-object v3, v3, Lcom/meawallet/mtp/dq;->a:Lcom/meawallet/mtp/ai;

    invoke-interface {v3, v2, v4}, Lcom/meawallet/mtp/ai;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/meawallet/mtp/cm;)Lcom/meawallet/mtp/cm;

    move-result-object v2

    .line 11043
    iget-object v3, v0, Lcom/meawallet/mtp/cc;->b:Lcom/meawallet/mtp/cn;

    .line 12027
    iput-object v2, v3, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 12035
    iget-object v3, v0, Lcom/meawallet/mtp/cc;->a:Lcom/meawallet/mtp/ce;

    .line 13019
    iget-object v1, v1, Lcom/meawallet/mtp/ce;->a:Ljava/lang/String;

    .line 13027
    iput-object v1, v3, Lcom/meawallet/mtp/ce;->a:Ljava/lang/String;

    .line 100
    invoke-virtual {p0}, Lcom/meawallet/mtp/gr;->h()Lcom/meawallet/mtp/ci;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/meawallet/mtp/ci;->a(Lcom/meawallet/mtp/cc;)V

    .line 102
    iget-object v0, p0, Lcom/meawallet/mtp/gr;->i:Lcom/meawallet/mtp/dq;

    .line 13062
    iget-object v1, p1, Lcom/meawallet/mtp/gt;->b:Ljava/lang/String;

    .line 103
    invoke-static {v1}, Lcom/meawallet/mtp/h;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    .line 14023
    iget-object v3, v2, Lcom/meawallet/mtp/cm;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 14027
    iget-object v4, v2, Lcom/meawallet/mtp/cm;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 14059
    iget-object v0, v0, Lcom/meawallet/mtp/dq;->a:Lcom/meawallet/mtp/ai;

    invoke-interface {v0, v1, v3, v4}, Lcom/meawallet/mtp/ai;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    .line 106
    invoke-virtual {p0}, Lcom/meawallet/mtp/gr;->h()Lcom/meawallet/mtp/ci;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/meawallet/mtp/ci;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V

    .line 108
    invoke-virtual {v2}, Lcom/meawallet/mtp/cm;->a()V

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/gr;->h()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->c()V

    .line 113
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    .line 15023
    iput-object p1, v0, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    return-object v0

    .line 74
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bl;

    const-string v0, "Response data is null."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bl; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 116
    new-instance v0, Lcom/meawallet/mtp/fs;

    invoke-direct {v0}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p1}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p1

    return-object p1
.end method

.method final d()Lcom/meawallet/mtp/ep;
    .locals 1

    .line 61
    sget-object v0, Lcom/meawallet/mtp/gy;->a:Lcom/meawallet/mtp/ep;

    return-object v0
.end method

.method final e()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/meawallet/mtp/gr;->h:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

    if-eqz v0, :cond_0

    .line 131
    invoke-super {p0}, Lcom/meawallet/mtp/fv;->e()V

    return-void

    .line 129
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "MpaManagementHelper is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0
.end method
