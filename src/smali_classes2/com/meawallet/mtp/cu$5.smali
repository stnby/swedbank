.class final Lcom/meawallet/mtp/cu$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/cu;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/MeaCard;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/meawallet/mtp/MeaCardListener;

.field final synthetic d:Lcom/meawallet/mtp/cu;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/cu;Lcom/meawallet/mtp/MeaCard;Landroid/content/Context;Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 0

    .line 672
    iput-object p1, p0, Lcom/meawallet/mtp/cu$5;->d:Lcom/meawallet/mtp/cu;

    iput-object p2, p0, Lcom/meawallet/mtp/cu$5;->a:Lcom/meawallet/mtp/MeaCard;

    iput-object p3, p0, Lcom/meawallet/mtp/cu$5;->b:Landroid/content/Context;

    iput-object p4, p0, Lcom/meawallet/mtp/cu$5;->c:Lcom/meawallet/mtp/MeaCardListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()Lcom/meawallet/mtp/d;
    .locals 4

    .line 679
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu$5;->d:Lcom/meawallet/mtp/cu;

    invoke-static {v0}, Lcom/meawallet/mtp/cu;->d(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/l;

    move-result-object v0

    iget-object v1, p0, Lcom/meawallet/mtp/cu$5;->a:Lcom/meawallet/mtp/MeaCard;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/meawallet/mtp/cu$5;->d:Lcom/meawallet/mtp/cu;

    invoke-static {v2}, Lcom/meawallet/mtp/cu;->b(Lcom/meawallet/mtp/cu;)Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    iget-object v3, p0, Lcom/meawallet/mtp/cu$5;->d:Lcom/meawallet/mtp/cu;

    invoke-static {v3}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/k;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/meawallet/mtp/l;->a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/k;)V

    .line 681
    iget-object v0, p0, Lcom/meawallet/mtp/cu$5;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result v0

    .line 682
    iget-object v1, p0, Lcom/meawallet/mtp/cu$5;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/meawallet/mtp/cu$5;->d:Lcom/meawallet/mtp/cu;

    invoke-static {v2}, Lcom/meawallet/mtp/cu;->e(Lcom/meawallet/mtp/cu;)Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/meawallet/mtp/bo;->b(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Z)V
    :try_end_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    .line 699
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "markForDeletion() failed."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 701
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    .line 2029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1

    :catch_1
    move-exception v0

    .line 686
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 688
    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    const/16 v1, 0x3f2

    .line 690
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 691
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    .line 693
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    return-object v0

    .line 696
    :cond_0
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    .line 1029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 672
    invoke-direct {p0}, Lcom/meawallet/mtp/cu$5;->b()Lcom/meawallet/mtp/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 2

    .line 672
    check-cast p1, Lcom/meawallet/mtp/d;

    .line 2708
    iget-object v0, p0, Lcom/meawallet/mtp/cu$5;->a:Lcom/meawallet/mtp/MeaCard;

    iget-object v1, p0, Lcom/meawallet/mtp/cu$5;->c:Lcom/meawallet/mtp/MeaCardListener;

    invoke-static {p1, v0, v1}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V

    return-void
.end method
