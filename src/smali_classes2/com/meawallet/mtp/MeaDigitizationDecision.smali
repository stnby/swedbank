.class public final enum Lcom/meawallet/mtp/MeaDigitizationDecision;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaDigitizationDecision;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum APPROVED:Lcom/meawallet/mtp/MeaDigitizationDecision;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum DECLINED:Lcom/meawallet/mtp/MeaDigitizationDecision;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaDigitizationDecision;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaDigitizationDecision;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 9
    new-instance v0, Lcom/meawallet/mtp/MeaDigitizationDecision;

    const-string v1, "APPROVED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaDigitizationDecision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaDigitizationDecision;->APPROVED:Lcom/meawallet/mtp/MeaDigitizationDecision;

    .line 11
    new-instance v0, Lcom/meawallet/mtp/MeaDigitizationDecision;

    const-string v1, "DECLINED"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaDigitizationDecision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaDigitizationDecision;->DECLINED:Lcom/meawallet/mtp/MeaDigitizationDecision;

    .line 13
    new-instance v0, Lcom/meawallet/mtp/MeaDigitizationDecision;

    const-string v1, "REQUIRE_ADDITIONAL_AUTHENTICATION"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaDigitizationDecision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaDigitizationDecision;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaDigitizationDecision;

    const/4 v0, 0x3

    .line 8
    new-array v0, v0, [Lcom/meawallet/mtp/MeaDigitizationDecision;

    sget-object v1, Lcom/meawallet/mtp/MeaDigitizationDecision;->APPROVED:Lcom/meawallet/mtp/MeaDigitizationDecision;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaDigitizationDecision;->DECLINED:Lcom/meawallet/mtp/MeaDigitizationDecision;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaDigitizationDecision;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaDigitizationDecision;

    aput-object v1, v0, v4

    sput-object v0, Lcom/meawallet/mtp/MeaDigitizationDecision;->a:[Lcom/meawallet/mtp/MeaDigitizationDecision;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaDigitizationDecision;
    .locals 1

    .line 8
    const-class v0, Lcom/meawallet/mtp/MeaDigitizationDecision;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaDigitizationDecision;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaDigitizationDecision;
    .locals 1

    .line 8
    sget-object v0, Lcom/meawallet/mtp/MeaDigitizationDecision;->a:[Lcom/meawallet/mtp/MeaDigitizationDecision;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaDigitizationDecision;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaDigitizationDecision;

    return-object v0
.end method
