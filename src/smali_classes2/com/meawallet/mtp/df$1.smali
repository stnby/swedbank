.class final synthetic Lcom/meawallet/mtp/df$1;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/df;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I

.field static final synthetic c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 305
    invoke-static {}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->values()[Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/meawallet/mtp/df$1;->c:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/df$1;->c:[I

    sget-object v2, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    invoke-virtual {v2}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/meawallet/mtp/df$1;->c:[I

    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CARD_LIKE:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/meawallet/mtp/df$1;->c:[I

    sget-object v4, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v3, Lcom/meawallet/mtp/df$1;->c:[I

    sget-object v4, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    invoke-virtual {v4}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 259
    :catch_3
    invoke-static {}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/meawallet/mtp/df$1;->b:[I

    :try_start_4
    sget-object v3, Lcom/meawallet/mtp/df$1;->b:[I

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/meawallet/mtp/df$1;->b:[I

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 159
    :catch_5
    invoke-static {}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->values()[Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/meawallet/mtp/df$1;->a:[I

    :try_start_6
    sget-object v3, Lcom/meawallet/mtp/df$1;->a:[I

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-virtual {v4}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/meawallet/mtp/df$1;->a:[I

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-virtual {v3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/meawallet/mtp/df$1;->a:[I

    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    return-void
.end method
