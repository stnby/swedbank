.class Lcom/meawallet/mtp/bo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "bo"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/content/Context;)Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;
    .locals 2

    .line 31
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 32
    new-instance v0, Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    new-instance v1, Lcom/firebase/jobdispatcher/f;

    invoke-direct {v1, p0}, Lcom/firebase/jobdispatcher/f;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;-><init>(Lcom/firebase/jobdispatcher/c;)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static a(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;)V
    .locals 2

    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    const-string p1, "jobscheduler"

    .line 49
    invoke-virtual {p0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/job/JobScheduler;

    if-eqz p0, :cond_0

    .line 51
    invoke-virtual {p0}, Landroid/app/job/JobScheduler;->cancelAll()V

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    .line 55
    invoke-virtual {p1}, Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;->a()I

    :cond_2
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Z)V
    .locals 4

    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x15

    if-lt v0, v3, :cond_2

    const-string p1, "jobscheduler"

    .line 1063
    invoke-virtual {p0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/job/JobScheduler;

    if-eqz p1, :cond_1

    .line 1067
    new-instance v0, Landroid/app/job/JobInfo$Builder;

    new-instance v1, Landroid/content/ComponentName;

    const-class v3, Lcom/meawallet/mtp/JobSchedulerReplenishPaymentTokens;

    invoke-direct {v1, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v2, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 1069
    invoke-virtual {v0, v2}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object p0

    if-eqz p2, :cond_0

    const-wide/16 v0, 0x3e8

    .line 1072
    invoke-virtual {p0, v0, v1}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    .line 1075
    :cond_0
    invoke-virtual {p0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object p0

    .line 1076
    invoke-virtual {p1, p0}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    return-void

    .line 1082
    :cond_1
    sget-object p0, Lcom/meawallet/mtp/bo;->a:Ljava/lang/String;

    const/16 p1, 0x1f5

    const-string p2, "JobScheduler is null."

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p0, p1, p2, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1089
    :cond_2
    invoke-virtual {p1}, Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;->b()Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    const-class p2, Lcom/meawallet/mtp/JobDispatcherReplenishPaymentTokens;

    .line 1090
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a(Ljava/lang/Class;)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    const-string p2, "replenish-payment-tokens-job"

    .line 1091
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a(Ljava/lang/String;)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    new-array p2, v2, [I

    const/4 v0, 0x2

    aput v0, p2, v1

    .line 1092
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a([I)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    .line 1093
    invoke-virtual {p0, v1}, Lcom/firebase/jobdispatcher/m$a;->b(Z)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    .line 1094
    invoke-virtual {p0, v2}, Lcom/firebase/jobdispatcher/m$a;->a(Z)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    sget-object p2, Lcom/firebase/jobdispatcher/x;->a:Lcom/firebase/jobdispatcher/t$c;

    .line 1095
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a(Lcom/firebase/jobdispatcher/t;)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    .line 1096
    invoke-virtual {p0}, Lcom/firebase/jobdispatcher/m$a;->j()Lcom/firebase/jobdispatcher/m;

    move-result-object p0

    .line 1098
    invoke-virtual {p1, p0}, Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;->b(Lcom/firebase/jobdispatcher/m;)V

    return-void
.end method

.method static b(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Z)V
    .locals 6

    .line 102
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1f5

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x15

    if-lt v0, v5, :cond_2

    const-string p1, "jobscheduler"

    .line 1119
    invoke-virtual {p0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/job/JobScheduler;

    if-eqz p1, :cond_1

    .line 1123
    new-instance v0, Landroid/app/job/JobInfo$Builder;

    new-instance v1, Landroid/content/ComponentName;

    const-class v4, Lcom/meawallet/mtp/JobSchedulerDeleteCards;

    invoke-direct {v1, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v2, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 1125
    invoke-virtual {v0, v3}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object p0

    if-eqz p2, :cond_0

    const-wide/16 v0, 0x3e8

    .line 1128
    invoke-virtual {p0, v0, v1}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    .line 1131
    :cond_0
    invoke-virtual {p0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object p0

    .line 1132
    invoke-virtual {p1, p0}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    return-void

    .line 1139
    :cond_1
    sget-object p0, Lcom/meawallet/mtp/bo;->a:Ljava/lang/String;

    const-string p1, "JobScheduler is null."

    new-array p2, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, p1, p2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_2
    if-nez p1, :cond_3

    .line 106
    sget-object p0, Lcom/meawallet/mtp/bo;->a:Ljava/lang/String;

    const-string p1, "Job dispatcher is null."

    new-array p2, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, p1, p2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1146
    :cond_3
    invoke-virtual {p1}, Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;->b()Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    const-class p2, Lcom/meawallet/mtp/JobDispatcherDeleteCards;

    .line 1147
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a(Ljava/lang/Class;)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    const-string p2, "delete-cards-job"

    .line 1148
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a(Ljava/lang/String;)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    new-array p2, v3, [I

    aput v2, p2, v4

    .line 1149
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a([I)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    .line 1150
    invoke-virtual {p0, v4}, Lcom/firebase/jobdispatcher/m$a;->b(Z)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    .line 1151
    invoke-virtual {p0, v3}, Lcom/firebase/jobdispatcher/m$a;->a(Z)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    sget-object p2, Lcom/firebase/jobdispatcher/x;->a:Lcom/firebase/jobdispatcher/t$c;

    .line 1152
    invoke-virtual {p0, p2}, Lcom/firebase/jobdispatcher/m$a;->a(Lcom/firebase/jobdispatcher/t;)Lcom/firebase/jobdispatcher/m$a;

    move-result-object p0

    .line 1153
    invoke-virtual {p0}, Lcom/firebase/jobdispatcher/m$a;->j()Lcom/firebase/jobdispatcher/m;

    move-result-object p0

    .line 1155
    invoke-virtual {p1, p0}, Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;->b(Lcom/firebase/jobdispatcher/m;)V

    return-void
.end method
