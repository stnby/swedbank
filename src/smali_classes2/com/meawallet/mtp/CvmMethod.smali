.class final enum Lcom/meawallet/mtp/CvmMethod;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/CvmMethod;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum LOCAL_CVM:Lcom/meawallet/mtp/CvmMethod;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum MOBILE_PIN:Lcom/meawallet/mtp/CvmMethod;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum WALLET_PIN:Lcom/meawallet/mtp/CvmMethod;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/CvmMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 6
    new-instance v0, Lcom/meawallet/mtp/CvmMethod;

    const-string v1, "LOCAL_CVM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/CvmMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CvmMethod;->LOCAL_CVM:Lcom/meawallet/mtp/CvmMethod;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/CvmMethod;

    const-string v1, "WALLET_PIN"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/CvmMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CvmMethod;->WALLET_PIN:Lcom/meawallet/mtp/CvmMethod;

    .line 10
    new-instance v0, Lcom/meawallet/mtp/CvmMethod;

    const-string v1, "MOBILE_PIN"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/CvmMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CvmMethod;->MOBILE_PIN:Lcom/meawallet/mtp/CvmMethod;

    const/4 v0, 0x3

    .line 5
    new-array v0, v0, [Lcom/meawallet/mtp/CvmMethod;

    sget-object v1, Lcom/meawallet/mtp/CvmMethod;->LOCAL_CVM:Lcom/meawallet/mtp/CvmMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/CvmMethod;->WALLET_PIN:Lcom/meawallet/mtp/CvmMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/CvmMethod;->MOBILE_PIN:Lcom/meawallet/mtp/CvmMethod;

    aput-object v1, v0, v4

    sput-object v0, Lcom/meawallet/mtp/CvmMethod;->a:[Lcom/meawallet/mtp/CvmMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/CvmMethod;
    .locals 1

    .line 5
    const-class v0, Lcom/meawallet/mtp/CvmMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/CvmMethod;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/CvmMethod;
    .locals 1

    .line 5
    sget-object v0, Lcom/meawallet/mtp/CvmMethod;->a:[Lcom/meawallet/mtp/CvmMethod;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/CvmMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/CvmMethod;

    return-object v0
.end method
