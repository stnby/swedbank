.class public Lcom/meawallet/mtp/MeaUncheckedException;
.super Ljava/lang/RuntimeException;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/cz;


# static fields
.field private static final a:Ljava/lang/String; = "MeaUncheckedException"


# instance fields
.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const/16 p1, 0x1f5

    .line 24
    iput p1, p0, Lcom/meawallet/mtp/MeaUncheckedException;->b:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 4

    .line 34
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 36
    sget-object p1, Lcom/meawallet/mtp/MeaUncheckedException;->a:Ljava/lang/String;

    const-string v0, "MeaErrorCode = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1, p2, v0, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 38
    iput p2, p0, Lcom/meawallet/mtp/MeaUncheckedException;->b:I

    return-void
.end method


# virtual methods
.method public bridge synthetic getMeaError()Lcom/meawallet/mtp/MeaError;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaUncheckedException;->getMeaError()Lcom/meawallet/mtp/cy;

    move-result-object v0

    return-object v0
.end method

.method public getMeaError()Lcom/meawallet/mtp/cy;
    .locals 3

    .line 48
    new-instance v0, Lcom/meawallet/mtp/cy;

    iget v1, p0, Lcom/meawallet/mtp/MeaUncheckedException;->b:I

    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaUncheckedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object v0
.end method
