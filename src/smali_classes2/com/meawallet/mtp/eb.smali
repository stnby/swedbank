.class Lcom/meawallet/mtp/eb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# static fields
.field private static final a:Ljava/lang/String; = "eb"


# instance fields
.field private b:Lcom/meawallet/mtp/ai;

.field private c:I

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ai;ILjava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/meawallet/mtp/eb;->b:Lcom/meawallet/mtp/ai;

    .line 37
    iput p2, p0, Lcom/meawallet/mtp/eb;->c:I

    .line 38
    iput-object p3, p0, Lcom/meawallet/mtp/eb;->d:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/security/cert/X509Certificate;)Z
    .locals 2

    .line 107
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1120
    check-cast p1, Ljava/security/interfaces/RSAPublicKey;

    .line 1121
    invoke-interface {p1}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object p1

    .line 1122
    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    const/4 v0, 0x1

    .line 1124
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->copyOfRange(II)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 1127
    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    .line 110
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/eb;->b:Lcom/meawallet/mtp/ai;

    iget v1, p0, Lcom/meawallet/mtp/eb;->c:I

    invoke-interface {v0, p1, v1}, Lcom/meawallet/mtp/ai;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;I)Z

    move-result p1
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 112
    sget-object v0, Lcom/meawallet/mtp/eb;->a:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "TrustAllX509TrustManager"
        }
    .end annotation

    return-void
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 7

    if-eqz p1, :cond_6

    .line 47
    array-length v0, p1

    if-eqz v0, :cond_6

    :try_start_0
    const-string v0, "X509"

    .line 53
    invoke-static {v0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    const/4 v1, 0x0

    .line 54
    invoke-virtual {v0, v1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 58
    invoke-virtual {v0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, v0, v4

    .line 59
    instance-of v6, v5, Ljavax/net/ssl/X509TrustManager;

    if-eqz v6, :cond_0

    .line 60
    move-object v1, v5

    check-cast v1, Ljavax/net/ssl/X509TrustManager;

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 66
    :cond_1
    :goto_1
    new-instance v0, Landroid/net/http/X509TrustManagerExtensions;

    invoke-direct {v0, v1}, Landroid/net/http/X509TrustManagerExtensions;-><init>(Ljavax/net/ssl/X509TrustManager;)V

    .line 67
    iget-object v1, p0, Lcom/meawallet/mtp/eb;->d:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1}, Landroid/net/http/X509TrustManagerExtensions;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_5

    .line 75
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    if-eqz p2, :cond_5

    .line 81
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/security/cert/X509Certificate;

    .line 82
    invoke-direct {p0, p2}, Lcom/meawallet/mtp/eb;->a(Ljava/security/cert/X509Certificate;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    const/4 v3, 0x1

    goto :goto_2

    :cond_3
    if-eqz v3, :cond_4

    return-void

    .line 89
    :cond_4
    new-instance p1, Ljava/security/cert/CertificateException;

    const-string p2, "Invalid certificate pin."

    invoke-direct {p1, p2}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 76
    :cond_5
    new-instance p1, Ljava/security/cert/CertificateException;

    const-string p2, "Failed to validate server."

    invoke-direct {p1, p2}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    .line 70
    sget-object p2, Lcom/meawallet/mtp/eb;->a:Ljava/lang/String;

    invoke-static {p2, p1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    new-instance p1, Ljava/security/cert/CertificateException;

    const-string p2, "Failed to validate server."

    invoke-direct {p1, p2}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 48
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Failed to validate server."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
