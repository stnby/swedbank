.class final enum Lcom/meawallet/mtp/RemoteErrorCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/RemoteErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUTHENTICATION:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum CARD_NOT_ELIGIBLE:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum DEVICE_NOT_ELIGIBLE:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum ENCRYPTION:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum INTERNAL:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum INVALID_JSON:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum PROCESSING:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum VALIDATION:Lcom/meawallet/mtp/RemoteErrorCode;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/RemoteErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 6
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "INVALID_JSON"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->INVALID_JSON:Lcom/meawallet/mtp/RemoteErrorCode;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "VALIDATION"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->VALIDATION:Lcom/meawallet/mtp/RemoteErrorCode;

    .line 10
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "ENCRYPTION"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->ENCRYPTION:Lcom/meawallet/mtp/RemoteErrorCode;

    .line 12
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "AUTHENTICATION"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->AUTHENTICATION:Lcom/meawallet/mtp/RemoteErrorCode;

    .line 14
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "INTERNAL"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->INTERNAL:Lcom/meawallet/mtp/RemoteErrorCode;

    .line 16
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "PROCESSING"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->PROCESSING:Lcom/meawallet/mtp/RemoteErrorCode;

    .line 18
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "DEVICE_NOT_ELIGIBLE"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->DEVICE_NOT_ELIGIBLE:Lcom/meawallet/mtp/RemoteErrorCode;

    .line 20
    new-instance v0, Lcom/meawallet/mtp/RemoteErrorCode;

    const-string v1, "CARD_NOT_ELIGIBLE"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/meawallet/mtp/RemoteErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->CARD_NOT_ELIGIBLE:Lcom/meawallet/mtp/RemoteErrorCode;

    const/16 v0, 0x8

    .line 5
    new-array v0, v0, [Lcom/meawallet/mtp/RemoteErrorCode;

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->INVALID_JSON:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->VALIDATION:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->ENCRYPTION:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->AUTHENTICATION:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->INTERNAL:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->PROCESSING:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->DEVICE_NOT_ELIGIBLE:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/meawallet/mtp/RemoteErrorCode;->CARD_NOT_ELIGIBLE:Lcom/meawallet/mtp/RemoteErrorCode;

    aput-object v1, v0, v9

    sput-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->a:[Lcom/meawallet/mtp/RemoteErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/RemoteErrorCode;
    .locals 1

    .line 5
    const-class v0, Lcom/meawallet/mtp/RemoteErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/RemoteErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/RemoteErrorCode;
    .locals 1

    .line 5
    sget-object v0, Lcom/meawallet/mtp/RemoteErrorCode;->a:[Lcom/meawallet/mtp/RemoteErrorCode;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/RemoteErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/RemoteErrorCode;

    return-object v0
.end method
