.class final Lcom/meawallet/mtp/dy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccountType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;
    .locals 1

    .line 15
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;->UNKNOWN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/AccountType;

    return-object v0
.end method

.method public final getProductType()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;
    .locals 1

    .line 20
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;->COMMERCIAL:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/ProductType;

    return-object v0
.end method

.method public final getUcafVersion()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;
    .locals 1

    .line 25
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;->V0:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/UcafVersion;

    return-object v0
.end method

.method public final isTransitSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isUsAipMaskSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
