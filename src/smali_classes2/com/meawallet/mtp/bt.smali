.class final Lcom/meawallet/mtp/bt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uuid"
    .end annotation
.end field

.field b:Lcom/meawallet/mtp/MeaCardState;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cardState"
    .end annotation
.end field

.field c:Lcom/meawallet/mtp/LdePinState;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pinState"
    .end annotation
.end field

.field d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenUniqueReference"
    .end annotation
.end field

.field e:Lcom/meawallet/mtp/MeaDigitizationDecision;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "decision"
    .end annotation
.end field

.field f:[Lcom/meawallet/mtp/MeaAuthenticationMethod;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "authenticationMethods"
    .end annotation
.end field

.field g:Lcom/meawallet/mtp/MeaProductConfig;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "productConfig"
    .end annotation
.end field

.field h:Lcom/meawallet/mtp/MeaTokenInfo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenInfo"
    .end annotation
.end field

.field i:Lcom/meawallet/mtp/MeaEligibilityReceipt;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "eligibilityReceipt"
    .end annotation
.end field

.field j:Lcom/meawallet/mtp/PaymentNetwork;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentNetwork"
    .end annotation
.end field

.field k:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "termsAndConditionsAssetId"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
