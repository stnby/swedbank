.class Lcom/meawallet/mtp/ba;
.super Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/ba$a;
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String; = "ba"


# instance fields
.field a:Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

.field b:Landroid/os/CancellationSignal;

.field private final d:Lcom/meawallet/mtp/ba$a;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ba$a;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 4

    .line 43
    invoke-static {}, Lcom/meawallet/mtp/br;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "fingerprint_fragment_key"

    .line 49
    invoke-static {v0}, Lcom/meawallet/mtp/br;->a(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 51
    invoke-static {v0}, Lcom/meawallet/mtp/br;->a(Ljavax/crypto/SecretKey;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 52
    new-instance v2, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    invoke-direct {v2, v0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Cipher;)V

    iput-object v2, p0, Lcom/meawallet/mtp/ba;->a:Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;
    :try_end_0
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    .line 55
    sget-object v2, Lcom/meawallet/mtp/ba;->c:Ljava/lang/String;

    const/16 v3, 0x12d

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v0, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 57
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to initialize Cipher."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onAuthenticationError(ILjava/lang/CharSequence;)V
    .locals 3

    const/4 v0, 0x2

    .line 97
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 99
    iget-object v0, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 102
    iget-object p1, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    invoke-interface {p1}, Lcom/meawallet/mtp/ba$a;->onCancel()V

    return-void

    .line 105
    :cond_0
    iget-object p1, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/meawallet/mtp/ba$a;->onError(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onAuthenticationFailed()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    const-string v1, "Authentication failed."

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ba$a;->onError(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .locals 3

    const/4 v0, 0x2

    .line 112
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 114
    iget-object v0, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    invoke-interface {v0, p1, p2}, Lcom/meawallet/mtp/ba$a;->onHelp(ILjava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onAuthenticationSucceeded(Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/meawallet/mtp/ba;->d:Lcom/meawallet/mtp/ba$a;

    invoke-virtual {p1}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;->getCryptoObject()Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    move-result-object p1

    invoke-virtual {p1}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/meawallet/mtp/ba$a;->onAuthenticated(Ljavax/crypto/Cipher;)V

    :cond_0
    return-void
.end method
