.class final Lcom/meawallet/mtp/au$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/au;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

.field final synthetic b:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iput-object p2, p0, Lcom/meawallet/mtp/au$2;->b:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->aid:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getAlternateContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->b:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->alternateContactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    invoke-static {v0}, Lcom/meawallet/mtp/as;->a(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;

    move-result-object v0

    return-object v0
.end method

.method public final getCdol1RelatedDataLength()I
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->cdol1RelatedDataLength:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCiacDeclineOnPpms()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvmModel()Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;
    .locals 4

    .line 175
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->cvmModel:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 176
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->cvmModel:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x66c3a89a

    if-eq v2, v3, :cond_3

    const v3, -0x5e1f552b

    if-eq v2, v3, :cond_2

    const v3, 0x7473b8c5

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "FLEXIBLE_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "CDCVM_ALWAYS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const-string v2, "CARD_LIKE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 187
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 185
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CARD_LIKE:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 183
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    .line 181
    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getGpoResponse()[B
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->gpoResponse:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getIccPrivateKeyCrtComponents()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->b:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->protectedIccPrivateKeyCrtComponents:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    invoke-static {v0}, Lcom/meawallet/mtp/as;->a(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v0

    return-object v0
.end method

.method public final getIssuerApplicationData()[B
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->issuerApplicationData:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->paymentFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPinIvCvc3Track2()[B
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->pinIvCvc3Track2:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getPpseFci()[B
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->ppseFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;"
        }
    .end annotation

    .line 229
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->b:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->records:[Lcom/mastercard/mpsdk/card/profile/RecordsJson;

    invoke-static {v0}, Lcom/meawallet/mtp/as;->a([Lcom/mastercard/mpsdk/card/profile/RecordsJson;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getTrack1ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->b:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->track1ConstructionData:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    .line 235
    invoke-static {v0}, Lcom/meawallet/mtp/au;->a(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;

    move-result-object v0

    return-object v0
.end method

.method public final getTrack2ConstructionData()Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->b:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->track2ConstructionData:Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;

    .line 242
    invoke-static {v0}, Lcom/meawallet/mtp/au;->a(Lcom/mastercard/mpsdk/card/profile/v2/TrackConstructionDataV2Json;)Lcom/mastercard/mpsdk/componentinterface/TrackConstructionData;

    move-result-object v0

    return-object v0
.end method

.method public final getUmdGeneration()Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;
    .locals 4

    .line 191
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->umdGeneration:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 192
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->umdGeneration:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0xc95c30a

    if-eq v2, v3, :cond_3

    const v3, 0x42a6720a

    if-eq v2, v3, :cond_2

    const v3, 0x42ec93be

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "ALWAYS_VALIDATED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v2, "ALWAYS_GENERATE_RANDOM_UMD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    goto :goto_0

    :cond_3
    const-string v2, "VALIDATED_ON_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 203
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 201
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_GENERATE_RANDOM_UMD:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 199
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->VALIDATED_ON_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    .line 197
    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;->ALWAYS_VALIDATED:Lcom/mastercard/mpsdk/componentinterface/CardUmdConfig;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final isTransitSupported()Z
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->isTransitSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final isUsAipMaskingSupported()Z
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/meawallet/mtp/au$2;->a:Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/ContactlessPaymentDataV2Json;->isUsAipMaskingSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
