.class Lcom/meawallet/mtp/j;
.super Lcom/meawallet/mtp/dj;
.source "SourceFile"


# static fields
.field private static final c:Ljava/lang/String; = "j"


# instance fields
.field a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Lcom/meawallet/mtp/dj;-><init>()V

    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/meawallet/mtp/j;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 20
    invoke-super {p0}, Lcom/meawallet/mtp/dj;->a()V

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/meawallet/mtp/j;->a:Ljava/lang/String;

    return-void
.end method

.method public isCdCvmSuccessful(Lcom/mastercard/mpsdk/componentinterface/Card;)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 38
    :goto_0
    iget-object v3, p0, Lcom/meawallet/mtp/j;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x2

    const/4 v5, 0x3

    if-nez v3, :cond_1

    if-eqz v2, :cond_1

    .line 39
    iget-object v3, p0, Lcom/meawallet/mtp/j;->a:Ljava/lang/String;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 41
    new-array v6, v5, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/meawallet/mtp/j;->a:Ljava/lang/String;

    aput-object v7, v6, v1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v6, v4

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 44
    :goto_1
    new-array p1, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/meawallet/mtp/j;->a:Ljava/lang/String;

    aput-object v6, p1, v1

    .line 45
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, p1, v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, p1, v4

    if-eqz v3, :cond_3

    .line 48
    invoke-static {}, Lcom/meawallet/mtp/fp;->c()Z

    move-result p1

    .line 49
    invoke-static {}, Lcom/meawallet/mtp/fp;->d()Z

    move-result v2

    .line 51
    new-array v3, v5, [Ljava/lang/Object;

    iget-boolean v5, p0, Lcom/meawallet/mtp/j;->b:Z

    .line 52
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    .line 54
    iget-boolean v3, p0, Lcom/meawallet/mtp/j;->b:Z

    if-eqz v3, :cond_2

    if-eqz p1, :cond_2

    if-eqz v2, :cond_2

    return v0

    :cond_2
    return v1

    .line 56
    :cond_3
    sget-object p1, Lcom/meawallet/mtp/j;->c:Ljava/lang/String;

    const/16 v0, 0x1f5

    const-string v2, "CDCVM declined because card is not authenticated"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p1, v0, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return v1
.end method
