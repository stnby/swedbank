.class public final enum Lcom/meawallet/mtp/MeaMediaContent$Type;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaMediaContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaMediaContent$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum HTML_TEXT:Lcom/meawallet/mtp/MeaMediaContent$Type;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "text/html"
    .end annotation
.end field

.field public static final enum PDF:Lcom/meawallet/mtp/MeaMediaContent$Type;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "application/pdf"
    .end annotation
.end field

.field public static final enum PLAIN_TEXT:Lcom/meawallet/mtp/MeaMediaContent$Type;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "text/plain"
    .end annotation
.end field

.field public static final enum PNG:Lcom/meawallet/mtp/MeaMediaContent$Type;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "image/png"
    .end annotation
.end field

.field public static final enum SVG:Lcom/meawallet/mtp/MeaMediaContent$Type;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "image/svg+xml"
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaMediaContent$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 71
    new-instance v0, Lcom/meawallet/mtp/MeaMediaContent$Type;

    const-string v1, "PDF"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaMediaContent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaMediaContent$Type;->PDF:Lcom/meawallet/mtp/MeaMediaContent$Type;

    .line 74
    new-instance v0, Lcom/meawallet/mtp/MeaMediaContent$Type;

    const-string v1, "PNG"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaMediaContent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaMediaContent$Type;->PNG:Lcom/meawallet/mtp/MeaMediaContent$Type;

    .line 77
    new-instance v0, Lcom/meawallet/mtp/MeaMediaContent$Type;

    const-string v1, "PLAIN_TEXT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaMediaContent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaMediaContent$Type;->PLAIN_TEXT:Lcom/meawallet/mtp/MeaMediaContent$Type;

    .line 80
    new-instance v0, Lcom/meawallet/mtp/MeaMediaContent$Type;

    const-string v1, "HTML_TEXT"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/MeaMediaContent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaMediaContent$Type;->HTML_TEXT:Lcom/meawallet/mtp/MeaMediaContent$Type;

    .line 83
    new-instance v0, Lcom/meawallet/mtp/MeaMediaContent$Type;

    const-string v1, "SVG"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/MeaMediaContent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaMediaContent$Type;->SVG:Lcom/meawallet/mtp/MeaMediaContent$Type;

    const/4 v0, 0x5

    .line 70
    new-array v0, v0, [Lcom/meawallet/mtp/MeaMediaContent$Type;

    sget-object v1, Lcom/meawallet/mtp/MeaMediaContent$Type;->PDF:Lcom/meawallet/mtp/MeaMediaContent$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaMediaContent$Type;->PNG:Lcom/meawallet/mtp/MeaMediaContent$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaMediaContent$Type;->PLAIN_TEXT:Lcom/meawallet/mtp/MeaMediaContent$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/MeaMediaContent$Type;->HTML_TEXT:Lcom/meawallet/mtp/MeaMediaContent$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/MeaMediaContent$Type;->SVG:Lcom/meawallet/mtp/MeaMediaContent$Type;

    aput-object v1, v0, v6

    sput-object v0, Lcom/meawallet/mtp/MeaMediaContent$Type;->a:[Lcom/meawallet/mtp/MeaMediaContent$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaMediaContent$Type;
    .locals 1

    .line 70
    const-class v0, Lcom/meawallet/mtp/MeaMediaContent$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaMediaContent$Type;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaMediaContent$Type;
    .locals 1

    .line 70
    sget-object v0, Lcom/meawallet/mtp/MeaMediaContent$Type;->a:[Lcom/meawallet/mtp/MeaMediaContent$Type;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaMediaContent$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaMediaContent$Type;

    return-object v0
.end method
