.class Lcom/meawallet/mtp/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "n"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Z)Lcom/meawallet/mtp/CdCvmType;
    .locals 5

    if-eqz p0, :cond_0

    .line 28
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object p0

    return-object p0

    .line 1051
    :cond_0
    sget-object p0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_TO_DEVICE_UNLOCK_ALLOWED:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 1052
    sget-object p0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_TO_MOBILE_PIN_ALLOWED:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    .line 1056
    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 1059
    sget-object v3, Lcom/meawallet/mtp/CdCvmType;->NO_CDCVM:Lcom/meawallet/mtp/CdCvmType;

    goto :goto_0

    .line 1283
    :cond_1
    new-array v0, v2, [Ljava/lang/Object;

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    aput-object v4, v0, v1

    .line 1072
    invoke-static {}, Lcom/meawallet/mtp/n;->k()Z

    .line 2268
    invoke-static {}, Lcom/meawallet/mtp/n;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1076
    sget-object v3, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    goto :goto_0

    :cond_2
    if-eqz p0, :cond_3

    .line 3100
    invoke-static {}, Lcom/meawallet/mtp/n;->l()Z

    .line 3103
    invoke-static {}, Lcom/meawallet/mtp/n;->m()Z

    :cond_3
    :goto_0
    if-eqz v3, :cond_4

    .line 3316
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object p0

    const-string v0, "CDCVM_TYPE"

    invoke-virtual {v3}, Lcom/meawallet/mtp/CdCvmType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    .line 3320
    :cond_4
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object p0

    const-string v0, "CDCVM_TYPE"

    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    .line 43
    :goto_1
    new-array p0, v2, [Ljava/lang/Object;

    aput-object v3, p0, v1

    return-object v3
.end method

.method static a(Lcom/meawallet/mtp/CdCvmType;)Lcom/meawallet/mtp/CvmMethod;
    .locals 6

    const/4 v0, 0x1

    .line 183
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    .line 187
    sget-object p0, Lcom/meawallet/mtp/n;->a:Ljava/lang/String;

    const/16 v3, 0x1f5

    const-string v4, "getCvmMethod(cdCvmType = null) CvmMethod not selected. isCardLike = %b"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v0, v2

    invoke-static {p0, v3, v4, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v1

    .line 192
    :cond_0
    sget-object v0, Lcom/meawallet/mtp/n$1;->a:[I

    invoke-virtual {p0}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    return-object v1

    .line 195
    :pswitch_0
    sget-object p0, Lcom/meawallet/mtp/CvmMethod;->MOBILE_PIN:Lcom/meawallet/mtp/CvmMethod;

    return-object p0

    .line 198
    :pswitch_1
    sget-object p0, Lcom/meawallet/mtp/CvmMethod;->WALLET_PIN:Lcom/meawallet/mtp/CvmMethod;

    return-object p0

    .line 203
    :pswitch_2
    sget-object p0, Lcom/meawallet/mtp/CvmMethod;->LOCAL_CVM:Lcom/meawallet/mtp/CvmMethod;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static a(Lcom/meawallet/mtp/CdCvmType;Landroid/content/Context;)Lcom/meawallet/mtp/cv;
    .locals 4

    .line 116
    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    new-instance p0, Lcom/meawallet/mtp/dr;

    invoke-direct {p0}, Lcom/meawallet/mtp/dr;-><init>()V

    return-object p0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v2, 0x1f5

    if-nez p0, :cond_1

    .line 123
    sget-object p0, Lcom/meawallet/mtp/n;->a:Ljava/lang/String;

    const-string p1, "CdCvm type is null trying to get CdCvm status provider."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, p1, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    .line 128
    :cond_1
    sget-object v3, Lcom/meawallet/mtp/n$1;->a:[I

    invoke-virtual {p0}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result p0

    aget p0, v3, p0

    packed-switch p0, :pswitch_data_0

    .line 140
    sget-object p0, Lcom/meawallet/mtp/n;->a:Ljava/lang/String;

    const-string p1, "Unknown CdCvm type trying to get CdCvm status provider."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, p1, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    .line 137
    :pswitch_0
    new-instance p0, Lcom/meawallet/mtp/j;

    invoke-direct {p0}, Lcom/meawallet/mtp/j;-><init>()V

    return-object p0

    .line 134
    :pswitch_1
    new-instance p0, Lcom/meawallet/mtp/dj;

    invoke-direct {p0}, Lcom/meawallet/mtp/dj;-><init>()V

    return-object p0

    .line 131
    :pswitch_2
    new-instance p0, Lcom/meawallet/mtp/co;

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/co;-><init>(Landroid/content/Context;)V

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static a(ZZ)Lcom/meawallet/mtp/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Lcom/meawallet/mtp/d<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 245
    sget-object p0, Lcom/meawallet/mtp/n;->a:Ljava/lang/String;

    const-string p1, "Wrong library CDCVM configuration. isCardLike = %s, SUPPORTED_CDCVMS = %s"

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 246
    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1f5

    .line 245
    invoke-static {p0, v1, p1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 248
    new-instance p0, Lcom/meawallet/mtp/d;

    invoke-direct {p0}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance p1, Lcom/meawallet/mtp/cy;

    const-string v0, "Wrong library CDCVM configuration."

    invoke-direct {p1, v1, v0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 4029
    iput-object p1, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object p0

    .line 252
    :cond_0
    new-instance p0, Lcom/meawallet/mtp/d;

    invoke-direct {p0}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x385

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    .line 5029
    iput-object p1, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object p0
.end method

.method static a()Z
    .locals 4

    .line 91
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_WHEN_FINGERPRINT_NOT_READY:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->FALLBACK_TO_DEVICE_UNLOCK_ALLOWED:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 93
    :goto_0
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    return v0
.end method

.method static b()Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;
    .locals 1

    .line 147
    new-instance v0, Lcom/meawallet/mtp/dy;

    invoke-direct {v0}, Lcom/meawallet/mtp/dy;-><init>()V

    return-object v0
.end method

.method static b(Lcom/meawallet/mtp/CdCvmType;)Z
    .locals 4

    .line 230
    sget-object v0, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    sget-object v0, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const/4 v3, 0x2

    .line 232
    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    aput-object p0, v3, v2

    return v0
.end method

.method static c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;
    .locals 2

    const-string v0, "ALWAYS_CDCVM"

    .line 152
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0

    :cond_0
    const-string v0, "ALWAYS_CDCVM"

    .line 157
    sget-object v1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-virtual {v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0

    .line 164
    :cond_1
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    return-object v0
.end method

.method static d()Z
    .locals 4

    .line 169
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    invoke-static {}, Lcom/meawallet/mtp/n;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    .line 171
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    return v0
.end method

.method static e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method static f()Lcom/meawallet/mtp/CdCvmType;
    .locals 3

    .line 212
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CDCVM_TYPE"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/CdCvmType;->a(Ljava/lang/String;)Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0

    const/4 v1, 0x1

    .line 214
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    return-object v0
.end method

.method static g()Z
    .locals 2

    .line 220
    sget-object v0, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static h()Z
    .locals 2

    .line 224
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0

    .line 226
    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method static i()Z
    .locals 2

    .line 238
    sget-object v0, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static j()Z
    .locals 4

    .line 272
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x17

    if-lt v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 274
    :goto_0
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v1

    return v0
.end method

.method static k()Z
    .locals 4

    const/4 v0, 0x1

    .line 292
    new-array v1, v0, [Ljava/lang/Object;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    return v0
.end method

.method static l()Z
    .locals 3

    const/4 v0, 0x1

    .line 301
    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return v2
.end method

.method static m()Z
    .locals 3

    const/4 v0, 0x1

    .line 310
    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return v2
.end method
