.class final Lcom/meawallet/mtp/dd$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/dd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;

.field final synthetic c:Lcom/meawallet/mtp/dd;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/dd;Ljava/lang/String;Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;)V
    .locals 0

    .line 727
    iput-object p1, p0, Lcom/meawallet/mtp/dd$3;->c:Lcom/meawallet/mtp/dd;

    iput-object p2, p0, Lcom/meawallet/mtp/dd$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/meawallet/mtp/dd$3;->b:Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 4

    .line 2733
    invoke-static {}, Lcom/meawallet/mtp/dd;->G()Lcom/meawallet/mtp/cr;

    move-result-object v0

    iget-object v1, p0, Lcom/meawallet/mtp/dd$3;->a:Ljava/lang/String;

    const/4 v2, 0x1

    .line 3244
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 3246
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 3247
    iget-object v2, v0, Lcom/meawallet/mtp/cr;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    invoke-interface {v2, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;->encryptWalletData([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v1

    .line 3249
    iget-object v2, v0, Lcom/meawallet/mtp/cr;->a:Lcom/meawallet/mtp/cv;

    invoke-virtual {v2, v1}, Lcom/meawallet/mtp/cv;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V

    .line 3251
    iget-object v0, v0, Lcom/meawallet/mtp/cr;->c:Lcom/meawallet/mtp/ds;

    invoke-virtual {v0}, Lcom/meawallet/mtp/ds;->b()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 1740
    iget-object p1, p0, Lcom/meawallet/mtp/dd$3;->b:Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;

    if-eqz p1, :cond_0

    .line 1741
    iget-object p1, p0, Lcom/meawallet/mtp/dd$3;->b:Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;->onPinSet()V

    :cond_0
    return-void
.end method
