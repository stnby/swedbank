.class final enum Lcom/meawallet/mtp/bp;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/bp;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/meawallet/mtp/bp;

.field public static final enum b:Lcom/meawallet/mtp/bp;

.field public static final enum c:Lcom/meawallet/mtp/bp;

.field public static final enum d:Lcom/meawallet/mtp/bp;

.field public static final enum e:Lcom/meawallet/mtp/bp;

.field private static final synthetic g:[Lcom/meawallet/mtp/bp;


# instance fields
.field final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 5
    new-instance v0, Lcom/meawallet/mtp/bp;

    const-string v1, "ENVIRONMENT_CONT"

    const-string v2, "environment_container"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/meawallet/mtp/bp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    .line 6
    new-instance v0, Lcom/meawallet/mtp/bp;

    const-string v1, "CARD_LIST"

    const-string v2, "card_profiles_list"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/meawallet/mtp/bp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    .line 7
    new-instance v0, Lcom/meawallet/mtp/bp;

    const-string v1, "MOBILE_KEYS"

    const-string v2, "mobile_keys"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/meawallet/mtp/bp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/bp;->c:Lcom/meawallet/mtp/bp;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/bp;

    const-string v1, "TRANSACTION_LIMITS"

    const-string v2, "transaction_limits"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/meawallet/mtp/bp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/bp;->d:Lcom/meawallet/mtp/bp;

    .line 9
    new-instance v0, Lcom/meawallet/mtp/bp;

    const-string v1, "NOT_AUTHENTICATED_TRANSACTIONS"

    const-string v2, "not_authenticated_transactions"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/meawallet/mtp/bp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/bp;->e:Lcom/meawallet/mtp/bp;

    const/4 v0, 0x5

    .line 3
    new-array v0, v0, [Lcom/meawallet/mtp/bp;

    sget-object v1, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/bp;->b:Lcom/meawallet/mtp/bp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/bp;->c:Lcom/meawallet/mtp/bp;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/bp;->d:Lcom/meawallet/mtp/bp;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/bp;->e:Lcom/meawallet/mtp/bp;

    aput-object v1, v0, v7

    sput-object v0, Lcom/meawallet/mtp/bp;->g:[Lcom/meawallet/mtp/bp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput-object p3, p0, Lcom/meawallet/mtp/bp;->f:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/bp;
    .locals 1

    .line 3
    const-class v0, Lcom/meawallet/mtp/bp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/bp;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/bp;
    .locals 1

    .line 3
    sget-object v0, Lcom/meawallet/mtp/bp;->g:[Lcom/meawallet/mtp/bp;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/bp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/bp;

    return-object v0
.end method
