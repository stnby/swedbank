.class final Lcom/meawallet/mtp/gp;
.super Lcom/meawallet/mtp/gx;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "accountNumber"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cardholderName"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expiryMonth"
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expiryYear"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 23
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/gx;-><init>(Z)V

    .line 25
    iput-object p1, p0, Lcom/meawallet/mtp/gp;->a:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/meawallet/mtp/gp;->f:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/meawallet/mtp/gp;->g:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/meawallet/mtp/gp;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final b()V
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/meawallet/mtp/gp;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 35
    iget-object v0, p0, Lcom/meawallet/mtp/gp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lcom/meawallet/mtp/gp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x13

    if-gt v0, v1, :cond_5

    .line 40
    iget-object v0, p0, Lcom/meawallet/mtp/gp;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 45
    iget-object v0, p0, Lcom/meawallet/mtp/gp;->e:Ljava/lang/String;

    const-string v1, "^[A-Za-z0-9,.\\s]+$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 50
    iget-object v0, p0, Lcom/meawallet/mtp/gp;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1a

    if-gt v0, v1, :cond_2

    .line 55
    iget-object v0, p0, Lcom/meawallet/mtp/gp;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/meawallet/mtp/gp;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/gp;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/meawallet/mtp/gp;->g:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/meawallet/mtp/al;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 59
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Expiry date cannot be in past."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 62
    new-instance v1, Lcom/meawallet/mtp/bn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid expiry date. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    return-void

    .line 52
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Cardholder name is too long."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Cardholder name contains invalid characters."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "CardHolderName should not be empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_5
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "PAN length is not correct."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_6
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "PAN should not be empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
