.class final Lcom/meawallet/mtp/fq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "atc"
    .end annotation
.end field

.field b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "idn"
    .end annotation
.end field

.field c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contactlessMdSessionKey"
    .end annotation
.end field

.field d:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contactlessUmdSingleUseKey"
    .end annotation
.end field

.field e:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dsrpMdSessionKey"
    .end annotation
.end field

.field f:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dsrpUmdSingleUseKey"
    .end annotation
.end field

.field g:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contactlessUmdSessionKey"
    .end annotation
.end field

.field h:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dsrpUmdSessionKey"
    .end annotation
.end field


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
