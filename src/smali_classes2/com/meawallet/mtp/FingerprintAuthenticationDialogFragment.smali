.class public Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;
.super Landroid/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/ba$a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field private static final d:Ljava/lang/String; = "FingerprintAuthenticationDialogFragment"


# instance fields
.field a:Lcom/meawallet/mtp/cr;

.field b:Lcom/meawallet/mtp/MeaListener;

.field c:Lcom/mastercard/mpsdk/componentinterface/Card;

.field private e:Lcom/meawallet/mtp/ba;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/TextView;

.field private h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    .line 37
    iput-boolean v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->h:Z

    .line 38
    iput-boolean v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->i:Z

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;)Lcom/meawallet/mtp/cr;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->a:Lcom/meawallet/mtp/cr;

    return-object p0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .line 24
    sget-object v0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(ILjava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 1

    .line 216
    new-instance v0, Lcom/meawallet/mtp/cy;

    invoke-direct {v0, p1, p2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {p3, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    .line 218
    invoke-direct {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b()V

    const/4 p1, 0x1

    .line 220
    iput-boolean p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->i:Z

    .line 222
    invoke-virtual {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->dismissAllowingStateLoss()V

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    const/16 v0, 0x65

    const/4 v1, 0x0

    .line 24
    invoke-direct {p0, v0, v1, p1}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->a(ILjava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method static synthetic b(Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;)Lcom/meawallet/mtp/MeaListener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b:Lcom/meawallet/mtp/MeaListener;

    return-object p0
.end method

.method private b()V
    .locals 4

    .line 163
    iget-object v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->c:Lcom/mastercard/mpsdk/componentinterface/Card;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 164
    sget-object v0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->d:Ljava/lang/String;

    const/16 v2, 0x1f5

    const-string v3, "Failed to stop contactless transaction for card, Card object is null."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->c:Lcom/mastercard/mpsdk/componentinterface/Card;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Card;->stopContactlessTransaction()V

    const/4 v0, 0x1

    .line 171
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->c:Lcom/mastercard/mpsdk/componentinterface/Card;

    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-void
.end method


# virtual methods
.method public onAuthenticated(Ljavax/crypto/Cipher;)V
    .locals 2

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/meawallet/mtp/fp;->a(J)V

    .line 141
    invoke-static {}, Lcom/meawallet/mtp/fp;->k()V

    .line 143
    iget-object p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->a:Lcom/meawallet/mtp/cr;

    .line 1361
    iget-object p1, p1, Lcom/meawallet/mtp/cr;->c:Lcom/meawallet/mtp/ds;

    invoke-virtual {p1}, Lcom/meawallet/mtp/ds;->b()V

    .line 145
    iget-object p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaListener;->onSuccess()V

    .line 147
    invoke-virtual {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->dismissAllowingStateLoss()V

    return-void
.end method

.method public onCancel()V
    .locals 4

    .line 178
    iget-boolean v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 180
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    return-void

    .line 185
    :cond_0
    invoke-direct {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b()V

    .line 187
    invoke-static {}, Lcom/meawallet/mtp/fp;->e()V

    .line 189
    iget-object v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b:Lcom/meawallet/mtp/MeaListener;

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x38c

    const-string v3, "Cardholder authentication with fingerprint canceled."

    invoke-direct {v1, v2, v3}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    .line 193
    invoke-virtual {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->dismissAllowingStateLoss()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 42
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 45
    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->setRetainInstance(Z)V

    const/4 p1, 0x0

    const v0, 0x1030239

    .line 46
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->setStyle(II)V

    .line 48
    invoke-virtual {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/meawallet/mtp/ar;->h(Landroid/content/Context;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 50
    new-instance p1, Lcom/meawallet/mtp/ba;

    invoke-direct {p1, p0}, Lcom/meawallet/mtp/ba;-><init>(Lcom/meawallet/mtp/ba$a;)V

    iput-object p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->e:Lcom/meawallet/mtp/ba;

    .line 51
    iget-object p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->e:Lcom/meawallet/mtp/ba;

    invoke-virtual {p1}, Lcom/meawallet/mtp/ba;->a()Z

    move-result p1

    iput-boolean p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->h:Z

    .line 53
    iget-boolean p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->h:Z

    if-nez p1, :cond_1

    const/16 p1, 0x38d

    .line 54
    iget-object v1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-direct {p0, p1, v0, v1}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->a(ILjava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    return-void

    :cond_0
    const/16 p1, 0x387

    .line 61
    iget-object v1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-direct {p0, p1, v0, v1}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->a(ILjava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const/4 p3, 0x0

    .line 70
    iput-boolean p3, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->i:Z

    .line 72
    sget v0, Lcom/meawallet/mtp/R$layout;->fingerprint_dialog_container:I

    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 73
    invoke-virtual {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object p2

    sget p3, Lcom/meawallet/mtp/R$string;->auth_title:I

    invoke-virtual {p0, p3}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 75
    sget p2, Lcom/meawallet/mtp/R$id;->fingerprint_icon:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->f:Landroid/widget/ImageView;

    .line 76
    sget p2, Lcom/meawallet/mtp/R$id;->fingerprint_status:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->g:Landroid/widget/TextView;

    .line 78
    sget p2, Lcom/meawallet/mtp/R$id;->second_dialog_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 79
    new-instance p3, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment$1;

    invoke-direct {p3, p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment$1;-><init>(Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    sget p2, Lcom/meawallet/mtp/R$id;->cancel_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 96
    new-instance p3, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment$2;

    invoke-direct {p3, p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment$2;-><init>(Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method public onError(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    .line 198
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 200
    invoke-static {}, Lcom/meawallet/mtp/fp;->e()V

    const-string v1, "Failed to authenticated cardholder with fingerprint. %s"

    .line 202
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 203
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b:Lcom/meawallet/mtp/MeaListener;

    const/16 v1, 0x38a

    .line 202
    invoke-direct {p0, v1, p1, v0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->a(ILjava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public onHelp(ILjava/lang/CharSequence;)V
    .locals 2

    const/4 p1, 0x1

    .line 152
    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    .line 2209
    iget-object p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->f:Landroid/widget/ImageView;

    sget v0, Lcom/meawallet/mtp/R$drawable;->ic_fingerprint_error:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2211
    iget-object p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->g:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2212
    iget-object p1, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->g:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->g:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/meawallet/mtp/R$color;->warning_color:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public onPause()V
    .locals 2

    .line 117
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 119
    iget-boolean v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->h:Z

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->e:Lcom/meawallet/mtp/ba;

    .line 1089
    iget-object v1, v0, Lcom/meawallet/mtp/ba;->b:Landroid/os/CancellationSignal;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/meawallet/mtp/ba;->b:Landroid/os/CancellationSignal;

    invoke-virtual {v1}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1090
    iget-object v1, v0, Lcom/meawallet/mtp/ba;->b:Landroid/os/CancellationSignal;

    invoke-virtual {v1}, Landroid/os/CancellationSignal;->cancel()V

    const/4 v1, 0x0

    .line 1091
    iput-object v1, v0, Lcom/meawallet/mtp/ba;->b:Landroid/os/CancellationSignal;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 7

    .line 108
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 110
    iget-boolean v0, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->h:Z

    if-eqz v0, :cond_0

    .line 111
    iget-object v5, p0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->e:Lcom/meawallet/mtp/ba;

    invoke-virtual {p0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1066
    invoke-static {v0}, Lcom/meawallet/mtp/ar;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1071
    new-instance v1, Landroid/os/CancellationSignal;

    invoke-direct {v1}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v1, v5, Lcom/meawallet/mtp/ba;->b:Landroid/os/CancellationSignal;

    .line 1073
    iget-object v1, v5, Lcom/meawallet/mtp/ba;->a:Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    if-eqz v1, :cond_0

    const-string v1, "fingerprint"

    .line 1075
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/hardware/fingerprint/FingerprintManager;

    if-eqz v1, :cond_0

    .line 1082
    iget-object v2, v5, Lcom/meawallet/mtp/ba;->a:Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    iget-object v3, v5, Lcom/meawallet/mtp/ba;->b:Landroid/os/CancellationSignal;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/hardware/fingerprint/FingerprintManager;->authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V

    :cond_0
    return-void
.end method
