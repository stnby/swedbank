.class public interface abstract Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/ReplenishCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "LocalDekExchanger"
.end annotation


# virtual methods
.method public abstract exchangeDataWithLocalDek([B[B[B)V
.end method

.method public abstract getExchangedMdSessionKey()[B
.end method

.method public abstract getExchangedUmdSessionKey()[B
.end method

.method public abstract getExchangedUmdSingleUseKey()[B
.end method
