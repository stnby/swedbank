.class final Lcom/meawallet/mtp/cu$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d<",
        "Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/Card;

.field final synthetic b:Lcom/meawallet/mtp/MeaRemotePaymentData;

.field final synthetic c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

.field final synthetic d:Lcom/meawallet/mtp/MeaCard;

.field final synthetic e:Lcom/meawallet/mtp/cu;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/cu;Lcom/mastercard/mpsdk/componentinterface/Card;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;)V
    .locals 0

    .line 465
    iput-object p1, p0, Lcom/meawallet/mtp/cu$3;->e:Lcom/meawallet/mtp/cu;

    iput-object p2, p0, Lcom/meawallet/mtp/cu$3;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    iput-object p3, p0, Lcom/meawallet/mtp/cu$3;->b:Lcom/meawallet/mtp/MeaRemotePaymentData;

    iput-object p4, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iput-object p5, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 2

    .line 2470
    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    iget-object v1, p0, Lcom/meawallet/mtp/cu$3;->b:Lcom/meawallet/mtp/MeaRemotePaymentData;

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/Card;->processDsrpTransaction(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionContext;)Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;

    move-result-object v0

    .line 2472
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    .line 3023
    iput-object v0, v1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    return-object v1
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .line 465
    check-cast p1, Lcom/meawallet/mtp/d;

    .line 1478
    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->e:Lcom/meawallet/mtp/cu;

    invoke-static {v0}, Lcom/meawallet/mtp/cu;->c(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/cv;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1479
    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->e:Lcom/meawallet/mtp/cu;

    invoke-static {v0}, Lcom/meawallet/mtp/cu;->c(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/cv;->a()V

    :cond_0
    if-eqz p1, :cond_2

    .line 1482
    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    if-nez v0, :cond_1

    goto :goto_0

    .line 2036
    :cond_1
    iget-object p1, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 1487
    check-cast p1, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;

    .line 1489
    sget-object v0, Lcom/meawallet/mtp/cu$8;->a:[I

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getResult()Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome$Result;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1517
    iget-object p1, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x451

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0, v1}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 1513
    :pswitch_0
    iget-object p1, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x3f1

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0, v1}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 1509
    :pswitch_1
    iget-object p1, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x1f9

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0, v1}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 1505
    :pswitch_2
    iget-object p1, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0, v1}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 1501
    :pswitch_3
    iget-object p1, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x1f5

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0, v1}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 1497
    :pswitch_4
    iget-object p1, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iget-object v0, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x454

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0, v1}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 1492
    :pswitch_5
    new-instance v0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;

    invoke-direct {v0, p1}, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;-><init>(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;)V

    .line 1493
    iget-object p1, p0, Lcom/meawallet/mtp/cu$3;->c:Lcom/meawallet/mtp/MeaRemoteTransactionListener;

    iget-object v1, p0, Lcom/meawallet/mtp/cu$3;->d:Lcom/meawallet/mtp/MeaCard;

    invoke-interface {p1, v1, v0}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteCompleted(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;)V

    return-void

    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
