.class final Lcom/meawallet/mtp/dd$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/dd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/MeaListener;

.field final synthetic b:Lcom/meawallet/mtp/dd;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/dd;Lcom/meawallet/mtp/MeaListener;)V
    .locals 0

    .line 596
    iput-object p1, p0, Lcom/meawallet/mtp/dd$2;->b:Lcom/meawallet/mtp/dd;

    iput-object p2, p0, Lcom/meawallet/mtp/dd$2;->a:Lcom/meawallet/mtp/MeaListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b()Lcom/meawallet/mtp/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/meawallet/mtp/d<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 600
    invoke-static {}, Lcom/meawallet/mtp/dd;->A()Ljava/lang/String;

    .line 603
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->c()V

    .line 605
    invoke-static {}, Lcom/meawallet/mtp/dd;->C()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/meawallet/mtp/dd;->D()Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/meawallet/mtp/bo;->a(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;)V

    .line 606
    invoke-static {}, Lcom/meawallet/mtp/dd;->E()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->i()V

    .line 607
    invoke-static {}, Lcom/meawallet/mtp/dd;->F()Lcom/meawallet/mtp/dm;

    move-result-object v0

    .line 1263
    iget-object v0, v0, Lcom/meawallet/mtp/dm;->b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/Mcbp;->resetToUninitializedState()V

    .line 609
    invoke-static {}, Lcom/meawallet/mtp/dd;->C()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/dx;->a(Landroid/content/Context;)V

    .line 610
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/dx;->b()Z

    const/4 v0, 0x0

    .line 612
    invoke-static {v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->a(Lcom/meawallet/mtp/dd;)V

    .line 614
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 596
    invoke-static {}, Lcom/meawallet/mtp/dd$2;->b()Lcom/meawallet/mtp/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .line 596
    check-cast p1, Lcom/meawallet/mtp/d;

    .line 1623
    invoke-static {}, Lcom/meawallet/mtp/dd;->A()Ljava/lang/String;

    .line 1625
    new-instance v0, Lcom/meawallet/mtp/ft;

    invoke-direct {v0}, Lcom/meawallet/mtp/ft;-><init>()V

    iget-object v0, p0, Lcom/meawallet/mtp/dd$2;->a:Lcom/meawallet/mtp/MeaListener;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method
