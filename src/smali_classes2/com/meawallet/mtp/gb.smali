.class Lcom/meawallet/mtp/gb;
.super Lcom/meawallet/mtp/fv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/fv<",
        "Lcom/meawallet/mtp/MeaCompleteDigitizationListener;",
        "Lcom/meawallet/mtp/gd;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "gb"


# instance fields
.field private final h:Lcom/meawallet/mtp/MeaEligibilityReceipt;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/MeaEligibilityReceipt;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V
    .locals 7

    .line 34
    new-instance v6, Lcom/meawallet/mtp/gc;

    move-object v0, v6

    move-object v1, p4

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/gc;-><init>(Lcom/meawallet/mtp/MeaEligibilityReceipt;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, v6

    move-object/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/fv;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V

    move-object v1, p4

    .line 40
    iput-object v1, v0, Lcom/meawallet/mtp/gb;->h:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/gd;",
            ">;)V"
        }
    .end annotation

    .line 10147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 122
    invoke-virtual {p1}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 124
    check-cast v0, Lcom/meawallet/mtp/MeaCoreListener;

    .line 12041
    iget-object p1, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 124
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 13036
    :cond_0
    iget-object p1, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 129
    check-cast p1, Lcom/meawallet/mtp/gd;

    if-nez p1, :cond_1

    .line 13147
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 132
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteDigitizationListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f5

    const-string v2, "Complete digitization response data element is null."

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCompleteDigitizationListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 14034
    :cond_1
    iget-object v0, p1, Lcom/meawallet/mtp/gd;->c:Ljava/lang/String;

    .line 139
    new-instance v1, Lcom/meawallet/mtp/ct;

    iget-object v2, p0, Lcom/meawallet/mtp/gb;->h:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    invoke-virtual {v2}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    sget-object v0, Lcom/meawallet/mtp/gb$1;->a:[I

    .line 15026
    iget-object p1, p1, Lcom/meawallet/mtp/gd;->a:Lcom/meawallet/mtp/MeaDigitizationDecision;

    .line 141
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaDigitizationDecision;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 16147
    :pswitch_0
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 147
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteDigitizationListener;

    invoke-interface {p1, v1}, Lcom/meawallet/mtp/MeaCompleteDigitizationListener;->onRequireAdditionalAuthentication(Lcom/meawallet/mtp/MeaCard;)V

    goto :goto_0

    .line 15147
    :pswitch_1
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 143
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteDigitizationListener;

    invoke-interface {p1, v1}, Lcom/meawallet/mtp/MeaCompleteDigitizationListener;->onSuccess(Lcom/meawallet/mtp/MeaCard;)V

    return-void

    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method final b()Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 45
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method

.method final b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/gd;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 56
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 59
    :try_start_0
    const-class v1, Lcom/meawallet/mtp/gd;

    .line 60
    invoke-virtual {p0, p1, v1}, Lcom/meawallet/mtp/gb;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/gd;

    if-eqz p1, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/meawallet/mtp/gd;->validate()V

    .line 1026
    iget-object v1, p1, Lcom/meawallet/mtp/gd;->a:Lcom/meawallet/mtp/MeaDigitizationDecision;

    .line 71
    invoke-virtual {p0}, Lcom/meawallet/mtp/gb;->h()Lcom/meawallet/mtp/ci;

    move-result-object v3

    iget-object v4, p0, Lcom/meawallet/mtp/gb;->h:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    invoke-virtual {v4}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/meawallet/mtp/ci;->c(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object v3

    const/16 v4, 0x1f5

    if-nez v3, :cond_0

    .line 74
    new-instance p1, Lcom/meawallet/mtp/d;

    invoke-direct {p1}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cy;

    const-string v3, "Failed to get card by eligibility receipt: %s, LdeCard is null."

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/meawallet/mtp/gb;->h:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 76
    invoke-virtual {v5}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 1029
    iput-object v1, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object p1

    .line 1075
    :cond_0
    iput-object v1, v3, Lcom/meawallet/mtp/bt;->e:Lcom/meawallet/mtp/MeaDigitizationDecision;

    .line 2034
    iget-object v5, p1, Lcom/meawallet/mtp/gd;->c:Ljava/lang/String;

    .line 2092
    iput-object v5, v3, Lcom/meawallet/mtp/bt;->d:Ljava/lang/String;

    .line 3030
    iget-object v5, p1, Lcom/meawallet/mtp/gd;->b:[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    .line 3084
    iput-object v5, v3, Lcom/meawallet/mtp/bt;->f:[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    .line 4038
    iget-object v5, p1, Lcom/meawallet/mtp/gd;->d:Lcom/meawallet/mtp/MeaProductConfig;

    .line 4101
    iput-object v5, v3, Lcom/meawallet/mtp/bt;->g:Lcom/meawallet/mtp/MeaProductConfig;

    .line 5042
    iget-object v5, p1, Lcom/meawallet/mtp/gd;->e:Lcom/meawallet/mtp/MeaTokenInfo;

    .line 5110
    iput-object v5, v3, Lcom/meawallet/mtp/bt;->h:Lcom/meawallet/mtp/MeaTokenInfo;

    .line 85
    sget-object v5, Lcom/meawallet/mtp/gb$1;->a:[I

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaDigitizationDecision;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 104
    new-instance p1, Lcom/meawallet/mtp/d;

    goto :goto_1

    .line 100
    :pswitch_0
    new-instance p1, Lcom/meawallet/mtp/d;

    invoke-direct {p1}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x25d

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    .line 8029
    iput-object v0, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object p1

    .line 94
    :pswitch_1
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaCardState;

    .line 7058
    iput-object v0, v3, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 96
    invoke-virtual {p0}, Lcom/meawallet/mtp/gb;->h()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/meawallet/mtp/ci;->b(Lcom/meawallet/mtp/bt;)V

    goto :goto_0

    .line 88
    :pswitch_2
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->DIGITIZED:Lcom/meawallet/mtp/MeaCardState;

    .line 6058
    iput-object v0, v3, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 90
    invoke-virtual {p0}, Lcom/meawallet/mtp/gb;->h()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/meawallet/mtp/ci;->b(Lcom/meawallet/mtp/bt;)V

    .line 109
    :goto_0
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    .line 10023
    iput-object p1, v0, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    return-object v0

    .line 104
    :goto_1
    invoke-direct {p1}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v3, Lcom/meawallet/mtp/cy;

    const-string v5, "Wrong complete digitization decision type: %s."

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v1, v0, v2

    .line 106
    invoke-static {v5, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 9029
    iput-object v3, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object p1

    .line 63
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bl;

    const-string v0, "Response data is null."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bl; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 112
    new-instance v0, Lcom/meawallet/mtp/fs;

    invoke-direct {v0}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p1}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method final d()Lcom/meawallet/mtp/ep;
    .locals 1

    .line 50
    sget-object v0, Lcom/meawallet/mtp/gy;->e:Lcom/meawallet/mtp/ep;

    return-object v0
.end method

.method final e()V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/meawallet/mtp/gb;->h:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    if-eqz v0, :cond_0

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/gb;->h:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->validate()V
    :try_end_0
    .catch Lcom/meawallet/mtp/bl; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 164
    new-instance v1, Lcom/meawallet/mtp/bn;

    invoke-virtual {v0}, Lcom/meawallet/mtp/bl;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v1

    .line 159
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Mea eligibility receipt is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
