.class final Lcom/meawallet/mtp/cu$6$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/MeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/cu$6;->b()Lcom/meawallet/mtp/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/cu$6;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/cu$6;)V
    .locals 0

    .line 734
    iput-object p1, p0, Lcom/meawallet/mtp/cu$6$1;->a:Lcom/meawallet/mtp/cu$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic onFailure(Ljava/lang/Object;)V
    .locals 1

    .line 734
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    .line 2762
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 3154
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    .line 2763
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public final onSuccess()V
    .locals 3

    .line 738
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu$6$1;->a:Lcom/meawallet/mtp/cu$6;

    iget-object v0, v0, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    invoke-virtual {v0}, Lcom/meawallet/mtp/cu;->a()Ljava/util/List;

    move-result-object v0

    .line 740
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 742
    iget-object v0, p0, Lcom/meawallet/mtp/cu$6$1;->a:Lcom/meawallet/mtp/cu$6;

    iget-object v0, v0, Lcom/meawallet/mtp/cu$6;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result v0

    .line 743
    iget-object v1, p0, Lcom/meawallet/mtp/cu$6$1;->a:Lcom/meawallet/mtp/cu$6;

    iget-object v1, v1, Lcom/meawallet/mtp/cu$6;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/meawallet/mtp/cu$6$1;->a:Lcom/meawallet/mtp/cu$6;

    iget-object v2, v2, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    invoke-static {v2}, Lcom/meawallet/mtp/cu;->e(Lcom/meawallet/mtp/cu;)Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/meawallet/mtp/bo;->b(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Z)V

    return-void

    .line 745
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    .line 748
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 1154
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    .line 749
    invoke-static {v0}, Lcom/meawallet/mtp/ft;->b(Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 753
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v1

    .line 2154
    iget-object v1, v1, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    .line 756
    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    .line 754
    invoke-static {v1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method
