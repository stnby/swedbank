.class final Lcom/meawallet/mtp/go;
.super Lcom/meawallet/mtp/gx;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "encryptedData"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "publicKeyFingerprint"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "encryptedKey"
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "iv"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 26
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/gx;-><init>(Z)V

    .line 28
    iput-object p1, p0, Lcom/meawallet/mtp/go;->a:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/meawallet/mtp/go;->e:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/meawallet/mtp/go;->f:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/meawallet/mtp/go;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final b()V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/meawallet/mtp/go;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 38
    iget-object v0, p0, Lcom/meawallet/mtp/go;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 40
    iget-object v0, p0, Lcom/meawallet/mtp/go;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/meawallet/mtp/go;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Initial vector is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Encrypted key is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Public key fingerprint is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Encrypted card data is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
