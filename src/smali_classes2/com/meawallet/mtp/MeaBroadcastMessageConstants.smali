.class public interface abstract Lcom/meawallet/mtp/MeaBroadcastMessageConstants;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final INTENT_ACTION_ON_AUTHENTICATION_REQUIRED:Ljava/lang/String; = "com.meawallet.mtp.intent.ON_AUTHENTICATION_REQUIRED_MESSAGE"

.field public static final INTENT_ACTION_ON_CARD_PIN_RESET_REQUEST:Ljava/lang/String; = "com.meawallet.mtp.intent.ON_CARD_PIN_RESET_REQUEST"

.field public static final INTENT_ACTION_ON_TRANSACTION_FAILURE:Ljava/lang/String; = "com.meawallet.mtp.intent.ON_TRANSACTION_FAILURE_MESSAGE"

.field public static final INTENT_ACTION_ON_TRANSACTION_SUBMITTED:Ljava/lang/String; = "com.meawallet.mtp.intent.ON_TRANSACTION_SUBMITTED_MESSAGE"

.field public static final INTENT_ACTION_ON_WALLET_PIN_RESET_REQUEST:Ljava/lang/String; = "com.meawallet.mtp.intent.ON_WALLET_PIN_RESET_REQUEST"

.field public static final INTENT_DATA_AMOUNT_KEY:Ljava/lang/String; = "amount"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INTENT_DATA_CARD_ID_KEY:Ljava/lang/String; = "card_id"

.field public static final INTENT_DATA_CONTACTLESS_TRANSACTION_DATA:Ljava/lang/String; = "contactless_transaction_data"

.field public static final INTENT_DATA_CURRENCY_KEY:Ljava/lang/String; = "currency"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INTENT_DATA_ERROR_CODE_KEY:Ljava/lang/String; = "error_code"

.field public static final INTENT_DATA_ERROR_MESSAGE:Ljava/lang/String; = "error_message"

.field public static final INTENT_DATA_REASON_KEY:Ljava/lang/String; = "reason"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INTENT_DATA_TIME_KEY:Ljava/lang/String; = "time"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field
