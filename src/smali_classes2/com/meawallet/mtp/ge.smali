.class Lcom/meawallet/mtp/ge;
.super Lcom/meawallet/mtp/fv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/fv<",
        "Lcom/meawallet/mtp/MeaGetAssetListener;",
        "Lcom/meawallet/mtp/gg;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "ge"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/meawallet/mtp/MeaGetAssetListener;)V
    .locals 6

    .line 24
    new-instance v4, Lcom/meawallet/mtp/gf;

    invoke-direct {v4, p4}, Lcom/meawallet/mtp/gf;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/fv;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/gg;",
            ">;)V"
        }
    .end annotation

    .line 2147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    if-nez v0, :cond_0

    return-void

    .line 76
    :cond_0
    invoke-virtual {p1}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 78
    check-cast v0, Lcom/meawallet/mtp/MeaCoreListener;

    .line 4041
    iget-object p1, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 78
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 5036
    :cond_1
    iget-object p1, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 83
    check-cast p1, Lcom/meawallet/mtp/gg;

    if-nez p1, :cond_2

    .line 5147
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 86
    check-cast p1, Lcom/meawallet/mtp/MeaGetAssetListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f5

    const-string v2, "Get asset response data element is null."

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaGetAssetListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 6147
    :cond_2
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 91
    check-cast v0, Lcom/meawallet/mtp/MeaGetAssetListener;

    .line 7012
    iget-object p1, p1, Lcom/meawallet/mtp/gg;->a:[Lcom/meawallet/mtp/MeaMediaContent;

    .line 91
    invoke-interface {v0, p1}, Lcom/meawallet/mtp/MeaGetAssetListener;->onSuccess([Lcom/meawallet/mtp/MeaMediaContent;)V

    return-void
.end method

.method final b()Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 29
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method

.method final b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/gg;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 40
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 43
    :try_start_0
    const-class v0, Lcom/meawallet/mtp/gg;

    .line 44
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/ge;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/gg;

    if-eqz p1, :cond_1

    .line 50
    invoke-virtual {p1}, Lcom/meawallet/mtp/gg;->validate()V

    .line 1012
    iget-object v0, p1, Lcom/meawallet/mtp/gg;->a:[Lcom/meawallet/mtp/MeaMediaContent;

    if-nez v0, :cond_0

    .line 55
    new-instance p1, Lcom/meawallet/mtp/d;

    invoke-direct {p1}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1fb

    const-string v2, "Media content is null"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 1029
    iput-object v0, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object p1

    .line 59
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    .line 2023
    iput-object p1, v0, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    return-object v0

    .line 47
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bl;

    const-string v0, "Response data is null."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bl; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 61
    new-instance v0, Lcom/meawallet/mtp/fs;

    invoke-direct {v0}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p1}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p1

    return-object p1
.end method

.method final d()Lcom/meawallet/mtp/ep;
    .locals 1

    .line 34
    sget-object v0, Lcom/meawallet/mtp/gy;->h:Lcom/meawallet/mtp/ep;

    return-object v0
.end method
