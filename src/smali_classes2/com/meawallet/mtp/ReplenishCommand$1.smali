.class final Lcom/meawallet/mtp/ReplenishCommand$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/ReplenishCommand;->a([Lcom/meawallet/mtp/fq;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Ljava/util/ArrayList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:[B

.field final synthetic b:[B

.field final synthetic c:[B

.field final synthetic d:[B

.field final synthetic e:[B

.field final synthetic f:[B

.field final synthetic g:[B

.field final synthetic h:I

.field final synthetic i:Lcom/meawallet/mtp/ReplenishCommand;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/ReplenishCommand;[B[B[B[B[B[B[BI)V
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->i:Lcom/meawallet/mtp/ReplenishCommand;

    iput-object p2, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->a:[B

    iput-object p3, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->b:[B

    iput-object p4, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->c:[B

    iput-object p5, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->d:[B

    iput-object p6, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->e:[B

    iput-object p7, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->f:[B

    iput-object p8, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->g:[B

    iput p9, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->h:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAtc()I
    .locals 1

    .line 257
    iget v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->h:I

    return v0
.end method

.method public final getHash()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getIdn()[B
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->g:[B

    return-object v0
.end method

.method public final getInfo()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSessionKeyContactlessMd()[B
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->c:[B

    return-object v0
.end method

.method public final getSessionKeyContactlessUmd()[B
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->e:[B

    return-object v0
.end method

.method public final getSessionKeyRemotePaymentMd()[B
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->d:[B

    return-object v0
.end method

.method public final getSessionKeyRemotePaymentUmd()[B
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->f:[B

    return-object v0
.end method

.method public final getSukContactlessUmd()[B
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->a:[B

    return-object v0
.end method

.method public final getSukRemotePaymentUmd()[B
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$1;->b:[B

    return-object v0
.end method

.method public final getTimestamp()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final setStatus(I)V
    .locals 0

    return-void
.end method

.method public final wipe()V
    .locals 0

    return-void
.end method
