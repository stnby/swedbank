.class final Lcom/meawallet/mtp/as;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

.field static b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field static c:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;


# direct methods
.method static a(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 130
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/as$3;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/as$3;-><init>(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)V

    return-object v0
.end method

.method static a(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 3

    .line 85
    new-instance v0, Lcom/meawallet/mtp/as$2;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/as$2;-><init>(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)V

    .line 114
    :try_start_0
    sget-object p0, Lcom/meawallet/mtp/as;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    sget-object v1, Lcom/meawallet/mtp/as;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    sget-object v2, Lcom/meawallet/mtp/as;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p0, v1, v2, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeIccKekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p0
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string v0, "DomainProfileBuilder"

    .line 116
    invoke-virtual {p0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    new-instance p0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    const-string v0, ""

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object p0
.end method

.method static a([Lcom/mastercard/mpsdk/card/profile/RecordsJson;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/mastercard/mpsdk/card/profile/RecordsJson;",
            ")",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/Records;",
            ">;"
        }
    .end annotation

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    .line 64
    new-instance v4, Lcom/meawallet/mtp/as$1;

    invoke-direct {v4, v3}, Lcom/meawallet/mtp/as$1;-><init>(Lcom/mastercard/mpsdk/card/profile/RecordsJson;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method
