.class final Lcom/meawallet/mtp/bb;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 0

    .line 31
    invoke-direct/range {p0 .. p8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 3

    const/4 v0, 0x0

    .line 70
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/bb;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 92
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 94
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bb;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 86
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 87
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 86
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bb;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 79
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute Get System Health command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bb;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 72
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute Get System Health HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/bb;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 58
    iput-object p2, p0, Lcom/meawallet/mtp/bb;->c:Ljava/lang/String;

    .line 59
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 2317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 60
    iput-object p1, p0, Lcom/meawallet/mtp/bb;->h:Lcom/meawallet/mtp/ae;

    .line 62
    iget-object p1, p0, Lcom/meawallet/mtp/bb;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-ne p1, v0, :cond_0

    .line 63
    iget-object p1, p0, Lcom/meawallet/mtp/bb;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p1, p2, p3, p4}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final a(Lcom/meawallet/mtp/s;)V
    .locals 0

    .line 52
    sget-object p1, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 53
    sget-object p1, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    iput-object p1, p0, Lcom/meawallet/mtp/bb;->i:Lcom/meawallet/mtp/ah;

    .line 54
    iget-object p1, p0, Lcom/meawallet/mtp/bb;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p1}, Lcom/meawallet/mtp/ad;->a()V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/health"

    return-object v0
.end method

.method public final j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()V
    .locals 4

    .line 3230
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 100
    sget-object v1, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/meawallet/mtp/bb;->d:Lcom/meawallet/mtp/ad;

    const-string v1, "CANCELED"

    const-string v3, "Too many commands already in queue."

    invoke-interface {v0, v1, v3, v2}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 4230
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 106
    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_1

    .line 107
    iget-object v0, p0, Lcom/meawallet/mtp/bb;->d:Lcom/meawallet/mtp/ad;

    const-string v1, "CANCELED"

    const-string v3, "Duplicate Request"

    invoke-interface {v0, v1, v3, v2}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 113
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 4317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 115
    iget-object v0, p0, Lcom/meawallet/mtp/bb;->d:Lcom/meawallet/mtp/ad;

    const-string v1, "CANCELED"

    const-string v3, "Get System Health command cancelled or Could not get a valid session from CMS-D"

    invoke-interface {v0, v1, v3, v2}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 120
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/bb;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 125
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->GET:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
