.class Lcom/meawallet/mtp/TaskPin;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/TaskPin$a;,
        Lcom/meawallet/mtp/TaskPin$c;,
        Lcom/meawallet/mtp/TaskPin$b;,
        Lcom/meawallet/mtp/TaskPin$PinOperation;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/meawallet/mtp/MeaError;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "TaskPin"


# instance fields
.field private final b:Lcom/meawallet/mtp/dm;

.field private final c:Lcom/meawallet/mtp/TaskPin$a;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/dm;Lcom/meawallet/mtp/TaskPin$a;)V
    .locals 0

    .line 136
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/meawallet/mtp/TaskPin;->b:Lcom/meawallet/mtp/dm;

    .line 139
    iput-object p2, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 1

    .line 287
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 292
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    .line 294
    iget-object v0, p0, Lcom/meawallet/mtp/TaskPin;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->f()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;->encryptWalletData([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object p1

    return-object p1
.end method

.method private varargs a()Lcom/meawallet/mtp/MeaError;
    .locals 6

    .line 148
    invoke-static {}, Lcom/meawallet/mtp/dw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x6a

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object v0

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/TaskPin;->b:Lcom/meawallet/mtp/dm;

    .line 1273
    iget-object v0, v0, Lcom/meawallet/mtp/dm;->c:Lcom/meawallet/mtp/ci;

    .line 153
    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x6d

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object v0

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 159
    :try_start_0
    invoke-static {v1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 161
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x67

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    return-object v1

    .line 164
    :cond_2
    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 2109
    sget-object v3, Lcom/meawallet/mtp/TaskPin$1;->a:[I

    iget-object v4, v2, Lcom/meawallet/mtp/TaskPin$a;->a:Lcom/meawallet/mtp/TaskPin$PinOperation;

    invoke-virtual {v4}, Lcom/meawallet/mtp/TaskPin$PinOperation;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 2115
    :pswitch_0
    iget-object v3, v2, Lcom/meawallet/mtp/TaskPin$a;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2117
    iget-object v2, v2, Lcom/meawallet/mtp/TaskPin$a;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/meawallet/mtp/bn;

    const-string v2, "Old PIN is empty."

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2115
    :cond_4
    new-instance v1, Lcom/meawallet/mtp/bn;

    const-string v2, "New PIN is empty."

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2111
    :pswitch_1
    iget-object v2, v2, Lcom/meawallet/mtp/TaskPin$a;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_0

    :cond_5
    new-instance v1, Lcom/meawallet/mtp/bn;

    const-string v2, "New PIN is empty."

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v1

    .line 168
    :goto_0
    sget-object v2, Lcom/meawallet/mtp/TaskPin$1;->b:[I

    iget-object v3, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    invoke-virtual {v3}, Lcom/meawallet/mtp/TaskPin$a;->a()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/16 v3, 0x1f5

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    .line 192
    :pswitch_2
    sget-object v2, Lcom/meawallet/mtp/TaskPin$1;->a:[I

    iget-object v4, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 4105
    iget-object v4, v4, Lcom/meawallet/mtp/TaskPin$a;->a:Lcom/meawallet/mtp/TaskPin$PinOperation;

    .line 192
    invoke-virtual {v4}, Lcom/meawallet/mtp/TaskPin$PinOperation;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_2

    goto :goto_1

    .line 195
    :pswitch_3
    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    invoke-direct {p0}, Lcom/meawallet/mtp/TaskPin;->b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->changeWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    return-object v1

    .line 200
    :pswitch_4
    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    invoke-direct {p0}, Lcom/meawallet/mtp/TaskPin;->b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->setWalletPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 170
    :pswitch_5
    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    iget-object v4, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    check-cast v4, Lcom/meawallet/mtp/TaskPin$b;

    .line 3046
    iget-object v4, v4, Lcom/meawallet/mtp/TaskPin$b;->d:Ljava/lang/String;

    .line 170
    invoke-interface {v2, v4}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v2

    if-nez v2, :cond_6

    .line 173
    sget-object v1, Lcom/meawallet/mtp/TaskPin;->a:Ljava/lang/String;

    const-string v2, "Failed to set card PIN, card is not provisioned."

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v1, v3, v2, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 175
    new-instance v1, Lcom/meawallet/mtp/cy;

    const-string v2, "Failed to set card PIN, card is not provisioned."

    invoke-direct {v1, v3, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object v1

    .line 178
    :cond_6
    sget-object v4, Lcom/meawallet/mtp/TaskPin$1;->a:[I

    iget-object v5, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 3105
    iget-object v5, v5, Lcom/meawallet/mtp/TaskPin$a;->a:Lcom/meawallet/mtp/TaskPin$PinOperation;

    .line 178
    invoke-virtual {v5}, Lcom/meawallet/mtp/TaskPin$PinOperation;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_3

    goto :goto_1

    .line 181
    :pswitch_6
    invoke-direct {p0}, Lcom/meawallet/mtp/TaskPin;->b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/Card;->changePin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    return-object v1

    .line 186
    :pswitch_7
    invoke-direct {p0}, Lcom/meawallet/mtp/TaskPin;->b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/mastercard/mpsdk/componentinterface/Card;->setPin(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :goto_1
    move-object v2, v1

    :goto_2
    if-nez v2, :cond_7

    .line 207
    sget-object v1, Lcom/meawallet/mtp/TaskPin;->a:Ljava/lang/String;

    const-string v2, "Failed to set PIN."

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v1, v3, v2, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 209
    new-instance v1, Lcom/meawallet/mtp/cy;

    const-string v2, "Failed to set PIN."

    invoke-direct {v1, v3, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_7
    return-object v1

    :catch_0
    move-exception v1

    .line 216
    sget-object v2, Lcom/meawallet/mtp/TaskPin;->a:Ljava/lang/String;

    const-string v3, "Failed to complete PIN operation."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    invoke-static {v1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method private b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;
    .locals 3

    .line 272
    iget-object v0, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 5099
    iget-object v0, v0, Lcom/meawallet/mtp/TaskPin$a;->c:Ljava/lang/String;

    .line 272
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 5105
    iget-object v0, v0, Lcom/meawallet/mtp/TaskPin$a;->a:Lcom/meawallet/mtp/TaskPin$PinOperation;

    .line 277
    sget-object v1, Lcom/meawallet/mtp/TaskPin$PinOperation;->CHANGE:Lcom/meawallet/mtp/TaskPin$PinOperation;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 6095
    iget-object v0, v0, Lcom/meawallet/mtp/TaskPin$a;->b:Ljava/lang/String;

    .line 277
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 279
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Missing old PIN."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_1
    :goto_0
    new-instance v0, Lcom/meawallet/mtp/dc;

    iget-object v1, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 7095
    iget-object v1, v1, Lcom/meawallet/mtp/TaskPin$a;->b:Ljava/lang/String;

    .line 282
    invoke-direct {p0, v1}, Lcom/meawallet/mtp/TaskPin;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v1

    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 7099
    iget-object v2, v2, Lcom/meawallet/mtp/TaskPin$a;->c:Ljava/lang/String;

    .line 282
    invoke-direct {p0, v2}, Lcom/meawallet/mtp/TaskPin;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/dc;-><init>(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V

    return-object v0

    .line 274
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Missing new PIN."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/meawallet/mtp/TaskPin;->a()Lcom/meawallet/mtp/MeaError;

    move-result-object p1

    return-object p1
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .line 18
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    const/4 v0, 0x1

    .line 7224
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    if-eqz p1, :cond_0

    .line 7232
    sget-object v0, Lcom/meawallet/mtp/TaskPin$1;->b:[I

    iget-object v1, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    invoke-virtual {v1}, Lcom/meawallet/mtp/TaskPin$a;->a()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 7255
    :pswitch_0
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 10119
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->c:Lcom/meawallet/mtp/MeaWalletPinListener;

    .line 7257
    sget-object v1, Lcom/meawallet/mtp/TaskPin$1;->a:[I

    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 11105
    iget-object v2, v2, Lcom/meawallet/mtp/TaskPin$a;->a:Lcom/meawallet/mtp/TaskPin$PinOperation;

    .line 7257
    invoke-virtual {v2}, Lcom/meawallet/mtp/TaskPin$PinOperation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    .line 7260
    :pswitch_1
    invoke-interface {v0, p1}, Lcom/meawallet/mtp/MeaWalletPinListener;->onWalletPinChangeFailed(Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 7264
    :pswitch_2
    invoke-interface {v0, p1}, Lcom/meawallet/mtp/MeaWalletPinListener;->onWalletPinSetFailed(Lcom/meawallet/mtp/MeaError;)V

    goto :goto_1

    .line 7235
    :pswitch_3
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    iget-object v1, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    check-cast v1, Lcom/meawallet/mtp/TaskPin$b;

    .line 8046
    iget-object v1, v1, Lcom/meawallet/mtp/TaskPin$b;->d:Ljava/lang/String;

    .line 7235
    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/ax;->d(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardPinListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7242
    sget-object v1, Lcom/meawallet/mtp/TaskPin$1;->a:[I

    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    .line 8105
    iget-object v2, v2, Lcom/meawallet/mtp/TaskPin$a;->a:Lcom/meawallet/mtp/TaskPin$PinOperation;

    .line 7242
    invoke-virtual {v2}, Lcom/meawallet/mtp/TaskPin$PinOperation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 7245
    :pswitch_4
    new-instance v1, Lcom/meawallet/mtp/ct;

    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    check-cast v2, Lcom/meawallet/mtp/TaskPin$b;

    .line 9046
    iget-object v2, v2, Lcom/meawallet/mtp/TaskPin$b;->d:Ljava/lang/String;

    .line 7245
    invoke-direct {v1, v2}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Lcom/meawallet/mtp/MeaCardPinListener;->onCardPinChangeFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 7249
    :pswitch_5
    new-instance v1, Lcom/meawallet/mtp/ct;

    iget-object v2, p0, Lcom/meawallet/mtp/TaskPin;->c:Lcom/meawallet/mtp/TaskPin$a;

    check-cast v2, Lcom/meawallet/mtp/TaskPin$b;

    .line 10046
    iget-object v2, v2, Lcom/meawallet/mtp/TaskPin$b;->d:Ljava/lang/String;

    .line 7249
    invoke-direct {v1, v2}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Lcom/meawallet/mtp/MeaCardPinListener;->onCardPinSetFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    :goto_0
    return-void

    :cond_0
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
