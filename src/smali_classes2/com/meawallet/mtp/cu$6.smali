.class final Lcom/meawallet/mtp/cu$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/cu;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/meawallet/mtp/MeaListener;

.field final synthetic c:Lcom/meawallet/mtp/cu;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/cu;Landroid/content/Context;Lcom/meawallet/mtp/MeaListener;)V
    .locals 0

    .line 717
    iput-object p1, p0, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    iput-object p2, p0, Lcom/meawallet/mtp/cu$6;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/meawallet/mtp/cu$6;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()Lcom/meawallet/mtp/d;
    .locals 4

    .line 724
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/cu;->a(Z)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 726
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 732
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    invoke-static {v1}, Lcom/meawallet/mtp/cu;->d(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/l;

    move-result-object v1

    iget-object v2, p0, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    invoke-static {v2}, Lcom/meawallet/mtp/cu;->b(Lcom/meawallet/mtp/cu;)Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    iget-object v3, p0, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    invoke-static {v3}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/k;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/meawallet/mtp/l;->a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/k;)V

    .line 734
    iget-object v1, p0, Lcom/meawallet/mtp/cu$6;->c:Lcom/meawallet/mtp/cu;

    new-instance v2, Lcom/meawallet/mtp/cu$6$1;

    invoke-direct {v2, p0}, Lcom/meawallet/mtp/cu$6$1;-><init>(Lcom/meawallet/mtp/cu$6;)V

    invoke-static {v1, v0, v2}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/cu;Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 768
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    .line 1029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 727
    :cond_2
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    .line 729
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 775
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 777
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    .line 2029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 717
    invoke-direct {p0}, Lcom/meawallet/mtp/cu$6;->b()Lcom/meawallet/mtp/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 1

    .line 717
    check-cast p1, Lcom/meawallet/mtp/d;

    .line 2784
    iget-object v0, p0, Lcom/meawallet/mtp/cu$6;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method
