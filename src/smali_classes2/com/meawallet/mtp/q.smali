.class final Lcom/meawallet/mtp/q;
.super Lcom/meawallet/mtp/fj;
.source "SourceFile"


# instance fields
.field private n:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 0

    .line 32
    invoke-direct/range {p0 .. p10}, Lcom/meawallet/mtp/fj;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-void
.end method


# virtual methods
.method protected final a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    const-string v0, "CANCELED"

    .line 86
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Change Pin request is cancelled or "

    .line 87
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    :cond_0
    move-object v4, p3

    .line 90
    iget-object p3, p0, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    if-eqz p3, :cond_1

    .line 91
    iget-object v0, p0, Lcom/meawallet/mtp/q;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 99
    :cond_1
    iget-object p3, p0, Lcom/meawallet/mtp/q;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p3, p1, p2, v4, p4}, Lcom/meawallet/mtp/ad;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 4

    .line 47
    :try_start_0
    sget-object v0, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 49
    iget-object v0, p0, Lcom/meawallet/mtp/q;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    if-nez v0, :cond_0

    .line 52
    sget-object p1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v0, "MOBILE_KEYS_MISSING"

    const-string v1, "Failed to execute change PIN command"

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/meawallet/mtp/q;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/q;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    iget-object v2, p0, Lcom/meawallet/mtp/q;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v3, p0, Lcom/meawallet/mtp/q;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    .line 62
    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v3

    .line 60
    invoke-interface {v1, v0, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/meawallet/mtp/q;->m:Lcom/meawallet/mtp/o;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    .line 2029
    iput-object v0, v1, Lcom/meawallet/mtp/o;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 66
    iget-boolean v0, p0, Lcom/meawallet/mtp/q;->n:Z

    if-nez v0, :cond_2

    .line 67
    iget-object v0, p0, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/meawallet/mtp/q;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/meawallet/mtp/q;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/meawallet/mtp/q;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/q;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ad;->d(Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x1

    .line 73
    iput-boolean v0, p0, Lcom/meawallet/mtp/q;->n:Z

    .line 76
    :cond_2
    invoke-super {p0, p1}, Lcom/meawallet/mtp/fj;->a(Lcom/google/gson/Gson;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 78
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute change PIN command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/q;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected final a_()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/meawallet/mtp/q;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {v0}, Lcom/meawallet/mtp/ad;->d()V

    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/q;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ad;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .line 112
    instance-of v0, p1, Lcom/meawallet/mtp/q;

    if-eqz v0, :cond_0

    .line 113
    check-cast p1, Lcom/meawallet/mtp/q;

    iget-object p1, p1, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/meawallet/mtp/q;->j:Ljava/lang/String;

    .line 114
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
