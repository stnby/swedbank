.class Lcom/meawallet/mtp/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;


# instance fields
.field a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sessionFingerprint"
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "version"
    .end annotation
.end field

.field private c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sessionCode"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expiryTimestamp"
    .end annotation
.end field

.field private e:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "validForSeconds"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pendingAction"
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenUniqueReference"
    .end annotation
.end field


# direct methods
.method static a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/aa;
    .locals 2

    .line 88
    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/4 p1, 0x0

    .line 90
    :try_start_0
    const-class v1, Lcom/meawallet/mtp/aa;

    invoke-virtual {p0, v0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/aa;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    return-object p0

    :catchall_0
    move-exception p0

    goto :goto_0

    :catch_0
    move-exception p0

    move-object p1, p0

    .line 88
    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-eqz p1, :cond_0

    .line 91
    :try_start_2
    invoke-virtual {v0}, Ljava/io/Reader;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {p1, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    :goto_1
    throw p0
.end method


# virtual methods
.method public getExpiryTimestamp()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/meawallet/mtp/aa;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPendingAction()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/meawallet/mtp/aa;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionCode()[B
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/meawallet/mtp/aa;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public getTokenUniqueReference()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/meawallet/mtp/aa;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getValidForSeconds()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/meawallet/mtp/aa;->e:I

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/meawallet/mtp/aa;->b:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
