.class Lcom/meawallet/mtp/ft;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "ft"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaAuthenticationListener;I)V
    .locals 1

    if-eqz p0, :cond_0

    .line 146
    new-instance v0, Lcom/meawallet/mtp/cy;

    invoke-direct {v0, p1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p0, v0}, Lcom/meawallet/mtp/MeaAuthenticationListener;->onFailure(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;)V
    .locals 1

    const/4 v0, 0x0

    .line 109
    invoke-static {p0, p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;Lcom/meawallet/mtp/MeaCard;)V

    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;Lcom/meawallet/mtp/MeaCard;)V
    .locals 1

    if-nez p0, :cond_0

    .line 114
    sget-object p0, Lcom/meawallet/mtp/ft;->a:Ljava/lang/String;

    const/16 p1, 0x1f5

    const-string p2, "Failed to notify authentication required, listener is null."

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, p1, p2, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 119
    :cond_0
    sget-object v0, Lcom/meawallet/mtp/ft$3;->a:[I

    invoke-virtual {p1}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    if-eqz p2, :cond_1

    .line 138
    invoke-interface {p0, p2}, Lcom/meawallet/mtp/MeaAuthenticationListener;->onCardPinRequired(Lcom/meawallet/mtp/MeaCard;)V

    goto :goto_0

    .line 133
    :pswitch_1
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaAuthenticationListener;->onWalletPinRequired()V

    return-void

    .line 129
    :pswitch_2
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaAuthenticationListener;->onDeviceUnlockRequired()V

    return-void

    .line 125
    :pswitch_3
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaAuthenticationListener;->onFingerprintRequired()V

    return-void

    :pswitch_4
    return-void

    :cond_1
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static a(Lcom/meawallet/mtp/MeaCardListener;Lcom/meawallet/mtp/MeaCard;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 104
    invoke-interface {p0, p1}, Lcom/meawallet/mtp/MeaCardListener;->onSuccess(Lcom/meawallet/mtp/MeaCard;)V

    :cond_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 66
    invoke-interface {p0, p1, p2}, Lcom/meawallet/mtp/MeaCardReplenishListener;->onReplenishFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    :cond_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaCoreListener;I)V
    .locals 1

    if-eqz p0, :cond_0

    .line 38
    invoke-static {p1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    new-instance v0, Lcom/meawallet/mtp/cy;

    invoke-direct {v0, p1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    :cond_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V
    .locals 2

    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    .line 22
    invoke-interface {p0, p1}, Lcom/meawallet/mtp/MeaCoreListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 25
    :cond_0
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x1f5

    const-string v1, "Unknown error."

    invoke-direct {p1, v0, v1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-interface {p0, p1}, Lcom/meawallet/mtp/MeaCoreListener;->onFailure(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaListener;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 98
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaListener;->onSuccess()V

    :cond_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaListener;Lcom/meawallet/mtp/MeaError;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 153
    new-instance v0, Lcom/meawallet/mtp/ft$1;

    invoke-direct {v0, p1, p0}, Lcom/meawallet/mtp/ft$1;-><init>(Lcom/meawallet/mtp/MeaError;Lcom/meawallet/mtp/MeaListener;)V

    invoke-static {v0}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
    .locals 0

    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 53
    invoke-interface {p0, p1, p2}, Lcom/meawallet/mtp/MeaRemoteTransactionListener;->onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    :cond_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1041
    iget-object p0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 83
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 88
    :cond_0
    instance-of p0, p1, Lcom/meawallet/mtp/MeaListener;

    if-eqz p0, :cond_1

    .line 89
    check-cast p1, Lcom/meawallet/mtp/MeaListener;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaListener;->onSuccess()V

    :cond_1
    return-void
.end method

.method static b(Lcom/meawallet/mtp/MeaListener;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 175
    new-instance v0, Lcom/meawallet/mtp/ft$2;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/ft$2;-><init>(Lcom/meawallet/mtp/MeaListener;)V

    invoke-static {v0}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
