.class Lcom/meawallet/mtp/da;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/lang/String; = "da"

.field private static final f:[Ljava/lang/String;


# instance fields
.field a:I

.field b:Ljava/lang/String;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/meawallet/mtp/MeaHttpMethod;

.field private final h:Ljava/lang/String;

.field private final i:Ljavax/net/ssl/X509TrustManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->FORCE_TLS_PROTOCOL:[Ljava/lang/String;

    sput-object v0, Lcom/meawallet/mtp/da;->f:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/MeaHttpMethod;Ljava/lang/String;Ljavax/net/ssl/X509TrustManager;)V
    .locals 1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->REMOTE_REQUEST_TIMEOUT:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/meawallet/mtp/da;->a:I

    .line 76
    iput-object p1, p0, Lcom/meawallet/mtp/da;->g:Lcom/meawallet/mtp/MeaHttpMethod;

    .line 77
    iput-object p2, p0, Lcom/meawallet/mtp/da;->h:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/meawallet/mtp/da;->i:Ljavax/net/ssl/X509TrustManager;

    return-void
.end method

.method private a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 3

    const/4 v0, 0x1

    .line 250
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 251
    iget v1, p0, Lcom/meawallet/mtp/da;->a:I

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 252
    iget v1, p0, Lcom/meawallet/mtp/da;->a:I

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 253
    iget-object v1, p0, Lcom/meawallet/mtp/da;->g:Lcom/meawallet/mtp/MeaHttpMethod;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaHttpMethod;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 255
    sget-object v1, Lcom/meawallet/mtp/da$2;->a:[I

    iget-object v2, p0, Lcom/meawallet/mtp/da;->g:Lcom/meawallet/mtp/MeaHttpMethod;

    invoke-virtual {v2}, Lcom/meawallet/mtp/MeaHttpMethod;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "Content-Type"

    const-string v1, "text/plain, application/octet-stream, application/pkix-cert"

    .line 262
    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :pswitch_1
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const-string v0, "Content-Type"

    const-string v1, "application/json"

    .line 258
    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :goto_0
    iget-object v0, p0, Lcom/meawallet/mtp/da;->d:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/meawallet/mtp/da;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 269
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 271
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/meawallet/mtp/da;)Ljava/util/List;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/meawallet/mtp/da;->c:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/bf;)Lcom/meawallet/mtp/bj;
    .locals 9

    const/4 v0, 0x0

    .line 1183
    :try_start_0
    new-instance v1, Ljava/net/URL;

    iget-object v2, p0, Lcom/meawallet/mtp/da;->h:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 129
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    const-string v3, "https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    .line 2192
    new-array v3, v2, [Ljavax/net/ssl/TrustManager;

    iget-object v4, p0, Lcom/meawallet/mtp/da;->i:Ljavax/net/ssl/X509TrustManager;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "TLS"

    .line 2194
    invoke-static {v4}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v4

    .line 2195
    invoke-virtual {v4, v0, v3, v0}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 1210
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljavax/net/ssl/HttpsURLConnection;

    .line 1212
    sget-object v6, Lcom/meawallet/mtp/da;->f:[Ljava/lang/String;

    if-eqz v6, :cond_0

    sget-object v6, Lcom/meawallet/mtp/da;->f:[Ljava/lang/String;

    array-length v6, v6

    if-eqz v6, :cond_0

    .line 1213
    new-array v2, v2, [Ljava/lang/Object;

    sget-object v6, Lcom/meawallet/mtp/da;->f:[Ljava/lang/String;

    invoke-static {v6}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 1215
    new-instance v2, Lcom/meawallet/mtp/ak;

    invoke-virtual {v4}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v4

    sget-object v5, Lcom/meawallet/mtp/da;->f:[Ljava/lang/String;

    invoke-direct {v2, v4, v5}, Lcom/meawallet/mtp/ak;-><init>(Ljavax/net/ssl/SSLSocketFactory;[Ljava/lang/String;)V

    .line 1216
    invoke-virtual {v3, v2}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    goto :goto_0

    .line 1220
    :cond_0
    invoke-virtual {v4}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 1223
    :goto_0
    new-instance v2, Lcom/meawallet/mtp/da$1;

    invoke-direct {v2, p0}, Lcom/meawallet/mtp/da$1;-><init>(Lcom/meawallet/mtp/da;)V

    invoke-virtual {v3, v2}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 1229
    invoke-direct {p0, v3}, Lcom/meawallet/mtp/da;->a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    move-result-object v2

    check-cast v2, Ljavax/net/ssl/HttpsURLConnection;

    goto :goto_1

    .line 2241
    :cond_1
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 2243
    invoke-direct {p0, v2}, Lcom/meawallet/mtp/da;->a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    move-result-object v2
    :try_end_0
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_1
    move-object v8, v2

    .line 137
    :try_start_1
    sget-object v2, Lcom/meawallet/mtp/BuildConfig;->DEBUG_MODE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 138
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->getRequestProperties()Ljava/util/Map;

    move-result-object v0

    :cond_2
    move-object v4, v0

    .line 141
    iget-object v0, p0, Lcom/meawallet/mtp/da;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 142
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iget-object v2, p0, Lcom/meawallet/mtp/da;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    .line 145
    :cond_3
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->connect()V

    .line 147
    invoke-virtual {p1, v8}, Lcom/meawallet/mtp/bf;->a(Ljava/net/HttpURLConnection;)Lcom/meawallet/mtp/bg;

    move-result-object p1

    .line 150
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/meawallet/mtp/da;->b:Ljava/lang/String;

    .line 3026
    iget v5, p1, Lcom/meawallet/mtp/bg;->a:I

    .line 153
    new-instance v6, Ljava/lang/String;

    .line 3030
    iget-object v0, p1, Lcom/meawallet/mtp/bg;->b:[B

    const-string v7, "UTF-8"

    .line 154
    invoke-direct {v6, v0, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 155
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v7

    .line 3288
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->DEBUG_MODE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3293
    invoke-static/range {v1 .. v7}, Lcom/meawallet/mtp/bh;->a(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    .line 159
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bj;

    .line 4026
    iget v1, p1, Lcom/meawallet/mtp/bg;->a:I

    .line 4030
    iget-object p1, p1, Lcom/meawallet/mtp/bg;->b:[B

    .line 161
    invoke-static {v8}, Lcom/meawallet/mtp/bh;->a(Ljava/net/HttpURLConnection;)I

    move-result v2

    invoke-direct {v0, v1, p1, v2}, Lcom/meawallet/mtp/bj;-><init>(I[BI)V
    :try_end_1
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v8, :cond_5

    .line 172
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    return-object v0

    :catchall_0
    move-exception p1

    move-object v0, v8

    goto :goto_3

    :catch_0
    move-exception p1

    move-object v0, v8

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_3

    :catch_1
    move-exception p1

    .line 166
    :goto_2
    :try_start_2
    sget-object v1, Lcom/meawallet/mtp/da;->e:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 168
    new-instance v1, Lcom/meawallet/mtp/bi;

    invoke-direct {v1, p1}, Lcom/meawallet/mtp/bi;-><init>(Ljava/lang/Exception;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_3
    if-eqz v0, :cond_6

    .line 172
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_6
    throw p1
.end method
