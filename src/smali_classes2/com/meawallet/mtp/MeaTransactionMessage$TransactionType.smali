.class public final enum Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaTransactionMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TransactionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ATM_DEPOSIT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum ATM_TRANSFER:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum ATM_WITHDRAWAL:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum CASH_DISBURSEMENT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum PAYMENT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum PURCHASE:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum REFUND:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 208
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const-string v1, "PURCHASE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->PURCHASE:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    .line 210
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const-string v1, "REFUND"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->REFUND:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    .line 212
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const-string v1, "PAYMENT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->PAYMENT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    .line 214
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const-string v1, "ATM_WITHDRAWAL"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->ATM_WITHDRAWAL:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    .line 216
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const-string v1, "CASH_DISBURSEMENT"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->CASH_DISBURSEMENT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    .line 218
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const-string v1, "ATM_DEPOSIT"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->ATM_DEPOSIT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    .line 220
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const-string v1, "ATM_TRANSFER"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->ATM_TRANSFER:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    const/4 v0, 0x7

    .line 207
    new-array v0, v0, [Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->PURCHASE:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->REFUND:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->PAYMENT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->ATM_WITHDRAWAL:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->CASH_DISBURSEMENT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->ATM_DEPOSIT:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->ATM_TRANSFER:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->a:[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 207
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .locals 1

    .line 207
    const-class v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .locals 1

    .line 207
    sget-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->a:[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    return-object v0
.end method
