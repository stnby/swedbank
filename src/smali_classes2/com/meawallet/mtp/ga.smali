.class Lcom/meawallet/mtp/ga;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# instance fields
.field a:Lcom/meawallet/mtp/MeaAuthenticationResult;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method


# virtual methods
.method validate()V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/meawallet/mtp/ga;->a:Lcom/meawallet/mtp/MeaAuthenticationResult;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Authentication result is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
