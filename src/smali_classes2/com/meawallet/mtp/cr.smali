.class Lcom/meawallet/mtp/cr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/lang/String; = "cr"


# instance fields
.field final a:Lcom/meawallet/mtp/cv;

.field final b:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

.field final c:Lcom/meawallet/mtp/ds;

.field d:Z

.field private final f:Landroid/content/Context;

.field private final g:Lcom/mastercard/mpsdk/componentinterface/CardManager;

.field private final h:Lcom/meawallet/mtp/k;

.field private i:Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/dn;)V
    .locals 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 42
    iput-boolean v0, p0, Lcom/meawallet/mtp/cr;->d:Z

    .line 46
    invoke-interface {p1}, Lcom/meawallet/mtp/dn;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cr;->g:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    .line 47
    invoke-interface {p1}, Lcom/meawallet/mtp/dn;->e()Lcom/meawallet/mtp/k;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cr;->h:Lcom/meawallet/mtp/k;

    .line 48
    invoke-interface {p1}, Lcom/meawallet/mtp/dn;->d()Lcom/meawallet/mtp/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cr;->a:Lcom/meawallet/mtp/cv;

    .line 49
    invoke-interface {p1}, Lcom/meawallet/mtp/dn;->f()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cr;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    .line 50
    invoke-interface {p1}, Lcom/meawallet/mtp/dn;->g()Landroid/app/Application;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/cr;->f:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/meawallet/mtp/ds;

    invoke-interface {p1}, Lcom/meawallet/mtp/dn;->b()Lcom/meawallet/mtp/ci;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/meawallet/mtp/ds;-><init>(Lcom/meawallet/mtp/ci;)V

    iput-object v0, p0, Lcom/meawallet/mtp/cr;->c:Lcom/meawallet/mtp/ds;

    return-void
.end method

.method private c()Lcom/mastercard/mpsdk/componentinterface/Card;
    .locals 4

    .line 318
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->h:Lcom/meawallet/mtp/k;

    .line 6166
    iget-object v0, v0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    if-eqz v0, :cond_0

    return-object v0

    .line 326
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->g:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    invoke-static {}, Lcom/meawallet/mtp/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v0
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 328
    :catch_0
    sget-object v0, Lcom/meawallet/mtp/cr;->e:Ljava/lang/String;

    const/16 v1, 0x1f5

    const-string v2, "Failed to get card for fingerprint authentication."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/MeaCoreListener;)Lcom/meawallet/mtp/CvmMethod;
    .locals 3

    .line 58
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0

    .line 60
    invoke-static {v0}, Lcom/meawallet/mtp/n;->b(Lcom/meawallet/mtp/CdCvmType;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 62
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_1

    .line 64
    iget-object v1, p0, Lcom/meawallet/mtp/cr;->f:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 70
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/br;->c()V
    :try_end_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 77
    sget-object v1, Lcom/meawallet/mtp/cr;->e:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x1f5

    .line 79
    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-object v2

    :cond_1
    const/16 v1, 0x389

    .line 85
    invoke-static {p1, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    .line 89
    :cond_2
    :goto_0
    invoke-static {v0}, Lcom/meawallet/mtp/n;->a(Lcom/meawallet/mtp/CdCvmType;)Lcom/meawallet/mtp/CvmMethod;

    move-result-object p1

    return-object p1
.end method

.method final a()V
    .locals 2

    .line 143
    invoke-static {}, Lcom/meawallet/mtp/n;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2353
    iput-boolean v0, p0, Lcom/meawallet/mtp/cr;->d:Z

    .line 147
    sget-object v0, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/cr;->a(Lcom/meawallet/mtp/CdCvmType;)V

    return-void

    .line 149
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 3125
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->d:Lcom/meawallet/mtp/MeaAuthenticationListener;

    const/16 v1, 0x389

    .line 151
    invoke-static {v0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;I)V

    return-void
.end method

.method final declared-synchronized a(Landroid/app/FragmentManager;Lcom/meawallet/mtp/MeaListener;)V
    .locals 9

    monitor-enter p0

    .line 273
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/fp;->e()V

    .line 4336
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->i:Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;

    if-nez v0, :cond_0

    .line 4337
    new-instance v0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;

    invoke-direct {v0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/cr;->i:Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;

    .line 4340
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->i:Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;

    .line 5125
    iput-object p0, v0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->a:Lcom/meawallet/mtp/cr;

    .line 4341
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->i:Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;

    invoke-direct {p0}, Lcom/meawallet/mtp/cr;->c()Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v1

    .line 5133
    iput-object v1, v0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->c:Lcom/mastercard/mpsdk/componentinterface/Card;

    .line 4342
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->i:Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;

    .line 6129
    iput-object p2, v0, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->b:Lcom/meawallet/mtp/MeaListener;

    .line 4344
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->i:Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0x38a

    const/4 v2, 0x0

    .line 278
    :try_start_1
    invoke-virtual {p1}, Landroid/app/FragmentManager;->executePendingTransactions()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v3

    .line 283
    :try_start_2
    sget-object v4, Lcom/meawallet/mtp/cr;->e:Ljava/lang/String;

    const-string v5, "Failed FragmentManager.executePendingTransactions()"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v4, v3, v5, v6}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    new-instance v3, Lcom/meawallet/mtp/cy;

    const-string v4, "Failed to authenticated cardholder with fingerprint."

    invoke-direct {v3, v1, v4}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {p2, v3}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    .line 290
    :goto_0
    invoke-virtual {v0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->isResumed()Z

    move-result v3

    .line 291
    invoke-virtual {v0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->isVisible()Z

    move-result v4

    .line 292
    invoke-virtual {v0}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->isAdded()Z

    move-result v5

    const/4 v6, 0x3

    .line 294
    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v3, :cond_2

    if-nez v4, :cond_2

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    :try_start_3
    const-string v3, "authenticationFragment"

    .line 301
    invoke-virtual {v0, p1, v3}, Lcom/meawallet/mtp/FingerprintAuthenticationDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 308
    monitor-exit p0

    return-void

    :catch_1
    move-exception p1

    .line 303
    :try_start_4
    sget-object v0, Lcom/meawallet/mtp/cr;->e:Ljava/lang/String;

    const-string v3, "Failed to show FingerprintAuthenticationDialogFragment, because of incorrect fragment state."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    new-instance p1, Lcom/meawallet/mtp/cy;

    const-string v0, "Failed to authenticated cardholder with fingerprint."

    invoke-direct {p1, v1, v0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {p2, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 309
    monitor-exit p0

    return-void

    .line 296
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 272
    monitor-exit p0

    throw p1
.end method

.method final a(Lcom/meawallet/mtp/CdCvmType;)V
    .locals 3

    .line 99
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 1125
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->d:Lcom/meawallet/mtp/MeaAuthenticationListener;

    .line 101
    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 p1, 0x388

    .line 103
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;I)V

    return-void

    .line 108
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/cr$1;->a:[I

    invoke-virtual {p1}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/16 v1, 0x389

    const/16 v2, 0x17

    packed-switch p1, :pswitch_data_0

    .line 2202
    sget-object p1, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;)V

    return-void

    .line 1208
    :pswitch_0
    iget-object p1, p0, Lcom/meawallet/mtp/cr;->h:Lcom/meawallet/mtp/k;

    .line 2166
    iget-object p1, p1, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    if-eqz p1, :cond_1

    .line 1212
    new-instance v1, Lcom/meawallet/mtp/ct;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1214
    :cond_1
    new-instance v1, Lcom/meawallet/mtp/ct;

    invoke-static {}, Lcom/meawallet/mtp/k;->a()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    .line 1217
    :goto_0
    sget-object p1, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {v0, p1, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;Lcom/meawallet/mtp/MeaCard;)V

    return-void

    .line 1202
    :pswitch_1
    sget-object p1, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;)V

    return-void

    .line 119
    :pswitch_2
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v2, :cond_3

    .line 1192
    iget-object p1, p0, Lcom/meawallet/mtp/cr;->f:Landroid/content/Context;

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {p1, v1, v0}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1197
    sget-object p1, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;)V

    :cond_2
    return-void

    .line 122
    :cond_3
    invoke-static {v0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;I)V

    return-void

    .line 110
    :pswitch_3
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v2, :cond_7

    .line 1158
    iget-object p1, p0, Lcom/meawallet/mtp/cr;->f:Landroid/content/Context;

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {p1, v1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;)I

    move-result p1

    .line 1160
    invoke-static {p1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1162
    invoke-static {}, Lcom/meawallet/mtp/n;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0x387

    if-ne v1, p1, :cond_5

    .line 1166
    iget-object p1, p0, Lcom/meawallet/mtp/cr;->f:Landroid/content/Context;

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {p1, v1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;)I

    move-result p1

    .line 1168
    invoke-static {p1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1170
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;I)V

    return-void

    .line 1175
    :cond_4
    invoke-virtual {p0}, Lcom/meawallet/mtp/cr;->a()V

    return-void

    .line 1181
    :cond_5
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;I)V

    return-void

    .line 1186
    :cond_6
    sget-object p1, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;Lcom/meawallet/mtp/CdCvmType;)V

    return-void

    .line 113
    :cond_7
    invoke-static {v0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;I)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method final b()V
    .locals 4

    const-string v0, "device_unlock_key"

    const/4 v1, 0x1

    .line 222
    invoke-static {v0, v1}, Lcom/meawallet/mtp/br;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 224
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    if-eqz v0, :cond_1

    .line 228
    invoke-static {}, Lcom/meawallet/mtp/aq;->b()V

    .line 3348
    iget-boolean v0, p0, Lcom/meawallet/mtp/cr;->d:Z

    if-eqz v0, :cond_0

    .line 231
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/meawallet/mtp/fp;->a(J)V

    .line 232
    invoke-static {}, Lcom/meawallet/mtp/fp;->k()V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/cr;->c:Lcom/meawallet/mtp/ds;

    invoke-virtual {v0}, Lcom/meawallet/mtp/ds;->b()V

    return-void

    .line 237
    :cond_1
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 4125
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->d:Lcom/meawallet/mtp/MeaAuthenticationListener;

    .line 237
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x38a

    const-string v3, "Failed to authenticate cardholder with device unlock."

    invoke-direct {v1, v2, v3}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method
