.class final Lcom/meawallet/mtp/ReplenishCommand$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/ReplenishCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

.field final synthetic b:Lcom/meawallet/mtp/ReplenishCommand;

.field private c:[B

.field private d:[B

.field private e:[B


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/ReplenishCommand;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->b:Lcom/meawallet/mtp/ReplenishCommand;

    iput-object p2, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final exchangeDataWithLocalDek([B[B[B)V
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 332
    new-array p1, v0, [B

    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->c:[B

    goto :goto_0

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->b:Lcom/meawallet/mtp/ReplenishCommand;

    iget-object v1, v1, Lcom/meawallet/mtp/ReplenishCommand;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    invoke-direct {v2, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    iget-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {v1, v2, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    .line 337
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->c:[B

    :goto_0
    if-nez p2, :cond_1

    .line 341
    new-array p1, v0, [B

    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->d:[B

    goto :goto_1

    .line 345
    :cond_1
    iget-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->b:Lcom/meawallet/mtp/ReplenishCommand;

    iget-object p1, p1, Lcom/meawallet/mtp/ReplenishCommand;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance v1, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    invoke-direct {v1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    iget-object p2, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1, v1, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    .line 346
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->d:[B

    :goto_1
    if-nez p3, :cond_2

    .line 350
    new-array p1, v0, [B

    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->e:[B

    return-void

    .line 354
    :cond_2
    iget-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->b:Lcom/meawallet/mtp/ReplenishCommand;

    iget-object p1, p1, Lcom/meawallet/mtp/ReplenishCommand;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance p2, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    invoke-direct {p2, p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    iget-object p3, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-interface {p1, p2, p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object p1

    .line 355
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->e:[B

    return-void
.end method

.method public final getExchangedMdSessionKey()[B
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->c:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->c:[B

    return-object v0
.end method

.method public final getExchangedUmdSessionKey()[B
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->e:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->e:[B

    return-object v0
.end method

.method public final getExchangedUmdSingleUseKey()[B
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->d:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand$2;->d:[B

    return-object v0
.end method
