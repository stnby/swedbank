.class final enum Lcom/meawallet/mtp/FormFactor;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/FormFactor;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum JEWELRY:Lcom/meawallet/mtp/FormFactor;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum MEDIA_OR_GAMING_DEVICE:Lcom/meawallet/mtp/FormFactor;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum PHONE:Lcom/meawallet/mtp/FormFactor;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum TABLET_OR_EREADER:Lcom/meawallet/mtp/FormFactor;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum TAG:Lcom/meawallet/mtp/FormFactor;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum VEHICLE:Lcom/meawallet/mtp/FormFactor;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum WATCH_OR_WRISTBAND:Lcom/meawallet/mtp/FormFactor;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/FormFactor;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 6
    new-instance v0, Lcom/meawallet/mtp/FormFactor;

    const-string v1, "PHONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/FormFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->PHONE:Lcom/meawallet/mtp/FormFactor;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/FormFactor;

    const-string v1, "TABLET_OR_EREADER"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/FormFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->TABLET_OR_EREADER:Lcom/meawallet/mtp/FormFactor;

    .line 10
    new-instance v0, Lcom/meawallet/mtp/FormFactor;

    const-string v1, "WATCH_OR_WRISTBAND"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/FormFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->WATCH_OR_WRISTBAND:Lcom/meawallet/mtp/FormFactor;

    .line 12
    new-instance v0, Lcom/meawallet/mtp/FormFactor;

    const-string v1, "TAG"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/FormFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->TAG:Lcom/meawallet/mtp/FormFactor;

    .line 14
    new-instance v0, Lcom/meawallet/mtp/FormFactor;

    const-string v1, "JEWELRY"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/FormFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->JEWELRY:Lcom/meawallet/mtp/FormFactor;

    .line 16
    new-instance v0, Lcom/meawallet/mtp/FormFactor;

    const-string v1, "VEHICLE"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/FormFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->VEHICLE:Lcom/meawallet/mtp/FormFactor;

    .line 18
    new-instance v0, Lcom/meawallet/mtp/FormFactor;

    const-string v1, "MEDIA_OR_GAMING_DEVICE"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/FormFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->MEDIA_OR_GAMING_DEVICE:Lcom/meawallet/mtp/FormFactor;

    const/4 v0, 0x7

    .line 5
    new-array v0, v0, [Lcom/meawallet/mtp/FormFactor;

    sget-object v1, Lcom/meawallet/mtp/FormFactor;->PHONE:Lcom/meawallet/mtp/FormFactor;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/FormFactor;->TABLET_OR_EREADER:Lcom/meawallet/mtp/FormFactor;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/FormFactor;->WATCH_OR_WRISTBAND:Lcom/meawallet/mtp/FormFactor;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/FormFactor;->TAG:Lcom/meawallet/mtp/FormFactor;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/FormFactor;->JEWELRY:Lcom/meawallet/mtp/FormFactor;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/FormFactor;->VEHICLE:Lcom/meawallet/mtp/FormFactor;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/FormFactor;->MEDIA_OR_GAMING_DEVICE:Lcom/meawallet/mtp/FormFactor;

    aput-object v1, v0, v8

    sput-object v0, Lcom/meawallet/mtp/FormFactor;->a:[Lcom/meawallet/mtp/FormFactor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/FormFactor;
    .locals 1

    .line 5
    const-class v0, Lcom/meawallet/mtp/FormFactor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/FormFactor;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/FormFactor;
    .locals 1

    .line 5
    sget-object v0, Lcom/meawallet/mtp/FormFactor;->a:[Lcom/meawallet/mtp/FormFactor;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/FormFactor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/FormFactor;

    return-object v0
.end method
