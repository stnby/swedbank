.class public Lcom/meawallet/mtp/MeaTransactionLog;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Date;

.field private b:D

.field private c:Ljava/util/Currency;

.field private d:Lcom/meawallet/mtp/MeaTransactionLogType;

.field private e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;)V
    .locals 3

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getDate()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/meawallet/mtp/al;->c(J)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->a:Ljava/util/Date;

    .line 25
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getCurrencyCode()I

    move-result v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->getCurrencyByCode(I)Ljava/util/Currency;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->c:Ljava/util/Currency;

    .line 26
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getAmount()J

    move-result-wide v0

    iget-object v2, p0, Lcom/meawallet/mtp/MeaTransactionLog;->c:Ljava/util/Currency;

    invoke-static {v0, v1, v2}, Lcom/meawallet/mtp/a;->a(JLjava/util/Currency;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->b:D

    .line 27
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getCryptogramFormat()B

    move-result v0

    invoke-static {v0}, Lcom/meawallet/mtp/MeaTransactionLogType;->getByCode(I)Lcom/meawallet/mtp/MeaTransactionLogType;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->d:Lcom/meawallet/mtp/MeaTransactionLogType;

    .line 28
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/database/TransactionLog;->getTransactionId()[B

    move-result-object p1

    invoke-static {p1}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/MeaTransactionLog;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAmount()D
    .locals 2

    .line 46
    iget-wide v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->b:D

    return-wide v0
.end method

.method public getCurrency()Ljava/util/Currency;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->c:Ljava/util/Currency;

    return-object v0
.end method

.method public getDate()Ljava/util/Date;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->a:Ljava/util/Date;

    return-object v0
.end method

.method public getDisplayableAmountAndCurrency()Ljava/lang/String;
    .locals 3

    .line 74
    iget-wide v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->b:D

    iget-object v2, p0, Lcom/meawallet/mtp/MeaTransactionLog;->c:Ljava/util/Currency;

    invoke-static {v0, v1, v2}, Lcom/meawallet/mtp/a;->a(DLjava/util/Currency;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTransactionIdHexString()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/meawallet/mtp/MeaTransactionLogType;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLog;->d:Lcom/meawallet/mtp/MeaTransactionLogType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MeaTransactionLog[\tdate = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionLog;->a:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tamount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/meawallet/mtp/MeaTransactionLog;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, "\n\tcurrency = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionLog;->c:Ljava/util/Currency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttype = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionLog;->d:Lcom/meawallet/mtp/MeaTransactionLogType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttransactionIdHexString = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionLog;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
