.class public Lcom/meawallet/mtp/MeaTransactionDetails;
.super Lcom/meawallet/mtp/MeaTransactionMessage;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "merchantPostalCode"
    .end annotation
.end field

.field private b:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "installments"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionCountryCode"
    .end annotation
.end field

.field private d:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "comboCardAccountType"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/meawallet/mtp/MeaTransactionMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getComboCardAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionDetails;->d:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0
.end method

.method public getInstallments()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/meawallet/mtp/MeaTransactionDetails;->b:I

    return v0
.end method

.method public getMerchantPostalCode()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionDetails;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionCountryCode()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionDetails;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
