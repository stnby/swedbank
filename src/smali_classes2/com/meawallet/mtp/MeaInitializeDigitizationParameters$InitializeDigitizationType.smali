.class public final enum Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InitializeDigitizationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ENCRYPTED_PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

.field public static final enum PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

.field public static final enum SECRET:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 23
    new-instance v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    const-string v1, "SECRET"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->SECRET:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    .line 28
    new-instance v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    const-string v1, "PAN"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    .line 33
    new-instance v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    const-string v1, "ENCRYPTED_PAN"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->ENCRYPTED_PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    const/4 v0, 0x3

    .line 18
    new-array v0, v0, [Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    sget-object v1, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->SECRET:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->ENCRYPTED_PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->a:[Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;
    .locals 1

    .line 18
    const-class v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;
    .locals 1

    .line 18
    sget-object v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->a:[Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    return-object v0
.end method
