.class final Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/CryptoServiceNativeImpl;->getDatabaseStorageMacKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/CryptoServiceNativeImpl;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;->a:Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 225
    iput-boolean p1, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;->b:Z

    return-void
.end method


# virtual methods
.method public final abandonRollover()V
    .locals 1

    const/4 v0, 0x0

    .line 255
    iput-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;->b:Z

    return-void
.end method

.method public final getCurrentKeyId()[B
    .locals 1

    .line 229
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final isRolloverInProgress()Z
    .locals 1

    .line 250
    iget-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;->b:Z

    return v0
.end method

.method public final rolloverComplete()V
    .locals 3

    const/4 v0, 0x0

    .line 240
    iput-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;->b:Z

    .line 241
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->s()V

    .line 244
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "DATA_STORAGE_MAC_KEY_ID_PREF_KEY"

    .line 245
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->q()Ljava/lang/String;

    move-result-object v2

    .line 244
    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public final rolloverData([B[B[B)[B
    .locals 0

    .line 267
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Not implemented"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final startRollover()[B
    .locals 1

    const/4 v0, 0x1

    .line 234
    iput-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;->b:Z

    .line 235
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final wipeKey()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
