.class abstract Lcom/meawallet/mtp/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/ee;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/meawallet/mtp/s;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/ee;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/String; = "g"


# instance fields
.field a:Lcom/meawallet/mtp/ag;

.field b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Lcom/meawallet/mtp/ad;

.field final e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

.field h:Lcom/meawallet/mtp/ae;

.field i:Lcom/meawallet/mtp/ah;

.field private final k:Lcom/meawallet/mtp/fi;

.field private final l:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

.field private final m:I

.field private n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

.field private p:Ljava/lang/String;

.field private q:I

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-interface {p8}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;->getRetryCount()I

    move-result v0

    iput v0, p0, Lcom/meawallet/mtp/g;->m:I

    .line 62
    iget v0, p0, Lcom/meawallet/mtp/g;->m:I

    iput v0, p0, Lcom/meawallet/mtp/g;->q:I

    .line 64
    iput-object p1, p0, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    .line 65
    iput-object p2, p0, Lcom/meawallet/mtp/g;->o:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    .line 66
    iput-object p3, p0, Lcom/meawallet/mtp/g;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 67
    iput-object p4, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    .line 68
    iput-object p5, p0, Lcom/meawallet/mtp/g;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 69
    iput-object p7, p0, Lcom/meawallet/mtp/g;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    .line 70
    iput-object p8, p0, Lcom/meawallet/mtp/g;->l:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    .line 71
    iput-object p6, p0, Lcom/meawallet/mtp/g;->d:Lcom/meawallet/mtp/ad;

    .line 73
    sget-object p1, Lcom/meawallet/mtp/ag;->g:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 75
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/meawallet/mtp/g;->n:Ljava/util/HashMap;

    .line 76
    iget-object p1, p0, Lcom/meawallet/mtp/g;->n:Ljava/util/HashMap;

    const-string p2, "Content-Type"

    const-string p3, "application/json"

    invoke-virtual {p1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object p1, p0, Lcom/meawallet/mtp/g;->n:Ljava/util/HashMap;

    const-string p2, "Accept"

    const-string p3, "application/json"

    invoke-virtual {p1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 364
    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 366
    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 367
    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method abstract a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "[B)TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/String;)Lcom/meawallet/mtp/y;
    .locals 11

    const/4 v0, 0x1

    .line 92
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 94
    invoke-virtual {p0}, Lcom/meawallet/mtp/g;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    invoke-virtual {v1}, Lcom/meawallet/mtp/fi;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    .line 97
    new-array v1, v0, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/g;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v1

    .line 106
    invoke-static {v1}, Lcom/meawallet/mtp/g;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2122
    iget-object v3, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object v4, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    .line 2170
    new-array v5, v0, [Ljava/lang/Object;

    aput-object v4, v5, v2

    .line 2172
    invoke-virtual {v3, v4}, Lcom/meawallet/mtp/fi;->a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 3047
    iget v4, v3, Lcom/meawallet/mtp/ab;->a:I

    add-int/2addr v4, v0

    iput v4, v3, Lcom/meawallet/mtp/ab;->a:I

    .line 3049
    new-array v4, v0, [Ljava/lang/Object;

    iget v5, v3, Lcom/meawallet/mtp/ab;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 2177
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MPA to CMS counter Increased to: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4023
    iget v3, v3, Lcom/meawallet/mtp/ab;->a:I

    .line 2177
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2124
    :cond_1
    iget-object v3, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object v4, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    .line 4152
    new-array v5, v0, [Ljava/lang/Object;

    aput-object v4, v5, v2

    .line 4154
    invoke-virtual {v3, v4}, Lcom/meawallet/mtp/fi;->a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 5023
    iget v3, v3, Lcom/meawallet/mtp/ab;->a:I

    move v10, v3

    goto :goto_0

    :cond_2
    const/4 v10, 0x0

    .line 4164
    :goto_0
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    .line 5131
    iget-object v4, p0, Lcom/meawallet/mtp/g;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 5132
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v5

    .line 5133
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v6

    .line 5134
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    iget-object v8, p0, Lcom/meawallet/mtp/g;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object p1, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object v3, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    .line 5136
    invoke-virtual {p1, v3}, Lcom/meawallet/mtp/fi;->c(Ljava/lang/String;)Lcom/meawallet/mtp/aa;

    move-result-object v9

    .line 5131
    invoke-interface/range {v4 .. v10}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->buildRemoteServiceRequest(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;I)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;

    move-result-object p1

    .line 5139
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;->getEncryptedData()[B

    move-result-object p1

    const/4 v3, 0x2

    invoke-static {p1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    .line 117
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getKeySetId()Ljava/lang/String;

    move-result-object v1

    .line 5143
    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    aput-object v1, v3, v0

    .line 5145
    iget-object v0, p0, Lcom/meawallet/mtp/g;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v2, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object v3, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/meawallet/mtp/fi;->c(Ljava/lang/String;)Lcom/meawallet/mtp/aa;

    move-result-object v2

    .line 5146
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    iget-object v4, p0, Lcom/meawallet/mtp/g;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 5145
    invoke-interface {v0, v2, v3, v4}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->calculateAuthenticationCode(Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;)[B

    move-result-object v0

    .line 5149
    new-instance v2, Lcom/meawallet/mtp/y;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    iget-object v3, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    invoke-direct {v2, v1, v0, p1, v3}, Lcom/meawallet/mtp/y;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 108
    :cond_3
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string v0, "Failed to encrypt request data, Mobile keys are missing. MOBILE_KEYS_MISSING"

    invoke-direct {p1, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    .locals 9

    const/4 v0, 0x1

    .line 155
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v1, 0x0

    .line 157
    iput-object v1, p0, Lcom/meawallet/mtp/g;->c:Ljava/lang/String;

    .line 158
    sget-object v3, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    .line 5317
    iput-object v3, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 159
    iput-object v1, p0, Lcom/meawallet/mtp/g;->h:Lcom/meawallet/mtp/ae;

    .line 160
    iput-object v1, p0, Lcom/meawallet/mtp/g;->i:Lcom/meawallet/mtp/ah;

    .line 162
    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/meawallet/mtp/g;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 164
    iget-object v3, p0, Lcom/meawallet/mtp/g;->o:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    invoke-virtual {p0}, Lcom/meawallet/mtp/g;->m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    move-result-object v4

    invoke-virtual {p0}, Lcom/meawallet/mtp/g;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/meawallet/mtp/g;->n:Ljava/util/HashMap;

    invoke-interface {v3, v4, v5, p2, v6}, Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;->execute(Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 167
    new-array v3, v0, [Ljava/lang/Object;

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    :cond_0
    if-eqz p2, :cond_a

    .line 173
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_1

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    const/16 v5, 0xcc

    if-ne v3, v5, :cond_a

    .line 176
    :cond_1
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result v3

    if-ne v3, v4, :cond_8

    .line 177
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getContent()[B

    move-result-object v3

    if-eqz v3, :cond_8

    .line 178
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getContent()[B

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_8

    .line 180
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getContent()[B

    move-result-object p2

    .line 6246
    invoke-static {p1, p2}, Lcom/meawallet/mtp/z;->a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/z;

    move-result-object p2

    .line 7034
    iget-object v3, p2, Lcom/meawallet/mtp/z;->a:Ljava/lang/String;

    if-nez v3, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0x25a

    if-eqz v3, :cond_7

    .line 10030
    iget-object p2, p2, Lcom/meawallet/mtp/z;->c:Ljava/lang/String;

    .line 6258
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/Utils;->decodeBase64([B)[B

    move-result-object p2

    const/4 v3, 0x3

    .line 6260
    invoke-static {p2, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    .line 10295
    invoke-static {v3}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x10

    .line 10297
    invoke-static {v3, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 6262
    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 11285
    iget-object v5, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object v6, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    .line 12120
    new-array v7, v0, [Ljava/lang/Object;

    aput-object v6, v7, v2

    .line 12122
    invoke-virtual {v5, v6}, Lcom/meawallet/mtp/fi;->a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 13027
    iget v5, v5, Lcom/meawallet/mtp/ab;->b:I

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    .line 12132
    :goto_1
    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    .line 11287
    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    if-ge v5, v3, :cond_6

    .line 6266
    iget-object v5, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object v6, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    .line 13138
    new-array v7, v0, [Ljava/lang/Object;

    aput-object v6, v7, v2

    .line 13140
    invoke-virtual {v5, v6}, Lcom/meawallet/mtp/fi;->a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 14053
    iput v3, v5, Lcom/meawallet/mtp/ab;->b:I

    .line 6268
    :cond_4
    iget-object v3, p0, Lcom/meawallet/mtp/g;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v3

    .line 6270
    invoke-static {v3}, Lcom/meawallet/mtp/g;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 6276
    iget-object v5, p0, Lcom/meawallet/mtp/g;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedTransportKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v6

    .line 6277
    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedMacKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v3

    new-instance v7, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;

    invoke-direct {v7, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;-><init>([B)V

    iget-object p2, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object v8, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    .line 6279
    invoke-virtual {p2, v8}, Lcom/meawallet/mtp/fi;->c(Ljava/lang/String;)Lcom/meawallet/mtp/aa;

    move-result-object p2

    .line 6276
    invoke-interface {v5, v6, v3, v7, p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->decryptRemoteServiceResponse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;)[B

    move-result-object p2

    .line 182
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/Utils;->fromByteArrayToHexString([B)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    .line 184
    invoke-virtual {p0, p1, p2}, Lcom/meawallet/mtp/g;->a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;

    move-result-object p1

    .line 186
    new-array p2, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/meawallet/mtp/s;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p2, v2

    .line 15034
    iget-object p2, p1, Lcom/meawallet/mtp/s;->c:Ljava/lang/String;

    if-eqz p2, :cond_9

    .line 189
    sget-object p2, Lcom/meawallet/mtp/g;->j:Ljava/lang/String;

    const-string v3, "CMS-D error code = %s"

    new-array v0, v0, [Ljava/lang/Object;

    .line 16034
    iget-object v5, p1, Lcom/meawallet/mtp/s;->c:Ljava/lang/String;

    aput-object v5, v0, v2

    .line 189
    invoke-static {p2, v4, v3, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 191
    sget-object p2, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 17034
    iget-object v0, p1, Lcom/meawallet/mtp/s;->c:Ljava/lang/String;

    .line 17038
    iget-object p1, p1, Lcom/meawallet/mtp/s;->d:Ljava/lang/String;

    .line 191
    invoke-virtual {p0, p2, v0, p1, v1}, Lcom/meawallet/mtp/g;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 6272
    :cond_5
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "Communication parameters are missing, Mobile keys are missing. MOBILE_KEYS_MISSING"

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 11290
    :cond_6
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    const-string p2, "CMS-D to MPA Counter mismatch"

    const-string v0, "COUNTER_MISMATCH"

    invoke-direct {p1, p2, v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1

    .line 6249
    :cond_7
    sget-object p1, Lcom/meawallet/mtp/g;->j:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 8022
    iget-object v3, p2, Lcom/meawallet/mtp/z;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 8026
    iget-object v2, p2, Lcom/meawallet/mtp/z;->b:Ljava/lang/String;

    aput-object v2, v1, v0

    const-string v0, "decryptResponse(): Error code received with CmsDResponse; errorCode: %s, errorDescription: %s"

    .line 6249
    invoke-static {p1, v4, v0, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 6253
    new-instance p1, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;

    .line 9026
    iget-object v0, p2, Lcom/meawallet/mtp/z;->b:Ljava/lang/String;

    .line 10022
    iget-object p2, p2, Lcom/meawallet/mtp/z;->a:Ljava/lang/String;

    .line 6253
    invoke-direct {p1, v0, p2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw p1

    :cond_8
    move-object p1, v1

    .line 200
    :cond_9
    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/g;->a(Lcom/meawallet/mtp/s;)V

    goto/16 :goto_7

    :cond_a
    if-eqz p2, :cond_b

    .line 203
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result p1

    const/16 v3, 0x191

    if-ne p1, v3, :cond_b

    .line 205
    sget-object p1, Lcom/meawallet/mtp/ae;->b:Lcom/meawallet/mtp/ae;

    iput-object p1, p0, Lcom/meawallet/mtp/g;->h:Lcom/meawallet/mtp/ae;

    goto/16 :goto_5

    .line 206
    :cond_b
    iget p1, p0, Lcom/meawallet/mtp/g;->q:I

    if-lez p1, :cond_f

    if-eqz p2, :cond_f

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result p1

    const/16 v3, 0x12e

    const/16 v4, 0x1f7

    if-eq p1, v3, :cond_d

    const/16 v3, 0x1f4

    if-eq p1, v3, :cond_d

    const/16 v3, 0x198

    if-eq p1, v3, :cond_d

    const/16 v3, 0x1f8

    if-eq p1, v3, :cond_d

    if-eq p1, v4, :cond_d

    const/16 v3, 0x453

    if-ne p1, v3, :cond_c

    goto :goto_2

    :cond_c
    const/4 p1, 0x0

    goto :goto_3

    :cond_d
    :goto_2
    const/4 p1, 0x1

    :goto_3
    if-eqz p1, :cond_f

    .line 17334
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result p1

    if-ne p1, v4, :cond_e

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getRetryAfterValue()I

    move-result p1

    if-eqz p1, :cond_e

    .line 17336
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getRetryAfterValue()I

    move-result p1

    mul-int/lit16 p1, p1, 0x3e8

    .line 17338
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    goto :goto_4

    .line 17343
    :cond_e
    iget-object p1, p0, Lcom/meawallet/mtp/g;->l:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;->getRetryIntervals()[I

    move-result-object p1

    iget v3, p0, Lcom/meawallet/mtp/g;->m:I

    iget v4, p0, Lcom/meawallet/mtp/g;->q:I

    sub-int/2addr v3, v4

    aget p1, p1, v3

    mul-int/lit16 p1, p1, 0x3e8

    .line 17345
    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    .line 208
    :goto_4
    iput p1, p0, Lcom/meawallet/mtp/g;->r:I

    .line 210
    iget p1, p0, Lcom/meawallet/mtp/g;->q:I

    sub-int/2addr p1, v0

    iput p1, p0, Lcom/meawallet/mtp/g;->q:I

    .line 212
    sget-object p1, Lcom/meawallet/mtp/ae;->a:Lcom/meawallet/mtp/ae;

    iput-object p1, p0, Lcom/meawallet/mtp/g;->h:Lcom/meawallet/mtp/ae;

    goto :goto_5

    .line 214
    :cond_f
    sget-object p1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object p1, p0, Lcom/meawallet/mtp/g;->h:Lcom/meawallet/mtp/ae;

    .line 217
    :goto_5
    iget-object p1, p0, Lcom/meawallet/mtp/g;->h:Lcom/meawallet/mtp/ae;

    if-eqz p2, :cond_10

    .line 218
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;->getResponseCode()I

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_6

    :cond_10
    const-string p2, "NETWORK_ERROR"

    :goto_6
    const-string v0, "HTTP Error"

    .line 217
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/meawallet/mtp/g;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 223
    :goto_7
    invoke-virtual {p0}, Lcom/meawallet/mtp/g;->j()Z

    move-result p1

    if-eqz p1, :cond_11

    .line 224
    iget-object p1, p0, Lcom/meawallet/mtp/g;->k:Lcom/meawallet/mtp/fi;

    iget-object p2, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/meawallet/mtp/fi;->b(Ljava/lang/String;)V

    :cond_11
    return-void
.end method

.method abstract a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public final a(Lcom/meawallet/mtp/ag;)V
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    return-void
.end method

.method abstract a(Lcom/meawallet/mtp/s;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public final b()Lcom/meawallet/mtp/ag;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    return-object v0
.end method

.method public final c()Lcom/meawallet/mtp/ae;
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/meawallet/mtp/g;->h:Lcom/meawallet/mtp/ae;

    return-object v0
.end method

.method public final d()Lcom/meawallet/mtp/ah;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/meawallet/mtp/g;->i:Lcom/meawallet/mtp/ah;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/meawallet/mtp/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    .line 307
    iput v0, p0, Lcom/meawallet/mtp/g;->q:I

    return-void
.end method

.method public final g()I
    .locals 1

    .line 312
    iget v0, p0, Lcom/meawallet/mtp/g;->r:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/meawallet/mtp/g;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getRemoteManagementServiceUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/meawallet/mtp/g;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract i()Ljava/lang/String;
.end method

.method public j()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/meawallet/mtp/g;->p:Ljava/lang/String;

    return-object v0
.end method
