.class public Lcom/meawallet/mtp/MeaTransactionMessage;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;,
        Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "MeaTransactionMessage"


# instance fields
.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenUniqueReference"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "recordId"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionIdentifier"
    .end annotation
.end field

.field private e:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionType"
    .end annotation
.end field

.field private f:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "amount"
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currencyCode"
    .end annotation
.end field

.field private h:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "authorizationStatus"
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionTimestamp"
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "merchantName"
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "merchantType"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAmount()Ljava/lang/Double;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->f:Ljava/lang/Double;

    return-object v0
.end method

.method public getAuthorizationStatus()Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->h:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    return-object v0
.end method

.method public getCardId()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrency()Ljava/util/Currency;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantName()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantType()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getRecordId()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionDate()Ljava/util/Date;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/al;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 174
    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTransactionIdentifier()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionTimestamp()Ljava/lang/String;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionType()Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->e:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MeaTransactionMessage{\n\tcardId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tamount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->f:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tauthorizationStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->h:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tcurrencyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tmerchantName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tmerchantType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\trecordId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttransactionIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttransactionTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttransactionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionMessage;->e:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
