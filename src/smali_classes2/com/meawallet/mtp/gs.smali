.class Lcom/meawallet/mtp/gs;
.super Lcom/meawallet/mtp/gx;
.source "SourceFile"


# static fields
.field private static final transient e:Ljava/lang/String; = "gs"


# instance fields
.field transient a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

.field private f:Lcom/meawallet/mtp/eu;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "rnsInfo"
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "publicKeyFingerprintWallet"
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "publicKeyFingerprintCms"
    .end annotation
.end field

.field private i:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "rgkWallet"
    .end annotation
.end field

.field private j:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "rgkCms"
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deviceFingerprint"
    .end annotation
.end field

.field private l:Lcom/meawallet/mtp/ap;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deviceInfo"
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "consumerLanguage"
    .end annotation
.end field

.field private n:Lcom/meawallet/mtp/CvmMethod;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cvmMethod"
    .end annotation
.end field

.field private final transient o:Lcom/meawallet/mtp/dq;

.field private final transient p:Lcom/meawallet/mtp/ci;

.field private final transient q:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/dq;Lcom/meawallet/mtp/ci;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;Ljava/lang/String;Lcom/meawallet/mtp/ap;Lcom/meawallet/mtp/CvmMethod;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, v0}, Lcom/meawallet/mtp/gx;-><init>(Z)V

    .line 61
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/meawallet/mtp/eu;

    invoke-direct {v0, p4}, Lcom/meawallet/mtp/eu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/meawallet/mtp/gs;->f:Lcom/meawallet/mtp/eu;

    :cond_0
    const-string p4, "F397AEC7A252FB813AB80E7F0085CBD8071D515E3CD8C9FC4167E95AB91F7B6D"

    .line 65
    iput-object p4, p0, Lcom/meawallet/mtp/gs;->g:Ljava/lang/String;

    const-string p4, "2DC6385290121D2B7A6DB7FAAE1EB56C2C9F2D8858687346A68FF8908E4035DE"

    .line 66
    iput-object p4, p0, Lcom/meawallet/mtp/gs;->h:Ljava/lang/String;

    .line 67
    iput-object p5, p0, Lcom/meawallet/mtp/gs;->l:Lcom/meawallet/mtp/ap;

    .line 68
    iput-object p6, p0, Lcom/meawallet/mtp/gs;->n:Lcom/meawallet/mtp/CvmMethod;

    .line 69
    iput-object p7, p0, Lcom/meawallet/mtp/gs;->m:Ljava/lang/String;

    .line 71
    iput-object p1, p0, Lcom/meawallet/mtp/gs;->o:Lcom/meawallet/mtp/dq;

    .line 72
    iput-object p2, p0, Lcom/meawallet/mtp/gs;->p:Lcom/meawallet/mtp/ci;

    .line 73
    iput-object p3, p0, Lcom/meawallet/mtp/gs;->q:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .line 82
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->o:Lcom/meawallet/mtp/dq;

    .line 1040
    iget-object v0, v0, Lcom/meawallet/mtp/dq;->a:Lcom/meawallet/mtp/ai;

    invoke-interface {v0}, Lcom/meawallet/mtp/ai;->a()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    .line 82
    iput-object v0, p0, Lcom/meawallet/mtp/gs;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 84
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v0, :cond_4

    .line 89
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->o:Lcom/meawallet/mtp/dq;

    iget-object v1, p0, Lcom/meawallet/mtp/gs;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    const-string v2, "F397AEC7A252FB813AB80E7F0085CBD8071D515E3CD8C9FC4167E95AB91F7B6D"

    .line 90
    invoke-static {v2}, Lcom/meawallet/mtp/h;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    .line 1046
    iget-object v0, v0, Lcom/meawallet/mtp/dq;->a:Lcom/meawallet/mtp/ai;

    invoke-interface {v0, v1, v2}, Lcom/meawallet/mtp/ai;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 97
    iput-object v0, p0, Lcom/meawallet/mtp/gs;->i:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 101
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->q:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

    const-string v1, "2DC6385290121D2B7A6DB7FAAE1EB56C2C9F2D8858687346A68FF8908E4035DE"

    .line 102
    invoke-static {v1}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x0

    .line 101
    invoke-interface {v0, v1, v2}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;->getRegistrationRequestData([BLcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RegistrationRequestParameters;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 110
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RegistrationRequestParameters;->getRandomGeneratedKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;->getEncryptedData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 117
    iput-object v0, p0, Lcom/meawallet/mtp/gs;->j:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 119
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->p:Lcom/meawallet/mtp/ci;

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    iput-object v0, p0, Lcom/meawallet/mtp/gs;->k:Ljava/lang/String;

    .line 128
    invoke-super {p0}, Lcom/meawallet/mtp/gx;->a()V

    return-void

    .line 123
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Device fingerprint is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Signed mobile RGK is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Registration request parameters are null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Signed wallet RGK is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "Wallet RGK is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final b()V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->f:Lcom/meawallet/mtp/eu;

    if-eqz v0, :cond_11

    .line 139
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->f:Lcom/meawallet/mtp/eu;

    .line 2018
    iget-object v0, v0, Lcom/meawallet/mtp/eu;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 141
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 143
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 145
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->i:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v0, :cond_d

    .line 147
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->j:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v0, :cond_c

    .line 149
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 151
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->l:Lcom/meawallet/mtp/ap;

    if-eqz v0, :cond_a

    .line 153
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->l:Lcom/meawallet/mtp/ap;

    .line 2063
    iget-object v1, v0, Lcom/meawallet/mtp/ap;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2065
    iget-object v1, v0, Lcom/meawallet/mtp/ap;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2067
    iget-object v1, v0, Lcom/meawallet/mtp/ap;->c:Lcom/meawallet/mtp/FormFactor;

    if-eqz v1, :cond_7

    .line 2069
    iget-object v1, v0, Lcom/meawallet/mtp/ap;->d:Lcom/meawallet/mtp/StorageTechnology;

    if-eqz v1, :cond_6

    .line 2071
    iget-object v1, v0, Lcom/meawallet/mtp/ap;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2073
    iget-object v1, v0, Lcom/meawallet/mtp/ap;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2075
    iget-object v0, v0, Lcom/meawallet/mtp/ap;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 155
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 161
    iget-object v0, p0, Lcom/meawallet/mtp/gs;->n:Lcom/meawallet/mtp/CvmMethod;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "CVM method is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Consumer language doesn\'t match ISO-639-1 standard"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Consumer language is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2075
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "NFC capable is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2073
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "OS version is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2071
    :cond_5
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "OS name is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2069
    :cond_6
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Storage technology is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2067
    :cond_7
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Form factor is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2065
    :cond_8
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Serial number is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2063
    :cond_9
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Device name is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_a
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Device info is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_b
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Device fingerprint is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_c
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Mobile random generated key is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_d
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Wallet random generated key is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_e
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Public key fingerprint mobile is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_f
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Public key fingerprint wallet is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2018
    :cond_10
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Gcm registration id is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_11
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "RNS info is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
