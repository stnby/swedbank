.class final Lcom/meawallet/mtp/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[C

.field private static final b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    .line 5
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/e;->a:[C

    const/16 v0, 0x80

    .line 7
    new-array v0, v0, [I

    sput-object v0, Lcom/meawallet/mtp/e;->b:[I

    const/4 v0, 0x0

    .line 10
    :goto_0
    sget-object v1, Lcom/meawallet/mtp/e;->a:[C

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 11
    sget-object v1, Lcom/meawallet/mtp/e;->b:[I

    sget-object v2, Lcom/meawallet/mtp/e;->a:[C

    aget-char v2, v2, v0

    aput v0, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static a(Ljava/lang/String;)[B
    .locals 8

    const-string v0, "=="

    .line 28
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const-string v0, "="

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 29
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x4

    sub-int/2addr v3, v0

    new-array v0, v3, [B

    const/4 v3, 0x0

    .line 33
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 34
    sget-object v4, Lcom/meawallet/mtp/e;->b:[I

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aget v4, v4, v5

    .line 35
    sget-object v5, Lcom/meawallet/mtp/e;->b:[I

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aget v5, v5, v6

    add-int/lit8 v6, v3, 0x1

    shl-int/2addr v4, v2

    shr-int/lit8 v7, v5, 0x4

    or-int/2addr v4, v7

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    .line 36
    aput-byte v4, v0, v3

    .line 38
    array-length v3, v0

    if-lt v6, v3, :cond_2

    return-object v0

    .line 43
    :cond_2
    sget-object v3, Lcom/meawallet/mtp/e;->b:[I

    add-int/lit8 v4, v1, 0x2

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aget v3, v3, v4

    add-int/lit8 v4, v6, 0x1

    shl-int/lit8 v5, v5, 0x4

    shr-int/lit8 v7, v3, 0x2

    or-int/2addr v5, v7

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    .line 44
    aput-byte v5, v0, v6

    .line 46
    array-length v5, v0

    if-lt v4, v5, :cond_3

    return-object v0

    .line 51
    :cond_3
    sget-object v5, Lcom/meawallet/mtp/e;->b:[I

    add-int/lit8 v6, v1, 0x3

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aget v5, v5, v6

    add-int/lit8 v6, v4, 0x1

    shl-int/lit8 v3, v3, 0x6

    or-int/2addr v3, v5

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    .line 52
    aput-byte v3, v0, v4

    add-int/lit8 v1, v1, 0x4

    move v3, v6

    goto :goto_1

    :cond_4
    return-object v0
.end method
