.class public Lcom/meawallet/mtp/DeviceUnlockService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String; = "DeviceUnlockService"


# instance fields
.field a:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 11
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/meawallet/mtp/DeviceUnlockService;->a:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    .line 24
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 26
    iget-object v0, p0, Lcom/meawallet/mtp/DeviceUnlockService;->a:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/meawallet/mtp/DeviceUnlockService;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/DeviceUnlockService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 30
    iput-object v0, p0, Lcom/meawallet/mtp/DeviceUnlockService;->a:Landroid/content/BroadcastReceiver;

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    const/4 p2, 0x1

    const/4 p3, 0x2

    if-eqz p1, :cond_1

    .line 39
    new-array v0, p3, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "com.meawallet.mtp.DeviceUnlockService.EXTRA_STATE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p2

    const-string v0, "com.meawallet.mtp.DeviceUnlockService.ACTION_DEVICE_UNLOCK"

    .line 41
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.action.USER_PRESENT"

    const-string v1, "com.meawallet.mtp.DeviceUnlockService.EXTRA_STATE"

    .line 42
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/meawallet/mtp/DeviceUnlockService;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/meawallet/mtp/dx;->a(Landroid/content/Context;)V

    .line 46
    invoke-static {}, Lcom/meawallet/mtp/aq;->b()V

    goto :goto_0

    :cond_0
    const-string v0, "com.meawallet.mtp.DeviceUnlockService.ACTION_DEVICE_LOCK"

    .line 48
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "android.intent.action.SCREEN_OFF"

    const-string v1, "com.meawallet.mtp.DeviceUnlockService.EXTRA_STATE"

    .line 49
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 50
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/meawallet/mtp/DeviceUnlockService;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/meawallet/mtp/dx;->a(Landroid/content/Context;)V

    .line 53
    invoke-static {}, Lcom/meawallet/mtp/fp;->h()V

    .line 57
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/meawallet/mtp/DeviceUnlockService;->a:Landroid/content/BroadcastReceiver;

    if-nez p1, :cond_2

    .line 61
    new-instance p1, Lcom/meawallet/mtp/DeviceUnlockReceiver;

    invoke-direct {p1}, Lcom/meawallet/mtp/DeviceUnlockReceiver;-><init>()V

    iput-object p1, p0, Lcom/meawallet/mtp/DeviceUnlockService;->a:Landroid/content/BroadcastReceiver;

    .line 62
    new-instance p1, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-direct {p1, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_OFF"

    .line 63
    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/meawallet/mtp/DeviceUnlockService;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, p1}, Lcom/meawallet/mtp/DeviceUnlockService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 67
    :cond_2
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1a

    if-lt p1, v0, :cond_3

    return p3

    :cond_3
    return p2
.end method
