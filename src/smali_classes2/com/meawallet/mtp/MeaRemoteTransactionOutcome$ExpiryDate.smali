.class public Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ExpiryDate"
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;Ljava/lang/String;)V
    .locals 2

    .line 134
    iput-object p1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;->a:Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_0

    .line 136
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    const/4 v1, 0x2

    .line 137
    invoke-virtual {p2, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;->d:Ljava/lang/String;

    const/4 p1, 0x4

    .line 138
    invoke-virtual {p2, v1, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;->c:Ljava/lang/String;

    .line 139
    invoke-virtual {p2, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;->b:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public getDay()Ljava/lang/String;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getMonth()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getYear()Ljava/lang/String;
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;->d:Ljava/lang/String;

    return-object v0
.end method
