.class Lcom/meawallet/mtp/CryptoServiceNativeImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;
.implements Lcom/meawallet/mtp/ai;


# static fields
.field private static final a:Ljava/lang/String; = "CryptoServiceNativeImpl"

.field private static h:Lcom/meawallet/mtp/CryptoServiceNativeImpl;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

.field private d:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

.field private e:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

.field private f:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

.field private final g:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "mtp-native-1.8.1"

    .line 42
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 54
    sput-object v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->h:Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->g:Landroid/content/Context;

    .line 59
    iget-object p1, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->g:Landroid/content/Context;

    .line 2966
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a1(Landroid/content/Context;)V

    .line 61
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object p1

    const-string v0, "LOCAL_DEK_ID_PREF_KEY"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 62
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "DATA_STORAGE_DEK_ID_PREF_KEY"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v1

    const-string v2, "DATA_STORAGE_MAC_KEY_ID_PREF_KEY"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v2

    const-string v3, "RM_KEK_KEY_ID_PREF_KEY"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 65
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v3

    const-string v4, "WALLET_DEK_ID_PREF_KEY"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3962
    invoke-static {p1, v0, v1, v2, v3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic A()Ljava/lang/String;
    .locals 1

    .line 29016
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic B()V
    .locals 0

    .line 29134
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ah()V

    return-void
.end method

.method private static a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 1

    .line 891
    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    .line 892
    invoke-static {p0}, Lcom/meawallet/mtp/h;->a([B)V

    return-object v0
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lcom/meawallet/mtp/CryptoServiceNativeImpl;
    .locals 2

    const-class v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    monitor-enter v0

    .line 71
    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->h:Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    if-nez v1, :cond_0

    .line 72
    new-instance v1, Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    invoke-direct {v1, p0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->h:Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    .line 75
    :cond_0
    sget-object p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->h:Lcom/meawallet/mtp/CryptoServiceNativeImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    .line 70
    monitor-exit v0

    throw p0
.end method

.method static native a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;[B)[B
    .locals 0

    .line 23097
    invoke-static {p0, p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->aa(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object p0

    return-object p0
.end method

.method private static a([B[B[B[BI)[B
    .locals 0

    .line 1194
    invoke-static {p0, p1, p2, p3, p4}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ao([B[B[B[BI)[B

    move-result-object p0

    return-object p0
.end method

.method static native a1(Landroid/content/Context;)V
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native a2([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native aa(Ljava/lang/String;Ljava/lang/String;[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ab(Ljava/lang/String;Ljava/lang/String;[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ac(Ljava/lang/String;Ljava/lang/String;[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ad(Ljava/lang/String;Ljava/lang/String;[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ae()V
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native af()V
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ag()V
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ah()V
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ai()V
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native aj([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ak([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native al([B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native am([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native an()[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ao([B[B[B[BI)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ap([B[B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native aq([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ar([B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native as([B[B[B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native at([B)I
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native au([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native av([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native aw()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ax([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ay([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native az([BI)Z
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic b(Ljava/lang/String;Ljava/lang/String;[B)[B
    .locals 0

    .line 26104
    invoke-static {p0, p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ab(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object p0

    return-object p0
.end method

.method static native b([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native ba([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native bb([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native bc([B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native be([B[B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method private c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
    .locals 4

    .line 902
    array-length v0, p1

    div-int/lit8 v0, v0, 0x2

    .line 904
    new-array v1, v0, [B

    .line 905
    new-array v2, v0, [B

    const/4 v3, 0x0

    .line 907
    invoke-static {p1, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 908
    invoke-static {p1, v0, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 909
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->d([B)V

    .line 911
    new-instance p1, Lcom/meawallet/mtp/CryptoServiceNativeImpl$6;

    invoke-direct {p1, p0, v1, v2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl$6;-><init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;[B[B)V

    return-object p1
.end method

.method static native c()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic c(Ljava/lang/String;Ljava/lang/String;[B)[B
    .locals 0

    .line 28111
    invoke-static {p0, p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ac(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object p0

    return-object p0
.end method

.method static native d()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method private static d([B)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    .line 933
    :cond_0
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 935
    aput-byte v1, p0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic d(Ljava/lang/String;Ljava/lang/String;[B)[B
    .locals 0

    .line 30118
    invoke-static {p0, p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ad(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object p0

    return-object p0
.end method

.method static native e()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native f()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native g()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native h()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native i()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native j()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native k()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native l()Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static native m([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    .line 21980
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native n([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    .line 21984
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native o([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic p()V
    .locals 0

    .line 22122
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ae()V

    return-void
.end method

.method static native p(Ljava/lang/String;[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic q()Ljava/lang/String;
    .locals 1

    .line 23988
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native q([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic r()Ljava/lang/String;
    .locals 1

    .line 23992
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native r([BLjava/lang/String;)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic s()V
    .locals 0

    .line 24138
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ai()V

    return-void
.end method

.method static native s([B[B)Z
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic t()Ljava/lang/String;
    .locals 1

    .line 24996
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native t([B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic u()Ljava/lang/String;
    .locals 1

    .line 25000
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native u([B[B[B[B[B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic v()V
    .locals 0

    .line 25126
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->af()V

    return-void
.end method

.method static native v([B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic w()Ljava/lang/String;
    .locals 1

    .line 27004
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native w([B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic x()Ljava/lang/String;
    .locals 1

    .line 27008
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native x([B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic y()V
    .locals 0

    .line 27130
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ag()V

    return-void
.end method

.method static native y([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method

.method static synthetic z()Ljava/lang/String;
    .locals 1

    .line 29012
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static native z([B[B[B)[B
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end method


# virtual methods
.method public final a()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 1

    .line 17179
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->an()[B

    move-result-object v0

    .line 789
    invoke-static {v0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 0

    .line 869
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    .line 21259
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ax([B)[B

    move-result-object p1

    .line 871
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 0

    .line 797
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    .line 17222
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ar([B[B)[B

    move-result-object p1

    .line 797
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 2

    .line 850
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p3

    .line 21183
    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->bb([B[B[B)[B

    move-result-object p1

    .line 853
    array-length p2, p1

    const/4 p3, 0x1

    if-le p2, p3, :cond_0

    .line 854
    array-length p2, p1

    const/4 p3, 0x3

    sub-int/2addr p2, p3

    new-array p2, p2, [B

    const/4 v0, 0x0

    .line 856
    array-length v1, p1

    sub-int/2addr v1, p3

    invoke-static {p1, p3, p2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 858
    invoke-static {p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 0

    .line 837
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p3

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    invoke-virtual {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p4

    .line 20205
    invoke-static {p1, p3, p2, p4}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ap([B[B[B[B)[B

    move-result-object p1

    .line 839
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;I)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 0

    .line 824
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    invoke-virtual {p3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p3

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    invoke-virtual {p4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p4

    invoke-static {p1, p3, p2, p4, p5}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B[B[B[BI)[B

    move-result-object p1

    .line 826
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/meawallet/mtp/cm;)Lcom/meawallet/mtp/cm;
    .locals 4

    .line 806
    new-instance v0, Lcom/meawallet/mtp/cm;

    .line 807
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    .line 18023
    iget-object v2, p2, Lcom/meawallet/mtp/cm;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 807
    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v2

    .line 18070
    invoke-static {v1, v2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->v([B[B)[B

    move-result-object v1

    .line 807
    invoke-static {v1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    .line 808
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v2

    .line 19027
    iget-object v3, p2, Lcom/meawallet/mtp/cm;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 808
    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v3

    .line 19070
    invoke-static {v2, v3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->v([B[B)[B

    move-result-object v2

    .line 808
    invoke-static {v2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    .line 809
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    .line 20031
    iget-object p2, p2, Lcom/meawallet/mtp/cm;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 809
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    .line 20070
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->v([B[B)[B

    move-result-object p1

    .line 809
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/meawallet/mtp/cm;-><init>(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V

    return-object v0
.end method

.method public final a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;I)Z
    .locals 0

    .line 952
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    .line 21280
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->az([BI)Z

    move-result p1

    return p1
.end method

.method public final b(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 0

    .line 879
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    .line 21266
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ay([B)[B

    move-result-object p1

    .line 881
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 21273
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->aw()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public buildComputeCcCryptograms([BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
    .locals 0

    .line 568
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 569
    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    .line 9159
    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ak([B[B[B)[B

    move-result-object p1

    .line 571
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    move-result-object p1

    return-object p1
.end method

.method public buildGenerateAcCryptograms([BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;
    .locals 0

    .line 553
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 554
    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    .line 9149
    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->aj([B[B[B)[B

    move-result-object p1

    .line 556
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->c([B)Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto$TransactionCryptograms;

    move-result-object p1

    return-object p1
.end method

.method public buildRemoteServiceRequest(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;I)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;
    .locals 0

    .line 710
    new-instance p4, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;

    .line 711
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 712
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 713
    invoke-interface {p5}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getSessionCode()[B

    move-result-object p5

    .line 710
    invoke-static {p3, p2, p1, p5, p6}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a([B[B[B[BI)[B

    move-result-object p1

    invoke-direct {p4, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;-><init>([B)V

    return-object p4
.end method

.method public buildSignedDynamicApplicationData([B[B[B[BLcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)[B
    .locals 0

    .line 584
    invoke-virtual {p5}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p5

    .line 9231
    invoke-static {p1, p2, p3, p4, p5}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->as([B[B[B[B[B)[B

    move-result-object p1

    return-object p1
.end method

.method public calculateAuthenticationCode(Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;[BLcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;)[B
    .locals 1

    .line 619
    invoke-interface {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;->getEncryptedDeviceFingerPrint()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 626
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    .line 627
    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p2

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p2

    .line 628
    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    .line 629
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getSessionCode()[B

    move-result-object p1

    .line 11214
    invoke-static {p2, p3, p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->aq([B[B[B)[B

    move-result-object p1

    return-object p1

    .line 623
    :cond_0
    new-instance p1, Ljava/security/GeneralSecurityException;

    const-string p2, "Failed to calculate authentication code, encrypted device fingerprint is null."

    invoke-direct {p1, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public createSignedRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;
    .locals 1

    .line 600
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 10222
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ar([B[B)[B

    move-result-object p1

    .line 600
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public decryptPushedRemoteData(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;[B)[B
    .locals 0

    .line 741
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 742
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 16175
    invoke-static {p3, p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ba([B[B[B)[B

    move-result-object p1

    const/16 p2, 0x10

    .line 746
    invoke-static {p2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->get(I)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p2

    .line 748
    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->append(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    return-object p1
.end method

.method public decryptRemoteServiceResponse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;)[B
    .locals 0

    .line 726
    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/TransportKeyEncryptedData;->getEncryptedData()[B

    move-result-object p3

    .line 727
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 728
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 729
    invoke-interface {p4}, Lcom/mastercard/mpsdk/componentinterface/crypto/SessionData;->getSessionCode()[B

    move-result-object p4

    .line 15205
    invoke-static {p3, p2, p1, p4}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->ap([B[B[B[B)[B

    move-result-object p1

    return-object p1
.end method

.method public deriveSessionKeyFromSingleUseKey(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 1

    .line 521
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    .line 523
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 524
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 8168
    invoke-static {p2, p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->al([B[B)[B

    move-result-object p1

    .line 522
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public encryptDataForStorage([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;
    .locals 1

    .line 110
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;

    .line 5021
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->m([B)[B

    move-result-object p1

    .line 110
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public encryptDataUsingLocalDekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 1

    .line 148
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    .line 6971
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a2([B)[B

    move-result-object p1

    .line 148
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public encryptDataUsingRemoteKekKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
    .locals 1

    .line 154
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    .line 6976
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->b([B)[B

    move-result-object p1

    .line 154
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;
    .locals 1

    .line 669
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    .line 670
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 671
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;->getPaymentAppInstanceId()[B

    move-result-object p2

    .line 672
    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    .line 13090
    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->z([B[B[B)[B

    move-result-object p1

    .line 669
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public encryptPinBlockWithRgk(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
    .locals 1

    .line 683
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 684
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;->getPaymentAppInstanceId()[B

    move-result-object p2

    .line 685
    invoke-virtual {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object p3

    .line 14085
    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->y([B[B[B)[B

    move-result-object p1

    .line 683
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public encryptSessionKey([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 1

    .line 536
    :try_start_0
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    .line 9080
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->x([B)[B

    move-result-object p1

    .line 536
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    .line 538
    sget-object v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public encryptWalletData([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 1

    .line 92
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    .line 4030
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->o([B)[B

    move-result-object p1

    .line 92
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public encryptWalletData([B[B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 2

    .line 102
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    .line 4034
    invoke-static {v1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->p(Ljava/lang/String;[B)[B

    move-result-object p1

    .line 102
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public exchangeDekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 1

    .line 656
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    .line 658
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 659
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 13053
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->t([B[B)[B

    move-result-object p1

    .line 657
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public exchangeIccKekForLocalDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 8

    .line 639
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    .line 640
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;->getEncryptedData()[B

    move-result-object v1

    .line 641
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    .line 642
    invoke-interface {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;->getU()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;->getEncryptedData()[B

    move-result-object v3

    .line 643
    invoke-interface {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;->getP()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;->getEncryptedData()[B

    move-result-object v4

    .line 644
    invoke-interface {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;->getQ()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;->getEncryptedData()[B

    move-result-object v5

    .line 645
    invoke-interface {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;->getDp()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;->getEncryptedData()[B

    move-result-object v6

    .line 646
    invoke-interface {p3}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;->getDq()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;->getEncryptedData()[B

    move-result-object v7

    .line 12064
    invoke-static/range {v1 .. v7}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->u([B[B[B[B[B[B[B)[B

    move-result-object p1

    .line 640
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public exchangeRgkForRmKek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
    .locals 1

    .line 609
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 11070
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->v([B[B)[B

    move-result-object p1

    .line 609
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public generateEncryptedRgk()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;
    .locals 2

    .line 592
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    .line 10179
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->an()[B

    move-result-object v1

    .line 592
    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public generateMac([B)[B
    .locals 0

    .line 6038
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->q([B)[B

    move-result-object p1

    return-object p1
.end method

.method public generateMac([B[B)[B
    .locals 1

    .line 142
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    .line 6043
    invoke-static {p2, v0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->r([BLjava/lang/String;)[B

    move-result-object p1

    return-object p1
.end method

.method public getDatabaseCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseCrypto;
    .locals 0

    return-object p0
.end method

.method public getDatabaseStorageKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$1;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl$1;-><init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;)V

    iput-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    return-object v0
.end method

.method public getDatabaseStorageMacKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl$2;-><init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;)V

    iput-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    return-object v0
.end method

.method public getDatabaseUpgradeCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/DatabaseUpgradeCrypto;
    .locals 0

    return-object p0
.end method

.method public getLocalDataEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    if-nez v0, :cond_0

    .line 281
    new-instance v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$3;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl$3;-><init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;)V

    iput-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    return-object v0
.end method

.method public getLocalDekEncryptedIdn(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
    .locals 1

    .line 694
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    .line 695
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 696
    invoke-virtual {p2}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;->getEncryptedData()[B

    move-result-object p2

    .line 15075
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->w([B[B)[B

    move-result-object p1

    .line 694
    invoke-direct {v0, p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public getRemoteManagementCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;
    .locals 0

    return-object p0
.end method

.method public getRemoteManagementKeyEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    if-nez v0, :cond_0

    .line 341
    new-instance v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;-><init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;)V

    iput-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    return-object v0
.end method

.method public getTransactionCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/TransactionCrypto;
    .locals 0

    return-object p0
.end method

.method public getWalletDataCrypto()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;
    .locals 0

    return-object p0
.end method

.method public getWalletDataEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    if-nez v0, :cond_0

    .line 401
    new-instance v0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$5;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl$5;-><init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;)V

    iput-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;

    return-object v0
.end method

.method public initIccKey(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;)I
    .locals 0

    .line 512
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 7238
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->at([B)I

    move-result p1

    return p1
.end method

.method public isMacValid([B[B)Z
    .locals 0

    .line 5048
    invoke-static {p1, p2}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->s([B[B)Z

    move-result p1

    return p1
.end method

.method public final m()Landroid/content/Context;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->g:Landroid/content/Context;

    return-object v0
.end method

.method public sha1([B)[B
    .locals 0

    .line 16245
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->au([B)[B

    move-result-object p1

    return-object p1
.end method

.method public sha256([B)[B
    .locals 0

    .line 16252
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->av([B)[B

    move-result-object p1

    return-object p1
.end method

.method public unencryptStoredDataForUse(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;)[B
    .locals 0

    .line 118
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DstDekEncryptedData;->getEncryptedData()[B

    move-result-object p1

    .line 5026
    invoke-static {p1}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->n([B)[B

    move-result-object p1

    return-object p1
.end method

.method public wipeCryptoParameters()Z
    .locals 2

    .line 497
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "LOCAL_DEK_ID_PREF_KEY"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    .line 498
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "DATA_STORAGE_DEK_ID_PREF_KEY"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    .line 499
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "DATA_STORAGE_MAC_KEY_ID_PREF_KEY"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    .line 500
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "RM_KEK_KEY_ID_PREF_KEY"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    .line 501
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "WALLET_DEK_ID_PREF_KEY"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    const/4 v0, 0x1

    return v0
.end method
