.class Lcom/meawallet/mtp/hc;
.super Lcom/meawallet/mtp/fv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/fv<",
        "Lcom/meawallet/mtp/MeaListener;",
        "Lcom/meawallet/mtp/eh;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "hc"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/ap;Lcom/meawallet/mtp/CvmMethod;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 6

    .line 26
    new-instance v4, Lcom/meawallet/mtp/hd;

    invoke-direct {v4, p6, p4, p5, p7}, Lcom/meawallet/mtp/hd;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/ap;Lcom/meawallet/mtp/CvmMethod;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/fv;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/d;)V
    .locals 1

    .line 55
    new-instance v0, Lcom/meawallet/mtp/ft;

    invoke-direct {v0}, Lcom/meawallet/mtp/ft;-><init>()V

    .line 1147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 55
    check-cast v0, Lcom/meawallet/mtp/MeaCoreListener;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method

.method final b()Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 35
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method

.method final b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/eh;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 46
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 48
    new-instance p1, Lcom/meawallet/mtp/d;

    invoke-direct {p1}, Lcom/meawallet/mtp/d;-><init>()V

    return-object p1
.end method

.method final d()Lcom/meawallet/mtp/ep;
    .locals 1

    .line 40
    sget-object v0, Lcom/meawallet/mtp/gy;->i:Lcom/meawallet/mtp/ep;

    return-object v0
.end method
