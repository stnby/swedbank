.class Lcom/meawallet/mtp/u;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/u$b;,
        Lcom/meawallet/mtp/u$a;
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String; = "u"


# instance fields
.field final a:Lcom/meawallet/mtp/u$a;

.field b:Lcom/meawallet/mtp/ee;

.field c:Ljava/util/concurrent/Semaphore;

.field private e:Lcom/meawallet/mtp/fi;

.field private f:Lcom/meawallet/mtp/af;

.field private g:Lcom/google/gson/Gson;

.field private final h:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/af;Lcom/meawallet/mtp/fi;Lcom/google/gson/Gson;)V
    .locals 2

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/meawallet/mtp/u;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 58
    iput-object p1, p0, Lcom/meawallet/mtp/u;->f:Lcom/meawallet/mtp/af;

    .line 59
    iput-object p2, p0, Lcom/meawallet/mtp/u;->e:Lcom/meawallet/mtp/fi;

    .line 60
    iput-object p3, p0, Lcom/meawallet/mtp/u;->g:Lcom/google/gson/Gson;

    .line 61
    new-instance p1, Lcom/meawallet/mtp/u$a;

    invoke-direct {p1, p0, v1}, Lcom/meawallet/mtp/u$a;-><init>(Lcom/meawallet/mtp/u;B)V

    iput-object p1, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    .line 62
    new-instance p1, Ljava/util/concurrent/Semaphore;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object p1, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/u;Lcom/meawallet/mtp/ee;)Lcom/meawallet/mtp/ee;
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    return-object p1
.end method

.method static synthetic a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/meawallet/mtp/u;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic a(Lcom/meawallet/mtp/u;Lcom/meawallet/mtp/ee;Lcom/google/gson/Gson;)V
    .locals 6

    if-eqz p1, :cond_6

    .line 2245
    invoke-interface {p1, p2}, Lcom/meawallet/mtp/ee;->a(Lcom/google/gson/Gson;)V

    .line 2255
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->b()Lcom/meawallet/mtp/ag;

    move-result-object p2

    sget-object v0, Lcom/meawallet/mtp/ag;->c:Lcom/meawallet/mtp/ag;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq p2, v0, :cond_1

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->b()Lcom/meawallet/mtp/ag;

    move-result-object p2

    sget-object v0, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_5

    .line 2303
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->d()Lcom/meawallet/mtp/ah;

    move-result-object p2

    .line 2304
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->b()Lcom/meawallet/mtp/ag;

    move-result-object v0

    .line 2305
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->k()Ljava/lang/String;

    move-result-object v3

    .line 2307
    new-array v4, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 2308
    new-array v4, v2, [Ljava/lang/Object;

    aput-object p2, v4, v1

    .line 2309
    new-array v4, v2, [Ljava/lang/Object;

    aput-object v0, v4, v1

    .line 2310
    new-array v0, v2, [Ljava/lang/Object;

    aput-object v3, v0, v1

    .line 2312
    sget-object v0, Lcom/meawallet/mtp/u$1;->b:[I

    invoke-virtual {p2}, Lcom/meawallet/mtp/ah;->ordinal()I

    move-result p2

    aget p2, v0, p2

    if-eq p2, v2, :cond_2

    goto :goto_2

    .line 2316
    :cond_2
    :try_start_0
    iget-object p2, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p2}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result p2

    .line 2318
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v0, v1

    .line 2320
    instance-of p2, p1, Lcom/meawallet/mtp/eq;

    if-eqz p2, :cond_3

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->b()Lcom/meawallet/mtp/ag;

    move-result-object p1

    sget-object p2, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    if-ne p1, p2, :cond_3

    invoke-direct {p0}, Lcom/meawallet/mtp/u;->e()Z

    move-result p1

    if-nez p1, :cond_4

    .line 2324
    :cond_3
    iget-object p0, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-void

    :catch_0
    move-exception p0

    .line 2329
    sget-object p1, Lcom/meawallet/mtp/u;->d:Ljava/lang/String;

    invoke-static {p1, p0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-void

    .line 3261
    :cond_5
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->c()Lcom/meawallet/mtp/ae;

    move-result-object p2

    .line 3263
    new-array v0, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 3264
    new-array v0, v2, [Ljava/lang/Object;

    aput-object p2, v0, v1

    .line 3266
    sget-object v0, Lcom/meawallet/mtp/u$1;->a:[I

    invoke-virtual {p2}, Lcom/meawallet/mtp/ae;->ordinal()I

    move-result p2

    aget p2, v0, p2

    packed-switch p2, :pswitch_data_0

    goto :goto_4

    .line 3292
    :pswitch_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/u;->b()V

    goto :goto_4

    .line 3282
    :pswitch_1
    new-array p2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, v1

    .line 3284
    iget-object p2, p0, Lcom/meawallet/mtp/u;->e:Lcom/meawallet/mtp/fi;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/meawallet/mtp/fi;->d(Ljava/lang/String;)V

    .line 3286
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->j()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 3287
    iget-object p0, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/u$a;->b(Lcom/meawallet/mtp/ee;)Z

    return-void

    .line 3269
    :pswitch_2
    :try_start_1
    new-array p2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p2, v1

    .line 3271
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->g()I

    move-result p2

    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception p2

    .line 3273
    sget-object v0, Lcom/meawallet/mtp/u;->d:Ljava/lang/String;

    const-string v2, "processCommandFailed(): Error in retry delay time"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p2, v2, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3276
    :goto_3
    iget-object p0, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/u$a;->a(Lcom/meawallet/mtp/ee;)V

    return-void

    :cond_6
    :goto_4
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b(Lcom/meawallet/mtp/ee;)Lcom/meawallet/mtp/ee;
    .locals 4

    .line 219
    iget-object v0, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    monitor-enter v0

    .line 221
    :try_start_0
    iget-object v1, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {v1}, Lcom/meawallet/mtp/u$a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/ee;

    .line 223
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 224
    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    invoke-interface {p1, v1}, Lcom/meawallet/mtp/ee;->a(Lcom/meawallet/mtp/ag;)V

    const/4 v1, 0x1

    .line 226
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->h()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v3

    .line 228
    monitor-exit v0

    return-object v2

    .line 231
    :cond_1
    monitor-exit v0

    const/4 p1, 0x0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic b(Lcom/meawallet/mtp/u;)V
    .locals 2

    .line 1110
    new-instance v0, Lcom/meawallet/mtp/u$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/meawallet/mtp/u$b;-><init>(Lcom/meawallet/mtp/u;B)V

    invoke-virtual {v0}, Lcom/meawallet/mtp/u$b;->start()V

    return-void
.end method

.method static synthetic c(Lcom/meawallet/mtp/u;)V
    .locals 4

    .line 1167
    iget-object v0, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    const/4 v1, 0x1

    .line 1169
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 1171
    iget-object v0, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    .line 1175
    iget-object p0, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void
.end method

.method static synthetic d(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/u$a;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    return-object p0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .line 13
    sget-object v0, Lcom/meawallet/mtp/u;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/ee;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    return-object p0
.end method

.method private e()Z
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/meawallet/mtp/u;->e:Lcom/meawallet/mtp/fi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/u;->e:Lcom/meawallet/mtp/fi;

    invoke-virtual {v0}, Lcom/meawallet/mtp/fi;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic f(Lcom/meawallet/mtp/u;)Z
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/meawallet/mtp/u;->e()Z

    move-result p0

    return p0
.end method

.method static synthetic g(Lcom/meawallet/mtp/u;)V
    .locals 1

    .line 2116
    iget-object v0, p0, Lcom/meawallet/mtp/u;->f:Lcom/meawallet/mtp/af;

    invoke-interface {v0}, Lcom/meawallet/mtp/af;->e()Lcom/meawallet/mtp/ee;

    move-result-object v0

    .line 2118
    iget-object p0, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/u$a;->b(Lcom/meawallet/mtp/ee;)Z

    return-void
.end method

.method static synthetic h(Lcom/meawallet/mtp/u;)Lcom/google/gson/Gson;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/meawallet/mtp/u;->g:Lcom/google/gson/Gson;

    return-object p0
.end method


# virtual methods
.method final declared-synchronized a(Lcom/meawallet/mtp/ee;)Ljava/lang/String;
    .locals 1

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    const-string p1, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    .line 77
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/u;->b(Lcom/meawallet/mtp/ee;)Lcom/meawallet/mtp/ee;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 81
    invoke-interface {v0}, Lcom/meawallet/mtp/ee;->a()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    .line 84
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/u$a;->b(Lcom/meawallet/mtp/ee;)Z

    .line 86
    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->a()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    .line 67
    :goto_0
    monitor-exit p0

    throw p1
.end method

.method final a()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/meawallet/mtp/u;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_0
    return-void
.end method

.method final declared-synchronized b()V
    .locals 3

    monitor-enter p0

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 188
    :try_start_1
    iget-object v1, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    invoke-interface {v1}, Lcom/meawallet/mtp/ee;->f()V

    .line 193
    iget-object v1, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    invoke-interface {v1}, Lcom/meawallet/mtp/ee;->l()V

    const/4 v1, 0x0

    .line 194
    iput-object v1, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    .line 199
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {v1}, Lcom/meawallet/mtp/u$a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/ee;

    .line 200
    invoke-interface {v2}, Lcom/meawallet/mtp/ee;->l()V

    goto :goto_0

    .line 205
    :cond_1
    iget-object v1, p0, Lcom/meawallet/mtp/u;->a:Lcom/meawallet/mtp/u$a;

    invoke-virtual {v1}, Lcom/meawallet/mtp/u$a;->clear()V

    .line 206
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    :try_start_2
    invoke-virtual {p0}, Lcom/meawallet/mtp/u;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 211
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    .line 206
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    .line 186
    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    .line 340
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/u;->b:Lcom/meawallet/mtp/ee;

    invoke-interface {v0}, Lcom/meawallet/mtp/ee;->a()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "NULL"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
