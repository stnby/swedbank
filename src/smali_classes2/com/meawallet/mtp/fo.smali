.class Lcom/meawallet/mtp/fo;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "fo"


# instance fields
.field private b:Landroid/app/Application;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/app/Application;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/meawallet/mtp/fo;->b:Landroid/app/Application;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .line 16
    sget-object v0, Lcom/meawallet/mtp/fo;->a:Ljava/lang/String;

    return-object v0
.end method

.method private varargs b()Ljava/lang/Boolean;
    .locals 5

    .line 36
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/meawallet/mtp/fo;->b:Landroid/app/Application;

    invoke-static {v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->initialize(Landroid/app/Application;)V

    .line 44
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->d()Lcom/meawallet/mtp/m;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/meawallet/mtp/m;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 47
    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    .line 53
    :cond_1
    iget-object v2, p0, Lcom/meawallet/mtp/fo;->b:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 54
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    .line 57
    :cond_2
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    .line 59
    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 61
    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v3

    .line 1082
    new-instance v4, Lcom/meawallet/mtp/dk;

    invoke-direct {v4, v2, v3}, Lcom/meawallet/mtp/dk;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/cu;)V

    .line 1084
    new-instance v3, Lcom/meawallet/mtp/fo$1;

    invoke-direct {v3, p0}, Lcom/meawallet/mtp/fo$1;-><init>(Lcom/meawallet/mtp/fo;)V

    .line 2031
    iput-object v3, v4, Lcom/meawallet/mtp/dk;->a:Lcom/meawallet/mtp/en;

    .line 1109
    invoke-virtual {v4}, Lcom/meawallet/mtp/dk;->a()V

    .line 63
    invoke-virtual {v0, v2}, Lcom/meawallet/mtp/m;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/InitializationFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 72
    :cond_3
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0

    .line 50
    :cond_4
    :goto_1
    :try_start_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_1
    .catch Lcom/meawallet/mtp/InitializationFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 67
    sget-object v1, Lcom/meawallet/mtp/fo;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    const/4 v0, 0x1

    .line 77
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/meawallet/mtp/fo;->b()Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/fo;->a(Ljava/lang/Boolean;)V

    return-void
.end method
