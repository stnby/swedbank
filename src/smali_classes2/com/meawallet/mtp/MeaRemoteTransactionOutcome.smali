.class public Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pan"
    .end annotation
.end field

.field private b:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "panSequenceNumber"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "par"
    .end annotation
.end field

.field private d:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cryptogramType"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "track2Data"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionCryptogramData"
    .end annotation
.end field

.field private g:Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expiryDate"
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionId"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;)V
    .locals 2

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 40
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getTransactionCryptogramData()[B

    move-result-object v0

    .line 41
    invoke-static {v0}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->f:Ljava/lang/String;

    .line 43
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getPan()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->a:Ljava/lang/String;

    .line 44
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getPanSequenceNumber()I

    move-result v0

    iput v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->b:I

    .line 45
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->d:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    .line 46
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getTrack2Data()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->e:Ljava/lang/String;

    .line 47
    new-instance v0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v1

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getExpirationDate()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;-><init>(Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->g:Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;

    .line 48
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getTransactionId()[B

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->h:Ljava/lang/String;

    .line 49
    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpTransactionOutcome;->getDsrpOutputData()Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;

    move-result-object p1

    invoke-interface {p1}, Lcom/mastercard/mpsdk/componentinterface/DsrpOutputData;->getPar()[B

    move-result-object p1

    invoke-static {p1}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->c:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public getCryptogramType()Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->d:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    return-object v0
.end method

.method public getExpiryDate()Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->g:Lcom/meawallet/mtp/MeaRemoteTransactionOutcome$ExpiryDate;

    return-object v0
.end method

.method public getPan()Ljava/lang/String;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPanSequenceNumber()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->b:I

    return v0
.end method

.method public getPar()Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack2Data()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionCryptogramData()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionId()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->h:Ljava/lang/String;

    return-object v0
.end method

.method public toJsonString()Ljava/lang/String;
    .locals 1

    .line 183
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MeaRemoteTransactionOutcome[\n\ttransactionCryptogramData = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tpan = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tpanSequenceNumber = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\n\tcryptogramType = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->d:Lcom/mastercard/mpsdk/componentinterface/RemoteCryptogramType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\ttrack2Data = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
