.class final Lcom/meawallet/mtp/db;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/net/ssl/X509TrustManager;

.field private final c:Lcom/meawallet/mtp/bf;


# direct methods
.method constructor <init>(Ljava/util/List;Ljavax/net/ssl/X509TrustManager;Lcom/meawallet/mtp/bf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/net/ssl/X509TrustManager;",
            "Lcom/meawallet/mtp/bf;",
            ")V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/meawallet/mtp/db;->a:Ljava/util/List;

    .line 22
    iput-object p2, p0, Lcom/meawallet/mtp/db;->b:Ljavax/net/ssl/X509TrustManager;

    .line 23
    iput-object p3, p0, Lcom/meawallet/mtp/db;->c:Lcom/meawallet/mtp/bf;

    return-void
.end method


# virtual methods
.method public final execute(Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;"
        }
    .end annotation

    .line 32
    :try_start_0
    new-instance v0, Lcom/meawallet/mtp/da;

    .line 33
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->name()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/meawallet/mtp/MeaHttpMethod;->valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaHttpMethod;

    move-result-object p1

    iget-object v1, p0, Lcom/meawallet/mtp/db;->b:Ljavax/net/ssl/X509TrustManager;

    invoke-direct {v0, p1, p2, v1}, Lcom/meawallet/mtp/da;-><init>(Lcom/meawallet/mtp/MeaHttpMethod;Ljava/lang/String;Ljavax/net/ssl/X509TrustManager;)V

    iget-object p1, p0, Lcom/meawallet/mtp/db;->a:Ljava/util/List;

    .line 1088
    iput-object p1, v0, Lcom/meawallet/mtp/da;->c:Ljava/util/List;

    .line 1112
    iput-object p4, v0, Lcom/meawallet/mtp/da;->d:Ljava/util/Map;

    if-eqz p3, :cond_0

    .line 2100
    iput-object p3, v0, Lcom/meawallet/mtp/da;->b:Ljava/lang/String;

    .line 43
    :cond_0
    iget-object p1, p0, Lcom/meawallet/mtp/db;->c:Lcom/meawallet/mtp/bf;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/da;->a(Lcom/meawallet/mtp/bf;)Lcom/meawallet/mtp/bj;

    move-result-object p1
    :try_end_0
    .catch Lcom/meawallet/mtp/bi; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 45
    new-instance p2, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;

    invoke-virtual {p1}, Lcom/meawallet/mtp/bi;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/mastercard/mpsdk/componentinterface/http/HttpException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
