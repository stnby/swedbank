.class final enum Lcom/meawallet/mtp/StorageTechnology;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/StorageTechnology;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEVICE_MEMORY:Lcom/meawallet/mtp/StorageTechnology;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum DEVICE_MEMORY_PROTECTED_TPM:Lcom/meawallet/mtp/StorageTechnology;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum SE:Lcom/meawallet/mtp/StorageTechnology;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum SERVER:Lcom/meawallet/mtp/StorageTechnology;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum TEE:Lcom/meawallet/mtp/StorageTechnology;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum VEE:Lcom/meawallet/mtp/StorageTechnology;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/StorageTechnology;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 6
    new-instance v0, Lcom/meawallet/mtp/StorageTechnology;

    const-string v1, "DEVICE_MEMORY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/StorageTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/StorageTechnology;->DEVICE_MEMORY:Lcom/meawallet/mtp/StorageTechnology;

    .line 10
    new-instance v0, Lcom/meawallet/mtp/StorageTechnology;

    const-string v1, "DEVICE_MEMORY_PROTECTED_TPM"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/StorageTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/StorageTechnology;->DEVICE_MEMORY_PROTECTED_TPM:Lcom/meawallet/mtp/StorageTechnology;

    .line 14
    new-instance v0, Lcom/meawallet/mtp/StorageTechnology;

    const-string v1, "TEE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/StorageTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/StorageTechnology;->TEE:Lcom/meawallet/mtp/StorageTechnology;

    .line 18
    new-instance v0, Lcom/meawallet/mtp/StorageTechnology;

    const-string v1, "SE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/StorageTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/StorageTechnology;->SE:Lcom/meawallet/mtp/StorageTechnology;

    .line 22
    new-instance v0, Lcom/meawallet/mtp/StorageTechnology;

    const-string v1, "SERVER"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/StorageTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/StorageTechnology;->SERVER:Lcom/meawallet/mtp/StorageTechnology;

    .line 26
    new-instance v0, Lcom/meawallet/mtp/StorageTechnology;

    const-string v1, "VEE"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/StorageTechnology;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/StorageTechnology;->VEE:Lcom/meawallet/mtp/StorageTechnology;

    const/4 v0, 0x6

    .line 5
    new-array v0, v0, [Lcom/meawallet/mtp/StorageTechnology;

    sget-object v1, Lcom/meawallet/mtp/StorageTechnology;->DEVICE_MEMORY:Lcom/meawallet/mtp/StorageTechnology;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/StorageTechnology;->DEVICE_MEMORY_PROTECTED_TPM:Lcom/meawallet/mtp/StorageTechnology;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/StorageTechnology;->TEE:Lcom/meawallet/mtp/StorageTechnology;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/StorageTechnology;->SE:Lcom/meawallet/mtp/StorageTechnology;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/StorageTechnology;->SERVER:Lcom/meawallet/mtp/StorageTechnology;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/StorageTechnology;->VEE:Lcom/meawallet/mtp/StorageTechnology;

    aput-object v1, v0, v7

    sput-object v0, Lcom/meawallet/mtp/StorageTechnology;->a:[Lcom/meawallet/mtp/StorageTechnology;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/StorageTechnology;
    .locals 1

    .line 5
    const-class v0, Lcom/meawallet/mtp/StorageTechnology;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/StorageTechnology;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/StorageTechnology;
    .locals 1

    .line 5
    sget-object v0, Lcom/meawallet/mtp/StorageTechnology;->a:[Lcom/meawallet/mtp/StorageTechnology;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/StorageTechnology;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/StorageTechnology;

    return-object v0
.end method
