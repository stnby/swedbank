.class Lcom/meawallet/mtp/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/f$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "f"


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/f;->b:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/meawallet/mtp/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 121
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    .line 122
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/meawallet/mtp/ax;->d(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardPinListener;

    move-result-object v3

    .line 123
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 1119
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->c:Lcom/meawallet/mtp/MeaWalletPinListener;

    .line 125
    sget-object v1, Lcom/meawallet/mtp/f$9;->a:[I

    invoke-virtual {p1}, Lcom/meawallet/mtp/f$a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const/4 p2, 0x1

    .line 255
    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    goto/16 :goto_0

    :pswitch_1
    if-eqz v0, :cond_0

    .line 236
    new-instance p1, Lcom/meawallet/mtp/f$8;

    invoke-direct {p1, p0, v0}, Lcom/meawallet/mtp/f$8;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaWalletPinListener;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_2
    if-eqz v3, :cond_0

    .line 225
    new-instance p1, Lcom/meawallet/mtp/f$7;

    invoke-direct {p1, p0, v3, p2}, Lcom/meawallet/mtp/f$7;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaCardPinListener;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_3
    if-eqz v0, :cond_0

    .line 214
    new-instance p1, Lcom/meawallet/mtp/f$6;

    invoke-direct {p1, p0, v0, p3, p4}, Lcom/meawallet/mtp/f$6;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaWalletPinListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_4
    return-void

    :pswitch_5
    if-eqz v0, :cond_0

    .line 197
    new-instance p1, Lcom/meawallet/mtp/f$5;

    invoke-direct {p1, p0, v0, p3, p4}, Lcom/meawallet/mtp/f$5;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaWalletPinListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_6
    if-eqz v0, :cond_0

    .line 186
    new-instance p1, Lcom/meawallet/mtp/f$4;

    invoke-direct {p1, p0, v0}, Lcom/meawallet/mtp/f$4;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaWalletPinListener;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_7
    if-eqz v3, :cond_0

    .line 174
    new-instance p1, Lcom/meawallet/mtp/f$3;

    move-object v1, p1

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/meawallet/mtp/f$3;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaCardPinListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_8
    return-void

    :pswitch_9
    if-eqz v3, :cond_0

    .line 156
    new-instance p1, Lcom/meawallet/mtp/f$2;

    move-object v1, p1

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/meawallet/mtp/f$2;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaCardPinListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_a
    if-eqz v3, :cond_0

    .line 145
    new-instance p1, Lcom/meawallet/mtp/f$1;

    invoke-direct {p1, p0, v3, p2}, Lcom/meawallet/mtp/f$1;-><init>(Lcom/meawallet/mtp/f;Lcom/meawallet/mtp/MeaCardPinListener;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    :pswitch_b
    return-void

    :pswitch_c
    return-void

    :pswitch_d
    return-void

    :pswitch_e
    return-void

    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 8

    .line 267
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/meawallet/mtp/f;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    .line 269
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;

    .line 271
    sget-object v1, Lcom/meawallet/mtp/f$9;->a:[I

    invoke-virtual {p1}, Lcom/meawallet/mtp/f$a;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 366
    :pswitch_0
    invoke-interface {v2, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onRequestSessionFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 362
    :pswitch_1
    invoke-interface {v2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onRequestSessionCompleted()Z

    goto :goto_0

    .line 358
    :pswitch_2
    invoke-interface {v2, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onTaskStatusFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    :pswitch_3
    const-string v1, ""

    .line 354
    invoke-interface {v2, v1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onTaskStatusCompleted(Ljava/lang/String;)Z

    goto :goto_0

    :pswitch_4
    const-string v1, ""

    .line 342
    invoke-interface {v2, v1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onChangeWalletMobilePinStarted(Ljava/lang/String;)Z

    goto :goto_0

    :pswitch_5
    const-string v1, ""

    .line 338
    invoke-interface {v2, p3, v1}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 334
    :pswitch_6
    invoke-interface {v2, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 330
    :pswitch_7
    invoke-interface {v2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSystemHealthCompleted()Z

    goto :goto_0

    .line 294
    :pswitch_8
    invoke-interface {v2, p3, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 290
    :pswitch_9
    invoke-interface {v2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onDeleteCardCompleted(Ljava/lang/String;)Z

    goto :goto_0

    .line 350
    :pswitch_a
    invoke-interface {v2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onWalletMobilePinResetCompleted()Z

    goto :goto_0

    .line 346
    :pswitch_b
    invoke-interface {v2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardMobilePinResetCompleted(Ljava/lang/String;)Z

    goto :goto_0

    .line 326
    :pswitch_c
    invoke-interface {v2, p2, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 322
    :pswitch_d
    invoke-interface {v2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onSetWalletPinCompleted()Z

    goto :goto_0

    .line 318
    :pswitch_e
    invoke-interface {v2, p2, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onWalletPinChangeFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 314
    :pswitch_f
    invoke-interface {v2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onWalletPinChangeCompleted()Z

    goto :goto_0

    :pswitch_10
    move-object v3, p3

    move v4, p2

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    .line 310
    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinSetFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 306
    :pswitch_11
    invoke-interface {v2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinSetCompleted(Ljava/lang/String;)Z

    goto :goto_0

    :pswitch_12
    move-object v3, p3

    move v4, p2

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    .line 302
    invoke-interface/range {v2 .. v7}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinChangeFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 298
    :pswitch_13
    invoke-interface {v2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardPinChangeCompleted(Ljava/lang/String;)Z

    goto :goto_0

    .line 286
    :pswitch_14
    invoke-interface {v2, p3, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 282
    :pswitch_15
    invoke-interface {v2, p3, p2}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onReplenishCompleted(Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 278
    :pswitch_16
    invoke-interface {v2, p4, p5, p6}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z

    goto/16 :goto_0

    .line 274
    :pswitch_17
    invoke-interface {v2, p3}, Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;->onCardProvisionCompleted(Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method final declared-synchronized a(Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;)V
    .locals 1

    monitor-enter p0

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 56
    monitor-exit p0

    throw p1
.end method

.method final a(Lcom/meawallet/mtp/f$a;)V
    .locals 7

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    .line 73
    invoke-virtual/range {v0 .. v6}, Lcom/meawallet/mtp/f;->a(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    const/4 v0, 0x1

    .line 107
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 109
    invoke-direct/range {p0 .. p6}, Lcom/meawallet/mtp/f;->b(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 110
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/meawallet/mtp/f;->a(Lcom/meawallet/mtp/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/f$a;Ljava/lang/String;)V
    .locals 7

    const-string v4, ""

    const-string v5, ""

    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    .line 77
    invoke-virtual/range {v0 .. v6}, Lcom/meawallet/mtp/f;->a(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 7

    const-string v3, ""

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 85
    invoke-virtual/range {v0 .. v6}, Lcom/meawallet/mtp/f;->a(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;I)V
    .locals 7

    const-string v3, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p5

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 93
    invoke-virtual/range {v0 .. v6}, Lcom/meawallet/mtp/f;->a(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 7

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 89
    invoke-virtual/range {v0 .. v6}, Lcom/meawallet/mtp/f;->a(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;I)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move v2, p6

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 97
    invoke-virtual/range {v0 .. v6}, Lcom/meawallet/mtp/f;->a(Lcom/meawallet/mtp/f$a;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final declared-synchronized b(Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;)V
    .locals 2

    monitor-enter p0

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 70
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 65
    monitor-exit p0

    throw p1
.end method
