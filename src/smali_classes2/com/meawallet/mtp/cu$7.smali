.class final Lcom/meawallet/mtp/cu$7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/MeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/cu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/meawallet/mtp/MeaListener;

.field final synthetic c:Lcom/meawallet/mtp/cu;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/cu;Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V
    .locals 0

    .line 848
    iput-object p1, p0, Lcom/meawallet/mtp/cu$7;->c:Lcom/meawallet/mtp/cu;

    iput-object p2, p0, Lcom/meawallet/mtp/cu$7;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/meawallet/mtp/cu$7;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic onFailure(Ljava/lang/Object;)V
    .locals 4

    .line 848
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    .line 1864
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1866
    iget-object v0, p0, Lcom/meawallet/mtp/cu$7;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public final onSuccess()V
    .locals 4

    .line 852
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/cu$7;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/meawallet/mtp/cu$7;->c:Lcom/meawallet/mtp/cu;

    invoke-static {v1}, Lcom/meawallet/mtp/cu;->f(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/ci;

    move-result-object v1

    iget-object v2, p0, Lcom/meawallet/mtp/cu$7;->c:Lcom/meawallet/mtp/cu;

    invoke-static {v2}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/cu;)Lcom/meawallet/mtp/k;

    invoke-static {v0, v1}, Lcom/meawallet/mtp/i;->a(Ljava/util/List;Lcom/meawallet/mtp/ci;)V

    .line 853
    iget-object v0, p0, Lcom/meawallet/mtp/cu$7;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-static {v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 856
    invoke-static {}, Lcom/meawallet/mtp/cu;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cancelDigitizationAndDeleteInLde() failed."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 858
    iget-object v1, p0, Lcom/meawallet/mtp/cu$7;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-static {v0, v1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method
