.class abstract Lcom/meawallet/mtp/fv;
.super Lcom/meawallet/mtp/el;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Ljava/lang/Object;",
        "R:",
        "Lcom/meawallet/mtp/eh;",
        ">",
        "Lcom/meawallet/mtp/el<",
        "T",
        "L;",
        "TR;>;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "fv"


# instance fields
.field private final h:Lcom/meawallet/mtp/gv;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/ci;",
            "Lcom/meawallet/mtp/dq;",
            "Lcom/google/gson/Gson;",
            "Lcom/meawallet/mtp/gx;",
            "T",
            "L;",
            ")V"
        }
    .end annotation

    .line 39
    new-instance v1, Lcom/meawallet/mtp/gz;

    .line 40
    invoke-interface {p1}, Lcom/meawallet/mtp/ci;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/meawallet/mtp/ec;

    invoke-direct {v2, p1}, Lcom/meawallet/mtp/ec;-><init>(Lcom/meawallet/mtp/ci;)V

    .line 1056
    iput-object v2, p4, Lcom/meawallet/mtp/gx;->c:Lcom/meawallet/mtp/ec;

    .line 41
    invoke-direct {v1, v0, p4}, Lcom/meawallet/mtp/gz;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/gx;)V

    new-instance v2, Lcom/meawallet/mtp/eb;

    .line 2018
    iget-object p1, p2, Lcom/meawallet/mtp/dq;->a:Lcom/meawallet/mtp/ai;

    const-string p4, "https://wsp.meawallet.com"

    const/4 v0, 0x1

    .line 42
    invoke-direct {v2, p1, v0, p4}, Lcom/meawallet/mtp/eb;-><init>(Lcom/meawallet/mtp/ai;ILjava/lang/String;)V

    new-instance v3, Lcom/meawallet/mtp/gw;

    .line 3018
    iget-object p1, p2, Lcom/meawallet/mtp/dq;->a:Lcom/meawallet/mtp/ai;

    .line 43
    invoke-interface {p1}, Lcom/meawallet/mtp/ai;->m()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/meawallet/mtp/gw;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/meawallet/mtp/ha;

    invoke-direct {v4}, Lcom/meawallet/mtp/ha;-><init>()V

    move-object v0, p0

    move-object v5, p3

    move-object v6, p5

    .line 39
    invoke-direct/range {v0 .. v6}, Lcom/meawallet/mtp/el;-><init>(Lcom/meawallet/mtp/eg;Ljavax/net/ssl/X509TrustManager;Lcom/meawallet/mtp/ef;Lcom/meawallet/mtp/bf;Lcom/google/gson/Gson;Ljava/lang/Object;)V

    .line 48
    new-instance p1, Lcom/meawallet/mtp/gv;

    invoke-direct {p1}, Lcom/meawallet/mtp/gv;-><init>()V

    iput-object p1, p0, Lcom/meawallet/mtp/fv;->h:Lcom/meawallet/mtp/gv;

    return-void
.end method

.method private static a(Lcom/google/gson/JsonElement;)Ljava/lang/String;
    .locals 5

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    const/4 v0, 0x1

    .line 136
    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonObject()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 137
    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonPrimitive()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    .line 139
    invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonObject()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonPrimitive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 146
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 148
    sget-object v1, Lcom/meawallet/mtp/fv;->g:Ljava/lang/String;

    const/16 v2, 0x25a

    const-string v4, "Failed to consume JSON element as string exception: %s"

    new-array v0, v0, [Ljava/lang/Object;

    .line 149
    invoke-virtual {p0}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v0, v3

    .line 148
    invoke-static {v1, v2, v4, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    const-string p0, ""

    return-object p0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "TR;>;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 101
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 104
    :try_start_0
    new-array v1, v0, [Ljava/lang/Object;

    invoke-static {p1, v2}, Lcom/meawallet/mtp/JsonUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 107
    iget-object v1, p0, Lcom/meawallet/mtp/fv;->e:Lcom/google/gson/Gson;

    const-class v3, Lcom/meawallet/mtp/hb;

    invoke-virtual {v1, p1, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/hb;

    .line 109
    invoke-virtual {p1}, Lcom/meawallet/mtp/hb;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 110
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    .line 12019
    invoke-virtual {p1}, Lcom/meawallet/mtp/hb;->a()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    .line 12044
    iget-object p1, p1, Lcom/meawallet/mtp/hb;->b:[Lcom/meawallet/mtp/WspErrorResponseData;

    .line 12025
    array-length v3, p1

    if-lez v3, :cond_1

    .line 12056
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, p1, v4

    .line 12058
    invoke-static {v5}, Lcom/meawallet/mtp/gv;->b(Lcom/meawallet/mtp/WspErrorResponseData;)I

    move-result v6

    .line 12059
    sget-object v7, Lcom/meawallet/mtp/gv;->a:Ljava/lang/String;

    invoke-static {v5}, Lcom/meawallet/mtp/gv;->a(Lcom/meawallet/mtp/WspErrorResponseData;)Ljava/lang/String;

    move-result-object v5

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v7, v6, v5, v8}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 12029
    :cond_0
    aget-object p1, p1, v2

    .line 12031
    new-instance v4, Lcom/meawallet/mtp/cy;

    invoke-static {p1}, Lcom/meawallet/mtp/gv;->b(Lcom/meawallet/mtp/WspErrorResponseData;)I

    move-result v3

    .line 13037
    iget-object p1, p1, Lcom/meawallet/mtp/WspErrorResponseData;->c:Ljava/lang/String;

    .line 12031
    invoke-direct {v4, v3, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    :cond_1
    if-nez v4, :cond_2

    .line 12035
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v3, 0x1fc

    invoke-direct {p1, v3}, Lcom/meawallet/mtp/cy;-><init>(I)V

    move-object v4, p1

    .line 14029
    :cond_2
    iput-object v4, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1

    .line 14048
    :cond_3
    iget-object p1, p1, Lcom/meawallet/mtp/hb;->a:Lcom/google/gson/JsonElement;

    .line 115
    invoke-static {p1}, Lcom/meawallet/mtp/fv;->a(Lcom/google/gson/JsonElement;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/fv;->b(Ljava/lang/String;)Lcom/meawallet/mtp/d;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 118
    sget-object v1, Lcom/meawallet/mtp/fv;->g:Ljava/lang/String;

    const-string v3, "Failed to parse response JSON. message = %s"

    new-array v0, v0, [Ljava/lang/Object;

    .line 119
    invoke-virtual {p1}, Lcom/google/gson/JsonSyntaxException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    const/16 p1, 0x1fb

    .line 118
    invoke-static {v1, p1, v3, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 121
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cy;

    const-string v2, "Failed to parse WSP response JSON."

    invoke-direct {v1, p1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 15029
    iput-object v1, v0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v0
.end method

.method abstract b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "TR;>;"
        }
    .end annotation
.end method

.method final b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 90
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 92
    invoke-virtual {p0}, Lcom/meawallet/mtp/fv;->g()Lcom/meawallet/mtp/gx;

    move-result-object v1

    .line 6050
    iget-object v3, v1, Lcom/meawallet/mtp/gx;->c:Lcom/meawallet/mtp/ec;

    if-eqz v3, :cond_0

    iget-boolean v1, v1, Lcom/meawallet/mtp/gx;->d:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_b

    .line 93
    invoke-virtual {p0}, Lcom/meawallet/mtp/fv;->g()Lcom/meawallet/mtp/gx;

    move-result-object v1

    .line 6064
    iget-object v1, v1, Lcom/meawallet/mtp/gx;->c:Lcom/meawallet/mtp/ec;

    .line 6070
    new-array v3, v0, [Ljava/lang/Object;

    aput-object p1, v3, v2

    .line 6072
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 6077
    iget-object v3, v1, Lcom/meawallet/mtp/ec;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v3}, Lcom/meawallet/mtp/ci;->e()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 6084
    iget-object v4, v1, Lcom/meawallet/mtp/ec;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v4}, Lcom/meawallet/mtp/ci;->h()Lcom/meawallet/mtp/cc;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 7043
    iget-object v4, v4, Lcom/meawallet/mtp/cc;->b:Lcom/meawallet/mtp/cn;

    if-eqz v4, :cond_7

    .line 8023
    iget-object v5, v4, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 9023
    iget-object v5, v5, Lcom/meawallet/mtp/cm;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v5, :cond_6

    .line 10023
    iget-object v6, v4, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 10027
    iget-object v6, v6, Lcom/meawallet/mtp/cm;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v6, :cond_5

    const/4 v7, 0x3

    .line 6112
    new-array v7, v7, [Ljava/lang/Object;

    .line 6113
    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v8

    invoke-static {v8}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    .line 6114
    invoke-virtual {v5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v8

    invoke-static {v8}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v0

    const/4 v8, 0x2

    .line 6115
    invoke-virtual {v6}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v9

    invoke-static {v9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArrayUtils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    if-eqz p1, :cond_3

    .line 10190
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-gtz v7, :cond_1

    goto :goto_2

    .line 10195
    :cond_1
    invoke-static {p1}, Lcom/meawallet/mtp/e;->a(Ljava/lang/String;)[B

    move-result-object p1

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    .line 10197
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/meawallet/mtp/h;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v2

    .line 10201
    invoke-virtual {v1}, Lcom/meawallet/mtp/ec;->a()Lcom/meawallet/mtp/ai;

    move-result-object v0

    invoke-interface {v0, p1, v5, v6, v3}, Lcom/meawallet/mtp/ai;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    const-string v0, ""

    if-eqz p1, :cond_2

    .line 10206
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 10207
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    goto :goto_1

    .line 10209
    :cond_2
    sget-object p1, Lcom/meawallet/mtp/ec;->a:Ljava/lang/String;

    const/16 v1, 0x12d

    const-string v5, "Decrypted remote data is null."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v5, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    move-object p1, v0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_4

    .line 6125
    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    .line 11023
    iget-object v0, v4, Lcom/meawallet/mtp/cn;->a:Lcom/meawallet/mtp/cm;

    .line 6126
    invoke-virtual {v0}, Lcom/meawallet/mtp/cm;->a()V

    goto :goto_4

    .line 6121
    :cond_4
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "Decrypted plaintext data is null trying to decrypt response JSON data."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6109
    :cond_5
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "Mac key is null trying to decrypt response JSON data"

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6102
    :cond_6
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "Transport key is null trying to decrypt response JSON data"

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6095
    :cond_7
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "Wallet keyset is null trying to decrypt response JSON data"

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6088
    :cond_8
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "LDE key container is null trying to decrypt response JSON data"

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6081
    :cond_9
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "Session id is null trying to decrypt response JSON data"

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6074
    :cond_a
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string p2, "JSON string is empty trying to decrypt response JSON data"

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p1

    .line 96
    :cond_b
    :goto_4
    invoke-super {p0, p1, p2}, Lcom/meawallet/mtp/el;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method final c()Ljava/lang/String;
    .locals 2

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://wsp.meawallet.com/wsp/wallet/v"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/meawallet/mtp/BuildConfig;->WSP_VERSION:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/meawallet/mtp/fv;->d()Lcom/meawallet/mtp/ep;

    move-result-object v1

    .line 5025
    iget-object v1, v1, Lcom/meawallet/mtp/ep;->a:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method e()V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/meawallet/mtp/fv;->h:Lcom/meawallet/mtp/gv;

    if-eqz v0, :cond_0

    .line 72
    invoke-super {p0}, Lcom/meawallet/mtp/el;->e()V

    return-void

    .line 70
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "WSP remote error wrapper is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final g()Lcom/meawallet/mtp/gx;
    .locals 1

    .line 3119
    iget-object v0, p0, Lcom/meawallet/mtp/el;->d:Lcom/meawallet/mtp/eg;

    .line 52
    check-cast v0, Lcom/meawallet/mtp/gz;

    .line 4080
    iget-object v0, v0, Lcom/meawallet/mtp/gz;->a:Lcom/meawallet/mtp/gx;

    return-object v0
.end method

.method final h()Lcom/meawallet/mtp/ci;
    .locals 1

    .line 63
    invoke-virtual {p0}, Lcom/meawallet/mtp/fv;->g()Lcom/meawallet/mtp/gx;

    move-result-object v0

    .line 5064
    iget-object v0, v0, Lcom/meawallet/mtp/gx;->c:Lcom/meawallet/mtp/ec;

    .line 6032
    iget-object v0, v0, Lcom/meawallet/mtp/ec;->b:Lcom/meawallet/mtp/ci;

    return-object v0
.end method
