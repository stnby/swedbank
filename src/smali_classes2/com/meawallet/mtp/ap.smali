.class final Lcom/meawallet/mtp/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deviceName"
    .end annotation
.end field

.field b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "serialNumber"
    .end annotation
.end field

.field c:Lcom/meawallet/mtp/FormFactor;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "formFactor"
    .end annotation
.end field

.field d:Lcom/meawallet/mtp/StorageTechnology;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "storageTechnology"
    .end annotation
.end field

.field e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "osName"
    .end annotation
.end field

.field f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "osVersion"
    .end annotation
.end field

.field g:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nfcCapable"
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "imei"
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "msisdn"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/FormFactor;Lcom/meawallet/mtp/StorageTechnology;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/meawallet/mtp/ap;->a:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lcom/meawallet/mtp/ap;->b:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/meawallet/mtp/ap;->c:Lcom/meawallet/mtp/FormFactor;

    .line 54
    iput-object p4, p0, Lcom/meawallet/mtp/ap;->d:Lcom/meawallet/mtp/StorageTechnology;

    .line 55
    iput-object p5, p0, Lcom/meawallet/mtp/ap;->e:Ljava/lang/String;

    .line 56
    iput-object p6, p0, Lcom/meawallet/mtp/ap;->f:Ljava/lang/String;

    .line 57
    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/ap;->g:Ljava/lang/Boolean;

    const/4 p1, 0x0

    .line 58
    iput-object p1, p0, Lcom/meawallet/mtp/ap;->h:Ljava/lang/String;

    .line 59
    iput-object p8, p0, Lcom/meawallet/mtp/ap;->i:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
