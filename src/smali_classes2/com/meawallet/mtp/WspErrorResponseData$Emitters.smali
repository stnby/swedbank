.class final enum Lcom/meawallet/mtp/WspErrorResponseData$Emitters;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/WspErrorResponseData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Emitters"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/WspErrorResponseData$Emitters;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

.field public static final enum AE7460:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

.field public static final enum ISE1086:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

.field public static final enum MC9636:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

.field public static final enum MT2756:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

.field public static final enum VI6593:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 78
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    const-string v1, "MT2756"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->MT2756:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    .line 79
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    const-string v1, "AE7460"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->AE7460:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    .line 80
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    const-string v1, "VI6593"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->VI6593:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    .line 81
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    const-string v1, "MC9636"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->MC9636:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    .line 82
    new-instance v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    const-string v1, "ISE1086"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->ISE1086:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    const/4 v0, 0x5

    .line 76
    new-array v0, v0, [Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->MT2756:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->AE7460:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->VI6593:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->MC9636:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->ISE1086:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    aput-object v1, v0, v6

    sput-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->$VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/WspErrorResponseData$Emitters;
    .locals 1

    .line 76
    const-class v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/WspErrorResponseData$Emitters;
    .locals 1

    .line 76
    sget-object v0, Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->$VALUES:[Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/WspErrorResponseData$Emitters;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    return-object v0
.end method
