.class Lcom/meawallet/mtp/ds;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "ds"


# instance fields
.field private b:Lcom/meawallet/mtp/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/meawallet/mtp/ds;->b:Lcom/meawallet/mtp/ci;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ds;->b:Lcom/meawallet/mtp/ci;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/meawallet/mtp/ds;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->p()Lcom/meawallet/mtp/cg;

    move-result-object v0

    .line 1019
    iget-object v0, v0, Lcom/meawallet/mtp/cg;->a:Ljava/util/List;

    .line 47
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    .line 44
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bm;

    const-string v1, "LdeService is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 49
    sget-object v1, Lcom/meawallet/mtp/ds;->a:Ljava/lang/String;

    const-string v2, "Failed to get not authenticated transaction data from LDE."

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v3
.end method

.method public final a(Ljava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x3

    .line 21
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v2, 0x1

    aput-object p2, v0, v2

    const/4 v2, 0x2

    aput-object p3, v0, v2

    .line 25
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ds;->b:Lcom/meawallet/mtp/ci;

    if-eqz v0, :cond_1

    .line 29
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getCurrencyCode()[B

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/a;->a([B)Ljava/util/Currency;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 30
    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    .line 31
    :goto_0
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getAuthorizedAmount()[B

    move-result-object p2

    invoke-static {p2}, Lcom/meawallet/mtp/a;->b([B)I

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    .line 33
    iget-object v2, p0, Lcom/meawallet/mtp/ds;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v2, p1, v0, p2, p3}, Lcom/meawallet/mtp/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 26
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "LdeService is null."

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 35
    sget-object p2, Lcom/meawallet/mtp/ds;->a:Ljava/lang/String;

    const-string p3, "Failed to add not authenticated transaction data to LDE."

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2, p1, p3, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final b()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/meawallet/mtp/ds;->b:Lcom/meawallet/mtp/ci;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/meawallet/mtp/ds;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->q()V

    :cond_0
    return-void
.end method
