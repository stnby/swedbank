.class final Lcom/meawallet/mtp/x$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RegistrationRequestParameters;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/x;->getRegistrationRequestData([BLcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RegistrationRequestParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;

.field final synthetic b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

.field final synthetic c:Lcom/meawallet/mtp/x;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/x;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;)V
    .locals 0

    .line 514
    iput-object p1, p0, Lcom/meawallet/mtp/x$1;->c:Lcom/meawallet/mtp/x;

    iput-object p2, p0, Lcom/meawallet/mtp/x$1;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;

    iput-object p3, p0, Lcom/meawallet/mtp/x$1;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNewMobilePin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;
    .locals 1

    .line 521
    iget-object v0, p0, Lcom/meawallet/mtp/x$1;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RgkEncryptedData;

    return-object v0
.end method

.method public final getRandomGeneratedKey()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;
    .locals 1

    .line 516
    iget-object v0, p0, Lcom/meawallet/mtp/x$1;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/CmsDPublicKeyEncryptedData;

    return-object v0
.end method
