.class final Lcom/meawallet/mtp/gc;
.super Lcom/meawallet/mtp/gx;
.source "SourceFile"


# instance fields
.field private a:Lcom/meawallet/mtp/MeaEligibilityReceipt;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "eligibilityReceipt"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tacAssetId"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tacAcceptedTimestamp"
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "securityCode"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/MeaEligibilityReceipt;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 27
    invoke-direct {p0, p2}, Lcom/meawallet/mtp/gx;-><init>(Lcom/meawallet/mtp/PaymentNetwork;)V

    .line 29
    iput-object p1, p0, Lcom/meawallet/mtp/gc;->a:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 30
    iput-object p3, p0, Lcom/meawallet/mtp/gc;->e:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/meawallet/mtp/gc;->f:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/meawallet/mtp/gc;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final b()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/meawallet/mtp/gc;->a:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    if-eqz v0, :cond_3

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/gc;->a:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->validate()V
    :try_end_0
    .catch Lcom/meawallet/mtp/bl; {:try_start_0 .. :try_end_0} :catch_0

    .line 1070
    iget-object v0, p0, Lcom/meawallet/mtp/gx;->b:Lcom/meawallet/mtp/PaymentNetwork;

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/meawallet/mtp/gc;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/meawallet/mtp/gc;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/meawallet/mtp/gc;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    goto :goto_0

    .line 56
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Security code length is incorrect"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void

    .line 50
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Payment network is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Lcom/meawallet/mtp/bn;

    invoke-virtual {v0}, Lcom/meawallet/mtp/bl;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v1

    .line 39
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Mea eligibility receipt is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
