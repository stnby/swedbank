.class Lcom/meawallet/mtp/JsonUtil$ByteArraySerializer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;
.implements Lcom/google/gson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/JsonUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ByteArraySerializer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer<",
        "Lcom/mastercard/mpsdk/utils/bytes/ByteArray;",
        ">;",
        "Lcom/google/gson/JsonSerializer<",
        "Lcom/mastercard/mpsdk/utils/bytes/ByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/meawallet/mtp/JsonUtil$1;)V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/meawallet/mtp/JsonUtil$ByteArraySerializer;-><init>()V

    return-void
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 0

    .line 123
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/meawallet/mtp/h;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 0

    .line 112
    invoke-virtual {p0, p1, p2, p3}, Lcom/meawallet/mtp/JsonUtil$ByteArraySerializer;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 0

    .line 117
    new-instance p2, Lcom/google/gson/JsonPrimitive;

    invoke-static {p1}, Lcom/meawallet/mtp/h;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    return-object p2
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 0

    .line 112
    check-cast p1, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    invoke-virtual {p0, p1, p2, p3}, Lcom/meawallet/mtp/JsonUtil$ByteArraySerializer;->serialize(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;

    move-result-object p1

    return-object p1
.end method
