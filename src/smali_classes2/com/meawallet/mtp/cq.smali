.class final enum Lcom/meawallet/mtp/cq;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/cq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/meawallet/mtp/cq;

.field public static final enum b:Lcom/meawallet/mtp/cq;

.field public static final enum c:Lcom/meawallet/mtp/cq;

.field public static final enum d:Lcom/meawallet/mtp/cq;

.field public static final enum e:Lcom/meawallet/mtp/cq;

.field public static final enum f:Lcom/meawallet/mtp/cq;

.field public static final enum g:Lcom/meawallet/mtp/cq;

.field public static final enum h:Lcom/meawallet/mtp/cq;

.field public static final enum i:Lcom/meawallet/mtp/cq;

.field public static final enum j:Lcom/meawallet/mtp/cq;

.field private static final synthetic k:[Lcom/meawallet/mtp/cq;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 4
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "AUTHENTICATE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->a:Lcom/meawallet/mtp/cq;

    .line 5
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "CONSENT_FAILED"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->b:Lcom/meawallet/mtp/cq;

    .line 6
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "REFRESH_CREDENTIALS"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->c:Lcom/meawallet/mtp/cq;

    .line 7
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "UNLOCK_DEVICE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->d:Lcom/meawallet/mtp/cq;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "KEY_INVALIDATED"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->e:Lcom/meawallet/mtp/cq;

    .line 9
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "CURRENCY_AMOUNT_LIMIT_EXCEEDED"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->f:Lcom/meawallet/mtp/cq;

    .line 12
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "CONTEXT_NOT_MATCHING"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->g:Lcom/meawallet/mtp/cq;

    .line 13
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "UNSUPPORTED_TRANSIT"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->h:Lcom/meawallet/mtp/cq;

    .line 14
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "INSUFFICIENT_POI_AUTHENTICATION"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->i:Lcom/meawallet/mtp/cq;

    .line 15
    new-instance v0, Lcom/meawallet/mtp/cq;

    const-string v1, "TRANSACTION_CONDITIONS_NOT_ALLOWED"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/meawallet/mtp/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/cq;->j:Lcom/meawallet/mtp/cq;

    const/16 v0, 0xa

    .line 3
    new-array v0, v0, [Lcom/meawallet/mtp/cq;

    sget-object v1, Lcom/meawallet/mtp/cq;->a:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/cq;->b:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/cq;->c:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/cq;->d:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/cq;->e:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/cq;->f:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/cq;->g:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v8

    sget-object v1, Lcom/meawallet/mtp/cq;->h:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v9

    sget-object v1, Lcom/meawallet/mtp/cq;->i:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v10

    sget-object v1, Lcom/meawallet/mtp/cq;->j:Lcom/meawallet/mtp/cq;

    aput-object v1, v0, v11

    sput-object v0, Lcom/meawallet/mtp/cq;->k:[Lcom/meawallet/mtp/cq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/cq;
    .locals 1

    .line 3
    const-class v0, Lcom/meawallet/mtp/cq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/cq;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/cq;
    .locals 1

    .line 3
    sget-object v0, Lcom/meawallet/mtp/cq;->k:[Lcom/meawallet/mtp/cq;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/cq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/cq;

    return-object v0
.end method
