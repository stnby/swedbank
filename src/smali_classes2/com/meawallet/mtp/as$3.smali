.class final Lcom/meawallet/mtp/as$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/as;->a(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)Lcom/mastercard/mpsdk/componentinterface/AlternateContactlessPaymentData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/meawallet/mtp/as$3;->a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAid()[B
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/meawallet/mtp/as$3;->a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->aid:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getCiacDecline()[B
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/meawallet/mtp/as$3;->a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/meawallet/mtp/as$3;->a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->ciacDecline:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCvrMaskAnd()[B
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/meawallet/mtp/as$3;->a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->cvrMaskAnd:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentFci()[B
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/meawallet/mtp/as$3;->a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->paymentFci:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getgpoResponse()[B
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/meawallet/mtp/as$3;->a:Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/AlternateContactlessPaymentDataJson;->gpoResponse:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method
