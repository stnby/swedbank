.class final enum Lcom/meawallet/mtp/ae;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/ae;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/meawallet/mtp/ae;

.field public static final enum b:Lcom/meawallet/mtp/ae;

.field public static final enum c:Lcom/meawallet/mtp/ae;

.field public static final enum d:Lcom/meawallet/mtp/ae;

.field private static final synthetic e:[Lcom/meawallet/mtp/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 4
    new-instance v0, Lcom/meawallet/mtp/ae;

    const-string v1, "RETRY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ae;->a:Lcom/meawallet/mtp/ae;

    .line 5
    new-instance v0, Lcom/meawallet/mtp/ae;

    const-string v1, "INVALIDATE_SESSION_AND_TRY_AGAIN"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ae;->b:Lcom/meawallet/mtp/ae;

    .line 6
    new-instance v0, Lcom/meawallet/mtp/ae;

    const-string v1, "CLEAR_QUEUE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ae;->c:Lcom/meawallet/mtp/ae;

    .line 7
    new-instance v0, Lcom/meawallet/mtp/ae;

    const-string v1, "REMOVE_FROM_QUEUE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const/4 v0, 0x4

    .line 3
    new-array v0, v0, [Lcom/meawallet/mtp/ae;

    sget-object v1, Lcom/meawallet/mtp/ae;->a:Lcom/meawallet/mtp/ae;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/ae;->b:Lcom/meawallet/mtp/ae;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/ae;->c:Lcom/meawallet/mtp/ae;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    aput-object v1, v0, v5

    sput-object v0, Lcom/meawallet/mtp/ae;->e:[Lcom/meawallet/mtp/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/ae;
    .locals 1

    .line 3
    const-class v0, Lcom/meawallet/mtp/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/ae;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/ae;
    .locals 1

    .line 3
    sget-object v0, Lcom/meawallet/mtp/ae;->e:[Lcom/meawallet/mtp/ae;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/ae;

    return-object v0
.end method
