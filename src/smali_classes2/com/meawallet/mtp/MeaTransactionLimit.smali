.class public Lcom/meawallet/mtp/MeaTransactionLimit;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Currency;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currency"
    .end annotation
.end field

.field private b:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "maxAmount"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Currency;I)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/meawallet/mtp/MeaTransactionLimit;->a:Ljava/util/Currency;

    .line 22
    iput p2, p0, Lcom/meawallet/mtp/MeaTransactionLimit;->b:I

    return-void
.end method


# virtual methods
.method public getCurrency()Ljava/util/Currency;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTransactionLimit;->a:Ljava/util/Currency;

    return-object v0
.end method

.method public getMaxAmount()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/meawallet/mtp/MeaTransactionLimit;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MeaTransactionLimit[\tcurrency = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/meawallet/mtp/MeaTransactionLimit;->a:Ljava/util/Currency;

    .line 47
    invoke-virtual {v1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tmaxAmount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/meawallet/mtp/MeaTransactionLimit;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
