.class public Lcom/meawallet/mtp/MeaTokenInfo;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenPanSuffix"
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "accountPanSuffix"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenExpiry"
    .end annotation
.end field

.field private d:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dsrpCapable"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountPanSuffix()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getDsrpCapable()Ljava/lang/Boolean;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getTokenExpiry()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenPanSuffix()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method validate()V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 65
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 67
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/meawallet/mtp/MeaTokenInfo;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "DSRP capable is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Token expiry is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Account PAN suffix is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Token PAN suffix is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
