.class final enum Lcom/meawallet/mtp/MeaTransactionDataType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaTransactionDataType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CONTACTLESS:Lcom/meawallet/mtp/MeaTransactionDataType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum REMOTE:Lcom/meawallet/mtp/MeaTransactionDataType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaTransactionDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 6
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionDataType;

    const-string v1, "CONTACTLESS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaTransactionDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionDataType;->CONTACTLESS:Lcom/meawallet/mtp/MeaTransactionDataType;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionDataType;

    const-string v1, "REMOTE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaTransactionDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionDataType;->REMOTE:Lcom/meawallet/mtp/MeaTransactionDataType;

    const/4 v0, 0x2

    .line 5
    new-array v0, v0, [Lcom/meawallet/mtp/MeaTransactionDataType;

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionDataType;->CONTACTLESS:Lcom/meawallet/mtp/MeaTransactionDataType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionDataType;->REMOTE:Lcom/meawallet/mtp/MeaTransactionDataType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionDataType;->a:[Lcom/meawallet/mtp/MeaTransactionDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaTransactionDataType;
    .locals 1

    .line 5
    const-class v0, Lcom/meawallet/mtp/MeaTransactionDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaTransactionDataType;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaTransactionDataType;
    .locals 1

    .line 5
    sget-object v0, Lcom/meawallet/mtp/MeaTransactionDataType;->a:[Lcom/meawallet/mtp/MeaTransactionDataType;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaTransactionDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaTransactionDataType;

    return-object v0
.end method
