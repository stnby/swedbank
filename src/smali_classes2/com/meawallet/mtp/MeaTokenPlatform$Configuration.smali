.class public Lcom/meawallet/mtp/MeaTokenPlatform$Configuration;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaTokenPlatform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Configuration"
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "MeaTokenPlatform$Configuration"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildType()Ljava/lang/String;
    .locals 1

    .line 1236
    invoke-static {}, Lcom/meawallet/mtp/dd;->w()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static cdCvmModel()Ljava/lang/String;
    .locals 1

    .line 1248
    invoke-static {}, Lcom/meawallet/mtp/dd;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isSaveAuthWhenLocked()Z
    .locals 1

    .line 1261
    invoke-static {}, Lcom/meawallet/mtp/dd;->y()Z

    move-result v0

    return v0
.end method

.method public static versionCode()I
    .locals 1

    .line 1212
    invoke-static {}, Lcom/meawallet/mtp/dd;->u()I

    move-result v0

    return v0
.end method

.method public static versionName()Ljava/lang/String;
    .locals 1

    .line 1224
    invoke-static {}, Lcom/meawallet/mtp/dd;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
