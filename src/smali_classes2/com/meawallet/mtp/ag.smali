.class final enum Lcom/meawallet/mtp/ag;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/ag;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/meawallet/mtp/ag;

.field public static final enum b:Lcom/meawallet/mtp/ag;

.field public static final enum c:Lcom/meawallet/mtp/ag;

.field public static final enum d:Lcom/meawallet/mtp/ag;

.field public static final enum e:Lcom/meawallet/mtp/ag;

.field public static final enum f:Lcom/meawallet/mtp/ag;

.field public static final enum g:Lcom/meawallet/mtp/ag;

.field public static final enum h:Lcom/meawallet/mtp/ag;

.field public static final enum i:Lcom/meawallet/mtp/ag;

.field private static final synthetic j:[Lcom/meawallet/mtp/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 4
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "PENDING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->a:Lcom/meawallet/mtp/ag;

    .line 5
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "EXECUTING"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    .line 6
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "SENT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->c:Lcom/meawallet/mtp/ag;

    .line 7
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "SUCCESS"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "FAILED"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 9
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "CANCELED"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 10
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "UNKNOWN"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->g:Lcom/meawallet/mtp/ag;

    .line 11
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "DUPLICATE"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    .line 12
    new-instance v0, Lcom/meawallet/mtp/ag;

    const-string v1, "TOO_MANY_COMMANDS_IN_QUEUE"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/meawallet/mtp/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    const/16 v0, 0x9

    .line 3
    new-array v0, v0, [Lcom/meawallet/mtp/ag;

    sget-object v1, Lcom/meawallet/mtp/ag;->a:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/ag;->c:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/ag;->g:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v8

    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v9

    sget-object v1, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    aput-object v1, v0, v10

    sput-object v0, Lcom/meawallet/mtp/ag;->j:[Lcom/meawallet/mtp/ag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/ag;
    .locals 1

    .line 3
    const-class v0, Lcom/meawallet/mtp/ag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/ag;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/ag;
    .locals 1

    .line 3
    sget-object v0, Lcom/meawallet/mtp/ag;->j:[Lcom/meawallet/mtp/ag;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/ag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/ag;

    return-object v0
.end method
