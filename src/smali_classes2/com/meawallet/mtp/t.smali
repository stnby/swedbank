.class final Lcom/meawallet/mtp/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

.field b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

.field final c:Lcom/meawallet/mtp/fi;

.field final d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

.field e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

.field final f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

.field final g:Lcom/meawallet/mtp/ad;

.field private final h:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/ad;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/t;->h:Ljava/util/concurrent/atomic/AtomicLong;

    .line 37
    iput-object p1, p0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    .line 38
    iput-object p2, p0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 39
    iput-object p4, p0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    .line 40
    iput-object p5, p0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    .line 41
    iput-object p3, p0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    .line 42
    iput-object p6, p0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    .line 43
    iput-object p7, p0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/meawallet/mtp/fj;
    .locals 12

    .line 139
    new-instance v11, Lcom/meawallet/mtp/fj;

    invoke-virtual {p0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    iget-object v5, p0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v7, p0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v10}, Lcom/meawallet/mtp/fj;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v11
.end method

.method final a()Ljava/lang/String;
    .locals 4

    const-string v0, "UTC"

    .line 165
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 166
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    const-string v1, "yyyyMMddHHmmssSSS"

    .line 168
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 170
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/meawallet/mtp/t;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/meawallet/mtp/q;
    .locals 12

    .line 152
    new-instance v11, Lcom/meawallet/mtp/q;

    invoke-virtual {p0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v3, p0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v4, p0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    iget-object v5, p0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v6, p0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v7, p0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v10, p0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v0, v11

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v10}, Lcom/meawallet/mtp/q;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    return-object v11
.end method
