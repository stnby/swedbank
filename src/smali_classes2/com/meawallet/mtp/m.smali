.class Lcom/meawallet/mtp/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "m"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

.field private d:Lcom/meawallet/mtp/ci;

.field private volatile e:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/meawallet/mtp/ci;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;)V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lcom/meawallet/mtp/m;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 32
    invoke-static {p1}, Lcom/meawallet/mtp/dx;->a(Landroid/content/Context;)V

    .line 33
    iput-object p1, p0, Lcom/meawallet/mtp/m;->b:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/meawallet/mtp/m;->d:Lcom/meawallet/mtp/ci;

    .line 35
    iput-object p3, p0, Lcom/meawallet/mtp/m;->c:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    return-void
.end method

.method private declared-synchronized a([Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    .line 57
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 59
    array-length v0, p1

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v1, p1, v2

    .line 60
    invoke-direct {p0, v1}, Lcom/meawallet/mtp/m;->b(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 63
    :cond_0
    invoke-direct {p0}, Lcom/meawallet/mtp/m;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 56
    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized b()V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x1

    .line 89
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/meawallet/mtp/m;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 91
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARDS_PENDING_REPLENISH"

    new-instance v2, Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/meawallet/mtp/m;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Ljava/util/Set;)Z

    const/4 v0, 0x0

    .line 93
    iput-object v0, p0, Lcom/meawallet/mtp/m;->e:Ljava/util/concurrent/CopyOnWriteArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 88
    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    .line 83
    :try_start_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/m;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/meawallet/mtp/m;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 82
    monitor-exit p0

    throw p1
.end method

.method private c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    :try_start_0
    iget-object v1, p0, Lcom/meawallet/mtp/m;->d:Lcom/meawallet/mtp/ci;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/ci;Z)Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 135
    sget-object v2, Lcom/meawallet/mtp/m;->a:Ljava/lang/String;

    const-string v3, "Failed to fetch MeaCards."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method final declared-synchronized a()Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 39
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/m;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/m;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 43
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARDS_PENDING_REPLENISH"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->g(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x1

    .line 45
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 47
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/meawallet/mtp/m;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->addAll(Ljava/util/Collection;)Z

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/m;->e:Ljava/util/concurrent/CopyOnWriteArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 38
    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    .line 75
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 77
    invoke-virtual {p0}, Lcom/meawallet/mtp/m;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 79
    invoke-direct {p0}, Lcom/meawallet/mtp/m;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 74
    monitor-exit p0

    throw p1
.end method

.method final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/CdCvmType;",
            ">;)V"
        }
    .end annotation

    .line 115
    invoke-direct {p0}, Lcom/meawallet/mtp/m;->c()Ljava/util/List;

    move-result-object v0

    .line 116
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 118
    new-array v2, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    .line 121
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/meawallet/mtp/MeaCard;

    invoke-interface {v4}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 124
    :cond_0
    invoke-virtual {p0, p1, v2}, Lcom/meawallet/mtp/m;->a(Ljava/util/List;[Ljava/lang/String;)V

    return-void
.end method

.method final a(Ljava/util/List;[Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/CdCvmType;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x2

    .line 97
    new-array v0, v0, [Ljava/lang/Object;

    .line 98
    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 100
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0

    .line 102
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 104
    invoke-direct {p0, p2}, Lcom/meawallet/mtp/m;->a([Ljava/lang/String;)V

    .line 106
    iget-object p1, p0, Lcom/meawallet/mtp/m;->b:Landroid/content/Context;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p1

    .line 108
    iget-object p2, p0, Lcom/meawallet/mtp/m;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/meawallet/mtp/m;->c:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    invoke-static {p2, v0, p1}, Lcom/meawallet/mtp/bo;->a(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Z)V

    return-void

    .line 110
    :cond_0
    new-array p1, v3, [Ljava/lang/Object;

    aput-object v0, p1, v2

    return-void
.end method
