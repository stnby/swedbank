.class Lcom/meawallet/mtp/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final j:Ljava/lang/String; = "ax"

.field private static k:Lcom/meawallet/mtp/ax;


# instance fields
.field a:Lcom/meawallet/mtp/MeaCardProvisionListener;

.field b:Lcom/meawallet/mtp/MeaCardReplenishListener;

.field c:Lcom/meawallet/mtp/MeaWalletPinListener;

.field d:Lcom/meawallet/mtp/MeaAuthenticationListener;

.field e:Lcom/meawallet/mtp/MeaListener;

.field f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

.field g:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/String;",
            "Lcom/meawallet/mtp/MeaCardPinListener;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/String;",
            "Lcom/meawallet/mtp/MeaContactlessTransactionListener;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/String;",
            "Lcom/meawallet/mtp/MeaCardListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/ax;->g:Ljava/util/SortedMap;

    .line 26
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/ax;->h:Ljava/util/SortedMap;

    .line 27
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/ax;->i:Ljava/util/SortedMap;

    return-void
.end method

.method static declared-synchronized a()Lcom/meawallet/mtp/ax;
    .locals 2

    const-class v0, Lcom/meawallet/mtp/ax;

    monitor-enter v0

    .line 31
    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/ax;->k:Lcom/meawallet/mtp/ax;

    if-nez v1, :cond_0

    .line 32
    new-instance v1, Lcom/meawallet/mtp/ax;

    invoke-direct {v1}, Lcom/meawallet/mtp/ax;-><init>()V

    sput-object v1, Lcom/meawallet/mtp/ax;->k:Lcom/meawallet/mtp/ax;

    .line 35
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/ax;->k:Lcom/meawallet/mtp/ax;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 30
    monitor-exit v0

    throw v1
.end method


# virtual methods
.method final declared-synchronized a(Lcom/meawallet/mtp/MeaAuthenticationListener;)V
    .locals 0

    monitor-enter p0

    .line 56
    :try_start_0
    iput-object p1, p0, Lcom/meawallet/mtp/ax;->d:Lcom/meawallet/mtp/MeaAuthenticationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 55
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized a(Lcom/meawallet/mtp/MeaCardProvisionListener;)V
    .locals 0

    monitor-enter p0

    .line 39
    :try_start_0
    iput-object p1, p0, Lcom/meawallet/mtp/ax;->a:Lcom/meawallet/mtp/MeaCardProvisionListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 38
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized a(Lcom/meawallet/mtp/MeaCardReplenishListener;)V
    .locals 0

    monitor-enter p0

    .line 43
    :try_start_0
    iput-object p1, p0, Lcom/meawallet/mtp/ax;->b:Lcom/meawallet/mtp/MeaCardReplenishListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 42
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized a(Lcom/meawallet/mtp/MeaWalletPinListener;)V
    .locals 0

    monitor-enter p0

    .line 47
    :try_start_0
    iput-object p1, p0, Lcom/meawallet/mtp/ax;->c:Lcom/meawallet/mtp/MeaWalletPinListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 46
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->g:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 82
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 1

    monitor-enter p0

    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->i:Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 140
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardPinListener;)V
    .locals 1

    monitor-enter p0

    .line 67
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->g:Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 66
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized a(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    .locals 1

    monitor-enter p0

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->h:Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 87
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 63
    :try_start_0
    iput-object v0, p0, Lcom/meawallet/mtp/ax;->d:Lcom/meawallet/mtp/MeaAuthenticationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 62
    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->h:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 91
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized c(Ljava/lang/String;)Lcom/meawallet/mtp/MeaContactlessTransactionListener;
    .locals 3

    monitor-enter p0

    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->h:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/MeaContactlessTransactionListener;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    .line 99
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    .line 96
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 71
    :try_start_0
    iput-object v0, p0, Lcom/meawallet/mtp/ax;->a:Lcom/meawallet/mtp/MeaCardProvisionListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 70
    monitor-exit p0

    throw v0
.end method

.method final d(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardPinListener;
    .locals 3

    .line 130
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->g:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/MeaCardPinListener;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    .line 134
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    :cond_0
    return-object v0
.end method

.method final declared-synchronized d()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 75
    :try_start_0
    iput-object v0, p0, Lcom/meawallet/mtp/ax;->b:Lcom/meawallet/mtp/MeaCardReplenishListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 74
    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized e(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardListener;
    .locals 1

    monitor-enter p0

    .line 145
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->i:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/MeaCardListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized e()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 79
    :try_start_0
    iput-object v0, p0, Lcom/meawallet/mtp/ax;->c:Lcom/meawallet/mtp/MeaWalletPinListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 78
    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized f(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ax;->i:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 148
    monitor-exit p0

    throw p1
.end method
