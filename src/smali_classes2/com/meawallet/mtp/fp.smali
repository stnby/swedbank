.class Lcom/meawallet/mtp/fp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "fp"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()I
    .locals 1

    const/16 v0, 0x3c

    return v0
.end method

.method static a(J)V
    .locals 3

    const/4 v0, 0x1

    .line 114
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 116
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "LAST_AUTH_TIME"

    invoke-virtual {v0, v1, p0, p1}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;J)Z

    return-void
.end method

.method static b()Z
    .locals 7

    .line 25
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->DEVICE_UNLOCK_REQ_FOR_EACH_TXS:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 26
    invoke-static {}, Lcom/meawallet/mtp/aq;->a()J

    move-result-wide v3

    .line 27
    invoke-static {}, Lcom/meawallet/mtp/aq;->d()Z

    move-result v0

    .line 28
    invoke-static {v3, v4}, Lcom/meawallet/mtp/al;->b(J)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v3, 0x0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x4

    .line 33
    new-array v5, v5, [Ljava/lang/Object;

    .line 35
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    sget-object v2, Lcom/meawallet/mtp/BuildConfig;->DEVICE_UNLOCK_REQ_FOR_EACH_TXS:Ljava/lang/Boolean;

    aput-object v2, v5, v1

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x3

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v1

    return v4
.end method

.method static c()Z
    .locals 12

    .line 41
    invoke-static {}, Lcom/meawallet/mtp/fp;->i()J

    move-result-wide v0

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x1

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    cmp-long v8, v0, v5

    if-lez v8, :cond_0

    .line 46
    invoke-static {v0, v1}, Lcom/meawallet/mtp/al;->b(J)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    :goto_0
    const/4 v10, 0x3

    .line 51
    new-array v10, v10, [Ljava/lang/Object;

    .line 52
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v10, v7

    if-nez v8, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    sub-long v5, v2, v0

    :goto_1
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v4

    const/4 v0, 0x2

    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v0

    return v9
.end method

.method static d()Z
    .locals 6

    .line 58
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "TRANSACTIONS_SINCE_AUTH"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->c(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ltz v0, :cond_0

    if-gtz v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x3

    .line 67
    new-array v4, v4, [Ljava/lang/Object;

    .line 68
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    const/4 v0, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    return v3
.end method

.method static e()V
    .locals 2

    const-wide/16 v0, -0x1

    .line 1134
    invoke-static {v0, v1}, Lcom/meawallet/mtp/fp;->a(J)V

    .line 77
    invoke-static {}, Lcom/meawallet/mtp/fp;->k()V

    .line 78
    invoke-static {}, Lcom/meawallet/mtp/fp;->h()V

    return-void
.end method

.method static f()Z
    .locals 2

    .line 87
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "AUTHENTICATED_WHEN_DEVICE_WAS_LOCKED"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static g()V
    .locals 3

    .line 96
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "AUTHENTICATED_WHEN_DEVICE_WAS_LOCKED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Z)Z

    return-void
.end method

.method static h()V
    .locals 3

    .line 105
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->SAVE_AUTH_WHEN_LOCKED:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/meawallet/mtp/fp;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "AUTHENTICATED_WHEN_DEVICE_WAS_LOCKED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Z)Z

    :cond_0
    return-void
.end method

.method static i()J
    .locals 4

    .line 125
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "LAST_AUTH_TIME"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method static j()V
    .locals 2

    .line 140
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "TRANSACTIONS_SINCE_AUTH"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->i(Ljava/lang/String;)Z

    return-void
.end method

.method static k()V
    .locals 3

    .line 146
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "TRANSACTIONS_SINCE_AUTH"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;I)Z

    return-void
.end method
