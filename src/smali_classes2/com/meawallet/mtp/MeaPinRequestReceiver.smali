.class public abstract Lcom/meawallet/mtp/MeaPinRequestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "MeaPinRequestReceiver"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract handleOnCardPinResetIntent(Landroid/content/Context;Ljava/lang/String;)V
.end method

.method protected abstract handleOnWalletPinResetIntent(Landroid/content/Context;)V
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .line 26
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x1f5

    if-nez v0, :cond_0

    .line 27
    sget-object p1, Lcom/meawallet/mtp/MeaPinRequestReceiver;->a:Ljava/lang/String;

    const-string p2, "Received intent with null action."

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1, v2, p2, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 32
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.meawallet.mtp.intent.ON_WALLET_PIN_RESET_REQUEST"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/MeaPinRequestReceiver;->handleOnWalletPinResetIntent(Landroid/content/Context;)V

    return-void

    .line 37
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.meawallet.mtp.intent.ON_CARD_PIN_RESET_REQUEST"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "card_id"

    .line 40
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 42
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    sget-object p1, Lcom/meawallet/mtp/MeaPinRequestReceiver;->a:Ljava/lang/String;

    const-string p2, "Failed to handle Mobile PIN change broadcast, passed card id in Intent is null"

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1, v2, p2, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 48
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/meawallet/mtp/MeaPinRequestReceiver;->handleOnCardPinResetIntent(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    return-void
.end method
