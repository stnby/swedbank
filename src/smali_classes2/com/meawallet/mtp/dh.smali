.class Lcom/meawallet/mtp/dh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;


# static fields
.field private static final a:Ljava/lang/String; = "dh"


# instance fields
.field private final b:Lcom/meawallet/mtp/ci;

.field private final c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/meawallet/mtp/dh;->b:Lcom/meawallet/mtp/ci;

    .line 22
    iput-object p2, p0, Lcom/meawallet/mtp/dh;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    return-void
.end method


# virtual methods
.method public getEncryptedDeviceFingerPrint()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 55
    :try_start_0
    iget-object v2, p0, Lcom/meawallet/mtp/dh;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v2}, Lcom/meawallet/mtp/ci;->g()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 58
    sget-object v2, Lcom/meawallet/mtp/dh;->a:Ljava/lang/String;

    const/16 v3, 0x1f5

    const-string v4, "Failed to read encrypted device fingerprint from LDE, fingerprint is null."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4, v5}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    .line 63
    :cond_0
    iget-object v3, p0, Lcom/meawallet/mtp/dh;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    invoke-static {v2}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;->encryptWalletData([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v2
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception v2

    .line 65
    sget-object v3, Lcom/meawallet/mtp/dh;->a:Ljava/lang/String;

    const-string v4, "Failed to read encrypted device fingerprint from LDE."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public getPaymentAppInstanceId()[B
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 29
    :try_start_0
    iget-object v2, p0, Lcom/meawallet/mtp/dh;->b:Lcom/meawallet/mtp/ci;

    invoke-interface {v2}, Lcom/meawallet/mtp/ci;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 32
    sget-object v2, Lcom/meawallet/mtp/dh;->a:Ljava/lang/String;

    const/16 v3, 0x1f5

    const-string v4, "Failed to read payment app instance id from LDE, payment app instance id is null."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4, v5}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    .line 38
    :cond_0
    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception v2

    .line 40
    sget-object v3, Lcom/meawallet/mtp/dh;->a:Ljava/lang/String;

    const-string v4, "Failed to read payment app instance id from LDE."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public getPaymentAppProviderId()[B
    .locals 2

    const-string v0, "SWEDBANKLIETUVA"

    .line 48
    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0
.end method

.method public onKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V
    .locals 0

    return-void
.end method
