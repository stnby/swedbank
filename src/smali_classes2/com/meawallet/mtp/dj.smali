.class Lcom/meawallet/mtp/dj;
.super Lcom/meawallet/mtp/cv;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "dj"


# instance fields
.field b:Z

.field private c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Lcom/meawallet/mtp/cv;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/meawallet/mtp/dj;->b:Z

    return-void
.end method

.method static synthetic a(Lcom/meawallet/mtp/dj;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/meawallet/mtp/dj;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lcom/meawallet/mtp/dj;->b:Z

    .line 56
    invoke-static {}, Lcom/meawallet/mtp/fp;->e()V

    .line 58
    iget-object v0, p0, Lcom/meawallet/mtp/dj;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/meawallet/mtp/dj;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/h;->a([B)V

    const/4 v0, 0x0

    .line 61
    iput-object v0, p0, Lcom/meawallet/mtp/dj;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    :cond_0
    return-void
.end method

.method public final a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;)V
    .locals 3

    const/4 v0, 0x1

    .line 102
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 104
    sget-object v1, Lcom/meawallet/mtp/dj$2;->a:[I

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result p1

    aget p1, v1, p1

    if-eq p1, v0, :cond_0

    .line 110
    invoke-static {}, Lcom/meawallet/mtp/fp;->j()V

    return-void

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/dj;->a()V

    return-void
.end method

.method public final a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V
    .locals 4

    const/4 v0, 0x1

    .line 90
    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;->getEncryptedData()[B

    move-result-object v2

    invoke-static {v2}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 92
    invoke-virtual {p0}, Lcom/meawallet/mtp/dj;->a()V

    .line 94
    iput-object p1, p0, Lcom/meawallet/mtp/dj;->c:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    .line 95
    iput-boolean v0, p0, Lcom/meawallet/mtp/dj;->b:Z

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/meawallet/mtp/fp;->a(J)V

    return-void
.end method

.method public final b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;
    .locals 1

    .line 31
    new-instance v0, Lcom/meawallet/mtp/dj$1;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/dj$1;-><init>(Lcom/meawallet/mtp/dj;)V

    return-object v0
.end method

.method public getTimeOfLastSuccessfulCdCvm()J
    .locals 2

    .line 85
    invoke-static {}, Lcom/meawallet/mtp/fp;->i()J

    move-result-wide v0

    return-wide v0
.end method

.method public isCdCvmEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isCdCvmSuccessful(Lcom/mastercard/mpsdk/componentinterface/Card;)Z
    .locals 6

    .line 74
    invoke-static {}, Lcom/meawallet/mtp/fp;->c()Z

    move-result p1

    .line 75
    invoke-static {}, Lcom/meawallet/mtp/fp;->d()Z

    move-result v0

    const/4 v1, 0x3

    .line 77
    new-array v1, v1, [Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/meawallet/mtp/dj;->b:Z

    .line 78
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    .line 80
    iget-boolean v1, p0, Lcom/meawallet/mtp/dj;->b:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    return v4

    :cond_0
    return v3
.end method
