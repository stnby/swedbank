.class public Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaProductConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ExtraTextValue"
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/MeaProductConfig;

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentAppProviderId"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentAppId"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentAppInstanceId"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenUniqueReference"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/meawallet/mtp/MeaProductConfig;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;->a:Lcom/meawallet/mtp/MeaProductConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPaymentAppId()Ljava/lang/String;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentAppInstanceId()Ljava/lang/String;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentAppProviderId()Ljava/lang/String;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenUniqueReference()Ljava/lang/String;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;->e:Ljava/lang/String;

    return-object v0
.end method
