.class Lcom/meawallet/mtp/gt;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mobileKeysetId"
    .end annotation
.end field

.field b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "walletSessionCode"
    .end annotation
.end field

.field c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "walletKeysetId"
    .end annotation
.end field

.field d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "remoteManagementUrl"
    .end annotation
.end field

.field e:Lcom/meawallet/mtp/gu;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mobileKeys"
    .end annotation
.end field

.field f:Lcom/meawallet/mtp/gu;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "walletKeys"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method


# virtual methods
.method validate()V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 85
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 87
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 89
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->e:Lcom/meawallet/mtp/gu;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->e:Lcom/meawallet/mtp/gu;

    invoke-virtual {v0}, Lcom/meawallet/mtp/gu;->validate()V

    .line 95
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->f:Lcom/meawallet/mtp/gu;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/meawallet/mtp/gt;->f:Lcom/meawallet/mtp/gu;

    invoke-virtual {v0}, Lcom/meawallet/mtp/gu;->validate()V

    return-void

    .line 95
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Wallet keys container is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Mobile keys container is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Remote management URL is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Wallet session code is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Wallet key set id is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_5
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Mobile key set id is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
