.class Lcom/meawallet/mtp/fy;
.super Lcom/meawallet/mtp/fv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/fv<",
        "Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;",
        "Lcom/meawallet/mtp/ga;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "fy"


# instance fields
.field private final h:Ljava/lang/String;

.field private final i:Lcom/meawallet/mtp/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;)V
    .locals 6

    .line 32
    new-instance v4, Lcom/meawallet/mtp/fz;

    invoke-direct {v4, p4, p5, p6}, Lcom/meawallet/mtp/fz;-><init>(Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/fv;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V

    .line 38
    iput-object p5, p0, Lcom/meawallet/mtp/fy;->h:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/meawallet/mtp/fy;->i:Lcom/meawallet/mtp/ci;

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/d;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/ga;",
            ">;)V"
        }
    .end annotation

    .line 1147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 94
    invoke-virtual {p1}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 96
    check-cast v0, Lcom/meawallet/mtp/MeaCoreListener;

    .line 3041
    iget-object p1, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 96
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 4036
    :cond_0
    iget-object v0, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 101
    check-cast v0, Lcom/meawallet/mtp/ga;

    const/16 v1, 0x1f5

    if-nez v0, :cond_1

    .line 4147
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 104
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const-string v2, "Complete authentication response data element is null."

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 110
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/fy$1;->a:[I

    .line 5036
    iget-object v2, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 110
    check-cast v2, Lcom/meawallet/mtp/ga;

    .line 6012
    iget-object v2, v2, Lcom/meawallet/mtp/ga;->a:Lcom/meawallet/mtp/MeaAuthenticationResult;

    .line 110
    invoke-virtual {v2}, Lcom/meawallet/mtp/MeaAuthenticationResult;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 11147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 133
    check-cast v0, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;

    new-instance v2, Lcom/meawallet/mtp/cy;

    const-string v3, "Wrong authentication result type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 12036
    iget-object p1, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 134
    check-cast p1, Lcom/meawallet/mtp/ga;

    .line 13012
    iget-object p1, p1, Lcom/meawallet/mtp/ga;->a:Lcom/meawallet/mtp/MeaAuthenticationResult;

    aput-object p1, v4, v5

    .line 134
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v1, p1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 133
    invoke-interface {v0, v2}, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;->onFailure(Ljava/lang/Object;)V

    goto :goto_0

    .line 10147
    :pswitch_0
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 129
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x261

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 9147
    :pswitch_1
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 125
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x260

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 8147
    :pswitch_2
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 121
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x25f

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 7147
    :pswitch_3
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 117
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x25e

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;->onFailure(Ljava/lang/Object;)V

    return-void

    .line 6147
    :pswitch_4
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 113
    check-cast p1, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;

    new-instance v0, Lcom/meawallet/mtp/ct;

    iget-object v1, p0, Lcom/meawallet/mtp/fy;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;->onSuccess(Lcom/meawallet/mtp/MeaCard;)V

    return-void

    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method final b()Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 44
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method

.method final b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/ga;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 55
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 58
    :try_start_0
    const-class v0, Lcom/meawallet/mtp/ga;

    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/fy;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/ga;

    if-eqz p1, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/meawallet/mtp/ga;->validate()V

    .line 69
    sget-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->SUCCESS:Lcom/meawallet/mtp/MeaAuthenticationResult;

    .line 1012
    iget-object v1, p1, Lcom/meawallet/mtp/ga;->a:Lcom/meawallet/mtp/MeaAuthenticationResult;

    .line 69
    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/MeaAuthenticationResult;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Lcom/meawallet/mtp/ct;

    iget-object v1, p0, Lcom/meawallet/mtp/fy;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/meawallet/mtp/fy;->i:Lcom/meawallet/mtp/ci;

    invoke-static {v0, v1}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-virtual {p0}, Lcom/meawallet/mtp/fy;->h()Lcom/meawallet/mtp/ci;

    move-result-object v1

    sget-object v2, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_COMPLETE:Lcom/meawallet/mtp/MeaCardState;

    invoke-interface {v1, v0, v2}, Lcom/meawallet/mtp/ci;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardState;)V

    .line 75
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    .line 1023
    iput-object p1, v0, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    return-object v0

    .line 63
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bl;

    const-string v0, "Response data is null."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bl; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 84
    new-instance v0, Lcom/meawallet/mtp/fs;

    invoke-direct {v0}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p1}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p1

    return-object p1
.end method

.method final d()Lcom/meawallet/mtp/ep;
    .locals 1

    .line 49
    sget-object v0, Lcom/meawallet/mtp/gy;->g:Lcom/meawallet/mtp/ep;

    return-object v0
.end method

.method final e()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/meawallet/mtp/fy;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    invoke-super {p0}, Lcom/meawallet/mtp/fv;->e()V

    return-void

    .line 143
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Token unique reference is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
