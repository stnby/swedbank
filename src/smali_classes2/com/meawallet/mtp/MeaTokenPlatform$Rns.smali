.class public Lcom/meawallet/mtp/MeaTokenPlatform$Rns;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaTokenPlatform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Rns"
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "MeaTokenPlatform$Rns"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isMeaRemoteMessage(Ljava/util/Map;)Ljava/lang/Boolean;
    .locals 1

    const-string v0, "mea_data"

    .line 1130
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "mea_token_status"

    .line 1131
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "mea_transaction"

    .line 1132
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 1137
    :cond_0
    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p0

    .line 1134
    :cond_1
    :goto_0
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p0
.end method

.method public static isMeaTransactionMessage(Ljava/util/Map;)Ljava/lang/Boolean;
    .locals 1

    const-string v0, "mea_transaction"

    .line 1150
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1152
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p0

    .line 1155
    :cond_0
    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p0
.end method

.method public static onMessageReceived(Ljava/util/Map;)V
    .locals 14

    .line 1167
    invoke-static {p0}, Lcom/meawallet/mtp/MeaTokenPlatform$Rns;->isMeaRemoteMessage(Ljava/util/Map;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1169
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    const/4 v0, 0x0

    .line 1938
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "mea_data"

    .line 1942
    invoke-interface {p0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_7

    const-string v1, "mea_data"

    .line 1943
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 1945
    sget-object v1, Lcom/meawallet/mtp/dd;->g:Lcom/google/gson/Gson;

    invoke-static {p0, v1}, Lcom/meawallet/mtp/v;->a(Ljava/lang/String;Lcom/google/gson/Gson;)Lcom/meawallet/mtp/v;

    move-result-object p0

    if-eqz p0, :cond_6

    .line 1947
    sget-object v1, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    .line 1948
    invoke-static {v1, p0}, Lcom/meawallet/mtp/fu;->a(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/v;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1950
    sget-object v1, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 2256
    :try_start_0
    iget-object v1, v1, Lcom/meawallet/mtp/dm;->g:Lcom/meawallet/mtp/x;

    .line 3182
    invoke-virtual {v1, p0}, Lcom/meawallet/mtp/x;->a(Lcom/meawallet/mtp/v;)Lcom/meawallet/mtp/aa;

    move-result-object p0

    .line 3210
    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getPendingAction()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    .line 3212
    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getPendingAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 3213
    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getPendingAction()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v6

    const v7, -0x7a84574b

    if-eq v6, v7, :cond_1

    const v7, 0x62d13568

    if-eq v6, v7, :cond_0

    goto :goto_0

    :cond_0
    const-string v6, "RESET_MOBILE_PIN"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const-string v6, "PROVISION"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v5, 0x0

    :cond_2
    :goto_0
    packed-switch v5, :pswitch_data_0

    .line 3233
    new-array v2, v2, [Ljava/lang/Object;

    goto :goto_2

    .line 3224
    :pswitch_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getTokenUniqueReference()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 3225
    iget-object v2, v1, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getTokenUniqueReference()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, p0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onCardPinReset(Ljava/lang/String;)V

    goto :goto_1

    .line 3227
    :cond_3
    iget-object p0, v1, Lcom/meawallet/mtp/x;->a:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;

    invoke-interface {p0}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;->onWalletPinReset()V

    .line 3230
    :goto_1
    iget-object p0, v1, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    invoke-virtual {p0}, Lcom/meawallet/mtp/u;->b()V

    goto :goto_3

    .line 3218
    :pswitch_1
    iget-object v0, v1, Lcom/meawallet/mtp/x;->b:Lcom/meawallet/mtp/t;

    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getTokenUniqueReference()Ljava/lang/String;

    move-result-object v5

    .line 4058
    new-instance p0, Lcom/meawallet/mtp/dz;

    .line 4059
    invoke-virtual {v0}, Lcom/meawallet/mtp/t;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/meawallet/mtp/t;->a:Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;

    iget-object v8, v0, Lcom/meawallet/mtp/t;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    iget-object v9, v0, Lcom/meawallet/mtp/t;->c:Lcom/meawallet/mtp/fi;

    iget-object v10, v0, Lcom/meawallet/mtp/t;->d:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v11, v0, Lcom/meawallet/mtp/t;->g:Lcom/meawallet/mtp/ad;

    iget-object v12, v0, Lcom/meawallet/mtp/t;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    iget-object v13, v0, Lcom/meawallet/mtp/t;->f:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;

    move-object v4, p0

    invoke-direct/range {v4 .. v13}, Lcom/meawallet/mtp/dz;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    goto :goto_4

    .line 3233
    :goto_2
    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getPendingAction()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v3

    :cond_4
    :goto_3
    move-object p0, v0

    :goto_4
    if-eqz p0, :cond_5

    .line 3238
    iget-object v0, v1, Lcom/meawallet/mtp/x;->c:Lcom/meawallet/mtp/u;

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/ee;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    return-void

    :catch_0
    move-exception p0

    .line 2258
    sget-object v0, Lcom/meawallet/mtp/dm;->a:Ljava/lang/String;

    const-string v1, "Failed to process PUSH notification."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, p0, v1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    return-void

    :cond_7
    const-string v0, "mea_token_status"

    .line 1956
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "mea_token_status"

    .line 1958
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 1960
    sget-object v0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 4248
    iget-object v0, v0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    const-class v1, Lcom/meawallet/mtp/PushNotifyTokenUpdated;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/PushNotifyTokenUpdated;

    if-eqz p0, :cond_e

    .line 1969
    :try_start_1
    sget-object v0, Lcom/meawallet/mtp/dd;->c:Lcom/meawallet/mtp/l;

    .line 5026
    iget-object v1, p0, Lcom/meawallet/mtp/PushNotifyTokenUpdated;->b:Ljava/lang/String;

    .line 6022
    iget-object v4, p0, Lcom/meawallet/mtp/PushNotifyTokenUpdated;->a:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    .line 1971
    invoke-static {}, Lcom/meawallet/mtp/dd;->a()Lcom/meawallet/mtp/dm;

    move-result-object v5

    invoke-virtual {v5}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v5

    const/4 v6, 0x2

    .line 6259
    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v3

    aput-object v4, v6, v2

    if-eqz v4, :cond_8

    if-eqz v1, :cond_8

    .line 6263
    sget-object v6, Lcom/meawallet/mtp/l$4;->b:[I

    invoke-virtual {v4}, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_1

    goto :goto_5

    .line 6284
    :pswitch_2
    new-instance v4, Lcom/meawallet/mtp/ct;

    invoke-direct {v4, v1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    .line 8218
    new-array v1, v2, [Ljava/lang/Object;

    aput-object v4, v1, v3

    .line 8219
    invoke-virtual {v0, v4, v5}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 8226
    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->getRules()Lcom/meawallet/mtp/ev;

    move-result-object v1

    invoke-interface {v1}, Lcom/meawallet/mtp/ev;->b()I

    move-result v1

    .line 8228
    invoke-static {v1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-nez v1, :cond_8

    .line 8233
    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    iget-object v6, v0, Lcom/meawallet/mtp/l;->a:Lcom/meawallet/mtp/ci;

    invoke-virtual {v0, v4, v1, v6}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardState;Lcom/meawallet/mtp/ci;)V

    .line 8235
    invoke-static {v4, v5}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 8241
    invoke-interface {v5, v0}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->deleteCard(Lcom/mastercard/mpsdk/componentinterface/Card;)Ljava/lang/String;

    goto :goto_5

    .line 6278
    :pswitch_3
    new-instance v4, Lcom/meawallet/mtp/ct;

    invoke-direct {v4, v1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    .line 7174
    new-array v1, v2, [Ljava/lang/Object;

    aput-object v4, v1, v3

    .line 7176
    invoke-virtual {v0, v4, v5}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 7183
    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->getRules()Lcom/meawallet/mtp/ev;

    move-result-object v1

    invoke-interface {v1}, Lcom/meawallet/mtp/ev;->a()I

    move-result v1

    .line 7185
    invoke-static {v1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-nez v1, :cond_8

    .line 7190
    invoke-static {v4, v5}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 7194
    invoke-interface {v5, v1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->suspendCard(Lcom/mastercard/mpsdk/componentinterface/Card;)V

    .line 7196
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v1

    .line 8162
    iget-object v1, v1, Lcom/meawallet/mtp/ax;->f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    if-eqz v1, :cond_8

    .line 7200
    new-instance v5, Lcom/meawallet/mtp/l$2;

    invoke-direct {v5, v0, v1, v4}, Lcom/meawallet/mtp/l$2;-><init>(Lcom/meawallet/mtp/l;Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;Lcom/meawallet/mtp/MeaCard;)V

    invoke-static {v5}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    goto :goto_5

    .line 6266
    :pswitch_4
    invoke-virtual {v0, v1, v5}, Lcom/meawallet/mtp/l;->a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    .line 1973
    :cond_8
    :goto_5
    :pswitch_5
    sget-object v0, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    .line 9030
    iget-object v1, p0, Lcom/meawallet/mtp/PushNotifyTokenUpdated;->c:Lcom/meawallet/mtp/MeaProductConfig;

    .line 9034
    iget-object v4, p0, Lcom/meawallet/mtp/PushNotifyTokenUpdated;->d:Lcom/meawallet/mtp/MeaTokenInfo;

    .line 10026
    iget-object p0, p0, Lcom/meawallet/mtp/PushNotifyTokenUpdated;->b:Ljava/lang/String;

    .line 10554
    new-array v5, v2, [Ljava/lang/Object;

    aput-object p0, v5, v3

    if-nez v1, :cond_9

    if-eqz v4, :cond_d

    .line 10558
    :cond_9
    iget-object v5, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-interface {v5, p0}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object v5

    if-nez v5, :cond_a

    .line 10561
    sget-object v0, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v1, 0x3f2

    const-string v4, "updateTokenData(): Card not found. cardId=%s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v0, v1, v4, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_a
    if-eqz v1, :cond_b

    .line 11101
    iput-object v1, v5, Lcom/meawallet/mtp/bt;->g:Lcom/meawallet/mtp/MeaProductConfig;

    :cond_b
    if-eqz v4, :cond_c

    .line 11110
    iput-object v4, v5, Lcom/meawallet/mtp/bt;->h:Lcom/meawallet/mtp/MeaTokenInfo;

    .line 10574
    :cond_c
    iget-object p0, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-interface {p0, v5}, Lcom/meawallet/mtp/ci;->b(Lcom/meawallet/mtp/bt;)V
    :try_end_1
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/bv; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/bn; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/cs; {:try_start_1 .. :try_end_1} :catch_1

    :cond_d
    return-void

    :catch_1
    move-exception p0

    .line 1979
    sget-object v0, Lcom/meawallet/mtp/dd;->a:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_e
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static parseTransactionMessage(Ljava/util/Map;)Lcom/meawallet/mtp/MeaTransactionMessage;
    .locals 1

    .line 1184
    invoke-static {p0}, Lcom/meawallet/mtp/MeaTokenPlatform$Rns;->isMeaTransactionMessage(Ljava/util/Map;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mea_transaction"

    .line 1186
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-eqz p0, :cond_0

    .line 1190
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {p0}, Lcom/meawallet/mtp/dd;->c(Ljava/lang/String;)Lcom/meawallet/mtp/MeaTransactionMessage;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method
