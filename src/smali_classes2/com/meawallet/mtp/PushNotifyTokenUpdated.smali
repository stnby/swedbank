.class Lcom/meawallet/mtp/PushNotifyTokenUpdated;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    }
.end annotation


# instance fields
.field a:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field

.field b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenUniqueReference"
    .end annotation
.end field

.field c:Lcom/meawallet/mtp/MeaProductConfig;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "productConfig"
    .end annotation
.end field

.field d:Lcom/meawallet/mtp/MeaTokenInfo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenInfo"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
