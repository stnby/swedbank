.class final enum Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/PushNotifyTokenUpdated;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TokenStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTIVE:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum DEACTIVATED:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum INACTIVE:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum SUSPENDED:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 38
    new-instance v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    const-string v1, "INACTIVE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->INACTIVE:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    .line 40
    new-instance v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    const-string v1, "ACTIVE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->ACTIVE:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    .line 42
    new-instance v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    const-string v1, "SUSPENDED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->SUSPENDED:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    .line 44
    new-instance v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    const-string v1, "DEACTIVATED"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->DEACTIVATED:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    const/4 v0, 0x4

    .line 37
    new-array v0, v0, [Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    sget-object v1, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->INACTIVE:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->ACTIVE:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->SUSPENDED:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->DEACTIVATED:Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->a:[Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    .locals 1

    .line 37
    const-class v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;
    .locals 1

    .line 37
    sget-object v0, Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->a:[Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/PushNotifyTokenUpdated$TokenStatus;

    return-object v0
.end method
