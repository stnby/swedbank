.class public interface abstract Lcom/meawallet/mtp/MeaCardPinListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onCardPinChangeFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
.end method

.method public abstract onCardPinChangeSuccess(Lcom/meawallet/mtp/MeaCard;)V
.end method

.method public abstract onCardPinResetCompleted(Lcom/meawallet/mtp/MeaCard;)V
.end method

.method public abstract onCardPinSetFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
.end method

.method public abstract onCardPinSetSuccess(Lcom/meawallet/mtp/MeaCard;)V
.end method
