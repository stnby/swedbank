.class Lcom/meawallet/mtp/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/http/HttpResponse;


# static fields
.field private static final a:Ljava/lang/String; = "bj"


# instance fields
.field private b:I

.field private c:[B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(I[BI)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lcom/meawallet/mtp/bj;->b:I

    .line 18
    iput-object p2, p0, Lcom/meawallet/mtp/bj;->c:[B

    .line 19
    iput p3, p0, Lcom/meawallet/mtp/bj;->d:I

    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 7

    const-string v0, ""

    .line 42
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/meawallet/mtp/bj;->c:[B

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 44
    sget-object v2, Lcom/meawallet/mtp/bj;->a:Ljava/lang/String;

    const/16 v3, 0x1fb

    const-string v4, "Failed to convert response byte array content to String exception: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 45
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    .line 44
    invoke-static {v2, v3, v4, v5}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v0
.end method

.method public getContent()[B
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/meawallet/mtp/bj;->c:[B

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/meawallet/mtp/bj;->b:I

    return v0
.end method

.method public getRetryAfterValue()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/meawallet/mtp/bj;->d:I

    return v0
.end method
