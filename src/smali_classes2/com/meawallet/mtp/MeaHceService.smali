.class public Lcom/meawallet/mtp/MeaHceService;
.super Landroid/nfc/cardemulation/HostApduService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "MeaHceService"

.field private static b:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

.field private static c:Lcom/meawallet/mtp/k;

.field private static d:Lcom/mastercard/mpsdk/componentinterface/CardManager;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroid/nfc/cardemulation/HostApduService;-><init>()V

    return-void
.end method

.method static a(Lcom/mastercard/mpsdk/interfaces/ApduProcessor;Lcom/meawallet/mtp/k;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V
    .locals 0

    .line 126
    sput-object p0, Lcom/meawallet/mtp/MeaHceService;->b:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    .line 127
    sput-object p1, Lcom/meawallet/mtp/MeaHceService;->c:Lcom/meawallet/mtp/k;

    .line 128
    sput-object p2, Lcom/meawallet/mtp/MeaHceService;->d:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .line 31
    invoke-super {p0}, Landroid/nfc/cardemulation/HostApduService;->onCreate()V

    .line 35
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaHceService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    :try_start_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaHceService;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->initialize(Landroid/app/Application;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/InitializationFailedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 42
    sget-object v1, Lcom/meawallet/mtp/MeaHceService;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public onDeactivated(I)V
    .locals 7

    const/4 v0, 0x1

    .line 101
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "UNKNOWN"

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v2, "DEACTIVATION_DESELECTED"

    goto :goto_0

    :pswitch_1
    const-string v2, "DEACTIVATION_LINK_LOSS"

    .line 1122
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%s(%d)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    .line 103
    sget-object v0, Lcom/meawallet/mtp/MeaHceService;->b:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 105
    sget-object p1, Lcom/meawallet/mtp/MeaHceService;->b:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/interfaces/ApduProcessor;->processOnDeactivated()V

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public processCommandApdu([BLandroid/os/Bundle;)[B
    .locals 9

    const-string p2, "CommandAPDU"

    .line 56
    invoke-static {p1}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/meawallet/mtp/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaHceService;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;)Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    return-object v0

    .line 64
    :cond_0
    sget-object p2, Lcom/meawallet/mtp/MeaHceService;->b:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    if-nez p2, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaHceService;->notifyUnhandled()V

    return-object v0

    .line 72
    :cond_1
    sget-object p2, Lcom/meawallet/mtp/MeaHceService;->c:Lcom/meawallet/mtp/k;

    if-eqz p2, :cond_3

    sget-object p2, Lcom/meawallet/mtp/MeaHceService;->c:Lcom/meawallet/mtp/k;

    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    sget-object v1, Lcom/meawallet/mtp/MeaHceService;->d:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    invoke-virtual {p2, v0, v1}, Lcom/meawallet/mtp/k;->getActiveCard(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p2

    if-nez p2, :cond_2

    goto :goto_0

    .line 85
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 87
    sget-object p2, Lcom/meawallet/mtp/MeaHceService;->b:Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/interfaces/ApduProcessor;->processApdu([B)[B

    move-result-object p1

    const-string p2, "ResponseAPDU"

    .line 89
    invoke-static {p1}, Lcom/meawallet/mtp/h;->c([B)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/meawallet/mtp/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x1

    .line 91
    new-array p2, p2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p2, v2

    return-object p1

    .line 75
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaHceService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.meawallet.mtp.intent.ON_TRANSACTION_FAILURE_MESSAGE"

    const/4 v5, 0x0

    const/16 v6, 0x458

    const/4 v7, 0x0

    new-instance v8, Lcom/meawallet/mtp/cw;

    .line 80
    invoke-static {}, Lcom/meawallet/mtp/h;->a()Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v8, p1}, Lcom/meawallet/mtp/cw;-><init>(Ljava/lang/String;)V

    .line 75
    invoke-static/range {v3 .. v8}, Lcom/meawallet/mtp/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    const/16 p1, 0x6985

    .line 82
    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(C)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object p1

    return-object p1
.end method
