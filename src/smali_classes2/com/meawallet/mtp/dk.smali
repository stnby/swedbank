.class Lcom/meawallet/mtp/dk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String; = "dk"


# instance fields
.field a:Lcom/meawallet/mtp/en;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/meawallet/mtp/cu;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/meawallet/mtp/cu;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/meawallet/mtp/dk;->c:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/meawallet/mtp/dk;->d:Lcom/meawallet/mtp/cu;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/meawallet/mtp/do;Lcom/meawallet/mtp/en;)Lcom/meawallet/mtp/fk;
    .locals 1

    .line 96
    new-instance v0, Lcom/meawallet/mtp/dk$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/meawallet/mtp/dk$1;-><init>(Lcom/meawallet/mtp/dk;Lcom/meawallet/mtp/do;Ljava/lang/String;Lcom/meawallet/mtp/en;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lcom/meawallet/mtp/cu;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/en;)Z
    .locals 1

    .line 4047
    iget-object p1, p1, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 76
    new-instance v0, Lcom/meawallet/mtp/ct;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, p2}, Lcom/meawallet/mtp/l;->c(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result p0

    .line 78
    invoke-static {p0}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result p1

    if-eqz p1, :cond_1

    if-eqz p3, :cond_0

    .line 81
    new-instance p1, Lcom/meawallet/mtp/cy;

    invoke-direct {p1, p0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p3, p1}, Lcom/meawallet/mtp/en;->a(Lcom/meawallet/mtp/MeaError;)V

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .line 16
    sget-object v0, Lcom/meawallet/mtp/dk;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 6

    const/4 v0, 0x1

    .line 37
    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/meawallet/mtp/dk;->c:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 39
    iget-object v1, p0, Lcom/meawallet/mtp/dk;->d:Lcom/meawallet/mtp/cu;

    .line 2051
    iget-object v1, v1, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    .line 41
    iget-object v2, p0, Lcom/meawallet/mtp/dk;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/meawallet/mtp/dk;->d:Lcom/meawallet/mtp/cu;

    iget-object v5, p0, Lcom/meawallet/mtp/dk;->a:Lcom/meawallet/mtp/en;

    invoke-static {v2, v4, v1, v5}, Lcom/meawallet/mtp/dk;->a(Ljava/lang/String;Lcom/meawallet/mtp/cu;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/en;)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    .line 46
    :cond_0
    iget-object v2, p0, Lcom/meawallet/mtp/dk;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 50
    iget-object v2, p0, Lcom/meawallet/mtp/dk;->a:Lcom/meawallet/mtp/en;

    if-eqz v2, :cond_1

    .line 52
    iget-object v2, p0, Lcom/meawallet/mtp/dk;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/meawallet/mtp/dk;->d:Lcom/meawallet/mtp/cu;

    .line 3043
    iget-object v4, v4, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    .line 3303
    iget-object v4, v4, Lcom/meawallet/mtp/dm;->e:Lcom/meawallet/mtp/do;

    .line 54
    iget-object v5, p0, Lcom/meawallet/mtp/dk;->a:Lcom/meawallet/mtp/en;

    .line 52
    invoke-direct {p0, v2, v4, v5}, Lcom/meawallet/mtp/dk;->a(Ljava/lang/String;Lcom/meawallet/mtp/do;Lcom/meawallet/mtp/en;)Lcom/meawallet/mtp/fk;

    move-result-object v2

    .line 57
    invoke-virtual {v2}, Lcom/meawallet/mtp/fk;->a()V

    .line 60
    :cond_1
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/Card;->replenishCredentials()Ljava/lang/String;

    move-result-object v1

    .line 62
    new-array v0, v0, [Ljava/lang/Object;

    aput-object v1, v0, v3

    return-void

    .line 66
    :cond_2
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/meawallet/mtp/dk;->c:Ljava/lang/String;

    aput-object v1, v0, v3

    return-void
.end method
