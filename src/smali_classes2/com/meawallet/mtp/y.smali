.class final Lcom/meawallet/mtp/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sdkVersion"
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mobileKeysetId"
    .end annotation
.end field

.field private c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "authenticationCode"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "encryptedData"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sessionFingerprint"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "2.0.8"

    .line 25
    iput-object v0, p0, Lcom/meawallet/mtp/y;->a:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/meawallet/mtp/y;->b:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/meawallet/mtp/y;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 28
    iput-object p3, p0, Lcom/meawallet/mtp/y;->d:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/meawallet/mtp/y;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
