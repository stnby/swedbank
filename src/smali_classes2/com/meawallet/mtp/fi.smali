.class Lcom/meawallet/mtp/fi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "fi"


# instance fields
.field private final b:Lcom/meawallet/mtp/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/meawallet/mtp/ac;

    invoke-direct {v0}, Lcom/meawallet/mtp/ac;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/fi;->b:Lcom/meawallet/mtp/ac;

    return-void
.end method

.method private static b(Lcom/meawallet/mtp/ab;)Z
    .locals 0

    if-eqz p0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/meawallet/mtp/ab;->a()Lcom/meawallet/mtp/aa;

    move-result-object p0

    invoke-virtual {p0}, Lcom/meawallet/mtp/aa;->getExpiryTimestamp()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/mastercard/mpsdk/utils/TimeUtils;->isBefore(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/meawallet/mtp/fi;->b:Lcom/meawallet/mtp/ac;

    invoke-virtual {v0}, Lcom/meawallet/mtp/ac;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/meawallet/mtp/fi;->b:Lcom/meawallet/mtp/ac;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/ac;->a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method final declared-synchronized a()Ljava/lang/String;
    .locals 5

    monitor-enter p0

    .line 37
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/fi;->b:Lcom/meawallet/mtp/ac;

    invoke-virtual {v0}, Lcom/meawallet/mtp/ac;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/ab;

    .line 39
    invoke-virtual {v1}, Lcom/meawallet/mtp/ab;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/meawallet/mtp/fi;->b(Lcom/meawallet/mtp/ab;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    .line 40
    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/ab;->a(Z)V

    .line 42
    invoke-virtual {v1}, Lcom/meawallet/mtp/ab;->a()Lcom/meawallet/mtp/aa;

    move-result-object v3

    if-nez v3, :cond_1

    .line 45
    sget-object v1, Lcom/meawallet/mtp/fi;->a:Ljava/lang/String;

    const/16 v3, 0x1f5

    const-string v4, "Session data is null"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v3, v4, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    monitor-exit p0

    return-object v2

    :cond_1
    const/4 v2, 0x1

    .line 50
    :try_start_1
    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v0

    .line 52
    invoke-virtual {v1}, Lcom/meawallet/mtp/ab;->a()Lcom/meawallet/mtp/aa;

    move-result-object v0

    .line 2084
    iget-object v0, v0, Lcom/meawallet/mtp/aa;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    monitor-exit p0

    return-object v0

    .line 56
    :cond_2
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception v0

    .line 36
    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/meawallet/mtp/ab;)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    .line 18
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/meawallet/mtp/ab;->a()Lcom/meawallet/mtp/aa;

    move-result-object v2

    .line 1084
    iget-object v2, v2, Lcom/meawallet/mtp/aa;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 20
    iget-object v0, p0, Lcom/meawallet/mtp/fi;->b:Lcom/meawallet/mtp/ac;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/ac;->a(Lcom/meawallet/mtp/ab;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 17
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized b(Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    .line 91
    :try_start_0
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 93
    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/fi;->a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {v1}, Lcom/meawallet/mtp/ab;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 97
    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/ab;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 100
    :cond_0
    :try_start_1
    new-instance v0, Lcom/meawallet/mtp/MeaUncheckedException;

    const-string v1, "Session cannot be marked as not available. sessionFingerprint="

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/meawallet/mtp/MeaUncheckedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    .line 90
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized b()Z
    .locals 3

    monitor-enter p0

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/fi;->b:Lcom/meawallet/mtp/ac;

    invoke-virtual {v0}, Lcom/meawallet/mtp/ac;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/ab;

    if-eqz v1, :cond_0

    .line 77
    invoke-virtual {v1}, Lcom/meawallet/mtp/ab;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/meawallet/mtp/fi;->b(Lcom/meawallet/mtp/ab;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 81
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    .line 87
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    .line 74
    monitor-exit p0

    throw v0
.end method

.method final c(Ljava/lang/String;)Lcom/meawallet/mtp/aa;
    .locals 2

    const/4 v0, 0x1

    .line 106
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 108
    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/fi;->a(Ljava/lang/String;)Lcom/meawallet/mtp/ab;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 116
    :cond_0
    invoke-virtual {p1}, Lcom/meawallet/mtp/ab;->a()Lcom/meawallet/mtp/aa;

    move-result-object p1

    return-object p1
.end method

.method final declared-synchronized d(Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    .line 184
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 186
    iget-object v0, p0, Lcom/meawallet/mtp/fi;->b:Lcom/meawallet/mtp/ac;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/ac;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 183
    monitor-exit p0

    throw p1
.end method
