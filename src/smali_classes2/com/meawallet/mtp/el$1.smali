.class final Lcom/meawallet/mtp/el$1;
.super Lcom/meawallet/mtp/em;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/el;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/em<",
        "TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/el;

.field final synthetic b:Lcom/meawallet/mtp/el;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/el;Lcom/meawallet/mtp/el;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/meawallet/mtp/el$1;->b:Lcom/meawallet/mtp/el;

    iput-object p2, p0, Lcom/meawallet/mtp/el$1;->a:Lcom/meawallet/mtp/el;

    invoke-direct {p0}, Lcom/meawallet/mtp/em;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/meawallet/mtp/el$1;->b()Lcom/meawallet/mtp/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/meawallet/mtp/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/d<",
            "TR;>;)V"
        }
    .end annotation

    .line 73
    invoke-static {}, Lcom/meawallet/mtp/el;->f()Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/meawallet/mtp/el$1;->b:Lcom/meawallet/mtp/el;

    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/el;->a(Lcom/meawallet/mtp/d;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 45
    check-cast p1, Lcom/meawallet/mtp/d;

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/el$1;->a(Lcom/meawallet/mtp/d;)V

    return-void
.end method

.method public final b()Lcom/meawallet/mtp/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/meawallet/mtp/d<",
            "TR;>;"
        }
    .end annotation

    .line 48
    invoke-static {}, Lcom/meawallet/mtp/el;->f()Ljava/lang/String;

    .line 53
    :try_start_0
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->REMOTE_REQUEST_TIMEOUT:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1014
    invoke-static {v0}, Lcom/meawallet/mtp/ek;->a(I)Lcom/meawallet/mtp/ej;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/meawallet/mtp/el$1;->b:Lcom/meawallet/mtp/el;

    iget-object v1, v1, Lcom/meawallet/mtp/el;->e:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/meawallet/mtp/el$1;->a:Lcom/meawallet/mtp/el;

    invoke-interface {v0, v1, v2}, Lcom/meawallet/mtp/ej;->a(Lcom/google/gson/Gson;Lcom/meawallet/mtp/el;)Lcom/meawallet/mtp/ei;

    move-result-object v0
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0

    .line 2027
    iget-object v1, v0, Lcom/meawallet/mtp/ei;->b:Lcom/meawallet/mtp/MeaError;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 61
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    .line 3023
    iget-object v0, v0, Lcom/meawallet/mtp/ei;->b:Lcom/meawallet/mtp/MeaError;

    .line 61
    invoke-static {v0}, Lcom/meawallet/mtp/el;->a(Lcom/meawallet/mtp/MeaError;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    .line 3029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1

    .line 4019
    :cond_1
    iget-object v0, v0, Lcom/meawallet/mtp/ei;->a:Ljava/lang/String;

    .line 66
    invoke-static {}, Lcom/meawallet/mtp/el;->f()Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/meawallet/mtp/JsonUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 68
    iget-object v1, p0, Lcom/meawallet/mtp/el$1;->b:Lcom/meawallet/mtp/el;

    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/el;->a(Ljava/lang/String;)Lcom/meawallet/mtp/d;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/el;->a(Lcom/meawallet/mtp/MeaError;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    .line 1029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1
.end method
