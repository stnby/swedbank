.class final Lcom/meawallet/mtp/cm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transportKey"
    .end annotation
.end field

.field b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "macKey"
    .end annotation
.end field

.field c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dataEncryptionKey"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/meawallet/mtp/cm;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 18
    iput-object p2, p0, Lcom/meawallet/mtp/cm;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 19
    iput-object p3, p0, Lcom/meawallet/mtp/cm;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/meawallet/mtp/cm;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/meawallet/mtp/cm;->a:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/cm;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/meawallet/mtp/cm;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/meawallet/mtp/cm;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v0, :cond_2

    .line 42
    iget-object v0, p0, Lcom/meawallet/mtp/cm;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->clear()V

    :cond_2
    return-void
.end method
