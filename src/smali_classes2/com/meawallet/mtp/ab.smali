.class Lcom/meawallet/mtp/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/lang/String; = "ab"


# instance fields
.field a:I

.field b:I

.field private d:Lcom/meawallet/mtp/aa;

.field private volatile e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/aa;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/meawallet/mtp/ab;->d:Lcom/meawallet/mtp/aa;

    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/meawallet/mtp/ab;->e:Z

    const/4 p1, 0x0

    .line 18
    iput p1, p0, Lcom/meawallet/mtp/ab;->a:I

    .line 19
    iput p1, p0, Lcom/meawallet/mtp/ab;->b:I

    return-void
.end method


# virtual methods
.method final declared-synchronized a()Lcom/meawallet/mtp/aa;
    .locals 1

    monitor-enter p0

    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ab;->d:Lcom/meawallet/mtp/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Z)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    .line 41
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 43
    iput-boolean p1, p0, Lcom/meawallet/mtp/ab;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 40
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized b()Z
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    .line 35
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/meawallet/mtp/ab;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 37
    iget-boolean v0, p0, Lcom/meawallet/mtp/ab;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    .line 34
    monitor-exit p0

    throw v0
.end method
