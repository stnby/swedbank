.class final Lcom/meawallet/mtp/u$a;
.super Ljava/util/concurrent/LinkedBlockingDeque;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/LinkedBlockingDeque<",
        "Lcom/meawallet/mtp/ee;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/u;


# direct methods
.method private constructor <init>(Lcom/meawallet/mtp/u;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/meawallet/mtp/u$a;->a:Lcom/meawallet/mtp/u;

    invoke-direct {p0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/meawallet/mtp/u;B)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/u$a;-><init>(Lcom/meawallet/mtp/u;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/meawallet/mtp/ee;)V
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/meawallet/mtp/u$a;->a:Lcom/meawallet/mtp/u;

    invoke-static {v0}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/meawallet/mtp/u$a;->a:Lcom/meawallet/mtp/u;

    invoke-static {v0}, Lcom/meawallet/mtp/u;->b(Lcom/meawallet/mtp/u;)V

    .line 40
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/u;->d()Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 42
    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public final synthetic add(Ljava/lang/Object;)Z
    .locals 0

    .line 29
    check-cast p1, Lcom/meawallet/mtp/ee;

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/u$a;->b(Lcom/meawallet/mtp/ee;)Z

    move-result p1

    return p1
.end method

.method public final synthetic addFirst(Ljava/lang/Object;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/meawallet/mtp/ee;

    invoke-virtual {p0, p1}, Lcom/meawallet/mtp/u$a;->a(Lcom/meawallet/mtp/ee;)V

    return-void
.end method

.method public final b(Lcom/meawallet/mtp/ee;)Z
    .locals 3

    .line 47
    iget-object v0, p0, Lcom/meawallet/mtp/u$a;->a:Lcom/meawallet/mtp/u;

    invoke-static {v0}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/meawallet/mtp/u$a;->a:Lcom/meawallet/mtp/u;

    invoke-static {v0}, Lcom/meawallet/mtp/u;->b(Lcom/meawallet/mtp/u;)V

    .line 51
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/u;->d()Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/meawallet/mtp/ee;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 53
    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
