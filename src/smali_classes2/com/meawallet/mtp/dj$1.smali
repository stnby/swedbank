.class final Lcom/meawallet/mtp/dj$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/dj;->b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/dj;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/dj;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/meawallet/mtp/dj$1;->a:Lcom/meawallet/mtp/dj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/meawallet/mtp/dj$1;->a:Lcom/meawallet/mtp/dj;

    invoke-static {v0}, Lcom/meawallet/mtp/dj;->a(Lcom/meawallet/mtp/dj;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v0

    return-object v0
.end method

.method public final getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 2

    .line 40
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported operation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V
    .locals 1

    .line 45
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Unsupported operation"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
