.class final Lcom/meawallet/mtp/dd$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/dd;->a(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Application;

.field final synthetic b:Lcom/meawallet/mtp/MeaListener;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/meawallet/mtp/dd$1;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/meawallet/mtp/dd$1;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()Lcom/meawallet/mtp/d;
    .locals 3

    .line 93
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :try_start_1
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    invoke-static {}, Lcom/meawallet/mtp/dd;->A()Ljava/lang/String;

    .line 99
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V
    :try_end_1
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    return-object v0

    .line 103
    :catch_0
    :try_start_2
    invoke-static {}, Lcom/meawallet/mtp/dd;->A()Ljava/lang/String;

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/dd$1;->a:Landroid/app/Application;

    invoke-static {v0}, Lcom/meawallet/mtp/dx;->a(Landroid/content/Context;)V

    .line 108
    invoke-static {}, Lcom/meawallet/mtp/dw;->a()I

    move-result v0

    .line 110
    invoke-static {v0}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/cy;

    invoke-direct {v2, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    .line 1029
    iput-object v2, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 127
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    return-object v1

    .line 115
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/meawallet/mtp/dd$1;->a:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->b(Landroid/content/Context;)V

    .line 117
    iget-object v0, p0, Lcom/meawallet/mtp/dd$1;->a:Landroid/app/Application;

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->b(Landroid/app/Application;)Lcom/meawallet/mtp/d;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lcom/meawallet/mtp/d;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 121
    new-instance v1, Lcom/meawallet/mtp/dd;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/dd;-><init>(B)V

    invoke-static {v1}, Lcom/meawallet/mtp/MeaTokenPlatform;->a(Lcom/meawallet/mtp/dd;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 127
    :cond_2
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 87
    invoke-direct {p0}, Lcom/meawallet/mtp/dd$1;->b()Lcom/meawallet/mtp/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .line 87
    check-cast p1, Lcom/meawallet/mtp/d;

    .line 1133
    invoke-virtual {p1}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1135
    iget-object p1, p0, Lcom/meawallet/mtp/dd$1;->b:Lcom/meawallet/mtp/MeaListener;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaListener;->onSuccess()V

    return-void

    .line 1138
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/dd;->B()V

    .line 1140
    iget-object v0, p0, Lcom/meawallet/mtp/dd$1;->b:Lcom/meawallet/mtp/MeaListener;

    .line 2041
    iget-object p1, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 1140
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method
