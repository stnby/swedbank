.class Lcom/meawallet/mtp/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "aq"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()J
    .locals 2

    .line 8
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "LAST_TRANSACTION_TIMESTAMP"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->d(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(J)V
    .locals 3

    const/4 v0, 0x1

    .line 12
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 14
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "LAST_TRANSACTION_TIMESTAMP"

    invoke-virtual {v0, v1, p0, p1}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;J)Z

    return-void
.end method

.method static b()V
    .locals 3

    const-wide/16 v0, 0x0

    .line 20
    invoke-static {v0, v1}, Lcom/meawallet/mtp/aq;->a(J)V

    .line 1045
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "IS_NEW_DEVICE_UNLOCK_MANDATORY"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Z)Z

    return-void
.end method

.method static c()V
    .locals 3

    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/meawallet/mtp/aq;->a(J)V

    .line 2039
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "IS_NEW_DEVICE_UNLOCK_MANDATORY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Z)Z

    return-void
.end method

.method static d()Z
    .locals 2

    .line 33
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "IS_NEW_DEVICE_UNLOCK_MANDATORY"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
