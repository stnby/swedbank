.class public Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;
    }
.end annotation


# instance fields
.field a:Lcom/meawallet/mtp/ep;

.field b:Lcom/meawallet/mtp/gx;

.field private c:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 1

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->g:Ljava/lang/String;

    .line 76
    iput p2, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->h:I

    .line 77
    iput p3, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->i:I

    .line 78
    iput-object p4, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->j:Ljava/lang/String;

    .line 80
    sget-object p1, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->c:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    .line 81
    sget-object p1, Lcom/meawallet/mtp/gy;->c:Lcom/meawallet/mtp/ep;

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->a:Lcom/meawallet/mtp/ep;

    .line 82
    new-instance p1, Lcom/meawallet/mtp/gp;

    iget-object p2, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->g:Ljava/lang/String;

    iget p3, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->h:I

    .line 83
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/meawallet/mtp/al;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iget p4, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->i:I

    .line 84
    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->j:Ljava/lang/String;

    invoke-direct {p1, p2, p3, p4, v0}, Lcom/meawallet/mtp/gp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->b:Lcom/meawallet/mtp/gx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->d:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->e:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->f:Ljava/lang/String;

    .line 69
    sget-object p1, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->SECRET:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->c:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    .line 70
    sget-object p1, Lcom/meawallet/mtp/gy;->d:Lcom/meawallet/mtp/ep;

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->a:Lcom/meawallet/mtp/ep;

    .line 71
    new-instance p1, Lcom/meawallet/mtp/gq;

    iget-object p2, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->d:Ljava/lang/String;

    iget-object p3, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->f:Ljava/lang/String;

    invoke-direct {p1, p2, p3, v0}, Lcom/meawallet/mtp/gq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->b:Lcom/meawallet/mtp/gx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->k:Ljava/lang/String;

    .line 94
    iput-object p2, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->l:Ljava/lang/String;

    .line 95
    iput-object p3, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->m:Ljava/lang/String;

    .line 96
    iput-object p4, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->n:Ljava/lang/String;

    .line 98
    sget-object p1, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->ENCRYPTED_PAN:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->c:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    .line 99
    sget-object p1, Lcom/meawallet/mtp/gy;->b:Lcom/meawallet/mtp/ep;

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->a:Lcom/meawallet/mtp/ep;

    .line 100
    new-instance p1, Lcom/meawallet/mtp/go;

    iget-object p2, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->k:Ljava/lang/String;

    iget-object p3, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->l:Ljava/lang/String;

    iget-object p4, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->m:Ljava/lang/String;

    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->n:Ljava/lang/String;

    invoke-direct {p1, p2, p3, p4, v0}, Lcom/meawallet/mtp/go;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->b:Lcom/meawallet/mtp/gx;

    return-void
.end method

.method public static withCardSecret(Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
    .locals 2

    .line 118
    new-instance v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static withCardSecret(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
    .locals 1

    .line 134
    new-instance v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    invoke-direct {v0, p0, p1, p2}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static withEncryptedPan(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
    .locals 1

    .line 168
    new-instance v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static withPan(Ljava/lang/String;IILjava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
    .locals 1

    .line 151
    new-instance v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getBin()Ljava/lang/String;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getCardId()Ljava/lang/String;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getCardSecret()Ljava/lang/String;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getCardholderName()Ljava/lang/String;
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getEncryptedCardData()Ljava/lang/String;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getEncryptedKey()Ljava/lang/String;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryMonth()I
    .locals 1

    .line 192
    iget v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->h:I

    return v0
.end method

.method public getExpiryYear()I
    .locals 1

    .line 196
    iget v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->i:I

    return v0
.end method

.method public getInitialVector()Ljava/lang/String;
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getPan()Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicKeyFingerprint()Ljava/lang/String;
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->c:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MeaInitializeDigitizationParameters[\n"

    .line 230
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    sget-object v1, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$1;->a:[I

    iget-object v2, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->c:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;

    invoke-virtual {v2}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters$InitializeDigitizationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const-string v1, "\tencryptedCardData = "

    .line 249
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tpublicKeyFingerprint = "

    .line 250
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tencryptedKey = "

    .line 251
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tinitialVector = "

    .line 252
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const-string v1, "\tPAN = "

    .line 241
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\texpiryMonth = "

    .line 242
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->h:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\texpiryYear = "

    .line 243
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->i:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tcardholderName = "

    .line 244
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    const-string v1, "\tcardId = "

    .line 234
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tcardSecret = "

    .line 235
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tbin = "

    .line 236
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string v1, "]"

    .line 257
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
