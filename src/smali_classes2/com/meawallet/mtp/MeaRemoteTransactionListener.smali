.class public interface abstract Lcom/meawallet/mtp/MeaRemoteTransactionListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onAuthenticationRequired(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;)V
.end method

.method public abstract onRemoteCompleted(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemoteTransactionOutcome;)V
.end method

.method public abstract onRemoteFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
.end method
