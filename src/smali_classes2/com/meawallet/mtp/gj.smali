.class Lcom/meawallet/mtp/gj;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenTransactions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/meawallet/mtp/gj;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method validate()V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/meawallet/mtp/gj;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    return-void

    .line 24
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Card transaction history list is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
