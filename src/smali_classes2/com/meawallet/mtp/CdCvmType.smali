.class public final enum Lcom/meawallet/mtp/CdCvmType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/CdCvmType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum NO_CDCVM:Lcom/meawallet/mtp/CdCvmType;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/CdCvmType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 13
    new-instance v0, Lcom/meawallet/mtp/CdCvmType;

    const-string v1, "NO_CDCVM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/CdCvmType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CdCvmType;->NO_CDCVM:Lcom/meawallet/mtp/CdCvmType;

    .line 15
    new-instance v0, Lcom/meawallet/mtp/CdCvmType;

    const-string v1, "FINGERPRINT_FRAGMENT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/CdCvmType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    .line 17
    new-instance v0, Lcom/meawallet/mtp/CdCvmType;

    const-string v1, "DEVICE_UNLOCK"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/CdCvmType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    .line 19
    new-instance v0, Lcom/meawallet/mtp/CdCvmType;

    const-string v1, "MOBILE_PIN_WALLET"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/CdCvmType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    .line 21
    new-instance v0, Lcom/meawallet/mtp/CdCvmType;

    const-string v1, "MOBILE_PIN_CARD"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/CdCvmType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    const/4 v0, 0x5

    .line 12
    new-array v0, v0, [Lcom/meawallet/mtp/CdCvmType;

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->NO_CDCVM:Lcom/meawallet/mtp/CdCvmType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/meawallet/mtp/CdCvmType;->a:[Lcom/meawallet/mtp/CdCvmType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/meawallet/mtp/CdCvmType;
    .locals 1

    .line 26
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    invoke-static {p0}, Lcom/meawallet/mtp/CdCvmType;->valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/CdCvmType;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/CdCvmType;
    .locals 1

    .line 12
    const-class v0, Lcom/meawallet/mtp/CdCvmType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/CdCvmType;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/CdCvmType;
    .locals 1

    .line 12
    sget-object v0, Lcom/meawallet/mtp/CdCvmType;->a:[Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/CdCvmType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/CdCvmType;

    return-object v0
.end method
