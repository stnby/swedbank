.class Lcom/meawallet/mtp/dd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String; = "dd"

.field static b:Lcom/meawallet/mtp/fu;

.field static c:Lcom/meawallet/mtp/l;

.field static d:Lcom/meawallet/mtp/cu;

.field static e:Lcom/meawallet/mtp/dm;

.field static f:Lcom/meawallet/mtp/ci;

.field static g:Lcom/google/gson/Gson;

.field private static h:Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private static i:Lcom/meawallet/mtp/cr;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private static j:Lcom/meawallet/mtp/dq;

.field private static k:Lcom/meawallet/mtp/m;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private static l:Lcom/meawallet/mtp/fr;

.field private static m:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/meawallet/mtp/dd;-><init>()V

    return-void
.end method

.method static synthetic A()Ljava/lang/String;
    .locals 1

    .line 25
    sget-object v0, Lcom/meawallet/mtp/dd;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B()V
    .locals 0

    .line 25
    invoke-static {}, Lcom/meawallet/mtp/dd;->H()V

    return-void
.end method

.method static synthetic C()Landroid/content/Context;
    .locals 1

    .line 25
    sget-object v0, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic D()Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;
    .locals 1

    .line 25
    sget-object v0, Lcom/meawallet/mtp/dd;->m:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    return-object v0
.end method

.method static synthetic E()Lcom/meawallet/mtp/ci;
    .locals 1

    .line 25
    sget-object v0, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    return-object v0
.end method

.method static synthetic F()Lcom/meawallet/mtp/dm;
    .locals 1

    .line 25
    sget-object v0, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    return-object v0
.end method

.method static synthetic G()Lcom/meawallet/mtp/cr;
    .locals 1

    .line 25
    sget-object v0, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    return-object v0
.end method

.method private static H()V
    .locals 1

    const/4 v0, 0x0

    .line 1011
    sput-object v0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 1012
    sput-object v0, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    .line 1013
    sput-object v0, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 1014
    sput-object v0, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    .line 1015
    sput-object v0, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    .line 1016
    sput-object v0, Lcom/meawallet/mtp/dd;->j:Lcom/meawallet/mtp/dq;

    .line 1017
    sput-object v0, Lcom/meawallet/mtp/dd;->k:Lcom/meawallet/mtp/m;

    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;
    .locals 4

    const/4 v0, 0x1

    .line 341
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    .line 349
    :cond_0
    invoke-static {v1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v3

    if-nez v3, :cond_1

    return-object v1

    .line 354
    :cond_1
    sget-object v3, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    .line 10151
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v2

    .line 10153
    iget-object v0, v3, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-interface {v0, p0}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 10157
    new-instance v0, Lcom/meawallet/mtp/ct;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/ct;-><init>(Lcom/meawallet/mtp/bt;)V

    return-object v0

    .line 10159
    :cond_2
    sget-object p0, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v0, 0x3f2

    const-string v3, "getMeaCardByCardId failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v1
.end method

.method static a(Landroid/app/Application;)Lcom/meawallet/mtp/dd;
    .locals 5

    .line 51
    invoke-static {p0}, Lcom/meawallet/mtp/dx;->a(Landroid/content/Context;)V

    .line 53
    invoke-static {}, Lcom/meawallet/mtp/dw;->a()I

    move-result v0

    .line 55
    invoke-static {v0}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_2

    .line 60
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->c(Landroid/content/Context;)V

    .line 62
    invoke-static {p0}, Lcom/meawallet/mtp/dd;->c(Landroid/app/Application;)Lcom/meawallet/mtp/d;

    move-result-object p0

    .line 64
    invoke-virtual {p0}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    new-instance p0, Lcom/meawallet/mtp/dd;

    invoke-direct {p0}, Lcom/meawallet/mtp/dd;-><init>()V

    return-object p0

    .line 69
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/dd;->H()V

    .line 2041
    iget-object v0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    if-nez v0, :cond_1

    .line 72
    new-instance p0, Lcom/meawallet/mtp/InitializationFailedException;

    const/16 v0, 0x1f5

    const-string v1, "Error in result is null."

    invoke-direct {p0, v1, v0, v2}, Lcom/meawallet/mtp/InitializationFailedException;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    throw p0

    .line 3041
    :cond_1
    iget-object v0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 75
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    .line 77
    sget-object v1, Lcom/meawallet/mtp/dd;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "initialize() failed: errorCode=%s"

    invoke-static {v1, v0, v3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 79
    new-instance v1, Lcom/meawallet/mtp/InitializationFailedException;

    .line 4041
    iget-object p0, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 79
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object p0

    const-string v2, "MeaTokenPlatform"

    invoke-direct {v1, v2, v0, p0}, Lcom/meawallet/mtp/InitializationFailedException;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    throw v1

    .line 57
    :cond_2
    new-instance p0, Lcom/meawallet/mtp/InitializationFailedException;

    const-string v1, "MeaTokenPlatform"

    invoke-direct {p0, v1, v0, v2}, Lcom/meawallet/mtp/InitializationFailedException;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    throw p0
.end method

.method static a()Lcom/meawallet/mtp/dm;
    .locals 1

    .line 228
    sget-object v0, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    return-object v0
.end method

.method static a(Landroid/app/Activity;I)V
    .locals 5

    if-nez p0, :cond_0

    return-void

    .line 567
    :cond_0
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 573
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.nfc.cardemulation.action.ACTION_CHANGE_DEFAULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "category"

    const-string v2, "payment"

    .line 574
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "component"

    .line 575
    new-instance v2, Landroid/content/ComponentName;

    .line 576
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/meawallet/mtp/MeaHceService;

    .line 577
    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 575
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 579
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static a(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    .line 86
    new-instance v0, Lcom/meawallet/mtp/c;

    invoke-direct {v0}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/dd$1;

    invoke-direct {v1, p0, p1}, Lcom/meawallet/mtp/dd$1;-><init>(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V

    return-void
.end method

.method static a(Landroid/app/FragmentManager;Lcom/meawallet/mtp/MeaListener;)V
    .locals 1

    if-nez p0, :cond_0

    const-string p0, "Empty fragment manager."

    const/16 v0, 0x1f9

    .line 799
    invoke-static {p0, v0, p1}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    .line 805
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/n;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 807
    sget-object v0, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    invoke-virtual {v0, p0, p1}, Lcom/meawallet/mtp/cr;->a(Landroid/app/FragmentManager;Lcom/meawallet/mtp/MeaListener;)V

    return-void

    :cond_1
    const-string p0, "CDCVM type is not Biometric."

    const/16 v0, 0x389

    .line 809
    invoke-static {p0, v0, p1}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V
    .locals 9

    .line 300
    invoke-static {p2}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    invoke-static {p2}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    invoke-static {p0, p2}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    invoke-static {p0, p2}, Lcom/meawallet/mtp/dw;->f(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    .line 308
    :cond_0
    sget-object p0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    const/4 v0, 0x1

    .line 9066
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/meawallet/mtp/fu;->a(Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 9069
    :try_start_0
    new-instance v0, Lcom/meawallet/mtp/gm;

    iget-object v4, p0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v5, p0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v6, p0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    move-object v3, v0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/meawallet/mtp/gm;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V

    .line 9073
    invoke-virtual {v0}, Lcom/meawallet/mtp/gm;->a()V
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    .line 9076
    new-instance p1, Lcom/meawallet/mtp/ft;

    invoke-direct {p1}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance p1, Lcom/meawallet/mtp/fs;

    invoke-direct {p1}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p0

    invoke-static {p0, p2}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/meawallet/mtp/MeaPinRequestReceiver;)V
    .locals 2

    .line 916
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.meawallet.mtp.intent.ON_WALLET_PIN_RESET_REQUEST"

    .line 917
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.meawallet.mtp.intent.ON_CARD_PIN_RESET_REQUEST"

    .line 918
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 920
    invoke-static {p0}, Landroidx/i/a/a;->a(Landroid/content/Context;)Landroidx/i/a/a;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, Landroidx/i/a/a;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/meawallet/mtp/MeaTransactionReceiver;)V
    .locals 2

    .line 907
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.meawallet.mtp.intent.ON_TRANSACTION_SUBMITTED_MESSAGE"

    .line 908
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.meawallet.mtp.intent.ON_TRANSACTION_FAILURE_MESSAGE"

    .line 909
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.meawallet.mtp.intent.ON_AUTHENTICATION_REQUIRED_MESSAGE"

    .line 910
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 912
    invoke-static {p0}, Landroidx/i/a/a;->a(Landroid/content/Context;)Landroidx/i/a/a;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, Landroidx/i/a/a;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaGetAssetListener;)V
    .locals 9

    const/4 v0, 0x1

    .line 494
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 496
    invoke-static {p2}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 497
    invoke-static {p2}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 498
    invoke-static {p0, p2}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    .line 503
    :cond_0
    sget-object p0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 14161
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 14164
    :try_start_0
    new-instance v0, Lcom/meawallet/mtp/ge;

    iget-object v4, p0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v5, p0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v6, p0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    move-object v3, v0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/meawallet/mtp/ge;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/meawallet/mtp/MeaGetAssetListener;)V

    invoke-virtual {v0}, Lcom/meawallet/mtp/ge;->a()V
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    .line 14167
    new-instance p1, Lcom/meawallet/mtp/ft;

    invoke-direct {p1}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance p1, Lcom/meawallet/mtp/fs;

    invoke-direct {p1}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p0

    invoke-static {p0, p2}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V
    .locals 12

    move-object v0, p0

    move-object/from16 v10, p6

    .line 319
    invoke-static/range {p6 .. p6}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 320
    invoke-static/range {p6 .. p6}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 321
    invoke-static {p0, v10}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 322
    invoke-static {p0, v10}, Lcom/meawallet/mtp/dw;->f(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 328
    :cond_0
    invoke-static/range {p3 .. p4}, Lcom/meawallet/mtp/al;->a(J)Ljava/lang/String;

    move-result-object v7

    .line 330
    invoke-static {v7}, Lcom/meawallet/mtp/al;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 331
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f9

    const-string v2, "Passed \'tacAcceptedTimestamp\' parameter is not valid!"

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 332
    invoke-static {v10, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 336
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 9087
    :try_start_0
    iget-object v1, v0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    move-object v2, p1

    invoke-interface {v1, p1}, Lcom/meawallet/mtp/ci;->c(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v2, 0x4

    .line 9090
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 9114
    iget-object v4, v1, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 9091
    invoke-static {v4}, Lcom/meawallet/mtp/fu;->a(Lcom/meawallet/mtp/MeaEligibilityReceipt;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object v7, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    .line 9093
    new-instance v11, Lcom/meawallet/mtp/gb;

    iget-object v2, v0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v3, v0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v4, v0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    .line 10114
    iget-object v5, v1, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 10122
    iget-object v9, v1, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    move-object v0, v11

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v9

    move-object v6, p2

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    .line 9097
    invoke-direct/range {v0 .. v9}, Lcom/meawallet/mtp/gb;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/MeaEligibilityReceipt;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V

    .line 9101
    invoke-virtual {v11}, Lcom/meawallet/mtp/gb;->a()V

    return-void

    .line 9088
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "LdeCard object is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 9104
    new-instance v1, Lcom/meawallet/mtp/ft;

    invoke-direct {v1}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/fs;

    invoke-direct {v1}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {v0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object v0

    invoke-static {v0, v10}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    :cond_3
    :goto_0
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;)V
    .locals 10

    const/4 v0, 0x2

    .line 480
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    aput-object p2, v1, v3

    .line 483
    invoke-static {p3}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 484
    invoke-static {p3}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 485
    invoke-static {p0, p3}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    .line 490
    :cond_0
    sget-object p0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 13137
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v3

    .line 13141
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {v0, p1}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 13145
    new-instance v9, Lcom/meawallet/mtp/fy;

    iget-object v2, p0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v3, p0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v4, p0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    .line 14122
    iget-object v5, v0, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    move-object v1, v9

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    .line 13148
    invoke-direct/range {v1 .. v8}, Lcom/meawallet/mtp/fy;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;)V

    .line 13151
    invoke-virtual {v9}, Lcom/meawallet/mtp/fy;->a()V

    return-void

    .line 13143
    :cond_1
    new-instance p0, Lcom/meawallet/mtp/bn;

    const-string p1, "LdeJsonMeaCard object is null"

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    .line 13155
    new-instance p1, Lcom/meawallet/mtp/ft;

    invoke-direct {p1}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance p1, Lcom/meawallet/mtp/fs;

    invoke-direct {p1}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p0

    invoke-static {p0, p3}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 11

    const/4 v1, 0x2

    .line 260
    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v4, 0x1

    aput-object p2, v2, v4

    .line 262
    invoke-static {}, Lcom/meawallet/mtp/dd;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 263
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f8

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p3, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 268
    :cond_0
    invoke-static {p3}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 269
    invoke-static {p0, p3}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 270
    invoke-static {p0, p3}, Lcom/meawallet/mtp/dw;->f(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 275
    :cond_1
    sget-object v2, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    invoke-virtual {v2, p3}, Lcom/meawallet/mtp/cr;->a(Lcom/meawallet/mtp/MeaCoreListener;)Lcom/meawallet/mtp/CvmMethod;

    move-result-object v6

    if-nez v6, :cond_2

    return-void

    .line 283
    :cond_2
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->a(Landroid/content/Context;)Lcom/meawallet/mtp/ap;

    move-result-object v5

    .line 292
    sget-object v0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    const/4 v2, 0x3

    .line 9045
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    .line 9046
    invoke-static {v5}, Lcom/meawallet/mtp/fu;->a(Lcom/meawallet/mtp/ap;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p2, v2, v1

    .line 9049
    :try_start_0
    new-instance v10, Lcom/meawallet/mtp/gr;

    iget-object v1, v0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v2, v0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v3, v0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    iget-object v4, v0, Lcom/meawallet/mtp/fu;->c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

    move-object v0, v10

    move-object v7, p1

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v0 .. v9}, Lcom/meawallet/mtp/gr;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;Lcom/meawallet/mtp/ap;Lcom/meawallet/mtp/CvmMethod;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    .line 9057
    invoke-virtual {v10}, Lcom/meawallet/mtp/gr;->a()V
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 9060
    new-instance v1, Lcom/meawallet/mtp/ft;

    invoke-direct {v1}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/fs;

    invoke-direct {v1}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {v0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    :cond_3
    :goto_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    .line 527
    invoke-static {p0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    .line 528
    invoke-static {v0, p0}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 533
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 15158
    iput-object p0, v0, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    .line 535
    sget-object p0, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    sget-object v0, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/meawallet/mtp/cu;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaListener;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method static a(Lcom/meawallet/mtp/TaskPin$a;)V
    .locals 3

    const/4 v0, 0x1

    .line 519
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/meawallet/mtp/TaskPin$a;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 521
    new-instance v0, Lcom/meawallet/mtp/TaskPin;

    sget-object v1, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    invoke-direct {v0, v1, p0}, Lcom/meawallet/mtp/TaskPin;-><init>(Lcom/meawallet/mtp/dm;Lcom/meawallet/mtp/TaskPin$a;)V

    new-array p0, v2, [Ljava/lang/Void;

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/TaskPin;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V
    .locals 4

    .line 993
    sget-object v0, Lcom/meawallet/mtp/dd;->a:Ljava/lang/String;

    const-string v1, "handleWalletError(message = %s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, p1, v1, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 995
    new-instance v0, Lcom/meawallet/mtp/cy;

    invoke-direct {v0, p1, p0}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 996
    invoke-static {p2, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method static a(Ljava/lang/String;Lcom/meawallet/mtp/MeaGetCardTransactionHistoryListener;)V
    .locals 9

    const/4 v0, 0x1

    .line 453
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    .line 455
    invoke-static {p1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 457
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 11196
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v2

    .line 11199
    :try_start_0
    new-instance v0, Lcom/meawallet/mtp/gh;

    iget-object v4, v1, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v5, v1, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v6, v1, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    move-object v3, v0

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v3 .. v8}, Lcom/meawallet/mtp/gh;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/meawallet/mtp/MeaGetCardTransactionHistoryListener;)V

    invoke-virtual {v0}, Lcom/meawallet/mtp/gh;->a()V
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    .line 11202
    new-instance v0, Lcom/meawallet/mtp/ft;

    invoke-direct {v0}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance v0, Lcom/meawallet/mtp/fs;

    invoke-direct {v0}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method

.method static a(Z)V
    .locals 2

    .line 897
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "ALLOW_PAYMENTS_WHEN_LOCKED"

    invoke-virtual {v0, v1, p0}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Z)Z

    return-void
.end method

.method static a(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    .line 557
    invoke-static {p0, v0}, Lcom/meawallet/mtp/dw;->d(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p0

    return p0
.end method

.method static b(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;
    .locals 4

    const/4 v0, 0x1

    .line 359
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    .line 367
    :cond_0
    invoke-static {v1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v3

    if-nez v3, :cond_1

    return-object v1

    .line 372
    :cond_1
    sget-object v3, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    .line 10168
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v2

    .line 10170
    iget-object v0, v3, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-interface {v0, p0}, Lcom/meawallet/mtp/ci;->c(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object p0

    if-nez p0, :cond_2

    .line 10173
    sget-object p0, Lcom/meawallet/mtp/cu;->a:Ljava/lang/String;

    const/16 v0, 0x3f2

    const-string v3, "getCardByEligibilityReceipt failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-object v1

    .line 10178
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/ct;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/ct;-><init>(Lcom/meawallet/mtp/bt;)V

    return-object v0
.end method

.method static synthetic b(Landroid/app/Application;)Lcom/meawallet/mtp/d;
    .locals 0

    .line 25
    invoke-static {p0}, Lcom/meawallet/mtp/dd;->c(Landroid/app/Application;)Lcom/meawallet/mtp/d;

    move-result-object p0

    return-object p0
.end method

.method static b()Lcom/meawallet/mtp/l;
    .locals 1

    .line 232
    sget-object v0, Lcom/meawallet/mtp/dd;->c:Lcom/meawallet/mtp/l;

    return-object v0
.end method

.method static synthetic b(Landroid/content/Context;)V
    .locals 0

    .line 25
    invoke-static {p0}, Lcom/meawallet/mtp/dd;->c(Landroid/content/Context;)V

    return-void
.end method

.method static b(Landroid/content/Context;Lcom/meawallet/mtp/MeaPinRequestReceiver;)V
    .locals 0

    .line 928
    invoke-static {p0}, Landroidx/i/a/a;->a(Landroid/content/Context;)Landroidx/i/a/a;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroidx/i/a/a;->a(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method static b(Landroid/content/Context;Lcom/meawallet/mtp/MeaTransactionReceiver;)V
    .locals 0

    .line 924
    invoke-static {p0}, Landroidx/i/a/a;->a(Landroid/content/Context;)Landroidx/i/a/a;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroidx/i/a/a;->a(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 10

    .line 466
    invoke-static {p3}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 467
    invoke-static {p3}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 468
    invoke-static {p0, p3}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    .line 473
    :cond_0
    sget-object p0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    const/4 v0, 0x2

    .line 12110
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 12115
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {v0, p1}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 12119
    new-instance v9, Lcom/meawallet/mtp/gk;

    iget-object v2, p0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v3, p0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v4, p0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    .line 13122
    iget-object v5, v0, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    move-object v1, v9

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    .line 12122
    invoke-direct/range {v1 .. v8}, Lcom/meawallet/mtp/gk;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    .line 12125
    invoke-virtual {v9}, Lcom/meawallet/mtp/gk;->a()V

    return-void

    .line 12117
    :cond_1
    new-instance p0, Lcom/meawallet/mtp/bn;

    const-string p1, "LdeJsonMeaCard object is null"

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    .line 12129
    new-instance p1, Lcom/meawallet/mtp/ft;

    invoke-direct {p1}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance p1, Lcom/meawallet/mtp/fs;

    invoke-direct {p1}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p0

    invoke-static {p0, p3}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method static b(Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    .line 541
    invoke-static {p0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 546
    :cond_0
    sget-object v0, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    sget-object v1, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/meawallet/mtp/cu;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method static c(Ljava/lang/String;)Lcom/meawallet/mtp/MeaTransactionMessage;
    .locals 2

    .line 989
    sget-object v0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 19242
    iget-object v0, v0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    const-class v1, Lcom/meawallet/mtp/MeaTransactionMessage;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaTransactionMessage;

    return-object p0
.end method

.method static c()Lcom/meawallet/mtp/cu;
    .locals 1

    .line 236
    sget-object v0, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    return-object v0
.end method

.method private static declared-synchronized c(Landroid/app/Application;)Lcom/meawallet/mtp/d;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/meawallet/mtp/dd;

    monitor-enter v0

    .line 151
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/meawallet/mtp/az;->a(Landroid/content/Context;)V

    .line 153
    new-instance v1, Lcom/meawallet/mtp/aj;

    invoke-direct {v1}, Lcom/meawallet/mtp/aj;-><init>()V

    .line 5019
    invoke-static {p0}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a(Landroid/content/Context;)Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    move-result-object v5

    .line 154
    new-instance v1, Lcom/meawallet/mtp/dq;

    invoke-direct {v1, v5}, Lcom/meawallet/mtp/dq;-><init>(Lcom/meawallet/mtp/ai;)V

    sput-object v1, Lcom/meawallet/mtp/dd;->j:Lcom/meawallet/mtp/dq;

    .line 156
    invoke-static {}, Lcom/meawallet/mtp/JsonUtil;->b()Lcom/google/gson/Gson;

    move-result-object v1

    sput-object v1, Lcom/meawallet/mtp/dd;->g:Lcom/google/gson/Gson;

    .line 157
    sget-object v1, Lcom/meawallet/mtp/dd;->g:Lcom/google/gson/Gson;

    invoke-static {v5, v1}, Lcom/meawallet/mtp/cj;->a(Lcom/meawallet/mtp/ai;Lcom/google/gson/Gson;)Lcom/meawallet/mtp/ci;

    move-result-object v1

    sput-object v1, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    .line 159
    invoke-static {}, Lcom/meawallet/mtp/dd;->l()Z

    move-result v1

    invoke-static {v1}, Lcom/meawallet/mtp/n;->a(Z)Lcom/meawallet/mtp/CdCvmType;

    move-result-object v9

    if-nez v9, :cond_0

    .line 162
    invoke-static {}, Lcom/meawallet/mtp/n;->d()Z

    move-result p0

    invoke-static {}, Lcom/meawallet/mtp/n;->e()Z

    move-result v1

    invoke-static {p0, v1}, Lcom/meawallet/mtp/n;->a(ZZ)Lcom/meawallet/mtp/d;

    move-result-object p0
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    .line 165
    :cond_0
    :try_start_1
    sget-object v1, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    invoke-interface {v1}, Lcom/meawallet/mtp/ci;->b()Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x10

    .line 167
    invoke-static {v1}, Lcom/meawallet/mtp/h;->a(I)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->toHexString()Ljava/lang/String;

    move-result-object v1

    .line 168
    sget-object v2, Lcom/meawallet/mtp/dd;->j:Lcom/meawallet/mtp/dq;

    .line 5029
    iget-object v2, v2, Lcom/meawallet/mtp/dq;->a:Lcom/meawallet/mtp/ai;

    invoke-interface {v2}, Lcom/meawallet/mtp/ai;->b()Ljava/lang/String;

    move-result-object v2

    .line 170
    new-instance v3, Lcom/meawallet/mtp/cb;

    invoke-direct {v3, v1, v2}, Lcom/meawallet/mtp/cb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    sget-object v1, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    invoke-interface {v1, v3}, Lcom/meawallet/mtp/ci;->a(Lcom/meawallet/mtp/cb;)V

    .line 174
    :cond_1
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    .line 176
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/meawallet/mtp/bo;->a(Landroid/content/Context;)Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    move-result-object v1

    sput-object v1, Lcom/meawallet/mtp/dd;->m:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    .line 178
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_2

    sget-object v1, Lcom/meawallet/mtp/dd;->m:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    if-nez v1, :cond_2

    .line 179
    new-instance p0, Lcom/meawallet/mtp/d;

    invoke-direct {p0}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x1f5

    const-string v3, "Firebase job dispatcher is null"

    invoke-direct {v1, v2, v3}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 6029
    iput-object v1, p0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;
    :try_end_1
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    monitor-exit v0

    return-object p0

    .line 182
    :cond_2
    :try_start_2
    new-instance v1, Lcom/meawallet/mtp/m;

    sget-object v2, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    sget-object v3, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    sget-object v4, Lcom/meawallet/mtp/dd;->m:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    invoke-direct {v1, v2, v3, v4}, Lcom/meawallet/mtp/m;-><init>(Landroid/content/Context;Lcom/meawallet/mtp/ci;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;)V

    sput-object v1, Lcom/meawallet/mtp/dd;->k:Lcom/meawallet/mtp/m;

    .line 184
    new-instance v1, Lcom/meawallet/mtp/fr;

    sget-object v2, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/fr;-><init>(Lcom/meawallet/mtp/ci;)V

    sput-object v1, Lcom/meawallet/mtp/dd;->l:Lcom/meawallet/mtp/fr;

    .line 186
    new-instance v1, Lcom/meawallet/mtp/l;

    sget-object v2, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/l;-><init>(Lcom/meawallet/mtp/ci;)V

    sput-object v1, Lcom/meawallet/mtp/dd;->c:Lcom/meawallet/mtp/l;

    .line 188
    new-instance v1, Lcom/meawallet/mtp/dm;

    sget-object v4, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    sget-object v6, Lcom/meawallet/mtp/dd;->k:Lcom/meawallet/mtp/m;

    sget-object v7, Lcom/meawallet/mtp/dd;->c:Lcom/meawallet/mtp/l;

    sget-object v8, Lcom/meawallet/mtp/dd;->l:Lcom/meawallet/mtp/fr;

    move-object v2, v1

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/meawallet/mtp/dm;-><init>(Landroid/app/Application;Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/ai;Lcom/meawallet/mtp/m;Lcom/meawallet/mtp/l;Lcom/meawallet/mtp/fr;Lcom/meawallet/mtp/CdCvmType;)V

    sput-object v1, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 196
    new-instance p0, Lcom/meawallet/mtp/fu;

    sget-object v1, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    sget-object v2, Lcom/meawallet/mtp/dd;->j:Lcom/meawallet/mtp/dq;

    sget-object v3, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 6199
    iget-object v3, v3, Lcom/meawallet/mtp/dm;->g:Lcom/meawallet/mtp/x;

    .line 198
    sget-object v4, Lcom/meawallet/mtp/dd;->g:Lcom/google/gson/Gson;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/meawallet/mtp/fu;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;Lcom/google/gson/Gson;)V

    sput-object p0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 201
    new-instance p0, Lcom/meawallet/mtp/cu;

    sget-object v6, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    sget-object v7, Lcom/meawallet/mtp/dd;->c:Lcom/meawallet/mtp/l;

    sget-object v8, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    sget-object v9, Lcom/meawallet/mtp/dd;->m:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    sget-object v10, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    move-object v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/meawallet/mtp/cu;-><init>(Lcom/meawallet/mtp/dm;Lcom/meawallet/mtp/l;Lcom/meawallet/mtp/ci;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Lcom/meawallet/mtp/fu;)V

    sput-object p0, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    .line 203
    new-instance p0, Lcom/meawallet/mtp/cr;

    sget-object v1, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    invoke-direct {p0, v1}, Lcom/meawallet/mtp/cr;-><init>(Lcom/meawallet/mtp/dn;)V

    sput-object p0, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    .line 205
    sget-object p0, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    .line 6649
    iget-object p0, p0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    const/4 v1, 0x0

    invoke-interface {p0, v1}, Lcom/meawallet/mtp/ci;->a(Z)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/bt;

    .line 6651
    sget-object v4, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    .line 7054
    iget-object v2, v2, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 6651
    invoke-virtual {v4, v2}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 p0, 0x1

    goto :goto_0

    :cond_4
    const/4 p0, 0x0

    .line 6658
    :goto_0
    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v1

    if-eqz p0, :cond_5

    .line 206
    sget-object p0, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    invoke-static {p0}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result p0

    .line 207
    sget-object v1, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    sget-object v2, Lcom/meawallet/mtp/dd;->m:Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;

    invoke-static {v1, v2, p0}, Lcom/meawallet/mtp/bo;->b(Landroid/content/Context;Lcom/firebase/jobdispatcher/FirebaseJobDispatcher;Z)V
    :try_end_2
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/meawallet/mtp/bm; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 220
    :cond_5
    :try_start_3
    new-instance p0, Lcom/meawallet/mtp/d;

    invoke-direct {p0}, Lcom/meawallet/mtp/d;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 217
    :try_start_4
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    invoke-static {p0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object p0

    .line 8029
    iput-object p0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 217
    monitor-exit v0

    return-object v1

    .line 150
    :goto_1
    monitor-exit v0

    throw p0
.end method

.method private static c(Landroid/content/Context;)V
    .locals 1

    .line 1001
    invoke-static {}, Lcom/meawallet/mtp/di;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1004
    invoke-static {}, Lcom/meawallet/mtp/di;->b()V

    .line 1005
    invoke-static {p0}, Lcom/meawallet/mtp/di;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 10

    .line 880
    invoke-static {p3}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 881
    invoke-static {p3}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 882
    invoke-static {p0, p3}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 887
    :cond_0
    sget-object v0, Lcom/meawallet/mtp/dd;->b:Lcom/meawallet/mtp/fu;

    .line 888
    invoke-static {p0}, Lcom/meawallet/mtp/ar;->a(Landroid/content/Context;)Lcom/meawallet/mtp/ap;

    move-result-object v5

    .line 889
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object p0

    invoke-static {p0}, Lcom/meawallet/mtp/n;->a(Lcom/meawallet/mtp/CdCvmType;)Lcom/meawallet/mtp/CvmMethod;

    move-result-object v6

    const/4 p0, 0x4

    .line 19177
    new-array p0, p0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, p0, v1

    const/4 v1, 0x1

    .line 19178
    invoke-static {v5}, Lcom/meawallet/mtp/fu;->a(Lcom/meawallet/mtp/ap;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p0, v1

    const/4 v1, 0x2

    invoke-static {v6}, Lcom/meawallet/mtp/fu;->a(Lcom/meawallet/mtp/CvmMethod;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p0, v1

    const/4 v1, 0x3

    aput-object p2, p0, v1

    .line 19181
    :try_start_0
    new-instance p0, Lcom/meawallet/mtp/hc;

    iget-object v2, v0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    iget-object v3, v0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    iget-object v4, v0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    move-object v1, p0

    move-object v7, p1

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/meawallet/mtp/hc;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/ap;Lcom/meawallet/mtp/CvmMethod;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    .line 19188
    invoke-virtual {p0}, Lcom/meawallet/mtp/hc;->a()V
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    .line 19191
    new-instance p1, Lcom/meawallet/mtp/ft;

    invoke-direct {p1}, Lcom/meawallet/mtp/ft;-><init>()V

    new-instance p1, Lcom/meawallet/mtp/fs;

    invoke-direct {p1}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p0}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p0

    invoke-static {p0, p3}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method static d()Lcom/meawallet/mtp/m;
    .locals 1

    .line 240
    sget-object v0, Lcom/meawallet/mtp/dd;->k:Lcom/meawallet/mtp/m;

    return-object v0
.end method

.method static e()Lcom/meawallet/mtp/ci;
    .locals 1

    .line 244
    sget-object v0, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    return-object v0
.end method

.method static f()Lcom/meawallet/mtp/k;
    .locals 1

    .line 248
    sget-object v0, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 8289
    iget-object v0, v0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    return-object v0
.end method

.method static g()Lcom/meawallet/mtp/fr;
    .locals 1

    .line 252
    sget-object v0, Lcom/meawallet/mtp/dd;->l:Lcom/meawallet/mtp/fr;

    return-object v0
.end method

.method static h()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 379
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 386
    :cond_0
    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/dd;->d:Lcom/meawallet/mtp/cu;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/cu;->a(Z)Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    .line 388
    sget-object v2, Lcom/meawallet/mtp/dd;->a:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 390
    invoke-static {v1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    return-object v0
.end method

.method static i()Lcom/meawallet/mtp/MeaCard;
    .locals 2

    const/4 v0, 0x0

    .line 400
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 10248
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 10289
    iget-object v1, v1, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    .line 11166
    iget-object v1, v1, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    if-eqz v1, :cond_1

    .line 409
    new-instance v0, Lcom/meawallet/mtp/ct;

    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    return-object v0

    :cond_1
    return-object v0
.end method

.method static j()Lcom/meawallet/mtp/MeaCard;
    .locals 4

    const/4 v0, 0x0

    .line 418
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 423
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/k;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 425
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    if-eqz v1, :cond_1

    .line 427
    new-instance v0, Lcom/meawallet/mtp/ct;

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    return-object v0

    :cond_1
    return-object v0
.end method

.method static k()Lcom/meawallet/mtp/MeaCard;
    .locals 4

    const/4 v0, 0x0

    .line 435
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 440
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/k;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 442
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    if-eqz v1, :cond_1

    .line 446
    new-instance v0, Lcom/meawallet/mtp/ct;

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    return-object v0

    :cond_1
    return-object v0
.end method

.method static l()Z
    .locals 1

    .line 507
    sget-object v0, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->d()Z

    move-result v0

    return v0

    .line 509
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/NotInitializedException;

    invoke-direct {v0}, Lcom/meawallet/mtp/NotInitializedException;-><init>()V

    throw v0
.end method

.method static m()Z
    .locals 2

    .line 513
    sget-object v0, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/meawallet/mtp/dd;->f:Lcom/meawallet/mtp/ci;

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->l()Lcom/meawallet/mtp/LdePinState;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    .line 515
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/NotInitializedException;

    invoke-direct {v0}, Lcom/meawallet/mtp/NotInitializedException;-><init>()V

    throw v0
.end method

.method static n()V
    .locals 4

    .line 636
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0

    if-nez v0, :cond_0

    .line 639
    sget-object v0, Lcom/meawallet/mtp/dd;->a:Ljava/lang/String;

    const/16 v1, 0x1f5

    const-string v2, "Failed to request cardholder authentication, saved CD CVM type is null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 644
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/cr;->a(Lcom/meawallet/mtp/CdCvmType;)V

    return-void
.end method

.method static o()V
    .locals 3

    .line 653
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 16125
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->d:Lcom/meawallet/mtp/MeaAuthenticationListener;

    .line 655
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 660
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/BuildConfig;->SAVE_AUTH_WHEN_LOCKED:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    invoke-static {v1}, Lcom/meawallet/mtp/ar;->f(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 661
    invoke-static {}, Lcom/meawallet/mtp/fp;->g()V

    .line 664
    :cond_1
    sget-object v1, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    .line 16348
    iget-boolean v1, v1, Lcom/meawallet/mtp/cr;->d:Z

    if-eqz v1, :cond_2

    .line 666
    sget-object v0, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    invoke-virtual {v0}, Lcom/meawallet/mtp/cr;->b()V

    .line 668
    sget-object v0, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    const/4 v1, 0x0

    .line 16357
    iput-boolean v1, v0, Lcom/meawallet/mtp/cr;->d:Z

    return-void

    .line 670
    :cond_2
    invoke-static {}, Lcom/meawallet/mtp/n;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 672
    sget-object v0, Lcom/meawallet/mtp/dd;->i:Lcom/meawallet/mtp/cr;

    invoke-virtual {v0}, Lcom/meawallet/mtp/cr;->b()V

    return-void

    :cond_3
    const-string v1, "CDCVM type is not DeviceUnlock."

    const/16 v2, 0x389

    .line 676
    invoke-static {v1, v2, v0}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method

.method static p()V
    .locals 3

    .line 682
    invoke-static {}, Lcom/meawallet/mtp/n;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    const-class v2, Lcom/meawallet/mtp/DeviceUnlockService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.meawallet.mtp.DeviceUnlockService.ACTION_DEVICE_UNLOCK"

    .line 687
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 690
    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 692
    sget-object v1, Lcom/meawallet/mtp/dd;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method static q()Z
    .locals 2

    .line 816
    sget-object v0, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 17284
    iget-object v0, v0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    .line 818
    invoke-static {}, Lcom/meawallet/mtp/n;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 819
    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/cv;->isCdCvmSuccessful(Lcom/mastercard/mpsdk/componentinterface/Card;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static r()V
    .locals 1

    .line 829
    sget-object v0, Lcom/meawallet/mtp/dd;->e:Lcom/meawallet/mtp/dm;

    .line 18284
    iget-object v0, v0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    .line 831
    invoke-virtual {v0}, Lcom/meawallet/mtp/cv;->a()V

    return-void
.end method

.method static s()Z
    .locals 2

    .line 903
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "ALLOW_PAYMENTS_WHEN_LOCKED"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static t()Lcom/meawallet/mtp/CdCvmType;
    .locals 1

    .line 933
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0

    return-object v0
.end method

.method static u()I
    .locals 1

    const/16 v0, 0x2ac

    return v0
.end method

.method static v()Ljava/lang/String;
    .locals 1

    const-string v0, "mtp-swedbank-lt-prod-2.0.8-684"

    return-object v0
.end method

.method static w()Ljava/lang/String;
    .locals 1

    const-string v0, "release"

    return-object v0
.end method

.method static x()Ljava/lang/String;
    .locals 1

    const-string v0, "ALWAYS_CDCVM"

    return-object v0
.end method

.method static y()Z
    .locals 1

    .line 1042
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->SAVE_AUTH_WHEN_LOCKED:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static z()Landroid/content/Context;
    .locals 1

    .line 1050
    sget-object v0, Lcom/meawallet/mtp/dd;->h:Landroid/content/Context;

    return-object v0
.end method
