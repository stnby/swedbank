.class final Lcom/meawallet/mtp/u$b;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/u;


# direct methods
.method private constructor <init>(Lcom/meawallet/mtp/u;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/meawallet/mtp/u;B)V
    .locals 0

    .line 121
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/u$b;-><init>(Lcom/meawallet/mtp/u;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 126
    iget-object v0, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v0}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    monitor-enter v0

    .line 127
    :try_start_0
    iget-object v1, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v1}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    invoke-static {}, Lcom/meawallet/mtp/u;->d()Ljava/lang/String;

    .line 130
    monitor-exit v0

    return-void

    .line 132
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    iget-object v0, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v0}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 136
    :goto_0
    iget-object v0, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v0}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 138
    :try_start_1
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v2}, Lcom/meawallet/mtp/u;->c(Lcom/meawallet/mtp/u;)V

    .line 140
    invoke-static {}, Lcom/meawallet/mtp/u;->d()Ljava/lang/String;

    .line 142
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    iget-object v3, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v3}, Lcom/meawallet/mtp/u;->d(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/u$a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/meawallet/mtp/u$a;->take()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/meawallet/mtp/ee;

    invoke-static {v2, v3}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;Lcom/meawallet/mtp/ee;)Lcom/meawallet/mtp/ee;

    .line 144
    invoke-static {}, Lcom/meawallet/mtp/u;->d()Ljava/lang/String;

    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v3}, Lcom/meawallet/mtp/u;->e(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/ee;

    move-result-object v3

    invoke-interface {v3}, Lcom/meawallet/mtp/ee;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 146
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v2}, Lcom/meawallet/mtp/u;->e(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/ee;

    move-result-object v2

    invoke-interface {v2}, Lcom/meawallet/mtp/ee;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v2}, Lcom/meawallet/mtp/u;->f(Lcom/meawallet/mtp/u;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 147
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v2}, Lcom/meawallet/mtp/u;->g(Lcom/meawallet/mtp/u;)V

    .line 149
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v2}, Lcom/meawallet/mtp/u;->d(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/u$a;

    move-result-object v2

    iget-object v3, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v3}, Lcom/meawallet/mtp/u;->e(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/ee;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/meawallet/mtp/u$a;->b(Lcom/meawallet/mtp/ee;)Z

    goto :goto_1

    .line 151
    :cond_1
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    iget-object v3, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v3}, Lcom/meawallet/mtp/u;->e(Lcom/meawallet/mtp/u;)Lcom/meawallet/mtp/ee;

    move-result-object v3

    iget-object v4, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v4}, Lcom/meawallet/mtp/u;->h(Lcom/meawallet/mtp/u;)Lcom/google/gson/Gson;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;Lcom/meawallet/mtp/ee;Lcom/google/gson/Gson;)V

    .line 154
    :goto_1
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;Lcom/meawallet/mtp/ee;)Lcom/meawallet/mtp/ee;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 156
    invoke-static {}, Lcom/meawallet/mtp/u;->d()Ljava/lang/String;

    move-result-object v3

    const-string v4, "QueueProcessingThread:run(): Error in QueueProcessingThread"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    iget-object v2, p0, Lcom/meawallet/mtp/u$b;->a:Lcom/meawallet/mtp/u;

    invoke-static {v2}, Lcom/meawallet/mtp/u;->a(Lcom/meawallet/mtp/u;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_0

    :cond_2
    return-void

    :catchall_0
    move-exception v1

    .line 132
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
