.class public Lcom/meawallet/mtp/NotInitializedException;
.super Lcom/meawallet/mtp/MeaCheckedException;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 4

    const-string v0, "%1$s is not initialized."

    const/4 v1, 0x1

    .line 13
    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Module"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x65

    invoke-direct {p0, v0, v1}, Lcom/meawallet/mtp/MeaCheckedException;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 3

    const-string v0, "%1$s is not initialized."

    const/4 v1, 0x1

    .line 18
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0x65

    invoke-direct {p0, p1, v0}, Lcom/meawallet/mtp/MeaCheckedException;-><init>(Ljava/lang/String;I)V

    return-void
.end method
