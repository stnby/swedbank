.class final Lcom/meawallet/mtp/dd$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/dd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/meawallet/mtp/MeaCard;

.field final synthetic c:Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;

.field final synthetic d:Lcom/meawallet/mtp/dd;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/dd;Ljava/lang/String;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;)V
    .locals 0

    .line 775
    iput-object p1, p0, Lcom/meawallet/mtp/dd$4;->d:Lcom/meawallet/mtp/dd;

    iput-object p2, p0, Lcom/meawallet/mtp/dd$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/meawallet/mtp/dd$4;->b:Lcom/meawallet/mtp/MeaCard;

    iput-object p4, p0, Lcom/meawallet/mtp/dd$4;->c:Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 5

    .line 2781
    invoke-static {}, Lcom/meawallet/mtp/dd;->G()Lcom/meawallet/mtp/cr;

    move-result-object v0

    iget-object v1, p0, Lcom/meawallet/mtp/dd$4;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/meawallet/mtp/dd$4;->b:Lcom/meawallet/mtp/MeaCard;

    invoke-interface {v2}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    .line 3255
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    .line 3257
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 3258
    iget-object v3, v0, Lcom/meawallet/mtp/cr;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    invoke-interface {v3, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;->encryptWalletData([B)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v1

    .line 3260
    iget-object v3, v0, Lcom/meawallet/mtp/cr;->a:Lcom/meawallet/mtp/cv;

    invoke-virtual {v3, v1}, Lcom/meawallet/mtp/cv;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V

    .line 3261
    iget-object v1, v0, Lcom/meawallet/mtp/cr;->a:Lcom/meawallet/mtp/cv;

    check-cast v1, Lcom/meawallet/mtp/j;

    .line 4063
    iput-object v2, v1, Lcom/meawallet/mtp/j;->a:Ljava/lang/String;

    .line 3263
    iget-object v0, v0, Lcom/meawallet/mtp/cr;->c:Lcom/meawallet/mtp/ds;

    invoke-virtual {v0}, Lcom/meawallet/mtp/ds;->b()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .line 1788
    iget-object p1, p0, Lcom/meawallet/mtp/dd$4;->c:Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;

    if-eqz p1, :cond_0

    .line 1789
    iget-object p1, p0, Lcom/meawallet/mtp/dd$4;->c:Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;

    iget-object v0, p0, Lcom/meawallet/mtp/dd$4;->b:Lcom/meawallet/mtp/MeaCard;

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;->onPinSet(Lcom/meawallet/mtp/MeaCard;)V

    :cond_0
    return-void
.end method
