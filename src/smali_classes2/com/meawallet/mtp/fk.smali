.class abstract Lcom/meawallet/mtp/fk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;


# instance fields
.field private a:Lcom/meawallet/mtp/do;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/do;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/meawallet/mtp/fk;->a:Lcom/meawallet/mtp/do;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/meawallet/mtp/fk;->a:Lcom/meawallet/mtp/do;

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/do;->a(Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;)V

    return-void
.end method

.method final b()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/meawallet/mtp/fk;->a:Lcom/meawallet/mtp/do;

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/do;->b(Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;)V

    return-void
.end method

.method public onCardMobilePinResetCompleted(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onCardPinChangeCompleted(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onCardPinChangeFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onCardPinSetCompleted(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onCardPinSetFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onCardProvisionCompleted(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onCardProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onChangeWalletMobilePinStarted(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onDeleteCardCompleted(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onReplenishCompleted(Ljava/lang/String;I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onRequestSessionCompleted()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onRequestSessionFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onSetWalletPinCompleted()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onSystemHealthCompleted()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onTaskStatusCompleted(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onTaskStatusFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onWalletMobilePinResetCompleted()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onWalletPinChangeCompleted()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onWalletPinChangeFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
