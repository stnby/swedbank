.class final enum Lcom/meawallet/mtp/f$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/f$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/meawallet/mtp/f$a;

.field public static final enum b:Lcom/meawallet/mtp/f$a;

.field public static final enum c:Lcom/meawallet/mtp/f$a;

.field public static final enum d:Lcom/meawallet/mtp/f$a;

.field public static final enum e:Lcom/meawallet/mtp/f$a;

.field public static final enum f:Lcom/meawallet/mtp/f$a;

.field public static final enum g:Lcom/meawallet/mtp/f$a;

.field public static final enum h:Lcom/meawallet/mtp/f$a;

.field public static final enum i:Lcom/meawallet/mtp/f$a;

.field public static final enum j:Lcom/meawallet/mtp/f$a;

.field public static final enum k:Lcom/meawallet/mtp/f$a;

.field public static final enum l:Lcom/meawallet/mtp/f$a;

.field public static final enum m:Lcom/meawallet/mtp/f$a;

.field public static final enum n:Lcom/meawallet/mtp/f$a;

.field public static final enum o:Lcom/meawallet/mtp/f$a;

.field public static final enum p:Lcom/meawallet/mtp/f$a;

.field public static final enum q:Lcom/meawallet/mtp/f$a;

.field public static final enum r:Lcom/meawallet/mtp/f$a;

.field public static final enum s:Lcom/meawallet/mtp/f$a;

.field public static final enum t:Lcom/meawallet/mtp/f$a;

.field public static final enum u:Lcom/meawallet/mtp/f$a;

.field public static final enum v:Lcom/meawallet/mtp/f$a;

.field public static final enum w:Lcom/meawallet/mtp/f$a;

.field public static final enum x:Lcom/meawallet/mtp/f$a;

.field private static final synthetic y:[Lcom/meawallet/mtp/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 25
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PROVISION_SUCCESS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->a:Lcom/meawallet/mtp/f$a;

    .line 26
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PROVISION_FAILED"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->b:Lcom/meawallet/mtp/f$a;

    .line 27
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "PAYMENT_TOKENS_REPLENISH_SUCCESS"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->c:Lcom/meawallet/mtp/f$a;

    .line 28
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "PAYMENT_TOKENS_REPLENISH_FAILED"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->d:Lcom/meawallet/mtp/f$a;

    .line 29
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_DELETE_SUCCESS"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->e:Lcom/meawallet/mtp/f$a;

    .line 30
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_DELETE_FAILED"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->f:Lcom/meawallet/mtp/f$a;

    .line 31
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PIN_CHANGE_SUCCESS"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->g:Lcom/meawallet/mtp/f$a;

    .line 32
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PIN_CHANGE_FAILED"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->h:Lcom/meawallet/mtp/f$a;

    .line 33
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PIN_SET_SUCCESS"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->i:Lcom/meawallet/mtp/f$a;

    .line 34
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PIN_SET_FAILED"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->j:Lcom/meawallet/mtp/f$a;

    .line 35
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "WALLET_PIN_CHANGE_SUCCESS"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->k:Lcom/meawallet/mtp/f$a;

    .line 36
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "WALLET_PIN_CHANGE_FAILED"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->l:Lcom/meawallet/mtp/f$a;

    .line 37
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "WALLET_PIN_SET_SUCCESS"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->m:Lcom/meawallet/mtp/f$a;

    .line 38
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "WALLET_PIN_SET_FAILED"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->n:Lcom/meawallet/mtp/f$a;

    .line 39
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "SYSTEM_HEALTH_SUCCESS"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->o:Lcom/meawallet/mtp/f$a;

    .line 40
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "SYSTEM_HEALTH_FAILED"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->p:Lcom/meawallet/mtp/f$a;

    .line 41
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PIN_CHANGE_STARTED"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->q:Lcom/meawallet/mtp/f$a;

    .line 42
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "WALLET_PIN_CHANGE_STARTED"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->r:Lcom/meawallet/mtp/f$a;

    .line 43
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "CARD_PIN_RESET_SUCCESS"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->s:Lcom/meawallet/mtp/f$a;

    .line 44
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "WALLET_PIN_RESET_SUCCESS"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->t:Lcom/meawallet/mtp/f$a;

    .line 45
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "TASK_STATUS_SUCCESS"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->u:Lcom/meawallet/mtp/f$a;

    .line 46
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "TASK_STATUS_FAILURE"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->v:Lcom/meawallet/mtp/f$a;

    .line 47
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "SESSION_REQUEST_SUCCESS"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->w:Lcom/meawallet/mtp/f$a;

    .line 48
    new-instance v0, Lcom/meawallet/mtp/f$a;

    const-string v1, "SESSION_REQUEST_FAILURE"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/meawallet/mtp/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/f$a;->x:Lcom/meawallet/mtp/f$a;

    const/16 v0, 0x18

    .line 24
    new-array v0, v0, [Lcom/meawallet/mtp/f$a;

    sget-object v1, Lcom/meawallet/mtp/f$a;->a:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->b:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/f$a;->c:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/f$a;->d:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/f$a;->e:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/f$a;->f:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/meawallet/mtp/f$a;->g:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/meawallet/mtp/f$a;->h:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v9

    sget-object v1, Lcom/meawallet/mtp/f$a;->i:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v10

    sget-object v1, Lcom/meawallet/mtp/f$a;->j:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v11

    sget-object v1, Lcom/meawallet/mtp/f$a;->k:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v12

    sget-object v1, Lcom/meawallet/mtp/f$a;->l:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v13

    sget-object v1, Lcom/meawallet/mtp/f$a;->m:Lcom/meawallet/mtp/f$a;

    aput-object v1, v0, v14

    sget-object v1, Lcom/meawallet/mtp/f$a;->n:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->o:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->p:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->q:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->r:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->s:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->t:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->u:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->v:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->w:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/f$a;->x:Lcom/meawallet/mtp/f$a;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sput-object v0, Lcom/meawallet/mtp/f$a;->y:[Lcom/meawallet/mtp/f$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/f$a;
    .locals 1

    .line 24
    const-class v0, Lcom/meawallet/mtp/f$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/f$a;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/f$a;
    .locals 1

    .line 24
    sget-object v0, Lcom/meawallet/mtp/f$a;->y:[Lcom/meawallet/mtp/f$a;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/f$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/f$a;

    return-object v0
.end method
