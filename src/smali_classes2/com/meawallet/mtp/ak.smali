.class final Lcom/meawallet/mtp/ak;
.super Ljavax/net/ssl/SSLSocketFactory;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/ak$a;
    }
.end annotation


# instance fields
.field private final a:Ljavax/net/ssl/SSLSocketFactory;

.field private final b:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljavax/net/ssl/SSLSocketFactory;[Ljava/lang/String;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljavax/net/ssl/SSLSocketFactory;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    .line 23
    iput-object p2, p0, Lcom/meawallet/mtp/ak;->b:[Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/net/Socket;)Ljava/net/Socket;
    .locals 2

    .line 35
    instance-of v0, p1, Ljavax/net/ssl/SSLSocket;

    if-eqz v0, :cond_1

    .line 36
    new-instance v0, Lcom/meawallet/mtp/ak$a;

    check-cast p1, Ljavax/net/ssl/SSLSocket;

    invoke-direct {v0, p0, p1}, Lcom/meawallet/mtp/ak$a;-><init>(Lcom/meawallet/mtp/ak;Ljavax/net/ssl/SSLSocket;)V

    .line 37
    iget-object p1, p0, Lcom/meawallet/mtp/ak;->b:[Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/meawallet/mtp/ak;->b:[Ljava/lang/String;

    array-length p1, p1

    if-lez p1, :cond_0

    .line 38
    move-object p1, v0

    check-cast p1, Ljavax/net/ssl/SSLSocket;

    iget-object v1, p0, Lcom/meawallet/mtp/ak;->b:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    :cond_0
    move-object p1, v0

    :cond_1
    return-object p1
.end method


# virtual methods
.method public final createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/ak;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public final createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/ak;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public final createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/ak;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public final createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/ak;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public final createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/meawallet/mtp/ak;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public final getDefaultCipherSuites()[Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getDefaultCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSupportedCipherSuites()[Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/meawallet/mtp/ak;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
