.class public final enum Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaTransactionMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TransactionAuthorizationStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUTHORIZED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum CLEARED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum DECLINED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field public static final enum REVERSED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 194
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    const-string v1, "AUTHORIZED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->AUTHORIZED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    .line 196
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    const-string v1, "DECLINED"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->DECLINED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    .line 198
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    const-string v1, "CLEARED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->CLEARED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    .line 200
    new-instance v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    const-string v1, "REVERSED"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->REVERSED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    const/4 v0, 0x4

    .line 193
    new-array v0, v0, [Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->AUTHORIZED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->DECLINED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->CLEARED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->REVERSED:Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->a:[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .locals 1

    .line 193
    const-class v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;
    .locals 1

    .line 193
    sget-object v0, Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->a:[Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaTransactionMessage$TransactionAuthorizationStatus;

    return-object v0
.end method
