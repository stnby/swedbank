.class public abstract enum Lcom/meawallet/mtp/MeaCardState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaCardState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/meawallet/mtp/MeaCardState;

.field public static final enum ACTIVE:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum AUTHENTICATION_COMPLETE:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum AUTHENTICATION_INITIALIZED:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum DIGITIZATION_STARTED:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum DIGITIZED:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum PROVISIONED:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum PROVISION_FAILED:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum SUSPENDED:Lcom/meawallet/mtp/MeaCardState;

.field public static final enum UNKNOWN:Lcom/meawallet/mtp/MeaCardState;


# instance fields
.field final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 14
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$1;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->UNKNOWN:Lcom/meawallet/mtp/MeaCardState;

    .line 23
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$5;

    const-string v1, "DIGITIZATION_STARTED"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$5;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->DIGITIZATION_STARTED:Lcom/meawallet/mtp/MeaCardState;

    .line 32
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$6;

    const-string v1, "REQUIRE_ADDITIONAL_AUTHENTICATION"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$6;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaCardState;

    .line 42
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$7;

    const-string v1, "DIGITIZED"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$7;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->DIGITIZED:Lcom/meawallet/mtp/MeaCardState;

    .line 51
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$8;

    const-string v1, "AUTHENTICATION_INITIALIZED"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$8;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_INITIALIZED:Lcom/meawallet/mtp/MeaCardState;

    .line 61
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$9;

    const-string v1, "AUTHENTICATION_COMPLETE"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$9;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_COMPLETE:Lcom/meawallet/mtp/MeaCardState;

    .line 70
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$10;

    const-string v1, "PROVISIONED"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$10;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->PROVISIONED:Lcom/meawallet/mtp/MeaCardState;

    .line 79
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$11;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$11;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->ACTIVE:Lcom/meawallet/mtp/MeaCardState;

    .line 88
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$12;

    const-string v1, "SUSPENDED"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$12;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->SUSPENDED:Lcom/meawallet/mtp/MeaCardState;

    .line 97
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$2;

    const-string v1, "PROVISION_FAILED"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$2;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->PROVISION_FAILED:Lcom/meawallet/mtp/MeaCardState;

    .line 107
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$3;

    const-string v1, "DEACTIVATED"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$3;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    .line 118
    new-instance v0, Lcom/meawallet/mtp/MeaCardState$4;

    const-string v1, "MARKED_FOR_DELETION"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/MeaCardState$4;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    const/16 v0, 0xc

    .line 8
    new-array v0, v0, [Lcom/meawallet/mtp/MeaCardState;

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->UNKNOWN:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DIGITIZATION_STARTED:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DIGITIZED:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_INITIALIZED:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_COMPLETE:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->PROVISIONED:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->ACTIVE:Lcom/meawallet/mtp/MeaCardState;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->SUSPENDED:Lcom/meawallet/mtp/MeaCardState;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->PROVISION_FAILED:Lcom/meawallet/mtp/MeaCardState;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sput-object v0, Lcom/meawallet/mtp/MeaCardState;->$VALUES:[Lcom/meawallet/mtp/MeaCardState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 131
    iput p3, p0, Lcom/meawallet/mtp/MeaCardState;->value:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILcom/meawallet/mtp/MeaCardState$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1, p2, p3}, Lcom/meawallet/mtp/MeaCardState;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(I)Lcom/meawallet/mtp/MeaCardState;
    .locals 5

    .line 151
    invoke-static {}, Lcom/meawallet/mtp/MeaCardState;->values()[Lcom/meawallet/mtp/MeaCardState;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 152
    invoke-virtual {v3}, Lcom/meawallet/mtp/MeaCardState;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 157
    :cond_1
    sget-object p0, Lcom/meawallet/mtp/MeaCardState;->UNKNOWN:Lcom/meawallet/mtp/MeaCardState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardState;
    .locals 1

    .line 8
    const-class v0, Lcom/meawallet/mtp/MeaCardState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaCardState;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaCardState;
    .locals 1

    .line 8
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->$VALUES:[Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaCardState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaCardState;

    return-object v0
.end method


# virtual methods
.method abstract getRules()Lcom/meawallet/mtp/ev;
.end method

.method public getValue()I
    .locals 1

    .line 140
    iget v0, p0, Lcom/meawallet/mtp/MeaCardState;->value:I

    return v0
.end method
