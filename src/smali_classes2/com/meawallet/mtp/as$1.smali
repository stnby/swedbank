.class final Lcom/meawallet/mtp/as$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/Records;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/as;->a([Lcom/mastercard/mpsdk/card/profile/RecordsJson;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/RecordsJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/RecordsJson;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/meawallet/mtp/as$1;->a:Lcom/mastercard/mpsdk/card/profile/RecordsJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getRecordNumber()B
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/meawallet/mtp/as$1;->a:Lcom/mastercard/mpsdk/card/profile/RecordsJson;

    iget v0, v0, Lcom/mastercard/mpsdk/card/profile/RecordsJson;->recordNumber:I

    int-to-byte v0, v0

    return v0
.end method

.method public final getRecordValue()[B
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/meawallet/mtp/as$1;->a:Lcom/mastercard/mpsdk/card/profile/RecordsJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/RecordsJson;->recordValue:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getSfi()[B
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/meawallet/mtp/as$1;->a:Lcom/mastercard/mpsdk/card/profile/RecordsJson;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/RecordsJson;->sfi:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method
