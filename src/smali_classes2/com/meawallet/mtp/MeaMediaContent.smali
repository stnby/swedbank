.class public Lcom/meawallet/mtp/MeaMediaContent;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaMediaContent$Type;
    }
.end annotation


# instance fields
.field private a:Lcom/meawallet/mtp/MeaMediaContent$Type;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field

.field private c:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "width"
    .end annotation
.end field

.field private d:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "height"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Ljava/lang/String;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/meawallet/mtp/MeaMediaContent;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/meawallet/mtp/MeaMediaContent;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/meawallet/mtp/e;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/meawallet/mtp/MeaMediaContent;->d:I

    return v0
.end method

.method public getType()Lcom/meawallet/mtp/MeaMediaContent$Type;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/meawallet/mtp/MeaMediaContent;->a:Lcom/meawallet/mtp/MeaMediaContent$Type;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/meawallet/mtp/MeaMediaContent;->c:I

    return v0
.end method
