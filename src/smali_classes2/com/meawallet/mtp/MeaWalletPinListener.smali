.class public interface abstract Lcom/meawallet/mtp/MeaWalletPinListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onWalletPinChangeFailed(Lcom/meawallet/mtp/MeaError;)V
.end method

.method public abstract onWalletPinChangeSuccess()V
.end method

.method public abstract onWalletPinResetSuccess()V
.end method

.method public abstract onWalletPinSetFailed(Lcom/meawallet/mtp/MeaError;)V
.end method

.method public abstract onWalletPinSetSuccess()V
.end method
