.class public final Lcom/meawallet/mtp/MeaTokenPlatform;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaTokenPlatform$Configuration;,
        Lcom/meawallet/mtp/MeaTokenPlatform$Rns;,
        Lcom/meawallet/mtp/MeaTokenPlatform$CdCvm;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "MeaTokenPlatform"

.field private static b:Landroid/content/Context;

.field private static volatile c:Lcom/meawallet/mtp/dd;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private static d:Ljava/util/concurrent/Semaphore;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 41
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->d:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()Lcom/meawallet/mtp/dd;
    .locals 2

    .line 106
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->c:Lcom/meawallet/mtp/dd;

    if-eqz v0, :cond_0

    .line 107
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->c:Lcom/meawallet/mtp/dd;

    return-object v0

    .line 110
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/NotInitializedException;

    const-string v1, "MeaTokenPlatform"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/NotInitializedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a(Lcom/meawallet/mtp/dd;)V
    .locals 0

    .line 114
    sput-object p0, Lcom/meawallet/mtp/MeaTokenPlatform;->c:Lcom/meawallet/mtp/dd;

    return-void
.end method

.method public static addTransactionLimit(Lcom/meawallet/mtp/MeaTransactionLimit;Lcom/meawallet/mtp/MeaListener;)V
    .locals 3

    const/4 v0, 0x1

    .line 921
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 924
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    .line 7082
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/fr$1;

    invoke-direct {v2, v0, p0, p1}, Lcom/meawallet/mtp/fr$1;-><init>(Lcom/meawallet/mtp/fr;Lcom/meawallet/mtp/MeaTransactionLimit;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 927
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static addTransactionLimits(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;",
            "Lcom/meawallet/mtp/MeaListener;",
            ")V"
        }
    .end annotation

    .line 941
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    .line 7106
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/fr$2;

    invoke-direct {v2, v0, p0, p1}, Lcom/meawallet/mtp/fr$2;-><init>(Lcom/meawallet/mtp/fr;Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 944
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static authenticateWithDeviceUnlock()V
    .locals 0

    .line 619
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->o()V

    return-void
.end method

.method public static authenticateWithFingerprint(Landroid/app/FragmentManager;Lcom/meawallet/mtp/MeaListener;)V
    .locals 1

    .line 648
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {p0, p1}, Lcom/meawallet/mtp/dd;->a(Landroid/app/FragmentManager;Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 650
    :catch_0
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public static authenticateWithWalletPin(Ljava/lang/String;Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;)V
    .locals 4

    const/16 v0, 0x65

    .line 632
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    move-result-object v1

    .line 4703
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p0, "Empty PIN."

    const/16 v1, 0x1f9

    .line 4704
    invoke-static {p0, v1, p1}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    .line 4710
    :cond_0
    sget-object v2, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string p0, "CVM type is not Wallet PIN."

    const/16 v1, 0x389

    .line 4711
    invoke-static {p0, v1, p1}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    .line 4718
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/meawallet/mtp/dd;->m()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4719
    new-instance v2, Lcom/meawallet/mtp/cy;

    const/16 v3, 0x38f

    invoke-direct {v2, v3}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v2}, Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;->onFailure(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 4727
    :cond_2
    :try_start_2
    new-instance v2, Lcom/meawallet/mtp/c;

    invoke-direct {v2}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v3, Lcom/meawallet/mtp/dd$3;

    invoke-direct {v3, v1, p0, p1}, Lcom/meawallet/mtp/dd$3;-><init>(Lcom/meawallet/mtp/dd;Ljava/lang/String;Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;)V

    invoke-virtual {v2, v3}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V

    return-void

    .line 4722
    :catch_0
    new-instance p0, Lcom/meawallet/mtp/cy;

    invoke-direct {p0, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, p0}, Lcom/meawallet/mtp/MeaWalletPinAuthenticationListener;->onFailure(Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    .line 634
    :catch_1
    new-instance p0, Lcom/meawallet/mtp/cy;

    invoke-direct {p0, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method static b()Landroid/content/Context;
    .locals 2

    .line 127
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    return-object v0

    .line 131
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/NotInitializedException;

    const-string v1, "MeaTokenPlatform"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/NotInitializedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static c()V
    .locals 4

    const/4 v0, 0x1

    .line 1266
    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Lcom/meawallet/mtp/MeaTokenPlatform;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 1269
    :try_start_0
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->d:Ljava/util/concurrent/Semaphore;

    const-wide/16 v1, 0xf

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 1272
    sget-object v1, Lcom/meawallet/mtp/MeaTokenPlatform;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static changeWalletPin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    .line 543
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 545
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    new-instance v0, Lcom/meawallet/mtp/TaskPin$c;

    sget-object v1, Lcom/meawallet/mtp/TaskPin$PinOperation;->CHANGE:Lcom/meawallet/mtp/TaskPin$PinOperation;

    invoke-direct {v0, p0, p1, v1}, Lcom/meawallet/mtp/TaskPin$c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/TaskPin$PinOperation;)V

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->a(Lcom/meawallet/mtp/TaskPin$a;)V

    return-void
.end method

.method public static clearAllTransactionLimits(Lcom/meawallet/mtp/MeaListener;)V
    .locals 3

    .line 1057
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    .line 8237
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/fr$6;

    invoke-direct {v2, v0, p0}, Lcom/meawallet/mtp/fr$6;-><init>(Lcom/meawallet/mtp/fr;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 v0, 0x65

    .line 1060
    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static clearAuthForPayment()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 663
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->r()V

    return-void
.end method

.method public static clearAuthenticationForPayment()V
    .locals 0

    .line 674
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->r()V

    return-void
.end method

.method public static clearListeners()V
    .locals 2

    .line 781
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    const/4 v1, 0x0

    .line 6174
    iput-object v1, v0, Lcom/meawallet/mtp/ax;->d:Lcom/meawallet/mtp/MeaAuthenticationListener;

    .line 6175
    iput-object v1, v0, Lcom/meawallet/mtp/ax;->a:Lcom/meawallet/mtp/MeaCardProvisionListener;

    .line 6176
    iput-object v1, v0, Lcom/meawallet/mtp/ax;->b:Lcom/meawallet/mtp/MeaCardReplenishListener;

    .line 6177
    iput-object v1, v0, Lcom/meawallet/mtp/ax;->c:Lcom/meawallet/mtp/MeaWalletPinListener;

    .line 6178
    iput-object v1, v0, Lcom/meawallet/mtp/ax;->e:Lcom/meawallet/mtp/MeaListener;

    .line 6179
    iput-object v1, v0, Lcom/meawallet/mtp/ax;->f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    .line 6180
    iget-object v1, v0, Lcom/meawallet/mtp/ax;->g:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->clear()V

    .line 6181
    iget-object v1, v0, Lcom/meawallet/mtp/ax;->h:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->clear()V

    .line 6182
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->i:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    return-void
.end method

.method public static completeAdditionalAuthenticationForDigitization(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;)V
    .locals 2

    const/4 v0, 0x2

    .line 268
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 271
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaCompleteAuthenticationListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 273
    invoke-static {p2, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static completeDigitization(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V
    .locals 9

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "completeDigitization("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "eligibilityReceiptValue = "

    .line 208
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "termsAndConditionsAssetId = "

    .line 209
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "termsAndConditionsAcceptTimestamp = "

    .line 210
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "securityCode = "

    .line 211
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    sget-object v2, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    move-object v3, p0

    move-object v4, p1

    move-wide v5, p2

    move-object v7, p4

    move-object v8, p5

    invoke-static/range {v2 .. v8}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 223
    invoke-static {p5, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method static d()V
    .locals 1

    .line 1278
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    if-nez v0, :cond_0

    .line 1279
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_0
    return-void
.end method

.method public static delete(Lcom/meawallet/mtp/MeaListener;)V
    .locals 3

    .line 685
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    move-result-object v0

    .line 5596
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/dd$2;

    invoke-direct {v2, v0, p0}, Lcom/meawallet/mtp/dd$2;-><init>(Lcom/meawallet/mtp/dd;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 v0, 0x65

    .line 688
    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static deleteAllCards(Lcom/meawallet/mtp/MeaListener;)V
    .locals 1

    .line 376
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {p0}, Lcom/meawallet/mtp/dd;->a(Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 v0, 0x65

    .line 378
    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static deleteAllPaymentTokens()V
    .locals 1

    .line 507
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    const/4 v0, 0x0

    .line 2585
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2590
    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    .line 4051
    :try_start_0
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 3345
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->deleteAllTransactionCredentials()V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 3347
    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    :cond_0
    return-void
.end method

.method public static getAllowPaymentsWhenLocked()Z
    .locals 1

    .line 883
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->s()Z

    move-result v0

    return v0
.end method

.method public static getAsset(Ljava/lang/String;Lcom/meawallet/mtp/MeaGetAssetListener;)V
    .locals 2

    const/4 v0, 0x1

    .line 286
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 289
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaGetAssetListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 291
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static getCardByEligibilityReceipt(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;
    .locals 2

    const/4 v0, 0x1

    .line 331
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 333
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {p0}, Lcom/meawallet/mtp/dd;->b(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;

    move-result-object p0

    return-object p0
.end method

.method public static getCardById(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;
    .locals 2

    const/4 v0, 0x1

    .line 317
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 319
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {p0}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;

    move-result-object p0

    return-object p0
.end method

.method public static getCardSelectedForContactlessPayment()Lcom/meawallet/mtp/MeaCard;
    .locals 1

    .line 406
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->i()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    return-object v0
.end method

.method public static getCards()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    .line 346
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->h()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultCardForContactless()Lcom/meawallet/mtp/MeaCard;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 420
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->j()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultCardForContactlessPayments()Lcom/meawallet/mtp/MeaCard;
    .locals 1

    .line 433
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->j()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultCardForRemote()Lcom/meawallet/mtp/MeaCard;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 447
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->k()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultCardForRemotePayments()Lcom/meawallet/mtp/MeaCard;
    .locals 1

    .line 460
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->k()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultTransactionLimit()Ljava/lang/Integer;
    .locals 3

    .line 1005
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    .line 7205
    iget-object v0, v0, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->n()Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    .line 7207
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    return-object v0
.end method

.method public static getPaymentAppInstanceId(Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;)V
    .locals 3

    .line 847
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    move-result-object v0

    .line 6837
    invoke-static {p0}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 6842
    :cond_0
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/dd$5;

    invoke-direct {v2, v0, p0}, Lcom/meawallet/mtp/dd$5;-><init>(Lcom/meawallet/mtp/dd;Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_1
    :goto_0
    return-void

    :catch_0
    const/16 v0, 0x65

    .line 849
    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static getSelectedCardForContactless()Lcom/meawallet/mtp/MeaCard;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 393
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->i()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    return-object v0
.end method

.method public static getTransactionLimit(Ljava/util/Currency;)Lcom/meawallet/mtp/MeaTransactionLimit;
    .locals 4

    const/4 v0, 0x1

    .line 957
    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 959
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v1

    .line 7132
    iget-object v1, v1, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {v1, p0}, Lcom/meawallet/mtp/ci;->b(Ljava/util/Currency;)Lcom/meawallet/mtp/MeaTransactionLimit;

    move-result-object p0

    .line 7134
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v3

    return-object p0
.end method

.method public static getTransactionLimits()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;"
        }
    .end annotation

    .line 971
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    .line 7141
    iget-object v0, v0, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->m()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    .line 7143
    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    return-object v0
.end method

.method public static initialize(Landroid/app/Application;)V
    .locals 1

    .line 55
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->c()V

    .line 57
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->c:Lcom/meawallet/mtp/dd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 74
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 64
    :try_start_1
    invoke-static {p0, v0}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 74
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    return-void

    .line 69
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    .line 71
    invoke-static {p0}, Lcom/meawallet/mtp/dd;->a(Landroid/app/Application;)Lcom/meawallet/mtp/dd;

    move-result-object p0

    .line 2114
    sput-object p0, Lcom/meawallet/mtp/MeaTokenPlatform;->c:Lcom/meawallet/mtp/dd;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    return-void

    :catchall_0
    move-exception p0

    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->d()V

    throw p0
.end method

.method public static initialize(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V
    .locals 1

    .line 95
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 100
    :cond_0
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    .line 102
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dd;->a(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public static initializeAdditionalAuthenticationForDigitization(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    const/4 v0, 0x2

    .line 243
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 247
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2}, Lcom/meawallet/mtp/dd;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 249
    invoke-static {p2, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static initializeDigitization(Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V
    .locals 1

    .line 167
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 169
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static isDefaultPaymentApplication()Z
    .locals 1

    .line 472
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static isDefaultPaymentApplication(Landroid/content/Context;)Z
    .locals 0

    .line 484
    invoke-static {p0}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method public static isInitialized()Z
    .locals 1

    .line 123
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->c:Lcom/meawallet/mtp/dd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static isRegistered()Z
    .locals 1

    .line 304
    invoke-static {}, Lcom/meawallet/mtp/dd;->l()Z

    move-result v0

    return v0
.end method

.method public static isWalletPinSet()Z
    .locals 1

    .line 520
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->m()Z

    move-result v0

    return v0
.end method

.method public static markAllCardsForDeletion(Lcom/meawallet/mtp/MeaListener;)V
    .locals 1

    .line 360
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {p0}, Lcom/meawallet/mtp/dd;->b(Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 v0, 0x65

    .line 362
    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static register(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    const/4 v0, 0x2

    .line 146
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 149
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 151
    invoke-static {p2, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static registerDeviceUnlockReceiver()V
    .locals 0

    .line 608
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->p()V

    return-void
.end method

.method public static registerPinRequestReceiver(Landroid/content/Context;Lcom/meawallet/mtp/MeaPinRequestReceiver;)V
    .locals 0

    .line 809
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaPinRequestReceiver;)V

    return-void
.end method

.method public static registerTransactionReceiver(Landroid/content/Context;Lcom/meawallet/mtp/MeaTransactionReceiver;)V
    .locals 0

    .line 795
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dd;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaTransactionReceiver;)V

    return-void
.end method

.method public static removeAuthenticationListener()V
    .locals 1

    .line 565
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/ax;->b()V

    return-void
.end method

.method public static removeCardProvisionListener()V
    .locals 1

    .line 709
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/ax;->c()V

    return-void
.end method

.method public static removeCardReplenishListener()V
    .locals 1

    .line 729
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/ax;->d()V

    return-void
.end method

.method public static removeDefaultTransactionLimit(Lcom/meawallet/mtp/MeaListener;)V
    .locals 3

    .line 1040
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    .line 8213
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/fr$5;

    invoke-direct {v2, v0, p0}, Lcom/meawallet/mtp/fr$5;-><init>(Lcom/meawallet/mtp/fr;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 v0, 0x65

    .line 1043
    invoke-static {p0, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static removeDigitizedCardStateChangeListener()V
    .locals 2

    .line 752
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    const/4 v1, 0x0

    .line 6170
    iput-object v1, v0, Lcom/meawallet/mtp/ax;->f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    return-void
.end method

.method public static removeTransactionLimit(Ljava/util/Currency;Lcom/meawallet/mtp/MeaListener;)V
    .locals 3

    const/4 v0, 0x1

    .line 981
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 984
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    .line 7149
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/fr$3;

    invoke-direct {v2, v0, p0, p1}, Lcom/meawallet/mtp/fr$3;-><init>(Lcom/meawallet/mtp/fr;Ljava/util/Currency;Lcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 987
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static removeWalletPinListener()V
    .locals 1

    .line 772
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/ax;->e()V

    return-void
.end method

.method public static requestCardholderAuthentication()V
    .locals 0

    .line 589
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->n()V

    return-void
.end method

.method public static requestCardholderVerification()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 577
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->n()V

    return-void
.end method

.method public static setAccessToken(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 909
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 910
    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/meawallet/mtp/dx;->a(Landroid/content/Context;)V

    .line 911
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "AUTHORIZATION_ACCESS_TOKEN"

    invoke-virtual {v0, v1, p0}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public static setAllowPaymentsWhenLocked(Z)V
    .locals 1

    .line 867
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->z()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/ar;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 871
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {p0}, Lcom/meawallet/mtp/dd;->a(Z)V

    return-void

    .line 868
    :cond_0
    new-instance p0, Lcom/meawallet/mtp/MeaDeviceLockedException;

    invoke-direct {p0}, Lcom/meawallet/mtp/MeaDeviceLockedException;-><init>()V

    throw p0
.end method

.method public static setAuthenticationListener(Lcom/meawallet/mtp/MeaAuthenticationListener;)V
    .locals 1

    .line 556
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/ax;->a(Lcom/meawallet/mtp/MeaAuthenticationListener;)V

    return-void
.end method

.method public static setCardProvisionListener(Lcom/meawallet/mtp/MeaCardProvisionListener;)V
    .locals 1

    .line 700
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/ax;->a(Lcom/meawallet/mtp/MeaCardProvisionListener;)V

    return-void
.end method

.method public static setCardReplenishListener(Lcom/meawallet/mtp/MeaCardReplenishListener;)V
    .locals 1

    .line 720
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/ax;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;)V

    return-void
.end method

.method public static setDefaultPaymentApplication(Landroid/app/Activity;I)V
    .locals 3

    const/4 v0, 0x1

    .line 494
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 496
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dd;->a(Landroid/app/Activity;I)V

    return-void
.end method

.method public static setDefaultTransactionLimit(ILcom/meawallet/mtp/MeaListener;)V
    .locals 3

    const/4 v0, 0x1

    .line 1019
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 1022
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v0

    if-gez p0, :cond_0

    .line 8175
    new-instance p0, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x1f9

    const-string v1, "Negative \'TransactionLimit\' value!"

    invoke-direct {p0, v0, v1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 8176
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 8180
    :cond_0
    new-instance v1, Lcom/meawallet/mtp/c;

    invoke-direct {v1}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/fr$4;

    invoke-direct {v2, v0, p0, p1}, Lcom/meawallet/mtp/fr$4;-><init>(Lcom/meawallet/mtp/fr;ILcom/meawallet/mtp/MeaListener;)V

    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 1025
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public static setDigitizedCardStateChangeListener(Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;)V
    .locals 1

    .line 743
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 6166
    iput-object p0, v0, Lcom/meawallet/mtp/ax;->f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    return-void
.end method

.method public static setWalletPin(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 530
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 532
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    new-instance v0, Lcom/meawallet/mtp/TaskPin$c;

    sget-object v1, Lcom/meawallet/mtp/TaskPin$PinOperation;->SET:Lcom/meawallet/mtp/TaskPin$PinOperation;

    invoke-direct {v0, p0, v1}, Lcom/meawallet/mtp/TaskPin$c;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/TaskPin$PinOperation;)V

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->a(Lcom/meawallet/mtp/TaskPin$a;)V

    return-void
.end method

.method public static setWalletPinListener(Lcom/meawallet/mtp/MeaWalletPinListener;)V
    .locals 1

    .line 763
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/ax;->a(Lcom/meawallet/mtp/MeaWalletPinListener;)V

    return-void
.end method

.method public static unregisterPinRequestReceiver(Landroid/content/Context;Lcom/meawallet/mtp/MeaPinRequestReceiver;)V
    .locals 0

    .line 833
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dd;->b(Landroid/content/Context;Lcom/meawallet/mtp/MeaPinRequestReceiver;)V

    return-void
.end method

.method public static unregisterTransactionReceiver(Landroid/content/Context;Lcom/meawallet/mtp/MeaTransactionReceiver;)V
    .locals 0

    .line 821
    invoke-static {p0, p1}, Lcom/meawallet/mtp/dd;->b(Landroid/content/Context;Lcom/meawallet/mtp/MeaTransactionReceiver;)V

    return-void
.end method

.method public static updateDeviceInfo(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 2

    const/4 v0, 0x2

    .line 894
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 897
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    sget-object v0, Lcom/meawallet/mtp/MeaTokenPlatform;->b:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2}, Lcom/meawallet/mtp/dd;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    const/16 p0, 0x65

    .line 899
    invoke-static {p2, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method
