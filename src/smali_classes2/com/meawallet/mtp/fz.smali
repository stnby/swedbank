.class final Lcom/meawallet/mtp/fz;
.super Lcom/meawallet/mtp/gx;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenUniqueReference"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "authenticationCode"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/gx;-><init>(Lcom/meawallet/mtp/PaymentNetwork;)V

    .line 19
    iput-object p2, p0, Lcom/meawallet/mtp/fz;->a:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/meawallet/mtp/fz;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final b()V
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/meawallet/mtp/fz;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 27
    iget-object v0, p0, Lcom/meawallet/mtp/fz;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Authentication code is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_1
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Token unique reference is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
