.class final Lcom/meawallet/mtp/du;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# instance fields
.field j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 10

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p10

    .line 39
    invoke-direct/range {v0 .. v8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object/from16 v0, p8

    .line 48
    iput-object v0, v9, Lcom/meawallet/mtp/du;->j:Ljava/lang/String;

    const/4 v0, 0x0

    .line 49
    iput-object v0, v9, Lcom/meawallet/mtp/du;->k:Ljava/lang/String;

    .line 50
    iput-object v0, v9, Lcom/meawallet/mtp/du;->l:Ljava/lang/String;

    move-object/from16 v0, p9

    .line 51
    iput-object v0, v9, Lcom/meawallet/mtp/du;->m:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    .line 116
    invoke-static {p1, p2}, Lcom/meawallet/mtp/s;->b(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 7

    .line 57
    :try_start_0
    new-instance v6, Lcom/meawallet/mtp/dt;

    iget-object v1, p0, Lcom/meawallet/mtp/du;->j:Ljava/lang/String;

    .line 1085
    iget-object v2, p0, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    .line 58
    iget-object v3, p0, Lcom/meawallet/mtp/du;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/meawallet/mtp/du;->l:Ljava/lang/String;

    iget-object v5, p0, Lcom/meawallet/mtp/du;->m:Ljava/lang/String;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/dt;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2033
    invoke-virtual {p1, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/du;->a(Ljava/lang/String;)Lcom/meawallet/mtp/y;

    move-result-object v0

    .line 2034
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/du;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 86
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 88
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 86
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/du;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 81
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 82
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/du;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 74
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute Notify Provision Result command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/du;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 67
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute Notify Provision Result HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/du;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    .line 126
    iput-object p2, p0, Lcom/meawallet/mtp/du;->c:Ljava/lang/String;

    .line 127
    sget-object p4, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 4317
    iput-object p4, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 128
    iput-object p1, p0, Lcom/meawallet/mtp/du;->h:Lcom/meawallet/mtp/ae;

    .line 130
    iget-object p1, p0, Lcom/meawallet/mtp/du;->h:Lcom/meawallet/mtp/ae;

    sget-object p4, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-ne p1, p4, :cond_0

    .line 131
    iget-object p1, p0, Lcom/meawallet/mtp/du;->d:Lcom/meawallet/mtp/ad;

    iget-object p4, p0, Lcom/meawallet/mtp/du;->j:Ljava/lang/String;

    invoke-interface {p1, p4, p2, p3}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method final a(Lcom/meawallet/mtp/s;)V
    .locals 0

    .line 120
    sget-object p1, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 3317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 121
    sget-object p1, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    iput-object p1, p0, Lcom/meawallet/mtp/du;->i:Lcom/meawallet/mtp/ah;

    .line 122
    iget-object p1, p0, Lcom/meawallet/mtp/du;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p1, p0}, Lcom/meawallet/mtp/ad;->a(Lcom/meawallet/mtp/ee;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/notifyProvisioningResult"

    return-object v0
.end method

.method public final l()V
    .locals 5

    .line 95
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 2317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 96
    iget-object v0, p0, Lcom/meawallet/mtp/du;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/du;->j:Ljava/lang/String;

    const-string v2, "CANCELED"

    const-string v3, "Notify Provisioning Result command cancelled or Could not get a valid session from CMS-D"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 102
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/du;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 107
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
