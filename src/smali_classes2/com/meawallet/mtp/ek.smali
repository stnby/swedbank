.class Lcom/meawallet/mtp/ek;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/ej;


# static fields
.field private static final a:Ljava/lang/String; = "ek"

.field private static volatile c:Lcom/meawallet/mtp/ej;


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/meawallet/mtp/ek;->b:I

    return-void
.end method

.method static a(I)Lcom/meawallet/mtp/ej;
    .locals 2

    .line 27
    sget-object v0, Lcom/meawallet/mtp/ek;->c:Lcom/meawallet/mtp/ej;

    if-nez v0, :cond_1

    .line 29
    const-class v0, Lcom/meawallet/mtp/ej;

    monitor-enter v0

    .line 31
    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/ek;->c:Lcom/meawallet/mtp/ej;

    if-nez v1, :cond_0

    .line 32
    new-instance v1, Lcom/meawallet/mtp/ek;

    invoke-direct {v1, p0}, Lcom/meawallet/mtp/ek;-><init>(I)V

    sput-object v1, Lcom/meawallet/mtp/ek;->c:Lcom/meawallet/mtp/ej;

    .line 34
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 37
    :cond_1
    :goto_0
    sget-object p0, Lcom/meawallet/mtp/ek;->c:Lcom/meawallet/mtp/ej;

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/google/gson/Gson;Lcom/meawallet/mtp/el;)Lcom/meawallet/mtp/ei;
    .locals 7

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 1136
    iget-object v1, p2, Lcom/meawallet/mtp/el;->d:Lcom/meawallet/mtp/eg;

    invoke-virtual {v1}, Lcom/meawallet/mtp/eg;->a()V

    .line 55
    invoke-virtual {p2}, Lcom/meawallet/mtp/el;->e()V

    .line 57
    invoke-virtual {p2}, Lcom/meawallet/mtp/el;->d()Lcom/meawallet/mtp/ep;

    .line 59
    invoke-virtual {p2}, Lcom/meawallet/mtp/el;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 61
    new-array v3, v2, [Ljava/lang/Object;

    aput-object v1, v3, v0

    .line 1142
    iget-object v3, p2, Lcom/meawallet/mtp/el;->d:Lcom/meawallet/mtp/eg;

    .line 2024
    invoke-virtual {p1, v3}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 65
    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/JsonUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 69
    invoke-virtual {p2}, Lcom/meawallet/mtp/el;->c()Ljava/lang/String;

    move-result-object v3

    .line 72
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 73
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    new-instance v3, Lcom/meawallet/mtp/da;

    invoke-virtual {p2}, Lcom/meawallet/mtp/el;->b()Lcom/meawallet/mtp/MeaHttpMethod;

    move-result-object v5

    .line 2169
    iget-object v6, p2, Lcom/meawallet/mtp/el;->c:Ljavax/net/ssl/X509TrustManager;

    .line 76
    invoke-direct {v3, v5, v1, v6}, Lcom/meawallet/mtp/da;-><init>(Lcom/meawallet/mtp/MeaHttpMethod;Ljava/lang/String;Ljavax/net/ssl/X509TrustManager;)V

    iget v1, p0, Lcom/meawallet/mtp/ek;->b:I

    .line 3082
    iput v1, v3, Lcom/meawallet/mtp/da;->a:I

    .line 3100
    iput-object p1, v3, Lcom/meawallet/mtp/da;->b:Ljava/lang/String;

    .line 4088
    iput-object v4, v3, Lcom/meawallet/mtp/da;->c:Ljava/util/List;

    .line 4154
    iget-object p1, p2, Lcom/meawallet/mtp/el;->a:Lcom/meawallet/mtp/ef;

    if-eqz p1, :cond_0

    .line 4156
    iget-object p1, p2, Lcom/meawallet/mtp/el;->a:Lcom/meawallet/mtp/ef;

    .line 5028
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 6024
    invoke-virtual {p1}, Lcom/meawallet/mtp/ef;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "Authorization"

    const-string v5, "Bearer %s"

    .line 5031
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/meawallet/mtp/ef;->a()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v0

    invoke-static {v5, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 6112
    :cond_1
    :goto_0
    iput-object v1, v3, Lcom/meawallet/mtp/da;->d:Ljava/util/Map;

    .line 6163
    iget-object p1, p2, Lcom/meawallet/mtp/el;->b:Lcom/meawallet/mtp/bf;

    .line 82
    invoke-virtual {v3, p1}, Lcom/meawallet/mtp/da;->a(Lcom/meawallet/mtp/bf;)Lcom/meawallet/mtp/bj;

    move-result-object p1
    :try_end_0
    .catch Lcom/meawallet/mtp/bi; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    new-instance p2, Lcom/meawallet/mtp/ei;

    invoke-virtual {p1}, Lcom/meawallet/mtp/bj;->getResponseCode()I

    move-result v0

    invoke-virtual {p1}, Lcom/meawallet/mtp/bj;->a()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/meawallet/mtp/ei;-><init>(ILjava/lang/String;)V

    return-object p2

    :catch_0
    move-exception p1

    .line 85
    sget-object p2, Lcom/meawallet/mtp/ek;->a:Ljava/lang/String;

    const-string v1, "Request execution failed."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2, p1, v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    new-instance p2, Lcom/meawallet/mtp/ei;

    invoke-virtual {p1}, Lcom/meawallet/mtp/bi;->getMeaError()Lcom/meawallet/mtp/MeaError;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/meawallet/mtp/ei;-><init>(Lcom/meawallet/mtp/MeaError;)V

    return-object p2

    .line 51
    :cond_2
    new-instance p1, Lcom/meawallet/mtp/bn;

    const-string p2, "Request object is null."

    invoke-direct {p1, p2, v0}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;B)V

    throw p1
.end method
