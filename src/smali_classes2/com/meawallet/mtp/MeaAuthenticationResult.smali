.class final enum Lcom/meawallet/mtp/MeaAuthenticationResult;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaAuthenticationResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/meawallet/mtp/MeaAuthenticationResult;

.field public static final enum EXPIRED_CODE:Lcom/meawallet/mtp/MeaAuthenticationResult;

.field public static final enum EXPIRED_SESSION:Lcom/meawallet/mtp/MeaAuthenticationResult;

.field public static final enum INCORRECT_CODE:Lcom/meawallet/mtp/MeaAuthenticationResult;

.field public static final enum INCORRECT_CODE_RETRIES_EXCEEDED:Lcom/meawallet/mtp/MeaAuthenticationResult;

.field public static final enum SUCCESS:Lcom/meawallet/mtp/MeaAuthenticationResult;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 7
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationResult;

    const-string v1, "SUCCESS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaAuthenticationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->SUCCESS:Lcom/meawallet/mtp/MeaAuthenticationResult;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationResult;

    const-string v1, "INCORRECT_CODE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaAuthenticationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->INCORRECT_CODE:Lcom/meawallet/mtp/MeaAuthenticationResult;

    .line 9
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationResult;

    const-string v1, "INCORRECT_CODE_RETRIES_EXCEEDED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaAuthenticationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->INCORRECT_CODE_RETRIES_EXCEEDED:Lcom/meawallet/mtp/MeaAuthenticationResult;

    .line 10
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationResult;

    const-string v1, "EXPIRED_CODE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/MeaAuthenticationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->EXPIRED_CODE:Lcom/meawallet/mtp/MeaAuthenticationResult;

    .line 11
    new-instance v0, Lcom/meawallet/mtp/MeaAuthenticationResult;

    const-string v1, "EXPIRED_SESSION"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/MeaAuthenticationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->EXPIRED_SESSION:Lcom/meawallet/mtp/MeaAuthenticationResult;

    const/4 v0, 0x5

    .line 5
    new-array v0, v0, [Lcom/meawallet/mtp/MeaAuthenticationResult;

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationResult;->SUCCESS:Lcom/meawallet/mtp/MeaAuthenticationResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationResult;->INCORRECT_CODE:Lcom/meawallet/mtp/MeaAuthenticationResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationResult;->INCORRECT_CODE_RETRIES_EXCEEDED:Lcom/meawallet/mtp/MeaAuthenticationResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationResult;->EXPIRED_CODE:Lcom/meawallet/mtp/MeaAuthenticationResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/MeaAuthenticationResult;->EXPIRED_SESSION:Lcom/meawallet/mtp/MeaAuthenticationResult;

    aput-object v1, v0, v6

    sput-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->$VALUES:[Lcom/meawallet/mtp/MeaAuthenticationResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaAuthenticationResult;
    .locals 1

    .line 5
    const-class v0, Lcom/meawallet/mtp/MeaAuthenticationResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaAuthenticationResult;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaAuthenticationResult;
    .locals 1

    .line 5
    sget-object v0, Lcom/meawallet/mtp/MeaAuthenticationResult;->$VALUES:[Lcom/meawallet/mtp/MeaAuthenticationResult;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaAuthenticationResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaAuthenticationResult;

    return-object v0
.end method
