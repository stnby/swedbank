.class public Lcom/meawallet/mtp/MeaErrorCode;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaErrorCode$a;
    }
.end annotation


# static fields
.field public static final ABI_NOT_SUPPORTED:I = 0x6b

.field public static final ALREADY_REGISTERED:I = 0x1f8

.field public static final ANOTHER_REQUEST_ALREADY_IN_PROCESS:I = 0xd1

.field public static final APPLICATION_NOT_DEFAULT_FOR_CONTACTLESS:I = 0x6e

.field public static final AUTHORIZATION_FAILED:I = 0x262

.field public static final CARDHOLDER_AUTHENTICATION_CANCELLED:I = 0x38c

.field public static final CARDHOLDER_AUTHENTICATION_FAILED:I = 0x38a

.field public static final CARDHOLDER_AUTHENTICATION_NOT_REQUIRED:I = 0x388

.field public static final CARDHOLDER_AUTHENTICATION_NOT_SUPPORTED:I = 0x385

.field public static final CARDHOLDER_VERIFICATION_DEVICE_UNLOCK_NOT_ENABLED_BY_USER:I = 0x386
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_FAILED_WRONG_CVM_TYPE:I = 0x389
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_FINGERPRINT_NOT_SET_UP_BY_USER:I = 0x387
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_KEY_INVALIDATED:I = 0x38d
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_NOT_SUPPORTED:I = 0x388
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_ON_DEVICE_NOT_SUPPORTED:I = 0x385
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_WITH_DEVICE_UNLOCK_FAILED:I = 0x38a
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_WITH_FINGERPRINT_CANCELLED:I = 0x38c
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARDHOLDER_VERIFICATION_WITH_FINGERPRINT_FAILED:I = 0x38b
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARD_ALREADY_DIGITIZED:I = 0x3f5

.field public static final CARD_AUTHENTICATION_CODE_EXPIRED:I = 0x260

.field public static final CARD_AUTHENTICATION_CODE_INCORRECT:I = 0x25e

.field public static final CARD_AUTHENTICATION_RETRIES_EXCEEDED:I = 0x25f

.field public static final CARD_AUTHENTICATION_SESSION_EXPIRED:I = 0x261

.field public static final CARD_DEACTIVATED:I = 0x3f8

.field public static final CARD_DELETION_FAILED:I = 0x3f3

.field public static final CARD_DIGITIZATION_AUTHENTICATION_NOT_COMPLETED:I = 0x3f7

.field public static final CARD_DIGITIZATION_DECLINED:I = 0x25d

.field public static final CARD_ELIGIBILITY_RECEIPT_EXPIRED:I = 0x265

.field public static final CARD_MARKED_FOR_DELETION:I = 0x3ec

.field public static final CARD_NOT_ACTIVATED:I = 0x3ea

.field public static final CARD_NOT_DIGITIZED:I = 0x3f6

.field public static final CARD_NOT_ELIGIBLE:I = 0x25c

.field public static final CARD_NOT_FOUND:I = 0x3f2

.field public static final CARD_NOT_PROVISIONED:I = 0x3e9

.field public static final CARD_NOT_SUPPORT_CONTACTLESS_PAYMENTS:I = 0x3f0

.field public static final CARD_NOT_SUPPORT_REMOTE_PAYMENTS:I = 0x3f1

.field public static final CARD_NO_PAYMENT_TOKENS:I = 0x3ed

.field public static final CARD_PIN_NOT_SET:I = 0x3f4

.field public static final CARD_PROVISION_FAILED:I = 0x3ef

.field public static final CARD_REPLENISH_FAILED:I = 0x3ee
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CARD_SUSPENDED:I = 0x3eb

.field public static final CARD_VELOCITY_CHECK_REACHED:I = 0x267

.field public static final CRYPTO_ERROR:I = 0x12d

.field public static final DATA_PROCESSING_ERROR:I = 0x25a

.field public static final DATA_VALIDATION_ERROR:I = 0x259

.field public static final DEBUGGER_ATTACHED:I = 0x6a

.field public static final DEVICE_IS_LOCKED_WITH_LOCK_SCREEN:I = 0x455

.field public static final DEVICE_NOT_ELIGIBLE:I = 0x25b

.field public static final DEVICE_SCREEN_IS_SWITCHED_OFF:I = 0x38e

.field public static final DEVICE_UNLOCK_KEY_INVALIDATED:I = 0x38d

.field public static final DEVICE_UNLOCK_NOT_ENABLED:I = 0x386

.field public static final FINGERPRINT_CHANGED:I = 0x132

.field public static final FINGERPRINT_NOT_SET:I = 0x387

.field public static final GENERIC_NETWORK_PROBLEM:I = 0xc9

.field public static final GOOGLE_PLAY_SERVICES_NOT_AVAILABLE:I = 0x6f

.field public static final HCE_NOT_AVAILABLE:I = 0x69

.field public static final INCORRECT_INPUT_DATA:I = 0x1f9

.field public static final INCORRECT_MOBILE_PIN:I = 0x263

.field public static final INCORRECT_SERVER_RESPONSE_CODE:I = 0xd0

.field public static final INTERNAL_ERROR:I = 0x1f5

.field public static final INTERNAL_SERVER_ERROR:I = 0x1fc

.field public static final INVALID_OPERATION:I = 0x264

.field public static final NETWORK_TIMEOUT:I = 0xca

.field public static final NFC_NOT_AVAILABLE:I = 0x68

.field public static final NOT_INITIALIZED:I = 0x65

.field public static final NOT_REGISTERED:I = 0x67

.field public static final NO_NETWORK_CONNECTION:I = 0x6d

.field public static final NO_PAYMENT_APPLICATION_INSTANCE_ID:I = 0x1fd

.field public static final OS_VERSION_NOT_SUPPORTED:I = 0x66

.field public static final REMOTE_DATA_ERROR:I = 0x1fb

.field public static final REQUEST_NOT_AUTHORIZED:I = 0xcb
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RNS_ERROR:I = 0x1fa
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROOTED_CONNECTION:I = 0x7df

.field public static final ROOTED_DEBUGGABLE:I = 0x7e1

.field public static final ROOTED_STACKTRACE:I = 0x7e0

.field public static final SSL_HANDSHAKE_FAILED:I = 0xcf

.field public static final STORAGE_CORRUPTED_ATTACK_DETECTED:I = 0x12f

.field public static final STORAGE_DELETION_FAILED:I = 0x131
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final STORAGE_MIGRATION_FAILED:I = 0x130

.field public static final STORAGE_OPERATION_FAILED:I = 0x12e

.field public static final TRANSACTION_ABORTED:I = 0x44d

.field public static final TRANSACTION_AUTHENTICATE_OFFLINE:I = 0x452

.field public static final TRANSACTION_CARD_ERROR:I = 0x44f

.field public static final TRANSACTION_CARD_NOT_SELECTED:I = 0x458

.field public static final TRANSACTION_COMMAND_INCOMPATIBLE:I = 0x45f

.field public static final TRANSACTION_CONDITIONS_NOT_ALLOWED:I = 0x45a

.field public static final TRANSACTION_CONTEXT_NOT_MATCHING:I = 0x459

.field public static final TRANSACTION_DECLINED_BY_CARD:I = 0x454

.field public static final TRANSACTION_DECLINED_BY_TERMINAL:I = 0x453

.field public static final TRANSACTION_DECLINED_DEVICE_IS_LOCKED:I = 0x455
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TRANSACTION_DECLINED_DEVICE_SCREEN_IS_OFF:I = 0x455
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TRANSACTION_DECLINED_LIMIT_EXCEEDED:I = 0x456

.field public static final TRANSACTION_FAILED:I = 0x451

.field public static final TRANSACTION_INSUFFICIENT_POI_AUTHENTICATION:I = 0x45c

.field public static final TRANSACTION_MAGSTRIPE_TERMINAL_V2_ERROR:I = 0x457

.field public static final TRANSACTION_MANAGER_BUSY:I = 0x45d

.field public static final TRANSACTION_MISSING_ICC:I = 0x45e

.field public static final TRANSACTION_TERMINAL_ERROR:I = 0x450

.field public static final TRANSACTION_UNSUPPORTED_TRANSIT:I = 0x45b

.field public static final TRANSACTION_WALLET_CANCEL_REQUEST:I = 0x44e

.field public static final UNKNOWN_HOST:I = 0xcc

.field public static final USER_AUTHENTICATION_ATTEMPTS_EXCEEDED:I = 0x192

.field public static final USER_AUTHENTICATION_FAILED:I = 0x191

.field public static final USER_NO_SESSION_TOKEN:I = 0x193

.field public static final USER_ROLE_NO_PERMISSION:I = 0x194

.field public static final VERSION_ROLLBACK:I = 0x1f7

.field public static final WALLET_PIN_NOT_SET:I = 0x38f

.field public static final WRONG_CVM_TYPE:I = 0x389


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAsArray()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 906
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 908
    invoke-static {}, Lcom/meawallet/mtp/MeaErrorCode$a;->values()[Lcom/meawallet/mtp/MeaErrorCode$a;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 2840
    iget v4, v4, Lcom/meawallet/mtp/MeaErrorCode$a;->bi:I

    .line 909
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static getDescription(I)Ljava/lang/String;
    .locals 0

    .line 868
    invoke-static {p0}, Lcom/meawallet/mtp/MeaErrorCode$a;->a(I)Lcom/meawallet/mtp/MeaErrorCode$a;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1844
    iget-object p0, p0, Lcom/meawallet/mtp/MeaErrorCode$a;->bj:Ljava/lang/String;

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method

.method public static getName(I)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x38b

    if-ne p0, v0, :cond_0

    const-string p0, "CARDHOLDER_AUTHENTICATION_FAILED"

    return-object p0

    .line 889
    :cond_0
    invoke-static {p0}, Lcom/meawallet/mtp/MeaErrorCode$a;->a(I)Lcom/meawallet/mtp/MeaErrorCode$a;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 893
    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaErrorCode$a;->name()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method public static isErrorCode(I)Z
    .locals 0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method
