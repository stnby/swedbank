.class final Lcom/meawallet/mtp/dc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;


# instance fields
.field private a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

.field private b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/meawallet/mtp/dc;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    .line 17
    iput-object p2, p0, Lcom/meawallet/mtp/dc;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    return-void
.end method


# virtual methods
.method public final getEncryptedCurrentPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/meawallet/mtp/dc;->a:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    return-object v0
.end method

.method public final getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/meawallet/mtp/dc;->b:Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    return-object v0
.end method

.method public final onKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)V
    .locals 1

    .line 34
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Unsupported operation"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
