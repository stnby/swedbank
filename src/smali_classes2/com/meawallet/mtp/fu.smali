.class Lcom/meawallet/mtp/fu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/lang/String; = "fu"


# instance fields
.field final a:Lcom/meawallet/mtp/ci;

.field final b:Lcom/meawallet/mtp/dq;

.field final c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

.field d:Lcom/google/gson/Gson;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;Lcom/google/gson/Gson;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/meawallet/mtp/fu;->a:Lcom/meawallet/mtp/ci;

    .line 34
    iput-object p2, p0, Lcom/meawallet/mtp/fu;->b:Lcom/meawallet/mtp/dq;

    .line 35
    iput-object p3, p0, Lcom/meawallet/mtp/fu;->c:Lcom/mastercard/mpsdk/componentinterface/remotemanagement/MpaManagementHelper;

    .line 36
    iput-object p4, p0, Lcom/meawallet/mtp/fu;->d:Lcom/google/gson/Gson;

    return-void
.end method

.method static a(Lcom/meawallet/mtp/CvmMethod;)Ljava/lang/String;
    .locals 3

    const-string v0, "cvmMethod = %s"

    const/4 v1, 0x1

    .line 224
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Lcom/meawallet/mtp/MeaEligibilityReceipt;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    const-string p0, "meaEligibilityReceipt = null"

    return-object p0

    :cond_0
    const-string v0, "meaEligibilityReceipt = %s"

    const/4 v1, 0x1

    .line 236
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->toString()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    const-string p0, "meaInitializeDigitizationParameters = null"

    return-object p0

    :cond_0
    const-string v0, "meaInitializeDigitizationParameters = %s"

    const/4 v1, 0x1

    .line 220
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->toString()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Lcom/meawallet/mtp/ap;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    const-string p0, "deviceInfo = null"

    return-object p0

    :cond_0
    const-string v0, "deviceInfo = %s"

    const/4 v1, 0x1

    .line 230
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/meawallet/mtp/ap;->toString()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/v;)Z
    .locals 4

    .line 1022
    iget-object v0, p1, Lcom/meawallet/mtp/v;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 255
    new-array v2, v0, [Ljava/lang/Object;

    .line 2022
    iget-object v3, p1, Lcom/meawallet/mtp/v;->a:Ljava/lang/String;

    aput-object v3, v2, v1

    .line 257
    :try_start_0
    invoke-interface {p0}, Lcom/meawallet/mtp/ci;->h()Lcom/meawallet/mtp/cc;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 2035
    iget-object v2, p0, Lcom/meawallet/mtp/cc;->a:Lcom/meawallet/mtp/ce;

    if-eqz v2, :cond_0

    .line 3035
    iget-object v2, p0, Lcom/meawallet/mtp/cc;->a:Lcom/meawallet/mtp/ce;

    .line 4019
    iget-object v2, v2, Lcom/meawallet/mtp/ce;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 261
    new-array v0, v0, [Ljava/lang/Object;

    .line 4022
    iget-object v2, p1, Lcom/meawallet/mtp/v;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 4035
    iget-object p0, p0, Lcom/meawallet/mtp/cc;->a:Lcom/meawallet/mtp/ce;

    .line 5019
    iget-object p0, p0, Lcom/meawallet/mtp/ce;->a:Ljava/lang/String;

    .line 5022
    iget-object p1, p1, Lcom/meawallet/mtp/v;->a:Ljava/lang/String;

    .line 263
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    .line 267
    sget-object p1, Lcom/meawallet/mtp/fu;->e:Ljava/lang/String;

    invoke-static {p1, p0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return v1
.end method
