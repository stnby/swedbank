.class final Lcom/meawallet/mtp/dk$1;
.super Lcom/meawallet/mtp/fk;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/dk;->a(Ljava/lang/String;Lcom/meawallet/mtp/do;Lcom/meawallet/mtp/en;)Lcom/meawallet/mtp/fk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/meawallet/mtp/en;

.field final synthetic c:Lcom/meawallet/mtp/dk;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/dk;Lcom/meawallet/mtp/do;Ljava/lang/String;Lcom/meawallet/mtp/en;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/meawallet/mtp/dk$1;->c:Lcom/meawallet/mtp/dk;

    iput-object p3, p0, Lcom/meawallet/mtp/dk$1;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/meawallet/mtp/dk$1;->b:Lcom/meawallet/mtp/en;

    invoke-direct {p0, p2}, Lcom/meawallet/mtp/fk;-><init>(Lcom/meawallet/mtp/do;)V

    return-void
.end method


# virtual methods
.method public final onReplenishCompleted(Ljava/lang/String;I)Z
    .locals 4

    .line 99
    iget-object v0, p0, Lcom/meawallet/mtp/dk$1;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 100
    invoke-static {}, Lcom/meawallet/mtp/dk;->b()Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/meawallet/mtp/dk$1;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v2, 0x1

    .line 101
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 103
    iget-object v0, p0, Lcom/meawallet/mtp/dk$1;->b:Lcom/meawallet/mtp/en;

    invoke-interface {v0, p1, p2}, Lcom/meawallet/mtp/en;->a(Ljava/lang/String;I)V

    .line 105
    invoke-virtual {p0}, Lcom/meawallet/mtp/dk$1;->b()V

    :cond_0
    return v1
.end method

.method public final onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)Z
    .locals 7

    .line 114
    invoke-static {}, Lcom/meawallet/mtp/dk;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onReplenishFailed() invoked in selectForContactlessPayments() failed. %s"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "null"

    if-eqz p4, :cond_0

    .line 1022
    invoke-virtual {p4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_0
    const-string p4, "errorCode = %s, errorMessage = %s, exception = %s"

    const/4 v5, 0x3

    .line 1025
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    aput-object p3, v5, v2

    const/4 v2, 0x2

    aput-object v4, v5, v2

    invoke-static {p4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    aput-object p4, v3, v6

    const/16 p4, 0x1f5

    .line 114
    invoke-static {v0, p4, v1, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 117
    iget-object p4, p0, Lcom/meawallet/mtp/dk$1;->a:Ljava/lang/String;

    invoke-virtual {p1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 118
    iget-object p1, p0, Lcom/meawallet/mtp/dk$1;->b:Lcom/meawallet/mtp/en;

    invoke-static {p2, p3}, Lcom/meawallet/mtp/dl;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaError;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/meawallet/mtp/en;->a(Lcom/meawallet/mtp/MeaError;)V

    .line 120
    invoke-virtual {p0}, Lcom/meawallet/mtp/dk$1;->b()V

    :cond_1
    return v6
.end method
