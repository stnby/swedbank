.class Lcom/meawallet/mtp/dm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/dn;


# static fields
.field static final a:Ljava/lang/String; = "dm"


# instance fields
.field b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

.field c:Lcom/meawallet/mtp/ci;

.field d:Lcom/meawallet/mtp/k;

.field e:Lcom/meawallet/mtp/do;

.field f:Lcom/meawallet/mtp/cv;

.field g:Lcom/meawallet/mtp/x;

.field private final h:Landroid/app/Application;

.field private i:Lcom/meawallet/mtp/ai;

.field private j:Lcom/meawallet/mtp/dp;

.field private k:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

.field private l:Lcom/meawallet/mtp/l;

.field private m:Lcom/meawallet/mtp/fr;

.field private final n:Lcom/meawallet/mtp/m;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/app/Application;Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/ai;Lcom/meawallet/mtp/m;Lcom/meawallet/mtp/l;Lcom/meawallet/mtp/fr;Lcom/meawallet/mtp/CdCvmType;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/meawallet/mtp/dm;->h:Landroid/app/Application;

    .line 98
    iput-object p2, p0, Lcom/meawallet/mtp/dm;->c:Lcom/meawallet/mtp/ci;

    .line 99
    iput-object p3, p0, Lcom/meawallet/mtp/dm;->i:Lcom/meawallet/mtp/ai;

    .line 101
    invoke-virtual {p1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p7, p1}, Lcom/meawallet/mtp/n;->a(Lcom/meawallet/mtp/CdCvmType;Landroid/content/Context;)Lcom/meawallet/mtp/cv;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 108
    iput-object p1, p0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    .line 109
    iput-object p4, p0, Lcom/meawallet/mtp/dm;->n:Lcom/meawallet/mtp/m;

    .line 110
    iput-object p5, p0, Lcom/meawallet/mtp/dm;->l:Lcom/meawallet/mtp/l;

    .line 111
    iput-object p6, p0, Lcom/meawallet/mtp/dm;->m:Lcom/meawallet/mtp/fr;

    .line 1122
    sget-object p1, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->INSTANCE:Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    iget-object p2, p0, Lcom/meawallet/mtp/dm;->h:Landroid/app/Application;

    .line 1124
    invoke-virtual {p1, p2}, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->forApplication(Landroid/app/Application;)Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;

    move-result-object p1

    .line 1125
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibraryInitializer;->initialize()Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibrary;

    move-result-object p1

    .line 1127
    invoke-interface {p1}, Lcom/mastercard/mpsdk/implementation/MasterCardMobilePaymentLibrary;->getMcbpInitializer()Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    move-result-object p1

    .line 1129
    new-instance p2, Lcom/meawallet/mtp/am;

    sget-object p3, Lcom/meawallet/mtp/BuildConfig;->MPSDK_LOGGING_INFO:Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    invoke-direct {p2, p3}, Lcom/meawallet/mtp/am;-><init>(Z)V

    .line 1131
    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalMcbpLogger(Lcom/mastercard/mpsdk/componentinterface/McbpLogger;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1210
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    const-string p3, "https://cmsd.meawallet.com"

    .line 1211
    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1213
    new-instance p3, Lcom/meawallet/mtp/eb;

    iget-object p4, p0, Lcom/meawallet/mtp/dm;->i:Lcom/meawallet/mtp/ai;

    const/4 p5, 0x0

    const-string p6, "https://cmsd.meawallet.com"

    invoke-direct {p3, p4, p5, p6}, Lcom/meawallet/mtp/eb;-><init>(Lcom/meawallet/mtp/ai;ILjava/lang/String;)V

    .line 1215
    new-instance p4, Lcom/meawallet/mtp/ha;

    invoke-direct {p4}, Lcom/meawallet/mtp/ha;-><init>()V

    .line 1217
    new-instance p5, Lcom/meawallet/mtp/db;

    invoke-direct {p5, p2, p3, p4}, Lcom/meawallet/mtp/db;-><init>(Ljava/util/List;Ljavax/net/ssl/X509TrustManager;Lcom/meawallet/mtp/bf;)V

    .line 1133
    invoke-interface {p1, p5}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withHttpManager(Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1135
    new-instance p2, Lcom/meawallet/mtp/k;

    invoke-direct {p2}, Lcom/meawallet/mtp/k;-><init>()V

    iput-object p2, p0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    .line 1137
    iget-object p2, p0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withActiveCardProvider(Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1139
    iget-object p2, p0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withCdCvmStatusProvider(Lcom/mastercard/mpsdk/interfaces/CdCvmStatusProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1141
    new-instance p2, Lcom/meawallet/mtp/cx;

    invoke-direct {p2}, Lcom/meawallet/mtp/cx;-><init>()V

    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalCredentialsReplenishmentPolicy(Lcom/mastercard/mpsdk/componentinterface/CredentialsReplenishmentPolicy;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1142
    iget-object p2, p0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    invoke-virtual {p2}, Lcom/meawallet/mtp/cv;->b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalPinDataProviderForKeyRollover(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1144
    new-instance p2, Lcom/meawallet/mtp/df;

    iget-object p3, p0, Lcom/meawallet/mtp/dm;->h:Landroid/app/Application;

    iget-object p4, p0, Lcom/meawallet/mtp/dm;->m:Lcom/meawallet/mtp/fr;

    invoke-direct {p2, p3, p4}, Lcom/meawallet/mtp/df;-><init>(Landroid/content/Context;Lcom/meawallet/mtp/fr;)V

    .line 1146
    invoke-interface {p1, p2}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalAdviceManager(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletAdviceManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1149
    new-instance p3, Lcom/meawallet/mtp/do;

    invoke-direct {p3}, Lcom/meawallet/mtp/do;-><init>()V

    iput-object p3, p0, Lcom/meawallet/mtp/dm;->e:Lcom/meawallet/mtp/do;

    .line 1150
    iget-object p3, p0, Lcom/meawallet/mtp/dm;->e:Lcom/meawallet/mtp/do;

    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withCardManagerEventListener(Lcom/mastercard/mpsdk/interfaces/CardManagerEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1152
    new-instance p3, Lcom/meawallet/mtp/x;

    invoke-direct {p3}, Lcom/meawallet/mtp/x;-><init>()V

    iput-object p3, p0, Lcom/meawallet/mtp/dm;->g:Lcom/meawallet/mtp/x;

    .line 1153
    iget-object p3, p0, Lcom/meawallet/mtp/dm;->g:Lcom/meawallet/mtp/x;

    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalRemoteCommunicationManager(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1155
    new-instance p3, Lcom/meawallet/mtp/dv;

    iget-object p4, p0, Lcom/meawallet/mtp/dm;->l:Lcom/meawallet/mtp/l;

    invoke-direct {p3, p0, p4}, Lcom/meawallet/mtp/dv;-><init>(Lcom/meawallet/mtp/dn;Lcom/meawallet/mtp/l;)V

    .line 1157
    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalRemoteCommunicationEventListener(Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1159
    new-instance p3, Lcom/meawallet/mtp/dp;

    iget-object p4, p0, Lcom/meawallet/mtp/dm;->h:Landroid/app/Application;

    iget-object p5, p0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    iget-object p6, p0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    invoke-direct {p3, p4, p2, p5, p6}, Lcom/meawallet/mtp/dp;-><init>(Landroid/content/Context;Lcom/meawallet/mtp/de;Lcom/meawallet/mtp/cv;Lcom/meawallet/mtp/k;)V

    iput-object p3, p0, Lcom/meawallet/mtp/dm;->j:Lcom/meawallet/mtp/dp;

    .line 1165
    iget-object p3, p0, Lcom/meawallet/mtp/dm;->j:Lcom/meawallet/mtp/dp;

    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withTransactionEventListener(Lcom/mastercard/mpsdk/interfaces/TransactionEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1167
    new-instance p3, Lcom/meawallet/mtp/dg;

    invoke-direct {p3}, Lcom/meawallet/mtp/dg;-><init>()V

    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withWalletConsentManager(Lcom/mastercard/mchipengine/walletinterface/walletcallbacks/WalletConsentManager;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1169
    invoke-static {}, Lcom/meawallet/mtp/n;->b()Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;

    move-result-object p3

    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalDefaultDataForMcbpV1ProfileMapping(Lcom/mastercard/mpsdk/interfaces/McbpV1ToMcbpV2ProfileMappingDefaults;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1221
    new-instance p3, Lcom/meawallet/mtp/dm$1;

    invoke-direct {p3, p0}, Lcom/meawallet/mtp/dm$1;-><init>(Lcom/meawallet/mtp/dm;)V

    .line 1171
    iput-object p3, p0, Lcom/meawallet/mtp/dm;->k:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    .line 1172
    iget-object p3, p0, Lcom/meawallet/mtp/dm;->k:Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;

    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withKeyRolloverEventListener(Lcom/mastercard/mpsdk/interfaces/KeyRolloverEventListener;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1247
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 1248
    new-instance p4, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string p5, "9F7E"

    invoke-static {p5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p5

    invoke-virtual {p5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p5

    const/4 p6, 0x1

    invoke-direct {p4, p5, p6}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {p3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1249
    new-instance p4, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string p5, "9F1D"

    invoke-static {p5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p5

    invoke-virtual {p5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p5

    const/16 p6, 0x8

    invoke-direct {p4, p5, p6}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {p3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1174
    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalPdolItems(Ljava/util/List;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 2240
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 2241
    new-instance p4, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;

    const-string p5, "9F33"

    invoke-static {p5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mchipengine/utils/MChipByteArray;

    move-result-object p5

    invoke-virtual {p5}, Lcom/mastercard/mchipengine/utils/MChipByteArray;->getBytes()[B

    move-result-object p5

    const/4 p6, 0x3

    invoke-direct {p4, p5, p6}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/DolEntry;-><init>([BB)V

    invoke-interface {p3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1175
    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalUdolItems(Ljava/util/List;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1177
    sget-object p3, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {p3, p7}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 1178
    invoke-interface {p1}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalCardLevelPin()Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1181
    :cond_0
    iget-object p3, p0, Lcom/meawallet/mtp/dm;->h:Landroid/app/Application;

    .line 3236
    new-instance p4, Lcom/meawallet/mtp/aj;

    invoke-direct {p4}, Lcom/meawallet/mtp/aj;-><init>()V

    .line 4023
    invoke-static {p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a(Landroid/content/Context;)Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    move-result-object p3

    .line 1181
    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withCryptoEngine(Lcom/mastercard/mpsdk/componentinterface/crypto/McbpCryptoServices;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1183
    new-instance p3, Lcom/meawallet/mtp/aj;

    invoke-direct {p3}, Lcom/meawallet/mtp/aj;-><init>()V

    iget-object p3, p0, Lcom/meawallet/mtp/dm;->h:Landroid/app/Application;

    .line 4027
    invoke-static {p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->a(Landroid/content/Context;)Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    move-result-object p3

    .line 1184
    new-instance p4, Lcom/meawallet/mtp/dh;

    iget-object p5, p0, Lcom/meawallet/mtp/dm;->c:Lcom/meawallet/mtp/ci;

    invoke-direct {p4, p5, p3}, Lcom/meawallet/mtp/dh;-><init>(Lcom/meawallet/mtp/ci;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;)V

    .line 1185
    invoke-interface {p1, p4}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->withWalletIdentificationDataProvider(Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1187
    iget-object p3, p0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    invoke-virtual {p3}, Lcom/meawallet/mtp/cv;->b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    move-result-object p3

    invoke-interface {p1, p3}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->usingOptionalPinDataProviderForTransactions(Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;)Lcom/mastercard/mpsdk/interfaces/McbpInitializer;

    .line 1190
    invoke-interface {p1}, Lcom/mastercard/mpsdk/interfaces/McbpInitializer;->initialize()Lcom/mastercard/mpsdk/interfaces/Mcbp;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/dm;->b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

    .line 1192
    iget-object p1, p0, Lcom/meawallet/mtp/dm;->b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/interfaces/Mcbp;->getCardManager()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object p1

    .line 4044
    iput-object p1, p2, Lcom/meawallet/mtp/df;->b:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    .line 1193
    iget-object p1, p0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    .line 4048
    iput-object p1, p2, Lcom/meawallet/mtp/df;->a:Lcom/meawallet/mtp/k;

    .line 1195
    iget-object p1, p0, Lcom/meawallet/mtp/dm;->b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

    invoke-interface {p1}, Lcom/mastercard/mpsdk/interfaces/Mcbp;->getApduProcessor()Lcom/mastercard/mpsdk/interfaces/ApduProcessor;

    move-result-object p1

    iget-object p2, p0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    iget-object p3, p0, Lcom/meawallet/mtp/dm;->b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

    invoke-interface {p3}, Lcom/mastercard/mpsdk/interfaces/Mcbp;->getCardManager()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/MeaHceService;->a(Lcom/mastercard/mpsdk/interfaces/ApduProcessor;Lcom/meawallet/mtp/k;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V

    return-void

    .line 105
    :cond_1
    new-instance p1, Lcom/meawallet/mtp/bm;

    const-string p2, "MeaCdCvmStatusProvider is null"

    invoke-direct {p1, p2}, Lcom/meawallet/mtp/bm;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()Lcom/mastercard/mpsdk/componentinterface/CardManager;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/meawallet/mtp/dm;->b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/Mcbp;->getCardManager()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/meawallet/mtp/ci;
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/meawallet/mtp/dm;->c:Lcom/meawallet/mtp/ci;

    return-object v0
.end method

.method public final c()Lcom/meawallet/mtp/m;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/meawallet/mtp/dm;->n:Lcom/meawallet/mtp/m;

    return-object v0
.end method

.method public final d()Lcom/meawallet/mtp/cv;
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/meawallet/mtp/dm;->f:Lcom/meawallet/mtp/cv;

    return-object v0
.end method

.method public final e()Lcom/meawallet/mtp/k;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    return-object v0
.end method

.method public final f()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/meawallet/mtp/dm;->b:Lcom/mastercard/mpsdk/interfaces/Mcbp;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/Mcbp;->getWalletSecurityServices()Lcom/mastercard/mpsdk/interfaces/WalletSecurityServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/mastercard/mpsdk/interfaces/WalletSecurityServices;->getWalletCryptoApi()Lcom/mastercard/mpsdk/componentinterface/crypto/WalletDataCrypto;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/app/Application;
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/meawallet/mtp/dm;->h:Landroid/app/Application;

    return-object v0
.end method
