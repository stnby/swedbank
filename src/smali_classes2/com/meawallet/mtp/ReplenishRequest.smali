.class public Lcom/meawallet/mtp/ReplenishRequest;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private requestId:Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lflexjson/h;
        a = "requestId"
    .end annotation
.end field

.field private tokenUniqueReference:Ljava/lang/String;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lflexjson/h;
        a = "tokenUniqueReference"
    .end annotation
.end field

.field private transactionCredentialsStatus:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation runtime Lflexjson/h;
        a = "transactionCredentialsStatus"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishRequest;->requestId:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/meawallet/mtp/ReplenishRequest;->tokenUniqueReference:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/meawallet/mtp/ReplenishRequest;->transactionCredentialsStatus:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    return-void
.end method


# virtual methods
.method public buildAsJson(Lcom/google/gson/Gson;)Ljava/lang/String;
    .locals 4

    .line 63
    new-instance p1, Lflexjson/k;

    invoke-direct {p1}, Lflexjson/k;-><init>()V

    const-string v0, "*.class"

    .line 64
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflexjson/k;->a([Ljava/lang/String;)Lflexjson/k;

    .line 66
    new-instance v0, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;

    invoke-direct {v0}, Lcom/mastercard/mpsdk/utils/json/SuppressNullTransformer;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Lflexjson/k;->a(Lflexjson/c/q;[Ljava/lang/Class;)Lflexjson/k;

    .line 67
    invoke-virtual {p1, p0}, Lflexjson/k;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishRequest;->requestId:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenUniqueReference()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishRequest;->tokenUniqueReference:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionCredentialsStatus()[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishRequest;->transactionCredentialsStatus:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    return-object v0
.end method

.method public setRequestId(Ljava/lang/String;)Lcom/meawallet/mtp/ReplenishRequest;
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishRequest;->requestId:Ljava/lang/String;

    return-object p0
.end method

.method public setTokenUniqueReference(Ljava/lang/String;)Lcom/meawallet/mtp/ReplenishRequest;
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishRequest;->tokenUniqueReference:Ljava/lang/String;

    return-object p0
.end method

.method public setTransactionCredentialsStatus([Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)Lcom/meawallet/mtp/ReplenishRequest;
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishRequest;->transactionCredentialsStatus:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method
