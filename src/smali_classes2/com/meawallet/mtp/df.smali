.class final Lcom/meawallet/mtp/df;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/de;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/df$a;
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Lcom/meawallet/mtp/k;

.field b:Lcom/mastercard/mpsdk/componentinterface/CardManager;

.field private final d:Landroid/content/Context;

.field private e:Lcom/meawallet/mtp/fr;

.field private f:Lcom/meawallet/mtp/ds;

.field private g:Lcom/meawallet/mtp/df$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    const-class v0, Lcom/meawallet/mtp/de;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/df;->c:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/meawallet/mtp/fr;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/meawallet/mtp/df;->d:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/meawallet/mtp/df;->e:Lcom/meawallet/mtp/fr;

    if-eqz p2, :cond_0

    .line 39
    new-instance p1, Lcom/meawallet/mtp/ds;

    .line 1022
    iget-object p2, p2, Lcom/meawallet/mtp/fr;->a:Lcom/meawallet/mtp/ci;

    .line 39
    invoke-direct {p1, p2}, Lcom/meawallet/mtp/ds;-><init>(Lcom/meawallet/mtp/ci;)V

    iput-object p1, p0, Lcom/meawallet/mtp/df;->f:Lcom/meawallet/mtp/ds;

    :cond_0
    return-void
.end method

.method private static a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;)Z
    .locals 2

    .line 257
    invoke-static {}, Lcom/meawallet/mtp/n;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v0

    .line 259
    sget-object v1, Lcom/meawallet/mtp/df$1;->b:[I

    invoke-virtual {v0}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 266
    :pswitch_0
    invoke-interface {p0}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getTransactionRange()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    move-result-object p0

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    if-ne p0, v0, :cond_0

    return v1

    :pswitch_1
    return v1

    :cond_0
    :goto_0
    const/4 p0, 0x0

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()Lcom/meawallet/mtp/cq;
    .locals 4

    .line 245
    iget-object v0, p0, Lcom/meawallet/mtp/df;->g:Lcom/meawallet/mtp/df$a;

    if-nez v0, :cond_0

    .line 246
    sget-object v0, Lcom/meawallet/mtp/df;->c:Ljava/lang/String;

    const/16 v1, 0x1f5

    const-string v2, "MeaWallet final assessment is null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/df;->g:Lcom/meawallet/mtp/df$a;

    .line 15232
    iget-object v0, v0, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    return-object v0
.end method

.method public final getFinalAssessment(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TerminalInformation;)Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;
    .locals 11

    .line 1083
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getAdvice()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    move-result-object p3

    .line 1084
    invoke-interface {p1}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/AdviceAndReasons;->getReasons()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p3, :cond_0

    .line 1087
    sget-object p1, Lcom/meawallet/mtp/df;->c:Ljava/lang/String;

    const/16 p2, 0x1f5

    const-string p3, "MP SDK advice is null"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2, p3, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1089
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    goto/16 :goto_9

    .line 1092
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v1

    const/4 v2, 0x0

    .line 1095
    iget-object v3, p0, Lcom/meawallet/mtp/df;->a:Lcom/meawallet/mtp/k;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/meawallet/mtp/df;->b:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    if-eqz v3, :cond_1

    .line 1096
    iget-object v2, p0, Lcom/meawallet/mtp/df;->a:Lcom/meawallet/mtp/k;

    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    iget-object v4, p0, Lcom/meawallet/mtp/df;->b:Lcom/mastercard/mpsdk/componentinterface/CardManager;

    invoke-virtual {v2, v3, v4}, Lcom/meawallet/mtp/k;->getActiveCard(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v2

    :cond_1
    if-eqz v2, :cond_2

    .line 1303
    sget-object v3, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->CONTACTLESS:Lcom/mastercard/mpsdk/componentinterface/PaymentContext;

    invoke-interface {v2, v3}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCdCvmModel(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;)Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;

    move-result-object v3

    .line 1305
    sget-object v4, Lcom/meawallet/mtp/df$1;->c:[I

    invoke-virtual {v3}, Lcom/mastercard/mpsdk/componentinterface/CardCvmModel;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1311
    :pswitch_0
    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CDCVM_ALWAYS:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    goto :goto_1

    .line 1309
    :pswitch_1
    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    goto :goto_1

    .line 1307
    :pswitch_2
    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    goto :goto_1

    .line 1318
    :cond_2
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/n;->c()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    move-result-object v3

    .line 2297
    :goto_1
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v4

    const-string v5, "ALLOW_PAYMENTS_WHEN_LOCKED"

    invoke-virtual {v4, v5}, Lcom/meawallet/mtp/dx;->e(Ljava/lang/String;)Z

    move-result v4

    .line 1102
    iget-object v5, p0, Lcom/meawallet/mtp/df;->d:Landroid/content/Context;

    invoke-static {v5}, Lcom/meawallet/mtp/ar;->f(Landroid/content/Context;)Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v4, :cond_5

    .line 3280
    invoke-static {p2}, Lcom/meawallet/mtp/df;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;)Z

    move-result v7

    if-eqz v7, :cond_3

    :goto_2
    const/4 v7, 0x1

    goto :goto_3

    .line 3285
    :cond_3
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v7

    .line 3287
    sget-object v8, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    if-ne v7, v8, :cond_4

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    :goto_3
    if-eqz v7, :cond_5

    const/4 v7, 0x1

    goto :goto_4

    :cond_5
    const/4 v7, 0x0

    .line 1104
    :goto_4
    invoke-static {}, Lcom/meawallet/mtp/fp;->f()Z

    move-result v8

    const/4 v9, 0x3

    .line 1106
    new-array v10, v9, [Ljava/lang/Object;

    aput-object v1, v10, v0

    aput-object v3, v10, v6

    .line 1107
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v10, v4

    .line 1108
    new-array v1, v4, [Ljava/lang/Object;

    .line 1109
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v1, v0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v1, v6

    if-eqz v5, :cond_7

    if-nez v7, :cond_7

    if-nez v8, :cond_7

    .line 1114
    sget-object p1, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    if-ne v3, p1, :cond_6

    .line 1115
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    sget-object p2, Lcom/meawallet/mtp/cq;->d:Lcom/meawallet/mtp/cq;

    .line 4222
    iput-object p2, p1, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto/16 :goto_9

    .line 1117
    :cond_6
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    sget-object p2, Lcom/meawallet/mtp/cq;->d:Lcom/meawallet/mtp/cq;

    .line 5222
    iput-object p2, p1, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto/16 :goto_9

    .line 1120
    :cond_7
    sget-object v1, Lcom/meawallet/mtp/BuildConfig;->CURRENCY_TRANSACTION_LIMITS:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1124
    iget-object v5, p0, Lcom/meawallet/mtp/df;->e:Lcom/meawallet/mtp/fr;

    .line 6027
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getCurrencyCode()[B

    move-result-object v7

    invoke-static {v7}, Lcom/meawallet/mtp/a;->a([B)Ljava/util/Currency;

    move-result-object v7

    .line 6028
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getAuthorizedAmount()[B

    move-result-object v8

    invoke-static {v8}, Lcom/meawallet/mtp/a;->b([B)I

    move-result v8

    if-eqz v7, :cond_8

    .line 6033
    invoke-virtual {v5, v7, v8}, Lcom/meawallet/mtp/fr;->a(Ljava/util/Currency;I)Z

    move-result v5

    goto :goto_5

    :cond_8
    const/4 v5, 0x0

    .line 6036
    :goto_5
    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v0

    goto :goto_6

    :cond_9
    const/4 v5, 0x1

    .line 1127
    :goto_6
    new-array v4, v4, [Ljava/lang/Object;

    .line 1128
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v6

    if-eqz v1, :cond_a

    if-nez v5, :cond_a

    .line 1133
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    sget-object p2, Lcom/meawallet/mtp/cq;->f:Lcom/meawallet/mtp/cq;

    .line 6222
    iput-object p2, p1, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto/16 :goto_9

    .line 1136
    :cond_a
    sget-object v0, Lcom/meawallet/mtp/BuildConfig;->LOST_OR_STOLEN:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1137
    invoke-interface {p2}, Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;->getTransactionRange()Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    move-result-object v1

    .line 1139
    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->CARD_LIKE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    if-eq v3, v4, :cond_b

    sget-object v4, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;->FLEXIBLE_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/CdCvmModel;

    if-ne v3, v4, :cond_e

    sget-object v3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;->LOW_VALUE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionRange;

    if-ne v1, v3, :cond_e

    :cond_b
    if-eqz v0, :cond_e

    .line 1141
    iget-object v0, p0, Lcom/meawallet/mtp/df;->f:Lcom/meawallet/mtp/ds;

    invoke-virtual {v0}, Lcom/meawallet/mtp/ds;->a()I

    move-result v0

    if-le v0, v9, :cond_c

    .line 1144
    iget-object p1, p0, Lcom/meawallet/mtp/df;->f:Lcom/meawallet/mtp/ds;

    invoke-virtual {p1}, Lcom/meawallet/mtp/ds;->b()V

    .line 1146
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    sget-object p2, Lcom/meawallet/mtp/cq;->a:Lcom/meawallet/mtp/cq;

    .line 7222
    iput-object p2, p1, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto/16 :goto_9

    :cond_c
    const-string v0, ""

    if-eqz v2, :cond_d

    .line 1152
    invoke-interface {v2}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v0

    .line 1155
    :cond_d
    iget-object v1, p0, Lcom/meawallet/mtp/df;->f:Lcom/meawallet/mtp/ds;

    .line 1156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v2}, Lcom/meawallet/mtp/ds;->a(Ljava/lang/String;Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;Ljava/lang/String;)V

    .line 1159
    :cond_e
    sget-object v0, Lcom/meawallet/mtp/df$1;->a:[I

    invoke-virtual {p3}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->ordinal()I

    move-result p3

    aget p3, v0, p3

    packed-switch p3, :pswitch_data_1

    .line 1209
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    goto/16 :goto_9

    .line 1191
    :pswitch_3
    new-instance p2, Lcom/meawallet/mtp/df$a;

    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->DECLINE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p2, p0, p3}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    .line 1193
    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CONTEXT_NOT_MATCHING:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_f

    .line 1194
    sget-object p1, Lcom/meawallet/mtp/cq;->g:Lcom/meawallet/mtp/cq;

    .line 12222
    iput-object p1, p2, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto :goto_7

    .line 1196
    :cond_f
    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->TRANSACTION_CONDITIONS_NOT_ALLOWED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_10

    .line 1197
    sget-object p1, Lcom/meawallet/mtp/cq;->j:Lcom/meawallet/mtp/cq;

    .line 13222
    iput-object p1, p2, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto :goto_7

    .line 1199
    :cond_10
    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->UNSUPPORTED_TRANSIT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_11

    .line 1200
    sget-object p1, Lcom/meawallet/mtp/cq;->h:Lcom/meawallet/mtp/cq;

    .line 14222
    iput-object p1, p2, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto :goto_7

    .line 1202
    :cond_11
    sget-object p3, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_POI_AUTHENTICATION:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_12

    .line 1203
    sget-object p1, Lcom/meawallet/mtp/cq;->i:Lcom/meawallet/mtp/cq;

    .line 15222
    iput-object p1, p2, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    :cond_12
    :goto_7
    move-object p1, p2

    goto :goto_9

    .line 1166
    :pswitch_4
    new-instance p3, Lcom/meawallet/mtp/df$a;

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->TRY_AGAIN:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p3, p0, v0}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    .line 1168
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_AVAILABLE:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1170
    sget-object p1, Lcom/meawallet/mtp/cq;->c:Lcom/meawallet/mtp/cq;

    .line 8222
    iput-object p1, p3, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto :goto_8

    .line 1171
    :cond_13
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CONSENT:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1173
    sget-object p1, Lcom/meawallet/mtp/cq;->b:Lcom/meawallet/mtp/cq;

    .line 9222
    iput-object p1, p3, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto :goto_8

    .line 1174
    :cond_14
    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->MISSING_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->INSUFFICIENT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    .line 1175
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    sget-object v0, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;->CREDENTIALS_NOT_ACCESSIBLE_WITHOUT_CDCVM:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Reason;

    .line 1176
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 1178
    :cond_15
    invoke-static {p2}, Lcom/meawallet/mtp/df;->a(Lcom/mastercard/mchipengine/walletinterface/walletdatatypes/TransactionInformation;)Z

    move-result p1

    if-eqz p1, :cond_16

    .line 1180
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    goto :goto_9

    .line 1181
    :cond_16
    invoke-static {}, Lcom/meawallet/mtp/n;->h()Z

    move-result p1

    if-eqz p1, :cond_17

    invoke-static {}, Lcom/meawallet/mtp/br;->b()Z

    move-result p1

    if-eqz p1, :cond_17

    .line 1182
    sget-object p1, Lcom/meawallet/mtp/cq;->e:Lcom/meawallet/mtp/cq;

    .line 10222
    iput-object p1, p3, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    goto :goto_8

    .line 1184
    :cond_17
    sget-object p1, Lcom/meawallet/mtp/cq;->a:Lcom/meawallet/mtp/cq;

    .line 11222
    iput-object p1, p3, Lcom/meawallet/mtp/df$a;->b:Lcom/meawallet/mtp/cq;

    :cond_18
    :goto_8
    move-object p1, p3

    goto :goto_9

    .line 1162
    :pswitch_5
    new-instance p1, Lcom/meawallet/mtp/df$a;

    sget-object p2, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;->PROCEED:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    invoke-direct {p1, p0, p2}, Lcom/meawallet/mtp/df$a;-><init>(Lcom/meawallet/mtp/df;Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;)V

    .line 70
    :goto_9
    iput-object p1, p0, Lcom/meawallet/mtp/df;->g:Lcom/meawallet/mtp/df$a;

    .line 76
    iget-object p1, p0, Lcom/meawallet/mtp/df;->g:Lcom/meawallet/mtp/df$a;

    .line 15228
    iget-object p1, p1, Lcom/meawallet/mtp/df$a;->a:Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/Advice;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
