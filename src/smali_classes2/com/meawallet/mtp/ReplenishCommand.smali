.class final Lcom/meawallet/mtp/ReplenishCommand;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/g<",
        "Lcom/meawallet/mtp/eo;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:Ljava/lang/String;

.field private final k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 10

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p10

    .line 40
    invoke-direct/range {v0 .. v8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object/from16 v0, p8

    .line 49
    iput-object v0, v9, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    move-object/from16 v0, p9

    .line 50
    iput-object v0, v9, Lcom/meawallet/mtp/ReplenishCommand;->k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    return-void
.end method

.method private a([Lcom/meawallet/mtp/fq;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Ljava/util/ArrayList;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/meawallet/mtp/fq;",
            "Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;"
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    .line 151
    new-instance v13, Ljava/util/ArrayList;

    array-length v0, v11

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 4322
    new-instance v14, Lcom/meawallet/mtp/ReplenishCommand$2;

    invoke-direct {v14, v10, v12}, Lcom/meawallet/mtp/ReplenishCommand$2;-><init>(Lcom/meawallet/mtp/ReplenishCommand;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)V

    .line 155
    array-length v15, v11

    const/4 v0, 0x0

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v15, :cond_6

    aget-object v0, v11, v9

    .line 5041
    iget-object v1, v0, Lcom/meawallet/mtp/fq;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 6041
    iget-object v1, v0, Lcom/meawallet/mtp/fq;->c:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 162
    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    goto :goto_1

    :cond_0
    move-object v1, v2

    .line 6045
    :goto_1
    iget-object v3, v0, Lcom/meawallet/mtp/fq;->d:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v3, :cond_1

    .line 7045
    iget-object v3, v0, Lcom/meawallet/mtp/fq;->d:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 166
    invoke-virtual {v3}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v3

    goto :goto_2

    :cond_1
    move-object v3, v2

    .line 7057
    :goto_2
    iget-object v4, v0, Lcom/meawallet/mtp/fq;->g:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v4, :cond_2

    .line 8057
    iget-object v4, v0, Lcom/meawallet/mtp/fq;->g:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 170
    invoke-virtual {v4}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v4

    goto :goto_3

    :cond_2
    move-object v4, v2

    .line 173
    :goto_3
    invoke-interface {v14, v1, v3, v4}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->exchangeDataWithLocalDek([B[B[B)V

    .line 175
    invoke-interface {v14}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->getExchangedMdSessionKey()[B

    move-result-object v4

    .line 177
    invoke-interface {v14}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->getExchangedUmdSingleUseKey()[B

    move-result-object v3

    .line 179
    invoke-interface {v14}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->getExchangedUmdSessionKey()[B

    move-result-object v6

    .line 9049
    iget-object v1, v0, Lcom/meawallet/mtp/fq;->e:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v1, :cond_3

    .line 10049
    iget-object v1, v0, Lcom/meawallet/mtp/fq;->e:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 186
    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    goto :goto_4

    :cond_3
    move-object v1, v2

    .line 10053
    :goto_4
    iget-object v5, v0, Lcom/meawallet/mtp/fq;->f:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v5, :cond_4

    .line 11053
    iget-object v5, v0, Lcom/meawallet/mtp/fq;->f:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 190
    invoke-virtual {v5}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v5

    goto :goto_5

    :cond_4
    move-object v5, v2

    .line 11061
    :goto_5
    iget-object v7, v0, Lcom/meawallet/mtp/fq;->h:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    if-eqz v7, :cond_5

    .line 12061
    iget-object v2, v0, Lcom/meawallet/mtp/fq;->h:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 194
    invoke-virtual {v2}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v2

    .line 197
    :cond_5
    invoke-interface {v14, v1, v5, v2}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->exchangeDataWithLocalDek([B[B[B)V

    .line 199
    invoke-interface {v14}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->getExchangedMdSessionKey()[B

    move-result-object v5

    .line 201
    invoke-interface {v14}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->getExchangedUmdSingleUseKey()[B

    move-result-object v7

    .line 203
    invoke-interface {v14}, Lcom/meawallet/mtp/ReplenishCommand$LocalDekExchanger;->getExchangedUmdSessionKey()[B

    move-result-object v8

    .line 208
    :try_start_0
    iget-object v1, v10, Lcom/meawallet/mtp/ReplenishCommand;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    new-instance v2, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    move/from16 v16, v9

    .line 13037
    iget-object v9, v0, Lcom/meawallet/mtp/fq;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 209
    invoke-virtual {v9}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v9

    invoke-direct {v2, v9}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;-><init>([B)V

    .line 208
    invoke-interface {v1, v12, v2}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->getLocalDekEncryptedIdn(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;

    move-result-object v1

    .line 209
    invoke-virtual {v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;->getEncryptedData()[B

    move-result-object v9
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14033
    iget v2, v0, Lcom/meawallet/mtp/fq;->a:I

    .line 217
    new-instance v1, Lcom/meawallet/mtp/ReplenishCommand$1;

    move-object v0, v1

    move-object v10, v1

    move-object/from16 v1, p0

    move/from16 v17, v2

    move-object v2, v3

    move-object v3, v7

    move-object v7, v8

    move-object v8, v9

    move/from16 v9, v17

    invoke-direct/range {v0 .. v9}, Lcom/meawallet/mtp/ReplenishCommand$1;-><init>(Lcom/meawallet/mtp/ReplenishCommand;[B[B[B[B[B[B[BI)V

    .line 278
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v16, 0x1

    move-object/from16 v10, p0

    goto/16 :goto_0

    .line 212
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid IDN"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    return-object v13
.end method


# virtual methods
.method final bridge synthetic a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    .line 21055
    invoke-static {p1, p2}, Lcom/meawallet/mtp/eo;->a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/eo;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 4

    .line 114
    :try_start_0
    sget-object v0, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    .line 2317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 116
    new-instance v0, Lcom/meawallet/mtp/ReplenishRequest;

    .line 3085
    iget-object v1, p0, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    .line 116
    iget-object v2, p0, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/meawallet/mtp/ReplenishCommand;->k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    invoke-direct {v0, v1, v2, v3}, Lcom/meawallet/mtp/ReplenishRequest;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)V

    .line 118
    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/ReplenishRequest;->buildAsJson(Lcom/google/gson/Gson;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/ReplenishCommand;->a(Ljava/lang/String;)Lcom/meawallet/mtp/y;

    move-result-object v0

    .line 4034
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 140
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 142
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 140
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 134
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 135
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 134
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 128
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute replenish HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 122
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute replenish command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 7

    .line 98
    iput-object p2, p0, Lcom/meawallet/mtp/ReplenishCommand;->c:Ljava/lang/String;

    .line 99
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 100
    iput-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand;->h:Lcom/meawallet/mtp/ae;

    .line 102
    iget-object p1, p0, Lcom/meawallet/mtp/ReplenishCommand;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-ne p1, v0, :cond_0

    .line 103
    iget-object v1, p0, Lcom/meawallet/mtp/ReplenishCommand;->d:Lcom/meawallet/mtp/ad;

    iget-object v3, p0, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-interface/range {v1 .. v6}, Lcom/meawallet/mtp/ad;->b(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final synthetic a(Lcom/meawallet/mtp/s;)V
    .locals 3

    .line 25
    check-cast p1, Lcom/meawallet/mtp/eo;

    .line 16061
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    .line 16063
    invoke-static {v0}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 16065
    sget-object p1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v0, "MOBILE_KEYS_MISSING"

    const-string v1, "Failed to decrypt replenish response."

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 17019
    :cond_0
    iget-object v1, p1, Lcom/meawallet/mtp/eo;->a:[Lcom/meawallet/mtp/fq;

    if-eqz v1, :cond_1

    .line 18019
    iget-object v1, p1, Lcom/meawallet/mtp/eo;->a:[Lcom/meawallet/mtp/fq;

    .line 16073
    array-length v1, v1

    if-lez v1, :cond_1

    .line 19019
    iget-object p1, p1, Lcom/meawallet/mtp/eo;->a:[Lcom/meawallet/mtp/fq;

    .line 16076
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    .line 16075
    invoke-direct {p0, p1, v0}, Lcom/meawallet/mtp/ReplenishCommand;->a([Lcom/meawallet/mtp/fq;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;)Ljava/util/ArrayList;

    move-result-object p1

    .line 16078
    sget-object v0, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 19317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 16080
    sget-object v0, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    iput-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand;->i:Lcom/meawallet/mtp/ah;

    .line 16082
    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    .line 20085
    iget-object v2, p0, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    .line 16082
    invoke-interface {v0, v1, v2, p1}, Lcom/meawallet/mtp/ad;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void

    .line 16084
    :cond_1
    sget-object p1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v0, "MISSING_TRANSACTION_CREDENTIALS"

    const-string v1, "No transaction credentials received from CMS-D"

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 16091
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to exchange transaction credential encryption"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/ReplenishCommand;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .line 376
    instance-of v0, p1, Lcom/meawallet/mtp/ReplenishCommand;

    if-eqz v0, :cond_0

    .line 378
    check-cast p1, Lcom/meawallet/mtp/ReplenishCommand;

    iget-object p1, p1, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/replenish"

    return-object v0
.end method

.method public final l()V
    .locals 8

    .line 14230
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 285
    sget-object v1, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_0

    .line 286
    iget-object v2, p0, Lcom/meawallet/mtp/ReplenishCommand;->d:Lcom/meawallet/mtp/ad;

    iget-object v4, p0, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    const-string v5, "CANCELED"

    const-string v6, "Too many commands already in queue."

    const/4 v7, 0x0

    move-object v3, p0

    invoke-interface/range {v2 .. v7}, Lcom/meawallet/mtp/ad;->b(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 15230
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 292
    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_1

    .line 293
    iget-object v2, p0, Lcom/meawallet/mtp/ReplenishCommand;->d:Lcom/meawallet/mtp/ad;

    iget-object v4, p0, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    const-string v5, "CANCELED"

    const-string v6, "Duplicate Request"

    const/4 v7, 0x0

    move-object v3, p0

    invoke-interface/range {v2 .. v7}, Lcom/meawallet/mtp/ad;->b(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 301
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 15317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 302
    iget-object v1, p0, Lcom/meawallet/mtp/ReplenishCommand;->d:Lcom/meawallet/mtp/ad;

    iget-object v3, p0, Lcom/meawallet/mtp/ReplenishCommand;->j:Ljava/lang/String;

    const-string v4, "CANCELED"

    const-string v5, "Replenish command cancelled or Could not get a valid session from CMS-D"

    const/4 v6, 0x0

    move-object v2, p0

    invoke-interface/range {v1 .. v6}, Lcom/meawallet/mtp/ad;->b(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 308
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/ReplenishCommand;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 313
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
