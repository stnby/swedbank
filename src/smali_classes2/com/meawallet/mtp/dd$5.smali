.class final Lcom/meawallet/mtp/dd$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/fl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/dd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/meawallet/mtp/fl<",
        "Lcom/meawallet/mtp/d<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;

.field final synthetic b:Lcom/meawallet/mtp/dd;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/dd;Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;)V
    .locals 0

    .line 842
    iput-object p1, p0, Lcom/meawallet/mtp/dd$5;->b:Lcom/meawallet/mtp/dd;

    iput-object p2, p0, Lcom/meawallet/mtp/dd$5;->a:Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b()Lcom/meawallet/mtp/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/meawallet/mtp/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 848
    :try_start_0
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    invoke-static {}, Lcom/meawallet/mtp/dd;->E()Lcom/meawallet/mtp/ci;

    move-result-object v1

    invoke-interface {v1}, Lcom/meawallet/mtp/ci;->a()Ljava/lang/String;

    move-result-object v1

    .line 1023
    iput-object v1, v0, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 852
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    .line 1029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 842
    invoke-static {}, Lcom/meawallet/mtp/dd$5;->b()Lcom/meawallet/mtp/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .line 842
    check-cast p1, Lcom/meawallet/mtp/d;

    .line 1858
    iget-object v0, p0, Lcom/meawallet/mtp/dd$5;->a:Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;

    if-eqz v0, :cond_1

    .line 1860
    invoke-virtual {p1}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1862
    iget-object v0, p0, Lcom/meawallet/mtp/dd$5;->a:Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;

    .line 2041
    iget-object p1, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 1862
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 3036
    :cond_0
    iget-object p1, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 1867
    check-cast p1, Ljava/lang/String;

    .line 1869
    iget-object v0, p0, Lcom/meawallet/mtp/dd$5;->a:Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;

    invoke-interface {v0, p1}, Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;->onSuccess(Ljava/lang/String;)V

    :cond_1
    return-void
.end method
