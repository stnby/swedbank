.class public Lcom/meawallet/mtp/MeaTokenPlatform$CdCvm;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaTokenPlatform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CdCvm"
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "MeaTokenPlatform$CdCvm"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getType()Lcom/meawallet/mtp/CdCvmType;
    .locals 2

    .line 1080
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->t()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 1083
    sget-object v1, Lcom/meawallet/mtp/MeaTokenPlatform$CdCvm;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public static isCardholderAuthenticated()Z
    .locals 1

    .line 1097
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->q()Z

    move-result v0

    return v0
.end method
