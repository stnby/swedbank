.class Lcom/meawallet/mtp/gk;
.super Lcom/meawallet/mtp/fv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/fv<",
        "Lcom/meawallet/mtp/MeaListener;",
        "Lcom/meawallet/mtp/eh;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "gk"


# instance fields
.field private final h:Ljava/lang/String;

.field private final i:Lcom/meawallet/mtp/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V
    .locals 6

    .line 28
    new-instance v4, Lcom/meawallet/mtp/gl;

    invoke-direct {v4, p4, p5, p6}, Lcom/meawallet/mtp/gl;-><init>(Lcom/meawallet/mtp/PaymentNetwork;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/fv;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V

    .line 34
    iput-object p5, p0, Lcom/meawallet/mtp/gk;->h:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/meawallet/mtp/gk;->i:Lcom/meawallet/mtp/ci;

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/d;)V
    .locals 1

    .line 69
    new-instance v0, Lcom/meawallet/mtp/ft;

    invoke-direct {v0}, Lcom/meawallet/mtp/ft;-><init>()V

    .line 1147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 69
    check-cast v0, Lcom/meawallet/mtp/MeaCoreListener;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/d;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void
.end method

.method final b()Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 40
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method

.method final b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/eh;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 51
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 54
    :try_start_0
    new-instance p1, Lcom/meawallet/mtp/ct;

    iget-object v0, p0, Lcom/meawallet/mtp/gk;->h:Ljava/lang/String;

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/meawallet/mtp/gk;->i:Lcom/meawallet/mtp/ci;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/i;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object p1

    .line 56
    invoke-virtual {p0}, Lcom/meawallet/mtp/gk;->h()Lcom/meawallet/mtp/ci;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_INITIALIZED:Lcom/meawallet/mtp/MeaCardState;

    invoke-interface {v0, p1, v1}, Lcom/meawallet/mtp/ci;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardState;)V

    .line 58
    new-instance p1, Lcom/meawallet/mtp/d;

    invoke-direct {p1}, Lcom/meawallet/mtp/d;-><init>()V
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 61
    new-instance v0, Lcom/meawallet/mtp/fs;

    invoke-direct {v0}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p1}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p1

    return-object p1
.end method

.method final d()Lcom/meawallet/mtp/ep;
    .locals 1

    .line 45
    sget-object v0, Lcom/meawallet/mtp/gy;->f:Lcom/meawallet/mtp/ep;

    return-object v0
.end method

.method final e()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/meawallet/mtp/gk;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    invoke-super {p0}, Lcom/meawallet/mtp/fv;->e()V

    return-void

    .line 74
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Card ID is empty"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
