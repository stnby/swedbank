.class Lcom/meawallet/mtp/dv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteCommunicationEventListener;


# static fields
.field private static final a:Ljava/lang/String; = "dv"


# instance fields
.field private final b:Lcom/meawallet/mtp/dn;

.field private final c:Lcom/meawallet/mtp/l;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/dn;Lcom/meawallet/mtp/l;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/meawallet/mtp/dv;->b:Lcom/meawallet/mtp/dn;

    .line 34
    iput-object p2, p0, Lcom/meawallet/mtp/dv;->c:Lcom/meawallet/mtp/l;

    return-void
.end method

.method private a()Lcom/meawallet/mtp/ci;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/meawallet/mtp/dv;->b:Lcom/meawallet/mtp/dn;

    invoke-interface {v0}, Lcom/meawallet/mtp/dn;->b()Lcom/meawallet/mtp/ci;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    .line 414
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 416
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 417
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 419
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "card_id"

    .line 420
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 423
    :cond_0
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->c()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroidx/i/a/a;->a(Landroid/content/Context;)Landroidx/i/a/a;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/i/a/a;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method private static a(Lcom/meawallet/mtp/bt;)Z
    .locals 4

    const/4 v0, 0x1

    if-nez p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x2

    .line 431
    new-array v1, v1, [Ljava/lang/Object;

    .line 6088
    iget-object v2, p0, Lcom/meawallet/mtp/bt;->d:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 7054
    iget-object v2, p0, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    aput-object v2, v1, v0

    .line 8054
    iget-object v1, p0, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    if-eqz v1, :cond_2

    .line 432
    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    .line 9054
    iget-object v2, p0, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 433
    invoke-virtual {v1, v2}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    .line 10054
    iget-object p0, p0, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 433
    invoke-virtual {v1, p0}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    return v3

    :cond_2
    :goto_0
    return v0
.end method

.method private b()Lcom/meawallet/mtp/m;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/meawallet/mtp/dv;->b:Lcom/meawallet/mtp/dn;

    invoke-interface {v0}, Lcom/meawallet/mtp/dn;->c()Lcom/meawallet/mtp/m;

    move-result-object v0

    return-object v0
.end method

.method private c()Landroid/content/Context;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/meawallet/mtp/dv;->b:Lcom/meawallet/mtp/dn;

    invoke-interface {v0}, Lcom/meawallet/mtp/dn;->b()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0}, Lcom/meawallet/mtp/ci;->j()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCardPinReset(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 378
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v0, "com.meawallet.mtp.intent.ON_CARD_PIN_RESET_REQUEST"

    .line 380
    invoke-direct {p0, v0, p1}, Lcom/meawallet/mtp/dv;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onChangeCardMobilePinStarted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    .line 405
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    return-void
.end method

.method public onChangeCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onChangeCardPinSucceeded(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x1

    .line 224
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v1, 0x0

    .line 227
    :try_start_0
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v3

    invoke-static {p1, v1, v3}, Lcom/meawallet/mtp/i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v1

    .line 229
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v3

    sget-object v4, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    invoke-interface {v3, v1, v4}, Lcom/meawallet/mtp/ci;->a(Ljava/lang/String;Lcom/meawallet/mtp/LdePinState;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 231
    sget-object v3, Lcom/meawallet/mtp/dv;->a:Ljava/lang/String;

    const-string v4, "Failed to update card PIN state in LDE."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v5}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 235
    sget-object v3, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v2

    .line 239
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->b()Lcom/meawallet/mtp/m;

    move-result-object p1

    invoke-virtual {p1, v1, v0}, Lcom/meawallet/mtp/m;->a(Ljava/util/List;[Ljava/lang/String;)V

    return-void
.end method

.method public onChangeWalletMobilePinStarted(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 410
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    return-void
.end method

.method public onChangeWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onChangeWalletPinSucceeded()V
    .locals 2

    .line 262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 263
    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->b()Lcom/meawallet/mtp/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/m;->a(Ljava/util/List;)V

    return-void
.end method

.method public onDeleteCardFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onDeleteCardSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    .line 347
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    return-void
.end method

.method public onProvisionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 118
    new-instance p4, Lcom/meawallet/mtp/ct;

    invoke-direct {p4, p1}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/dv;->c:Lcom/meawallet/mtp/l;

    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v1

    .line 2253
    sget-object v2, Lcom/meawallet/mtp/MeaCardState;->PROVISION_FAILED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v0, p4, v2, v1}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardState;Lcom/meawallet/mtp/ci;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 125
    sget-object v1, Lcom/meawallet/mtp/dv;->a:Ljava/lang/String;

    const-string v2, "Failed to update card %s state after provision failure."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 3107
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->a:Lcom/meawallet/mtp/MeaCardProvisionListener;

    if-eqz v0, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object p1

    .line 134
    invoke-static {p1}, Lcom/meawallet/mtp/dv;->a(Lcom/meawallet/mtp/bt;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 136
    invoke-static {p2, p3}, Lcom/meawallet/mtp/dl;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaError;

    move-result-object p1

    .line 138
    new-instance p2, Lcom/meawallet/mtp/dv$3;

    invoke-direct {p2, p0, v0, p4, p1}, Lcom/meawallet/mtp/dv$3;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaCardProvisionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    invoke-static {p2}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onProvisionSucceeded(Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;Ljava/lang/String;)V
    .locals 4

    const/4 p1, 0x1

    .line 51
    new-array v0, p1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 53
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcom/meawallet/mtp/dv;->a(Lcom/meawallet/mtp/bt;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 57
    new-instance v0, Lcom/meawallet/mtp/ct;

    invoke-direct {v0, p2}, Lcom/meawallet/mtp/ct;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v2

    .line 1162
    iget-object v2, v2, Lcom/meawallet/mtp/ax;->f:Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    if-eqz v2, :cond_0

    .line 63
    new-instance v3, Lcom/meawallet/mtp/dv$1;

    invoke-direct {v3, p0, v2, v0}, Lcom/meawallet/mtp/dv$1;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;Lcom/meawallet/mtp/MeaCard;)V

    invoke-static {v3}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 71
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v2

    .line 2107
    iget-object v2, v2, Lcom/meawallet/mtp/ax;->a:Lcom/meawallet/mtp/MeaCardProvisionListener;

    if-eqz v2, :cond_1

    .line 74
    new-instance v3, Lcom/meawallet/mtp/dv$2;

    invoke-direct {v3, p0, v2, v0}, Lcom/meawallet/mtp/dv$2;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaCardProvisionListener;Lcom/meawallet/mtp/MeaCard;)V

    invoke-static {v3}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 84
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 85
    sget-object v2, Lcom/meawallet/mtp/CdCvmType;->FINGERPRINT_FRAGMENT:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v2, Lcom/meawallet/mtp/CdCvmType;->DEVICE_UNLOCK:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    sget-object v2, Lcom/meawallet/mtp/CdCvmType;->NO_CDCVM:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v2

    invoke-interface {v2}, Lcom/meawallet/mtp/ci;->l()Lcom/meawallet/mtp/LdePinState;

    move-result-object v2

    sget-object v3, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    if-ne v2, v3, :cond_2

    .line 91
    sget-object v2, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_2
    new-array p1, p1, [Ljava/lang/String;

    aput-object p2, p1, v1

    .line 96
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->b()Lcom/meawallet/mtp/m;

    move-result-object p2

    invoke-virtual {p2, v0, p1}, Lcom/meawallet/mtp/m;->a(Ljava/util/List;[Ljava/lang/String;)V

    return-void

    .line 101
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/dv;->b:Lcom/meawallet/mtp/dn;

    invoke-interface {v0}, Lcom/meawallet/mtp/dn;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 103
    invoke-interface {v0, p2}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->deleteCard(Lcom/mastercard/mpsdk/componentinterface/Card;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 106
    sget-object v2, Lcom/meawallet/mtp/dv;->a:Ljava/lang/String;

    const-string v3, "Failed to delete card %s."

    new-array p1, p1, [Ljava/lang/Object;

    aput-object p2, p1, v1

    invoke-static {v2, v0, v3, p1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onReplenishFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 303
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p2

    .line 5113
    iget-object v2, p2, Lcom/meawallet/mtp/ax;->b:Lcom/meawallet/mtp/MeaCardReplenishListener;

    if-eqz v2, :cond_1

    .line 306
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object p2

    .line 308
    invoke-static {p2}, Lcom/meawallet/mtp/dv;->a(Lcom/meawallet/mtp/bt;)Z

    move-result p2

    if-nez p2, :cond_1

    const-string p2, "MISSING_TRANSACTION_CREDENTIALS"

    .line 310
    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 312
    new-instance p2, Lcom/meawallet/mtp/dv$7;

    invoke-direct {p2, p0, v2, p1}, Lcom/meawallet/mtp/dv$7;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaCardReplenishListener;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    return-void

    .line 321
    :cond_0
    new-instance p2, Lcom/meawallet/mtp/dv$8;

    move-object v0, p2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/dv$8;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaCardReplenishListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public onReplenishSucceeded(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/mastercard/mpsdk/componentinterface/SingleUseKey;",
            ">;)V"
        }
    .end annotation

    .line 274
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object p2

    .line 4113
    iget-object p2, p2, Lcom/meawallet/mtp/ax;->b:Lcom/meawallet/mtp/MeaCardReplenishListener;

    if-eqz p2, :cond_1

    .line 277
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/meawallet/mtp/ci;->b(Ljava/lang/String;)Lcom/meawallet/mtp/bt;

    move-result-object v0

    .line 279
    invoke-static {v0}, Lcom/meawallet/mtp/dv;->a(Lcom/meawallet/mtp/bt;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    .line 281
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 283
    :goto_0
    new-instance v0, Lcom/meawallet/mtp/dv$6;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/meawallet/mtp/dv$6;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaCardReplenishListener;Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public onRequestSessionFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onRequestSessionSuccess()V
    .locals 0

    return-void
.end method

.method public onSetCardPinFailed(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onSetCardPinSucceeded(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x1

    .line 150
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    :try_start_0
    const-string v1, ""

    .line 153
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v3

    invoke-static {p1, v1, v3}, Lcom/meawallet/mtp/i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v3

    sget-object v4, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    invoke-interface {v3, v1, v4}, Lcom/meawallet/mtp/ci;->a(Ljava/lang/String;Lcom/meawallet/mtp/LdePinState;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 157
    sget-object v3, Lcom/meawallet/mtp/dv;->a:Ljava/lang/String;

    const-string v4, "Failed to update card PIN state in LDE."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v5}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/meawallet/mtp/ax;->d(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCardPinListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 163
    new-instance v3, Lcom/meawallet/mtp/dv$4;

    invoke-direct {v3, p0, v1, p1}, Lcom/meawallet/mtp/dv$4;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaCardPinListener;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 171
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 172
    sget-object v3, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v2

    .line 176
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->b()Lcom/meawallet/mtp/m;

    move-result-object p1

    invoke-virtual {p1, v1, v0}, Lcom/meawallet/mtp/m;->a(Ljava/util/List;[Ljava/lang/String;)V

    return-void
.end method

.method public onSetWalletPinFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onSetWalletPinSucceeded()V
    .locals 4

    .line 192
    :try_start_0
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->a()Lcom/meawallet/mtp/ci;

    move-result-object v0

    sget-object v1, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ci;->a(Lcom/meawallet/mtp/LdePinState;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 194
    sget-object v1, Lcom/meawallet/mtp/dv;->a:Ljava/lang/String;

    const-string v2, "Failed to update wallet PIN state in LDE."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 3119
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->c:Lcom/meawallet/mtp/MeaWalletPinListener;

    if-eqz v0, :cond_0

    .line 200
    new-instance v1, Lcom/meawallet/mtp/dv$5;

    invoke-direct {v1, p0, v0}, Lcom/meawallet/mtp/dv$5;-><init>(Lcom/meawallet/mtp/dv;Lcom/meawallet/mtp/MeaWalletPinListener;)V

    invoke-static {v1}, Lcom/meawallet/mtp/es;->a(Ljava/lang/Runnable;)V

    .line 208
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 209
    sget-object v1, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-direct {p0}, Lcom/meawallet/mtp/dv;->b()Lcom/meawallet/mtp/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/m;->a(Ljava/util/List;)V

    return-void
.end method

.method public onSystemHealthCompleted()V
    .locals 0

    return-void
.end method

.method public onSystemHealthFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onTaskStatusReceived(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 352
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    return-void
.end method

.method public onTaskStatusReceivedFailure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public onWalletPinReset()V
    .locals 2

    const-string v0, "com.meawallet.mtp.intent.ON_WALLET_PIN_RESET_REQUEST"

    const-string v1, ""

    .line 387
    invoke-direct {p0, v0, v1}, Lcom/meawallet/mtp/dv;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
