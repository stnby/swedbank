.class final enum Lcom/meawallet/mtp/ah;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/ah;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/meawallet/mtp/ah;

.field public static final enum b:Lcom/meawallet/mtp/ah;

.field private static final synthetic c:[Lcom/meawallet/mtp/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 4
    new-instance v0, Lcom/meawallet/mtp/ah;

    const-string v1, "WAIT_FOR_SESSION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ah;->a:Lcom/meawallet/mtp/ah;

    .line 5
    new-instance v0, Lcom/meawallet/mtp/ah;

    const-string v1, "REMOVE_FROM_QUEUE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    const/4 v0, 0x2

    .line 3
    new-array v0, v0, [Lcom/meawallet/mtp/ah;

    sget-object v1, Lcom/meawallet/mtp/ah;->a:Lcom/meawallet/mtp/ah;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    aput-object v1, v0, v3

    sput-object v0, Lcom/meawallet/mtp/ah;->c:[Lcom/meawallet/mtp/ah;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/ah;
    .locals 1

    .line 3
    const-class v0, Lcom/meawallet/mtp/ah;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/ah;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/ah;
    .locals 1

    .line 3
    sget-object v0, Lcom/meawallet/mtp/ah;->c:[Lcom/meawallet/mtp/ah;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/ah;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/ah;

    return-object v0
.end method
