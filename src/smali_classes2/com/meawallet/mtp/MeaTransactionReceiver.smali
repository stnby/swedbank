.class public abstract Lcom/meawallet/mtp/MeaTransactionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "MeaTransactionReceiver"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method protected handleOnAuthenticationRequiredIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 0

    return-void
.end method

.method protected handleOnAuthenticationRequiredIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method protected handleOnTransactionFailureIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaError;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method protected handleOnTransactionFailureIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaError;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 0

    return-void
.end method

.method protected handleOnTransactionFailureIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method protected handleOnTransactionSubmittedIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 0

    return-void
.end method

.method protected handleOnTransactionSubmittedIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x1f5

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 22
    sget-object p1, Lcom/meawallet/mtp/MeaTransactionReceiver;->a:Ljava/lang/String;

    const-string p2, "Received intent with null action."

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, p2, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "card_id"

    .line 27
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "contactless_transaction_data"

    .line 30
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/meawallet/mtp/MeaContactlessTransactionData;

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    .line 33
    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    .line 36
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v7

    const v8, 0x3ca5b102

    if-eq v7, v8, :cond_4

    const v8, 0x6e270765

    if-eq v7, v8, :cond_3

    const v4, 0x72c6b916

    if-eq v7, v4, :cond_2

    goto :goto_0

    :cond_2
    const-string v4, "com.meawallet.mtp.intent.ON_TRANSACTION_SUBMITTED_MESSAGE"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    const-string v7, "com.meawallet.mtp.intent.ON_TRANSACTION_FAILURE_MESSAGE"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_1

    :cond_4
    const-string v4, "com.meawallet.mtp.intent.ON_AUTHENTICATION_REQUIRED_MESSAGE"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x2

    goto :goto_1

    :cond_5
    :goto_0
    const/4 v4, -0x1

    :goto_1
    const/4 v5, 0x0

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_6

    .line 83
    :pswitch_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_6

    .line 84
    sget-object p1, Lcom/meawallet/mtp/MeaTransactionReceiver;->a:Ljava/lang/String;

    const-string p2, "Failed to handle on authentication required broadcast, passed card id in Intent is null"

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, p2, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_6
    if-eqz v3, :cond_8

    .line 94
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getAmount()Ljava/lang/Double;

    move-result-object p2

    if-eqz p2, :cond_7

    .line 95
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getAmount()Ljava/lang/Double;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_7
    move-object p2, v5

    .line 98
    :goto_2
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 99
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_8
    move-object p2, v5

    .line 103
    :cond_9
    :goto_3
    invoke-virtual {p0, p1, v0, v3}, Lcom/meawallet/mtp/MeaTransactionReceiver;->handleOnAuthenticationRequiredIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    .line 105
    invoke-virtual {p0, p1, v0, p2, v5}, Lcom/meawallet/mtp/MeaTransactionReceiver;->handleOnAuthenticationRequiredIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :pswitch_1
    const-string v2, "error_code"

    .line 68
    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "error_message"

    .line 69
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 71
    new-instance v2, Lcom/meawallet/mtp/cy;

    invoke-direct {v2, v1, p2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 74
    invoke-virtual {p0, p1, v0, v2}, Lcom/meawallet/mtp/MeaTransactionReceiver;->handleOnTransactionFailureIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaError;)V

    .line 76
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, v0, p2}, Lcom/meawallet/mtp/MeaTransactionReceiver;->handleOnTransactionFailureIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0, p1, v0, v2, v3}, Lcom/meawallet/mtp/MeaTransactionReceiver;->handleOnTransactionFailureIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaError;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    return-void

    .line 39
    :pswitch_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_a

    .line 40
    sget-object p1, Lcom/meawallet/mtp/MeaTransactionReceiver;->a:Ljava/lang/String;

    const-string p2, "Failed to handle on transaction submitted broadcast, passed card id in Intent is null"

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, p2, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 46
    :cond_a
    invoke-virtual {p0, p1, v0, v3}, Lcom/meawallet/mtp/MeaTransactionReceiver;->handleOnTransactionSubmittedIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V

    if-eqz v3, :cond_c

    .line 52
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getAmount()Ljava/lang/Double;

    move-result-object p2

    if-eqz p2, :cond_b

    .line 53
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getAmount()Ljava/lang/Double;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_4

    :cond_b
    move-object p2, v5

    .line 56
    :goto_4
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 57
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_c
    move-object p2, v5

    .line 62
    :cond_d
    :goto_5
    invoke-virtual {p0, p1, v0, p2, v5}, Lcom/meawallet/mtp/MeaTransactionReceiver;->handleOnTransactionSubmittedIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :goto_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
