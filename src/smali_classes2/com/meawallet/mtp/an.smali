.class final Lcom/meawallet/mtp/an;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# instance fields
.field private final j:Ljava/lang/String;

.field private final k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

.field private final l:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 10

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p10

    .line 38
    invoke-direct/range {v0 .. v8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object/from16 v0, p8

    .line 47
    iput-object v0, v9, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    move-object/from16 v0, p9

    .line 48
    iput-object v0, v9, Lcom/meawallet/mtp/an;->k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    move-object v0, p1

    .line 49
    iput-object v0, v9, Lcom/meawallet/mtp/an;->l:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    .line 58
    invoke-static {p1, p2}, Lcom/meawallet/mtp/s;->b(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/google/gson/Gson;)V
    .locals 4

    .line 86
    :try_start_0
    sget-object v0, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    .line 3317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 88
    new-instance v0, Lcom/meawallet/mtp/ao;

    iget-object v1, p0, Lcom/meawallet/mtp/an;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/meawallet/mtp/an;->k:[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;

    invoke-direct {v0, v1, v2, v3}, Lcom/meawallet/mtp/ao;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/mastercard/mpsdk/componentinterface/SingleUseKeyStatus;)V

    .line 4027
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/an;->a(Ljava/lang/String;)Lcom/meawallet/mtp/y;

    move-result-object v0

    .line 4034
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 92
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/an;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 113
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 115
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/an;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 107
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 108
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/an;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 101
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute set Delete Token command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/an;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 94
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute Delete Token HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/an;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 7

    .line 69
    iput-object p2, p0, Lcom/meawallet/mtp/an;->c:Ljava/lang/String;

    .line 70
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 2317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 71
    iput-object p1, p0, Lcom/meawallet/mtp/an;->h:Lcom/meawallet/mtp/ae;

    .line 73
    iget-object p1, p0, Lcom/meawallet/mtp/an;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-ne p1, v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/meawallet/mtp/an;->d:Lcom/meawallet/mtp/ad;

    iget-object v3, p0, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-interface/range {v1 .. v6}, Lcom/meawallet/mtp/ad;->a(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final a(Lcom/meawallet/mtp/s;)V
    .locals 1

    .line 62
    sget-object p1, Lcom/meawallet/mtp/ag;->d:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 64
    sget-object p1, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    iput-object p1, p0, Lcom/meawallet/mtp/an;->i:Lcom/meawallet/mtp/ah;

    .line 65
    iget-object p1, p0, Lcom/meawallet/mtp/an;->d:Lcom/meawallet/mtp/ad;

    iget-object v0, p0, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    invoke-interface {p1, p0, v0}, Lcom/meawallet/mtp/ad;->a(Lcom/meawallet/mtp/ee;Ljava/lang/String;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .line 159
    instance-of v0, p1, Lcom/meawallet/mtp/an;

    if-eqz v0, :cond_0

    .line 160
    check-cast p1, Lcom/meawallet/mtp/an;

    iget-object p1, p1, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/delete"

    return-object v0
.end method

.method public final l()V
    .locals 8

    .line 4230
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 121
    sget-object v1, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_0

    .line 122
    iget-object v2, p0, Lcom/meawallet/mtp/an;->d:Lcom/meawallet/mtp/ad;

    iget-object v4, p0, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    const-string v5, "CANCELED"

    const-string v6, "Too many commands already in queue."

    const/4 v7, 0x0

    move-object v3, p0

    invoke-interface/range {v2 .. v7}, Lcom/meawallet/mtp/ad;->a(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 5230
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 128
    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_1

    .line 129
    iget-object v2, p0, Lcom/meawallet/mtp/an;->d:Lcom/meawallet/mtp/ad;

    iget-object v4, p0, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    const-string v5, "CANCELED"

    const-string v6, "Duplicate Request"

    const/4 v7, 0x0

    move-object v3, p0

    invoke-interface/range {v2 .. v7}, Lcom/meawallet/mtp/ad;->a(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 137
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 5317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 138
    iget-object v1, p0, Lcom/meawallet/mtp/an;->d:Lcom/meawallet/mtp/ad;

    iget-object v3, p0, Lcom/meawallet/mtp/an;->j:Ljava/lang/String;

    const-string v4, "CANCELED"

    const-string v5, "Delete Token command cancelled or Could not get a valid session from CMS-D"

    const/4 v6, 0x0

    move-object v2, p0

    invoke-interface/range {v1 .. v6}, Lcom/meawallet/mtp/ad;->a(Lcom/meawallet/mtp/ee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 145
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/an;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 150
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
