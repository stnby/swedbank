.class final Lcom/meawallet/mtp/au$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/WalletData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/au$1;->getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/au$1;


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/au$1;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/meawallet/mtp/au$1$1;->a:Lcom/meawallet/mtp/au$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccountType()Lcom/mastercard/mpsdk/componentinterface/CardAccountType;
    .locals 4

    .line 80
    iget-object v0, p0, Lcom/meawallet/mtp/au$1$1;->a:Lcom/meawallet/mtp/au$1;

    iget-object v0, v0, Lcom/meawallet/mtp/au$1;->a:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->accountType:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 82
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$1$1;->a:Lcom/meawallet/mtp/au$1;

    iget-object v0, v0, Lcom/meawallet/mtp/au$1;->a:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->accountType:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0x3de9ccc

    if-eq v2, v3, :cond_3

    const v3, 0x19d1382a

    if-eq v2, v3, :cond_2

    const v3, 0x76f89ef9

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "CREDIT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v2, "UNKNOWN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    goto :goto_0

    :cond_3
    const-string v2, "DEBIT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 94
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    .line 91
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    .line 89
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->DEBIT:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    .line 87
    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardAccountType;->CREDIT:Lcom/mastercard/mpsdk/componentinterface/CardAccountType;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCardholderValidator()Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;
    .locals 4

    .line 57
    iget-object v0, p0, Lcom/meawallet/mtp/au$1$1;->a:Lcom/meawallet/mtp/au$1;

    iget-object v0, v0, Lcom/meawallet/mtp/au$1;->a:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->walletRelatedData:Lcom/mastercard/mpsdk/card/profile/v2/WalletRelatedDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/WalletRelatedDataV2Json;->cardholderValidator:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 58
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->MOBILE_PIN:Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    return-object v0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$1$1;->a:Lcom/meawallet/mtp/au$1;

    iget-object v0, v0, Lcom/meawallet/mtp/au$1;->a:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->walletRelatedData:Lcom/mastercard/mpsdk/card/profile/v2/WalletRelatedDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/WalletRelatedDataV2Json;->cardholderValidator:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x674cfcf7

    if-eq v2, v3, :cond_2

    const v3, -0x616d37a8

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "MOBILE_PIN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v2, "LOCALLY_VERIFIED_CDCVM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 68
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->MOBILE_PIN:Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    return-object v0

    .line 65
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->LOCALLY_VERIFIED_CDCVM:Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    return-object v0

    .line 63
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;->MOBILE_PIN:Lcom/mastercard/mpsdk/componentinterface/CardholderValidator;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCvmResetTimeout()I
    .locals 1

    const/16 v0, 0x1e

    return v0
.end method

.method public final getDualTapResetTimeout()I
    .locals 1

    const/16 v0, 0x1e

    return v0
.end method

.method public final getProductType()Lcom/mastercard/mpsdk/componentinterface/CardProductType;
    .locals 3

    .line 99
    iget-object v0, p0, Lcom/meawallet/mtp/au$1$1;->a:Lcom/meawallet/mtp/au$1;

    iget-object v0, v0, Lcom/meawallet/mtp/au$1;->a:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->productType:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 100
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/au$1$1;->a:Lcom/meawallet/mtp/au$1;

    iget-object v0, v0, Lcom/meawallet/mtp/au$1;->a:Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/DigitizedCardProfileV2Json;->mchipCardProfile:Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/MchipCardProfileV2Json;->commonData:Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v2/CommonDataV2Json;->productType:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "CREDIT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "COMMERCIAL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_2
    const-string v2, "UNKNOWN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_3
    const-string v2, "PREPAID"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "DEBIT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 120
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    .line 117
    :pswitch_0
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->UNKNOWN:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    .line 114
    :pswitch_1
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->PREPAID:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    .line 111
    :pswitch_2
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->COMMERCIAL:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    .line 108
    :pswitch_3
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->DEBIT:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    .line 105
    :pswitch_4
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/CardProductType;->CREDIT:Lcom/mastercard/mpsdk/componentinterface/CardProductType;

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x3de9ccc -> :sswitch_4
        0x17d197cf -> :sswitch_3
        0x19d1382a -> :sswitch_2
        0x52b2a74a -> :sswitch_1
        0x76f89ef9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
