.class final Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/CryptoServiceNativeImpl;->getRemoteManagementKeyEncryptionKeyManager()Lcom/mastercard/mpsdk/componentinterface/crypto/KeyLifeCycleManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/CryptoServiceNativeImpl;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/meawallet/mtp/CryptoServiceNativeImpl;)V
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;->a:Lcom/meawallet/mtp/CryptoServiceNativeImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 343
    iput-boolean p1, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;->b:Z

    return-void
.end method


# virtual methods
.method public final abandonRollover()V
    .locals 1

    const/4 v0, 0x0

    .line 372
    iput-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;->b:Z

    return-void
.end method

.method public final getCurrentKeyId()[B
    .locals 1

    .line 347
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final isRolloverInProgress()Z
    .locals 1

    .line 367
    iget-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;->b:Z

    return v0
.end method

.method public final rolloverComplete()V
    .locals 3

    const/4 v0, 0x0

    .line 358
    iput-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;->b:Z

    .line 359
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->y()V

    .line 362
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "RM_KEK_KEY_ID_PREF_KEY"

    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public final rolloverData([B[B[B)[B
    .locals 1

    .line 384
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/String;-><init>([B)V

    invoke-static {v0, p1, p3}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->c(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object p1

    return-object p1
.end method

.method public final startRollover()[B
    .locals 1

    const/4 v0, 0x1

    .line 352
    iput-boolean v0, p0, Lcom/meawallet/mtp/CryptoServiceNativeImpl$4;->b:Z

    .line 353
    invoke-static {}, Lcom/meawallet/mtp/CryptoServiceNativeImpl;->x()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final wipeKey()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
