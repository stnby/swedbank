.class public Lcom/meawallet/mtp/MeaAuthenticationMethod;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private b:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "value"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/meawallet/mtp/MeaAuthenticationMethod;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/meawallet/mtp/MeaAuthenticationMethod;->b:Lcom/meawallet/mtp/MeaAuthenticationMethod$Type;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/meawallet/mtp/MeaAuthenticationMethod;->c:Ljava/lang/String;

    return-object v0
.end method

.method validate()V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/meawallet/mtp/MeaAuthenticationMethod;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Authentication method id is null"

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
