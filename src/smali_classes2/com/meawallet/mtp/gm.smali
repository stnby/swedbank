.class Lcom/meawallet/mtp/gm;
.super Lcom/meawallet/mtp/fv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/fv<",
        "Lcom/meawallet/mtp/MeaInitializeDigitizationListener;",
        "Lcom/meawallet/mtp/gn;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String; = "gm"


# instance fields
.field private final h:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V
    .locals 6

    .line 1224
    iget-object v4, p4, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->b:Lcom/meawallet/mtp/gx;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    .line 26
    invoke-direct/range {v0 .. v5}, Lcom/meawallet/mtp/fv;-><init>(Lcom/meawallet/mtp/ci;Lcom/meawallet/mtp/dq;Lcom/google/gson/Gson;Lcom/meawallet/mtp/gx;Ljava/lang/Object;)V

    .line 28
    iput-object p4, p0, Lcom/meawallet/mtp/gm;->h:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    return-void
.end method


# virtual methods
.method final a(Lcom/meawallet/mtp/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/gn;",
            ">;)V"
        }
    .end annotation

    .line 14147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 117
    invoke-virtual {p1}, Lcom/meawallet/mtp/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15147
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 118
    check-cast v0, Lcom/meawallet/mtp/MeaCoreListener;

    .line 16041
    iget-object p1, p1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 118
    invoke-static {v0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 17036
    :cond_0
    iget-object p1, p1, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    .line 123
    check-cast p1, Lcom/meawallet/mtp/gn;

    if-nez p1, :cond_1

    .line 17147
    iget-object p1, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 126
    check-cast p1, Lcom/meawallet/mtp/MeaCoreListener;

    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f5

    const-string v2, "Initialize digitization response data element is null."

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 18147
    :cond_1
    iget-object v0, p0, Lcom/meawallet/mtp/el;->f:Ljava/lang/Object;

    .line 132
    check-cast v0, Lcom/meawallet/mtp/MeaInitializeDigitizationListener;

    .line 19023
    iget-object v1, p1, Lcom/meawallet/mtp/gn;->a:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 19027
    iget-object v2, p1, Lcom/meawallet/mtp/gn;->b:Ljava/lang/String;

    .line 19031
    iget-object p1, p1, Lcom/meawallet/mtp/gn;->c:Ljava/lang/Boolean;

    .line 132
    invoke-interface {v0, v1, v2, p1}, Lcom/meawallet/mtp/MeaInitializeDigitizationListener;->onSuccess(Lcom/meawallet/mtp/MeaEligibilityReceipt;Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_2
    return-void
.end method

.method final b()Lcom/meawallet/mtp/MeaHttpMethod;
    .locals 1

    .line 33
    sget-object v0, Lcom/meawallet/mtp/MeaHttpMethod;->POST:Lcom/meawallet/mtp/MeaHttpMethod;

    return-object v0
.end method

.method final b(Ljava/lang/String;)Lcom/meawallet/mtp/d;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/meawallet/mtp/d<",
            "Lcom/meawallet/mtp/gn;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 44
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 47
    :try_start_0
    const-class v0, Lcom/meawallet/mtp/gn;

    .line 48
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/gm;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/gn;

    if-eqz p1, :cond_4

    .line 54
    invoke-virtual {p1}, Lcom/meawallet/mtp/gn;->validate()V

    .line 3023
    iget-object v0, p1, Lcom/meawallet/mtp/gn;->a:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 57
    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/bm; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/meawallet/mtp/bl; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    :try_start_1
    invoke-virtual {p0}, Lcom/meawallet/mtp/gm;->h()Lcom/meawallet/mtp/ci;

    move-result-object v2

    const/4 v3, 0x0

    .line 63
    invoke-static {v3, v1, v2}, Lcom/meawallet/mtp/i;->b(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object v1

    if-nez v1, :cond_0

    .line 67
    new-instance v1, Lcom/meawallet/mtp/bt;

    invoke-direct {v1}, Lcom/meawallet/mtp/bt;-><init>()V

    .line 3118
    iput-object v0, v1, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 4027
    iget-object v0, p1, Lcom/meawallet/mtp/gn;->b:Ljava/lang/String;

    .line 4134
    iput-object v0, v1, Lcom/meawallet/mtp/bt;->k:Ljava/lang/String;

    .line 5035
    iget-object v0, p1, Lcom/meawallet/mtp/gn;->d:Lcom/meawallet/mtp/PaymentNetwork;

    .line 5126
    iput-object v0, v1, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    .line 72
    invoke-virtual {p0}, Lcom/meawallet/mtp/gm;->h()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ci;->a(Lcom/meawallet/mtp/bt;)V

    goto :goto_0

    .line 74
    :cond_0
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->DIGITIZATION_STARTED:Lcom/meawallet/mtp/MeaCardState;

    .line 6054
    iget-object v2, v1, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 74
    invoke-virtual {v0, v2}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7027
    iget-object v0, p1, Lcom/meawallet/mtp/gn;->b:Ljava/lang/String;

    .line 7134
    iput-object v0, v1, Lcom/meawallet/mtp/bt;->k:Ljava/lang/String;

    .line 8035
    iget-object v0, p1, Lcom/meawallet/mtp/gn;->d:Lcom/meawallet/mtp/PaymentNetwork;

    .line 8126
    iput-object v0, v1, Lcom/meawallet/mtp/bt;->j:Lcom/meawallet/mtp/PaymentNetwork;

    .line 79
    invoke-virtual {p0}, Lcom/meawallet/mtp/gm;->h()Lcom/meawallet/mtp/ci;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ci;->b(Lcom/meawallet/mtp/bt;)V

    goto :goto_0

    .line 81
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    .line 9054
    iget-object v2, v1, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 81
    invoke-virtual {v0, v2}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x3ec

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    .line 10029
    iput-object v1, v0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v0

    .line 87
    :cond_2
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    .line 10054
    iget-object v2, v1, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 87
    invoke-virtual {v0, v2}, Lcom/meawallet/mtp/MeaCardState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x3f8

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    .line 11029
    iput-object v1, v0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    return-object v0

    .line 93
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    new-instance v2, Lcom/meawallet/mtp/cy;

    const/16 v3, 0x3f5

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Already existing card state: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 11054
    iget-object v1, v1, Lcom/meawallet/mtp/bt;->b:Lcom/meawallet/mtp/MeaCardState;

    .line 95
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    .line 12029
    iput-object v2, v0, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;
    :try_end_1
    .catch Lcom/meawallet/mtp/av; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/bv; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/bn; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/bm; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/meawallet/mtp/bl; {:try_start_1 .. :try_end_1} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    .line 100
    :try_start_2
    new-instance v1, Lcom/meawallet/mtp/d;

    invoke-direct {v1}, Lcom/meawallet/mtp/d;-><init>()V

    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    .line 13029
    iput-object v0, v1, Lcom/meawallet/mtp/d;->b:Lcom/meawallet/mtp/MeaError;

    .line 103
    :goto_0
    new-instance v0, Lcom/meawallet/mtp/d;

    invoke-direct {v0}, Lcom/meawallet/mtp/d;-><init>()V

    .line 14023
    iput-object p1, v0, Lcom/meawallet/mtp/d;->a:Ljava/lang/Object;

    return-object v0

    .line 51
    :cond_4
    new-instance p1, Lcom/meawallet/mtp/bl;

    const-string v0, "Response data is null."

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/meawallet/mtp/bv; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/meawallet/mtp/bn; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/meawallet/mtp/bm; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/meawallet/mtp/bl; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    .line 107
    new-instance v0, Lcom/meawallet/mtp/fs;

    invoke-direct {v0}, Lcom/meawallet/mtp/fs;-><init>()V

    invoke-static {p1}, Lcom/meawallet/mtp/fs;->c(Ljava/lang/Exception;)Lcom/meawallet/mtp/d;

    move-result-object p1

    return-object p1
.end method

.method final d()Lcom/meawallet/mtp/ep;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/meawallet/mtp/gm;->h:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    .line 2220
    iget-object v0, v0, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->a:Lcom/meawallet/mtp/ep;

    return-object v0
.end method

.method public final e()V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/meawallet/mtp/gm;->h:Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    if-eqz v0, :cond_0

    .line 146
    invoke-super {p0}, Lcom/meawallet/mtp/fv;->e()V

    return-void

    .line 144
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bn;

    const-string v1, "Initialize digitization parameters are null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bn;-><init>(Ljava/lang/String;)V

    throw v0
.end method
