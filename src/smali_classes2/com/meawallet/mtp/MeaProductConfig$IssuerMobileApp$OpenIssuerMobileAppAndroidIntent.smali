.class public Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp$OpenIssuerMobileAppAndroidIntent;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OpenIssuerMobileAppAndroidIntent"
.end annotation


# instance fields
.field final synthetic a:Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "action"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "packageName"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "extraTextValue"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;)V
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp$OpenIssuerMobileAppAndroidIntent;->a:Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp$OpenIssuerMobileAppAndroidIntent;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraTextValue()Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;
    .locals 4

    .line 207
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp$OpenIssuerMobileAppAndroidIntent;->d:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp$OpenIssuerMobileAppAndroidIntent;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/meawallet/mtp/e;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 211
    new-instance v2, Ljava/lang/String;

    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 212
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    const-class v3, Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/meawallet/mtp/MeaProductConfig$ExtraTextValue;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 215
    invoke-static {}, Lcom/meawallet/mtp/MeaProductConfig;->a()Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    return-object v1
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/meawallet/mtp/MeaProductConfig$IssuerMobileApp$OpenIssuerMobileAppAndroidIntent;->c:Ljava/lang/String;

    return-object v0
.end method
