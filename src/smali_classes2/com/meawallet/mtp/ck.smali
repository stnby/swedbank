.class Lcom/meawallet/mtp/ck;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/bz;


# instance fields
.field a:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transactionLimits"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "defaultMaxAmount"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/meawallet/mtp/ck;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/meawallet/mtp/bp;
    .locals 1

    .line 47
    sget-object v0, Lcom/meawallet/mtp/bp;->d:Lcom/meawallet/mtp/bp;

    return-object v0
.end method

.method public final a(Lcom/google/gson/Gson;)Ljava/lang/String;
    .locals 0

    .line 52
    invoke-virtual {p1, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method final b()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/meawallet/mtp/ck;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
