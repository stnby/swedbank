.class Lcom/meawallet/mtp/co;
.super Lcom/meawallet/mtp/cv;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "co"


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/meawallet/mtp/cv;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/meawallet/mtp/co;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .line 166
    invoke-static {}, Lcom/meawallet/mtp/aq;->c()V

    .line 168
    invoke-static {}, Lcom/meawallet/mtp/fp;->e()V

    return-void
.end method

.method public final a(Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;)V
    .locals 2

    const/4 v0, 0x1

    .line 131
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 133
    sget-object v0, Lcom/meawallet/mtp/co$1;->b:[I

    invoke-virtual {p1}, Lcom/mastercard/mchipengine/walletinterface/walletcommonenumeration/TransactionOutcome;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 2166
    invoke-static {}, Lcom/meawallet/mtp/aq;->c()V

    .line 2168
    invoke-static {}, Lcom/meawallet/mtp/fp;->e()V

    return-void

    :pswitch_0
    return-void

    .line 142
    :pswitch_1
    invoke-static {}, Lcom/meawallet/mtp/aq;->c()V

    .line 144
    invoke-static {}, Lcom/meawallet/mtp/fp;->j()V

    return-void

    .line 1166
    :pswitch_2
    invoke-static {}, Lcom/meawallet/mtp/aq;->c()V

    .line 1168
    invoke-static {}, Lcom/meawallet/mtp/fp;->e()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTimeOfLastSuccessfulCdCvm()J
    .locals 4

    .line 96
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v0

    const-wide/16 v1, -0x1

    if-nez v0, :cond_0

    return-wide v1

    .line 103
    :cond_0
    sget-object v3, Lcom/meawallet/mtp/co$1;->a:[I

    invoke-virtual {v0}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    return-wide v1

    .line 110
    :pswitch_0
    invoke-static {}, Lcom/meawallet/mtp/fp;->i()J

    move-result-wide v0

    return-wide v0

    .line 107
    :pswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isCdCvmBlocked()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCdCvmEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isCdCvmSuccessful(Lcom/mastercard/mpsdk/componentinterface/Card;)Z
    .locals 10

    .line 33
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 36
    sget-object p1, Lcom/meawallet/mtp/co;->a:Ljava/lang/String;

    const/16 v1, 0x1f5

    const-string v2, "Saved CD CVM type is null."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p1, v1, v2, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return v0

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/meawallet/mtp/co;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/meawallet/mtp/ar;->f(Landroid/content/Context;)Z

    move-result v1

    .line 43
    invoke-static {}, Lcom/meawallet/mtp/fp;->f()Z

    move-result v2

    const/4 v3, 0x2

    .line 45
    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v4, v6

    .line 47
    sget-object v4, Lcom/meawallet/mtp/co$1;->a:[I

    invoke-virtual {p1}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result p1

    aget p1, v4, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 66
    :pswitch_0
    invoke-static {}, Lcom/meawallet/mtp/br;->b()Z

    move-result p1

    .line 67
    invoke-static {}, Lcom/meawallet/mtp/fp;->c()Z

    move-result v4

    .line 68
    invoke-static {}, Lcom/meawallet/mtp/fp;->d()Z

    move-result v5

    const-string v7, "fingerprint_fragment_key"

    .line 69
    invoke-static {v7, v0}, Lcom/meawallet/mtp/br;->a(Ljava/lang/String;Z)Z

    move-result v7

    const/4 v8, 0x4

    .line 71
    new-array v8, v8, [Ljava/lang/Object;

    .line 74
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v3

    const/4 v3, 0x3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v3

    if-nez p1, :cond_3

    if-eqz v7, :cond_3

    if-eqz v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v4, :cond_3

    if-eqz v5, :cond_3

    goto :goto_0

    :pswitch_1
    const-string p1, "device_unlock_key"

    .line 51
    invoke-static {p1, v0}, Lcom/meawallet/mtp/br;->a(Ljava/lang/String;Z)Z

    move-result p1

    .line 53
    invoke-static {}, Lcom/meawallet/mtp/fp;->b()Z

    move-result v4

    .line 55
    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v6

    if-eqz p1, :cond_3

    if-eqz v1, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    if-nez v4, :cond_3

    :goto_0
    const/4 p1, 0x1

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x0

    .line 87
    :goto_2
    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
