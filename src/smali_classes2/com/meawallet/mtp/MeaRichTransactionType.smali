.class public final enum Lcom/meawallet/mtp/MeaRichTransactionType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/MeaRichTransactionType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CASH:Lcom/meawallet/mtp/MeaRichTransactionType;

.field public static final enum PURCHASE:Lcom/meawallet/mtp/MeaRichTransactionType;

.field public static final enum PURCHASE_WITH_CASHBACK:Lcom/meawallet/mtp/MeaRichTransactionType;

.field public static final enum REFUND:Lcom/meawallet/mtp/MeaRichTransactionType;

.field public static final enum TRANSIT:Lcom/meawallet/mtp/MeaRichTransactionType;

.field public static final enum UNKNOWN:Lcom/meawallet/mtp/MeaRichTransactionType;

.field private static final synthetic a:[Lcom/meawallet/mtp/MeaRichTransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 7
    new-instance v0, Lcom/meawallet/mtp/MeaRichTransactionType;

    const-string v1, "PURCHASE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/MeaRichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->PURCHASE:Lcom/meawallet/mtp/MeaRichTransactionType;

    .line 8
    new-instance v0, Lcom/meawallet/mtp/MeaRichTransactionType;

    const-string v1, "REFUND"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/MeaRichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->REFUND:Lcom/meawallet/mtp/MeaRichTransactionType;

    .line 9
    new-instance v0, Lcom/meawallet/mtp/MeaRichTransactionType;

    const-string v1, "CASH"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/meawallet/mtp/MeaRichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->CASH:Lcom/meawallet/mtp/MeaRichTransactionType;

    .line 10
    new-instance v0, Lcom/meawallet/mtp/MeaRichTransactionType;

    const-string v1, "TRANSIT"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/meawallet/mtp/MeaRichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->TRANSIT:Lcom/meawallet/mtp/MeaRichTransactionType;

    .line 11
    new-instance v0, Lcom/meawallet/mtp/MeaRichTransactionType;

    const-string v1, "PURCHASE_WITH_CASHBACK"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/meawallet/mtp/MeaRichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->PURCHASE_WITH_CASHBACK:Lcom/meawallet/mtp/MeaRichTransactionType;

    .line 12
    new-instance v0, Lcom/meawallet/mtp/MeaRichTransactionType;

    const-string v1, "UNKNOWN"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/meawallet/mtp/MeaRichTransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->UNKNOWN:Lcom/meawallet/mtp/MeaRichTransactionType;

    const/4 v0, 0x6

    .line 6
    new-array v0, v0, [Lcom/meawallet/mtp/MeaRichTransactionType;

    sget-object v1, Lcom/meawallet/mtp/MeaRichTransactionType;->PURCHASE:Lcom/meawallet/mtp/MeaRichTransactionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/MeaRichTransactionType;->REFUND:Lcom/meawallet/mtp/MeaRichTransactionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/meawallet/mtp/MeaRichTransactionType;->CASH:Lcom/meawallet/mtp/MeaRichTransactionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/meawallet/mtp/MeaRichTransactionType;->TRANSIT:Lcom/meawallet/mtp/MeaRichTransactionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/meawallet/mtp/MeaRichTransactionType;->PURCHASE_WITH_CASHBACK:Lcom/meawallet/mtp/MeaRichTransactionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/meawallet/mtp/MeaRichTransactionType;->UNKNOWN:Lcom/meawallet/mtp/MeaRichTransactionType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->a:[Lcom/meawallet/mtp/MeaRichTransactionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/MeaRichTransactionType;
    .locals 1

    .line 6
    const-class v0, Lcom/meawallet/mtp/MeaRichTransactionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/MeaRichTransactionType;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/MeaRichTransactionType;
    .locals 1

    .line 6
    sget-object v0, Lcom/meawallet/mtp/MeaRichTransactionType;->a:[Lcom/meawallet/mtp/MeaRichTransactionType;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/MeaRichTransactionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/MeaRichTransactionType;

    return-object v0
.end method
