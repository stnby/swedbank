.class public Lcom/meawallet/mtp/JobSchedulerDeleteCards;
.super Landroid/app/job/JobService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "JobSchedulerDeleteCards"


# instance fields
.field private b:Lcom/meawallet/mtp/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/meawallet/mtp/JobSchedulerDeleteCards;->b:Lcom/meawallet/mtp/fm;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .line 9
    sget-object v0, Lcom/meawallet/mtp/JobSchedulerDeleteCards;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    .line 18
    new-instance v0, Lcom/meawallet/mtp/JobSchedulerDeleteCards$1;

    invoke-virtual {p0}, Lcom/meawallet/mtp/JobSchedulerDeleteCards;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/meawallet/mtp/JobSchedulerDeleteCards$1;-><init>(Lcom/meawallet/mtp/JobSchedulerDeleteCards;Landroid/app/Application;Landroid/app/job/JobParameters;)V

    iput-object v0, p0, Lcom/meawallet/mtp/JobSchedulerDeleteCards;->b:Lcom/meawallet/mtp/fm;

    .line 28
    iget-object p1, p0, Lcom/meawallet/mtp/JobSchedulerDeleteCards;->b:Lcom/meawallet/mtp/fm;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/meawallet/mtp/fm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 p1, 0x1

    return p1
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .line 37
    iget-object p1, p0, Lcom/meawallet/mtp/JobSchedulerDeleteCards;->b:Lcom/meawallet/mtp/fm;

    if-eqz p1, :cond_0

    .line 38
    iget-object p1, p0, Lcom/meawallet/mtp/JobSchedulerDeleteCards;->b:Lcom/meawallet/mtp/fm;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/meawallet/mtp/fm;->cancel(Z)Z

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
