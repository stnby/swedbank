.class public interface abstract Lcom/meawallet/mtp/MeaContactlessTransactionData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract getAmount()Ljava/lang/Double;
.end method

.method public abstract getCurrency()Ljava/util/Currency;
.end method

.method public abstract getDate()Ljava/util/Date;
.end method

.method public abstract getDisplayableAmountAndCurrency()Ljava/lang/String;
.end method

.method public abstract getMerchantAndLocation()Ljava/lang/String;
.end method

.method public abstract getTransactionIdHexString()Ljava/lang/String;
.end method

.method public abstract getTransactionType()Lcom/meawallet/mtp/MeaRichTransactionType;
.end method
