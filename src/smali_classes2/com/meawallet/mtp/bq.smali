.class Lcom/meawallet/mtp/bq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "bq"

.field private static volatile b:Lcom/meawallet/mtp/bq;

.field private static d:Lcom/google/gson/Gson;


# instance fields
.field private c:Lcom/meawallet/mtp/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Lcom/meawallet/mtp/ai;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/meawallet/mtp/bq;->c:Lcom/meawallet/mtp/ai;

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .locals 5

    monitor-enter p0

    .line 131
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "LDE"

    .line 5073
    invoke-virtual {v0, p1, v1}, Lcom/meawallet/mtp/az;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 136
    monitor-exit p0

    return-object v2

    .line 139
    :cond_0
    :try_start_1
    invoke-virtual {v0, p1}, Lcom/meawallet/mtp/az;->a(Ljava/lang/String;)[B

    move-result-object p1

    if-eqz p1, :cond_4

    .line 146
    array-length v0, p1

    const/16 v1, 0x12e

    const/4 v3, 0x0

    if-gtz v0, :cond_1

    .line 147
    sget-object p1, Lcom/meawallet/mtp/bq;->a:Ljava/lang/String;

    const-string v0, "File does not contain any data."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, v1, v0, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    monitor-exit p0

    return-object v2

    .line 154
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/meawallet/mtp/bq;->c:Lcom/meawallet/mtp/ai;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/meawallet/mtp/bq;->c:Lcom/meawallet/mtp/ai;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/meawallet/mtp/ai;->b(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v2

    goto :goto_0

    .line 159
    :cond_2
    sget-object p1, Lcom/meawallet/mtp/bq;->a:Ljava/lang/String;

    const-string v0, "No crypto service"

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {p1, v1, v0, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    if-nez v2, :cond_3

    .line 163
    sget-object p1, Lcom/meawallet/mtp/bq;->a:Ljava/lang/String;

    const/16 v0, 0x1f5

    const-string v1, "Decrypted plaintext data is null"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v1, v3}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 166
    :cond_3
    monitor-exit p0

    return-object v2

    .line 143
    :cond_4
    :try_start_3
    new-instance p1, Lcom/meawallet/mtp/bv;

    const-string v0, "Failed to read file, ciphertext is null"

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/bv;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    .line 130
    monitor-exit p0

    throw p1
.end method

.method static declared-synchronized a(Lcom/meawallet/mtp/ai;Lcom/google/gson/Gson;)Lcom/meawallet/mtp/bq;
    .locals 2

    const-class v0, Lcom/meawallet/mtp/bq;

    monitor-enter v0

    .line 26
    :try_start_0
    sget-object v1, Lcom/meawallet/mtp/bq;->b:Lcom/meawallet/mtp/bq;

    if-nez v1, :cond_0

    .line 27
    new-instance v1, Lcom/meawallet/mtp/bq;

    invoke-direct {v1, p0}, Lcom/meawallet/mtp/bq;-><init>(Lcom/meawallet/mtp/ai;)V

    sput-object v1, Lcom/meawallet/mtp/bq;->b:Lcom/meawallet/mtp/bq;

    .line 30
    :cond_0
    sput-object p1, Lcom/meawallet/mtp/bq;->d:Lcom/google/gson/Gson;

    .line 32
    sget-object p0, Lcom/meawallet/mtp/bq;->b:Lcom/meawallet/mtp/bq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    .line 25
    monitor-exit v0

    throw p0
.end method


# virtual methods
.method final declared-synchronized a(Lcom/meawallet/mtp/bp;)Lcom/meawallet/mtp/bz;
    .locals 7

    monitor-enter p0

    const/4 v0, 0x1

    .line 46
    :try_start_0
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 1018
    iget-object v1, p1, Lcom/meawallet/mtp/bp;->f:Ljava/lang/String;

    .line 48
    invoke-direct {p0, v1}, Lcom/meawallet/mtp/bq;->a(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 50
    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->isEmpty()Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v4, :cond_0

    goto/16 :goto_4

    .line 57
    :cond_0
    :try_start_1
    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 59
    :try_start_2
    sget-object v1, Lcom/meawallet/mtp/bq$1;->a:[I

    invoke-virtual {p1}, Lcom/meawallet/mtp/bp;->ordinal()I

    move-result v5

    aget v1, v1, v5

    packed-switch v1, :pswitch_data_0

    .line 81
    new-instance v1, Ljava/lang/IllegalArgumentException;

    goto :goto_1

    .line 77
    :pswitch_0
    sget-object v1, Lcom/meawallet/mtp/bq;->d:Lcom/google/gson/Gson;

    const-class v5, Lcom/meawallet/mtp/cg;

    invoke-virtual {v1, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/bz;

    goto :goto_0

    .line 73
    :pswitch_1
    sget-object v1, Lcom/meawallet/mtp/bq;->d:Lcom/google/gson/Gson;

    const-class v5, Lcom/meawallet/mtp/ck;

    invoke-virtual {v1, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/bz;

    goto :goto_0

    .line 69
    :pswitch_2
    sget-object v1, Lcom/meawallet/mtp/bq;->d:Lcom/google/gson/Gson;

    const-class v5, Lcom/meawallet/mtp/bu;

    invoke-virtual {v1, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/bz;

    goto :goto_0

    .line 65
    :pswitch_3
    sget-object v1, Lcom/meawallet/mtp/bq;->d:Lcom/google/gson/Gson;

    const-class v5, Lcom/meawallet/mtp/cc;

    invoke-virtual {v1, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/bz;

    goto :goto_0

    .line 61
    :pswitch_4
    sget-object v1, Lcom/meawallet/mtp/bq;->d:Lcom/google/gson/Gson;

    const-class v5, Lcom/meawallet/mtp/ca;

    invoke-virtual {v1, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/meawallet/mtp/bz;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 84
    :goto_0
    :try_start_3
    invoke-virtual {v4}, Ljava/io/Reader;->close()V
    :try_end_3
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 89
    monitor-exit p0

    return-object v1

    .line 81
    :goto_1
    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-class v6, Lcom/meawallet/mtp/bq;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, " cannot deserialize "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, " file Json data."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v1

    goto :goto_2

    :catch_0
    move-exception v1

    move-object v3, v1

    .line 57
    :try_start_5
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_2
    if-eqz v3, :cond_1

    .line 84
    :try_start_6
    invoke-virtual {v4}, Ljava/io/Reader;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    :catch_1
    move-exception v4

    :try_start_7
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_1
    invoke-virtual {v4}, Ljava/io/Reader;->close()V

    :goto_3
    throw v1
    :try_end_7
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catch_2
    move-exception v1

    .line 85
    :try_start_8
    new-instance v3, Lcom/meawallet/mtp/bv;

    const-string v4, "%s cannot deserialize %s file Json data. Exception: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lcom/meawallet/mtp/bq;->a:Ljava/lang/String;

    aput-object v6, v5, v2

    aput-object p1, v5, v0

    const/4 p1, 0x2

    .line 86
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, p1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/meawallet/mtp/bv;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 52
    :cond_2
    :goto_4
    monitor-exit p0

    return-object v3

    :catchall_1
    move-exception p1

    .line 45
    monitor-exit p0

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method final declared-synchronized a(Lcom/meawallet/mtp/bz;)Z
    .locals 6

    monitor-enter p0

    const/4 v0, 0x1

    .line 99
    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/meawallet/mtp/bz;->a()Lcom/meawallet/mtp/bp;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 101
    invoke-interface {p1}, Lcom/meawallet/mtp/bz;->a()Lcom/meawallet/mtp/bp;

    move-result-object v0

    .line 2018
    iget-object v0, v0, Lcom/meawallet/mtp/bp;->f:Ljava/lang/String;

    .line 101
    sget-object v1, Lcom/meawallet/mtp/bq;->d:Lcom/google/gson/Gson;

    invoke-interface {p1, v1}, Lcom/meawallet/mtp/bz;->a(Lcom/google/gson/Gson;)Ljava/lang/String;

    move-result-object p1

    .line 2178
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v1

    .line 2180
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    const/4 v3, 0x0

    .line 2185
    iget-object v4, p0, Lcom/meawallet/mtp/bq;->c:Lcom/meawallet/mtp/ai;

    if-eqz v4, :cond_0

    .line 2187
    iget-object v2, p0, Lcom/meawallet/mtp/bq;->c:Lcom/meawallet/mtp/ai;

    invoke-static {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/meawallet/mtp/ai;->a(Lcom/mastercard/mpsdk/utils/bytes/ByteArray;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object p1

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v3

    goto :goto_0

    .line 2189
    :cond_0
    sget-object p1, Lcom/meawallet/mtp/bq;->a:Ljava/lang/String;

    const/16 v4, 0x12e

    const-string v5, "No crypto service"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v4, v5, v2}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 2192
    :goto_0
    invoke-virtual {v1, v0, v3}, Lcom/meawallet/mtp/az;->a(Ljava/lang/String;[B)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    .line 98
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized b(Lcom/meawallet/mtp/bp;)Z
    .locals 2

    monitor-enter p0

    .line 3018
    :try_start_0
    iget-object p1, p1, Lcom/meawallet/mtp/bp;->f:Ljava/lang/String;

    .line 3202
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "LDE"

    .line 3254
    invoke-virtual {v0, p1, v1}, Lcom/meawallet/mtp/az;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    .line 111
    monitor-exit p0

    throw p1
.end method

.method final declared-synchronized c(Lcom/meawallet/mtp/bp;)Z
    .locals 2

    monitor-enter p0

    .line 120
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    .line 4018
    iget-object p1, p1, Lcom/meawallet/mtp/bp;->f:Ljava/lang/String;

    const-string v1, "LDE"

    .line 4073
    invoke-virtual {v0, p1, v1}, Lcom/meawallet/mtp/az;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    .line 119
    monitor-exit p0

    throw p1
.end method
