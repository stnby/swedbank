.class Lcom/meawallet/mtp/fj;
.super Lcom/meawallet/mtp/g;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/g<",
        "Lcom/meawallet/mtp/p;",
        ">;"
    }
.end annotation


# instance fields
.field protected j:Ljava/lang/String;

.field protected k:Ljava/lang/String;

.field protected final l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

.field protected m:Lcom/meawallet/mtp/o;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V
    .locals 10

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p10

    .line 52
    invoke-direct/range {v0 .. v8}, Lcom/meawallet/mtp/g;-><init>(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/http/HttpManager;Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;Lcom/meawallet/mtp/fi;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/meawallet/mtp/ad;Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;Lcom/mastercard/mpsdk/componentinterface/remotemanagement/CommunicationRetryParametersProvider;)V

    move-object/from16 v0, p8

    .line 61
    iput-object v0, v9, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    .line 62
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/meawallet/mtp/fj;->k:Ljava/lang/String;

    move-object/from16 v0, p9

    .line 63
    iput-object v0, v9, Lcom/meawallet/mtp/fj;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    .line 65
    new-instance v0, Lcom/meawallet/mtp/o;

    .line 1085
    iget-object v1, v9, Lcom/meawallet/mtp/g;->b:Ljava/lang/String;

    .line 65
    iget-object v2, v9, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    iget-object v3, v9, Lcom/meawallet/mtp/fj;->k:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/meawallet/mtp/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v9, Lcom/meawallet/mtp/fj;->m:Lcom/meawallet/mtp/o;

    return-void
.end method


# virtual methods
.method final bridge synthetic a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/s;
    .locals 0

    .line 9070
    invoke-static {p1, p2}, Lcom/meawallet/mtp/p;->a(Lcom/google/gson/Gson;[B)Lcom/meawallet/mtp/p;

    move-result-object p1

    return-object p1
.end method

.method protected a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    const-string v0, "CANCELED"

    .line 91
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Change Pin request is cancelled or "

    .line 92
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    :cond_0
    move-object v4, p3

    .line 95
    iget-object p3, p0, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    if-eqz p3, :cond_1

    .line 96
    iget-object v0, p0, Lcom/meawallet/mtp/fj;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/meawallet/mtp/ad;->b(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 104
    :cond_1
    iget-object p3, p0, Lcom/meawallet/mtp/fj;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {p3, p1, p2, v4, p4}, Lcom/meawallet/mtp/ad;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public a(Lcom/google/gson/Gson;)V
    .locals 4

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/fj;->g:Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/CommunicationParametersProvider;->getEncryptedMobileKeys()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;

    move-result-object v0

    .line 140
    invoke-static {v0}, Lcom/meawallet/mtp/fj;->a(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    sget-object p1, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v0, "MOBILE_KEYS_MISSING"

    const-string v1, "Failed to execute set pin command."

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/meawallet/mtp/fj;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 150
    :cond_0
    sget-object v1, Lcom/meawallet/mtp/ag;->b:Lcom/meawallet/mtp/ag;

    .line 2317
    iput-object v1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 152
    iget-object v1, p0, Lcom/meawallet/mtp/fj;->f:Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;

    .line 153
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedMobileKeys;->getEncryptedDek()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;

    move-result-object v0

    iget-object v2, p0, Lcom/meawallet/mtp/fj;->e:Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;

    iget-object v3, p0, Lcom/meawallet/mtp/fj;->l:Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;

    .line 155
    invoke-interface {v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/PinDataProvider;->getEncryptedNewPin()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;

    move-result-object v3

    .line 152
    invoke-interface {v1, v0, v2, v3}, Lcom/mastercard/mpsdk/componentinterface/crypto/RemoteManagementCrypto;->encryptPinBlockWithDek(Lcom/mastercard/mpsdk/componentinterface/crypto/keys/RMKekEncryptedData;Lcom/mastercard/mpsdk/componentinterface/crypto/WalletIdentificationDataProvider;Lcom/mastercard/mpsdk/componentinterface/crypto/keys/WalletDekEncryptedData;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/meawallet/mtp/fj;->m:Lcom/meawallet/mtp/o;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/DekEncryptedData;->getEncryptedData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of([B)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    .line 3033
    iput-object v0, v1, Lcom/meawallet/mtp/o;->b:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    .line 159
    iget-object v0, p0, Lcom/meawallet/mtp/fj;->m:Lcom/meawallet/mtp/o;

    .line 3037
    invoke-static {}, Lcom/meawallet/mtp/JsonUtil;->a()Lcom/google/gson/Gson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/fj;->a(Ljava/lang/String;)Lcom/meawallet/mtp/y;

    move-result-object v0

    .line 4034
    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-virtual {p0, p1, v0}, Lcom/meawallet/mtp/fj;->a(Lcom/google/gson/Gson;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/http/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 182
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_INVALID_INPUTS"

    .line 184
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 182
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/fj;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception p1

    .line 176
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    .line 177
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/remotemanagement/RemoteManagementException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 176
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/fj;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_2
    move-exception p1

    .line 170
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_CRYPTO_OPERATION_FAILED"

    const-string v2, "Failed to execute set PIN command"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/fj;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    :catch_3
    move-exception p1

    .line 163
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    const-string v1, "SDK_COMMUNICATION_ERROR"

    const-string v2, "Failed to execute Set PIN HTTP request"

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/meawallet/mtp/fj;->a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method final a(Lcom/meawallet/mtp/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 122
    iput-object p2, p0, Lcom/meawallet/mtp/fj;->c:Ljava/lang/String;

    .line 123
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 1317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 124
    iput-object p1, p0, Lcom/meawallet/mtp/fj;->h:Lcom/meawallet/mtp/ae;

    .line 126
    iget-object p1, p0, Lcom/meawallet/mtp/fj;->h:Lcom/meawallet/mtp/ae;

    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    .line 127
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/meawallet/mtp/fj;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method final synthetic a(Lcom/meawallet/mtp/s;)V
    .locals 3

    .line 26
    check-cast p1, Lcom/meawallet/mtp/p;

    .line 7020
    iget-object v0, p1, Lcom/meawallet/mtp/p;->a:Ljava/lang/String;

    const-string v1, "SUCCESS"

    .line 6074
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6075
    sget-object p1, Lcom/meawallet/mtp/ag;->c:Lcom/meawallet/mtp/ag;

    .line 7317
    iput-object p1, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 6076
    sget-object p1, Lcom/meawallet/mtp/ah;->b:Lcom/meawallet/mtp/ah;

    iput-object p1, p0, Lcom/meawallet/mtp/fj;->i:Lcom/meawallet/mtp/ah;

    .line 6078
    invoke-virtual {p0}, Lcom/meawallet/mtp/fj;->a_()V

    return-void

    .line 8020
    :cond_0
    iget-object v0, p1, Lcom/meawallet/mtp/p;->a:Ljava/lang/String;

    const-string v1, "INCORRECT_PIN"

    .line 6079
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6080
    sget-object v0, Lcom/meawallet/mtp/ag;->e:Lcom/meawallet/mtp/ag;

    .line 8317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 6081
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/fj;->h:Lcom/meawallet/mtp/ae;

    .line 9024
    iget p1, p1, Lcom/meawallet/mtp/p;->b:I

    const-string v0, "INCORRECT_MOBILE_PIN"

    const-string v1, "Incorrect current PIN supplied to set/change PIN request"

    const/4 v2, 0x0

    .line 6083
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/meawallet/mtp/fj;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_1
    return-void
.end method

.method protected a_()V
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/meawallet/mtp/fj;->d:Lcom/meawallet/mtp/ad;

    invoke-interface {v0}, Lcom/meawallet/mtp/ad;->c()V

    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/fj;->d:Lcom/meawallet/mtp/ad;

    iget-object v1, p0, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/ad;->c(Ljava/lang/String;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 213
    instance-of v0, p1, Lcom/meawallet/mtp/fj;

    if-eqz v0, :cond_0

    .line 214
    check-cast p1, Lcom/meawallet/mtp/fj;

    iget-object p1, p1, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/meawallet/mtp/fj;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method final i()Ljava/lang/String;
    .locals 1

    const-string v0, "/paymentapp/1/0/changeMobilePin"

    return-object v0
.end method

.method public final l()V
    .locals 4

    .line 4230
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 190
    sget-object v1, Lcom/meawallet/mtp/ag;->i:Lcom/meawallet/mtp/ag;

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "CANCELED"

    const-string v1, "Too many commands already in queue."

    .line 191
    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/meawallet/mtp/fj;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 5230
    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    .line 193
    sget-object v1, Lcom/meawallet/mtp/ag;->h:Lcom/meawallet/mtp/ag;

    if-ne v0, v1, :cond_1

    const-string v0, "CANCELED"

    const-string v1, "Duplicate Request"

    .line 194
    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/meawallet/mtp/fj;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void

    .line 197
    :cond_1
    sget-object v0, Lcom/meawallet/mtp/ag;->f:Lcom/meawallet/mtp/ag;

    .line 5317
    iput-object v0, p0, Lcom/meawallet/mtp/g;->a:Lcom/meawallet/mtp/ag;

    const-string v0, "CANCELED"

    const-string v1, "Could not get a valid session from CMS-D"

    .line 198
    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/meawallet/mtp/fj;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 200
    sget-object v0, Lcom/meawallet/mtp/ae;->d:Lcom/meawallet/mtp/ae;

    iput-object v0, p0, Lcom/meawallet/mtp/fj;->h:Lcom/meawallet/mtp/ae;

    return-void
.end method

.method public final m()Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;
    .locals 1

    .line 205
    sget-object v0, Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;->POST:Lcom/mastercard/mpsdk/componentinterface/http/HttpMethod;

    return-object v0
.end method
