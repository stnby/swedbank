.class Lcom/meawallet/mtp/ca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/bz;


# static fields
.field private static final g:Lcom/meawallet/mtp/bp;


# instance fields
.field a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "payment_app_instance_id"
    .end annotation
.end field

.field b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "remote_url"
    .end annotation
.end field

.field c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_fgp"
    .end annotation
.end field

.field d:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lde_crypto_version"
    .end annotation
.end field

.field e:Lcom/mastercard/mpsdk/utils/bytes/ByteArray;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "wallet_session_id"
    .end annotation
.end field

.field f:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "wallet_session_request_counter"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    sget-object v0, Lcom/meawallet/mtp/bp;->a:Lcom/meawallet/mtp/bp;

    sput-object v0, Lcom/meawallet/mtp/ca;->g:Lcom/meawallet/mtp/bp;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/meawallet/mtp/bp;
    .locals 1

    .line 53
    sget-object v0, Lcom/meawallet/mtp/ca;->g:Lcom/meawallet/mtp/bp;

    return-object v0
.end method

.method public final a(Lcom/google/gson/Gson;)Ljava/lang/String;
    .locals 0

    .line 109
    invoke-virtual {p1, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
