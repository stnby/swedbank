.class Lcom/meawallet/mtp/bi;
.super Lcom/meawallet/mtp/MeaCheckedException;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "bi"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/Exception;)V
    .locals 4

    .line 23
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    .line 1030
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 1032
    instance-of v1, p1, Ljava/net/UnknownHostException;

    const/16 v2, 0xc9

    const/16 v3, 0x1f5

    if-eqz v1, :cond_0

    const/16 v2, 0xcc

    goto :goto_1

    .line 1035
    :cond_0
    instance-of v1, p1, Ljava/net/SocketTimeoutException;

    if-eqz v1, :cond_1

    const/16 v2, 0xca

    goto :goto_1

    .line 1038
    :cond_1
    instance-of v1, p1, Ljava/net/ConnectException;

    if-eqz v1, :cond_2

    .line 1040
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    const-string v1, "network is unreachable"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_a

    const/16 v2, 0x6d

    goto :goto_1

    .line 1047
    :cond_2
    instance-of v1, p1, Ljava/net/NoRouteToHostException;

    if-eqz v1, :cond_3

    goto :goto_1

    .line 1050
    :cond_3
    instance-of v1, p1, Ljava/net/SocketException;

    if-eqz v1, :cond_4

    goto :goto_1

    .line 1053
    :cond_4
    instance-of v1, p1, Ljava/security/KeyManagementException;

    if-eqz v1, :cond_6

    :cond_5
    :goto_0
    const/16 v2, 0x1f5

    goto :goto_1

    .line 1056
    :cond_6
    instance-of v1, p1, Ljava/security/NoSuchAlgorithmException;

    if-eqz v1, :cond_7

    goto :goto_0

    .line 1059
    :cond_7
    instance-of v1, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v1, :cond_8

    const/16 v2, 0xcf

    goto :goto_1

    .line 1062
    :cond_8
    instance-of v1, p1, Ljava/io/IOException;

    if-eqz v1, :cond_9

    goto :goto_0

    .line 1065
    :cond_9
    instance-of v1, p1, Lcom/meawallet/mtp/bk;

    if-eqz v1, :cond_5

    .line 1066
    check-cast p1, Lcom/meawallet/mtp/bk;

    invoke-virtual {p1}, Lcom/meawallet/mtp/bk;->getMeaError()Lcom/meawallet/mtp/MeaError;

    move-result-object p1

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v2

    .line 23
    :cond_a
    :goto_1
    invoke-direct {p0, v0, v2}, Lcom/meawallet/mtp/MeaCheckedException;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/MeaCheckedException;-><init>(Ljava/lang/String;)V

    return-void
.end method
