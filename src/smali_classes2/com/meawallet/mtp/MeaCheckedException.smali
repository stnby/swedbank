.class public Lcom/meawallet/mtp/MeaCheckedException;
.super Ljava/lang/Exception;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/cz;


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/16 p1, 0x1f5

    .line 22
    iput p1, p0, Lcom/meawallet/mtp/MeaCheckedException;->a:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 34
    invoke-static {p2}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 p2, 0x1f5

    :goto_0
    iput p2, p0, Lcom/meawallet/mtp/MeaCheckedException;->a:I

    return-void
.end method


# virtual methods
.method public getMeaError()Lcom/meawallet/mtp/MeaError;
    .locals 3

    .line 44
    new-instance v0, Lcom/meawallet/mtp/cy;

    iget v1, p0, Lcom/meawallet/mtp/MeaCheckedException;->a:I

    invoke-virtual {p0}, Lcom/meawallet/mtp/MeaCheckedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    return-object v0
.end method
