.class final Lcom/meawallet/mtp/as$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/meawallet/mtp/as;->a(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)Lcom/mastercard/mpsdk/componentinterface/crypto/keys/LocalDekEncryptedData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/meawallet/mtp/as$2;->a:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDp()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    .line 103
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/meawallet/mtp/as$2;->a:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->dp:Ljava/lang/String;

    .line 104
    invoke-static {v1}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getDq()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    .line 108
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/meawallet/mtp/as$2;->a:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->dq:Ljava/lang/String;

    .line 109
    invoke-static {v1}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getP()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    .line 93
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/meawallet/mtp/as$2;->a:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->p:Ljava/lang/String;

    .line 94
    invoke-static {v1}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getQ()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    .line 98
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/meawallet/mtp/as$2;->a:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->q:Ljava/lang/String;

    .line 99
    invoke-static {v1}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method

.method public final getU()Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;
    .locals 2

    .line 88
    new-instance v0, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;

    iget-object v1, p0, Lcom/meawallet/mtp/as$2;->a:Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;

    iget-object v1, v1, Lcom/mastercard/mpsdk/card/profile/IccPrivateKeyCrtComponentsJson;->u:Ljava/lang/String;

    .line 89
    invoke-static {v1}, Lcom/meawallet/mtp/h;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mastercard/mpsdk/componentinterface/crypto/keys/IccKekEncryptedData;-><init>([B)V

    return-object v0
.end method
