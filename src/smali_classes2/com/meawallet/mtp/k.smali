.class Lcom/meawallet/mtp/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/ActiveCardProvider;


# static fields
.field private static final b:Ljava/lang/String; = "k"


# instance fields
.field a:Lcom/mastercard/mpsdk/componentinterface/Card;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    return-void
.end method

.method static a()Ljava/lang/String;
    .locals 2

    .line 113
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_CONTACTLESS"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;)V"
        }
    .end annotation

    .line 258
    invoke-static {}, Lcom/meawallet/mtp/k;->a()Ljava/lang/String;

    move-result-object v0

    .line 259
    invoke-static {}, Lcom/meawallet/mtp/k;->b()Ljava/lang/String;

    move-result-object v1

    .line 261
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/meawallet/mtp/MeaCard;

    if-eqz v2, :cond_0

    .line 263
    invoke-interface {v2}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 265
    invoke-interface {v2}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    .line 266
    invoke-static {v4}, Lcom/meawallet/mtp/k;->c(Ljava/lang/String;)V

    .line 269
    :cond_1
    invoke-interface {v2}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 270
    invoke-static {v4}, Lcom/meawallet/mtp/k;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method static a(Ljava/lang/String;)Z
    .locals 4

    .line 40
    invoke-static {}, Lcom/meawallet/mtp/k;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 42
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_1

    return v2

    :cond_1
    return v1
.end method

.method static b()Ljava/lang/String;
    .locals 2

    .line 124
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_REMOTE"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/lang/String;)Z
    .locals 1

    .line 56
    invoke-static {}, Lcom/meawallet/mtp/k;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/meawallet/mtp/k;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static c(Ljava/lang/String;)V
    .locals 2

    .line 66
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_CONTACTLESS"

    invoke-virtual {v0, v1, p0}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method static d(Ljava/lang/String;)V
    .locals 2

    .line 76
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_CONTACTLESS"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_CONTACTLESS"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 79
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object p0

    const-string v0, "CARD_DEFAULT_CONTACTLESS"

    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method static e(Ljava/lang/String;)V
    .locals 2

    .line 89
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_REMOTE"

    invoke-virtual {v0, v1, p0}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method static f(Ljava/lang/String;)V
    .locals 2

    .line 98
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_REMOTE"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    const-string v1, "CARD_DEFAULT_REMOTE"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/dx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 101
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object p0

    const-string v0, "CARD_DEFAULT_REMOTE"

    invoke-virtual {p0, v0}, Lcom/meawallet/mtp/dx;->h(Ljava/lang/String;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method final a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    .line 134
    iget-object p1, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    return-object p1
.end method

.method final a(Ljava/lang/String;Lcom/mastercard/mpsdk/componentinterface/CardManager;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 142
    iput-object p1, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    return-void

    .line 148
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 149
    :cond_1
    invoke-interface {p2, p1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception p1

    .line 152
    sget-object p2, Lcom/meawallet/mtp/k;->b:Ljava/lang/String;

    const-string v0, "Failed to set selected card."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2, p1, v0, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public getActiveCard(Lcom/mastercard/mpsdk/componentinterface/PaymentContext;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/mastercard/mpsdk/componentinterface/Card;
    .locals 6

    .line 1166
    iget-object v0, p0, Lcom/meawallet/mtp/k;->a:Lcom/mastercard/mpsdk/componentinterface/Card;

    if-eqz v0, :cond_0

    .line 185
    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->ACTIVATED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardState()Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    const-string v1, ""

    .line 197
    sget-object v2, Lcom/meawallet/mtp/k$1;->a:[I

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 204
    :pswitch_0
    invoke-static {}, Lcom/meawallet/mtp/k;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 200
    :pswitch_1
    invoke-static {}, Lcom/meawallet/mtp/k;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const/4 v2, 0x2

    .line 208
    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v5, 0x1

    aput-object v1, v3, v5

    .line 210
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v0

    .line 216
    :cond_1
    invoke-interface {p2, v1}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p2

    if-nez p2, :cond_2

    return-object v0

    .line 224
    :cond_2
    sget-object v1, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->ACTIVATED:Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardState()Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/mastercard/mpsdk/componentinterface/database/state/CardState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return-object v0

    :cond_3
    const/4 v1, 0x3

    .line 230
    new-array v1, v1, [Ljava/lang/Object;

    .line 231
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/Card;->getCardId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/Card;->isContactlessSupported()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v5

    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/Card;->isDsrpSupported()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 233
    sget-object v1, Lcom/meawallet/mtp/k$1;->a:[I

    invoke-virtual {p1}, Lcom/mastercard/mpsdk/componentinterface/PaymentContext;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    .line 243
    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/meawallet/mtp/k;->a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    return-object p1

    .line 239
    :pswitch_3
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/Card;->isDsrpSupported()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, p2}, Lcom/meawallet/mtp/k;->a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1

    return-object p1

    .line 235
    :pswitch_4
    invoke-interface {p2}, Lcom/mastercard/mpsdk/componentinterface/Card;->isContactlessSupported()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, p2}, Lcom/meawallet/mtp/k;->a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object p1
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 250
    :cond_4
    :goto_1
    new-array p2, v5, [Ljava/lang/Object;

    aput-object p1, p2, v4

    return-object v0

    :catch_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
