.class Lcom/meawallet/mtp/ct;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/meawallet/mtp/MeaCard;


# static fields
.field private static final a:Ljava/lang/String; = "ct"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/meawallet/mtp/bt;)V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088
    iget-object v0, p1, Lcom/meawallet/mtp/bt;->d:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/meawallet/mtp/ct;->b:Ljava/lang/String;

    .line 1114
    iget-object v0, p1, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    if-eqz v0, :cond_0

    .line 2114
    iget-object p1, p1, Lcom/meawallet/mtp/bt;->i:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    .line 32
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->getValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/meawallet/mtp/ct;->c:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/meawallet/mtp/ct;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/meawallet/mtp/ct;->b:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/meawallet/mtp/ct;->c:Ljava/lang/String;

    return-void
.end method

.method private a()Ljava/lang/Boolean;
    .locals 2

    const/4 v0, 0x0

    .line 169
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/MeaCard;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method private a(Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 4

    .line 310
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->b()Landroid/content/Context;

    move-result-object v0

    .line 312
    invoke-static {p1}, Lcom/meawallet/mtp/dw;->b(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 313
    invoke-static {p1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 314
    invoke-static {v0, p1}, Lcom/meawallet/mtp/dw;->c(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 315
    invoke-static {v0, p1}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 316
    invoke-static {v0, p1}, Lcom/meawallet/mtp/dw;->d(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 318
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v1

    if-nez v1, :cond_0

    .line 321
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x1f5

    const-string v2, "Saved CVM type is null."

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 326
    :cond_0
    sget-object v2, Lcom/meawallet/mtp/ct$1;->a:[I

    invoke-virtual {v1}, Lcom/meawallet/mtp/CdCvmType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 356
    :pswitch_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isWalletPinSet()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x38f

    .line 357
    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void

    .line 347
    :pswitch_1
    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->isPinSet()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x3f4

    .line 348
    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void

    .line 330
    :pswitch_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_2

    .line 332
    invoke-static {v0, v1, p1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 333
    invoke-static {p1}, Lcom/meawallet/mtp/dw;->c(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x389

    .line 339
    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void

    .line 365
    :cond_3
    :goto_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-void

    :catch_0
    move-exception v0

    .line 368
    sget-object v1, Lcom/meawallet/mtp/ct;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x65

    .line 370
    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b()Ljava/lang/Boolean;
    .locals 2

    const/4 v0, 0x0

    .line 196
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/cu;->b(Lcom/meawallet/mtp/MeaCard;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method private c()Ljava/lang/Boolean;
    .locals 2

    const/4 v0, 0x0

    .line 245
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->f()Lcom/meawallet/mtp/k;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/k;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method private d()Ljava/lang/Boolean;
    .locals 2

    const/4 v0, 0x0

    .line 272
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 274
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->f()Lcom/meawallet/mtp/k;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/k;->b(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method private static e()V
    .locals 2

    const/4 v0, 0x0

    .line 390
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->f()Lcom/meawallet/mtp/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/meawallet/mtp/k;->a(Lcom/mastercard/mpsdk/componentinterface/Card;)Lcom/mastercard/mpsdk/componentinterface/Card;

    :cond_0
    return-void
.end method

.method private f()V
    .locals 6

    const/4 v0, 0x0

    .line 412
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    .line 416
    invoke-static {}, Lcom/meawallet/mtp/dd;->b()Lcom/meawallet/mtp/l;

    move-result-object v0

    invoke-static {}, Lcom/meawallet/mtp/dd;->a()Lcom/meawallet/mtp/dm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/meawallet/mtp/l;->d(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v0

    .line 418
    invoke-static {v0}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    sget-object v1, Lcom/meawallet/mtp/ct;->a:Ljava/lang/String;

    const/16 v2, 0x1f5

    const-string v3, "handleSetAsDefaultForContactlessPayments() failed "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 424
    :cond_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/k;->c(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 1

    const/4 v0, 0x0

    .line 444
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/k;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 6

    const/4 v0, 0x0

    .line 466
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 468
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    .line 470
    invoke-static {}, Lcom/meawallet/mtp/dd;->b()Lcom/meawallet/mtp/l;

    move-result-object v0

    invoke-static {}, Lcom/meawallet/mtp/dd;->a()Lcom/meawallet/mtp/dm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/meawallet/mtp/l;->d(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v0

    .line 472
    invoke-static {v0}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 473
    sget-object v1, Lcom/meawallet/mtp/ct;->a:Ljava/lang/String;

    const/16 v2, 0x1f5

    const-string v3, "handleSetAsDefaultForRemotePayments() failed "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 478
    :cond_0
    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/k;->e(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private i()V
    .locals 1

    const/4 v0, 0x0

    .line 498
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/meawallet/mtp/k;->f(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public authenticateWithCardPin(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;)V
    .locals 5

    const/16 v0, 0x65

    .line 509
    :try_start_0
    invoke-static {p2}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 511
    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->isPinSet()Z

    move-result v1

    const/16 v2, 0x3f4

    if-nez v1, :cond_0

    .line 512
    invoke-static {p2, v2}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void

    .line 517
    :cond_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    move-result-object v1

    .line 4750
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string p1, "Empty PIN."

    const/16 v1, 0x1f9

    .line 4751
    invoke-static {p1, v1, p2}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    .line 4757
    :cond_1
    sget-object v3, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string p1, "CVM type is not Card PIN."

    const/16 v1, 0x389

    .line 4758
    invoke-static {p1, v1, p2}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;ILcom/meawallet/mtp/MeaCoreListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    .line 4764
    :cond_2
    :try_start_1
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->isPinSet()Z

    move-result v3

    if-nez v3, :cond_3

    .line 4765
    new-instance p1, Lcom/meawallet/mtp/cy;

    invoke-direct {p1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p2, p1}, Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;->onFailure(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    .line 4775
    :cond_3
    :try_start_2
    new-instance v2, Lcom/meawallet/mtp/c;

    invoke-direct {v2}, Lcom/meawallet/mtp/c;-><init>()V

    new-instance v3, Lcom/meawallet/mtp/dd$4;

    invoke-direct {v3, v1, p1, p0, p2}, Lcom/meawallet/mtp/dd$4;-><init>(Lcom/meawallet/mtp/dd;Ljava/lang/String;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;)V

    invoke-virtual {v2, v3}, Lcom/meawallet/mtp/c;->a(Lcom/meawallet/mtp/fl;)V

    goto :goto_0

    .line 4770
    :catch_0
    new-instance p1, Lcom/meawallet/mtp/cy;

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p2, p1}, Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;->onFailure(Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :cond_4
    :goto_0
    return-void

    .line 520
    :catch_1
    invoke-static {p2, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;I)V

    return-void
.end method

.method public changePin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x2

    .line 218
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 220
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    new-instance v0, Lcom/meawallet/mtp/TaskPin$b;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/meawallet/mtp/TaskPin$PinOperation;->CHANGE:Lcom/meawallet/mtp/TaskPin$PinOperation;

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/meawallet/mtp/TaskPin$b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/TaskPin$PinOperation;)V

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->a(Lcom/meawallet/mtp/TaskPin$a;)V

    return-void
.end method

.method public delete(Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 4

    const/4 v0, 0x1

    .line 720
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 723
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    .line 725
    invoke-static {p1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 726
    invoke-static {}, Lcom/meawallet/mtp/dd;->z()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/meawallet/mtp/dw;->e(Landroid/content/Context;Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 728
    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-static {}, Lcom/meawallet/mtp/dd;->z()Landroid/content/Context;

    move-result-object v1

    .line 14051
    iget-object v2, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    .line 13598
    invoke-static {p0, v2}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 13599
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v2

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/meawallet/mtp/ax;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardListener;)V

    const/4 v2, 0x0

    .line 13601
    invoke-virtual {v0, v1, p0, v2}, Lcom/meawallet/mtp/cu;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V

    return-void

    .line 13604
    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/meawallet/mtp/cu$4;

    invoke-direct {v2, v0, p1, p0}, Lcom/meawallet/mtp/cu$4;-><init>(Lcom/meawallet/mtp/cu;Lcom/meawallet/mtp/MeaCardListener;Lcom/meawallet/mtp/MeaCard;)V

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/cu;->a(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)Lcom/meawallet/mtp/MeaError;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 13617
    invoke-static {p1, v0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCoreListener;Lcom/meawallet/mtp/MeaError;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception v0

    .line 734
    sget-object v1, Lcom/meawallet/mtp/ct;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 736
    invoke-static {v0, p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    .line 732
    :catch_1
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCardListener;->onFailure(Ljava/lang/Object;)V

    return-void
.end method

.method public deletePaymentTokens()V
    .locals 5

    const/4 v0, 0x0

    .line 664
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 666
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    const/4 v1, 0x1

    .line 10325
    new-array v2, v1, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 11051
    :try_start_0
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v0}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v0

    .line 10328
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 10332
    invoke-interface {v0}, Lcom/mastercard/mpsdk/componentinterface/Card;->deleteTransactionCredentials()V

    return-void

    .line 10334
    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    const/16 v1, 0x3f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4
    :try_end_0
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 10337
    invoke-static {v0}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    :cond_1
    return-void
.end method

.method public deselectForContactless()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 379
    invoke-static {}, Lcom/meawallet/mtp/ct;->e()V

    return-void
.end method

.method public deselectForContactlessPayment()V
    .locals 0

    .line 386
    invoke-static {}, Lcom/meawallet/mtp/ct;->e()V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 777
    :cond_0
    instance-of v0, p1, Lcom/meawallet/mtp/ct;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 779
    :cond_1
    check-cast p1, Lcom/meawallet/mtp/ct;

    .line 781
    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 783
    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 786
    :cond_2
    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getEligibilityReceipt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/meawallet/mtp/ct;->getEligibilityReceipt()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getDigitizationAuthenticationMethods()[Lcom/meawallet/mtp/MeaAuthenticationMethod;
    .locals 2

    const/4 v0, 0x0

    .line 103
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    .line 3534
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {p0, v0}, Lcom/meawallet/mtp/i;->f(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)[Lcom/meawallet/mtp/MeaAuthenticationMethod;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getDigitizationDecision()Lcom/meawallet/mtp/MeaDigitizationDecision;
    .locals 2

    const/4 v0, 0x0

    .line 90
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    .line 3529
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {p0, v0}, Lcom/meawallet/mtp/i;->e(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaDigitizationDecision;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getEligibilityReceipt()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/meawallet/mtp/ct;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/ct;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/meawallet/mtp/ct;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/meawallet/mtp/ct;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfPaymentTokens()Ljava/lang/Integer;
    .locals 2

    const/4 v0, 0x0

    .line 597
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 599
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/cu;->d(Lcom/meawallet/mtp/MeaCard;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getPaymentNetwork()Lcom/meawallet/mtp/PaymentNetwork;
    .locals 2

    const/4 v0, 0x0

    .line 142
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    .line 3549
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {p0, v0}, Lcom/meawallet/mtp/i;->i(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/PaymentNetwork;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getProductConfig()Lcom/meawallet/mtp/MeaProductConfig;
    .locals 2

    const/4 v0, 0x0

    .line 116
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    .line 3539
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {p0, v0}, Lcom/meawallet/mtp/i;->g(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaProductConfig;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getState()Lcom/meawallet/mtp/MeaCardState;
    .locals 2

    .line 75
    invoke-static {}, Lcom/meawallet/mtp/dw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    .line 79
    invoke-static {}, Lcom/meawallet/mtp/dd;->b()Lcom/meawallet/mtp/l;

    move-result-object v0

    invoke-static {}, Lcom/meawallet/mtp/dd;->a()Lcom/meawallet/mtp/dm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/meawallet/mtp/l;->a(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Lcom/meawallet/mtp/MeaCardState;

    move-result-object v0

    return-object v0

    .line 82
    :cond_0
    sget-object v0, Lcom/meawallet/mtp/MeaCardState;->UNKNOWN:Lcom/meawallet/mtp/MeaCardState;

    return-object v0
.end method

.method public getTermsAndConditionsAssetId()Ljava/lang/String;
    .locals 3

    .line 62
    invoke-static {}, Lcom/meawallet/mtp/dw;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {v1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    const/4 v1, 0x1

    .line 3063
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    .line 3065
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {p0, v0}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/bt;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 3130
    :cond_0
    iget-object v0, v0, Lcom/meawallet/mtp/bt;->k:Ljava/lang/String;

    return-object v0

    :cond_1
    return-object v1
.end method

.method public getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;
    .locals 2

    const/4 v0, 0x0

    .line 129
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    .line 3544
    iget-object v0, v0, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {p0, v0}, Lcom/meawallet/mtp/i;->h(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/MeaTokenInfo;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getTransactionHistory(Lcom/meawallet/mtp/MeaGetCardTransactionHistoryListener;)V
    .locals 2

    .line 688
    :try_start_0
    invoke-static {p1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/meawallet/mtp/dd;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaGetCardTransactionHistoryListener;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    .line 693
    :catch_0
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaGetCardTransactionHistoryListener;->onFailure(Ljava/lang/Object;)V

    return-void
.end method

.method public getTransactionLog()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLog;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 675
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/cu;->e(Lcom/meawallet/mtp/MeaCard;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    .line 770
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getEligibilityReceipt()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isContactlessPaymentsSupported()Ljava/lang/Boolean;
    .locals 1

    .line 164
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isContactlessSupported()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 156
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isDefaultForContactless()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 229
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->c()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isDefaultForContactlessPayments()Ljava/lang/Boolean;
    .locals 4

    .line 236
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->c()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x2

    .line 238
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    return-object v0
.end method

.method public isDefaultForRemote()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 259
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->d()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isDefaultForRemotePayments()Ljava/lang/Boolean;
    .locals 1

    .line 267
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->d()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isPinSet()Z
    .locals 2

    .line 206
    sget-object v0, Lcom/meawallet/mtp/LdePinState;->PIN_SET:Lcom/meawallet/mtp/LdePinState;

    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v1

    .line 4077
    iget-object v1, v1, Lcom/meawallet/mtp/cu;->d:Lcom/meawallet/mtp/ci;

    invoke-static {p0, v1}, Lcom/meawallet/mtp/i;->d(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/ci;)Lcom/meawallet/mtp/LdePinState;

    move-result-object v1

    .line 206
    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/LdePinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRemotePaymentsSupported()Ljava/lang/Boolean;
    .locals 1

    .line 191
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isRemoteSupported()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 183
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public markForDeletion(Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 5

    const/4 v0, 0x1

    .line 699
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 702
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    .line 704
    invoke-static {p1}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 706
    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-static {}, Lcom/meawallet/mtp/dd;->z()Landroid/content/Context;

    move-result-object v1

    .line 12051
    iget-object v2, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    .line 11581
    invoke-static {p0, v2}, Lcom/meawallet/mtp/i;->b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 11583
    invoke-virtual {v0, v1, p0, p1}, Lcom/meawallet/mtp/cu;->a(Landroid/content/Context;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaCardListener;)V

    return-void

    .line 11586
    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 11588
    iget-object v2, v0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 13051
    iget-object v3, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v3}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v3

    .line 13055
    iget-object v4, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    .line 13289
    iget-object v4, v4, Lcom/meawallet/mtp/dm;->d:Lcom/meawallet/mtp/k;

    .line 11588
    invoke-virtual {v2, v1, v3, v4}, Lcom/meawallet/mtp/l;->a(Ljava/util/List;Lcom/mastercard/mpsdk/componentinterface/CardManager;Lcom/meawallet/mtp/k;)V

    .line 11590
    invoke-static {p1, p0}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardListener;Lcom/meawallet/mtp/MeaCard;)V

    const/4 v2, 0x0

    .line 11592
    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/cu;->a(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)Lcom/meawallet/mtp/MeaError;
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/cs; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/bn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/meawallet/mtp/MeaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception v0

    .line 712
    sget-object v1, Lcom/meawallet/mtp/ct;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 714
    invoke-static {v0, p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;Lcom/meawallet/mtp/MeaCoreListener;)V

    return-void

    .line 710
    :catch_1
    new-instance v0, Lcom/meawallet/mtp/cy;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/meawallet/mtp/MeaCardListener;->onFailure(Ljava/lang/Object;)V

    return-void
.end method

.method public processRemoteTransaction(Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V
    .locals 6

    .line 530
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/dw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x6a

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 536
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 537
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x67

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 542
    :cond_1
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->b()Landroid/content/Context;

    move-result-object v0

    .line 544
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 545
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x38e

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 550
    :cond_2
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->g()Lcom/meawallet/mtp/fr;

    move-result-object v1

    .line 5043
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getCurrencyCode()I

    move-result v2

    invoke-static {v2}, Lcom/mastercard/mpsdk/utils/currency/Iso4217CurrencyUtils;->getCurrencyByCode(I)Ljava/util/Currency;

    move-result-object v2

    .line 5044
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaRemotePaymentData;->getTransactionAmount()J

    move-result-wide v3

    long-to-int v3, v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez v2, :cond_3

    const/4 v1, 0x0

    goto :goto_0

    .line 5054
    :cond_3
    invoke-virtual {v1, v2, v3}, Lcom/meawallet/mtp/fr;->a(Ljava/util/Currency;I)Z

    move-result v1

    .line 5056
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    :goto_0
    if-nez v1, :cond_4

    .line 551
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x456

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    :cond_4
    const/4 v1, -0x1

    .line 558
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_6

    invoke-static {}, Lcom/meawallet/mtp/n;->h()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 559
    invoke-static {v4}, Lcom/meawallet/mtp/n;->a(Z)Lcom/meawallet/mtp/CdCvmType;

    move-result-object v1

    if-nez v1, :cond_5

    .line 562
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x1f5

    const-string v1, "Applicable CD CVM is null"

    invoke-direct {p1, v0, v1}, Lcom/meawallet/mtp/cy;-><init>(ILjava/lang/String;)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 569
    :cond_5
    invoke-static {v0, v1}, Lcom/meawallet/mtp/dw;->a(Landroid/content/Context;Lcom/meawallet/mtp/CdCvmType;)I

    move-result v1

    .line 572
    :cond_6
    invoke-static {v1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 573
    new-instance p1, Lcom/meawallet/mtp/cy;

    invoke-direct {p1, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 578
    :cond_7
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 579
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x6d

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 584
    :cond_8
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    const/4 v1, 0x2

    .line 5393
    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaRemotePaymentData;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 5397
    :try_start_1
    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaRemotePaymentData;->isValid()Z

    move-result v1

    if-nez v1, :cond_9

    .line 5398
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x1f9

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    .line 5401
    :cond_9
    iget-object v1, v0, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 6051
    iget-object v2, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v2}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v2

    .line 5401
    invoke-virtual {v1, p0, v2}, Lcom/meawallet/mtp/l;->b(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v1

    .line 5403
    invoke-static {v1}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 5404
    new-instance p1, Lcom/meawallet/mtp/cy;

    invoke-direct {p1, v1}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 7051
    :cond_a
    iget-object v1, v0, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v1}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v1

    .line 5409
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/mastercard/mpsdk/componentinterface/CardManager;->getCardById(Ljava/lang/String;)Lcom/mastercard/mpsdk/componentinterface/Card;

    move-result-object v1

    if-nez v1, :cond_b

    .line 5412
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x3f2

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 5417
    :cond_b
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/Card;->isDsrpSupported()Z

    move-result v2

    if-nez v2, :cond_c

    .line 5418
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x3f1

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 5423
    :cond_c
    invoke-interface {v1}, Lcom/mastercard/mpsdk/componentinterface/Card;->getNumberOfAvailableCredentials()I

    move-result v1

    if-lez v1, :cond_d

    .line 5424
    invoke-virtual {v0, p0, p1, p2}, Lcom/meawallet/mtp/cu;->a(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V

    return-void

    .line 5429
    :cond_d
    new-instance v1, Lcom/meawallet/mtp/dk;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/meawallet/mtp/dk;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/cu;)V

    .line 5431
    new-instance v2, Lcom/meawallet/mtp/cu$2;

    invoke-direct {v2, v0, p0, p1, p2}, Lcom/meawallet/mtp/cu$2;-><init>(Lcom/meawallet/mtp/cu;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V

    .line 8031
    iput-object v2, v1, Lcom/meawallet/mtp/dk;->a:Lcom/meawallet/mtp/en;

    .line 5443
    invoke-virtual {v1}, Lcom/meawallet/mtp/dk;->a()V
    :try_end_1
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_0
    move-exception p1

    .line 5446
    :try_start_2
    invoke-static {p1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;

    move-result-object p1

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
    :try_end_2
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :catch_1
    move-exception p1

    .line 586
    sget-object v0, Lcom/meawallet/mtp/ct;->a:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 588
    new-instance p1, Lcom/meawallet/mtp/cy;

    const/16 v0, 0x65

    invoke-direct {p1, v0}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {p2, p0, p1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaRemoteTransactionListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public removeContactlessTransactionListener()V
    .locals 2

    .line 751
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/ax;->b(Ljava/lang/String;)V

    return-void
.end method

.method public removePinListener()V
    .locals 2

    .line 765
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/ax;->a(Ljava/lang/String;)V

    return-void
.end method

.method public replenishPaymentTokens()V
    .locals 7

    .line 609
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    .line 8113
    iget-object v0, v0, Lcom/meawallet/mtp/ax;->b:Lcom/meawallet/mtp/MeaCardReplenishListener;

    .line 613
    :try_start_0
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    .line 614
    invoke-static {}, Lcom/meawallet/mtp/dd;->z()Landroid/content/Context;

    move-result-object v1

    .line 616
    invoke-static {}, Lcom/meawallet/mtp/dw;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 618
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x6a

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {v0, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    .line 623
    invoke-static {v2}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 625
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x67

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {v0, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 630
    :cond_1
    invoke-static {v1}, Lcom/meawallet/mtp/dw;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 632
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x6d

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {v0, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 637
    :cond_2
    invoke-static {}, Lcom/meawallet/mtp/n;->f()Lcom/meawallet/mtp/CdCvmType;

    move-result-object v1

    .line 639
    sget-object v2, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_CARD:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {v2, v1}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->isPinSet()Z

    move-result v2

    if-nez v2, :cond_3

    .line 640
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x3f4

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {v0, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 645
    :cond_3
    sget-object v2, Lcom/meawallet/mtp/CdCvmType;->MOBILE_PIN_WALLET:Lcom/meawallet/mtp/CdCvmType;

    invoke-virtual {v2, v1}, Lcom/meawallet/mtp/CdCvmType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isWalletPinSet()Z

    move-result v1

    if-nez v1, :cond_4

    .line 646
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x38f

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {v0, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void

    .line 651
    :cond_4
    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v1

    const/4 v2, 0x1

    .line 8302
    new-array v3, v2, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 8304
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v3

    .line 9113
    iget-object v3, v3, Lcom/meawallet/mtp/ax;->b:Lcom/meawallet/mtp/MeaCardReplenishListener;

    .line 8306
    iget-object v4, v1, Lcom/meawallet/mtp/cu;->c:Lcom/meawallet/mtp/l;

    .line 10051
    iget-object v6, v1, Lcom/meawallet/mtp/cu;->b:Lcom/meawallet/mtp/dm;

    invoke-virtual {v6}, Lcom/meawallet/mtp/dm;->a()Lcom/mastercard/mpsdk/componentinterface/CardManager;

    move-result-object v6

    .line 8306
    invoke-virtual {v4, p0, v6}, Lcom/meawallet/mtp/l;->c(Lcom/meawallet/mtp/MeaCard;Lcom/mastercard/mpsdk/componentinterface/CardManager;)I

    move-result v4

    .line 8308
    invoke-static {v4}, Lcom/meawallet/mtp/MeaErrorCode;->isErrorCode(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 8309
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    .line 8311
    new-instance v1, Lcom/meawallet/mtp/cy;

    invoke-direct {v1, v4}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {v3, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
    :try_end_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    .line 8317
    :cond_5
    :try_start_1
    new-instance v2, Lcom/meawallet/mtp/dk;

    invoke-interface {p0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/meawallet/mtp/dk;-><init>(Ljava/lang/String;Lcom/meawallet/mtp/cu;)V

    invoke-virtual {v2}, Lcom/meawallet/mtp/dk;->a()V
    :try_end_1
    .catch Lcom/mastercard/mpsdk/componentinterface/RolloverInProgressException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_0
    move-exception v1

    .line 8320
    :try_start_2
    invoke-static {v1}, Lcom/meawallet/mtp/aw;->a(Ljava/lang/Exception;)Lcom/meawallet/mtp/MeaError;
    :try_end_2
    .catch Lcom/meawallet/mtp/NotInitializedException; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :catch_1
    move-exception v1

    .line 654
    sget-object v2, Lcom/meawallet/mtp/ct;->a:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/meawallet/mtp/cp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 656
    new-instance v1, Lcom/meawallet/mtp/cy;

    const/16 v2, 0x65

    invoke-direct {v1, v2}, Lcom/meawallet/mtp/cy;-><init>(I)V

    invoke-static {v0, p0, v1}, Lcom/meawallet/mtp/ft;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public selectForContactless(Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 298
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/ct;->a(Lcom/meawallet/mtp/MeaCardListener;)V

    return-void
.end method

.method public selectForContactlessPayment(Lcom/meawallet/mtp/MeaCardListener;)V
    .locals 0

    .line 305
    invoke-direct {p0, p1}, Lcom/meawallet/mtp/ct;->a(Lcom/meawallet/mtp/MeaCardListener;)V

    return-void
.end method

.method public setAsDefaultForContactless()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 401
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->f()V

    return-void
.end method

.method public setAsDefaultForContactlessPayments()V
    .locals 3

    const/4 v0, 0x1

    .line 406
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 408
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->f()V

    return-void
.end method

.method public setAsDefaultForRemote()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 455
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->h()V

    return-void
.end method

.method public setAsDefaultForRemotePayments()V
    .locals 0

    .line 462
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->h()V

    return-void
.end method

.method public setContactlessTransactionListener(Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    .locals 2

    .line 744
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/meawallet/mtp/ax;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V

    return-void
.end method

.method public setPin(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    .line 211
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 213
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    new-instance v0, Lcom/meawallet/mtp/TaskPin$b;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/meawallet/mtp/TaskPin$PinOperation;->SET:Lcom/meawallet/mtp/TaskPin$PinOperation;

    invoke-direct {v0, v1, p1, v2}, Lcom/meawallet/mtp/TaskPin$b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/TaskPin$PinOperation;)V

    invoke-static {v0}, Lcom/meawallet/mtp/dd;->a(Lcom/meawallet/mtp/TaskPin$a;)V

    return-void
.end method

.method public setPinListener(Lcom/meawallet/mtp/MeaCardPinListener;)V
    .locals 2

    .line 758
    invoke-static {}, Lcom/meawallet/mtp/ax;->a()Lcom/meawallet/mtp/ax;

    move-result-object v0

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/meawallet/mtp/ax;->a(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardPinListener;)V

    return-void
.end method

.method public stopContactlessTransaction()Ljava/lang/Boolean;
    .locals 2

    const/4 v0, 0x0

    .line 285
    invoke-static {v0}, Lcom/meawallet/mtp/dw;->a(Lcom/meawallet/mtp/MeaCoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->a()Lcom/meawallet/mtp/dd;

    invoke-static {}, Lcom/meawallet/mtp/dd;->c()Lcom/meawallet/mtp/cu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/meawallet/mtp/cu;->c(Lcom/meawallet/mtp/MeaCard;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public unsetAsDefaultForContactless()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 433
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->g()V

    return-void
.end method

.method public unsetAsDefaultForContactlessPayments()V
    .locals 3

    const/4 v0, 0x1

    .line 438
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/meawallet/mtp/ct;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 440
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->g()V

    return-void
.end method

.method public unsetAsDefaultForRemote()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 487
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->i()V

    return-void
.end method

.method public unsetAsDefaultForRemotePayments()V
    .locals 0

    .line 494
    invoke-direct {p0}, Lcom/meawallet/mtp/ct;->i()V

    return-void
.end method
