.class public interface abstract Lcom/meawallet/mtp/MeaCard;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract authenticateWithCardPin(Ljava/lang/String;Lcom/meawallet/mtp/MeaCardPinAuthenticationListener;)V
.end method

.method public abstract changePin(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract delete(Lcom/meawallet/mtp/MeaCardListener;)V
.end method

.method public abstract deletePaymentTokens()V
.end method

.method public abstract deselectForContactless()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract deselectForContactlessPayment()V
.end method

.method public abstract getDigitizationAuthenticationMethods()[Lcom/meawallet/mtp/MeaAuthenticationMethod;
.end method

.method public abstract getDigitizationDecision()Lcom/meawallet/mtp/MeaDigitizationDecision;
.end method

.method public abstract getEligibilityReceipt()Ljava/lang/String;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getNumberOfPaymentTokens()Ljava/lang/Integer;
.end method

.method public abstract getPaymentNetwork()Lcom/meawallet/mtp/PaymentNetwork;
.end method

.method public abstract getProductConfig()Lcom/meawallet/mtp/MeaProductConfig;
.end method

.method public abstract getState()Lcom/meawallet/mtp/MeaCardState;
.end method

.method public abstract getTermsAndConditionsAssetId()Ljava/lang/String;
.end method

.method public abstract getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;
.end method

.method public abstract getTransactionHistory(Lcom/meawallet/mtp/MeaGetCardTransactionHistoryListener;)V
.end method

.method public abstract getTransactionLog()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaTransactionLog;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isContactlessPaymentsSupported()Ljava/lang/Boolean;
.end method

.method public abstract isContactlessSupported()Ljava/lang/Boolean;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract isDefaultForContactless()Ljava/lang/Boolean;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract isDefaultForContactlessPayments()Ljava/lang/Boolean;
.end method

.method public abstract isDefaultForRemote()Ljava/lang/Boolean;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract isDefaultForRemotePayments()Ljava/lang/Boolean;
.end method

.method public abstract isPinSet()Z
.end method

.method public abstract isRemotePaymentsSupported()Ljava/lang/Boolean;
.end method

.method public abstract isRemoteSupported()Ljava/lang/Boolean;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract markForDeletion(Lcom/meawallet/mtp/MeaCardListener;)V
.end method

.method public abstract processRemoteTransaction(Lcom/meawallet/mtp/MeaRemotePaymentData;Lcom/meawallet/mtp/MeaRemoteTransactionListener;)V
.end method

.method public abstract removeContactlessTransactionListener()V
.end method

.method public abstract removePinListener()V
.end method

.method public abstract replenishPaymentTokens()V
.end method

.method public abstract selectForContactless(Lcom/meawallet/mtp/MeaCardListener;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract selectForContactlessPayment(Lcom/meawallet/mtp/MeaCardListener;)V
.end method

.method public abstract setAsDefaultForContactless()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract setAsDefaultForContactlessPayments()V
.end method

.method public abstract setAsDefaultForRemote()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract setAsDefaultForRemotePayments()V
.end method

.method public abstract setContactlessTransactionListener(Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
.end method

.method public abstract setPin(Ljava/lang/String;)V
.end method

.method public abstract setPinListener(Lcom/meawallet/mtp/MeaCardPinListener;)V
.end method

.method public abstract stopContactlessTransaction()Ljava/lang/Boolean;
.end method

.method public abstract unsetAsDefaultForContactless()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract unsetAsDefaultForContactlessPayments()V
.end method

.method public abstract unsetAsDefaultForRemote()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract unsetAsDefaultForRemotePayments()V
.end method
