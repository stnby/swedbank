.class public final enum Lcom/meawallet/mtp/TaskPin$PinOperation;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/TaskPin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PinOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/meawallet/mtp/TaskPin$PinOperation;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CHANGE:Lcom/meawallet/mtp/TaskPin$PinOperation;

.field public static final enum SET:Lcom/meawallet/mtp/TaskPin$PinOperation;

.field private static final synthetic a:[Lcom/meawallet/mtp/TaskPin$PinOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 26
    new-instance v0, Lcom/meawallet/mtp/TaskPin$PinOperation;

    const-string v1, "CHANGE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/meawallet/mtp/TaskPin$PinOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/TaskPin$PinOperation;->CHANGE:Lcom/meawallet/mtp/TaskPin$PinOperation;

    .line 27
    new-instance v0, Lcom/meawallet/mtp/TaskPin$PinOperation;

    const-string v1, "SET"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/meawallet/mtp/TaskPin$PinOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/meawallet/mtp/TaskPin$PinOperation;->SET:Lcom/meawallet/mtp/TaskPin$PinOperation;

    const/4 v0, 0x2

    .line 25
    new-array v0, v0, [Lcom/meawallet/mtp/TaskPin$PinOperation;

    sget-object v1, Lcom/meawallet/mtp/TaskPin$PinOperation;->CHANGE:Lcom/meawallet/mtp/TaskPin$PinOperation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/meawallet/mtp/TaskPin$PinOperation;->SET:Lcom/meawallet/mtp/TaskPin$PinOperation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/meawallet/mtp/TaskPin$PinOperation;->a:[Lcom/meawallet/mtp/TaskPin$PinOperation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/meawallet/mtp/TaskPin$PinOperation;
    .locals 1

    .line 25
    const-class v0, Lcom/meawallet/mtp/TaskPin$PinOperation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/meawallet/mtp/TaskPin$PinOperation;

    return-object p0
.end method

.method public static values()[Lcom/meawallet/mtp/TaskPin$PinOperation;
    .locals 1

    .line 25
    sget-object v0, Lcom/meawallet/mtp/TaskPin$PinOperation;->a:[Lcom/meawallet/mtp/TaskPin$PinOperation;

    invoke-virtual {v0}, [Lcom/meawallet/mtp/TaskPin$PinOperation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/meawallet/mtp/TaskPin$PinOperation;

    return-object v0
.end method
