.class final Lcom/meawallet/mtp/at$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mastercard/mpsdk/componentinterface/DigitizedCard;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/meawallet/mtp/at;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;


# direct methods
.method constructor <init>(Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/meawallet/mtp/at$1;->a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCardCountryCode()[B
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/meawallet/mtp/at$1;->a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->mppLiteModule:Lcom/mastercard/mpsdk/card/profile/v1/MppLiteModuleV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/MppLiteModuleV1Json;->cardRiskManagementData:Lcom/mastercard/mpsdk/card/profile/v1/CardRiskManagementDataV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/CardRiskManagementDataV1Json;->crmCountryCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getContactlessPaymentData()Lcom/mastercard/mpsdk/componentinterface/ContactlessPaymentData;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/meawallet/mtp/at$1;->a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->mppLiteModule:Lcom/mastercard/mpsdk/card/profile/v1/MppLiteModuleV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/MppLiteModuleV1Json;->contactlessPaymentData:Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2101
    :cond_0
    new-instance v1, Lcom/meawallet/mtp/at$2;

    invoke-direct {v1, v0}, Lcom/meawallet/mtp/at$2;-><init>(Lcom/mastercard/mpsdk/card/profile/v1/ContactlessPaymentDataV1Json;)V

    return-object v1
.end method

.method public final getDigitizedCardId()[B
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/meawallet/mtp/at$1;->a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->digitizedCardId:Ljava/lang/String;

    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->of(Ljava/lang/String;)Lcom/mastercard/mpsdk/utils/bytes/ByteArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/utils/bytes/ByteArray;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public final getDsrpData()Lcom/mastercard/mpsdk/componentinterface/DsrpData;
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/meawallet/mtp/at$1;->a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->mppLiteModule:Lcom/mastercard/mpsdk/card/profile/v1/MppLiteModuleV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/MppLiteModuleV1Json;->remotePaymentData:Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 3191
    :cond_0
    new-instance v1, Lcom/meawallet/mtp/at$3;

    invoke-direct {v1, v0}, Lcom/meawallet/mtp/at$3;-><init>(Lcom/mastercard/mpsdk/card/profile/v1/RemotePaymentDataV1Json;)V

    return-object v1
.end method

.method public final getPan()[B
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/meawallet/mtp/at$1;->a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    iget-object v0, v0, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->digitizedCardId:Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0x13

    .line 1088
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "F"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1089
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "F"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1091
    :cond_0
    invoke-static {v0}, Lcom/mastercard/mpsdk/utils/Utils;->fromHexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/meawallet/mtp/at$1;->a:Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;

    invoke-virtual {v0}, Lcom/mastercard/mpsdk/card/profile/v1/DigitizedCardProfileV1Json;->getProfileVersion()Lcom/mastercard/mpsdk/componentinterface/ProfileVersion;

    move-result-object v0

    return-object v0
.end method

.method public final getWalletData()Lcom/mastercard/mpsdk/componentinterface/WalletData;
    .locals 1

    .line 52
    new-instance v0, Lcom/meawallet/mtp/at$1$1;

    invoke-direct {v0, p0}, Lcom/meawallet/mtp/at$1$1;-><init>(Lcom/meawallet/mtp/at$1;)V

    return-object v0
.end method

.method public final isTransactionIdRequired()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
