.class Lcom/meawallet/mtp/di;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "di"

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "mpa_sdk_mcbp_random_value"

    const-string v1, "mpa_sdk_native_device_info2"

    const-string v2, "wb_jni_device_info"

    const-string v3, "mpa_sdk_native_device_info"

    .line 15
    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/meawallet/mtp/di;->b:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/content/Context;)V
    .locals 2

    const-string v0, "core_prefs"

    const/4 v1, 0x0

    .line 49
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    .line 50
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method static a()Z
    .locals 3

    .line 28
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "MeaWallet"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/meawallet/mtp/az;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static b()V
    .locals 6

    .line 35
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "MeaWallet"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/az;->b(Ljava/lang/String;)Z

    .line 36
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "MCBP"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/az;->b(Ljava/lang/String;)Z

    .line 37
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "logs"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/az;->b(Ljava/lang/String;)Z

    .line 40
    sget-object v0, Lcom/meawallet/mtp/di;->b:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 41
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/meawallet/mtp/az;->b(Ljava/lang/String;Ljava/lang/String;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static c()V
    .locals 2

    .line 57
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "LDE"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/az;->b(Ljava/lang/String;)Z

    .line 58
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "mtp_native"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/az;->b(Ljava/lang/String;)Z

    .line 61
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "MCBP.db"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/az;->c(Ljava/lang/String;)Z

    .line 62
    invoke-static {}, Lcom/meawallet/mtp/az;->a()Lcom/meawallet/mtp/az;

    move-result-object v0

    const-string v1, "MCBP.db-journal"

    invoke-virtual {v0, v1}, Lcom/meawallet/mtp/az;->c(Ljava/lang/String;)Z

    return-void
.end method

.method static d()V
    .locals 1

    .line 68
    invoke-static {}, Lcom/meawallet/mtp/dx;->a()Lcom/meawallet/mtp/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/meawallet/mtp/dx;->b()Z

    return-void
.end method
