.class Lcom/meawallet/mtp/gn;
.super Lcom/meawallet/mtp/eh;
.source "SourceFile"


# instance fields
.field a:Lcom/meawallet/mtp/MeaEligibilityReceipt;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "eligibilityReceipt"
    .end annotation
.end field

.field b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tacAssetId"
    .end annotation
.end field

.field c:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "securityCodeApplicable"
    .end annotation
.end field

.field d:Lcom/meawallet/mtp/PaymentNetwork;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentNetwork"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/meawallet/mtp/eh;-><init>()V

    return-void
.end method


# virtual methods
.method validate()V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/meawallet/mtp/gn;->a:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    if-eqz v0, :cond_4

    .line 45
    iget-object v0, p0, Lcom/meawallet/mtp/gn;->a:Lcom/meawallet/mtp/MeaEligibilityReceipt;

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaEligibilityReceipt;->validate()V

    .line 47
    iget-object v0, p0, Lcom/meawallet/mtp/gn;->d:Lcom/meawallet/mtp/PaymentNetwork;

    if-eqz v0, :cond_3

    .line 52
    iget-object v0, p0, Lcom/meawallet/mtp/gn;->d:Lcom/meawallet/mtp/PaymentNetwork;

    sget-object v1, Lcom/meawallet/mtp/PaymentNetwork;->MASTERCARD:Lcom/meawallet/mtp/PaymentNetwork;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/meawallet/mtp/gn;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Terms and conditions asset id is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/meawallet/mtp/gn;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    return-void

    .line 59
    :cond_2
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Security code applicable is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_3
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Payment network is empty."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_4
    new-instance v0, Lcom/meawallet/mtp/bl;

    const-string v1, "Eligibility receipt is null."

    invoke-direct {v0, v1}, Lcom/meawallet/mtp/bl;-><init>(Ljava/lang/String;)V

    throw v0
.end method
