.class Lcom/meawallet/mtp/gv;
.super Lcom/meawallet/mtp/ed;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/meawallet/mtp/ed<",
        "Lcom/meawallet/mtp/hb;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "gv"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/meawallet/mtp/ed;-><init>()V

    return-void
.end method

.method static a(Lcom/meawallet/mtp/WspErrorResponseData;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Category: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1029
    iget-object v1, p0, Lcom/meawallet/mtp/WspErrorResponseData;->a:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1033
    iget-object v1, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " Description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1037
    iget-object v1, p0, Lcom/meawallet/mtp/WspErrorResponseData;->c:Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " Emitter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1041
    iget-object p0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->d:Lcom/meawallet/mtp/WspErrorResponseData$Emitters;

    .line 49
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method

.method static b(Lcom/meawallet/mtp/WspErrorResponseData;)I
    .locals 4

    .line 2029
    iget-object v0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->a:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    const/16 v1, 0x1fc

    if-eqz v0, :cond_3

    .line 2033
    iget-object v0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    if-eqz v0, :cond_3

    .line 65
    sget-object v0, Lcom/meawallet/mtp/gv$1;->b:[I

    .line 3029
    iget-object v2, p0, Lcom/meawallet/mtp/WspErrorResponseData;->a:Lcom/meawallet/mtp/WspErrorResponseData$Category;

    .line 65
    invoke-virtual {v2}, Lcom/meawallet/mtp/WspErrorResponseData$Category;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/16 v2, 0x25a

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const/16 p0, 0x262

    return p0

    .line 150
    :pswitch_1
    sget-object v0, Lcom/meawallet/mtp/gv$1;->a:[I

    .line 8033
    iget-object p0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 150
    invoke-virtual {p0}, Lcom/meawallet/mtp/WspErrorResponseData$Code;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_1

    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_2
    return v1

    :pswitch_3
    return v1

    :pswitch_4
    return v1

    :cond_0
    return v1

    :cond_1
    return v1

    .line 138
    :pswitch_5
    sget-object v0, Lcom/meawallet/mtp/gv$1;->a:[I

    .line 7033
    iget-object p0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 138
    invoke-virtual {p0}, Lcom/meawallet/mtp/WspErrorResponseData$Code;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_2

    goto :goto_0

    :pswitch_6
    return v2

    :pswitch_7
    return v1

    .line 120
    :pswitch_8
    sget-object v0, Lcom/meawallet/mtp/gv$1;->a:[I

    .line 6033
    iget-object p0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 120
    invoke-virtual {p0}, Lcom/meawallet/mtp/WspErrorResponseData$Code;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_3

    goto :goto_0

    :pswitch_9
    const/16 p0, 0x267

    return p0

    :pswitch_a
    const/16 p0, 0x3ef

    return p0

    :pswitch_b
    return v1

    :pswitch_c
    const/16 p0, 0x25c

    return p0

    .line 105
    :pswitch_d
    sget-object v0, Lcom/meawallet/mtp/gv$1;->a:[I

    .line 5033
    iget-object p0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 105
    invoke-virtual {p0}, Lcom/meawallet/mtp/WspErrorResponseData$Code;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_4

    goto :goto_0

    :pswitch_e
    return v1

    :pswitch_f
    const/16 p0, 0x25b

    return p0

    :pswitch_10
    const/16 p0, 0x1f8

    return p0

    .line 90
    :pswitch_11
    sget-object v0, Lcom/meawallet/mtp/gv$1;->a:[I

    .line 4033
    iget-object p0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 90
    invoke-virtual {p0}, Lcom/meawallet/mtp/WspErrorResponseData$Code;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_5

    goto :goto_0

    :pswitch_12
    const/16 p0, 0xd1

    return p0

    :pswitch_13
    const/16 p0, 0x264

    return p0

    :pswitch_14
    return v2

    .line 68
    :pswitch_15
    sget-object v0, Lcom/meawallet/mtp/gv$1;->a:[I

    .line 3033
    iget-object v2, p0, Lcom/meawallet/mtp/WspErrorResponseData;->b:Lcom/meawallet/mtp/WspErrorResponseData$Code;

    .line 68
    invoke-virtual {v2}, Lcom/meawallet/mtp/WspErrorResponseData$Code;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/16 v2, 0x259

    const/16 v3, 0x1f9

    packed-switch v0, :pswitch_data_6

    goto :goto_0

    :pswitch_16
    return v2

    :pswitch_17
    const-string v0, "eligibilityReceipt"

    .line 3037
    iget-object p0, p0, Lcom/meawallet/mtp/WspErrorResponseData;->c:Ljava/lang/String;

    .line 76
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/16 p0, 0x265

    return p0

    :cond_2
    return v2

    :pswitch_18
    return v3

    :pswitch_19
    return v3

    :cond_3
    :goto_0
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_15
        :pswitch_11
        :pswitch_d
        :pswitch_8
        :pswitch_5
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xe
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0xc
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x8
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x7
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x5
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
    .end packed-switch
.end method
