.class Lcom/squareup/moshi/StandardJsonAdapters$10;
.super Lcom/squareup/moshi/JsonAdapter;
.source "StandardJsonAdapters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/StandardJsonAdapters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 185
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 185
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/StandardJsonAdapters$10;->b(Lcom/squareup/moshi/g;)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Ljava/lang/Long;)V
    .locals 2

    .line 191
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/moshi/l;->a(J)Lcom/squareup/moshi/l;

    return-void
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 185
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/moshi/StandardJsonAdapters$10;->a(Lcom/squareup/moshi/l;Ljava/lang/Long;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Ljava/lang/Long;
    .locals 2

    .line 187
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "JsonAdapter(Long)"

    return-object v0
.end method
