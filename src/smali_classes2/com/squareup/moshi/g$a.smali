.class public final Lcom/squareup/moshi/g$a;
.super Ljava/lang/Object;
.source "JsonReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field final a:[Ljava/lang/String;

.field final b:Lb/m;


# direct methods
.method private constructor <init>([Ljava/lang/String;Lb/m;)V
    .locals 0

    .line 524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525
    iput-object p1, p0, Lcom/squareup/moshi/g$a;->a:[Ljava/lang/String;

    .line 526
    iput-object p2, p0, Lcom/squareup/moshi/g$a;->b:Lb/m;

    return-void
.end method

.method public static varargs a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;
    .locals 4
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 531
    :try_start_0
    array-length v0, p0

    new-array v0, v0, [Lb/f;

    .line 532
    new-instance v1, Lb/c;

    invoke-direct {v1}, Lb/c;-><init>()V

    const/4 v2, 0x0

    .line 533
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 534
    aget-object v3, p0, v2

    invoke-static {v1, v3}, Lcom/squareup/moshi/j;->a(Lb/d;Ljava/lang/String;)V

    .line 535
    invoke-virtual {v1}, Lb/c;->j()B

    .line 536
    invoke-virtual {v1}, Lb/c;->q()Lb/f;

    move-result-object v3

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 538
    :cond_0
    new-instance v1, Lcom/squareup/moshi/g$a;

    invoke-virtual {p0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;

    invoke-static {v0}, Lb/m;->a([Lb/f;)Lb/m;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/squareup/moshi/g$a;-><init>([Ljava/lang/String;Lb/m;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception p0

    .line 540
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
