.class abstract Lcom/squareup/moshi/m$e;
.super Ljava/lang/Object;
.source "LinkedHashTreeMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field b:Lcom/squareup/moshi/m$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/m$f<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lcom/squareup/moshi/m$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/m$f<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field d:I

.field final synthetic e:Lcom/squareup/moshi/m;


# direct methods
.method constructor <init>(Lcom/squareup/moshi/m;)V
    .locals 0

    .line 760
    iput-object p1, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 761
    iget-object p1, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    iget-object p1, p1, Lcom/squareup/moshi/m;->c:Lcom/squareup/moshi/m$f;

    iget-object p1, p1, Lcom/squareup/moshi/m$f;->d:Lcom/squareup/moshi/m$f;

    iput-object p1, p0, Lcom/squareup/moshi/m$e;->b:Lcom/squareup/moshi/m$f;

    const/4 p1, 0x0

    .line 762
    iput-object p1, p0, Lcom/squareup/moshi/m$e;->c:Lcom/squareup/moshi/m$f;

    .line 763
    iget-object p1, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    iget p1, p1, Lcom/squareup/moshi/m;->e:I

    iput p1, p0, Lcom/squareup/moshi/m$e;->d:I

    return-void
.end method


# virtual methods
.method final b()Lcom/squareup/moshi/m$f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/moshi/m$f<",
            "TK;TV;>;"
        }
    .end annotation

    .line 770
    iget-object v0, p0, Lcom/squareup/moshi/m$e;->b:Lcom/squareup/moshi/m$f;

    .line 771
    iget-object v1, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    iget-object v1, v1, Lcom/squareup/moshi/m;->c:Lcom/squareup/moshi/m$f;

    if-eq v0, v1, :cond_1

    .line 774
    iget-object v1, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    iget v1, v1, Lcom/squareup/moshi/m;->e:I

    iget v2, p0, Lcom/squareup/moshi/m$e;->d:I

    if-ne v1, v2, :cond_0

    .line 777
    iget-object v1, v0, Lcom/squareup/moshi/m$f;->d:Lcom/squareup/moshi/m$f;

    iput-object v1, p0, Lcom/squareup/moshi/m$e;->b:Lcom/squareup/moshi/m$f;

    .line 778
    iput-object v0, p0, Lcom/squareup/moshi/m$e;->c:Lcom/squareup/moshi/m$f;

    return-object v0

    .line 775
    :cond_0
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 772
    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public final hasNext()Z
    .locals 2

    .line 766
    iget-object v0, p0, Lcom/squareup/moshi/m$e;->b:Lcom/squareup/moshi/m$f;

    iget-object v1, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    iget-object v1, v1, Lcom/squareup/moshi/m;->c:Lcom/squareup/moshi/m$f;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final remove()V
    .locals 3

    .line 782
    iget-object v0, p0, Lcom/squareup/moshi/m$e;->c:Lcom/squareup/moshi/m$f;

    if-eqz v0, :cond_0

    .line 785
    iget-object v0, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    iget-object v1, p0, Lcom/squareup/moshi/m$e;->c:Lcom/squareup/moshi/m$f;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/moshi/m;->a(Lcom/squareup/moshi/m$f;Z)V

    const/4 v0, 0x0

    .line 786
    iput-object v0, p0, Lcom/squareup/moshi/m$e;->c:Lcom/squareup/moshi/m$f;

    .line 787
    iget-object v0, p0, Lcom/squareup/moshi/m$e;->e:Lcom/squareup/moshi/m;

    iget v0, v0, Lcom/squareup/moshi/m;->e:I

    iput v0, p0, Lcom/squareup/moshi/m$e;->d:I

    return-void

    .line 783
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
