.class public abstract Lcom/squareup/moshi/g;
.super Ljava/lang/Object;
.source "JsonReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/moshi/g$b;,
        Lcom/squareup/moshi/g$a;
    }
.end annotation


# instance fields
.field a:I

.field b:[I

.field c:[Ljava/lang/String;

.field d:[I

.field e:Z

.field f:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x20

    .line 201
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/squareup/moshi/g;->b:[I

    .line 202
    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lcom/squareup/moshi/g;->c:[Ljava/lang/String;

    .line 203
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/squareup/moshi/g;->d:[I

    return-void
.end method

.method constructor <init>(Lcom/squareup/moshi/g;)V
    .locals 1

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    iget v0, p1, Lcom/squareup/moshi/g;->a:I

    iput v0, p0, Lcom/squareup/moshi/g;->a:I

    .line 209
    iget-object v0, p1, Lcom/squareup/moshi/g;->b:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/squareup/moshi/g;->b:[I

    .line 210
    iget-object v0, p1, Lcom/squareup/moshi/g;->c:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/moshi/g;->c:[Ljava/lang/String;

    .line 211
    iget-object v0, p1, Lcom/squareup/moshi/g;->d:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/squareup/moshi/g;->d:[I

    .line 212
    iget-boolean v0, p1, Lcom/squareup/moshi/g;->e:Z

    iput-boolean v0, p0, Lcom/squareup/moshi/g;->e:Z

    .line 213
    iget-boolean p1, p1, Lcom/squareup/moshi/g;->f:Z

    iput-boolean p1, p0, Lcom/squareup/moshi/g;->f:Z

    return-void
.end method

.method public static a(Lb/e;)Lcom/squareup/moshi/g;
    .locals 1
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 196
    new-instance v0, Lcom/squareup/moshi/i;

    invoke-direct {v0, p0}, Lcom/squareup/moshi/i;-><init>(Lb/e;)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/squareup/moshi/g$a;)I
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method final a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;
    .locals 2

    .line 233
    new-instance v0, Lcom/squareup/moshi/JsonEncodingException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " at path "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/moshi/JsonEncodingException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a(I)V
    .locals 3

    .line 217
    iget v0, p0, Lcom/squareup/moshi/g;->a:I

    iget-object v1, p0, Lcom/squareup/moshi/g;->b:[I

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 218
    iget v0, p0, Lcom/squareup/moshi/g;->a:I

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    .line 221
    iget-object v0, p0, Lcom/squareup/moshi/g;->b:[I

    iget-object v1, p0, Lcom/squareup/moshi/g;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/g;->b:[I

    .line 222
    iget-object v0, p0, Lcom/squareup/moshi/g;->c:[Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/moshi/g;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/moshi/g;->c:[Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lcom/squareup/moshi/g;->d:[I

    iget-object v1, p0, Lcom/squareup/moshi/g;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/g;->d:[I

    goto :goto_0

    .line 219
    :cond_0
    new-instance p1, Lcom/squareup/moshi/JsonDataException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Nesting too deep at "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 225
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/moshi/g;->b:[I

    iget v1, p0, Lcom/squareup/moshi/g;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/moshi/g;->a:I

    aput p1, v0, v1

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 272
    iput-boolean p1, p0, Lcom/squareup/moshi/g;->e:Z

    return-void
.end method

.method public final a()Z
    .locals 1
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 279
    iget-boolean v0, p0, Lcom/squareup/moshi/g;->e:Z

    return v0
.end method

.method public abstract b(Lcom/squareup/moshi/g$a;)I
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public final b(Z)V
    .locals 0

    .line 291
    iput-boolean p1, p0, Lcom/squareup/moshi/g;->f:Z

    return-void
.end method

.method public final b()Z
    .locals 1
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 298
    iget-boolean v0, p0, Lcom/squareup/moshi/g;->f:Z

    return v0
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()Z
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract h()Lcom/squareup/moshi/g$b;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract i()Ljava/lang/String;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract j()V
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public abstract l()Z
.end method

.method public abstract m()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()D
.end method

.method public abstract o()J
.end method

.method public abstract p()I
.end method

.method public abstract q()V
.end method

.method public abstract r()Lcom/squareup/moshi/g;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public final s()Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 507
    iget v0, p0, Lcom/squareup/moshi/g;->a:I

    iget-object v1, p0, Lcom/squareup/moshi/g;->b:[I

    iget-object v2, p0, Lcom/squareup/moshi/g;->c:[Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/moshi/g;->d:[I

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/moshi/h;->a(I[I[Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract t()V
.end method
