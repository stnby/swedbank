.class public final Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "Rfc3339DateJsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 36
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;->b(Lcom/squareup/moshi/g;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p2, Ljava/util/Date;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/util/Date;)V

    return-void
.end method

.method public declared-synchronized a(Lcom/squareup/moshi/l;Ljava/util/Date;)V
    .locals 0

    monitor-enter p0

    .line 43
    :try_start_0
    invoke-static {p2}, Lcom/squareup/moshi/adapters/a;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    .line 44
    invoke-virtual {p1, p2}, Lcom/squareup/moshi/l;->b(Ljava/lang/String;)Lcom/squareup/moshi/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 42
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized b(Lcom/squareup/moshi/g;)Ljava/util/Date;
    .locals 0

    monitor-enter p0

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->k()Ljava/lang/String;

    move-result-object p1

    .line 39
    invoke-static {p1}, Lcom/squareup/moshi/adapters/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 37
    monitor-exit p0

    throw p1
.end method
