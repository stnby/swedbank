.class final Lcom/squareup/moshi/m$c;
.super Ljava/util/AbstractSet;
.source "LinkedHashTreeMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet<",
        "Ljava/util/Map$Entry<",
        "TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/squareup/moshi/m;


# direct methods
.method constructor <init>(Lcom/squareup/moshi/m;)V
    .locals 0

    .line 791
    iput-object p1, p0, Lcom/squareup/moshi/m$c;->a:Lcom/squareup/moshi/m;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 822
    iget-object v0, p0, Lcom/squareup/moshi/m$c;->a:Lcom/squareup/moshi/m;

    invoke-virtual {v0}, Lcom/squareup/moshi/m;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .line 805
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/moshi/m$c;->a:Lcom/squareup/moshi/m;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/m;->a(Ljava/util/Map$Entry;)Lcom/squareup/moshi/m$f;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/util/Map$Entry<",
            "TK;TV;>;>;"
        }
    .end annotation

    .line 797
    new-instance v0, Lcom/squareup/moshi/m$c$1;

    invoke-direct {v0, p0}, Lcom/squareup/moshi/m$c$1;-><init>(Lcom/squareup/moshi/m$c;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .line 809
    instance-of v0, p1, Ljava/util/Map$Entry;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/m$c;->a:Lcom/squareup/moshi/m;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/m;->a(Ljava/util/Map$Entry;)Lcom/squareup/moshi/m$f;

    move-result-object p1

    if-nez p1, :cond_1

    return v1

    .line 817
    :cond_1
    iget-object v0, p0, Lcom/squareup/moshi/m$c;->a:Lcom/squareup/moshi/m;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/m;->a(Lcom/squareup/moshi/m$f;Z)V

    return v1
.end method

.method public size()I
    .locals 1

    .line 793
    iget-object v0, p0, Lcom/squareup/moshi/m$c;->a:Lcom/squareup/moshi/m;

    iget v0, v0, Lcom/squareup/moshi/m;->d:I

    return v0
.end method
