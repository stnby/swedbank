.class final Lcom/squareup/moshi/i;
.super Lcom/squareup/moshi/g;
.source "JsonUtf8Reader.java"


# static fields
.field private static final g:Lb/f;

.field private static final h:Lb/f;

.field private static final i:Lb/f;

.field private static final j:Lb/f;

.field private static final k:Lb/f;


# instance fields
.field private final l:Lb/e;

.field private final m:Lb/c;

.field private n:I

.field private o:J

.field private p:I

.field private q:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "\'\\"

    .line 29
    invoke-static {v0}, Lb/f;->a(Ljava/lang/String;)Lb/f;

    move-result-object v0

    sput-object v0, Lcom/squareup/moshi/i;->g:Lb/f;

    const-string v0, "\"\\"

    .line 30
    invoke-static {v0}, Lb/f;->a(Ljava/lang/String;)Lb/f;

    move-result-object v0

    sput-object v0, Lcom/squareup/moshi/i;->h:Lb/f;

    const-string v0, "{}[]:, \n\t\r\u000c/\\;#="

    .line 32
    invoke-static {v0}, Lb/f;->a(Ljava/lang/String;)Lb/f;

    move-result-object v0

    sput-object v0, Lcom/squareup/moshi/i;->i:Lb/f;

    const-string v0, "\n\r"

    .line 33
    invoke-static {v0}, Lb/f;->a(Ljava/lang/String;)Lb/f;

    move-result-object v0

    sput-object v0, Lcom/squareup/moshi/i;->j:Lb/f;

    const-string v0, "*/"

    .line 34
    invoke-static {v0}, Lb/f;->a(Ljava/lang/String;)Lb/f;

    move-result-object v0

    sput-object v0, Lcom/squareup/moshi/i;->k:Lb/f;

    return-void
.end method

.method constructor <init>(Lb/e;)V
    .locals 1

    .line 92
    invoke-direct {p0}, Lcom/squareup/moshi/g;-><init>()V

    const/4 v0, 0x0

    .line 72
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    if-eqz p1, :cond_0

    .line 96
    iput-object p1, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    .line 97
    invoke-interface {p1}, Lb/e;->d()Lb/c;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    const/4 p1, 0x6

    .line 98
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/i;->a(I)V

    return-void

    .line 94
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "source == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method constructor <init>(Lcom/squareup/moshi/i;)V
    .locals 3

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/moshi/g;-><init>(Lcom/squareup/moshi/g;)V

    const/4 v0, 0x0

    .line 72
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    .line 105
    iget-object v0, p1, Lcom/squareup/moshi/i;->l:Lb/e;

    invoke-interface {v0}, Lb/e;->g()Lb/e;

    move-result-object v0

    .line 106
    iput-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    .line 107
    invoke-interface {v0}, Lb/e;->d()Lb/c;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    .line 108
    iget v1, p1, Lcom/squareup/moshi/i;->n:I

    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 109
    iget-wide v1, p1, Lcom/squareup/moshi/i;->o:J

    iput-wide v1, p0, Lcom/squareup/moshi/i;->o:J

    .line 110
    iget v1, p1, Lcom/squareup/moshi/i;->p:I

    iput v1, p0, Lcom/squareup/moshi/i;->p:I

    .line 111
    iget-object v1, p1, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    iput-object v1, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 116
    :try_start_0
    iget-object p1, p1, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {p1}, Lb/c;->b()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lb/e;->a(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 118
    :catch_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method private A()V
    .locals 5

    .line 1061
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    sget-object v1, Lcom/squareup/moshi/i;->j:Lb/f;

    invoke-interface {v0, v1}, Lb/e;->c(Lb/f;)J

    move-result-wide v0

    .line 1062
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x1

    add-long/2addr v0, v3

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0}, Lb/c;->b()J

    move-result-wide v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Lb/c;->i(J)V

    return-void
.end method

.method private B()Z
    .locals 6

    .line 1069
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    sget-object v1, Lcom/squareup/moshi/i;->k:Lb/f;

    invoke-interface {v0, v1}, Lb/e;->b(Lb/f;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 1071
    :goto_0
    iget-object v3, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    if-eqz v2, :cond_1

    sget-object v4, Lcom/squareup/moshi/i;->k:Lb/f;

    invoke-virtual {v4}, Lb/f;->h()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v0, v4

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0}, Lb/c;->b()J

    move-result-wide v0

    :goto_1
    invoke-virtual {v3, v0, v1}, Lb/c;->i(J)V

    return v2
.end method

.method private C()C
    .locals 9

    .line 1091
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    const-wide/16 v1, 0x1

    invoke-interface {v0, v1, v2}, Lb/e;->b(J)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1095
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0}, Lb/c;->j()B

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_a

    const/16 v2, 0x22

    if-eq v0, v2, :cond_a

    const/16 v2, 0x27

    if-eq v0, v2, :cond_a

    const/16 v2, 0x2f

    if-eq v0, v2, :cond_a

    const/16 v2, 0x5c

    if-eq v0, v2, :cond_a

    const/16 v2, 0x62

    if-eq v0, v2, :cond_9

    const/16 v2, 0x66

    if-eq v0, v2, :cond_8

    const/16 v3, 0x6e

    if-eq v0, v3, :cond_7

    const/16 v3, 0x72

    if-eq v0, v3, :cond_6

    packed-switch v0, :pswitch_data_0

    .line 1142
    iget-boolean v1, p0, Lcom/squareup/moshi/i;->e:Z

    if-eqz v1, :cond_0

    int-to-char v0, v0

    return v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid escape sequence: \\"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v0

    throw v0

    .line 1098
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    const-wide/16 v3, 0x4

    invoke-interface {v0, v3, v4}, Lb/e;->b(J)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x4

    if-ge v0, v6, :cond_4

    .line 1104
    iget-object v6, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    int-to-long v7, v0

    invoke-virtual {v6, v7, v8}, Lb/c;->c(J)B

    move-result v6

    shl-int/lit8 v5, v5, 0x4

    int-to-char v5, v5

    const/16 v7, 0x30

    if-lt v6, v7, :cond_1

    const/16 v7, 0x39

    if-gt v6, v7, :cond_1

    add-int/lit8 v6, v6, -0x30

    add-int/2addr v5, v6

    int-to-char v5, v5

    goto :goto_1

    :cond_1
    const/16 v7, 0x61

    if-lt v6, v7, :cond_2

    if-gt v6, v2, :cond_2

    add-int/lit8 v6, v6, -0x61

    add-int/2addr v6, v1

    add-int/2addr v5, v6

    int-to-char v5, v5

    goto :goto_1

    :cond_2
    const/16 v7, 0x41

    if-lt v6, v7, :cond_3

    const/16 v7, 0x46

    if-gt v6, v7, :cond_3

    add-int/lit8 v6, v6, -0x41

    add-int/2addr v6, v1

    add-int/2addr v5, v6

    int-to-char v5, v5

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1113
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\\u"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1, v3, v4}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v0

    throw v0

    .line 1116
    :cond_4
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0, v3, v4}, Lb/c;->i(J)V

    return v5

    .line 1099
    :cond_5
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unterminated escape sequence at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/16 v0, 0x9

    return v0

    :cond_6
    const/16 v0, 0xd

    return v0

    :cond_7
    return v1

    :cond_8
    const/16 v0, 0xc

    return v0

    :cond_9
    const/16 v0, 0x8

    return v0

    :cond_a
    int-to-char v0, v0

    return v0

    :cond_b
    const-string v0, "Unterminated escape sequence"

    .line 1092
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x74
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lcom/squareup/moshi/g$a;)I
    .locals 4

    .line 613
    iget-object v0, p2, Lcom/squareup/moshi/g$a;->a:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 614
    iget-object v3, p2, Lcom/squareup/moshi/g$a;->a:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 615
    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 616
    iget-object p2, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v0, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v0, v0, -0x1

    aput-object p1, p2, v0

    return v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private a(Lb/f;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    .line 828
    :goto_0
    iget-object v1, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    invoke-interface {v1, p1}, Lb/e;->c(Lb/f;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-eqz v3, :cond_3

    .line 832
    iget-object v3, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v3, v1, v2}, Lb/c;->c(J)B

    move-result v3

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_1

    if-nez v0, :cond_0

    .line 833
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 834
    :cond_0
    iget-object v3, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v3, v1, v2}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    iget-object v1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 836
    invoke-direct {p0}, Lcom/squareup/moshi/i;->C()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    .line 842
    iget-object p1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {p1, v1, v2}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object p1

    .line 843
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0}, Lb/c;->j()B

    return-object p1

    .line 846
    :cond_2
    iget-object p1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {p1, v1, v2}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    iget-object p1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {p1}, Lb/c;->j()B

    .line 848
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    const-string p1, "Unterminated string"

    .line 829
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object p1

    throw p1
.end method

.method private b(Ljava/lang/String;Lcom/squareup/moshi/g$a;)I
    .locals 4

    .line 688
    iget-object v0, p2, Lcom/squareup/moshi/g$a;->a:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 689
    iget-object v3, p2, Lcom/squareup/moshi/g$a;->a:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 690
    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 691
    iget-object p1, p0, Lcom/squareup/moshi/i;->d:[I

    iget p2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 p2, p2, -0x1

    aget v0, p1, p2

    add-int/lit8 v0, v0, 0x1

    aput v0, p1, p2

    return v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private b(Lb/f;)V
    .locals 6

    .line 861
    :goto_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    invoke-interface {v0, p1}, Lb/e;->c(Lb/f;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 864
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v2, v0, v1}, Lb/c;->c(J)B

    move-result v2

    const/16 v3, 0x5c

    const-wide/16 v4, 0x1

    if-ne v2, v3, :cond_0

    .line 865
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    add-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Lb/c;->i(J)V

    .line 866
    invoke-direct {p0}, Lcom/squareup/moshi/i;->C()C

    goto :goto_0

    .line 868
    :cond_0
    iget-object p1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    add-long/2addr v0, v4

    invoke-virtual {p1, v0, v1}, Lb/c;->i(J)V

    return-void

    :cond_1
    const-string p1, "Unterminated string"

    .line 862
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object p1

    throw p1
.end method

.method private b(I)Z
    .locals 0

    sparse-switch p1, :sswitch_data_0

    const/4 p1, 0x1

    return p1

    .line 510
    :sswitch_0
    invoke-direct {p0}, Lcom/squareup/moshi/i;->z()V

    :sswitch_1
    const/4 p1, 0x0

    return p1

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private c(Z)I
    .locals 6

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    .line 996
    :goto_1
    iget-object v2, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    add-int/lit8 v3, v1, 0x1

    int-to-long v4, v3

    invoke-interface {v2, v4, v5}, Lb/e;->b(J)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 997
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lb/c;->c(J)B

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_7

    const/16 v2, 0x20

    if-eq v1, v2, :cond_7

    const/16 v2, 0xd

    if-eq v1, v2, :cond_7

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    goto :goto_2

    .line 1002
    :cond_0
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    add-int/lit8 v3, v3, -0x1

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Lb/c;->i(J)V

    const/16 v2, 0x2f

    if-ne v1, v2, :cond_5

    .line 1004
    iget-object v3, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    const-wide/16 v4, 0x2

    invoke-interface {v3, v4, v5}, Lb/e;->b(J)Z

    move-result v3

    if-nez v3, :cond_1

    return v1

    .line 1008
    :cond_1
    invoke-direct {p0}, Lcom/squareup/moshi/i;->z()V

    .line 1009
    iget-object v3, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5}, Lb/c;->c(J)B

    move-result v3

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_3

    if-eq v3, v2, :cond_2

    return v1

    .line 1023
    :cond_2
    iget-object v1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 1024
    iget-object v1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 1025
    invoke-direct {p0}, Lcom/squareup/moshi/i;->A()V

    goto :goto_0

    .line 1013
    :cond_3
    iget-object v1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 1014
    iget-object v1, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 1015
    invoke-direct {p0}, Lcom/squareup/moshi/i;->B()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    :cond_4
    const-string p1, "Unterminated comment"

    .line 1016
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object p1

    throw p1

    :cond_5
    const/16 v2, 0x23

    if-ne v1, v2, :cond_6

    .line 1035
    invoke-direct {p0}, Lcom/squareup/moshi/i;->z()V

    .line 1036
    invoke-direct {p0}, Lcom/squareup/moshi/i;->A()V

    goto :goto_0

    :cond_6
    return v1

    :cond_7
    :goto_2
    move v1, v3

    goto :goto_1

    :cond_8
    if-nez p1, :cond_9

    const/4 p1, -0x1

    return p1

    .line 1043
    :cond_9
    new-instance p1, Ljava/io/EOFException;

    const-string v0, "End of input"

    invoke-direct {p1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private u()I
    .locals 16

    move-object/from16 v0, p0

    .line 231
    iget-object v1, v0, Lcom/squareup/moshi/i;->b:[I

    iget v2, v0, Lcom/squareup/moshi/i;->a:I

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    aget v1, v1, v2

    const-wide/16 v4, 0x0

    const/16 v2, 0x8

    const/4 v6, 0x3

    const/16 v7, 0x5d

    const/16 v9, 0x22

    const/4 v10, 0x7

    const/16 v11, 0x3b

    const/16 v12, 0x2c

    const/4 v13, 0x4

    const/4 v14, 0x2

    if-ne v1, v3, :cond_0

    .line 233
    iget-object v15, v0, Lcom/squareup/moshi/i;->b:[I

    iget v8, v0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v8, v3

    aput v14, v15, v8

    goto/16 :goto_0

    :cond_0
    if-ne v1, v14, :cond_3

    .line 236
    invoke-direct {v0, v3}, Lcom/squareup/moshi/i;->c(Z)I

    move-result v8

    .line 237
    iget-object v15, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v15}, Lb/c;->j()B

    if-eq v8, v12, :cond_a

    if-eq v8, v11, :cond_2

    if-ne v8, v7, :cond_1

    .line 240
    iput v13, v0, Lcom/squareup/moshi/i;->n:I

    return v13

    :cond_1
    const-string v1, "Unterminated array"

    .line 246
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v1

    throw v1

    .line 242
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    goto :goto_0

    :cond_3
    const/4 v8, 0x5

    if-eq v1, v6, :cond_17

    if-ne v1, v8, :cond_4

    goto/16 :goto_2

    :cond_4
    if-ne v1, v13, :cond_6

    .line 290
    iget-object v15, v0, Lcom/squareup/moshi/i;->b:[I

    iget v14, v0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v14, v3

    aput v8, v15, v14

    .line 292
    invoke-direct {v0, v3}, Lcom/squareup/moshi/i;->c(Z)I

    move-result v8

    .line 293
    iget-object v14, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v14}, Lb/c;->j()B

    const/16 v14, 0x3a

    if-eq v8, v14, :cond_a

    const/16 v14, 0x3d

    if-ne v8, v14, :cond_5

    .line 298
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    .line 299
    iget-object v8, v0, Lcom/squareup/moshi/i;->l:Lb/e;

    const-wide/16 v14, 0x1

    invoke-interface {v8, v14, v15}, Lb/e;->b(J)Z

    move-result v8

    if-eqz v8, :cond_a

    iget-object v8, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v8, v4, v5}, Lb/c;->c(J)B

    move-result v8

    const/16 v14, 0x3e

    if-ne v8, v14, :cond_a

    .line 300
    iget-object v8, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v8}, Lb/c;->j()B

    goto :goto_0

    :cond_5
    const-string v1, "Expected \':\'"

    .line 304
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v1

    throw v1

    :cond_6
    const/4 v8, 0x6

    if-ne v1, v8, :cond_7

    .line 307
    iget-object v8, v0, Lcom/squareup/moshi/i;->b:[I

    iget v14, v0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v14, v3

    aput v10, v8, v14

    goto :goto_0

    :cond_7
    if-ne v1, v10, :cond_9

    const/4 v8, 0x0

    .line 309
    invoke-direct {v0, v8}, Lcom/squareup/moshi/i;->c(Z)I

    move-result v8

    const/4 v14, -0x1

    if-ne v8, v14, :cond_8

    const/16 v1, 0x12

    .line 311
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    .line 313
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    goto :goto_0

    :cond_9
    if-eq v1, v2, :cond_16

    .line 319
    :cond_a
    :goto_0
    invoke-direct {v0, v3}, Lcom/squareup/moshi/i;->c(Z)I

    move-result v8

    if-eq v8, v9, :cond_15

    const/16 v9, 0x27

    if-eq v8, v9, :cond_14

    if-eq v8, v12, :cond_11

    if-eq v8, v11, :cond_11

    const/16 v2, 0x5b

    if-eq v8, v2, :cond_10

    if-eq v8, v7, :cond_f

    const/16 v1, 0x7b

    if-eq v8, v1, :cond_e

    .line 352
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->v()I

    move-result v1

    if-eqz v1, :cond_b

    return v1

    .line 357
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->w()I

    move-result v1

    if-eqz v1, :cond_c

    return v1

    .line 362
    :cond_c
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1, v4, v5}, Lb/c;->c(J)B

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/i;->b(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 366
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    const/16 v1, 0xa

    .line 367
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    :cond_d
    const-string v1, "Expected value"

    .line 363
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v1

    throw v1

    .line 347
    :cond_e
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 348
    iput v3, v0, Lcom/squareup/moshi/i;->n:I

    return v3

    :cond_f
    if-ne v1, v3, :cond_11

    .line 323
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 324
    iput v13, v0, Lcom/squareup/moshi/i;->n:I

    return v13

    .line 344
    :cond_10
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 345
    iput v6, v0, Lcom/squareup/moshi/i;->n:I

    return v6

    :cond_11
    if-eq v1, v3, :cond_13

    const/4 v2, 0x2

    if-ne v1, v2, :cond_12

    goto :goto_1

    :cond_12
    const-string v1, "Unexpected value"

    .line 334
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v1

    throw v1

    .line 331
    :cond_13
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    .line 332
    iput v10, v0, Lcom/squareup/moshi/i;->n:I

    return v10

    .line 337
    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    .line 338
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 339
    iput v2, v0, Lcom/squareup/moshi/i;->n:I

    return v2

    .line 341
    :cond_15
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    const/16 v1, 0x9

    .line 342
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    .line 316
    :cond_16
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "JsonReader is closed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 249
    :cond_17
    :goto_2
    iget-object v2, v0, Lcom/squareup/moshi/i;->b:[I

    iget v4, v0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v4, v3

    aput v13, v2, v4

    const/16 v2, 0x7d

    if-ne v1, v8, :cond_1a

    .line 252
    invoke-direct {v0, v3}, Lcom/squareup/moshi/i;->c(Z)I

    move-result v4

    .line 253
    iget-object v5, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v5}, Lb/c;->j()B

    if-eq v4, v12, :cond_1a

    if-eq v4, v11, :cond_19

    if-ne v4, v2, :cond_18

    const/4 v1, 0x2

    .line 256
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    :cond_18
    const-string v1, "Unterminated object"

    .line 262
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v1

    throw v1

    .line 258
    :cond_19
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    .line 265
    :cond_1a
    invoke-direct {v0, v3}, Lcom/squareup/moshi/i;->c(Z)I

    move-result v3

    if-eq v3, v9, :cond_1f

    const/16 v4, 0x27

    if-eq v3, v4, :cond_1e

    if-eq v3, v2, :cond_1c

    .line 282
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    int-to-char v1, v3

    .line 283
    invoke-direct {v0, v1}, Lcom/squareup/moshi/i;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1b

    const/16 v1, 0xe

    .line 284
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    :cond_1b
    const-string v1, "Expected name"

    .line 286
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v1

    throw v1

    :cond_1c
    if-eq v1, v8, :cond_1d

    .line 276
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    const/4 v1, 0x2

    .line 277
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    :cond_1d
    const-string v1, "Expected name"

    .line 279
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v1

    throw v1

    .line 271
    :cond_1e
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    .line 272
    invoke-direct/range {p0 .. p0}, Lcom/squareup/moshi/i;->z()V

    const/16 v1, 0xc

    .line 273
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    .line 268
    :cond_1f
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v1}, Lb/c;->j()B

    const/16 v1, 0xd

    .line 269
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1
.end method

.method private v()I
    .locals 10

    .line 372
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lb/c;->c(J)B

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x74

    if-eq v0, v2, :cond_5

    const/16 v2, 0x54

    if-ne v0, v2, :cond_0

    goto :goto_2

    :cond_0
    const/16 v2, 0x66

    if-eq v0, v2, :cond_4

    const/16 v2, 0x46

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x6e

    if-eq v0, v2, :cond_3

    const/16 v2, 0x4e

    if-ne v0, v2, :cond_2

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_0
    const-string v0, "null"

    const-string v2, "NULL"

    const/4 v3, 0x7

    goto :goto_3

    :cond_4
    :goto_1
    const-string v0, "false"

    const-string v2, "FALSE"

    const/4 v3, 0x6

    goto :goto_3

    :cond_5
    :goto_2
    const-string v0, "true"

    const-string v2, "TRUE"

    const/4 v3, 0x5

    .line 393
    :goto_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    :goto_4
    if-ge v5, v4, :cond_8

    .line 395
    iget-object v6, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    add-int/lit8 v7, v5, 0x1

    int-to-long v8, v7

    invoke-interface {v6, v8, v9}, Lb/e;->b(J)Z

    move-result v6

    if-nez v6, :cond_6

    return v1

    .line 398
    :cond_6
    iget-object v6, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    int-to-long v8, v5

    invoke-virtual {v6, v8, v9}, Lb/c;->c(J)B

    move-result v6

    .line 399
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-eq v6, v8, :cond_7

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v6, v5, :cond_7

    return v1

    :cond_7
    move v5, v7

    goto :goto_4

    .line 404
    :cond_8
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    add-int/lit8 v2, v4, 0x1

    int-to-long v5, v2

    invoke-interface {v0, v5, v6}, Lb/e;->b(J)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    int-to-long v5, v4

    invoke-virtual {v0, v5, v6}, Lb/c;->c(J)B

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->b(I)Z

    move-result v0

    if-eqz v0, :cond_9

    return v1

    .line 409
    :cond_9
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    int-to-long v1, v4

    invoke-virtual {v0, v1, v2}, Lb/c;->i(J)V

    .line 410
    iput v3, p0, Lcom/squareup/moshi/i;->n:I

    return v3
.end method

.method private w()I
    .locals 16

    move-object/from16 v0, p0

    const-wide/16 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-wide v7, v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 423
    :goto_0
    iget-object v11, v0, Lcom/squareup/moshi/i;->l:Lb/e;

    add-int/lit8 v12, v5, 0x1

    int-to-long v13, v12

    invoke-interface {v11, v13, v14}, Lb/e;->b(J)Z

    move-result v11

    const/4 v15, 0x2

    if-nez v11, :cond_0

    goto/16 :goto_7

    .line 427
    :cond_0
    iget-object v11, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    int-to-long v13, v5

    invoke-virtual {v11, v13, v14}, Lb/c;->c(J)B

    move-result v11

    const/16 v13, 0x2b

    const/4 v14, 0x5

    if-eq v11, v13, :cond_19

    const/16 v13, 0x45

    if-eq v11, v13, :cond_16

    const/16 v13, 0x65

    if-eq v11, v13, :cond_16

    packed-switch v11, :pswitch_data_0

    const/16 v13, 0x30

    if-lt v11, v13, :cond_b

    const/16 v13, 0x39

    if-le v11, v13, :cond_1

    goto :goto_6

    :cond_1
    if-eq v6, v3, :cond_a

    if-nez v6, :cond_2

    goto :goto_5

    :cond_2
    if-ne v6, v15, :cond_6

    cmp-long v5, v7, v1

    if-nez v5, :cond_3

    return v4

    :cond_3
    const-wide/16 v13, 0xa

    mul-long v13, v13, v7

    add-int/lit8 v11, v11, -0x30

    int-to-long v3, v11

    sub-long/2addr v13, v3

    const-wide v3, -0xcccccccccccccccL

    cmp-long v3, v7, v3

    if-gtz v3, :cond_5

    if-nez v3, :cond_4

    cmp-long v3, v13, v7

    if-gez v3, :cond_4

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    :cond_5
    :goto_1
    const/4 v3, 0x1

    :goto_2
    and-int/2addr v3, v9

    move v9, v3

    move-wide v7, v13

    goto :goto_3

    :cond_6
    const/4 v3, 0x3

    if-ne v6, v3, :cond_7

    const/4 v4, 0x0

    const/4 v6, 0x4

    goto/16 :goto_c

    :cond_7
    if-eq v6, v14, :cond_9

    const/4 v3, 0x6

    if-ne v6, v3, :cond_8

    goto :goto_4

    :cond_8
    :goto_3
    const/4 v4, 0x0

    goto/16 :goto_c

    :cond_9
    :goto_4
    const/4 v4, 0x0

    const/4 v6, 0x7

    goto/16 :goto_c

    :cond_a
    :goto_5
    add-int/lit8 v11, v11, -0x30

    neg-int v3, v11

    int-to-long v3, v3

    move-wide v7, v3

    const/4 v4, 0x0

    const/4 v6, 0x2

    goto :goto_c

    .line 464
    :cond_b
    :goto_6
    invoke-direct {v0, v11}, Lcom/squareup/moshi/i;->b(I)Z

    move-result v3

    if-nez v3, :cond_12

    :goto_7
    if-ne v6, v15, :cond_f

    if-eqz v9, :cond_f

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v3, v7, v3

    if-nez v3, :cond_c

    if-eqz v10, :cond_f

    :cond_c
    cmp-long v1, v7, v1

    if-nez v1, :cond_d

    if-nez v10, :cond_f

    :cond_d
    if-eqz v10, :cond_e

    goto :goto_8

    :cond_e
    neg-long v7, v7

    .line 491
    :goto_8
    iput-wide v7, v0, Lcom/squareup/moshi/i;->o:J

    .line 492
    iget-object v1, v0, Lcom/squareup/moshi/i;->m:Lb/c;

    int-to-long v2, v5

    invoke-virtual {v1, v2, v3}, Lb/c;->i(J)V

    const/16 v1, 0x10

    .line 493
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    :cond_f
    if-eq v6, v15, :cond_11

    const/4 v1, 0x4

    if-eq v6, v1, :cond_11

    const/4 v1, 0x7

    if-ne v6, v1, :cond_10

    goto :goto_9

    :cond_10
    const/4 v4, 0x0

    return v4

    .line 496
    :cond_11
    :goto_9
    iput v5, v0, Lcom/squareup/moshi/i;->p:I

    const/16 v1, 0x11

    .line 497
    iput v1, v0, Lcom/squareup/moshi/i;->n:I

    return v1

    :cond_12
    const/4 v4, 0x0

    return v4

    :pswitch_0
    const/4 v3, 0x3

    if-ne v6, v15, :cond_13

    const/4 v6, 0x3

    goto :goto_c

    :cond_13
    return v4

    :pswitch_1
    const/4 v3, 0x6

    if-nez v6, :cond_14

    const/4 v6, 0x1

    const/4 v10, 0x1

    goto :goto_c

    :cond_14
    if-ne v6, v14, :cond_15

    goto :goto_b

    :cond_15
    return v4

    :cond_16
    if-eq v6, v15, :cond_18

    const/4 v3, 0x4

    if-ne v6, v3, :cond_17

    goto :goto_a

    :cond_17
    return v4

    :cond_18
    :goto_a
    const/4 v6, 0x5

    goto :goto_c

    :cond_19
    const/4 v3, 0x6

    if-ne v6, v14, :cond_1a

    :goto_b
    const/4 v6, 0x6

    :goto_c
    move v5, v12

    const/4 v3, 0x1

    goto/16 :goto_0

    :cond_1a
    return v4

    nop

    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private x()Ljava/lang/String;
    .locals 4

    .line 855
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    sget-object v1, Lcom/squareup/moshi/i;->i:Lb/f;

    invoke-interface {v0, v1}, Lb/e;->c(Lb/f;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 856
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v2, v0, v1}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0}, Lb/c;->r()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private y()V
    .locals 5

    .line 875
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    sget-object v1, Lcom/squareup/moshi/i;->i:Lb/f;

    invoke-interface {v0, v1}, Lb/e;->c(Lb/f;)J

    move-result-wide v0

    .line 876
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0}, Lb/c;->b()J

    move-result-wide v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Lb/c;->i(J)V

    return-void
.end method

.method private z()V
    .locals 1

    .line 1050
    iget-boolean v0, p0, Lcom/squareup/moshi/i;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    .line 1051
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;)Lcom/squareup/moshi/JsonEncodingException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/g$a;)I
    .locals 4

    .line 551
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 553
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0xc

    const/4 v2, -0x1

    if-lt v0, v1, :cond_5

    const/16 v1, 0xf

    if-le v0, v1, :cond_1

    goto :goto_0

    :cond_1
    if-ne v0, v1, :cond_2

    .line 559
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;Lcom/squareup/moshi/g$a;)I

    move-result p1

    return p1

    .line 562
    :cond_2
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    iget-object v3, p1, Lcom/squareup/moshi/g$a;->b:Lb/m;

    invoke-interface {v0, v3}, Lb/e;->a(Lb/m;)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x0

    .line 564
    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 565
    iget-object v1, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v2, v2, -0x1

    iget-object p1, p1, Lcom/squareup/moshi/g$a;->a:[Ljava/lang/String;

    aget-object p1, p1, v0

    aput-object p1, v1, v2

    return v0

    .line 572
    :cond_3
    iget-object v0, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v3, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v0, v3

    .line 574
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->i()Ljava/lang/String;

    move-result-object v3

    .line 575
    invoke-direct {p0, v3, p1}, Lcom/squareup/moshi/i;->a(Ljava/lang/String;Lcom/squareup/moshi/g$a;)I

    move-result p1

    if-ne p1, v2, :cond_4

    .line 578
    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 579
    iput-object v3, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 581
    iget-object v1, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    :cond_4
    return p1

    :cond_5
    :goto_0
    return v2
.end method

.method public b(Lcom/squareup/moshi/g$a;)I
    .locals 4

    .line 652
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 654
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0x8

    const/4 v2, -0x1

    if-lt v0, v1, :cond_5

    const/16 v1, 0xb

    if-le v0, v1, :cond_1

    goto :goto_0

    :cond_1
    if-ne v0, v1, :cond_2

    .line 660
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/moshi/i;->b(Ljava/lang/String;Lcom/squareup/moshi/g$a;)I

    move-result p1

    return p1

    .line 663
    :cond_2
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    iget-object v3, p1, Lcom/squareup/moshi/g$a;->b:Lb/m;

    invoke-interface {v0, v3}, Lb/e;->a(Lb/m;)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 p1, 0x0

    .line 665
    iput p1, p0, Lcom/squareup/moshi/i;->n:I

    .line 666
    iget-object p1, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, p1, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, p1, v1

    return v0

    .line 671
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->k()Ljava/lang/String;

    move-result-object v0

    .line 672
    invoke-direct {p0, v0, p1}, Lcom/squareup/moshi/i;->b(Ljava/lang/String;Lcom/squareup/moshi/g$a;)I

    move-result p1

    if-ne p1, v2, :cond_4

    .line 675
    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 676
    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 677
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1

    :cond_4
    return p1

    :cond_5
    :goto_0
    return v2
.end method

.method public c()V
    .locals 3

    .line 123
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 128
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(I)V

    .line 129
    iget-object v1, p0, Lcom/squareup/moshi/i;->d:[I

    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v2, v0

    const/4 v0, 0x0

    aput v0, v1, v2

    .line 130
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    return-void

    .line 132
    :cond_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 3

    const/4 v0, 0x0

    .line 935
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    .line 936
    iget-object v1, p0, Lcom/squareup/moshi/i;->b:[I

    const/16 v2, 0x8

    aput v2, v1, v0

    const/4 v0, 0x1

    .line 937
    iput v0, p0, Lcom/squareup/moshi/i;->a:I

    .line 938
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    invoke-virtual {v0}, Lb/c;->u()V

    .line 939
    iget-object v0, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    invoke-interface {v0}, Lb/e;->close()V

    return-void
.end method

.method public d()V
    .locals 3

    .line 138
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 143
    iget v0, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/moshi/i;->a:I

    .line 144
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    const/4 v0, 0x0

    .line 145
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    return-void

    .line 147
    :cond_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()V
    .locals 3

    .line 153
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    .line 158
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(I)V

    const/4 v0, 0x0

    .line 159
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    return-void

    .line 161
    :cond_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()V
    .locals 3

    .line 167
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 169
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 172
    iget v0, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/moshi/i;->a:I

    .line 173
    iget-object v0, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 174
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    const/4 v0, 0x0

    .line 175
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    return-void

    .line 177
    :cond_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g()Z
    .locals 2

    .line 183
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 185
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/16 v1, 0x12

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public h()Lcom/squareup/moshi/g$b;
    .locals 1

    .line 191
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 193
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 226
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 224
    :pswitch_0
    sget-object v0, Lcom/squareup/moshi/g$b;->j:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 222
    :pswitch_1
    sget-object v0, Lcom/squareup/moshi/g$b;->g:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 209
    :pswitch_2
    sget-object v0, Lcom/squareup/moshi/g$b;->e:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 219
    :pswitch_3
    sget-object v0, Lcom/squareup/moshi/g$b;->f:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 214
    :pswitch_4
    sget-object v0, Lcom/squareup/moshi/g$b;->i:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 212
    :pswitch_5
    sget-object v0, Lcom/squareup/moshi/g$b;->h:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 204
    :pswitch_6
    sget-object v0, Lcom/squareup/moshi/g$b;->b:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 202
    :pswitch_7
    sget-object v0, Lcom/squareup/moshi/g$b;->a:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 200
    :pswitch_8
    sget-object v0, Lcom/squareup/moshi/g$b;->d:Lcom/squareup/moshi/g$b;

    return-object v0

    .line 198
    :pswitch_9
    sget-object v0, Lcom/squareup/moshi/g$b;->c:Lcom/squareup/moshi/g$b;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public i()Ljava/lang/String;
    .locals 3

    .line 529
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 531
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    .line 535
    invoke-direct {p0}, Lcom/squareup/moshi/i;->x()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v1, 0xd

    if-ne v0, v1, :cond_2

    .line 537
    sget-object v0, Lcom/squareup/moshi/i;->h:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    .line 539
    sget-object v0, Lcom/squareup/moshi/i;->g:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/16 v1, 0xf

    if-ne v0, v1, :cond_4

    .line 541
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    :goto_0
    const/4 v1, 0x0

    .line 545
    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 546
    iget-object v1, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    return-object v0

    .line 543
    :cond_4
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public j()V
    .locals 3

    .line 588
    iget-boolean v0, p0, Lcom/squareup/moshi/i;->f:Z

    if-nez v0, :cond_5

    .line 591
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 593
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    .line 596
    invoke-direct {p0}, Lcom/squareup/moshi/i;->y()V

    goto :goto_0

    :cond_1
    const/16 v1, 0xd

    if-ne v0, v1, :cond_2

    .line 598
    sget-object v0, Lcom/squareup/moshi/i;->h:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->b(Lb/f;)V

    goto :goto_0

    :cond_2
    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    .line 600
    sget-object v0, Lcom/squareup/moshi/i;->g:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->b(Lb/f;)V

    goto :goto_0

    :cond_3
    const/16 v1, 0xf

    if-ne v0, v1, :cond_4

    :goto_0
    const/4 v0, 0x0

    .line 604
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    .line 605
    iget-object v0, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    const-string v2, "null"

    aput-object v2, v0, v1

    return-void

    .line 602
    :cond_4
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 589
    :cond_5
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot skip unexpected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public k()Ljava/lang/String;
    .locals 4

    .line 625
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 627
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 631
    invoke-direct {p0}, Lcom/squareup/moshi/i;->x()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    .line 633
    sget-object v0, Lcom/squareup/moshi/i;->h:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 635
    sget-object v0, Lcom/squareup/moshi/i;->g:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/16 v1, 0xb

    if-ne v0, v1, :cond_4

    .line 637
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    const/4 v1, 0x0

    .line 638
    iput-object v1, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 640
    iget-wide v0, p0, Lcom/squareup/moshi/i;->o:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/16 v1, 0x11

    if-ne v0, v1, :cond_6

    .line 642
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    iget v1, p0, Lcom/squareup/moshi/i;->p:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    .line 646
    iput v1, p0, Lcom/squareup/moshi/i;->n:I

    .line 647
    iget-object v1, p0, Lcom/squareup/moshi/i;->d:[I

    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    return-object v0

    .line 644
    :cond_6
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a string but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public l()Z
    .locals 5

    .line 700
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 702
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_1

    .line 705
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 706
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v1, v3

    aget v2, v0, v1

    add-int/2addr v2, v3

    aput v2, v0, v1

    return v3

    :cond_1
    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    .line 709
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 710
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v1, v3

    aget v4, v0, v1

    add-int/2addr v4, v3

    aput v4, v0, v1

    return v2

    .line 713
    :cond_2
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a boolean but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public m()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .line 717
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 719
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .line 722
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    .line 723
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    const/4 v0, 0x0

    return-object v0

    .line 726
    :cond_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected null but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public n()D
    .locals 6

    .line 731
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 733
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0x10

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 737
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 738
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 739
    iget-wide v0, p0, Lcom/squareup/moshi/i;->o:J

    long-to-double v0, v0

    return-wide v0

    :cond_1
    const/16 v1, 0x11

    const/16 v3, 0xb

    if-ne v0, v1, :cond_2

    .line 743
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    iget v1, p0, Lcom/squareup/moshi/i;->p:I

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 745
    sget-object v0, Lcom/squareup/moshi/i;->h:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 747
    sget-object v0, Lcom/squareup/moshi/i;->g:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const/16 v1, 0xa

    if-ne v0, v1, :cond_5

    .line 749
    invoke-direct {p0}, Lcom/squareup/moshi/i;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    goto :goto_0

    :cond_5
    if-ne v0, v3, :cond_8

    .line 754
    :goto_0
    iput v3, p0, Lcom/squareup/moshi/i;->n:I

    .line 757
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 762
    iget-boolean v3, p0, Lcom/squareup/moshi/i;->e:Z

    if-nez v3, :cond_7

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_1

    .line 763
    :cond_6
    new-instance v2, Lcom/squareup/moshi/JsonEncodingException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, " at path "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 764
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/moshi/JsonEncodingException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_7
    :goto_1
    const/4 v3, 0x0

    .line 766
    iput-object v3, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 767
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 768
    iget-object v2, p0, Lcom/squareup/moshi/i;->d:[I

    iget v3, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    return-wide v0

    .line 759
    :catch_0
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a double but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 760
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 751
    :cond_8
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a double but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public o()J
    .locals 7

    .line 773
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 775
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0x10

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 779
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 780
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 781
    iget-wide v0, p0, Lcom/squareup/moshi/i;->o:J

    return-wide v0

    :cond_1
    const/16 v1, 0x11

    const/16 v3, 0xb

    if-ne v0, v1, :cond_2

    .line 785
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    iget v1, p0, Lcom/squareup/moshi/i;->p:I

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    goto :goto_2

    :cond_2
    const/16 v1, 0x9

    if-eq v0, v1, :cond_5

    const/16 v4, 0x8

    if-ne v0, v4, :cond_3

    goto :goto_0

    :cond_3
    if-ne v0, v3, :cond_4

    goto :goto_2

    .line 799
    :cond_4
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 800
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_0
    if-ne v0, v1, :cond_6

    .line 788
    sget-object v0, Lcom/squareup/moshi/i;->h:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 789
    :cond_6
    sget-object v0, Lcom/squareup/moshi/i;->g:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 791
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 792
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 793
    iget-object v4, p0, Lcom/squareup/moshi/i;->d:[I

    iget v5, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v5, v5, -0x1

    aget v6, v4, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v4, v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    .line 803
    :catch_0
    :goto_2
    iput v3, p0, Lcom/squareup/moshi/i;->n:I

    .line 806
    :try_start_1
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 807
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v3, 0x0

    .line 812
    iput-object v3, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 813
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 814
    iget-object v2, p0, Lcom/squareup/moshi/i;->d:[I

    iget v3, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    return-wide v0

    .line 809
    :catch_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 810
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public p()I
    .locals 7

    .line 880
    iget v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v0, :cond_0

    .line 882
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v0

    :cond_0
    const/16 v1, 0x10

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    .line 887
    iget-wide v0, p0, Lcom/squareup/moshi/i;->o:J

    long-to-int v0, v0

    .line 888
    iget-wide v3, p0, Lcom/squareup/moshi/i;->o:J

    int-to-long v5, v0

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    .line 892
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 893
    iget-object v1, p0, Lcom/squareup/moshi/i;->d:[I

    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    return v0

    .line 889
    :cond_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/squareup/moshi/i;->o:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 890
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/16 v1, 0x11

    const/16 v3, 0xb

    if-ne v0, v1, :cond_3

    .line 898
    iget-object v0, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    iget v1, p0, Lcom/squareup/moshi/i;->p:I

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Lb/c;->e(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const/16 v1, 0x9

    if-eq v0, v1, :cond_6

    const/16 v4, 0x8

    if-ne v0, v4, :cond_4

    goto :goto_0

    :cond_4
    if-ne v0, v3, :cond_5

    goto :goto_2

    .line 912
    :cond_5
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_0
    if-ne v0, v1, :cond_7

    .line 901
    sget-object v0, Lcom/squareup/moshi/i;->h:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 902
    :cond_7
    sget-object v0, Lcom/squareup/moshi/i;->g:Lb/f;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/i;->a(Lb/f;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 904
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 905
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 906
    iget-object v1, p0, Lcom/squareup/moshi/i;->d:[I

    iget v4, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v4, v4, -0x1

    aget v5, v1, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v1, v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 915
    :catch_0
    :goto_2
    iput v3, p0, Lcom/squareup/moshi/i;->n:I

    .line 918
    :try_start_1
    iget-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    double-to-int v3, v0

    int-to-double v4, v3

    cmpl-double v0, v4, v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    .line 928
    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    .line 929
    iput v2, p0, Lcom/squareup/moshi/i;->n:I

    .line 930
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return v3

    .line 925
    :cond_8
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 926
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 920
    :catch_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public q()V
    .locals 7

    .line 943
    iget-boolean v0, p0, Lcom/squareup/moshi/i;->f:Z

    if-nez v0, :cond_d

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 948
    :cond_0
    iget v2, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v2, :cond_1

    .line 950
    invoke-direct {p0}, Lcom/squareup/moshi/i;->u()I

    move-result v2

    :cond_1
    const/4 v3, 0x3

    const/4 v4, 0x1

    if-ne v2, v3, :cond_2

    .line 954
    invoke-virtual {p0, v4}, Lcom/squareup/moshi/i;->a(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    if-ne v2, v4, :cond_3

    .line 957
    invoke-virtual {p0, v3}, Lcom/squareup/moshi/i;->a(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 960
    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 963
    iget v2, p0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/squareup/moshi/i;->a:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_5
    const/16 v3, 0xe

    if-eq v2, v3, :cond_b

    const/16 v3, 0xa

    if-ne v2, v3, :cond_6

    goto :goto_2

    :cond_6
    const/16 v3, 0x9

    if-eq v2, v3, :cond_a

    const/16 v3, 0xd

    if-ne v2, v3, :cond_7

    goto :goto_1

    :cond_7
    const/16 v3, 0x8

    if-eq v2, v3, :cond_9

    const/16 v3, 0xc

    if-ne v2, v3, :cond_8

    goto :goto_0

    :cond_8
    const/16 v3, 0x11

    if-ne v2, v3, :cond_c

    .line 972
    iget-object v2, p0, Lcom/squareup/moshi/i;->m:Lb/c;

    iget v3, p0, Lcom/squareup/moshi/i;->p:I

    int-to-long v5, v3

    invoke-virtual {v2, v5, v6}, Lb/c;->i(J)V

    goto :goto_3

    .line 970
    :cond_9
    :goto_0
    sget-object v2, Lcom/squareup/moshi/i;->g:Lb/f;

    invoke-direct {p0, v2}, Lcom/squareup/moshi/i;->b(Lb/f;)V

    goto :goto_3

    .line 968
    :cond_a
    :goto_1
    sget-object v2, Lcom/squareup/moshi/i;->h:Lb/f;

    invoke-direct {p0, v2}, Lcom/squareup/moshi/i;->b(Lb/f;)V

    goto :goto_3

    .line 966
    :cond_b
    :goto_2
    invoke-direct {p0}, Lcom/squareup/moshi/i;->y()V

    .line 974
    :cond_c
    :goto_3
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    if-nez v1, :cond_0

    .line 977
    iget-object v0, p0, Lcom/squareup/moshi/i;->d:[I

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v1, v4

    aget v2, v0, v1

    add-int/2addr v2, v4

    aput v2, v0, v1

    .line 978
    iget-object v0, p0, Lcom/squareup/moshi/i;->c:[Ljava/lang/String;

    iget v1, p0, Lcom/squareup/moshi/i;->a:I

    sub-int/2addr v1, v4

    const-string v2, "null"

    aput-object v2, v0, v1

    return-void

    .line 944
    :cond_d
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot skip unexpected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->h()Lcom/squareup/moshi/g$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/i;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public r()Lcom/squareup/moshi/g;
    .locals 1

    .line 1076
    new-instance v0, Lcom/squareup/moshi/i;

    invoke-direct {v0, p0}, Lcom/squareup/moshi/i;-><init>(Lcom/squareup/moshi/i;)V

    return-object v0
.end method

.method t()V
    .locals 1

    .line 1148
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149
    invoke-virtual {p0}, Lcom/squareup/moshi/i;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/i;->q:Ljava/lang/String;

    const/16 v0, 0xb

    .line 1150
    iput v0, p0, Lcom/squareup/moshi/i;->n:I

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1080
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JsonReader("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/moshi/i;->l:Lb/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
