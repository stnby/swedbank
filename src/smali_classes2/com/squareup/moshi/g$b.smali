.class public final enum Lcom/squareup/moshi/g$b;
.super Ljava/lang/Enum;
.source "JsonReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/moshi/g$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/squareup/moshi/g$b;

.field public static final enum b:Lcom/squareup/moshi/g$b;

.field public static final enum c:Lcom/squareup/moshi/g$b;

.field public static final enum d:Lcom/squareup/moshi/g$b;

.field public static final enum e:Lcom/squareup/moshi/g$b;

.field public static final enum f:Lcom/squareup/moshi/g$b;

.field public static final enum g:Lcom/squareup/moshi/g$b;

.field public static final enum h:Lcom/squareup/moshi/g$b;

.field public static final enum i:Lcom/squareup/moshi/g$b;

.field public static final enum j:Lcom/squareup/moshi/g$b;

.field private static final synthetic k:[Lcom/squareup/moshi/g$b;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 554
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "BEGIN_ARRAY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->a:Lcom/squareup/moshi/g$b;

    .line 560
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "END_ARRAY"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->b:Lcom/squareup/moshi/g$b;

    .line 566
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "BEGIN_OBJECT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->c:Lcom/squareup/moshi/g$b;

    .line 572
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "END_OBJECT"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->d:Lcom/squareup/moshi/g$b;

    .line 579
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "NAME"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->e:Lcom/squareup/moshi/g$b;

    .line 584
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "STRING"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->f:Lcom/squareup/moshi/g$b;

    .line 590
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "NUMBER"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->g:Lcom/squareup/moshi/g$b;

    .line 595
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "BOOLEAN"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->h:Lcom/squareup/moshi/g$b;

    .line 600
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "NULL"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->i:Lcom/squareup/moshi/g$b;

    .line 607
    new-instance v0, Lcom/squareup/moshi/g$b;

    const-string v1, "END_DOCUMENT"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/squareup/moshi/g$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/moshi/g$b;->j:Lcom/squareup/moshi/g$b;

    const/16 v0, 0xa

    .line 548
    new-array v0, v0, [Lcom/squareup/moshi/g$b;

    sget-object v1, Lcom/squareup/moshi/g$b;->a:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/moshi/g$b;->b:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/moshi/g$b;->c:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/moshi/g$b;->d:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/moshi/g$b;->e:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/moshi/g$b;->f:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/moshi/g$b;->g:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/moshi/g$b;->h:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/moshi/g$b;->i:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/moshi/g$b;->j:Lcom/squareup/moshi/g$b;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/moshi/g$b;->k:[Lcom/squareup/moshi/g$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 548
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/moshi/g$b;
    .locals 1

    .line 548
    const-class v0, Lcom/squareup/moshi/g$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/moshi/g$b;

    return-object p0
.end method

.method public static values()[Lcom/squareup/moshi/g$b;
    .locals 1

    .line 548
    sget-object v0, Lcom/squareup/moshi/g$b;->k:[Lcom/squareup/moshi/g$b;

    invoke-virtual {v0}, [Lcom/squareup/moshi/g$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/moshi/g$b;

    return-object v0
.end method
