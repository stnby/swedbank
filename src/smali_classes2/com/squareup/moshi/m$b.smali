.class Lcom/squareup/moshi/m$b;
.super Ljava/lang/Object;
.source "LinkedHashTreeMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lcom/squareup/moshi/m$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/m$f<",
            "TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .line 622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/squareup/moshi/m$f;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/moshi/m$f<",
            "TK;TV;>;"
        }
    .end annotation

    .line 636
    iget-object v0, p0, Lcom/squareup/moshi/m$b;->a:Lcom/squareup/moshi/m$f;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 641
    :cond_0
    iget-object v2, v0, Lcom/squareup/moshi/m$f;->a:Lcom/squareup/moshi/m$f;

    .line 642
    iput-object v1, v0, Lcom/squareup/moshi/m$f;->a:Lcom/squareup/moshi/m$f;

    .line 643
    iget-object v1, v0, Lcom/squareup/moshi/m$f;->c:Lcom/squareup/moshi/m$f;

    :goto_0
    move-object v3, v2

    move-object v2, v1

    move-object v1, v3

    if-eqz v2, :cond_1

    .line 644
    iput-object v1, v2, Lcom/squareup/moshi/m$f;->a:Lcom/squareup/moshi/m$f;

    .line 643
    iget-object v1, v2, Lcom/squareup/moshi/m$f;->b:Lcom/squareup/moshi/m$f;

    goto :goto_0

    .line 647
    :cond_1
    iput-object v1, p0, Lcom/squareup/moshi/m$b;->a:Lcom/squareup/moshi/m$f;

    return-object v0
.end method

.method a(Lcom/squareup/moshi/m$f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/m$f<",
            "TK;TV;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    move-object v1, v0

    move-object v0, p1

    move-object p1, v1

    if-eqz v0, :cond_0

    .line 629
    iput-object p1, v0, Lcom/squareup/moshi/m$f;->a:Lcom/squareup/moshi/m$f;

    .line 628
    iget-object p1, v0, Lcom/squareup/moshi/m$f;->b:Lcom/squareup/moshi/m$f;

    goto :goto_0

    .line 632
    :cond_0
    iput-object p1, p0, Lcom/squareup/moshi/m$b;->a:Lcom/squareup/moshi/m$f;

    return-void
.end method
