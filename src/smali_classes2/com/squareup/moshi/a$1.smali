.class Lcom/squareup/moshi/a$1;
.super Lcom/squareup/moshi/JsonAdapter;
.source "AdapterMethodsFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/moshi/a;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Lcom/squareup/moshi/n;)Lcom/squareup/moshi/JsonAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/squareup/moshi/a$a;

.field final synthetic b:Lcom/squareup/moshi/JsonAdapter;

.field final synthetic c:Lcom/squareup/moshi/n;

.field final synthetic d:Lcom/squareup/moshi/a$a;

.field final synthetic e:Ljava/util/Set;

.field final synthetic f:Ljava/lang/reflect/Type;

.field final synthetic g:Lcom/squareup/moshi/a;


# direct methods
.method constructor <init>(Lcom/squareup/moshi/a;Lcom/squareup/moshi/a$a;Lcom/squareup/moshi/JsonAdapter;Lcom/squareup/moshi/n;Lcom/squareup/moshi/a$a;Ljava/util/Set;Ljava/lang/reflect/Type;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/moshi/a$1;->g:Lcom/squareup/moshi/a;

    iput-object p2, p0, Lcom/squareup/moshi/a$1;->a:Lcom/squareup/moshi/a$a;

    iput-object p3, p0, Lcom/squareup/moshi/a$1;->b:Lcom/squareup/moshi/JsonAdapter;

    iput-object p4, p0, Lcom/squareup/moshi/a$1;->c:Lcom/squareup/moshi/n;

    iput-object p5, p0, Lcom/squareup/moshi/a$1;->d:Lcom/squareup/moshi/a$a;

    iput-object p6, p0, Lcom/squareup/moshi/a$1;->e:Ljava/util/Set;

    iput-object p7, p0, Lcom/squareup/moshi/a$1;->f:Ljava/lang/reflect/Type;

    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->d:Lcom/squareup/moshi/a$a;

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->b:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->d:Lcom/squareup/moshi/a$a;

    iget-boolean v0, v0, Lcom/squareup/moshi/a$a;->l:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/moshi/g;->h()Lcom/squareup/moshi/g$b;

    move-result-object v0

    sget-object v1, Lcom/squareup/moshi/g$b;->i:Lcom/squareup/moshi/g$b;

    if-ne v0, v1, :cond_1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->m()Ljava/lang/Object;

    const/4 p1, 0x0

    return-object p1

    .line 90
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->d:Lcom/squareup/moshi/a$a;

    iget-object v1, p0, Lcom/squareup/moshi/a$1;->c:Lcom/squareup/moshi/n;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/moshi/a$a;->a(Lcom/squareup/moshi/n;Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 92
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 93
    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 94
    :cond_2
    new-instance v1, Lcom/squareup/moshi/JsonDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 67
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->a:Lcom/squareup/moshi/a$a;

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->b:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->a:Lcom/squareup/moshi/a$a;

    iget-boolean v0, v0, Lcom/squareup/moshi/a$a;->l:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->e()Lcom/squareup/moshi/l;

    goto :goto_0

    .line 73
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/a$1;->a:Lcom/squareup/moshi/a$a;

    iget-object v1, p0, Lcom/squareup/moshi/a$1;->c:Lcom/squareup/moshi/n;

    invoke-virtual {v0, v1, p1, p2}, Lcom/squareup/moshi/a$a;->a(Lcom/squareup/moshi/n;Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception p2

    .line 75
    invoke-virtual {p2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    .line 76
    instance-of v0, p2, Ljava/io/IOException;

    if-eqz v0, :cond_2

    check-cast p2, Ljava/io/IOException;

    throw p2

    .line 77
    :cond_2
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/moshi/l;->m()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JsonAdapter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/moshi/a$1;->e:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/moshi/a$1;->f:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
