.class public final Lcom/squareup/moshi/Rfc3339DateJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "Rfc3339DateJsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# instance fields
.field delegate:Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    .line 26
    new-instance v0, Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;

    invoke-direct {v0}, Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;-><init>()V

    iput-object v0, p0, Lcom/squareup/moshi/Rfc3339DateJsonAdapter;->delegate:Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/Rfc3339DateJsonAdapter;->b(Lcom/squareup/moshi/g;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p2, Ljava/util/Date;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/moshi/Rfc3339DateJsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/util/Date;)V

    return-void
.end method

.method public a(Lcom/squareup/moshi/l;Ljava/util/Date;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/moshi/Rfc3339DateJsonAdapter;->delegate:Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/util/Date;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Ljava/util/Date;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/moshi/Rfc3339DateJsonAdapter;->delegate:Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/adapters/Rfc3339DateJsonAdapter;->b(Lcom/squareup/moshi/g;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method
