.class public abstract Lcom/squareup/moshi/l;
.super Ljava/lang/Object;
.source "JsonWriter.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# instance fields
.field b:I

.field c:[I

.field d:[Ljava/lang/String;

.field e:[I

.field f:Ljava/lang/String;

.field g:Z

.field h:Z

.field i:Z

.field j:I


# direct methods
.method constructor <init>()V
    .locals 2

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 131
    iput v0, p0, Lcom/squareup/moshi/l;->b:I

    const/16 v0, 0x20

    .line 132
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/squareup/moshi/l;->c:[I

    .line 133
    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lcom/squareup/moshi/l;->d:[Ljava/lang/String;

    .line 134
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/squareup/moshi/l;->e:[I

    const/4 v0, -0x1

    .line 163
    iput v0, p0, Lcom/squareup/moshi/l;->j:I

    return-void
.end method

.method public static a(Lb/d;)Lcom/squareup/moshi/l;
    .locals 1
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 167
    new-instance v0, Lcom/squareup/moshi/j;

    invoke-direct {v0, p0}, Lcom/squareup/moshi/j;-><init>(Lb/d;)V

    return-object v0
.end method


# virtual methods
.method public abstract a()Lcom/squareup/moshi/l;
.end method

.method public abstract a(D)Lcom/squareup/moshi/l;
.end method

.method public abstract a(J)Lcom/squareup/moshi/l;
.end method

.method public abstract a(Ljava/lang/Number;)Lcom/squareup/moshi/l;
    .param p1    # Ljava/lang/Number;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Ljava/lang/String;)Lcom/squareup/moshi/l;
.end method

.method public abstract a(Z)Lcom/squareup/moshi/l;
.end method

.method final a(I)V
    .locals 3

    .line 202
    iget-object v0, p0, Lcom/squareup/moshi/l;->c:[I

    iget v1, p0, Lcom/squareup/moshi/l;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/moshi/l;->b:I

    aput p1, v0, v1

    return-void
.end method

.method public abstract b()Lcom/squareup/moshi/l;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/squareup/moshi/l;
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method final b(I)V
    .locals 2

    .line 207
    iget-object v0, p0, Lcom/squareup/moshi/l;->c:[I

    iget v1, p0, Lcom/squareup/moshi/l;->b:I

    add-int/lit8 v1, v1, -0x1

    aput p1, v0, v1

    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 243
    iput-boolean p1, p0, Lcom/squareup/moshi/l;->g:Z

    return-void
.end method

.method public abstract c()Lcom/squareup/moshi/l;
.end method

.method public final c(I)V
    .locals 0

    .line 461
    iput p1, p0, Lcom/squareup/moshi/l;->j:I

    return-void
.end method

.method public final c(Z)V
    .locals 0

    .line 258
    iput-boolean p1, p0, Lcom/squareup/moshi/l;->h:Z

    return-void
.end method

.method public abstract d()Lcom/squareup/moshi/l;
.end method

.method public abstract e()Lcom/squareup/moshi/l;
.end method

.method final g()I
    .locals 2

    .line 176
    iget v0, p0, Lcom/squareup/moshi/l;->b:I

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/moshi/l;->c:[I

    iget v1, p0, Lcom/squareup/moshi/l;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0

    .line 177
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonWriter is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final h()Z
    .locals 3

    .line 184
    iget v0, p0, Lcom/squareup/moshi/l;->b:I

    iget-object v1, p0, Lcom/squareup/moshi/l;->c:[I

    array-length v1, v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    .line 186
    :cond_0
    iget v0, p0, Lcom/squareup/moshi/l;->b:I

    const/16 v1, 0x100

    if-eq v0, v1, :cond_2

    .line 190
    iget-object v0, p0, Lcom/squareup/moshi/l;->c:[I

    iget-object v1, p0, Lcom/squareup/moshi/l;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/l;->c:[I

    .line 191
    iget-object v0, p0, Lcom/squareup/moshi/l;->d:[Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/moshi/l;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/moshi/l;->d:[Ljava/lang/String;

    .line 192
    iget-object v0, p0, Lcom/squareup/moshi/l;->e:[I

    iget-object v1, p0, Lcom/squareup/moshi/l;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/l;->e:[I

    .line 193
    instance-of v0, p0, Lcom/squareup/moshi/k;

    if-eqz v0, :cond_1

    .line 194
    move-object v0, p0

    check-cast v0, Lcom/squareup/moshi/k;

    iget-object v1, v0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget-object v2, v0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    .line 195
    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    :cond_1
    const/4 v0, 0x1

    return v0

    .line 187
    :cond_2
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Nesting too deep at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/moshi/l;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ": circular reference?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final i()Z
    .locals 1
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 250
    iget-boolean v0, p0, Lcom/squareup/moshi/l;->g:Z

    return v0
.end method

.method public final j()Z
    .locals 1
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 266
    iget-boolean v0, p0, Lcom/squareup/moshi/l;->h:Z

    return v0
.end method

.method final k()V
    .locals 2

    .line 375
    invoke-virtual {p0}, Lcom/squareup/moshi/l;->g()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 377
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 379
    iput-boolean v0, p0, Lcom/squareup/moshi/l;->i:Z

    return-void
.end method

.method public final l()I
    .locals 2
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 449
    invoke-virtual {p0}, Lcom/squareup/moshi/l;->g()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 452
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_1
    :goto_0
    iget v0, p0, Lcom/squareup/moshi/l;->j:I

    .line 455
    iget v1, p0, Lcom/squareup/moshi/l;->b:I

    iput v1, p0, Lcom/squareup/moshi/l;->j:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .line 469
    iget v0, p0, Lcom/squareup/moshi/l;->b:I

    iget-object v1, p0, Lcom/squareup/moshi/l;->c:[I

    iget-object v2, p0, Lcom/squareup/moshi/l;->d:[Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/moshi/l;->e:[I

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/moshi/h;->a(I[I[Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
