.class final Lcom/squareup/moshi/j;
.super Lcom/squareup/moshi/l;
.source "JsonUtf8Writer.java"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final k:Lb/d;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x80

    .line 46
    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0x1f

    if-gt v1, v2, :cond_0

    .line 48
    sget-object v2, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const-string v3, "\\u%04x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_0
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0x22

    const-string v2, "\\\""

    aput-object v2, v0, v1

    .line 51
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0x5c

    const-string v2, "\\\\"

    aput-object v2, v0, v1

    .line 52
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0x9

    const-string v2, "\\t"

    aput-object v2, v0, v1

    .line 53
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0x8

    const-string v2, "\\b"

    aput-object v2, v0, v1

    .line 54
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0xa

    const-string v2, "\\n"

    aput-object v2, v0, v1

    .line 55
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0xd

    const-string v2, "\\r"

    aput-object v2, v0, v1

    .line 56
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0xc

    const-string v2, "\\f"

    aput-object v2, v0, v1

    return-void
.end method

.method constructor <init>(Lb/d;)V
    .locals 1

    .line 67
    invoke-direct {p0}, Lcom/squareup/moshi/l;-><init>()V

    const-string v0, ":"

    .line 63
    iput-object v0, p0, Lcom/squareup/moshi/j;->l:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 71
    iput-object p1, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    const/4 p1, 0x6

    .line 72
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/j;->a(I)V

    return-void

    .line 69
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "sink == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(IILjava/lang/String;)Lcom/squareup/moshi/l;
    .locals 2

    .line 112
    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    iget v1, p0, Lcom/squareup/moshi/j;->j:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/moshi/j;->c:[I

    iget v1, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/squareup/moshi/j;->c:[I

    iget v1, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    if-ne v0, p2, :cond_1

    .line 115
    :cond_0
    iget p1, p0, Lcom/squareup/moshi/j;->j:I

    not-int p1, p1

    iput p1, p0, Lcom/squareup/moshi/j;->j:I

    return-object p0

    .line 118
    :cond_1
    invoke-direct {p0}, Lcom/squareup/moshi/j;->p()V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->h()Z

    .line 120
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/j;->a(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/moshi/j;->e:[I

    iget p2, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 p2, p2, -0x1

    const/4 v0, 0x0

    aput v0, p1, p2

    .line 122
    iget-object p1, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-interface {p1, p3}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    return-object p0
.end method

.method static a(Lb/d;Ljava/lang/String;)V
    .locals 7

    .line 319
    sget-object v0, Lcom/squareup/moshi/j;->a:[Ljava/lang/String;

    const/16 v1, 0x22

    .line 320
    invoke-interface {p0, v1}, Lb/d;->i(I)Lb/d;

    .line 322
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v2, :cond_5

    .line 324
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x80

    if-ge v5, v6, :cond_0

    .line 327
    aget-object v5, v0, v5

    if-nez v5, :cond_2

    goto :goto_2

    :cond_0
    const/16 v6, 0x2028

    if-ne v5, v6, :cond_1

    const-string v5, "\\u2028"

    goto :goto_1

    :cond_1
    const/16 v6, 0x2029

    if-ne v5, v6, :cond_4

    const-string v5, "\\u2029"

    :cond_2
    :goto_1
    if-ge v4, v3, :cond_3

    .line 339
    invoke-interface {p0, p1, v4, v3}, Lb/d;->b(Ljava/lang/String;II)Lb/d;

    .line 341
    :cond_3
    invoke-interface {p0, v5}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    add-int/lit8 v4, v3, 0x1

    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    if-ge v4, v2, :cond_6

    .line 345
    invoke-interface {p0, p1, v4, v2}, Lb/d;->b(Ljava/lang/String;II)Lb/d;

    .line 347
    :cond_6
    invoke-interface {p0, v1}, Lb/d;->i(I)Lb/d;

    return-void
.end method

.method private b(IILjava/lang/String;)Lcom/squareup/moshi/l;
    .locals 3

    .line 131
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->g()I

    move-result v0

    if-eq v0, p2, :cond_1

    if-ne v0, p1, :cond_0

    goto :goto_0

    .line 133
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Nesting problem."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 135
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    if-nez p1, :cond_4

    .line 138
    iget p1, p0, Lcom/squareup/moshi/j;->b:I

    iget v1, p0, Lcom/squareup/moshi/j;->j:I

    not-int v1, v1

    if-ne p1, v1, :cond_2

    .line 140
    iget p1, p0, Lcom/squareup/moshi/j;->j:I

    not-int p1, p1

    iput p1, p0, Lcom/squareup/moshi/j;->j:I

    return-object p0

    .line 144
    :cond_2
    iget p1, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/squareup/moshi/j;->b:I

    .line 145
    iget-object p1, p0, Lcom/squareup/moshi/j;->d:[Ljava/lang/String;

    iget v1, p0, Lcom/squareup/moshi/j;->b:I

    const/4 v2, 0x0

    aput-object v2, p1, v1

    .line 146
    iget-object p1, p0, Lcom/squareup/moshi/j;->e:[I

    iget v1, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v2, p1, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, p1, v1

    if-ne v0, p2, :cond_3

    .line 148
    invoke-direct {p0}, Lcom/squareup/moshi/j;->n()V

    .line 150
    :cond_3
    iget-object p1, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-interface {p1, p3}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    return-object p0

    .line 136
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Dangling name: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private f()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 173
    invoke-direct {p0}, Lcom/squareup/moshi/j;->o()V

    .line 174
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    iget-object v1, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/moshi/j;->a(Lb/d;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 175
    iput-object v0, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private n()V
    .locals 4

    .line 351
    iget-object v0, p0, Lcom/squareup/moshi/j;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lb/d;->i(I)Lb/d;

    .line 356
    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    const/4 v1, 0x1

    :goto_0
    if-ge v1, v0, :cond_1

    .line 357
    iget-object v2, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    iget-object v3, p0, Lcom/squareup/moshi/j;->f:Ljava/lang/String;

    invoke-interface {v2, v3}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private o()V
    .locals 2

    .line 366
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->g()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 368
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    const/16 v1, 0x2c

    invoke-interface {v0, v1}, Lb/d;->i(I)Lb/d;

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 372
    :goto_0
    invoke-direct {p0}, Lcom/squareup/moshi/j;->n()V

    const/4 v0, 0x4

    .line 373
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/j;->b(I)V

    return-void

    .line 370
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private p()V
    .locals 2

    .line 383
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 410
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 385
    :pswitch_1
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->g:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 386
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JSON must have only one top-level value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :goto_0
    :pswitch_2
    const/4 v0, 0x7

    .line 391
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/j;->b(I)V

    goto :goto_1

    .line 405
    :pswitch_3
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    iget-object v1, p0, Lcom/squareup/moshi/j;->l:Ljava/lang/String;

    invoke-interface {v0, v1}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    const/4 v0, 0x5

    .line 406
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/j;->b(I)V

    goto :goto_1

    .line 400
    :pswitch_4
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    const/16 v1, 0x2c

    invoke-interface {v0, v1}, Lb/d;->i(I)Lb/d;

    .line 401
    invoke-direct {p0}, Lcom/squareup/moshi/j;->n()V

    goto :goto_1

    :pswitch_5
    const/4 v0, 0x2

    .line 395
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/j;->b(I)V

    .line 396
    invoke-direct {p0}, Lcom/squareup/moshi/j;->n()V

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()Lcom/squareup/moshi/l;
    .locals 3

    .line 81
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    if-nez v0, :cond_0

    .line 85
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    const/4 v0, 0x1

    const/4 v1, 0x2

    const-string v2, "["

    .line 86
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/moshi/j;->a(IILjava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object v0

    return-object v0

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Array cannot be used as a map key in JSON at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(D)Lcom/squareup/moshi/l;
    .locals 3

    .line 232
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->g:Z

    if-nez v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 233
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Numeric values must be finite, but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    if-eqz v0, :cond_2

    .line 236
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/moshi/j;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 238
    :cond_2
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    .line 239
    invoke-direct {p0}, Lcom/squareup/moshi/j;->p()V

    .line 240
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    .line 241
    iget-object p1, p0, Lcom/squareup/moshi/j;->e:[I

    iget p2, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 p2, p2, -0x1

    aget v0, p1, p2

    add-int/lit8 v0, v0, 0x1

    aput v0, p1, p2

    return-object p0
.end method

.method public a(J)Lcom/squareup/moshi/l;
    .locals 1

    .line 246
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    if-eqz v0, :cond_0

    .line 247
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/moshi/j;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 249
    :cond_0
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    .line 250
    invoke-direct {p0}, Lcom/squareup/moshi/j;->p()V

    .line 251
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    .line 252
    iget-object p1, p0, Lcom/squareup/moshi/j;->e:[I

    iget p2, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 p2, p2, -0x1

    aget v0, p1, p2

    add-int/lit8 v0, v0, 0x1

    aput v0, p1, p2

    return-object p0
.end method

.method public a(Ljava/lang/Number;)Lcom/squareup/moshi/l;
    .locals 3
    .param p1    # Ljava/lang/Number;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->e()Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 261
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 262
    iget-boolean v1, p0, Lcom/squareup/moshi/j;->g:Z

    if-nez v1, :cond_2

    const-string v1, "-Infinity"

    .line 263
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Infinity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "NaN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 264
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Numeric values must be finite, but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_2
    :goto_0
    iget-boolean p1, p0, Lcom/squareup/moshi/j;->i:Z

    if-eqz p1, :cond_3

    .line 267
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/j;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 269
    :cond_3
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    .line 270
    invoke-direct {p0}, Lcom/squareup/moshi/j;->p()V

    .line 271
    iget-object p1, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-interface {p1, v0}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    .line 272
    iget-object p1, p0, Lcom/squareup/moshi/j;->e:[I

    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/moshi/l;
    .locals 2

    if-eqz p1, :cond_3

    .line 158
    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    if-eqz v0, :cond_2

    .line 161
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->g()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 165
    iput-object p1, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    .line 166
    iget-object v0, p0, Lcom/squareup/moshi/j;->d:[Ljava/lang/String;

    iget v1, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    const/4 p1, 0x0

    .line 167
    iput-boolean p1, p0, Lcom/squareup/moshi/j;->i:Z

    return-object p0

    .line 163
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Nesting problem."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 159
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "JsonWriter is closed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 156
    :cond_3
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "name == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Z)Lcom/squareup/moshi/l;
    .locals 2

    .line 213
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    if-nez v0, :cond_1

    .line 217
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    .line 218
    invoke-direct {p0}, Lcom/squareup/moshi/j;->p()V

    .line 219
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    if-eqz p1, :cond_0

    const-string p1, "true"

    goto :goto_0

    :cond_0
    const-string p1, "false"

    :goto_0
    invoke-interface {v0, p1}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    .line 220
    iget-object p1, p0, Lcom/squareup/moshi/j;->e:[I

    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    return-object p0

    .line 214
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Boolean cannot be used as a map key in JSON at path "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()Lcom/squareup/moshi/l;
    .locals 3

    const-string v0, "]"

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 90
    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/moshi/j;->b(IILjava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/squareup/moshi/l;
    .locals 2

    if-nez p1, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->e()Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 183
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    if-eqz v0, :cond_1

    .line 184
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/j;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 186
    :cond_1
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    .line 187
    invoke-direct {p0}, Lcom/squareup/moshi/j;->p()V

    .line 188
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-static {v0, p1}, Lcom/squareup/moshi/j;->a(Lb/d;Ljava/lang/String;)V

    .line 189
    iget-object p1, p0, Lcom/squareup/moshi/j;->e:[I

    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    return-object p0
.end method

.method public c()Lcom/squareup/moshi/l;
    .locals 3

    .line 94
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    if-nez v0, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    const/4 v0, 0x3

    const/4 v1, 0x5

    const-string v2, "{"

    .line 99
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/moshi/j;->a(IILjava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object v0

    return-object v0

    .line 95
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object cannot be used as a map key in JSON at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 3

    .line 305
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-interface {v0}, Lb/d;->close()V

    .line 307
    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    if-ne v0, v1, :cond_0

    .line 308
    iget-object v2, p0, Lcom/squareup/moshi/j;->c:[I

    sub-int/2addr v0, v1

    aget v0, v2, v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 311
    iput v0, p0, Lcom/squareup/moshi/j;->b:I

    return-void

    .line 309
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Lcom/squareup/moshi/l;
    .locals 3

    const/4 v0, 0x0

    .line 103
    iput-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    const-string v0, "}"

    const/4 v1, 0x3

    const/4 v2, 0x5

    .line 104
    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/moshi/j;->b(IILjava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/squareup/moshi/l;
    .locals 3

    .line 194
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->i:Z

    if-nez v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 199
    iget-boolean v0, p0, Lcom/squareup/moshi/j;->h:Z

    if-eqz v0, :cond_0

    .line 200
    invoke-direct {p0}, Lcom/squareup/moshi/j;->f()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 202
    iput-object v0, p0, Lcom/squareup/moshi/j;->m:Ljava/lang/String;

    return-object p0

    .line 206
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/squareup/moshi/j;->p()V

    .line 207
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    const-string v1, "null"

    invoke-interface {v0, v1}, Lb/d;->b(Ljava/lang/String;)Lb/d;

    .line 208
    iget-object v0, p0, Lcom/squareup/moshi/j;->e:[I

    iget v1, p0, Lcom/squareup/moshi/j;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-object p0

    .line 195
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "null cannot be used as a map key in JSON at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    invoke-virtual {p0}, Lcom/squareup/moshi/j;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public flush()V
    .locals 2

    .line 293
    iget v0, p0, Lcom/squareup/moshi/j;->b:I

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/squareup/moshi/j;->k:Lb/d;

    invoke-interface {v0}, Lb/d;->flush()V

    return-void

    .line 294
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonWriter is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
