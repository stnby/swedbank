.class Lcom/squareup/moshi/ClassJsonAdapter$a;
.super Ljava/lang/Object;
.source "ClassJsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/ClassJsonAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/reflect/Field;

.field final c:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/reflect/Field;Lcom/squareup/moshi/JsonAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Field;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "TT;>;)V"
        }
    .end annotation

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput-object p1, p0, Lcom/squareup/moshi/ClassJsonAdapter$a;->a:Ljava/lang/String;

    .line 189
    iput-object p2, p0, Lcom/squareup/moshi/ClassJsonAdapter$a;->b:Ljava/lang/reflect/Field;

    .line 190
    iput-object p3, p0, Lcom/squareup/moshi/ClassJsonAdapter$a;->c:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method a(Lcom/squareup/moshi/g;Ljava/lang/Object;)V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/moshi/ClassJsonAdapter$a;->c:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object p1

    .line 195
    iget-object v0, p0, Lcom/squareup/moshi/ClassJsonAdapter$a;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/moshi/ClassJsonAdapter$a;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 201
    iget-object v0, p0, Lcom/squareup/moshi/ClassJsonAdapter$a;->c:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    return-void
.end method
