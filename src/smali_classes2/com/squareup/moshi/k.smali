.class final Lcom/squareup/moshi/k;
.super Lcom/squareup/moshi/l;
.source "JsonValueWriter.java"


# instance fields
.field a:[Ljava/lang/Object;

.field private k:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 38
    invoke-direct {p0}, Lcom/squareup/moshi/l;-><init>()V

    const/16 v0, 0x20

    .line 35
    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    const/4 v0, 0x6

    .line 39
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/k;->a(I)V

    return-void
.end method

.method private a(Ljava/lang/Object;)Lcom/squareup/moshi/k;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 261
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->g()I

    move-result v0

    .line 263
    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/squareup/moshi/k;->c:[I

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    sub-int/2addr v1, v2

    const/4 v3, 0x7

    aput v3, v0, v1

    .line 268
    iget-object v0, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    sub-int/2addr v1, v2

    aput-object p1, v0, v1

    goto :goto_0

    .line 265
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "JSON must have only one top-level value."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 270
    iget-object v1, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    if-eqz v1, :cond_5

    if-nez p1, :cond_2

    .line 271
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->h:Z

    if-eqz v0, :cond_3

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    sub-int/2addr v1, v2

    aget-object v0, v0, v1

    check-cast v0, Ljava/util/Map;

    .line 274
    iget-object v1, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    const/4 p1, 0x0

    .line 280
    iput-object p1, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    goto :goto_0

    .line 276
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Map key \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\' has multiple values at path "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " and "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    if-ne v0, v2, :cond_6

    .line 284
    iget-object v0, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    sub-int/2addr v1, v2

    aget-object v0, v0, v1

    check-cast v0, Ljava/util/List;

    .line 285
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object p0

    .line 288
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Nesting problem."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Lcom/squareup/moshi/l;
    .locals 4

    .line 51
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-nez v0, :cond_1

    .line 55
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    iget v1, p0, Lcom/squareup/moshi/k;->j:I

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/moshi/k;->c:[I

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    sub-int/2addr v1, v2

    aget v0, v0, v1

    if-ne v0, v2, :cond_0

    .line 57
    iget v0, p0, Lcom/squareup/moshi/k;->j:I

    not-int v0, v0

    iput v0, p0, Lcom/squareup/moshi/k;->j:I

    return-object p0

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->h()Z

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    invoke-direct {p0, v0}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 63
    iget-object v1, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget v3, p0, Lcom/squareup/moshi/k;->b:I

    aput-object v0, v1, v3

    .line 64
    iget-object v0, p0, Lcom/squareup/moshi/k;->e:[I

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    const/4 v3, 0x0

    aput v3, v0, v1

    .line 65
    invoke-virtual {p0, v2}, Lcom/squareup/moshi/k;->a(I)V

    return-object p0

    .line 52
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Array cannot be used as a map key in JSON at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(D)Lcom/squareup/moshi/l;
    .locals 3

    .line 178
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->g:Z

    if-nez v0, :cond_1

    .line 179
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 180
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Numeric values must be finite, but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-eqz v0, :cond_2

    .line 183
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 185
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 186
    iget-object p1, p0, Lcom/squareup/moshi/k;->e:[I

    iget p2, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 p2, p2, -0x1

    aget v0, p1, p2

    add-int/lit8 v0, v0, 0x1

    aput v0, p1, p2

    return-object p0
.end method

.method public a(J)Lcom/squareup/moshi/l;
    .locals 1

    .line 191
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-eqz v0, :cond_0

    .line 192
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 194
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 195
    iget-object p1, p0, Lcom/squareup/moshi/k;->e:[I

    iget p2, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 p2, p2, -0x1

    aget v0, p1, p2

    add-int/lit8 v0, v0, 0x1

    aput v0, p1, p2

    return-object p0
.end method

.method public a(Ljava/lang/Number;)Lcom/squareup/moshi/l;
    .locals 2
    .param p1    # Ljava/lang/Number;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 201
    instance-of v0, p1, Ljava/lang/Byte;

    if-nez v0, :cond_6

    instance-of v0, p1, Ljava/lang/Short;

    if-nez v0, :cond_6

    instance-of v0, p1, Ljava/lang/Integer;

    if-nez v0, :cond_6

    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_0

    goto :goto_2

    .line 209
    :cond_0
    instance-of v0, p1, Ljava/lang/Float;

    if-nez v0, :cond_5

    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    if-nez p1, :cond_2

    .line 214
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->e()Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 218
    :cond_2
    instance-of v0, p1, Ljava/math/BigDecimal;

    if-eqz v0, :cond_3

    .line 219
    check-cast p1, Ljava/math/BigDecimal;

    goto :goto_0

    .line 220
    :cond_3
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    .line 221
    :goto_0
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-eqz v0, :cond_4

    .line 222
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 224
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 225
    iget-object p1, p0, Lcom/squareup/moshi/k;->e:[I

    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    return-object p0

    .line 210
    :cond_5
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/moshi/k;->a(D)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 205
    :cond_6
    :goto_2
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/moshi/k;->a(J)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/moshi/l;
    .locals 2

    if-eqz p1, :cond_2

    .line 126
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    if-eqz v0, :cond_1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->g()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 132
    iput-object p1, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    .line 133
    iget-object v0, p0, Lcom/squareup/moshi/k;->d:[Ljava/lang/String;

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    const/4 p1, 0x0

    .line 134
    iput-boolean p1, p0, Lcom/squareup/moshi/k;->i:Z

    return-object p0

    .line 130
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Nesting problem."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 127
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "JsonWriter is closed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 124
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "name == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Z)Lcom/squareup/moshi/l;
    .locals 2

    .line 158
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-nez v0, :cond_0

    .line 162
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 163
    iget-object p1, p0, Lcom/squareup/moshi/k;->e:[I

    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    return-object p0

    .line 159
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Boolean cannot be used as a map key in JSON at path "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()Lcom/squareup/moshi/l;
    .locals 4

    .line 70
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->g()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 73
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    iget v2, p0, Lcom/squareup/moshi/k;->j:I

    not-int v2, v2

    if-ne v0, v2, :cond_0

    .line 75
    iget v0, p0, Lcom/squareup/moshi/k;->j:I

    not-int v0, v0

    iput v0, p0, Lcom/squareup/moshi/k;->j:I

    return-object p0

    .line 78
    :cond_0
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/moshi/k;->b:I

    .line 79
    iget-object v0, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget v2, p0, Lcom/squareup/moshi/k;->b:I

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 80
    iget-object v0, p0, Lcom/squareup/moshi/k;->e:[I

    iget v2, p0, Lcom/squareup/moshi/k;->b:I

    sub-int/2addr v2, v1

    aget v3, v0, v2

    add-int/2addr v3, v1

    aput v3, v0, v2

    return-object p0

    .line 71
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/lang/String;)Lcom/squareup/moshi/l;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 139
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    move-result-object p1

    return-object p1

    .line 142
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 143
    iget-object p1, p0, Lcom/squareup/moshi/k;->e:[I

    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    return-object p0
.end method

.method public c()Lcom/squareup/moshi/l;
    .locals 4

    .line 85
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-nez v0, :cond_1

    .line 89
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    iget v1, p0, Lcom/squareup/moshi/k;->j:I

    const/4 v2, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/moshi/k;->c:[I

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    if-ne v0, v2, :cond_0

    .line 91
    iget v0, p0, Lcom/squareup/moshi/k;->j:I

    not-int v0, v0

    iput v0, p0, Lcom/squareup/moshi/k;->j:I

    return-object p0

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->h()Z

    .line 95
    new-instance v0, Lcom/squareup/moshi/m;

    invoke-direct {v0}, Lcom/squareup/moshi/m;-><init>()V

    .line 96
    invoke-direct {p0, v0}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 97
    iget-object v1, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget v3, p0, Lcom/squareup/moshi/k;->b:I

    aput-object v0, v1, v3

    .line 98
    invoke-virtual {p0, v2}, Lcom/squareup/moshi/k;->a(I)V

    return-object p0

    .line 86
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object cannot be used as a map key in JSON at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 3

    .line 247
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    if-ne v0, v1, :cond_0

    .line 248
    iget-object v2, p0, Lcom/squareup/moshi/k;->c:[I

    sub-int/2addr v0, v1

    aget v0, v2, v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 251
    iput v0, p0, Lcom/squareup/moshi/k;->b:I

    return-void

    .line 249
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Lcom/squareup/moshi/l;
    .locals 3

    .line 103
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->g()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 106
    iget-object v0, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 109
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    iget v1, p0, Lcom/squareup/moshi/k;->j:I

    not-int v1, v1

    if-ne v0, v1, :cond_0

    .line 111
    iget v0, p0, Lcom/squareup/moshi/k;->j:I

    not-int v0, v0

    iput v0, p0, Lcom/squareup/moshi/k;->j:I

    return-object p0

    :cond_0
    const/4 v0, 0x0

    .line 114
    iput-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    .line 115
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/moshi/k;->b:I

    .line 116
    iget-object v0, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 117
    iget-object v0, p0, Lcom/squareup/moshi/k;->d:[Ljava/lang/String;

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    aput-object v2, v0, v1

    .line 118
    iget-object v0, p0, Lcom/squareup/moshi/k;->e:[I

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-object p0

    .line 107
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dangling name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/moshi/k;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()Lcom/squareup/moshi/l;
    .locals 3

    .line 148
    iget-boolean v0, p0, Lcom/squareup/moshi/k;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 152
    invoke-direct {p0, v0}, Lcom/squareup/moshi/k;->a(Ljava/lang/Object;)Lcom/squareup/moshi/k;

    .line 153
    iget-object v0, p0, Lcom/squareup/moshi/k;->e:[I

    iget v1, p0, Lcom/squareup/moshi/k;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-object p0

    .line 149
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "null cannot be used as a map key in JSON at path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()Ljava/lang/Object;
    .locals 3

    .line 43
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    if-ne v0, v1, :cond_0

    .line 44
    iget-object v2, p0, Lcom/squareup/moshi/k;->c:[I

    sub-int/2addr v0, v1

    aget v0, v2, v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/k;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0

    .line 45
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public flush()V
    .locals 2

    .line 255
    iget v0, p0, Lcom/squareup/moshi/k;->b:I

    if-eqz v0, :cond_0

    return-void

    .line 256
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonWriter is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
