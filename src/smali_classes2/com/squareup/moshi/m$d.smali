.class final Lcom/squareup/moshi/m$d;
.super Ljava/util/AbstractSet;
.source "LinkedHashTreeMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet<",
        "TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/squareup/moshi/m;


# direct methods
.method constructor <init>(Lcom/squareup/moshi/m;)V
    .locals 0

    .line 826
    iput-object p1, p0, Lcom/squareup/moshi/m$d;->a:Lcom/squareup/moshi/m;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 848
    iget-object v0, p0, Lcom/squareup/moshi/m$d;->a:Lcom/squareup/moshi/m;

    invoke-virtual {v0}, Lcom/squareup/moshi/m;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .line 840
    iget-object v0, p0, Lcom/squareup/moshi/m$d;->a:Lcom/squareup/moshi/m;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/m;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TK;>;"
        }
    .end annotation

    .line 832
    new-instance v0, Lcom/squareup/moshi/m$d$1;

    invoke-direct {v0, p0}, Lcom/squareup/moshi/m$d$1;-><init>(Lcom/squareup/moshi/m$d;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .line 844
    iget-object v0, p0, Lcom/squareup/moshi/m$d;->a:Lcom/squareup/moshi/m;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/m;->b(Ljava/lang/Object;)Lcom/squareup/moshi/m$f;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public size()I
    .locals 1

    .line 828
    iget-object v0, p0, Lcom/squareup/moshi/m$d;->a:Lcom/squareup/moshi/m;

    iget v0, v0, Lcom/squareup/moshi/m;->d:I

    return v0
.end method
