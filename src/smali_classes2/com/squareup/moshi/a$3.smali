.class Lcom/squareup/moshi/a$3;
.super Lcom/squareup/moshi/a$a;
.source "AdapterMethodsFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/moshi/a;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;)Lcom/squareup/moshi/a$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:[Ljava/lang/reflect/Type;

.field final synthetic b:Ljava/lang/reflect/Type;

.field final synthetic c:Ljava/util/Set;

.field final synthetic d:Ljava/util/Set;

.field private e:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/Object;Ljava/lang/reflect/Method;IIZ[Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0

    .line 175
    iput-object p8, p0, Lcom/squareup/moshi/a$3;->a:[Ljava/lang/reflect/Type;

    iput-object p9, p0, Lcom/squareup/moshi/a$3;->b:Ljava/lang/reflect/Type;

    iput-object p10, p0, Lcom/squareup/moshi/a$3;->c:Ljava/util/Set;

    iput-object p11, p0, Lcom/squareup/moshi/a$3;->d:Ljava/util/Set;

    invoke-direct/range {p0 .. p7}, Lcom/squareup/moshi/a$a;-><init>(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/Object;Ljava/lang/reflect/Method;IIZ)V

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/n;Lcom/squareup/moshi/JsonAdapter$a;)V
    .locals 2

    .line 179
    invoke-super {p0, p1, p2}, Lcom/squareup/moshi/a$a;->a(Lcom/squareup/moshi/n;Lcom/squareup/moshi/JsonAdapter$a;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/moshi/a$3;->a:[Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/squareup/moshi/a$3;->b:Ljava/lang/reflect/Type;

    .line 180
    invoke-static {v0, v1}, Lcom/squareup/moshi/p;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/moshi/a$3;->c:Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/moshi/a$3;->d:Ljava/util/Set;

    .line 181
    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/squareup/moshi/a$3;->b:Ljava/lang/reflect/Type;

    iget-object v1, p0, Lcom/squareup/moshi/a$3;->d:Ljava/util/Set;

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/moshi/n;->a(Lcom/squareup/moshi/JsonAdapter$a;Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    goto :goto_0

    .line 183
    :cond_0
    iget-object p2, p0, Lcom/squareup/moshi/a$3;->b:Ljava/lang/reflect/Type;

    iget-object v0, p0, Lcom/squareup/moshi/a$3;->d:Ljava/util/Set;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/moshi/a$3;->e:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method

.method public a(Lcom/squareup/moshi/n;Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 188
    invoke-virtual {p0, p3}, Lcom/squareup/moshi/a$3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 189
    iget-object p3, p0, Lcom/squareup/moshi/a$3;->e:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p3, p2, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    return-void
.end method
