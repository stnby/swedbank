.class public final Lcom/siimkinks/sqlitemagic/cp$a;
.super Ljava/lang/Object;
.source "SqliteMagic_AccountData_Handler.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/a/a;
.implements Lio/reactivex/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/account/b;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/siimkinks/sqlitemagic/as;

.field private c:I

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method private constructor <init>(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/account/b;",
            ">;)V"
        }
    .end annotation

    .line 682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 675
    iput v0, p0, Lcom/siimkinks/sqlitemagic/cp$a;->c:I

    .line 677
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    .line 683
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cp$a;->a:Ljava/lang/Iterable;

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cp$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/account/b;",
            ">;)",
            "Lcom/siimkinks/sqlitemagic/cp$a;"
        }
    .end annotation

    .line 688
    new-instance v0, Lcom/siimkinks/sqlitemagic/cp$a;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/cp$a;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/siimkinks/sqlitemagic/a/a;
    .locals 1

    const/4 v0, 0x1

    .line 718
    iput-boolean v0, p0, Lcom/siimkinks/sqlitemagic/cp$a;->e:Z

    return-object p0
.end method

.method public a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Lcom/siimkinks/sqlitemagic/dt<",
            "Ljava/lang/Object;",
            ">;>(TC;)",
            "Lcom/siimkinks/sqlitemagic/a/a;"
        }
    .end annotation

    .line 710
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    check-cast p1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lio/reactivex/c;)V
    .locals 21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 835
    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/cp$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/cp$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v3, "Core"

    const/4 v4, 0x5

    .line 836
    invoke-virtual {v0, v3, v4}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v3

    .line 837
    invoke-static {}, Lio/reactivex/b/d;->a()Lio/reactivex/b/c;

    move-result-object v4

    .line 838
    invoke-interface {v2, v4}, Lio/reactivex/c;->a(Lio/reactivex/b/c;)V

    .line 839
    iget-boolean v5, v1, Lcom/siimkinks/sqlitemagic/cp$a;->e:Z

    const/16 v6, 0x8

    const/4 v14, 0x1

    const/4 v15, 0x0

    if-eqz v5, :cond_b

    .line 840
    new-instance v11, Lcom/siimkinks/sqlitemagic/ec;

    iget v5, v1, Lcom/siimkinks/sqlitemagic/cp$a;->c:I

    invoke-direct {v11, v5}, Lcom/siimkinks/sqlitemagic/ec;-><init>(I)V

    .line 841
    new-instance v10, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {v10, v6}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    .line 843
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v16

    .line 846
    :try_start_0
    iget-object v5, v1, Lcom/siimkinks/sqlitemagic/cp$a;->a:Ljava/lang/Iterable;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v17

    const/16 v18, 0x0

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v9, v5

    check-cast v9, Lcom/swedbank/mobile/data/account/b;

    .line 847
    sget-boolean v5, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v5, :cond_1

    const-string v5, "PERSIST\n  table: account_data\n  object: %s"

    new-array v6, v14, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/swedbank/mobile/data/account/b;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v15

    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 848
    :cond_1
    invoke-interface {v4}, Lio/reactivex/b/c;->b()Z

    move-result v5

    if-nez v5, :cond_7

    .line 851
    invoke-static {v9, v10}, Lcom/swedbank/mobile/data/account/n;->a(Lcom/swedbank/mobile/data/account/b;Lcom/siimkinks/sqlitemagic/b/c;)V

    const-string v5, "account_data"

    .line 853
    iget-object v6, v1, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 856
    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    :goto_2
    move-object/from16 v19, v5

    goto :goto_3

    :cond_2
    const-string v5, "local_id"

    goto :goto_2

    :goto_3
    const/4 v6, 0x1

    const-string v7, "account_data"

    const/16 v8, 0x8

    move-object v5, v11

    move-object/from16 v20, v9

    move-object v9, v10

    move-object v12, v10

    move-object/from16 v10, v19

    move-object v13, v11

    move-object v11, v3

    .line 860
    invoke-virtual/range {v5 .. v11}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v5

    invoke-interface {v5}, Landroidx/k/a/f;->a()I

    move-result v5

    if-gtz v5, :cond_6

    .line 862
    sget-boolean v5, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v5, :cond_3

    const-string v5, "PERSIST update failed; trying insertion"

    new-array v6, v15, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    const-string v5, "local_id"

    .line 863
    invoke-virtual {v12, v5}, Lcom/siimkinks/sqlitemagic/b/c;->d(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "account_data"

    const/16 v8, 0x8

    const-string v10, "local_id"

    move-object v5, v13

    move-object v9, v12

    move-object v11, v3

    .line 865
    invoke-virtual/range {v5 .. v11}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v5

    invoke-interface {v5}, Landroidx/k/a/f;->b()J

    move-result-wide v5

    .line 866
    sget-boolean v7, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v7, :cond_4

    const-string v7, "PERSIST insert id: %s"

    new-array v8, v14, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v15

    invoke-static {v7, v8}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-nez v5, :cond_6

    .line 868
    iget-boolean v5, v13, Lcom/siimkinks/sqlitemagic/ec;->a:Z

    if-eqz v5, :cond_5

    goto :goto_4

    .line 869
    :cond_5
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to persist "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v5, v20

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/16 v18, 0x1

    :goto_4
    move-object v10, v12

    move-object v11, v13

    goto/16 :goto_1

    .line 849
    :cond_7
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    const-string v3, "Subscriber unsubscribed unexpectedly"

    invoke-direct {v0, v3}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 878
    :cond_8
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 885
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v18, :cond_a

    .line 887
    sget-object v3, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/b;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    goto :goto_5

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_0
    move-exception v0

    .line 881
    :try_start_1
    invoke-interface {v4}, Lio/reactivex/b/c;->b()Z

    move-result v3

    if-nez v3, :cond_9

    .line 882
    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 885
    :cond_9
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 889
    :cond_a
    :goto_5
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    goto/16 :goto_c

    .line 885
    :goto_6
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 889
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 890
    throw v0

    .line 892
    :cond_b
    new-instance v5, Lcom/siimkinks/sqlitemagic/bo;

    iget v7, v1, Lcom/siimkinks/sqlitemagic/cp$a;->c:I

    const/4 v8, 0x2

    iget-object v9, v1, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    invoke-direct {v5, v7, v8, v9}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    const-string v7, "account_data"

    const-string v8, "UPDATE%s account_data SET account_id=?, customer_id=?, iban=?, alias=?, is_default=?, main_currency=?, ordering=? WHERE local_id=?"

    .line 893
    invoke-virtual {v5, v7, v8, v3}, Lcom/siimkinks/sqlitemagic/bo;->b(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v7

    const-string v8, "account_data"

    const-string v9, "INSERT%s INTO account_data (account_id, customer_id, iban, alias, is_default, main_currency, ordering) VALUES (?, ?, ?, ?, ?, ?, ?)"

    .line 894
    invoke-virtual {v5, v8, v9, v3}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v3

    const-string v8, "account_data"

    .line 895
    iget-object v9, v1, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    invoke-static {v8, v9}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v8

    if-eqz v8, :cond_c

    .line 898
    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    goto :goto_7

    :cond_c
    const-string v8, "local_id"

    .line 903
    :goto_7
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v9

    .line 906
    :try_start_2
    monitor-enter v7
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 907
    :try_start_3
    monitor-enter v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 908
    :try_start_4
    iget-object v10, v1, Lcom/siimkinks/sqlitemagic/cp$a;->a:Ljava/lang/Iterable;

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    const/4 v11, 0x0

    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_15

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/swedbank/mobile/data/account/b;

    .line 909
    sget-boolean v13, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v13, :cond_d

    const-string v13, "PERSIST\n  table: account_data\n  object: %s"

    new-array v6, v14, [Ljava/lang/Object;

    invoke-virtual {v12}, Lcom/swedbank/mobile/data/account/b;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v6, v15

    invoke-static {v13, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 910
    :cond_d
    invoke-interface {v4}, Lio/reactivex/b/c;->b()Z

    move-result v6

    if-nez v6, :cond_14

    .line 914
    invoke-static {v8, v12}, Lcom/swedbank/mobile/data/account/n;->a(Ljava/lang/String;Lcom/swedbank/mobile/data/account/b;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 915
    invoke-static {v7, v12}, Lcom/swedbank/mobile/data/account/n;->b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/b;)V

    const/16 v6, 0x8

    .line 916
    invoke-static {v7, v6, v8, v12}, Lcom/swedbank/mobile/data/account/n;->a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/account/b;)V

    .line 917
    invoke-interface {v7}, Landroidx/k/a/f;->a()I

    move-result v13

    goto :goto_9

    :cond_e
    const/16 v6, 0x8

    const/4 v13, 0x0

    :goto_9
    if-gtz v13, :cond_12

    .line 920
    sget-boolean v13, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v13, :cond_f

    const-string v13, "PERSIST update failed; trying insertion"

    new-array v6, v15, [Ljava/lang/Object;

    invoke-static {v13, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 921
    :cond_f
    invoke-static {v3, v12}, Lcom/swedbank/mobile/data/account/n;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/b;)V

    .line 922
    invoke-interface {v3}, Landroidx/k/a/f;->b()J

    move-result-wide v16

    .line 923
    sget-boolean v6, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v6, :cond_10

    const-string v6, "INSERT id: %s"

    new-array v13, v14, [Ljava/lang/Object;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v13, v15

    invoke-static {v6, v13}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_10
    const-wide/16 v18, -0x1

    cmp-long v6, v16, v18

    if-nez v6, :cond_13

    .line 925
    iget-boolean v6, v5, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v6, :cond_11

    goto :goto_a

    .line 926
    :cond_11
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to persist "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    const-wide/16 v18, -0x1

    :cond_13
    const/4 v11, 0x1

    :goto_a
    const/16 v6, 0x8

    goto/16 :goto_8

    .line 911
    :cond_14
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    const-string v6, "Subscriber unsubscribed unexpectedly"

    invoke-direct {v0, v6}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 935
    :cond_15
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 936
    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 937
    :try_start_6
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 944
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v11, :cond_17

    .line 946
    sget-object v3, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/b;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    goto :goto_b

    :catchall_1
    move-exception v0

    .line 935
    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    :catchall_2
    move-exception v0

    .line 936
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    move-exception v0

    goto :goto_d

    :catch_1
    move-exception v0

    .line 940
    :try_start_a
    invoke-interface {v4}, Lio/reactivex/b/c;->b()Z

    move-result v3

    if-nez v3, :cond_16

    .line 941
    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 944
    :cond_16
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 948
    :cond_17
    :goto_b
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 950
    invoke-virtual {v5}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    :goto_c
    return-void

    .line 944
    :goto_d
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 948
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 949
    throw v0
.end method

.method public synthetic b(Lcom/siimkinks/sqlitemagic/dt;)Ljava/lang/Object;
    .locals 0

    .line 669
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/cp$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object p1

    return-object p1
.end method

.method public b()Z
    .locals 20

    move-object/from16 v1, p0

    .line 724
    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/cp$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/cp$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v2, "Core"

    const/4 v3, 0x5

    .line 725
    invoke-virtual {v0, v2, v3}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v2

    .line 726
    iget-boolean v3, v1, Lcom/siimkinks/sqlitemagic/cp$a;->e:Z

    const-wide/16 v11, -0x1

    const/16 v4, 0x8

    const/4 v13, 0x1

    const/4 v14, 0x0

    if-eqz v3, :cond_a

    .line 727
    new-instance v3, Lcom/siimkinks/sqlitemagic/ec;

    iget v5, v1, Lcom/siimkinks/sqlitemagic/cp$a;->c:I

    invoke-direct {v3, v5}, Lcom/siimkinks/sqlitemagic/ec;-><init>(I)V

    .line 728
    new-instance v15, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {v15, v4}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    .line 730
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v16

    .line 733
    :try_start_0
    iget-object v4, v1, Lcom/siimkinks/sqlitemagic/cp$a;->a:Ljava/lang/Iterable;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v17

    const/16 v18, 0x0

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/swedbank/mobile/data/account/b;

    .line 734
    sget-boolean v4, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v4, :cond_1

    const-string v4, "PERSIST\n  table: account_data\n  object: %s"

    new-array v5, v13, [Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/swedbank/mobile/data/account/b;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v14

    invoke-static {v4, v5}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 735
    :cond_1
    invoke-static {v10, v15}, Lcom/swedbank/mobile/data/account/n;->a(Lcom/swedbank/mobile/data/account/b;Lcom/siimkinks/sqlitemagic/b/c;)V

    const-string v4, "account_data"

    .line 737
    iget-object v5, v1, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 740
    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    :goto_2
    move-object v9, v4

    goto :goto_3

    :cond_2
    const-string v4, "local_id"

    goto :goto_2

    :goto_3
    const/4 v5, 0x1

    const-string v6, "account_data"

    const/16 v7, 0x8

    move-object v4, v3

    move-object v8, v15

    move-object/from16 v19, v10

    move-object v10, v2

    .line 744
    invoke-virtual/range {v4 .. v10}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v4

    invoke-interface {v4}, Landroidx/k/a/f;->a()I

    move-result v4

    if-gtz v4, :cond_6

    .line 746
    sget-boolean v4, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v4, :cond_3

    const-string v4, "PERSIST update failed; trying insertion"

    new-array v5, v14, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    const-string v4, "local_id"

    .line 747
    invoke-virtual {v15, v4}, Lcom/siimkinks/sqlitemagic/b/c;->d(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "account_data"

    const/16 v7, 0x8

    const-string v9, "local_id"

    move-object v4, v3

    move-object v8, v15

    move-object v10, v2

    .line 749
    invoke-virtual/range {v4 .. v10}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v4

    invoke-interface {v4}, Landroidx/k/a/f;->b()J

    move-result-wide v4

    .line 750
    sget-boolean v6, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v6, :cond_4

    const-string v6, "PERSIST insert id: %s"

    new-array v7, v13, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v14

    invoke-static {v6, v7}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    cmp-long v4, v4, v11

    if-nez v4, :cond_6

    .line 752
    iget-boolean v4, v3, Lcom/siimkinks/sqlitemagic/ec;->a:Z

    if-eqz v4, :cond_5

    goto/16 :goto_1

    .line 753
    :cond_5
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to persist "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v4, v19

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 762
    :cond_7
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 769
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v18, :cond_8

    .line 771
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    :cond_8
    return v18

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_0
    move-exception v0

    .line 766
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_9

    const-string v2, "Operation failed"

    new-array v3, v14, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 769
    :cond_9
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return v14

    :goto_4
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 773
    throw v0

    .line 775
    :cond_a
    new-instance v3, Lcom/siimkinks/sqlitemagic/bo;

    iget v5, v1, Lcom/siimkinks/sqlitemagic/cp$a;->c:I

    const/4 v6, 0x2

    iget-object v7, v1, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    invoke-direct {v3, v5, v6, v7}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    const-string v5, "account_data"

    const-string v6, "UPDATE%s account_data SET account_id=?, customer_id=?, iban=?, alias=?, is_default=?, main_currency=?, ordering=? WHERE local_id=?"

    .line 776
    invoke-virtual {v3, v5, v6, v2}, Lcom/siimkinks/sqlitemagic/bo;->b(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v5

    const-string v6, "account_data"

    const-string v7, "INSERT%s INTO account_data (account_id, customer_id, iban, alias, is_default, main_currency, ordering) VALUES (?, ?, ?, ?, ?, ?, ?)"

    .line 777
    invoke-virtual {v3, v6, v7, v2}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v2

    const-string v6, "account_data"

    .line 778
    iget-object v7, v1, Lcom/siimkinks/sqlitemagic/cp$a;->d:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 781
    iget-object v6, v6, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    goto :goto_5

    :cond_b
    const-string v6, "local_id"

    .line 786
    :goto_5
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v7

    .line 789
    :try_start_2
    monitor-enter v5
    :try_end_2
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 790
    :try_start_3
    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 791
    :try_start_4
    iget-object v8, v1, Lcom/siimkinks/sqlitemagic/cp$a;->a:Ljava/lang/Iterable;

    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    const/4 v9, 0x0

    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_13

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/swedbank/mobile/data/account/b;

    .line 792
    sget-boolean v15, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v15, :cond_c

    const-string v15, "PERSIST\n  table: account_data\n  object: %s"

    new-array v11, v13, [Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/swedbank/mobile/data/account/b;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v14

    invoke-static {v15, v11}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 794
    :cond_c
    invoke-static {v6, v10}, Lcom/swedbank/mobile/data/account/n;->a(Ljava/lang/String;Lcom/swedbank/mobile/data/account/b;)Z

    move-result v11

    if-nez v11, :cond_d

    .line 795
    invoke-static {v5, v10}, Lcom/swedbank/mobile/data/account/n;->b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/b;)V

    .line 796
    invoke-static {v5, v4, v6, v10}, Lcom/swedbank/mobile/data/account/n;->a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/account/b;)V

    .line 797
    invoke-interface {v5}, Landroidx/k/a/f;->a()I

    move-result v11

    goto :goto_7

    :cond_d
    const/4 v11, 0x0

    :goto_7
    if-gtz v11, :cond_11

    .line 800
    sget-boolean v11, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v11, :cond_e

    const-string v11, "PERSIST update failed; trying insertion"

    new-array v12, v14, [Ljava/lang/Object;

    invoke-static {v11, v12}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 801
    :cond_e
    invoke-static {v2, v10}, Lcom/swedbank/mobile/data/account/n;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/b;)V

    .line 802
    invoke-interface {v2}, Landroidx/k/a/f;->b()J

    move-result-wide v11

    .line 803
    sget-boolean v15, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v15, :cond_f

    const-string v15, "INSERT id: %s"

    new-array v4, v13, [Ljava/lang/Object;

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v4, v14

    invoke-static {v15, v4}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_f
    const-wide/16 v15, -0x1

    cmp-long v4, v11, v15

    if-nez v4, :cond_12

    .line 805
    iget-boolean v4, v3, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v4, :cond_10

    goto :goto_8

    .line 806
    :cond_10
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to persist "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    const-wide/16 v15, -0x1

    :cond_12
    const/4 v9, 0x1

    :goto_8
    move-wide v11, v15

    const/16 v4, 0x8

    goto :goto_6

    .line 815
    :cond_13
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 816
    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 817
    :try_start_6
    invoke-interface {v7}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_6
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 824
    invoke-interface {v7}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v9, :cond_14

    .line 826
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    .line 828
    :cond_14
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v9

    :catchall_1
    move-exception v0

    .line 815
    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    :catchall_2
    move-exception v0

    .line 816
    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v0
    :try_end_9
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    move-exception v0

    goto :goto_9

    :catch_1
    move-exception v0

    .line 821
    :try_start_a
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_15

    const-string v2, "Operation failed"

    new-array v4, v14, [Ljava/lang/Object;

    invoke-static {v0, v2, v4}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 824
    :cond_15
    invoke-interface {v7}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 828
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v14

    .line 824
    :goto_9
    invoke-interface {v7}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 828
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    .line 829
    throw v0
.end method
