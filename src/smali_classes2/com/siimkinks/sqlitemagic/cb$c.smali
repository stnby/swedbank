.class public final Lcom/siimkinks/sqlitemagic/cb$c;
.super Lcom/siimkinks/sqlitemagic/cd$a;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/cd$a<",
        "TR;TS;TN;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/siimkinks/sqlitemagic/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/bi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/cd;Lcom/siimkinks/sqlitemagic/dl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd<",
            "TS;>;",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;)V"
        }
    .end annotation

    .line 387
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/cd$a;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    .line 384
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$c;->b:Ljava/util/ArrayList;

    .line 388
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$c;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 389
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$c;->c:Lcom/siimkinks/sqlitemagic/cc;

    iput-object p0, p1, Lcom/siimkinks/sqlitemagic/cc;->c:Lcom/siimkinks/sqlitemagic/cb$c;

    .line 390
    instance-of p1, p2, Lcom/siimkinks/sqlitemagic/cf;

    if-eqz p1, :cond_0

    .line 391
    check-cast p2, Lcom/siimkinks/sqlitemagic/cf;

    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$c;->c:Lcom/siimkinks/sqlitemagic/cc;

    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/cf;->a(Lcom/siimkinks/sqlitemagic/cc;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/siimkinks/sqlitemagic/bi;)Lcom/siimkinks/sqlitemagic/cb$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/bi;",
            ")",
            "Lcom/siimkinks/sqlitemagic/cb$c<",
            "TT;TR;TS;TN;>;"
        }
    .end annotation

    const-string v0, "LEFT JOIN"

    .line 484
    iput-object v0, p1, Lcom/siimkinks/sqlitemagic/bi;->b:Ljava/lang/String;

    .line 485
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/bi;->a(Ljava/util/ArrayList;)V

    return-object p0
.end method

.method public varargs a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/siimkinks/sqlitemagic/cb$f;",
            ")",
            "Lcom/siimkinks/sqlitemagic/cb$e<",
            "TR;TS;TN;>;"
        }
    .end annotation

    .line 741
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$e;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$e;-><init>(Lcom/siimkinks/sqlitemagic/cd$a;[Lcom/siimkinks/sqlitemagic/cb$f;)V

    return-object v0
.end method

.method public a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/bb;",
            ")",
            "Lcom/siimkinks/sqlitemagic/cb$h<",
            "TR;TS;TN;>;"
        }
    .end annotation

    .line 717
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$h;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$h;-><init>(Lcom/siimkinks/sqlitemagic/cd$a;Lcom/siimkinks/sqlitemagic/bb;)V

    return-object v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 4

    const-string v0, "FROM "

    .line 397
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/lang/StringBuilder;)V

    .line 399
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->b:Ljava/util/ArrayList;

    .line 400
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    const/16 v3, 0x20

    .line 401
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 402
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/siimkinks/sqlitemagic/bi;

    invoke-virtual {v3, p1}, Lcom/siimkinks/sqlitemagic/bi;->a(Ljava/lang/StringBuilder;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "FROM "

    .line 408
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/lang/StringBuilder;)V

    .line 410
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->b:Ljava/util/ArrayList;

    .line 411
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    const/16 v3, 0x20

    .line 412
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 413
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/siimkinks/sqlitemagic/bi;

    invoke-virtual {v3, p1, p2}, Lcom/siimkinks/sqlitemagic/bi;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
