.class public final Lcom/siimkinks/sqlitemagic/bh;
.super Ljava/lang/Object;
.source "GeneratedClassesManager.java"


# direct methods
.method public static a()I
    .locals 1

    const/16 v0, 0x19

    return v0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    .line 72
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ak;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v1, v0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bs;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/w;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/dr;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ef;->a(Ljava/lang/String;)I

    move-result p0

    add-int/2addr v1, p0

    return v1

    :cond_0
    const/4 v1, -0x1

    .line 74
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "Transfer"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_1
    const-string v2, "Overview"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "Cards"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "Core"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_4
    const-string v2, "Widget"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x4

    :cond_1
    :goto_0
    packed-switch v1, :pswitch_data_0

    return v0

    .line 84
    :pswitch_0
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ef;->a(Ljava/lang/String;)I

    move-result p0

    return p0

    .line 82
    :pswitch_1
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/dr;->a(Ljava/lang/String;)I

    move-result p0

    return p0

    .line 80
    :pswitch_2
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/w;->a(Ljava/lang/String;)I

    move-result p0

    return p0

    .line 78
    :pswitch_3
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bs;->a(Ljava/lang/String;)I

    move-result p0

    return p0

    .line 76
    :pswitch_4
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ak;->a(Ljava/lang/String;)I

    move-result p0

    return p0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6593b99c -> :sswitch_4
        0x2023bf -> :sswitch_3
        0x3ddf743 -> :sswitch_2
        0x23735199 -> :sswitch_1
        0x50331c0b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroidx/k/a/b;)V
    .locals 0

    .line 16
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ak;->a(Landroidx/k/a/b;)V

    .line 17
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bs;->a(Landroidx/k/a/b;)V

    .line 18
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/w;->a(Landroidx/k/a/b;)V

    .line 19
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/dr;->a(Landroidx/k/a/b;)V

    .line 20
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ef;->a(Landroidx/k/a/b;)V

    return-void
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    const-string v0, "swedbank.db"

    return-object v0
.end method

.method public static b(Landroidx/k/a/b;)V
    .locals 3

    .line 24
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 26
    :try_start_0
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ak;->b(Landroidx/k/a/b;)V

    .line 27
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bs;->b(Landroidx/k/a/b;)V

    .line 28
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/w;->b(Landroidx/k/a/b;)V

    .line 29
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/dr;->b(Landroidx/k/a/b;)V

    .line 30
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ef;->b(Landroidx/k/a/b;)V

    .line 31
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Creating tables"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    :cond_0
    invoke-interface {p0}, Landroidx/k/a/b;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 34
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36
    :cond_1
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-void

    :goto_1
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 37
    throw v0
.end method

.method public static c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;
    .locals 4

    .line 42
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 44
    :try_start_0
    new-instance v1, Lcom/siimkinks/sqlitemagic/b/d;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/bh;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    .line 45
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ak;->c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    .line 46
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bs;->c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    .line 47
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/w;->c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    .line 48
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/dr;->c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    .line 49
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ef;->c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    .line 50
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "Clearing data"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :cond_0
    invoke-interface {p0}, Landroidx/k/a/b;->c()V

    .line 52
    new-array v2, v0, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 55
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    :cond_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 59
    throw v0
.end method

.method public static c()[Ljava/lang/String;
    .locals 5

    const-string v0, "Core"

    const-string v1, "Overview"

    const-string v2, "Cards"

    const-string v3, "Transfer"

    const-string v4, "Widget"

    .line 137
    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroidx/k/a/b;)V
    .locals 0

    .line 63
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ak;->d(Landroidx/k/a/b;)V

    .line 64
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bs;->d(Landroidx/k/a/b;)V

    .line 65
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/w;->d(Landroidx/k/a/b;)V

    .line 66
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/dr;->d(Landroidx/k/a/b;)V

    .line 67
    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/ef;->d(Landroidx/k/a/b;)V

    return-void
.end method
