.class final Lcom/siimkinks/sqlitemagic/ba;
.super Ljava/lang/Object;
.source "EntityDbManager.java"


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Landroidx/k/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Landroidx/k/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/siimkinks/sqlitemagic/as;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/as;)V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 12
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 17
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)Landroidx/k/a/f;
    .locals 4

    .line 48
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/k/a/f;

    if-nez v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p1

    .line 54
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-object p1

    .line 51
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "DB connection closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return-object v0
.end method

.method a(Ljava/lang/String;I)Landroidx/k/a/f;
    .locals 4

    .line 88
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 92
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/siimkinks/sqlitemagic/aj;->a:[Ljava/lang/String;

    aget-object p2, v3, p2

    aput-object p2, v1, v2

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p1

    return-object p1

    .line 90
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "DB connection closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a()V
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/k/a/f;

    if-eqz v0, :cond_0

    .line 24
    :try_start_0
    invoke-interface {v0}, Landroidx/k/a/f;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 27
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/k/a/f;

    if-eqz v0, :cond_1

    .line 30
    :try_start_1
    invoke-interface {v0}, Landroidx/k/a/f;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 33
    :catch_1
    :cond_1
    iput-object v1, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    return-void
.end method

.method b(Ljava/lang/String;)Landroidx/k/a/f;
    .locals 4

    .line 63
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/k/a/f;

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p1

    .line 69
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-object p1

    .line 66
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "DB connection closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return-object v0
.end method

.method c(Ljava/lang/String;)Landroidx/k/a/f;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ba;->c:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p1

    return-object p1

    .line 80
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "DB connection closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
