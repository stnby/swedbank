.class public final Lcom/siimkinks/sqlitemagic/f;
.super Lcom/siimkinks/sqlitemagic/dl;
.source "ActiveScopedSessionTable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/siimkinks/sqlitemagic/dl<",
        "Lcom/swedbank/mobile/data/authentication/b/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/f;


# instance fields
.field public final b:Lcom/siimkinks/sqlitemagic/dw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dw<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/authentication/b/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/authentication/b/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/authentication/b/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/siimkinks/sqlitemagic/bn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/bn<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/authentication/b/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/siimkinks/sqlitemagic/bn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/bn<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/authentication/b/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    new-instance v0, Lcom/siimkinks/sqlitemagic/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/siimkinks/sqlitemagic/f;->a:Lcom/siimkinks/sqlitemagic/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 8

    const-string v0, "active_scoped_session"

    const/4 v1, 0x5

    .line 31
    invoke-direct {p0, v0, p1, v1}, Lcom/siimkinks/sqlitemagic/dl;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 20
    new-instance v7, Lcom/siimkinks/sqlitemagic/dw;

    const-string v2, "local_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->c:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/dw;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/f;->b:Lcom/siimkinks/sqlitemagic/dw;

    .line 22
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "access_token"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/f;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 24
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "refresh_token"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/f;->d:Lcom/siimkinks/sqlitemagic/x;

    .line 26
    new-instance v7, Lcom/siimkinks/sqlitemagic/bn;

    const-string v2, "expires_in"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->b:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/f;->e:Lcom/siimkinks/sqlitemagic/bn;

    .line 28
    new-instance v7, Lcom/siimkinks/sqlitemagic/bn;

    const-string v2, "created"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->b:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/f;->f:Lcom/siimkinks/sqlitemagic/bn;

    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/ca$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/ca$b<",
            "Lcom/swedbank/mobile/data/authentication/b/a;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 45
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->c()Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    new-instance p3, Lcom/siimkinks/sqlitemagic/f$2;

    invoke-direct {p3, p0, p1, p2}, Lcom/siimkinks/sqlitemagic/f$2;-><init>(Lcom/siimkinks/sqlitemagic/f;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)V

    return-object p3

    .line 46
    :cond_1
    :goto_0
    new-instance p1, Lcom/siimkinks/sqlitemagic/f$1;

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/f$1;-><init>(Lcom/siimkinks/sqlitemagic/f;)V

    return-object p1
.end method
