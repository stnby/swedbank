.class final Lcom/siimkinks/sqlitemagic/au;
.super Ljava/lang/Object;
.source "DefaultDbDowngrader.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/at;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroidx/k/a/b;II)V
    .locals 3

    .line 10
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Downgrading database from "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " to "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-array p3, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string p2, "SELECT name FROM sqlite_master WHERE type=\'table\' AND name != \'android_metadata\' AND name NOT LIKE \'sqlite%\'"

    .line 13
    invoke-interface {p1, p2}, Landroidx/k/a/b;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    .line 16
    :goto_0
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 17
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DROP TABLE IF EXISTS "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Landroidx/k/a/b;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 21
    :cond_1
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 24
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;)V

    return-void

    :catchall_0
    move-exception p1

    .line 21
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw p1
.end method
