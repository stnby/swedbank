.class public final Lcom/siimkinks/sqlitemagic/cb$h;
.super Lcom/siimkinks/sqlitemagic/cd$a;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/cd$a<",
        "TT;TS;TN;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/siimkinks/sqlitemagic/bb;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/cd$a;Lcom/siimkinks/sqlitemagic/bb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd$a<",
            "TT;TS;TN;>;",
            "Lcom/siimkinks/sqlitemagic/bb;",
            ")V"
        }
    .end annotation

    .line 769
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/cd$a;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    .line 770
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$h;->a:Lcom/siimkinks/sqlitemagic/bb;

    .line 771
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$h;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/util/ArrayList;)V

    .line 772
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$h;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/bb;->b(Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public varargs a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/siimkinks/sqlitemagic/cb$f;",
            ")",
            "Lcom/siimkinks/sqlitemagic/cb$e<",
            "TT;TS;TN;>;"
        }
    .end annotation

    .line 808
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$e;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$e;-><init>(Lcom/siimkinks/sqlitemagic/cd$a;[Lcom/siimkinks/sqlitemagic/cb$f;)V

    return-object v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    const-string v0, "WHERE "

    .line 777
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 778
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$h;->a:Lcom/siimkinks/sqlitemagic/bb;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/lang/StringBuilder;)V

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "WHERE "

    .line 783
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$h;->a:Lcom/siimkinks/sqlitemagic/bb;

    invoke-virtual {v0, p1, p2}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    return-void
.end method
