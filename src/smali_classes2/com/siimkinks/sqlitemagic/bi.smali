.class public Lcom/siimkinks/sqlitemagic/bi;
.super Lcom/siimkinks/sqlitemagic/ck;
.source "JoinClause.java"


# instance fields
.field final a:Lcom/siimkinks/sqlitemagic/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dl<",
            "*>;"
        }
    .end annotation
.end field

.field b:Ljava/lang/String;

.field final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Lcom/siimkinks/sqlitemagic/ck;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bi;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 24
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/bi;->b:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/bi;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/StringBuilder;)V
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    .line 31
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 32
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bi;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/lang/StringBuilder;)V

    .line 33
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bi;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 34
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 35
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 36
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 42
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/bi;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    .line 43
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 44
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bi;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/lang/StringBuilder;)V

    .line 45
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bi;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 47
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 48
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
