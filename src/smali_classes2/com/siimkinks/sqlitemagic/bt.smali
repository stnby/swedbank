.class public final Lcom/siimkinks/sqlitemagic/bt;
.super Lcom/siimkinks/sqlitemagic/dl;
.source "OverviewSearchKeywordTable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/siimkinks/sqlitemagic/dl<",
        "Lcom/swedbank/mobile/data/overview/search/a/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/bt;


# instance fields
.field public final b:Lcom/siimkinks/sqlitemagic/dw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dw<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/overview/search/a/d;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/siimkinks/sqlitemagic/du;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/du<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/overview/search/a/d;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/siimkinks/sqlitemagic/bn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/bn<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/overview/search/a/d;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/siimkinks/sqlitemagic/ap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ap<",
            "Lcom/swedbank/mobile/data/overview/search/a/d;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    new-instance v0, Lcom/siimkinks/sqlitemagic/bt;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/bt;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/siimkinks/sqlitemagic/bt;->a:Lcom/siimkinks/sqlitemagic/bt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 8

    const-string v0, "overview_search_keyword"

    const/4 v1, 0x4

    .line 29
    invoke-direct {p0, v0, p1, v1}, Lcom/siimkinks/sqlitemagic/dl;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 20
    new-instance v7, Lcom/siimkinks/sqlitemagic/dw;

    const-string v2, "local_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->c:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/dw;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bt;->b:Lcom/siimkinks/sqlitemagic/dw;

    .line 22
    new-instance v7, Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "keyword"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/du;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bt;->c:Lcom/siimkinks/sqlitemagic/du;

    .line 24
    new-instance v7, Lcom/siimkinks/sqlitemagic/bn;

    const-string v2, "search_count"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->d:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bt;->d:Lcom/siimkinks/sqlitemagic/bn;

    .line 26
    new-instance v6, Lcom/siimkinks/sqlitemagic/ap;

    const-string v2, "last_searched"

    sget-object v3, Lcom/siimkinks/sqlitemagic/eb;->b:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/siimkinks/sqlitemagic/ap;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v6, p0, Lcom/siimkinks/sqlitemagic/bt;->e:Lcom/siimkinks/sqlitemagic/ap;

    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/ca$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/ca$b<",
            "Lcom/swedbank/mobile/data/overview/search/a/d;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 43
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->c()Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    new-instance p3, Lcom/siimkinks/sqlitemagic/bt$2;

    invoke-direct {p3, p0, p1, p2}, Lcom/siimkinks/sqlitemagic/bt$2;-><init>(Lcom/siimkinks/sqlitemagic/bt;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)V

    return-object p3

    .line 44
    :cond_1
    :goto_0
    new-instance p1, Lcom/siimkinks/sqlitemagic/bt$1;

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/bt$1;-><init>(Lcom/siimkinks/sqlitemagic/bt;)V

    return-object p1
.end method
