.class public final Lcom/siimkinks/sqlitemagic/eb;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/eb$a;
    }
.end annotation


# static fields
.field static final a:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "[B>;"
        }
    .end annotation
.end field

.field static final m:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "[",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/Random;

.field private static final o:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->n:Ljava/util/Random;

    const-string v0, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    .line 24
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->o:[C

    .line 121
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$1;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$1;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 133
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$6;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$6;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->b:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 145
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$7;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$7;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->c:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 161
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$8;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$8;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->d:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 173
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$9;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$9;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 189
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$10;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$10;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->f:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 201
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$11;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$11;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->g:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 217
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$12;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$12;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->h:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 233
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$13;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$13;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->i:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 249
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$2;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$2;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->j:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 261
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$3;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$3;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->k:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 277
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$4;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$4;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->l:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 289
    new-instance v0, Lcom/siimkinks/sqlitemagic/eb$5;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/eb$5;-><init>()V

    sput-object v0, Lcom/siimkinks/sqlitemagic/eb;->m:Lcom/siimkinks/sqlitemagic/eb$a;

    return-void
.end method

.method public static a([Ljava/lang/Byte;)[B
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 65
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 66
    sget-object p0, Lcom/siimkinks/sqlitemagic/b/a;->a:[B

    return-object p0

    .line 68
    :cond_1
    array-length v0, p0

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 70
    array-length v2, p0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 71
    aget-object v3, p0, v1

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static a([B)[Ljava/lang/Byte;
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 82
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 83
    sget-object p0, Lcom/siimkinks/sqlitemagic/b/a;->b:[Ljava/lang/Byte;

    return-object p0

    .line 85
    :cond_1
    array-length v0, p0

    new-array v0, v0, [Ljava/lang/Byte;

    const/4 v1, 0x0

    .line 87
    array-length v2, p0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 88
    aget-byte v3, p0, v1

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method
