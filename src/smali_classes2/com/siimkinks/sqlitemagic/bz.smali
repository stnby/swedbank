.class public final Lcom/siimkinks/sqlitemagic/bz;
.super Ljava/lang/Object;
.source "Queries.kt"


# direct methods
.method public static final a(Lcom/siimkinks/sqlitemagic/cb;[Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$a;
    .locals 1
    .param p0    # Lcom/siimkinks/sqlitemagic/cb;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # [Lcom/siimkinks/sqlitemagic/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;>(",
            "Lcom/siimkinks/sqlitemagic/cb<",
            "Ljava/lang/Object;",
            ">;[TC;)",
            "Lcom/siimkinks/sqlitemagic/cb$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$COLUMNS"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "columns"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$a;

    check-cast p0, Lcom/siimkinks/sqlitemagic/cd;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$a;-><init>(Lcom/siimkinks/sqlitemagic/cd;[Lcom/siimkinks/sqlitemagic/x;)V

    return-object v0
.end method

.method public static final a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;
    .locals 3
    .param p0    # Lcom/siimkinks/sqlitemagic/cb;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/siimkinks/sqlitemagic/dl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/siimkinks/sqlitemagic/cb<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;)",
            "Lcom/siimkinks/sqlitemagic/cb$c<",
            "TT;TT;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$FROM"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "table"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$c;

    new-instance v1, Lcom/siimkinks/sqlitemagic/cb$a;

    check-cast p0, Lcom/siimkinks/sqlitemagic/cd;

    sget-object v2, Lcom/siimkinks/sqlitemagic/cb;->a:[Lcom/siimkinks/sqlitemagic/x;

    invoke-direct {v1, p0, v2}, Lcom/siimkinks/sqlitemagic/cb$a;-><init>(Lcom/siimkinks/sqlitemagic/cd;[Lcom/siimkinks/sqlitemagic/x;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/cd;

    invoke-direct {v0, v1, p1}, Lcom/siimkinks/sqlitemagic/cb$c;-><init>(Lcom/siimkinks/sqlitemagic/cd;Lcom/siimkinks/sqlitemagic/dl;)V

    return-object v0
.end method

.method public static final a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;
    .locals 1
    .param p0    # Lcom/siimkinks/sqlitemagic/cb;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/siimkinks/sqlitemagic/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "N:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/siimkinks/sqlitemagic/cb<",
            "*>;",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*TR;**TN;>;)",
            "Lcom/siimkinks/sqlitemagic/cb$g<",
            "TR;TN;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$COLUMN"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "column"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$g;

    check-cast p0, Lcom/siimkinks/sqlitemagic/cd;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$g;-><init>(Lcom/siimkinks/sqlitemagic/cd;Lcom/siimkinks/sqlitemagic/x;)V

    return-object v0
.end method

.method public static final a()Lcom/siimkinks/sqlitemagic/cb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/cb<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 10
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb;

    const-string v1, "SELECT"

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/cb;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
