.class public final Lcom/siimkinks/sqlitemagic/cn;
.super Ljava/lang/Object;
.source "SqlUtil.java"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bh;->a(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;)",
            "Lcom/siimkinks/sqlitemagic/x;"
        }
    .end annotation

    .line 125
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 127
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    .line 128
    iget-object v3, v2, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_1
    return-object v2
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p2, :cond_0

    return-object p0

    .line 109
    :cond_0
    invoke-static {p1, p2}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object p1

    if-nez p1, :cond_1

    return-object p0

    :cond_1
    const-string p2, "WHERE"

    .line 113
    invoke-virtual {p0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p2

    add-int/lit8 p2, p2, 0x6

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    add-int/lit8 v1, p2, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    .line 115
    invoke-virtual {v0, p0, v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    iget-object p0, p1, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    .line 116
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "=?"

    .line 117
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroidx/k/a/b;)V
    .locals 0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bh;->b(Landroidx/k/a/b;)V

    return-void
.end method

.method static a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V
    .locals 3

    .line 92
    check-cast p1, Lcom/siimkinks/sqlitemagic/ag;

    .line 93
    iget-object v0, p1, Lcom/siimkinks/sqlitemagic/ag;->b:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE VIEW IF NOT EXISTS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " AS "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1, v0}, Landroidx/k/a/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE VIEW IF NOT EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " AS "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method static a(Landroidx/k/a/f;[Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 138
    array-length v0, p1

    :goto_0
    if-eqz v0, :cond_0

    add-int/lit8 v1, v0, -0x1

    .line 139
    aget-object v1, p1, v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a()[Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/siimkinks/sqlitemagic/bh;->c()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b()I
    .locals 1

    invoke-static {}, Lcom/siimkinks/sqlitemagic/bh;->a()I

    move-result v0

    return v0
.end method

.method public static b(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;
    .locals 0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bh;->c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object p0

    return-object p0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/siimkinks/sqlitemagic/bh;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroidx/k/a/b;)V
    .locals 0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/bh;->d(Landroidx/k/a/b;)V

    return-void
.end method
