.class public final Lcom/siimkinks/sqlitemagic/bf;
.super Ljava/lang/Object;
.source "FastCursor.java"

# interfaces
.implements Landroid/database/Cursor;


# instance fields
.field private final a:Landroid/database/AbstractWindowedCursor;

.field private b:Landroid/database/CursorWindow;

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>(Landroid/database/AbstractWindowedCursor;)V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    .line 32
    invoke-virtual {p1}, Landroid/database/AbstractWindowedCursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    .line 33
    invoke-virtual {p1}, Landroid/database/AbstractWindowedCursor;->getWindow()Landroid/database/CursorWindow;

    move-result-object p1

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    .line 34
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    invoke-virtual {p1}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result p1

    iput p1, p0, Lcom/siimkinks/sqlitemagic/bf;->c:I

    .line 35
    iget p1, p0, Lcom/siimkinks/sqlitemagic/bf;->c:I

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v0

    add-int/2addr p1, v0

    iput p1, p0, Lcom/siimkinks/sqlitemagic/bf;->d:I

    const/4 p1, -0x1

    .line 36
    iput p1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    return-void
.end method

.method static a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .line 40
    instance-of v0, p0, Landroid/database/AbstractWindowedCursor;

    if-eqz v0, :cond_0

    .line 41
    new-instance v0, Lcom/siimkinks/sqlitemagic/bf;

    check-cast p0, Landroid/database/AbstractWindowedCursor;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/bf;-><init>(Landroid/database/AbstractWindowedCursor;)V

    return-object v0

    :cond_0
    return-object p0
.end method

.method private a(II)V
    .locals 1

    .line 131
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->c:I

    if-lt p2, v0, :cond_0

    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->d:I

    if-lt p2, v0, :cond_1

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1, p2}, Landroid/database/AbstractWindowedCursor;->onMove(II)Z

    .line 133
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    invoke-virtual {p1}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result p1

    iput p1, p0, Lcom/siimkinks/sqlitemagic/bf;->c:I

    .line 134
    iget p1, p0, Lcom/siimkinks/sqlitemagic/bf;->c:I

    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    invoke-virtual {p2}, Landroid/database/CursorWindow;->getNumRows()I

    move-result p2

    add-int/2addr p1, p2

    iput p1, p0, Lcom/siimkinks/sqlitemagic/bf;->d:I

    :cond_1
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 55
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->close()V

    const/4 v0, 0x0

    .line 57
    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    :cond_0
    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1, p2}, Landroid/database/AbstractWindowedCursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public deactivate()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 236
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->deactivate()V

    return-void
.end method

.method public getBlob(I)[B
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getBlob(II)[B

    move-result-object p1

    return-object p1
.end method

.method public getColumnCount()I
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->getColumnCount()I

    move-result v0

    return v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    return v0
.end method

.method public getDouble(I)D
    .locals 2

    .line 220
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getDouble(II)D

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getFloat(I)F
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getDouble(II)D

    move-result-wide v0

    double-to-float p1, v0

    return p1
.end method

.method public getInt(I)I
    .locals 2

    .line 205
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getLong(II)J

    move-result-wide v0

    long-to-int p1, v0

    return p1
.end method

.method public getLong(I)J
    .locals 2

    .line 210
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getLong(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNotificationUri()Landroid/net/Uri;
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->getNotificationUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .line 68
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    return v0
.end method

.method public getShort(I)S
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getLong(II)J

    move-result-wide v0

    long-to-int p1, v0

    int-to-short p1, p1

    return p1
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getString(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getType(I)I
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->getType(I)I

    move-result p1

    return p1
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    return v0
.end method

.method public isAfterLast()Z
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->isAfterLast()Z

    move-result v0

    return v0
.end method

.method public isBeforeFirst()Z
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->isBeforeFirst()Z

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isFirst()Z
    .locals 1

    .line 140
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isLast()Z
    .locals 3

    .line 145
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public isNull(I)Z
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->b:Landroid/database/CursorWindow;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->getType(II)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public move(I)Z
    .locals 1

    .line 73
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/bf;->moveToPosition(I)Z

    move-result p1

    return p1
.end method

.method public moveToFirst()Z
    .locals 2

    .line 89
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/bf;->a(II)V

    .line 90
    iput v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    .line 91
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    if-lez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public moveToLast()Z
    .locals 3

    .line 96
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    if-lez v0, :cond_0

    .line 97
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    .line 98
    iget v2, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-direct {p0, v2, v0}, Lcom/siimkinks/sqlitemagic/bf;->a(II)V

    .line 99
    iput v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public moveToNext()Z
    .locals 3

    .line 108
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    .line 109
    iget v1, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, v0, 0x1

    .line 110
    invoke-direct {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/bf;->a(II)V

    .line 111
    iput v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    return v2

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 1

    if-ltz p1, :cond_0

    .line 78
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->f:I

    if-ge p1, v0, :cond_0

    .line 79
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    invoke-direct {p0, v0, p1}, Lcom/siimkinks/sqlitemagic/bf;->a(II)V

    .line 80
    iput p1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public moveToPrevious()Z
    .locals 2

    .line 120
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    if-lez v0, :cond_0

    add-int/lit8 v1, v0, -0x1

    .line 122
    invoke-direct {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/bf;->a(II)V

    .line 123
    iput v1, p0, Lcom/siimkinks/sqlitemagic/bf;->e:I

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public requery()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 242
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->requery()Z

    move-result v0

    return v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1

    return-object p1
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->setExtras(Landroid/os/Bundle;)V

    return-void
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1, p2}, Landroid/database/AbstractWindowedCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .line 267
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bf;->a:Landroid/database/AbstractWindowedCursor;

    invoke-virtual {v0, p1}, Landroid/database/AbstractWindowedCursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method
