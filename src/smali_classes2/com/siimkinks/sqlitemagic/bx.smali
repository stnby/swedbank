.class public final Lcom/siimkinks/sqlitemagic/bx;
.super Lcom/siimkinks/sqlitemagic/dl;
.source "PredefinedPaymentDataTable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/siimkinks/sqlitemagic/dl<",
        "Lcom/swedbank/mobile/data/transfer/predefined/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/bx;


# instance fields
.field public final b:Lcom/siimkinks/sqlitemagic/dw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dw<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/siimkinks/sqlitemagic/du;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/du<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/siimkinks/sqlitemagic/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/l<",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    new-instance v0, Lcom/siimkinks/sqlitemagic/bx;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/bx;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 8

    const-string v0, "predefined_payment_data"

    const/16 v1, 0xb

    .line 43
    invoke-direct {p0, v0, p1, v1}, Lcom/siimkinks/sqlitemagic/dl;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 20
    new-instance v7, Lcom/siimkinks/sqlitemagic/dw;

    const-string v2, "local_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->c:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/dw;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->b:Lcom/siimkinks/sqlitemagic/dw;

    .line 22
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "customer_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 24
    new-instance v7, Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "predefined_payment_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/du;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->d:Lcom/siimkinks/sqlitemagic/du;

    .line 26
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "predefined_payment_name"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->e:Lcom/siimkinks/sqlitemagic/x;

    .line 28
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "sender_account_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v5, 0x1

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->f:Lcom/siimkinks/sqlitemagic/x;

    .line 30
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "recipient_name"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->g:Lcom/siimkinks/sqlitemagic/x;

    .line 32
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "recipient_iban"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->h:Lcom/siimkinks/sqlitemagic/x;

    .line 34
    new-instance v6, Lcom/siimkinks/sqlitemagic/l;

    const-string v2, "amount"

    sget-object v3, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/siimkinks/sqlitemagic/l;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v6, p0, Lcom/siimkinks/sqlitemagic/bx;->i:Lcom/siimkinks/sqlitemagic/l;

    .line 36
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "currency"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->j:Lcom/siimkinks/sqlitemagic/x;

    .line 38
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "description"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->k:Lcom/siimkinks/sqlitemagic/x;

    .line 40
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "reference_number"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/bx;->l:Lcom/siimkinks/sqlitemagic/x;

    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/ca$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/ca$b<",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 57
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->c()Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 66
    :cond_0
    new-instance p3, Lcom/siimkinks/sqlitemagic/bx$2;

    invoke-direct {p3, p0, p1, p2}, Lcom/siimkinks/sqlitemagic/bx$2;-><init>(Lcom/siimkinks/sqlitemagic/bx;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)V

    return-object p3

    .line 58
    :cond_1
    :goto_0
    new-instance p1, Lcom/siimkinks/sqlitemagic/bx$1;

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/bx$1;-><init>(Lcom/siimkinks/sqlitemagic/bx;)V

    return-object p1
.end method
