.class public final Lcom/siimkinks/sqlitemagic/cb$g;
.super Lcom/siimkinks/sqlitemagic/cd;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/cd<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "*TR;**TN;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/cd;Lcom/siimkinks/sqlitemagic/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*TR;**TN;>;)V"
        }
    .end annotation

    .line 201
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/cd;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    .line 202
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    .line 203
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$g;->c:Lcom/siimkinks/sqlitemagic/cc;

    iput-object p0, p1, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    .line 205
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$g;->c:Lcom/siimkinks/sqlitemagic/cc;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/siimkinks/sqlitemagic/cc;->h:Z

    .line 206
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$g;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/util/ArrayList;)V

    .line 207
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$g;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method a()Lcom/siimkinks/sqlitemagic/b/d;
    .locals 2

    .line 212
    new-instance v0, Lcom/siimkinks/sqlitemagic/b/d;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    .line 213
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1, v0}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    return-object v0
.end method

.method public a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;)",
            "Lcom/siimkinks/sqlitemagic/cb$c<",
            "TT;TR;",
            "Ljava/lang/Object;",
            "TN;>;"
        }
    .end annotation

    .line 239
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$c;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;-><init>(Lcom/siimkinks/sqlitemagic/cd;Lcom/siimkinks/sqlitemagic/dl;)V

    return-object v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;)V

    .line 220
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/StringBuilder;)V

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 225
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1, p2}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    .line 226
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/StringBuilder;)V

    return-void
.end method
