.class final Lcom/siimkinks/sqlitemagic/bg;
.super Lcom/siimkinks/sqlitemagic/bn;
.source "FunctionColumn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        "ET:",
        "Ljava/lang/Object;",
        "P:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/bn<",
        "TT;TR;TET;TP;TN;>;"
    }
.end annotation


# instance fields
.field private final h:[Lcom/siimkinks/sqlitemagic/x;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private l:Z


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/dl;Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TP;>;",
            "Lcom/siimkinks/sqlitemagic/x;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "*>;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 57
    new-array v3, v0, [Lcom/siimkinks/sqlitemagic/x;

    const/4 v0, 0x0

    aput-object p2, v3, v0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v6, p4

    move-object v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/siimkinks/sqlitemagic/bg;-><init>(Lcom/siimkinks/sqlitemagic/dl;[Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-void
.end method

.method constructor <init>(Lcom/siimkinks/sqlitemagic/dl;[Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TP;>;[",
            "Lcom/siimkinks/sqlitemagic/x;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "*>;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object v7, p0

    move-object v8, p2

    move-object v9, p3

    move-object/from16 v10, p5

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v11, 0x0

    if-nez p8, :cond_1

    array-length v1, v8

    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    goto :goto_0

    :cond_0
    aget-object v0, v8, v11

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    move-object v6, v0

    goto :goto_1

    :cond_1
    :goto_0
    move-object/from16 v6, p8

    :goto_1
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v4, p6

    move/from16 v5, p7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    .line 32
    iput-boolean v11, v7, Lcom/siimkinks/sqlitemagic/bg;->l:Z

    .line 44
    iput-object v8, v7, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    .line 45
    iput-object v9, v7, Lcom/siimkinks/sqlitemagic/bg;->i:Ljava/lang/String;

    move-object/from16 v0, p4

    .line 46
    iput-object v0, v7, Lcom/siimkinks/sqlitemagic/bg;->j:Ljava/lang/String;

    .line 47
    iput-object v10, v7, Lcom/siimkinks/sqlitemagic/bg;->k:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;I)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "I)I"
        }
    .end annotation

    .line 127
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/bg;->a(Ljava/lang/StringBuilder;)V

    .line 128
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/bg;->b(Ljava/lang/StringBuilder;)V

    const/4 p2, 0x0

    .line 129
    invoke-static {p1, p2, p3, p0}, Lcom/siimkinks/sqlitemagic/bg;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    const/4 p1, 0x1

    .line 130
    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/bg;->l:Z

    add-int/2addr p3, p1

    return p3
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;I)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;I)I"
        }
    .end annotation

    .line 136
    invoke-virtual {p0, p2, p3}, Lcom/siimkinks/sqlitemagic/bg;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    .line 137
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/bg;->b(Ljava/lang/StringBuilder;)V

    const/4 p2, 0x0

    .line 138
    invoke-static {p1, p2, p4, p0}, Lcom/siimkinks/sqlitemagic/bg;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    const/4 p1, 0x1

    .line 139
    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/bg;->l:Z

    add-int/2addr p4, p1

    return p4
.end method

.method public a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/bg<",
            "TT;TR;TET;TP;TN;>;"
        }
    .end annotation

    .line 146
    new-instance v9, Lcom/siimkinks/sqlitemagic/bg;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bg;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/bg;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/bg;->j:Ljava/lang/String;

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/bg;->k:Ljava/lang/String;

    iget-object v6, p0, Lcom/siimkinks/sqlitemagic/bg;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v7, p0, Lcom/siimkinks/sqlitemagic/bg;->f:Z

    move-object v0, v9

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/siimkinks/sqlitemagic/bg;-><init>(Lcom/siimkinks/sqlitemagic/dl;[Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v9
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/d;)V
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    .line 101
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    array-length v1, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 103
    aget-object v3, v0, v2

    invoke-virtual {v3, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 5

    .line 62
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/bg;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bg;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    .line 68
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    array-length v1, v1

    .line 69
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/bg;->j:Ljava/lang/String;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    if-eqz v3, :cond_1

    .line 72
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    :cond_1
    aget-object v4, v0, v3

    invoke-virtual {v4, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 81
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/bg;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bg;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    .line 87
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    array-length v1, v1

    .line 88
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/bg;->j:Ljava/lang/String;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    if-eqz v3, :cond_1

    .line 91
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    :cond_1
    aget-object v4, v0, v3

    invoke-virtual {v4, p1, p2}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 95
    :cond_2
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/bg;->k:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    .line 110
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    array-length v1, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 112
    aget-object v3, v0, v2

    invoke-virtual {v3, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;
    .locals 0

    .line 23
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/bg;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bg;

    move-result-object p1

    return-object p1
.end method

.method b(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    .line 119
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bg;->h:[Lcom/siimkinks/sqlitemagic/x;

    array-length v1, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 121
    aget-object v3, v0, v2

    invoke-virtual {v3, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic c(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bn;
    .locals 0

    .line 23
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/bg;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bg;

    move-result-object p1

    return-object p1
.end method
