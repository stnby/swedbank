.class public final Lcom/siimkinks/sqlitemagic/cb;
.super Lcom/siimkinks/sqlitemagic/cd;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/cb$d;,
        Lcom/siimkinks/sqlitemagic/cb$e;,
        Lcom/siimkinks/sqlitemagic/cb$f;,
        Lcom/siimkinks/sqlitemagic/cb$b;,
        Lcom/siimkinks/sqlitemagic/cb$h;,
        Lcom/siimkinks/sqlitemagic/cb$c;,
        Lcom/siimkinks/sqlitemagic/cb$a;,
        Lcom/siimkinks/sqlitemagic/cb$g;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/cd<",
        "TS;>;"
    }
.end annotation


# static fields
.field static final a:[Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;"
        }
    .end annotation
.end field

.field private static final e:Lcom/siimkinks/sqlitemagic/bn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/bn<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "*",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x0

    .line 40
    new-array v0, v0, [Lcom/siimkinks/sqlitemagic/x;

    sput-object v0, Lcom/siimkinks/sqlitemagic/cb;->a:[Lcom/siimkinks/sqlitemagic/x;

    .line 1196
    new-instance v0, Lcom/siimkinks/sqlitemagic/bn;

    sget-object v2, Lcom/siimkinks/sqlitemagic/dl;->v:Lcom/siimkinks/sqlitemagic/dl;

    const-string v3, "count(*)"

    sget-object v5, Lcom/siimkinks/sqlitemagic/eb;->b:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    sput-object v0, Lcom/siimkinks/sqlitemagic/cb;->e:Lcom/siimkinks/sqlitemagic/bn;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, v0}, Lcom/siimkinks/sqlitemagic/cd;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    .line 47
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cb;->b:Ljava/lang/String;

    return-void
.end method

.method public static a()Lcom/siimkinks/sqlitemagic/bn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/bn<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "*",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1248
    sget-object v0, Lcom/siimkinks/sqlitemagic/cb;->e:Lcom/siimkinks/sqlitemagic/bn;

    return-object v0
.end method

.method public static a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/x;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "N:",
            "Ljava/lang/Object;",
            "X:",
            "Lcom/siimkinks/sqlitemagic/x<",
            "**+",
            "Ljava/lang/CharSequence;",
            "TP;TN;>;>(TX;)",
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "TP;TN;>;"
        }
    .end annotation

    .line 1590
    new-instance v8, Lcom/siimkinks/sqlitemagic/bg;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dl;

    move-result-object v1

    const-string v3, "lower("

    const-string v4, ")"

    sget-object v5, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v6, p0, Lcom/siimkinks/sqlitemagic/x;->f:Z

    const/4 v7, 0x0

    move-object v0, v8

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/siimkinks/sqlitemagic/bg;-><init>(Lcom/siimkinks/sqlitemagic/dl;Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v8
.end method

.method public static a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "N:",
            "Ljava/lang/Object;",
            "X:",
            "Lcom/siimkinks/sqlitemagic/x<",
            "***TP;TN;>;>(TX;",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "TP;TN;>;"
        }
    .end annotation

    .line 1304
    new-instance v8, Lcom/siimkinks/sqlitemagic/bg;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dl;

    move-result-object v1

    const-string v3, "group_concat("

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\')"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v6, p0, Lcom/siimkinks/sqlitemagic/x;->f:Z

    const/4 v7, 0x0

    move-object v0, v8

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/siimkinks/sqlitemagic/bg;-><init>(Lcom/siimkinks/sqlitemagic/dl;Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v8
.end method

.method public static a(Ljava/lang/CharSequence;)Lcom/siimkinks/sqlitemagic/x;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V::",
            "Ljava/lang/CharSequence;",
            ">(TV;)",
            "Lcom/siimkinks/sqlitemagic/x<",
            "TV;TV;",
            "Ljava/lang/CharSequence;",
            "*",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1528
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    sget-object v1, Lcom/siimkinks/sqlitemagic/dl;->v:Lcom/siimkinks/sqlitemagic/dl;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v7
.end method

.method public static varargs a([Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/x;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;>([TX;)",
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "*",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 1504
    new-instance v9, Lcom/siimkinks/sqlitemagic/bg;

    sget-object v1, Lcom/siimkinks/sqlitemagic/dl;->v:Lcom/siimkinks/sqlitemagic/dl;

    const-string v3, ""

    const-string v4, " || "

    const-string v5, ""

    sget-object v6, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v0, v9

    move-object v2, p0

    invoke-direct/range {v0 .. v8}, Lcom/siimkinks/sqlitemagic/bg;-><init>(Lcom/siimkinks/sqlitemagic/dl;[Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v9
.end method


# virtual methods
.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 57
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cb;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method
