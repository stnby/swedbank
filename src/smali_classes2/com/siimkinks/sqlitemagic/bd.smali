.class final Lcom/siimkinks/sqlitemagic/bd;
.super Lcom/siimkinks/sqlitemagic/bb;
.source "ExprC.java"


# instance fields
.field private final a:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/siimkinks/sqlitemagic/bb;-><init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;)V

    .line 16
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 33
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/lang/StringBuilder;)V

    .line 34
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/x;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;)V

    :goto_0
    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 43
    invoke-super {p0, p1, p2}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    .line 44
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p2}, Lcom/siimkinks/sqlitemagic/x;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1, p2}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    :goto_0
    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/util/ArrayList;)V

    .line 22
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method b(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/bb;->b(Ljava/util/ArrayList;)V

    .line 28
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bd;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/ArrayList;)V

    return-void
.end method
