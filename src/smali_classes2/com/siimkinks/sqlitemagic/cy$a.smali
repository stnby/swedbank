.class public final Lcom/siimkinks/sqlitemagic/cy$a;
.super Ljava/lang/Object;
.source "SqliteMagic_LoginInfo_Handler.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/a/d;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/siimkinks/sqlitemagic/a/d;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/authentication/a/e;

.field private b:Lcom/siimkinks/sqlitemagic/as;

.field private c:I

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/data/authentication/a/e;)V
    .locals 2

    .line 585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 578
    iput v0, p0, Lcom/siimkinks/sqlitemagic/cy$a;->c:I

    .line 580
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/cy$a;->d:Ljava/util/ArrayList;

    .line 586
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cy$a;->a:Lcom/swedbank/mobile/data/authentication/a/e;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/authentication/a/e;)Lcom/siimkinks/sqlitemagic/cy$a;
    .locals 1

    .line 591
    new-instance v0, Lcom/siimkinks/sqlitemagic/cy$a;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/cy$a;-><init>(Lcom/swedbank/mobile/data/authentication/a/e;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/siimkinks/sqlitemagic/a/d;
    .locals 1

    const/4 v0, 0x1

    .line 621
    iput-boolean v0, p0, Lcom/siimkinks/sqlitemagic/cy$a;->e:Z

    return-object p0
.end method

.method public a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Lcom/siimkinks/sqlitemagic/dt<",
            "Ljava/lang/Object;",
            ">;>(TC;)",
            "Lcom/siimkinks/sqlitemagic/a/d;"
        }
    .end annotation

    .line 613
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cy$a;->d:Ljava/util/ArrayList;

    check-cast p1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public b()J
    .locals 6

    .line 627
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cy$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cy$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    :try_start_0
    const-string v1, "Core"

    const/4 v2, 0x1

    .line 630
    invoke-virtual {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v1

    .line 631
    iget-boolean v2, p0, Lcom/siimkinks/sqlitemagic/cy$a;->e:Z

    if-eqz v2, :cond_1

    .line 632
    new-instance v2, Lcom/siimkinks/sqlitemagic/ec;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/cy$a;->c:I

    invoke-direct {v2, v3}, Lcom/siimkinks/sqlitemagic/ec;-><init>(I)V

    .line 633
    new-instance v3, Lcom/siimkinks/sqlitemagic/b/c;

    const/4 v4, 0x4

    invoke-direct {v3, v4}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    .line 634
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/cy$a;->a:Lcom/swedbank/mobile/data/authentication/a/e;

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/cy$a;->d:Ljava/util/ArrayList;

    invoke-static {v4, v3, v1, v2, v5}, Lcom/siimkinks/sqlitemagic/cy;->a(Lcom/swedbank/mobile/data/authentication/a/e;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/ec;Ljava/util/ArrayList;)J

    move-result-wide v1

    goto :goto_1

    .line 636
    :cond_1
    new-instance v2, Lcom/siimkinks/sqlitemagic/bo;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/cy$a;->c:I

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/cy$a;->d:Ljava/util/ArrayList;

    invoke-direct {v2, v3, v4, v5}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    :try_start_1
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/cy$a;->a:Lcom/swedbank/mobile/data/authentication/a/e;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/cy$a;->d:Ljava/util/ArrayList;

    invoke-static {v3, v1, v2, v4}, Lcom/siimkinks/sqlitemagic/cy;->a(Lcom/swedbank/mobile/data/authentication/a/e;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/bo;Ljava/util/ArrayList;)J

    move-result-wide v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 640
    :try_start_2
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    move-wide v1, v3

    .line 643
    :goto_1
    sget-object v3, Lcom/siimkinks/sqlitemagic/bm;->a:Lcom/siimkinks/sqlitemagic/bm;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/bm;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    return-wide v1

    :catchall_0
    move-exception v0

    .line 640
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    .line 641
    throw v0
    :try_end_2
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    .line 646
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_2

    const-string v1, "Operation failed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public synthetic b(Lcom/siimkinks/sqlitemagic/dt;)Ljava/lang/Object;
    .locals 0

    .line 572
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/cy$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/d;

    move-result-object p1

    return-object p1
.end method

.method public c()Ljava/lang/Long;
    .locals 4

    .line 653
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/cy$a;->b()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 654
    iget v2, p0, Lcom/siimkinks/sqlitemagic/cy$a;->c:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 655
    :cond_0
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to persist "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/cy$a;->a:Lcom/swedbank/mobile/data/authentication/a/e;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 657
    :cond_1
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 572
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/cy$a;->c()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 664
    invoke-static {p0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
