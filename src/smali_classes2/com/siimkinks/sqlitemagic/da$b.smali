.class public final Lcom/siimkinks/sqlitemagic/da$b;
.super Ljava/lang/Object;
.source "SqliteMagic_PaymentExecutionData_Handler.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/a/e;
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/da;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/transfer/payment/d;

.field private b:Lcom/siimkinks/sqlitemagic/as;

.field private c:I

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/d;)V
    .locals 2

    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 357
    iput v0, p0, Lcom/siimkinks/sqlitemagic/da$b;->c:I

    .line 359
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/da$b;->d:Ljava/util/ArrayList;

    .line 363
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/da$b;->a:Lcom/swedbank/mobile/data/transfer/payment/d;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lcom/siimkinks/sqlitemagic/da$b;
    .locals 1

    .line 368
    new-instance v0, Lcom/siimkinks/sqlitemagic/da$b;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/da$b;-><init>(Lcom/swedbank/mobile/data/transfer/payment/d;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Lcom/siimkinks/sqlitemagic/dt<",
            "Ljava/lang/Object;",
            ">;>(TC;)",
            "Lcom/siimkinks/sqlitemagic/a/e;"
        }
    .end annotation

    .line 390
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/da$b;->d:Ljava/util/ArrayList;

    check-cast p1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a()Z
    .locals 6

    .line 396
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/da$b;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/da$b;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v1, "Transfer"

    const/4 v2, 0x2

    .line 397
    invoke-virtual {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v1

    .line 398
    new-instance v2, Lcom/siimkinks/sqlitemagic/bo;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/da$b;->c:I

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/da$b;->d:Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v2, v3, v5, v4}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    .line 400
    :try_start_0
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/da$b;->a:Lcom/swedbank/mobile/data/transfer/payment/d;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/da$b;->d:Ljava/util/ArrayList;

    invoke-static {v3, v1, v2, v4}, Lcom/siimkinks/sqlitemagic/da;->a(Lcom/swedbank/mobile/data/transfer/payment/d;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/bo;Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 401
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bu;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v5

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    .line 407
    throw v0
.end method

.method public synthetic b(Lcom/siimkinks/sqlitemagic/dt;)Ljava/lang/Object;
    .locals 0

    .line 351
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/da$b;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/e;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 3

    .line 412
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/da$b;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/siimkinks/sqlitemagic/da$b;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 413
    :cond_0
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to update "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/da$b;->a:Lcom/swedbank/mobile/data/transfer/payment/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public c()Lio/reactivex/b;
    .locals 1

    .line 421
    invoke-static {p0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
