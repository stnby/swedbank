.class final Lcom/siimkinks/sqlitemagic/cc;
.super Ljava/lang/Object;
.source "SelectBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field a:Lcom/siimkinks/sqlitemagic/cm;

.field b:I

.field c:Lcom/siimkinks/sqlitemagic/cb$c;

.field d:Lcom/siimkinks/sqlitemagic/cb$a;

.field e:Lcom/siimkinks/sqlitemagic/cb$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/cb$g<",
            "**>;"
        }
    .end annotation
.end field

.field final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field h:Z

.field i:Lcom/siimkinks/sqlitemagic/as;

.field private j:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    .line 22
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/cc;->i:Lcom/siimkinks/sqlitemagic/as;

    const/4 v0, 0x0

    .line 23
    iput-boolean v0, p0, Lcom/siimkinks/sqlitemagic/cc;->j:Z

    return-void
.end method

.method private static a(Lcom/siimkinks/sqlitemagic/cb$c;Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cb$c;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->b:Ljava/util/ArrayList;

    .line 131
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 132
    iget-object p0, p0, Lcom/siimkinks/sqlitemagic/cb$c;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {p0, p1, p2, p3}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z

    move-result p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 134
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/siimkinks/sqlitemagic/bi;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/bi;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {v3, p1, p2, p3}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z

    move-result v3

    or-int/2addr p0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return p0
.end method


# virtual methods
.method a()Lcom/siimkinks/sqlitemagic/ae;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/siimkinks/sqlitemagic/ae<",
            "TT;TS;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 63
    iget-boolean v1, v0, Lcom/siimkinks/sqlitemagic/cc;->j:Z

    if-nez v1, :cond_d

    const/4 v1, 0x1

    .line 66
    iput-boolean v1, v0, Lcom/siimkinks/sqlitemagic/cc;->j:Z

    .line 68
    iget-object v2, v0, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    .line 70
    iget-object v4, v0, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/cb$g;->a()Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v4

    goto :goto_1

    .line 72
    :cond_1
    iget-object v4, v0, Lcom/siimkinks/sqlitemagic/cc;->d:Lcom/siimkinks/sqlitemagic/cb$a;

    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/cb$a;->a()Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v4

    :goto_1
    if-eqz v4, :cond_2

    .line 74
    new-instance v5, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/b/d;->size()I

    move-result v6

    invoke-direct {v5, v6}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    goto :goto_2

    :cond_2
    new-instance v5, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {v5}, Lcom/siimkinks/sqlitemagic/b/c;-><init>()V

    .line 77
    :goto_2
    iget-object v6, v0, Lcom/siimkinks/sqlitemagic/cc;->c:Lcom/siimkinks/sqlitemagic/cb$c;

    .line 78
    iget-object v10, v6, Lcom/siimkinks/sqlitemagic/cb$c;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 80
    iget-boolean v7, v0, Lcom/siimkinks/sqlitemagic/cc;->h:Z

    if-eqz v7, :cond_3

    .line 81
    invoke-virtual {v10, v6, v4, v5, v2}, Lcom/siimkinks/sqlitemagic/dl;->a(Lcom/siimkinks/sqlitemagic/cb$c;Lcom/siimkinks/sqlitemagic/b/d;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/b/c;

    move-result-object v2

    goto :goto_3

    .line 83
    :cond_3
    invoke-virtual {v10, v6, v4, v5, v2}, Lcom/siimkinks/sqlitemagic/dl;->b(Lcom/siimkinks/sqlitemagic/cb$c;Lcom/siimkinks/sqlitemagic/b/d;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/b/c;

    move-result-object v2

    .line 86
    :goto_3
    iget-object v4, v0, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 88
    iget-object v7, v0, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    const/4 v8, 0x0

    if-eqz v7, :cond_6

    if-eqz v2, :cond_4

    .line 89
    iget-object v1, v0, Lcom/siimkinks/sqlitemagic/cc;->a:Lcom/siimkinks/sqlitemagic/cm;

    iget v3, v0, Lcom/siimkinks/sqlitemagic/cc;->b:I

    .line 90
    invoke-static {v1, v3, v2}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;ILcom/siimkinks/sqlitemagic/b/c;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    move-object v10, v1

    goto :goto_5

    :cond_4
    iget-object v1, v0, Lcom/siimkinks/sqlitemagic/cc;->a:Lcom/siimkinks/sqlitemagic/cm;

    iget v2, v0, Lcom/siimkinks/sqlitemagic/cc;->b:I

    .line 91
    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 92
    :goto_5
    iget-object v1, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    invoke-static {v6, v1, v5, v8}, Lcom/siimkinks/sqlitemagic/cc;->a(Lcom/siimkinks/sqlitemagic/cb$c;Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z

    .line 94
    new-instance v1, Lcom/siimkinks/sqlitemagic/af;

    if-lez v4, :cond_5

    iget-object v2, v0, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    new-array v3, v4, [Ljava/lang/String;

    .line 96
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, [Ljava/lang/String;

    :cond_5
    move-object v11, v8

    iget-object v12, v0, Lcom/siimkinks/sqlitemagic/cc;->i:Lcom/siimkinks/sqlitemagic/as;

    iget-object v2, v0, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    iget-object v13, v2, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    iget-object v2, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    .line 99
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    move-object v14, v2

    check-cast v14, [Ljava/lang/String;

    move-object v9, v1

    invoke-direct/range {v9 .. v14}, Lcom/siimkinks/sqlitemagic/af;-><init>(Ljava/lang/String;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/as;Lcom/siimkinks/sqlitemagic/x;[Ljava/lang/String;)V

    return-object v1

    .line 103
    :cond_6
    iget-object v7, v0, Lcom/siimkinks/sqlitemagic/cc;->d:Lcom/siimkinks/sqlitemagic/cb$a;

    invoke-virtual {v7, v2}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/b/c;)Lcom/siimkinks/sqlitemagic/b/c;

    move-result-object v7

    if-eqz v2, :cond_7

    .line 106
    iget-object v9, v0, Lcom/siimkinks/sqlitemagic/cc;->a:Lcom/siimkinks/sqlitemagic/cm;

    iget v11, v0, Lcom/siimkinks/sqlitemagic/cc;->b:I

    invoke-static {v9, v11, v2}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;ILcom/siimkinks/sqlitemagic/b/c;)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 108
    :cond_7
    iget-object v2, v0, Lcom/siimkinks/sqlitemagic/cc;->a:Lcom/siimkinks/sqlitemagic/cm;

    iget v9, v0, Lcom/siimkinks/sqlitemagic/cc;->b:I

    invoke-static {v2, v9}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;I)Ljava/lang/String;

    move-result-object v2

    .line 111
    :goto_6
    iget-object v9, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    invoke-static {v6, v9, v5, v7}, Lcom/siimkinks/sqlitemagic/cc;->a(Lcom/siimkinks/sqlitemagic/cb$c;Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z

    move-result v6

    .line 112
    invoke-virtual {v7}, Lcom/siimkinks/sqlitemagic/b/c;->c()Z

    move-result v9

    .line 113
    new-instance v16, Lcom/siimkinks/sqlitemagic/ag;

    if-lez v4, :cond_8

    iget-object v11, v0, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    new-array v4, v4, [Ljava/lang/String;

    .line 115
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    goto :goto_7

    :cond_8
    move-object v4, v8

    :goto_7
    iget-object v11, v0, Lcom/siimkinks/sqlitemagic/cc;->i:Lcom/siimkinks/sqlitemagic/as;

    iget-object v12, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    iget-object v13, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    .line 118
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Ljava/lang/String;

    if-eqz v9, :cond_9

    move-object v13, v8

    goto :goto_8

    :cond_9
    move-object v13, v7

    :goto_8
    if-eqz v9, :cond_a

    move-object v14, v8

    goto :goto_9

    :cond_a
    move-object v14, v5

    :goto_9
    iget-boolean v5, v0, Lcom/siimkinks/sqlitemagic/cc;->h:Z

    if-nez v5, :cond_c

    if-eqz v6, :cond_b

    goto :goto_a

    :cond_b
    const/4 v15, 0x0

    goto :goto_b

    :cond_c
    :goto_a
    const/4 v15, 0x1

    :goto_b
    move-object/from16 v7, v16

    move-object v8, v2

    move-object v9, v4

    invoke-direct/range {v7 .. v15}, Lcom/siimkinks/sqlitemagic/ag;-><init>(Ljava/lang/String;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/dl;Lcom/siimkinks/sqlitemagic/as;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)V

    return-object v16

    .line 64
    :cond_d
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Select statement builder can be compiled only once"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 32
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a()Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v1

    goto :goto_1

    .line 34
    :cond_1
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/cc;->d:Lcom/siimkinks/sqlitemagic/cb$a;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/cb$a;->a()Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v1

    :goto_1
    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 36
    new-instance v3, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/b/d;->size()I

    move-result v4

    invoke-direct {v3, v4}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    goto :goto_2

    :cond_2
    move-object v3, v2

    .line 37
    :goto_2
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/cc;->c:Lcom/siimkinks/sqlitemagic/cb$c;

    .line 38
    iget-object v5, v4, Lcom/siimkinks/sqlitemagic/cb$c;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 40
    iget-boolean v6, p0, Lcom/siimkinks/sqlitemagic/cc;->h:Z

    if-eqz v6, :cond_3

    .line 41
    invoke-virtual {v5, v4, v1, v3, v0}, Lcom/siimkinks/sqlitemagic/dl;->a(Lcom/siimkinks/sqlitemagic/cb$c;Lcom/siimkinks/sqlitemagic/b/d;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/b/c;

    move-result-object v1

    goto :goto_3

    .line 43
    :cond_3
    invoke-virtual {v5, v4, v1, v3, v0}, Lcom/siimkinks/sqlitemagic/dl;->b(Lcom/siimkinks/sqlitemagic/cb$c;Lcom/siimkinks/sqlitemagic/b/d;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/b/c;

    move-result-object v1

    :goto_3
    if-nez v0, :cond_4

    .line 46
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cc;->d:Lcom/siimkinks/sqlitemagic/cb$a;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/b/c;)Lcom/siimkinks/sqlitemagic/b/c;

    :cond_4
    if-eqz p2, :cond_5

    .line 49
    invoke-static {v4, p2, v3, v2}, Lcom/siimkinks/sqlitemagic/cc;->a(Lcom/siimkinks/sqlitemagic/cb$c;Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z

    :cond_5
    if-eqz v1, :cond_6

    .line 52
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cc;->a:Lcom/siimkinks/sqlitemagic/cm;

    invoke-static {p2, v1, p1}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;)V

    goto :goto_4

    .line 54
    :cond_6
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cc;->a:Lcom/siimkinks/sqlitemagic/cm;

    invoke-static {p2, p1}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;)V

    :goto_4
    return-void
.end method
