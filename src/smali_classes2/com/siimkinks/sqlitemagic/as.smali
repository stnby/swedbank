.class public Lcom/siimkinks/sqlitemagic/as;
.super Ljava/lang/Object;
.source "DbConnectionImpl.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/ar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/as$a;
    }
.end annotation


# instance fields
.field final a:Landroidx/k/a/c;

.field final b:Lio/reactivex/v;

.field final c:[Lcom/siimkinks/sqlitemagic/ba;

.field final d:Lcom/siimkinks/sqlitemagic/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "[",
            "Lcom/siimkinks/sqlitemagic/ba;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/siimkinks/sqlitemagic/as$a;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lio/reactivex/k/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/c<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final g:Lio/reactivex/c/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/g<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/siimkinks/sqlitemagic/dm;


# direct methods
.method constructor <init>(Landroidx/k/a/c;Lio/reactivex/v;)V
    .locals 8

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    .line 42
    invoke-static {}, Lio/reactivex/k/c;->a()Lio/reactivex/k/c;

    move-result-object v0

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->f:Lio/reactivex/k/c;

    .line 44
    new-instance v0, Lcom/siimkinks/sqlitemagic/as$1;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/as$1;-><init>(Lcom/siimkinks/sqlitemagic/as;)V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->h:Lcom/siimkinks/sqlitemagic/dm;

    .line 83
    new-instance v0, Lcom/siimkinks/sqlitemagic/as$2;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/as$2;-><init>(Lcom/siimkinks/sqlitemagic/as;)V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->g:Lio/reactivex/c/g;

    .line 93
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/as;->a:Landroidx/k/a/c;

    .line 94
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/as;->b:Lio/reactivex/v;

    .line 95
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cn;->a()[Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    .line 97
    array-length v0, p1

    .line 98
    new-instance v1, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {v1, v0}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 100
    aget-object v3, p1, v2

    .line 101
    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;)I

    move-result v4

    .line 102
    new-array v5, v4, [Lcom/siimkinks/sqlitemagic/ba;

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v4, :cond_0

    .line 104
    new-instance v7, Lcom/siimkinks/sqlitemagic/ba;

    invoke-direct {v7, p0}, Lcom/siimkinks/sqlitemagic/ba;-><init>(Lcom/siimkinks/sqlitemagic/as;)V

    aput-object v7, v5, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 106
    :cond_0
    invoke-virtual {v1, v3, v5}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 108
    :cond_1
    iput-object v1, p0, Lcom/siimkinks/sqlitemagic/as;->d:Lcom/siimkinks/sqlitemagic/b/c;

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    .line 110
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/as;->d:Lcom/siimkinks/sqlitemagic/b/c;

    :goto_2
    const-string p1, ""

    .line 112
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;)I

    move-result p1

    .line 113
    new-array v0, p1, [Lcom/siimkinks/sqlitemagic/ba;

    :goto_3
    if-ge p2, p1, :cond_3

    .line 115
    new-instance v1, Lcom/siimkinks/sqlitemagic/ba;

    invoke-direct {v1, p0}, Lcom/siimkinks/sqlitemagic/ba;-><init>(Lcom/siimkinks/sqlitemagic/as;)V

    aput-object v1, v0, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_3

    .line 117
    :cond_3
    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->c:[Lcom/siimkinks/sqlitemagic/ba;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)Landroidx/k/a/f;
    .locals 1

    .line 176
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/as;->d()Landroidx/k/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroidx/k/a/b;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;
    .locals 1

    if-nez p1, :cond_0

    .line 166
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/as;->c:[Lcom/siimkinks/sqlitemagic/ba;

    aget-object p1, p1, p2

    return-object p1

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->d:Lcom/siimkinks/sqlitemagic/b/c;

    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/siimkinks/sqlitemagic/ba;

    aget-object p1, p1, p2

    return-object p1

    .line 170
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No submodules configured"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a()V
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/as;->d()Landroidx/k/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/cn;->b(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/as;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    :cond_0
    return-void
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/d;)V
    .locals 3

    .line 202
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/siimkinks/sqlitemagic/as$a;

    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/as$a;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    goto :goto_0

    .line 206
    :cond_0
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "TRIGGER %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->f:Lio/reactivex/k/c;

    invoke-virtual {v0, p1}, Lio/reactivex/k/c;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final b()Lcom/siimkinks/sqlitemagic/dm;
    .locals 4

    .line 138
    new-instance v0, Lcom/siimkinks/sqlitemagic/as$a;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/siimkinks/sqlitemagic/as$a;

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/as$a;-><init>(Lcom/siimkinks/sqlitemagic/as$a;)V

    .line 139
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 140
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "TXN BEGIN %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/as;->d()Landroidx/k/a/b;

    move-result-object v1

    invoke-interface {v1, v0}, Landroidx/k/a/b;->a(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 143
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->h:Lcom/siimkinks/sqlitemagic/dm;

    return-object v0
.end method

.method b(Ljava/lang/String;)V
    .locals 3

    .line 180
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/siimkinks/sqlitemagic/as$a;

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/as$a;->a(Ljava/lang/String;)Z

    goto :goto_0

    .line 184
    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    .line 185
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "TRIGGER %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->f:Lio/reactivex/k/c;

    invoke-virtual {v0, p1}, Lio/reactivex/k/c;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method c()Landroidx/k/a/b;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->a:Landroidx/k/a/c;

    invoke-interface {v0}, Landroidx/k/a/c;->c()Landroidx/k/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 5

    .line 122
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->f:Lio/reactivex/k/c;

    invoke-virtual {v0}, Lio/reactivex/k/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->f:Lio/reactivex/k/c;

    invoke-virtual {v0}, Lio/reactivex/k/c;->onComplete()V

    .line 126
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->c:[Lcom/siimkinks/sqlitemagic/ba;

    .line 127
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    .line 128
    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/ba;->a()V

    const/4 v4, 0x0

    .line 129
    aput-object v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->a:Landroidx/k/a/c;

    invoke-interface {v0}, Landroidx/k/a/c;->d()V

    const-string v0, "Closed database [name=%s]"

    const/4 v1, 0x1

    .line 132
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/as;->a:Landroidx/k/a/c;

    invoke-interface {v3}, Landroidx/k/a/c;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method d()Landroidx/k/a/b;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->a:Landroidx/k/a/c;

    invoke-interface {v0}, Landroidx/k/a/c;->b()Landroidx/k/a/b;

    move-result-object v0

    return-object v0
.end method
