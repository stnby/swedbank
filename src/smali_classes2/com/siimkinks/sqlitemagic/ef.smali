.class public final Lcom/siimkinks/sqlitemagic/ef;
.super Ljava/lang/Object;
.source "WidgetGeneratedClassesManager.java"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public static a(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method

.method public static b(Landroidx/k/a/b;)V
    .locals 3

    .line 18
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 20
    :try_start_0
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Creating tables"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v1, "CREATE TABLE IF NOT EXISTS widget_enrolled_account_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, account_id TEXT DEFAULT \'\')"

    .line 21
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 22
    invoke-interface {p0}, Landroidx/k/a/b;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 24
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 26
    :cond_1
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-void

    :goto_1
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 27
    throw v0
.end method

.method public static c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;
    .locals 4

    .line 32
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 34
    :try_start_0
    new-instance v1, Lcom/siimkinks/sqlitemagic/b/d;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/ef;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    .line 35
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "Clearing data"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v2, "DELETE FROM widget_enrolled_account_data"

    .line 36
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 37
    invoke-interface {p0}, Landroidx/k/a/b;->c()V

    const-string v2, "widget_enrolled_account_data"

    .line 38
    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 41
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    :cond_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 44
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 45
    throw v0
.end method

.method public static d(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method
