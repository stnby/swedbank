.class public Lcom/siimkinks/sqlitemagic/dl;
.super Ljava/lang/Object;
.source "Table.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final v:Lcom/siimkinks/sqlitemagic/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dl<",
            "*>;"
        }
    .end annotation
.end field


# instance fields
.field final A:Z

.field private final a:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "***TT;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final w:Ljava/lang/String;

.field final x:Ljava/lang/String;

.field final y:Ljava/lang/String;

.field final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 20
    new-instance v0, Lcom/siimkinks/sqlitemagic/dl;

    const-string v1, ""

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/siimkinks/sqlitemagic/dl;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/siimkinks/sqlitemagic/dl;->v:Lcom/siimkinks/sqlitemagic/dl;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/dl;->x:Ljava/lang/String;

    .line 35
    iput p3, p0, Lcom/siimkinks/sqlitemagic/dl;->z:I

    if-eqz p2, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 37
    :goto_0
    iput-boolean p3, p0, Lcom/siimkinks/sqlitemagic/dl;->A:Z

    if-eqz p3, :cond_1

    move-object p1, p2

    .line 38
    :cond_1
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/dl;->y:Ljava/lang/String;

    .line 39
    new-instance p1, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "*"

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/dl;->a:Lcom/siimkinks/sqlitemagic/x;

    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/cb$c;Lcom/siimkinks/sqlitemagic/b/d;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/b/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cb$c;",
            "Lcom/siimkinks/sqlitemagic/b/d;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bi;
    .locals 7

    .line 104
    new-instance v6, Lcom/siimkinks/sqlitemagic/dl$1;

    const-string v3, ""

    const-string v4, "ON "

    move-object v0, v6

    move-object v1, p0

    move-object v2, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/siimkinks/sqlitemagic/dl$1;-><init>(Lcom/siimkinks/sqlitemagic/dl;Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/bb;)V

    return-object v6
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/ca$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/ca$b<",
            "TT;>;"
        }
    .end annotation

    .line 196
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "not implemented"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method final a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dl;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;"
        }
    .end annotation

    .line 70
    new-instance v0, Lcom/siimkinks/sqlitemagic/dl;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    iget v2, p0, Lcom/siimkinks/sqlitemagic/dl;->z:I

    invoke-direct {v0, v1, p1, v2}, Lcom/siimkinks/sqlitemagic/dl;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final a()Lcom/siimkinks/sqlitemagic/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/x<",
            "***TT;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dl;->a:Lcom/siimkinks/sqlitemagic/x;

    return-object v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/dl;->A:Z

    if-eqz v0, :cond_0

    const-string v0, " AS "

    .line 45
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dl;->x:Ljava/lang/String;

    .line 46
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method final a(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    .line 205
    :cond_1
    :try_start_0
    check-cast p1, Lcom/siimkinks/sqlitemagic/dl;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :catch_0
    return v0
.end method

.method a(Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 62
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 63
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method b(Lcom/siimkinks/sqlitemagic/cb$c;Lcom/siimkinks/sqlitemagic/b/d;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/b/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cb$c;",
            "Lcom/siimkinks/sqlitemagic/b/d;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    .line 220
    :cond_1
    :try_start_0
    check-cast p1, Lcom/siimkinks/sqlitemagic/dl;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dl;->y:Ljava/lang/String;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/dl;->y:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :catch_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dl;->y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    return-object v0
.end method
