.class Lcom/siimkinks/sqlitemagic/as$1;
.super Ljava/lang/Object;
.source "DbConnectionImpl.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/dm;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/as;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/siimkinks/sqlitemagic/as;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/as;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/as$1;->a:Lcom/siimkinks/sqlitemagic/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 47
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "TXN SUCCESS %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/as$1;->a:Lcom/siimkinks/sqlitemagic/as;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as$1;->a:Lcom/siimkinks/sqlitemagic/as;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->d()Landroidx/k/a/b;

    move-result-object v0

    invoke-interface {v0}, Landroidx/k/a/b;->c()V

    return-void
.end method

.method public b()V
    .locals 4

    .line 63
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as$1;->a:Lcom/siimkinks/sqlitemagic/as;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/siimkinks/sqlitemagic/as$a;

    if-eqz v0, :cond_2

    .line 67
    iget-object v1, v0, Lcom/siimkinks/sqlitemagic/as$a;->a:Lcom/siimkinks/sqlitemagic/as$a;

    .line 68
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/as$1;->a:Lcom/siimkinks/sqlitemagic/as;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 69
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "TXN END %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/as$1;->a:Lcom/siimkinks/sqlitemagic/as;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/as;->d()Landroidx/k/a/b;

    move-result-object v1

    invoke-interface {v1}, Landroidx/k/a/b;->b()V

    .line 72
    iget-boolean v1, v0, Lcom/siimkinks/sqlitemagic/as$a;->b:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as$a;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/as$1;->a:Lcom/siimkinks/sqlitemagic/as;

    invoke-virtual {v1, v0}, Lcom/siimkinks/sqlitemagic/as;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    :cond_1
    return-void

    .line 65
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not in transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 0

    .line 79
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/as$1;->b()V

    return-void
.end method
