.class public abstract Lcom/siimkinks/sqlitemagic/ca;
.super Ljava/lang/Object;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/ca$b;,
        Lcom/siimkinks/sqlitemagic/ca$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final h:Lcom/siimkinks/sqlitemagic/as;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/as;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/ca;->h:Lcom/siimkinks/sqlitemagic/as;

    return-void
.end method


# virtual methods
.method a(Z)Landroid/database/Cursor;
    .locals 1

    if-eqz p1, :cond_1

    .line 92
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/ca;->h:Lcom/siimkinks/sqlitemagic/as;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/as;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {p1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 93
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot execute observable query in a transaction."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method abstract b(Landroid/database/Cursor;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation
.end method

.method public final e()Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/siimkinks/sqlitemagic/ca$1;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/ca$1;-><init>(Lcom/siimkinks/sqlitemagic/ca;)V

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    return-object v0
.end method
