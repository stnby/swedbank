.class public final Lcom/siimkinks/sqlitemagic/cx$b;
.super Ljava/lang/Object;
.source "SqliteMagic_CustomerData_Handler.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/a/b;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/siimkinks/sqlitemagic/a/b;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/siimkinks/sqlitemagic/as;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/siimkinks/sqlitemagic/cx$b;
    .locals 1

    .line 1082
    new-instance v0, Lcom/siimkinks/sqlitemagic/cx$b;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/cx$b;-><init>()V

    return-object v0
.end method


# virtual methods
.method public b()I
    .locals 5

    .line 1094
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cx$b;->a:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cx$b;->a:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    .line 1095
    :goto_0
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->d()Landroidx/k/a/b;

    move-result-object v1

    const-string v2, "customer_data"

    const-string v3, "1"

    const/4 v4, 0x0

    .line 1096
    invoke-interface {v1, v2, v3, v4}, Landroidx/k/a/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_1

    .line 1098
    sget-object v2, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/am;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    :cond_1
    return v1
.end method

.method public c()Ljava/lang/Integer;
    .locals 1

    .line 1105
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/cx$b;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 1073
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/cx$b;->c()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1112
    invoke-static {p0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
