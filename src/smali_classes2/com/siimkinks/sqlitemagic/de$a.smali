.class public final Lcom/siimkinks/sqlitemagic/de$a;
.super Ljava/lang/Object;
.source "SqliteMagic_SuggestedPaymentData_Handler.java"

# interfaces
.implements Lio/reactivex/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/de;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/transfer/suggested/b;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/siimkinks/sqlitemagic/as;

.field private c:I


# direct methods
.method private constructor <init>(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/transfer/suggested/b;",
            ">;)V"
        }
    .end annotation

    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 209
    iput v0, p0, Lcom/siimkinks/sqlitemagic/de$a;->c:I

    .line 212
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/de$a;->a:Ljava/lang/Iterable;

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/de$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/transfer/suggested/b;",
            ">;)",
            "Lcom/siimkinks/sqlitemagic/de$a;"
        }
    .end annotation

    .line 217
    new-instance v0, Lcom/siimkinks/sqlitemagic/de$a;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/de$a;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method


# virtual methods
.method public a(Lio/reactivex/c;)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 277
    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/de$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/de$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v3, "Transfer"

    const/4 v4, 0x3

    .line 278
    invoke-virtual {v0, v3, v4}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v3

    .line 279
    new-instance v4, Lcom/siimkinks/sqlitemagic/bo;

    iget v5, v1, Lcom/siimkinks/sqlitemagic/de$a;->c:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v4, v5, v7, v6}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    .line 280
    invoke-static {}, Lio/reactivex/b/d;->a()Lio/reactivex/b/c;

    move-result-object v5

    .line 281
    invoke-interface {v2, v5}, Lio/reactivex/c;->a(Lio/reactivex/b/c;)V

    const-string v6, "suggested_payment_data"

    const-string v8, "INSERT%s INTO suggested_payment_data (suggested_payment_id, customer_id, suggested_payment_creditor_bank, suggested_payment_rank, suggested_payment_type, sender_account_id, recipient_name, recipient_iban, amount, currency, description, reference_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 282
    invoke-virtual {v4, v6, v8, v3}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v3

    .line 284
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v6

    .line 287
    :try_start_0
    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 288
    :try_start_1
    iget-object v8, v1, Lcom/siimkinks/sqlitemagic/de$a;->a:Ljava/lang/Iterable;

    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    const/4 v9, 0x0

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/swedbank/mobile/data/transfer/suggested/b;

    .line 289
    sget-boolean v11, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v12, 0x1

    if-eqz v11, :cond_1

    const-string v11, "INSERT\n  table: suggested_payment_data\n  object: %s"

    new-array v13, v12, [Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/swedbank/mobile/data/transfer/suggested/b;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v7

    invoke-static {v11, v13}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 290
    :cond_1
    invoke-interface {v5}, Lio/reactivex/b/c;->b()Z

    move-result v11

    if-nez v11, :cond_5

    .line 293
    invoke-static {v3, v10}, Lcom/swedbank/mobile/data/transfer/suggested/a;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/suggested/b;)V

    .line 294
    invoke-interface {v3}, Landroidx/k/a/f;->b()J

    move-result-wide v13

    .line 295
    sget-boolean v11, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v11, :cond_2

    const-string v11, "INSERT id: %s"

    new-array v15, v12, [Ljava/lang/Object;

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v15, v7

    invoke-static {v11, v15}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    const-wide/16 v15, -0x1

    cmp-long v11, v13, v15

    if-nez v11, :cond_4

    .line 297
    iget-boolean v11, v4, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v11, :cond_3

    goto :goto_1

    .line 298
    :cond_3
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to insert "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v9, 0x1

    goto :goto_1

    .line 291
    :cond_5
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    const-string v7, "Subscriber unsubscribed unexpectedly"

    invoke-direct {v0, v7}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_6
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 305
    :try_start_2
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 312
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v9, :cond_8

    .line 314
    sget-object v3, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/di;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    .line 304
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 308
    :try_start_5
    invoke-interface {v5}, Lio/reactivex/b/c;->b()Z

    move-result v3

    if-nez v3, :cond_7

    .line 309
    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 312
    :cond_7
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 316
    :cond_8
    :goto_2
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 318
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return-void

    .line 312
    :goto_3
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 316
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 317
    throw v0
.end method

.method public a()Z
    .locals 14

    .line 237
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/de$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/de$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v1, "Transfer"

    const/4 v2, 0x3

    .line 238
    invoke-virtual {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v1

    .line 239
    new-instance v2, Lcom/siimkinks/sqlitemagic/bo;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/de$a;->c:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, v5, v4}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    const-string v3, "suggested_payment_data"

    const-string v4, "INSERT%s INTO suggested_payment_data (suggested_payment_id, customer_id, suggested_payment_creditor_bank, suggested_payment_rank, suggested_payment_type, sender_account_id, recipient_name, recipient_iban, amount, currency, description, reference_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 240
    invoke-virtual {v2, v3, v4, v1}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v1

    .line 242
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v3

    .line 245
    :try_start_0
    monitor-enter v1
    :try_end_0
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 246
    :try_start_1
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/de$a;->a:Ljava/lang/Iterable;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v6, 0x0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/swedbank/mobile/data/transfer/suggested/b;

    .line 247
    sget-boolean v8, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v9, 0x1

    if-eqz v8, :cond_1

    const-string v8, "INSERT\n  table: suggested_payment_data\n  object: %s"

    new-array v10, v9, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/transfer/suggested/b;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v5

    invoke-static {v8, v10}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    :cond_1
    invoke-static {v1, v7}, Lcom/swedbank/mobile/data/transfer/suggested/a;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/suggested/b;)V

    .line 249
    invoke-interface {v1}, Landroidx/k/a/f;->b()J

    move-result-wide v10

    .line 250
    sget-boolean v8, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v8, :cond_2

    const-string v8, "INSERT id: %s"

    new-array v12, v9, [Ljava/lang/Object;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v12, v5

    invoke-static {v8, v12}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    const-wide/16 v12, -0x1

    cmp-long v8, v10, v12

    if-nez v8, :cond_4

    .line 252
    iget-boolean v8, v2, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v8, :cond_3

    goto :goto_1

    .line 253
    :cond_3
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to insert "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v6, 0x1

    goto :goto_1

    .line 259
    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    :try_start_2
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_2
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 267
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v6, :cond_6

    .line 269
    sget-object v1, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/di;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    .line 271
    :cond_6
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v6

    :catchall_0
    move-exception v0

    .line 259
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 264
    :try_start_5
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_7

    const-string v1, "Operation failed"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 267
    :cond_7
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 271
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v5

    .line 267
    :goto_2
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 271
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    .line 272
    throw v0
.end method
