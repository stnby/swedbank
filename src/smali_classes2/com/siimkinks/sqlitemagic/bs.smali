.class public final Lcom/siimkinks/sqlitemagic/bs;
.super Ljava/lang/Object;
.source "OverviewGeneratedClassesManager.java"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 0

    const/4 p0, 0x5

    return p0
.end method

.method public static a(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method

.method public static b(Landroidx/k/a/b;)V
    .locals 3

    .line 21
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 23
    :try_start_0
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Creating tables"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v1, "CREATE TABLE IF NOT EXISTS combined_transaction_query_metadata (local_id INTEGER PRIMARY KEY AUTOINCREMENT, account_id TEXT DEFAULT \'\', start_cursor TEXT DEFAULT \'\', end_cursor TEXT DEFAULT \'\', has_next_page INTEGER DEFAULT 0)"

    .line 24
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS single_transaction_query_metadata (local_id INTEGER PRIMARY KEY AUTOINCREMENT, account_id TEXT DEFAULT \'\', start_cursor TEXT DEFAULT \'\', end_cursor TEXT DEFAULT \'\', has_next_page INTEGER DEFAULT 0)"

    .line 25
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS single_transaction_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, remote_cursor TEXT UNIQUE, account_id TEXT DEFAULT \'\', transaction_date INTEGER DEFAULT 0, transaction_bank_date INTEGER DEFAULT 0, transaction_number INTEGER DEFAULT 0, transaction_part INTEGER DEFAULT 0, amount TEXT DEFAULT \'\', currency TEXT DEFAULT \'\', counterparty TEXT DEFAULT \'\', counterparty_account TEXT DEFAULT \'\', description TEXT DEFAULT \'\', type TEXT DEFAULT \'\', payment_type TEXT DEFAULT \'\', direction TEXT DEFAULT \'\', number_of_transactions INTEGER DEFAULT 0)"

    .line 26
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS combined_transaction_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, remote_cursor TEXT UNIQUE, account_id TEXT DEFAULT \'\', transaction_date INTEGER DEFAULT 0, transaction_bank_date INTEGER DEFAULT 0, transaction_number INTEGER DEFAULT 0, transaction_part INTEGER DEFAULT 0, amount TEXT DEFAULT \'\', currency TEXT DEFAULT \'\', counterparty TEXT DEFAULT \'\', counterparty_account TEXT DEFAULT \'\', description TEXT DEFAULT \'\', type TEXT DEFAULT \'\', payment_type TEXT DEFAULT \'\', direction TEXT DEFAULT \'\', number_of_transactions INTEGER DEFAULT 0)"

    .line 27
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS overview_search_keyword (local_id INTEGER PRIMARY KEY AUTOINCREMENT, keyword TEXT UNIQUE, search_count INTEGER DEFAULT 0, last_searched INTEGER DEFAULT 0)"

    .line 28
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 29
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "Creating views"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/data/overview/transaction/k;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v2, "transaction_data_view"

    invoke-static {p0, v1, v2}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    .line 31
    invoke-interface {p0}, Landroidx/k/a/b;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 33
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_2

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35
    :cond_2
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-void

    :goto_1
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 36
    throw v0
.end method

.method public static c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;
    .locals 7

    .line 41
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 43
    :try_start_0
    new-instance v1, Lcom/siimkinks/sqlitemagic/b/d;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/bs;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    .line 44
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "Clearing data"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v2, "DELETE FROM combined_transaction_query_metadata"

    .line 45
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM single_transaction_query_metadata"

    .line 46
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM single_transaction_data"

    .line 47
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM combined_transaction_data"

    .line 48
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM overview_search_keyword"

    .line 49
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 50
    invoke-interface {p0}, Landroidx/k/a/b;->c()V

    const-string v2, "combined_transaction_query_metadata"

    const-string v3, "single_transaction_query_metadata"

    const-string v4, "single_transaction_data"

    const-string v5, "combined_transaction_data"

    const-string v6, "overview_search_keyword"

    .line 51
    filled-new-array {v2, v3, v4, v5, v6}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 54
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    :cond_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 58
    throw v0
.end method

.method public static d(Landroidx/k/a/b;)V
    .locals 2

    .line 62
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Migrating views"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v0, "DROP VIEW IF EXISTS transaction_data_view"

    .line 63
    invoke-interface {p0, v0}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 64
    sget-object v0, Lcom/swedbank/mobile/data/overview/transaction/k;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v1, "transaction_data_view"

    invoke-static {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    return-void
.end method
