.class public final Lcom/siimkinks/sqlitemagic/db$a;
.super Ljava/lang/Object;
.source "SqliteMagic_PredefinedPaymentData_Handler.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/a/a;
.implements Lio/reactivex/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/db;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/siimkinks/sqlitemagic/as;

.field private c:I

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method private constructor <init>(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            ">;)V"
        }
    .end annotation

    .line 683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 676
    iput v0, p0, Lcom/siimkinks/sqlitemagic/db$a;->c:I

    .line 678
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    .line 684
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/db$a;->a:Ljava/lang/Iterable;

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/db$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/transfer/predefined/a;",
            ">;)",
            "Lcom/siimkinks/sqlitemagic/db$a;"
        }
    .end annotation

    .line 689
    new-instance v0, Lcom/siimkinks/sqlitemagic/db$a;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/db$a;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/siimkinks/sqlitemagic/a/a;
    .locals 1

    const/4 v0, 0x1

    .line 719
    iput-boolean v0, p0, Lcom/siimkinks/sqlitemagic/db$a;->e:Z

    return-object p0
.end method

.method public a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Lcom/siimkinks/sqlitemagic/dt<",
            "Ljava/lang/Object;",
            ">;>(TC;)",
            "Lcom/siimkinks/sqlitemagic/a/a;"
        }
    .end annotation

    .line 711
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    check-cast p1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lio/reactivex/c;)V
    .locals 21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 836
    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/db$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/db$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v3, "Transfer"

    const/4 v4, 0x1

    .line 837
    invoke-virtual {v0, v3, v4}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v3

    .line 838
    invoke-static {}, Lio/reactivex/b/d;->a()Lio/reactivex/b/c;

    move-result-object v12

    .line 839
    invoke-interface {v2, v12}, Lio/reactivex/c;->a(Lio/reactivex/b/c;)V

    .line 840
    iget-boolean v5, v1, Lcom/siimkinks/sqlitemagic/db$a;->e:Z

    const/16 v6, 0xb

    const/4 v15, 0x0

    if-eqz v5, :cond_b

    .line 841
    new-instance v11, Lcom/siimkinks/sqlitemagic/ec;

    iget v5, v1, Lcom/siimkinks/sqlitemagic/db$a;->c:I

    invoke-direct {v11, v5}, Lcom/siimkinks/sqlitemagic/ec;-><init>(I)V

    .line 842
    new-instance v10, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {v10, v6}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    .line 844
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v16

    .line 847
    :try_start_0
    iget-object v5, v1, Lcom/siimkinks/sqlitemagic/db$a;->a:Ljava/lang/Iterable;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v17

    const/16 v18, 0x0

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v9, v5

    check-cast v9, Lcom/swedbank/mobile/data/transfer/predefined/a;

    .line 848
    sget-boolean v5, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v5, :cond_1

    const-string v5, "PERSIST\n  table: predefined_payment_data\n  object: %s"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/swedbank/mobile/data/transfer/predefined/a;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v15

    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 849
    :cond_1
    invoke-interface {v12}, Lio/reactivex/b/c;->b()Z

    move-result v5

    if-nez v5, :cond_7

    .line 852
    invoke-static {v9, v10}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Lcom/swedbank/mobile/data/transfer/predefined/a;Lcom/siimkinks/sqlitemagic/b/c;)V

    const-string v5, "predefined_payment_data"

    .line 854
    iget-object v6, v1, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 857
    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    :goto_2
    move-object/from16 v19, v5

    goto :goto_3

    :cond_2
    const-string v5, "local_id"

    goto :goto_2

    :goto_3
    const/4 v6, 0x1

    const-string v7, "predefined_payment_data"

    const/16 v8, 0xb

    move-object v5, v11

    move-object/from16 v20, v9

    move-object v9, v10

    move-object v13, v10

    move-object/from16 v10, v19

    move-object v14, v11

    move-object v11, v3

    .line 861
    invoke-virtual/range {v5 .. v11}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v5

    invoke-interface {v5}, Landroidx/k/a/f;->a()I

    move-result v5

    if-gtz v5, :cond_6

    .line 863
    sget-boolean v5, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v5, :cond_3

    const-string v5, "PERSIST update failed; trying insertion"

    new-array v6, v15, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    const-string v5, "local_id"

    .line 864
    invoke-virtual {v13, v5}, Lcom/siimkinks/sqlitemagic/b/c;->d(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "predefined_payment_data"

    const/16 v8, 0xb

    const-string v10, "local_id"

    move-object v5, v14

    move-object v9, v13

    move-object v11, v3

    .line 866
    invoke-virtual/range {v5 .. v11}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v5

    invoke-interface {v5}, Landroidx/k/a/f;->b()J

    move-result-wide v5

    .line 867
    sget-boolean v7, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v7, :cond_4

    const-string v7, "PERSIST insert id: %s"

    new-array v8, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v15

    invoke-static {v7, v8}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-nez v5, :cond_6

    .line 869
    iget-boolean v5, v14, Lcom/siimkinks/sqlitemagic/ec;->a:Z

    if-eqz v5, :cond_5

    goto :goto_4

    .line 870
    :cond_5
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to persist "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v5, v20

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/16 v18, 0x1

    :goto_4
    move-object v10, v13

    move-object v11, v14

    goto/16 :goto_1

    .line 850
    :cond_7
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    const-string v3, "Subscriber unsubscribed unexpectedly"

    invoke-direct {v0, v3}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 879
    :cond_8
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 886
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v18, :cond_a

    .line 888
    sget-object v3, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/bx;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    goto :goto_5

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_0
    move-exception v0

    .line 882
    :try_start_1
    invoke-interface {v12}, Lio/reactivex/b/c;->b()Z

    move-result v3

    if-nez v3, :cond_9

    .line 883
    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 886
    :cond_9
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 890
    :cond_a
    :goto_5
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    goto/16 :goto_c

    .line 886
    :goto_6
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 890
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 891
    throw v0

    .line 893
    :cond_b
    new-instance v5, Lcom/siimkinks/sqlitemagic/bo;

    iget v7, v1, Lcom/siimkinks/sqlitemagic/db$a;->c:I

    const/4 v8, 0x2

    iget-object v9, v1, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    invoke-direct {v5, v7, v8, v9}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    const-string v7, "predefined_payment_data"

    const-string v8, "UPDATE%s predefined_payment_data SET customer_id=?, predefined_payment_id=?, predefined_payment_name=?, sender_account_id=?, recipient_name=?, recipient_iban=?, amount=?, currency=?, description=?, reference_number=? WHERE local_id=?"

    .line 894
    invoke-virtual {v5, v7, v8, v3}, Lcom/siimkinks/sqlitemagic/bo;->b(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v7

    const-string v8, "predefined_payment_data"

    const-string v9, "INSERT%s INTO predefined_payment_data (customer_id, predefined_payment_id, predefined_payment_name, sender_account_id, recipient_name, recipient_iban, amount, currency, description, reference_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 895
    invoke-virtual {v5, v8, v9, v3}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v3

    const-string v8, "predefined_payment_data"

    .line 896
    iget-object v9, v1, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    invoke-static {v8, v9}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v8

    if-eqz v8, :cond_c

    .line 899
    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    goto :goto_7

    :cond_c
    const-string v8, "local_id"

    .line 904
    :goto_7
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v9

    .line 907
    :try_start_2
    monitor-enter v7
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 908
    :try_start_3
    monitor-enter v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 909
    :try_start_4
    iget-object v10, v1, Lcom/siimkinks/sqlitemagic/db$a;->a:Ljava/lang/Iterable;

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    const/4 v11, 0x0

    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_15

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/swedbank/mobile/data/transfer/predefined/a;

    .line 910
    sget-boolean v14, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v14, :cond_d

    const-string v14, "PERSIST\n  table: predefined_payment_data\n  object: %s"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-virtual {v13}, Lcom/swedbank/mobile/data/transfer/predefined/a;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v6, v15

    invoke-static {v14, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 911
    :cond_d
    invoke-interface {v12}, Lio/reactivex/b/c;->b()Z

    move-result v6

    if-nez v6, :cond_14

    .line 915
    invoke-static {v8, v13}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/predefined/a;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 916
    invoke-static {v7, v13}, Lcom/swedbank/mobile/data/transfer/predefined/g;->b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/predefined/a;)V

    const/16 v6, 0xb

    .line 917
    invoke-static {v7, v6, v8, v13}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/transfer/predefined/a;)V

    .line 918
    invoke-interface {v7}, Landroidx/k/a/f;->a()I

    move-result v14

    goto :goto_9

    :cond_e
    const/16 v6, 0xb

    const/4 v14, 0x0

    :goto_9
    if-gtz v14, :cond_12

    .line 921
    sget-boolean v14, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v14, :cond_f

    const-string v14, "PERSIST update failed; trying insertion"

    new-array v6, v15, [Ljava/lang/Object;

    invoke-static {v14, v6}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 922
    :cond_f
    invoke-static {v3, v13}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/predefined/a;)V

    .line 923
    invoke-interface {v3}, Landroidx/k/a/f;->b()J

    move-result-wide v16

    .line 924
    sget-boolean v6, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v6, :cond_10

    const-string v6, "INSERT id: %s"

    new-array v14, v4, [Ljava/lang/Object;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v14, v15

    invoke-static {v6, v14}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_10
    const-wide/16 v18, -0x1

    cmp-long v6, v16, v18

    if-nez v6, :cond_13

    .line 926
    iget-boolean v6, v5, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v6, :cond_11

    goto :goto_a

    .line 927
    :cond_11
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to persist "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    const-wide/16 v18, -0x1

    :cond_13
    const/4 v11, 0x1

    :goto_a
    const/16 v6, 0xb

    goto/16 :goto_8

    .line 912
    :cond_14
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    const-string v4, "Subscriber unsubscribed unexpectedly"

    invoke-direct {v0, v4}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 936
    :cond_15
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 937
    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 938
    :try_start_6
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 945
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v11, :cond_17

    .line 947
    sget-object v3, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/bx;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    goto :goto_b

    :catchall_1
    move-exception v0

    .line 936
    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    :catchall_2
    move-exception v0

    .line 937
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    move-exception v0

    goto :goto_d

    :catch_1
    move-exception v0

    .line 941
    :try_start_a
    invoke-interface {v12}, Lio/reactivex/b/c;->b()Z

    move-result v3

    if-nez v3, :cond_16

    .line 942
    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 945
    :cond_16
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 949
    :cond_17
    :goto_b
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 951
    invoke-virtual {v5}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    :goto_c
    return-void

    .line 945
    :goto_d
    invoke-interface {v9}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 949
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 950
    throw v0
.end method

.method public synthetic b(Lcom/siimkinks/sqlitemagic/dt;)Ljava/lang/Object;
    .locals 0

    .line 670
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/db$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object p1

    return-object p1
.end method

.method public b()Z
    .locals 20

    move-object/from16 v1, p0

    .line 725
    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/db$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/db$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v2, "Transfer"

    const/4 v3, 0x1

    .line 726
    invoke-virtual {v0, v2, v3}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v2

    .line 727
    iget-boolean v4, v1, Lcom/siimkinks/sqlitemagic/db$a;->e:Z

    const-wide/16 v11, -0x1

    const/16 v5, 0xb

    const/4 v13, 0x0

    if-eqz v4, :cond_a

    .line 728
    new-instance v14, Lcom/siimkinks/sqlitemagic/ec;

    iget v4, v1, Lcom/siimkinks/sqlitemagic/db$a;->c:I

    invoke-direct {v14, v4}, Lcom/siimkinks/sqlitemagic/ec;-><init>(I)V

    .line 729
    new-instance v15, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {v15, v5}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    .line 731
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v16

    .line 734
    :try_start_0
    iget-object v4, v1, Lcom/siimkinks/sqlitemagic/db$a;->a:Ljava/lang/Iterable;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v17

    const/16 v18, 0x0

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/swedbank/mobile/data/transfer/predefined/a;

    .line 735
    sget-boolean v4, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v4, :cond_1

    const-string v4, "PERSIST\n  table: predefined_payment_data\n  object: %s"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/swedbank/mobile/data/transfer/predefined/a;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v13

    invoke-static {v4, v5}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 736
    :cond_1
    invoke-static {v10, v15}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Lcom/swedbank/mobile/data/transfer/predefined/a;Lcom/siimkinks/sqlitemagic/b/c;)V

    const-string v4, "predefined_payment_data"

    .line 738
    iget-object v5, v1, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 741
    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    :goto_2
    move-object v9, v4

    goto :goto_3

    :cond_2
    const-string v4, "local_id"

    goto :goto_2

    :goto_3
    const/4 v5, 0x1

    const-string v6, "predefined_payment_data"

    const/16 v7, 0xb

    move-object v4, v14

    move-object v8, v15

    move-object/from16 v19, v10

    move-object v10, v2

    .line 745
    invoke-virtual/range {v4 .. v10}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v4

    invoke-interface {v4}, Landroidx/k/a/f;->a()I

    move-result v4

    if-gtz v4, :cond_6

    .line 747
    sget-boolean v4, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v4, :cond_3

    const-string v4, "PERSIST update failed; trying insertion"

    new-array v5, v13, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    const-string v4, "local_id"

    .line 748
    invoke-virtual {v15, v4}, Lcom/siimkinks/sqlitemagic/b/c;->d(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "predefined_payment_data"

    const/16 v7, 0xb

    const-string v9, "local_id"

    move-object v4, v14

    move-object v8, v15

    move-object v10, v2

    .line 750
    invoke-virtual/range {v4 .. v10}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v4

    invoke-interface {v4}, Landroidx/k/a/f;->b()J

    move-result-wide v4

    .line 751
    sget-boolean v6, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v6, :cond_4

    const-string v6, "PERSIST insert id: %s"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v13

    invoke-static {v6, v7}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    cmp-long v4, v4, v11

    if-nez v4, :cond_6

    .line 753
    iget-boolean v4, v14, Lcom/siimkinks/sqlitemagic/ec;->a:Z

    if-eqz v4, :cond_5

    goto/16 :goto_1

    .line 754
    :cond_5
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to persist "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v4, v19

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 763
    :cond_7
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 770
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v18, :cond_8

    .line 772
    sget-object v2, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bx;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    :cond_8
    return v18

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_0
    move-exception v0

    .line 767
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_9

    const-string v2, "Operation failed"

    new-array v3, v13, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 770
    :cond_9
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return v13

    :goto_4
    invoke-interface/range {v16 .. v16}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 774
    throw v0

    .line 776
    :cond_a
    new-instance v4, Lcom/siimkinks/sqlitemagic/bo;

    iget v6, v1, Lcom/siimkinks/sqlitemagic/db$a;->c:I

    const/4 v7, 0x2

    iget-object v8, v1, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    invoke-direct {v4, v6, v7, v8}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    const-string v6, "predefined_payment_data"

    const-string v7, "UPDATE%s predefined_payment_data SET customer_id=?, predefined_payment_id=?, predefined_payment_name=?, sender_account_id=?, recipient_name=?, recipient_iban=?, amount=?, currency=?, description=?, reference_number=? WHERE local_id=?"

    .line 777
    invoke-virtual {v4, v6, v7, v2}, Lcom/siimkinks/sqlitemagic/bo;->b(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v6

    const-string v7, "predefined_payment_data"

    const-string v8, "INSERT%s INTO predefined_payment_data (customer_id, predefined_payment_id, predefined_payment_name, sender_account_id, recipient_name, recipient_iban, amount, currency, description, reference_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 778
    invoke-virtual {v4, v7, v8, v2}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v2

    const-string v7, "predefined_payment_data"

    .line 779
    iget-object v8, v1, Lcom/siimkinks/sqlitemagic/db$a;->d:Ljava/util/ArrayList;

    invoke-static {v7, v8}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v7

    if-eqz v7, :cond_b

    .line 782
    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    goto :goto_5

    :cond_b
    const-string v7, "local_id"

    .line 787
    :goto_5
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v8

    .line 790
    :try_start_2
    monitor-enter v6
    :try_end_2
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 791
    :try_start_3
    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 792
    :try_start_4
    iget-object v9, v1, Lcom/siimkinks/sqlitemagic/db$a;->a:Ljava/lang/Iterable;

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v10, 0x0

    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_13

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/swedbank/mobile/data/transfer/predefined/a;

    .line 793
    sget-boolean v15, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v15, :cond_c

    const-string v15, "PERSIST\n  table: predefined_payment_data\n  object: %s"

    new-array v11, v3, [Ljava/lang/Object;

    invoke-virtual {v14}, Lcom/swedbank/mobile/data/transfer/predefined/a;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v13

    invoke-static {v15, v11}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 795
    :cond_c
    invoke-static {v7, v14}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/predefined/a;)Z

    move-result v11

    if-nez v11, :cond_d

    .line 796
    invoke-static {v6, v14}, Lcom/swedbank/mobile/data/transfer/predefined/g;->b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/predefined/a;)V

    .line 797
    invoke-static {v6, v5, v7, v14}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/transfer/predefined/a;)V

    .line 798
    invoke-interface {v6}, Landroidx/k/a/f;->a()I

    move-result v11

    goto :goto_7

    :cond_d
    const/4 v11, 0x0

    :goto_7
    if-gtz v11, :cond_11

    .line 801
    sget-boolean v11, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v11, :cond_e

    const-string v11, "PERSIST update failed; trying insertion"

    new-array v12, v13, [Ljava/lang/Object;

    invoke-static {v11, v12}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 802
    :cond_e
    invoke-static {v2, v14}, Lcom/swedbank/mobile/data/transfer/predefined/g;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/predefined/a;)V

    .line 803
    invoke-interface {v2}, Landroidx/k/a/f;->b()J

    move-result-wide v11

    .line 804
    sget-boolean v15, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v15, :cond_f

    const-string v15, "INSERT id: %s"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v5, v13

    invoke-static {v15, v5}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_f
    const-wide/16 v15, -0x1

    cmp-long v5, v11, v15

    if-nez v5, :cond_12

    .line 806
    iget-boolean v5, v4, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v5, :cond_10

    goto :goto_8

    .line 807
    :cond_10
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to persist "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    const-wide/16 v15, -0x1

    :cond_12
    const/4 v10, 0x1

    :goto_8
    move-wide v11, v15

    const/16 v5, 0xb

    goto :goto_6

    .line 816
    :cond_13
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 817
    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 818
    :try_start_6
    invoke-interface {v8}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_6
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 825
    invoke-interface {v8}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v10, :cond_14

    .line 827
    sget-object v2, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bx;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    .line 829
    :cond_14
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v10

    :catchall_1
    move-exception v0

    .line 816
    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    :catchall_2
    move-exception v0

    .line 817
    monitor-exit v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v0
    :try_end_9
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    move-exception v0

    goto :goto_9

    :catch_1
    move-exception v0

    .line 822
    :try_start_a
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_15

    const-string v2, "Operation failed"

    new-array v3, v13, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 825
    :cond_15
    invoke-interface {v8}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 829
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v13

    .line 825
    :goto_9
    invoke-interface {v8}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 829
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    .line 830
    throw v0
.end method
