.class public final Lcom/siimkinks/sqlitemagic/dk;
.super Lcom/siimkinks/sqlitemagic/bn;
.source "SuggestedPaymentTypeColumn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/bn<",
        "Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;",
        "Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;",
        "Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;",
        "TT;TN;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/eb$a;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    .line 17
    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dk;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/dk<",
            "TT;TN;>;"
        }
    .end annotation

    .line 29
    new-instance v6, Lcom/siimkinks/sqlitemagic/dk;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/dk;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/dk;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/dk;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v4, p0, Lcom/siimkinks/sqlitemagic/dk;->f:Z

    move-object v0, v6

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/siimkinks/sqlitemagic/dk;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v6
.end method

.method a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            ")TV;"
        }
    .end annotation

    .line 35
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/bn;->a(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    .line 36
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/transfer/suggested/c;->a(I)Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    move-result-object p1

    return-object p1
.end method

.method a(Landroidx/k/a/f;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/k/a/f;",
            ")TV;"
        }
    .end annotation

    .line 42
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/bn;->a(Landroidx/k/a/f;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    .line 43
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/transfer/suggested/c;->a(I)Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    move-result-object p1

    return-object p1
.end method

.method a(Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;)Ljava/lang/String;
    .locals 0

    .line 23
    invoke-static {p1}, Lcom/swedbank/mobile/data/transfer/suggested/c;->a(Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method bridge synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/dk;->a(Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/dk;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dk;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bn;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/dk;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dk;

    move-result-object p1

    return-object p1
.end method
