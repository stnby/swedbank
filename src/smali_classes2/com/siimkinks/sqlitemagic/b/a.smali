.class public final Lcom/siimkinks/sqlitemagic/b/a;
.super Ljava/lang/Object;
.source "ContainerHelpers.java"


# static fields
.field public static final a:[B

.field public static final b:[Ljava/lang/Byte;

.field public static final c:[Ljava/lang/String;

.field static final d:[I

.field static final e:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    .line 8
    new-array v1, v0, [B

    sput-object v1, Lcom/siimkinks/sqlitemagic/b/a;->a:[B

    .line 9
    new-array v1, v0, [Ljava/lang/Byte;

    sput-object v1, Lcom/siimkinks/sqlitemagic/b/a;->b:[Ljava/lang/Byte;

    .line 10
    new-array v1, v0, [Ljava/lang/String;

    sput-object v1, Lcom/siimkinks/sqlitemagic/b/a;->c:[Ljava/lang/String;

    .line 11
    new-array v1, v0, [I

    sput-object v1, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    .line 12
    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    return-void
.end method

.method static a([III)I
    .locals 3

    add-int/lit8 p1, p1, -0x1

    const/4 v0, 0x0

    :goto_0
    if-gt v0, p1, :cond_2

    add-int v1, v0, p1

    ushr-int/lit8 v1, v1, 0x1

    .line 21
    aget v2, p0, v1

    if-ge v2, p2, :cond_0

    add-int/lit8 v1, v1, 0x1

    move v0, v1

    goto :goto_0

    :cond_0
    if-le v2, p2, :cond_1

    add-int/lit8 v1, v1, -0x1

    move p1, v1

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    not-int p0, v0

    return p0
.end method
