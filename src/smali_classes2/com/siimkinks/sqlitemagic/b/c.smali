.class public Lcom/siimkinks/sqlitemagic/b/c;
.super Ljava/lang/Object;
.source "SimpleArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static a:[Ljava/lang/Object;

.field static b:I

.field static c:[Ljava/lang/Object;

.field static d:I


# instance fields
.field e:[I

.field f:[Ljava/lang/Object;

.field g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 189
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    const/4 v0, 0x0

    .line 190
    iput v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    .line 198
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 199
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    goto :goto_0

    .line 201
    :cond_0
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->e(I)V

    :goto_0
    const/4 p1, 0x0

    .line 203
    iput p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    return-void
.end method

.method private static a([I[Ljava/lang/Object;I)V
    .locals 7

    .line 156
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/16 v4, 0xa

    const/4 v5, 0x1

    const/16 v6, 0x8

    if-ne v0, v6, :cond_2

    .line 157
    const-class v0, Lcom/siimkinks/sqlitemagic/b/c;

    monitor-enter v0

    .line 158
    :try_start_0
    sget v6, Lcom/siimkinks/sqlitemagic/b/c;->d:I

    if-ge v6, v4, :cond_1

    .line 159
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/c;->c:[Ljava/lang/Object;

    aput-object v4, p1, v3

    .line 160
    aput-object p0, p1, v5

    shl-int/lit8 p0, p2, 0x1

    sub-int/2addr p0, v5

    :goto_0
    if-lt p0, v2, :cond_0

    .line 162
    aput-object v1, p1, p0

    add-int/lit8 p0, p0, -0x1

    goto :goto_0

    .line 164
    :cond_0
    sput-object p1, Lcom/siimkinks/sqlitemagic/b/c;->c:[Ljava/lang/Object;

    .line 165
    sget p0, Lcom/siimkinks/sqlitemagic/b/c;->d:I

    add-int/2addr p0, v5

    sput p0, Lcom/siimkinks/sqlitemagic/b/c;->d:I

    .line 167
    :cond_1
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 168
    :cond_2
    array-length v0, p0

    const/4 v6, 0x4

    if-ne v0, v6, :cond_5

    .line 169
    const-class v0, Lcom/siimkinks/sqlitemagic/b/c;

    monitor-enter v0

    .line 170
    :try_start_1
    sget v6, Lcom/siimkinks/sqlitemagic/b/c;->b:I

    if-ge v6, v4, :cond_4

    .line 171
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/c;->a:[Ljava/lang/Object;

    aput-object v4, p1, v3

    .line 172
    aput-object p0, p1, v5

    shl-int/lit8 p0, p2, 0x1

    sub-int/2addr p0, v5

    :goto_1
    if-lt p0, v2, :cond_3

    .line 174
    aput-object v1, p1, p0

    add-int/lit8 p0, p0, -0x1

    goto :goto_1

    .line 176
    :cond_3
    sput-object p1, Lcom/siimkinks/sqlitemagic/b/c;->a:[Ljava/lang/Object;

    .line 177
    sget p0, Lcom/siimkinks/sqlitemagic/b/c;->b:I

    add-int/2addr p0, v5

    sput p0, Lcom/siimkinks/sqlitemagic/b/c;->b:I

    .line 179
    :cond_4
    monitor-exit v0

    goto :goto_2

    :catchall_1
    move-exception p0

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw p0

    :cond_5
    :goto_2
    return-void
.end method

.method private e(I)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x8

    if-ne p1, v3, :cond_1

    .line 126
    const-class v3, Lcom/siimkinks/sqlitemagic/b/c;

    monitor-enter v3

    .line 127
    :try_start_0
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/c;->c:[Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 128
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/c;->c:[Ljava/lang/Object;

    .line 129
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    .line 130
    aget-object v4, p1, v1

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    sput-object v4, Lcom/siimkinks/sqlitemagic/b/c;->c:[Ljava/lang/Object;

    .line 131
    aget-object v4, p1, v2

    check-cast v4, [I

    check-cast v4, [I

    iput-object v4, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 132
    aput-object v0, p1, v2

    aput-object v0, p1, v1

    .line 133
    sget p1, Lcom/siimkinks/sqlitemagic/b/c;->d:I

    sub-int/2addr p1, v2

    sput p1, Lcom/siimkinks/sqlitemagic/b/c;->d:I

    .line 134
    monitor-exit v3

    return-void

    .line 136
    :cond_0
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    const/4 v3, 0x4

    if-ne p1, v3, :cond_3

    .line 138
    const-class v3, Lcom/siimkinks/sqlitemagic/b/c;

    monitor-enter v3

    .line 139
    :try_start_1
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/c;->a:[Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 140
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/c;->a:[Ljava/lang/Object;

    .line 141
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    .line 142
    aget-object v4, p1, v1

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    sput-object v4, Lcom/siimkinks/sqlitemagic/b/c;->a:[Ljava/lang/Object;

    .line 143
    aget-object v4, p1, v2

    check-cast v4, [I

    check-cast v4, [I

    iput-object v4, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 144
    aput-object v0, p1, v2

    aput-object v0, p1, v1

    .line 145
    sget p1, Lcom/siimkinks/sqlitemagic/b/c;->b:I

    sub-int/2addr p1, v2

    sput p1, Lcom/siimkinks/sqlitemagic/b/c;->b:I

    .line 146
    monitor-exit v3

    return-void

    .line 148
    :cond_2
    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception p1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw p1

    .line 151
    :cond_3
    :goto_0
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    shl-int/2addr p1, v2

    .line 152
    new-array p1, p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method a()I
    .locals 6

    .line 85
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/siimkinks/sqlitemagic/b/a;->a([III)I

    move-result v1

    if-gez v1, :cond_1

    return v1

    .line 100
    :cond_1
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 v3, v1, 0x1

    aget-object v2, v2, v3

    if-nez v2, :cond_2

    return v1

    .line 106
    :cond_2
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 107
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    add-int/lit8 v4, v1, 0x1

    :goto_0
    if-ge v4, v0, :cond_4

    .line 108
    aget v5, v2, v4

    if-nez v5, :cond_4

    shl-int/lit8 v5, v4, 0x1

    .line 109
    aget-object v5, v3, v5

    if-nez v5, :cond_3

    return v4

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_6

    .line 113
    aget v0, v2, v1

    if-nez v0, :cond_6

    shl-int/lit8 v0, v1, 0x1

    .line 114
    aget-object v0, v3, v0

    if-nez v0, :cond_5

    return v1

    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_6
    not-int v0, v4

    return v0
.end method

.method a(Ljava/lang/Object;I)I
    .locals 6

    .line 45
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    invoke-static {v1, v0, p2}, Lcom/siimkinks/sqlitemagic/b/a;->a([III)I

    move-result v1

    if-gez v1, :cond_1

    return v1

    .line 60
    :cond_1
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 v3, v1, 0x1

    aget-object v2, v2, v3

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v1

    .line 66
    :cond_2
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 67
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    add-int/lit8 v4, v1, 0x1

    :goto_0
    if-ge v4, v0, :cond_4

    .line 68
    aget v5, v2, v4

    if-ne v5, p2, :cond_4

    shl-int/lit8 v5, v4, 0x1

    .line 69
    aget-object v5, v3, v5

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    return v4

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_6

    .line 73
    aget v0, v2, v1

    if-ne v0, p2, :cond_6

    shl-int/lit8 v0, v1, 0x1

    .line 74
    aget-object v0, v3, v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    return v1

    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_6
    not-int p1, v4

    return p1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 362
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/c;->a()I

    move-result v1

    const/4 v2, 0x0

    goto :goto_0

    .line 364
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    .line 365
    invoke-virtual {p0, p1, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;I)I

    move-result v2

    move v7, v2

    move v2, v1

    move v1, v7

    :goto_0
    if-ltz v1, :cond_1

    shl-int/lit8 p1, v1, 0x1

    add-int/lit8 p1, p1, 0x1

    .line 369
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 370
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    aput-object p2, v1, p1

    return-object v0

    :cond_1
    not-int v1, v1

    .line 375
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    array-length v4, v4

    if-lt v3, v4, :cond_5

    .line 376
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 v4, 0x4

    const/16 v5, 0x8

    if-lt v3, v5, :cond_2

    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    iget v4, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    shr-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v3

    goto :goto_1

    :cond_2
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-lt v3, v4, :cond_3

    const/16 v4, 0x8

    .line 379
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 380
    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    .line 381
    invoke-direct {p0, v4}, Lcom/siimkinks/sqlitemagic/b/c;->e(I)V

    .line 383
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    array-length v4, v4

    if-lez v4, :cond_4

    .line 384
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    array-length v6, v3

    invoke-static {v3, v0, v4, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 385
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    array-length v6, v5

    invoke-static {v5, v0, v4, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 388
    :cond_4
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    invoke-static {v3, v5, v0}, Lcom/siimkinks/sqlitemagic/b/c;->a([I[Ljava/lang/Object;I)V

    .line 391
    :cond_5
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-ge v1, v0, :cond_6

    .line 392
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    add-int/lit8 v4, v1, 0x1

    iget v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v5, v1

    invoke-static {v0, v1, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 393
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 v3, v1, 0x1

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 v4, v4, 0x1

    iget v6, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v6, v1

    shl-int/lit8 v6, v6, 0x1

    invoke-static {v0, v3, v5, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 396
    :cond_6
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    aput v2, v0, v1

    .line 397
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 v1, v1, 0x1

    aput-object p1, v0, v1

    .line 398
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    aput-object p2, p1, v1

    .line 399
    iget p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 p1, 0x0

    return-object p1
.end method

.method public a(I)V
    .locals 4

    .line 233
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    array-length v0, v0

    if-ge v0, p1, :cond_1

    .line 234
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 235
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    .line 236
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->e(I)V

    .line 237
    iget p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-lez p1, :cond_0

    .line 238
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 v3, 0x0

    invoke-static {v0, v3, p1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    shl-int/lit8 v2, v2, 0x1

    invoke-static {v1, v3, p1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    :cond_0
    iget p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    invoke-static {v0, v1, p1}, Lcom/siimkinks/sqlitemagic/b/c;->a([I[Ljava/lang/Object;I)V

    :cond_1
    return-void
.end method

.method public a(Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "+TK;+TV;>;)V"
        }
    .end annotation

    .line 409
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    .line 410
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(I)V

    .line 411
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 v2, 0x0

    if-nez v1, :cond_0

    if-lez v0, :cond_1

    .line 413
    iget-object v1, p1, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    invoke-static {v1, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 414
    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    invoke-static {p1, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 415
    iput v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    goto :goto_1

    :cond_0
    :goto_0
    if-ge v2, v0, :cond_1

    .line 419
    invoke-virtual {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->b(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 0

    .line 252
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->b(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b(Ljava/lang/Object;)I
    .locals 1

    if-nez p1, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/c;->a()I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;I)I

    move-result p1

    :goto_0
    return p1
.end method

.method public b(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TK;"
        }
    .end annotation

    .line 314
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 p1, p1, 0x1

    aget-object p1, v0, p1

    return-object p1
.end method

.method public b()V
    .locals 3

    .line 220
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    invoke-static {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->a([I[Ljava/lang/Object;I)V

    .line 222
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 223
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    const/4 v0, 0x0

    .line 224
    iput v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    :cond_0
    return-void
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .line 324
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 p1, p1, 0x1

    add-int/lit8 p1, p1, 0x1

    aget-object p1, v0, p1

    return-object p1
.end method

.method public c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .line 303
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->b(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    .line 304
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 p1, p1, 0x1

    add-int/lit8 p1, p1, 0x1

    aget-object p1, v0, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public c()Z
    .locals 1

    .line 345
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()I
    .locals 1

    .line 493
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    return v0
.end method

.method public d(I)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .line 447
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v2, v1, 0x1

    aget-object v0, v0, v2

    .line 448
    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-gt v2, v4, :cond_0

    .line 450
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    invoke-static {p1, v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->a([I[Ljava/lang/Object;I)V

    .line 451
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 452
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    .line 453
    iput v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    goto/16 :goto_0

    .line 455
    :cond_0
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    array-length v2, v2

    const/16 v5, 0x8

    if-le v2, v5, :cond_3

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    iget-object v6, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    if-ge v2, v6, :cond_3

    .line 459
    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-le v2, v5, :cond_1

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    iget v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    shr-int/2addr v5, v4

    add-int/2addr v5, v2

    .line 461
    :cond_1
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 462
    iget-object v6, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    .line 463
    invoke-direct {p0, v5}, Lcom/siimkinks/sqlitemagic/b/c;->e(I)V

    .line 465
    iget v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v5, v4

    iput v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-lez p1, :cond_2

    .line 467
    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    invoke-static {v2, v3, v5, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 468
    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    invoke-static {v6, v3, v5, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 470
    :cond_2
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-ge p1, v3, :cond_5

    add-int/lit8 v3, p1, 0x1

    .line 471
    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    iget v7, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v7, p1

    invoke-static {v2, v3, v5, p1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    shl-int/lit8 v2, v3, 0x1

    .line 472
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v5, p1

    shl-int/lit8 p1, v5, 0x1

    invoke-static {v6, v2, v3, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 476
    :cond_3
    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    .line 477
    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-ge p1, v2, :cond_4

    .line 478
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    add-int/lit8 v3, p1, 0x1

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    iget v6, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v6, p1

    invoke-static {v2, v3, v5, p1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 479
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    shl-int/2addr v3, v4

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget v6, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    sub-int/2addr v6, p1

    shl-int/lit8 p1, v6, 0x1

    invoke-static {v2, v3, v5, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 482
    :cond_4
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    shl-int/2addr v1, v4

    const/4 v2, 0x0

    aput-object v2, p1, v1

    .line 483
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    shl-int/2addr v1, v4

    add-int/2addr v1, v4

    aput-object v2, p1, v1

    :cond_5
    :goto_0
    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .line 432
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->b(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    .line 434
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->d(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 509
    :cond_0
    instance-of v1, p1, Lcom/siimkinks/sqlitemagic/b/c;

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 510
    check-cast p1, Lcom/siimkinks/sqlitemagic/b/c;

    .line 511
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/c;->d()I

    move-result v1

    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->d()I

    move-result v3

    if-eq v1, v3, :cond_1

    return v2

    .line 516
    :cond_1
    :try_start_0
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_5

    .line 518
    invoke-virtual {p0, v3}, Lcom/siimkinks/sqlitemagic/b/c;->b(I)Ljava/lang/Object;

    move-result-object v4

    .line 519
    invoke-virtual {p0, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(I)Ljava/lang/Object;

    move-result-object v5

    .line 520
    invoke-virtual {p1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v5, :cond_3

    if-nez v6, :cond_2

    .line 522
    invoke-virtual {p1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_2
    return v2

    .line 525
    :cond_3
    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v4, :cond_4

    return v2

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    return v0

    :catch_0
    return v2

    :catch_1
    return v2

    .line 535
    :cond_6
    instance-of v1, p1, Ljava/util/Map;

    if-eqz v1, :cond_c

    .line 536
    check-cast p1, Ljava/util/Map;

    .line 537
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/c;->d()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v3

    if-eq v1, v3, :cond_7

    return v2

    .line 542
    :cond_7
    :try_start_1
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_b

    .line 544
    invoke-virtual {p0, v3}, Lcom/siimkinks/sqlitemagic/b/c;->b(I)Ljava/lang/Object;

    move-result-object v4

    .line 545
    invoke-virtual {p0, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(I)Ljava/lang/Object;

    move-result-object v5

    .line 546
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v5, :cond_9

    if-nez v6, :cond_8

    .line 548
    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    :cond_8
    return v2

    .line 551
    :cond_9
    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v4, :cond_a

    return v2

    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_b
    return v0

    :catch_2
    return v2

    :catch_3
    return v2

    :cond_c
    return v2
.end method

.method public hashCode()I
    .locals 9

    .line 570
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/c;->e:[I

    .line 571
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->f:[Ljava/lang/Object;

    .line 573
    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 574
    aget-object v7, v1, v5

    .line 575
    aget v8, v0, v3

    if-nez v7, :cond_0

    const/4 v7, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v7

    :goto_1
    xor-int/2addr v7, v8

    add-int/2addr v6, v7

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v5, v5, 0x2

    goto :goto_0

    :cond_1
    return v6
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 589
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/c;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "{}"

    return-object v0

    .line 593
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    mul-int/lit8 v1, v1, 0x1c

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v1, 0x7b

    .line 594
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 595
    :goto_0
    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/c;->g:I

    if-ge v1, v2, :cond_4

    if-lez v1, :cond_1

    const-string v2, ", "

    .line 597
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    :cond_1
    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_2

    .line 601
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string v2, "(this Map)"

    .line 603
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/16 v2, 0x3d

    .line 605
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 606
    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->c(I)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_3

    .line 608
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    const-string v2, "(this Map)"

    .line 610
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    const/16 v1, 0x7d

    .line 613
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 614
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
