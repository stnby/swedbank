.class public Lcom/siimkinks/sqlitemagic/b/d;
.super Ljava/lang/Object;
.source "StringArraySet.java"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Set<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static c:[Ljava/lang/Object;

.field static d:I

.field static e:[Ljava/lang/Object;

.field static f:I


# instance fields
.field g:[I

.field h:[Ljava/lang/Object;

.field i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 183
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    const/4 v0, 0x0

    .line 184
    iput v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    .line 192
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 193
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    goto :goto_0

    .line 195
    :cond_0
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/b/d;->d(I)V

    :goto_0
    const/4 p1, 0x0

    .line 197
    iput p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    return-void
.end method

.method private a()I
    .locals 6

    .line 79
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/siimkinks/sqlitemagic/b/a;->a([III)I

    move-result v1

    if-gez v1, :cond_1

    return v1

    .line 94
    :cond_1
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    aget-object v2, v2, v1

    if-nez v2, :cond_2

    return v1

    .line 100
    :cond_2
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 101
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    add-int/lit8 v4, v1, 0x1

    :goto_0
    if-ge v4, v0, :cond_4

    .line 102
    aget v5, v2, v4

    if-nez v5, :cond_4

    .line 103
    aget-object v5, v3, v4

    if-nez v5, :cond_3

    return v4

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_6

    .line 107
    aget v0, v2, v1

    if-nez v0, :cond_6

    .line 108
    aget-object v0, v3, v1

    if-nez v0, :cond_5

    return v1

    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_6
    not-int v0, v4

    return v0
.end method

.method private a(Ljava/lang/Object;I)I
    .locals 6

    .line 39
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 46
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    invoke-static {v1, v0, p2}, Lcom/siimkinks/sqlitemagic/b/a;->a([III)I

    move-result v1

    if-gez v1, :cond_1

    return v1

    .line 54
    :cond_1
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v1

    .line 60
    :cond_2
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 61
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    add-int/lit8 v4, v1, 0x1

    :goto_0
    if-ge v4, v0, :cond_4

    .line 62
    aget v5, v2, v4

    if-ne v5, p2, :cond_4

    .line 63
    aget-object v5, v3, v4

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    return v4

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_6

    .line 67
    aget v0, v2, v1

    if-ne v0, p2, :cond_6

    .line 68
    aget-object v0, v3, v1

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    return v1

    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_6
    not-int p1, v4

    return p1
.end method

.method private static a([I[Ljava/lang/Object;I)V
    .locals 7

    .line 150
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/16 v4, 0xa

    const/4 v5, 0x1

    const/16 v6, 0x8

    if-ne v0, v6, :cond_2

    .line 151
    const-class v0, Lcom/siimkinks/sqlitemagic/b/d;

    monitor-enter v0

    .line 152
    :try_start_0
    sget v6, Lcom/siimkinks/sqlitemagic/b/d;->f:I

    if-ge v6, v4, :cond_1

    .line 153
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/d;->e:[Ljava/lang/Object;

    aput-object v4, p1, v3

    .line 154
    aput-object p0, p1, v5

    sub-int/2addr p2, v5

    :goto_0
    if-lt p2, v2, :cond_0

    .line 156
    aput-object v1, p1, p2

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 158
    :cond_0
    sput-object p1, Lcom/siimkinks/sqlitemagic/b/d;->e:[Ljava/lang/Object;

    .line 159
    sget p0, Lcom/siimkinks/sqlitemagic/b/d;->f:I

    add-int/2addr p0, v5

    sput p0, Lcom/siimkinks/sqlitemagic/b/d;->f:I

    .line 161
    :cond_1
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 162
    :cond_2
    array-length v0, p0

    const/4 v6, 0x4

    if-ne v0, v6, :cond_5

    .line 163
    const-class v0, Lcom/siimkinks/sqlitemagic/b/d;

    monitor-enter v0

    .line 164
    :try_start_1
    sget v6, Lcom/siimkinks/sqlitemagic/b/d;->d:I

    if-ge v6, v4, :cond_4

    .line 165
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/d;->c:[Ljava/lang/Object;

    aput-object v4, p1, v3

    .line 166
    aput-object p0, p1, v5

    sub-int/2addr p2, v5

    :goto_1
    if-lt p2, v2, :cond_3

    .line 168
    aput-object v1, p1, p2

    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    .line 170
    :cond_3
    sput-object p1, Lcom/siimkinks/sqlitemagic/b/d;->c:[Ljava/lang/Object;

    .line 171
    sget p0, Lcom/siimkinks/sqlitemagic/b/d;->d:I

    add-int/2addr p0, v5

    sput p0, Lcom/siimkinks/sqlitemagic/b/d;->d:I

    .line 173
    :cond_4
    monitor-exit v0

    goto :goto_2

    :catchall_1
    move-exception p0

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw p0

    :cond_5
    :goto_2
    return-void
.end method

.method private d(I)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x8

    if-ne p1, v3, :cond_1

    .line 120
    const-class v3, Lcom/siimkinks/sqlitemagic/b/d;

    monitor-enter v3

    .line 121
    :try_start_0
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/d;->e:[Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 122
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/d;->e:[Ljava/lang/Object;

    .line 123
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    .line 124
    aget-object v4, p1, v1

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    sput-object v4, Lcom/siimkinks/sqlitemagic/b/d;->e:[Ljava/lang/Object;

    .line 125
    aget-object v4, p1, v2

    check-cast v4, [I

    check-cast v4, [I

    iput-object v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 126
    aput-object v0, p1, v2

    aput-object v0, p1, v1

    .line 127
    sget p1, Lcom/siimkinks/sqlitemagic/b/d;->f:I

    sub-int/2addr p1, v2

    sput p1, Lcom/siimkinks/sqlitemagic/b/d;->f:I

    .line 128
    monitor-exit v3

    return-void

    .line 130
    :cond_0
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    const/4 v3, 0x4

    if-ne p1, v3, :cond_3

    .line 132
    const-class v3, Lcom/siimkinks/sqlitemagic/b/d;

    monitor-enter v3

    .line 133
    :try_start_1
    sget-object v4, Lcom/siimkinks/sqlitemagic/b/d;->c:[Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 134
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/d;->c:[Ljava/lang/Object;

    .line 135
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    .line 136
    aget-object v4, p1, v1

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    sput-object v4, Lcom/siimkinks/sqlitemagic/b/d;->c:[Ljava/lang/Object;

    .line 137
    aget-object v4, p1, v2

    check-cast v4, [I

    check-cast v4, [I

    iput-object v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 138
    aput-object v0, p1, v2

    aput-object v0, p1, v1

    .line 139
    sget p1, Lcom/siimkinks/sqlitemagic/b/d;->d:I

    sub-int/2addr p1, v2

    sput p1, Lcom/siimkinks/sqlitemagic/b/d;->d:I

    .line 140
    monitor-exit v3

    return-void

    .line 142
    :cond_2
    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception p1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw p1

    .line 145
    :cond_3
    :goto_0
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 146
    new-array p1, p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1

    if-nez p1, :cond_0

    .line 265
    invoke-direct {p0}, Lcom/siimkinks/sqlitemagic/b/d;->a()I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/Object;I)I

    move-result p1

    :goto_0
    return p1
.end method

.method public a(I)V
    .locals 4

    .line 234
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    array-length v0, v0

    if-ge v0, p1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 236
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    .line 237
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/b/d;->d(I)V

    .line 238
    iget p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-lez p1, :cond_0

    .line 240
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    invoke-static {v1, v3, v2, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 243
    :cond_0
    invoke-static {v0, v1, p1}, Lcom/siimkinks/sqlitemagic/b/d;->a([I[Ljava/lang/Object;I)V

    :cond_1
    return-void
.end method

.method public a(Lcom/siimkinks/sqlitemagic/b/d;)V
    .locals 4

    .line 338
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    .line 339
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/d;->a(I)V

    .line 340
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v2, 0x0

    if-nez v1, :cond_0

    if-lez v0, :cond_1

    .line 342
    iget-object v1, p1, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    invoke-static {v1, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 343
    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    invoke-static {p1, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 344
    iput v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    goto :goto_1

    :cond_0
    :goto_0
    if-ge v2, v0, :cond_1

    .line 348
    invoke-virtual {p1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/String;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public a([Ljava/lang/String;)V
    .locals 3

    .line 354
    array-length v0, p1

    .line 355
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/d;->a(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 357
    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/String;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 9

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 300
    invoke-direct {p0}, Lcom/siimkinks/sqlitemagic/b/d;->a()I

    move-result v1

    const/4 v2, 0x0

    goto :goto_0

    .line 302
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 303
    invoke-direct {p0, p1, v1}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/Object;I)I

    move-result v2

    move v8, v2

    move v2, v1

    move v1, v8

    :goto_0
    if-ltz v1, :cond_1

    return v0

    :cond_1
    not-int v1, v1

    .line 310
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    array-length v4, v4

    const/4 v5, 0x1

    if-lt v3, v4, :cond_5

    .line 311
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v4, 0x4

    const/16 v6, 0x8

    if-lt v3, v6, :cond_2

    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    iget v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    shr-int/2addr v4, v5

    add-int/2addr v4, v3

    goto :goto_1

    :cond_2
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-lt v3, v4, :cond_3

    const/16 v4, 0x8

    .line 314
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 315
    iget-object v6, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    .line 316
    invoke-direct {p0, v4}, Lcom/siimkinks/sqlitemagic/b/d;->d(I)V

    .line 318
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    array-length v4, v4

    if-lez v4, :cond_4

    .line 319
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    array-length v7, v3

    invoke-static {v3, v0, v4, v0, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 320
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    array-length v7, v6

    invoke-static {v6, v0, v4, v0, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 323
    :cond_4
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    invoke-static {v3, v6, v0}, Lcom/siimkinks/sqlitemagic/b/d;->a([I[Ljava/lang/Object;I)V

    .line 326
    :cond_5
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-ge v1, v0, :cond_6

    .line 327
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    add-int/lit8 v4, v1, 0x1

    iget v6, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v6, v1

    invoke-static {v0, v1, v3, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 328
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v6, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v6, v1

    invoke-static {v0, v1, v3, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 331
    :cond_6
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    aput v2, v0, v1

    .line 332
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    aput-object p1, v0, v1

    .line 333
    iget p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    add-int/2addr p1, v5

    iput p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    return v5
.end method

.method public synthetic add(Ljava/lang/Object;)Z
    .locals 0

    .line 11
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 577
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/b/d;->a(I)V

    .line 579
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 580
    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    aget-object p1, v0, p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public c(I)Ljava/lang/String;
    .locals 6

    .line 384
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 385
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-gt v1, v3, :cond_0

    .line 387
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    invoke-static {p1, v1, v3}, Lcom/siimkinks/sqlitemagic/b/d;->a([I[Ljava/lang/Object;I)V

    .line 388
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 389
    sget-object p1, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    .line 390
    iput v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    goto :goto_0

    .line 392
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    array-length v1, v1

    const/16 v4, 0x8

    if-le v1, v4, :cond_3

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x3

    if-ge v1, v5, :cond_3

    .line 396
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-le v1, v4, :cond_1

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    iget v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    shr-int/2addr v4, v3

    add-int/2addr v4, v1

    .line 398
    :cond_1
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 399
    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    .line 400
    invoke-direct {p0, v4}, Lcom/siimkinks/sqlitemagic/b/d;->d(I)V

    .line 402
    iget v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v4, v3

    iput v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-lez p1, :cond_2

    .line 404
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    invoke-static {v1, v2, v3, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 405
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    invoke-static {v5, v2, v3, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 407
    :cond_2
    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-ge p1, v2, :cond_5

    add-int/lit8 v2, p1, 0x1

    .line 408
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    iget v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v4, p1

    invoke-static {v1, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 409
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v3, p1

    invoke-static {v5, v2, v1, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 412
    :cond_3
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v1, v3

    iput v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    .line 413
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-ge p1, v1, :cond_4

    .line 414
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    iget v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v4, p1

    invoke-static {v1, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 415
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v4, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    sub-int/2addr v4, p1

    invoke-static {v1, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 417
    :cond_4
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v2, 0x0

    aput-object v2, p1, v1

    .line 420
    :cond_5
    :goto_0
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public clear()V
    .locals 3

    .line 221
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    invoke-static {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a([I[Ljava/lang/Object;I)V

    .line 223
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->d:[I

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 224
    sget-object v0, Lcom/siimkinks/sqlitemagic/b/a;->e:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    const/4 v0, 0x0

    .line 225
    iput v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    :cond_0
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 0

    .line 255
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)Z"
        }
    .end annotation

    .line 561
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 562
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/b/d;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 467
    :cond_0
    instance-of v1, p1, Ljava/util/Set;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 468
    check-cast p1, Ljava/util/Set;

    .line 469
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/d;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v1, v3, :cond_1

    return v2

    :cond_1
    const/4 v1, 0x0

    .line 474
    :goto_0
    :try_start_0
    iget v3, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-ge v1, v3, :cond_3

    .line 475
    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/d;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 476
    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_2

    return v2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return v0

    :catch_0
    return v2

    :catch_1
    return v2

    :cond_4
    return v2
.end method

.method public hashCode()I
    .locals 5

    .line 495
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->g:[I

    .line 497
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 498
    aget v4, v0, v2

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v3
.end method

.method public isEmpty()Z
    .locals 1

    .line 283
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 549
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 0

    .line 369
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    .line 371
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/b/d;->c(I)Ljava/lang/String;

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)Z"
        }
    .end annotation

    .line 594
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 595
    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/d;->remove(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)Z"
        }
    .end annotation

    .line 610
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    .line 611
    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    :goto_0
    if-ltz v1, :cond_1

    .line 612
    aget-object v4, v0, v1

    invoke-interface {p1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 613
    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/d;->c(I)Ljava/lang/String;

    const/4 v3, 0x1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method public size()I
    .locals 1

    .line 428
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 4

    .line 433
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    new-array v0, v0, [Ljava/lang/Object;

    .line 434
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v2, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .line 440
    array-length v0, p1

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-ge v0, v1, :cond_0

    .line 442
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object p1

    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Object;

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->h:[Ljava/lang/Object;

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 446
    array-length v0, p1

    iget v1, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    if-le v0, v1, :cond_1

    .line 447
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    const/4 v1, 0x0

    aput-object v1, p1, v0

    :cond_1
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 513
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/d;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "{}"

    return-object v0

    .line 517
    :cond_0
    iget v0, p0, Lcom/siimkinks/sqlitemagic/b/d;->i:I

    .line 518
    new-instance v1, Ljava/lang/StringBuilder;

    mul-int/lit8 v2, v0, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v2, 0x7b

    .line 519
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    if-lez v2, :cond_1

    const-string v3, ", "

    .line 522
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    :cond_1
    invoke-virtual {p0, v2}, Lcom/siimkinks/sqlitemagic/b/d;->b(I)Ljava/lang/String;

    move-result-object v3

    if-eq v3, p0, :cond_2

    .line 526
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string v3, "(this Set)"

    .line 528
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const/16 v0, 0x7d

    .line 531
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 532
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
