.class public abstract Lcom/siimkinks/sqlitemagic/cd$a;
.super Lcom/siimkinks/sqlitemagic/cd;
.source "SelectSqlNode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/cd<",
        "TS;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/cd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd<",
            "TS;>;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/cd;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/siimkinks/sqlitemagic/ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/ae<",
            "TT;TS;>;"
        }
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cc;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/bn<",
            "TT;TT;TT;*TN;>;"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    invoke-static {v0, p1}, Lcom/siimkinks/sqlitemagic/ce;->a(Lcom/siimkinks/sqlitemagic/cc;Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/ce;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/siimkinks/sqlitemagic/cd$a;)Lcom/siimkinks/sqlitemagic/cb$b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd$a<",
            "*TS;*>;)",
            "Lcom/siimkinks/sqlitemagic/cb$b<",
            "TT;TS;TN;>;"
        }
    .end annotation

    .line 57
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$b;

    const-string v1, "UNION"

    invoke-direct {v0, p0, v1, p1}, Lcom/siimkinks/sqlitemagic/cb$b;-><init>(Lcom/siimkinks/sqlitemagic/cd$a;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/cd$a;)V

    return-object v0
.end method

.method public final b()Lcom/siimkinks/sqlitemagic/ad;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/ad<",
            "TT;TS;>;"
        }
    .end annotation

    .line 211
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cc;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ae;->c()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/siimkinks/sqlitemagic/ab;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cc;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ae;->d()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 265
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cc;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ae;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/siimkinks/sqlitemagic/bj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/bj<",
            "TT;>;"
        }
    .end annotation

    .line 294
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cc;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ae;->b()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    return-object v0
.end method
