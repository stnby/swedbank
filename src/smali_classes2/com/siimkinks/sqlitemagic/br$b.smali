.class final Lcom/siimkinks/sqlitemagic/br$b;
.super Lio/reactivex/f/a;
.source "OperatorRunSingleItemQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/br;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/f/a<",
        "Lcom/siimkinks/sqlitemagic/ca<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/u;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-TT;>;TT;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Lio/reactivex/f/a;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/br$b;->a:Lio/reactivex/u;

    .line 55
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/br$b;->c:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Lcom/siimkinks/sqlitemagic/ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "TT;>;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 67
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/ca;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    .line 68
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/ca;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object p1

    .line 69
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/br$b;->b()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/br$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :cond_0
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/br$b;->c:Ljava/lang/Object;

    if-eqz p1, :cond_1

    .line 73
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/br$b;->a:Lio/reactivex/u;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/br$b;->c:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 77
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 78
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/br$b;->onError(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected c()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/br$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    return-void
.end method

.method public onComplete()V
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/br$b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/br$b;->a:Lio/reactivex/u;

    invoke-interface {v0}, Lio/reactivex/u;->onComplete()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/br$b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/br$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 47
    check-cast p1, Lcom/siimkinks/sqlitemagic/ca;

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/br$b;->a(Lcom/siimkinks/sqlitemagic/ca;)V

    return-void
.end method
