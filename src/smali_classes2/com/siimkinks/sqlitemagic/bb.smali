.class public Lcom/siimkinks/sqlitemagic/bb;
.super Ljava/lang/Object;
.source "Expr.java"


# instance fields
.field private final a:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;"
        }
    .end annotation
.end field

.field final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bb;->a:Lcom/siimkinks/sqlitemagic/x;

    .line 27
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/bb;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;
    .locals 2

    .line 121
    new-instance v0, Lcom/siimkinks/sqlitemagic/m;

    const-string v1, " AND "

    invoke-direct {v0, v1, p0, p1}, Lcom/siimkinks/sqlitemagic/m;-><init>(Ljava/lang/String;Lcom/siimkinks/sqlitemagic/bb;Lcom/siimkinks/sqlitemagic/bb;)V

    return-object v0
.end method

.method public final a()Lcom/siimkinks/sqlitemagic/cb$f;
    .locals 3

    .line 98
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$f;

    const-string v1, " DESC"

    const/4 v2, 0x0

    invoke-direct {v0, v2, p0, v1}, Lcom/siimkinks/sqlitemagic/cb$f;-><init>(Lcom/siimkinks/sqlitemagic/x;Lcom/siimkinks/sqlitemagic/bb;Ljava/lang/String;)V

    return-object v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bb;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;)V

    .line 38
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bb;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1, p2}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    .line 43
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/bb;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final b(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;
    .locals 2

    .line 145
    new-instance v0, Lcom/siimkinks/sqlitemagic/m;

    const-string v1, " OR "

    invoke-direct {v0, v1, p0, p1}, Lcom/siimkinks/sqlitemagic/m;-><init>(Ljava/lang/String;Lcom/siimkinks/sqlitemagic/bb;Lcom/siimkinks/sqlitemagic/bb;)V

    return-object v0
.end method

.method b(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
