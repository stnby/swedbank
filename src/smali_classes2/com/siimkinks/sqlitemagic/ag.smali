.class final Lcom/siimkinks/sqlitemagic/ag;
.super Lcom/siimkinks/sqlitemagic/ca$a;
.source "CompiledSelectImpl.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/ae;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/ag$b;,
        Lcom/siimkinks/sqlitemagic/ag$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/ca$a<",
        "Ljava/util/List<",
        "TT;>;TT;>;",
        "Lcom/siimkinks/sqlitemagic/ae<",
        "TT;TS;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:[Ljava/lang/String;

.field final c:Lcom/siimkinks/sqlitemagic/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;"
        }
    .end annotation
.end field

.field final d:[Ljava/lang/String;

.field final e:Lcom/siimkinks/sqlitemagic/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/siimkinks/sqlitemagic/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final g:Z


# direct methods
.method constructor <init>(Ljava/lang/String;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/dl;Lcom/siimkinks/sqlitemagic/as;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;",
            "Lcom/siimkinks/sqlitemagic/as;",
            "[",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 49
    invoke-virtual {p3, p6, p7, p8}, Lcom/siimkinks/sqlitemagic/dl;->a(Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/ca$b;

    move-result-object v0

    invoke-direct {p0, p4, v0}, Lcom/siimkinks/sqlitemagic/ca$a;-><init>(Lcom/siimkinks/sqlitemagic/as;Lcom/siimkinks/sqlitemagic/ca$b;)V

    .line 50
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag;->b:[Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/ag;->c:Lcom/siimkinks/sqlitemagic/dl;

    .line 53
    iput-object p5, p0, Lcom/siimkinks/sqlitemagic/ag;->d:[Ljava/lang/String;

    .line 54
    iput-object p6, p0, Lcom/siimkinks/sqlitemagic/ag;->e:Lcom/siimkinks/sqlitemagic/b/c;

    .line 55
    iput-object p7, p0, Lcom/siimkinks/sqlitemagic/ag;->f:Lcom/siimkinks/sqlitemagic/b/c;

    .line 56
    iput-boolean p8, p0, Lcom/siimkinks/sqlitemagic/ag;->g:Z

    return-void
.end method

.method static a([Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ca$a;)Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Lcom/siimkinks/sqlitemagic/ca$a<",
            "TT;TE;>;T:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/String;",
            "TQ;)",
            "Lio/reactivex/o<",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "TT;>;>;"
        }
    .end annotation

    .line 351
    array-length v0, p0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 352
    new-instance v0, Lcom/siimkinks/sqlitemagic/ag$1;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/ag$1;-><init>([Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 364
    aget-object p0, p0, v0

    .line 365
    new-instance v0, Lcom/siimkinks/sqlitemagic/ag$2;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/ag$2;-><init>(Ljava/lang/String;)V

    .line 372
    :goto_0
    iget-object p0, p1, Lcom/siimkinks/sqlitemagic/ca$a;->h:Lcom/siimkinks/sqlitemagic/as;

    .line 373
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/as;->f:Lio/reactivex/k/c;

    .line 374
    invoke-virtual {v1, v0}, Lio/reactivex/k/c;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 375
    invoke-virtual {v0, p1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 376
    invoke-virtual {v0, p1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as;->b:Lio/reactivex/v;

    .line 377
    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object p1

    iget-object p0, p0, Lcom/siimkinks/sqlitemagic/as;->g:Lio/reactivex/c/g;

    .line 378
    invoke-virtual {p1, p0}, Lio/reactivex/o;->b(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method a(Z)Landroid/database/Cursor;
    .locals 5

    .line 62
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/ca$a;->a(Z)Landroid/database/Cursor;

    .line 63
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/ag;->h:Lcom/siimkinks/sqlitemagic/as;

    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/as;->c()Landroidx/k/a/b;

    move-result-object p1

    .line 64
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 65
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/ag;->b:[Ljava/lang/String;

    invoke-interface {p1, v2, v3}, Landroidx/k/a/b;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;

    move-result-object p1

    .line 66
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    .line 67
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 68
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ag;->d:[Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/ag;->b:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/siimkinks/sqlitemagic/bk;->a(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 70
    :cond_0
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/bf;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 100
    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/ag;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/ag;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 76
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 78
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0

    .line 80
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag;->i:Lcom/siimkinks/sqlitemagic/ca$b;

    .line 81
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 82
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    invoke-interface {v1, p1}, Lcom/siimkinks/sqlitemagic/ca$b;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 88
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v2

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public b()Lcom/siimkinks/sqlitemagic/bj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/bj<",
            "TT;>;"
        }
    .end annotation

    .line 106
    new-instance v0, Lcom/siimkinks/sqlitemagic/bj;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag;->d:[Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/siimkinks/sqlitemagic/ag;->a([Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ca$a;)Lio/reactivex/o;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/bj;-><init>(Lio/reactivex/o;)V

    return-object v0
.end method

.method synthetic b(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/ag;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public c()Lcom/siimkinks/sqlitemagic/ad;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/ad<",
            "TT;TS;>;"
        }
    .end annotation

    .line 112
    new-instance v0, Lcom/siimkinks/sqlitemagic/ag$b;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag;->h:Lcom/siimkinks/sqlitemagic/as;

    invoke-direct {v0, p0, v1}, Lcom/siimkinks/sqlitemagic/ag$b;-><init>(Lcom/siimkinks/sqlitemagic/ag;Lcom/siimkinks/sqlitemagic/as;)V

    return-object v0
.end method

.method public d()Lcom/siimkinks/sqlitemagic/ab;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/ab<",
            "TS;>;"
        }
    .end annotation

    .line 118
    new-instance v0, Lcom/siimkinks/sqlitemagic/ag$a;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ag;->b:[Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/ag;->h:Lcom/siimkinks/sqlitemagic/as;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/ag;->d:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/siimkinks/sqlitemagic/ag$a;-><init>(Ljava/lang/String;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/as;[Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[deepQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/siimkinks/sqlitemagic/ag;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ";sql="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
