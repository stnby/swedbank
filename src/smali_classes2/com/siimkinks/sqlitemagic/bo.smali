.class public final Lcom/siimkinks/sqlitemagic/bo;
.super Ljava/lang/Object;
.source "OperationHelper.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field final a:Z

.field private final b:I

.field private final c:Z

.field private final d:Z

.field private e:Lcom/siimkinks/sqlitemagic/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Landroidx/k/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/siimkinks/sqlitemagic/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Landroidx/k/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(IILjava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/siimkinks/sqlitemagic/bo;->b:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 48
    :goto_0
    iput-boolean v3, p0, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    .line 49
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/bo;->g:Ljava/util/ArrayList;

    if-eqz p1, :cond_1

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 50
    :goto_1
    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/bo;->c:Z

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    .line 51
    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput-boolean v0, p0, Lcom/siimkinks/sqlitemagic/bo;->d:Z

    .line 52
    iget-boolean p1, p0, Lcom/siimkinks/sqlitemagic/bo;->c:Z

    if-eqz p1, :cond_3

    packed-switch p2, :pswitch_data_0

    goto :goto_2

    .line 61
    :pswitch_0
    new-instance p1, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bo;->e:Lcom/siimkinks/sqlitemagic/b/c;

    .line 62
    new-instance p1, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bo;->f:Lcom/siimkinks/sqlitemagic/b/c;

    goto :goto_2

    .line 58
    :pswitch_1
    new-instance p1, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bo;->f:Lcom/siimkinks/sqlitemagic/b/c;

    goto :goto_2

    .line 55
    :pswitch_2
    new-instance p1, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bo;->e:Lcom/siimkinks/sqlitemagic/b/c;

    .line 68
    :cond_3
    :goto_2
    iget-boolean p1, p0, Lcom/siimkinks/sqlitemagic/bo;->c:Z

    if-nez p1, :cond_4

    iget-boolean p1, p0, Lcom/siimkinks/sqlitemagic/bo;->d:Z

    if-eqz p1, :cond_4

    .line 69
    new-instance p1, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bo;->f:Lcom/siimkinks/sqlitemagic/b/c;

    :cond_4
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Landroidx/k/a/f;",
            ">;)V"
        }
    .end annotation

    .line 121
    :try_start_0
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/b/c;->d()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 123
    invoke-virtual {p0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->c(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/k/a/f;

    invoke-interface {v2}, Landroidx/k/a/f;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/bo;->c:Z

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bo;->e:Lcom/siimkinks/sqlitemagic/b/c;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/k/a/f;

    if-nez v0, :cond_0

    .line 81
    iget v0, p0, Lcom/siimkinks/sqlitemagic/bo;->b:I

    invoke-virtual {p3, p2, v0}, Lcom/siimkinks/sqlitemagic/ba;->a(Ljava/lang/String;I)Landroidx/k/a/f;

    move-result-object v0

    .line 82
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/bo;->e:Lcom/siimkinks/sqlitemagic/b/c;

    invoke-virtual {p2, p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    .line 86
    :cond_1
    invoke-virtual {p3, p2}, Lcom/siimkinks/sqlitemagic/ba;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p1

    return-object p1
.end method

.method b(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;
    .locals 1

    .line 94
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/bo;->c:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/bo;->d:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {p3, p2}, Lcom/siimkinks/sqlitemagic/ba;->b(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p1

    return-object p1

    .line 95
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bo;->f:Lcom/siimkinks/sqlitemagic/b/c;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/k/a/f;

    if-nez v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bo;->g:Ljava/util/ArrayList;

    invoke-static {p2, p1, v0}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object p2

    iget v0, p0, Lcom/siimkinks/sqlitemagic/bo;->b:I

    invoke-virtual {p3, p2, v0}, Lcom/siimkinks/sqlitemagic/ba;->a(Ljava/lang/String;I)Landroidx/k/a/f;

    move-result-object v0

    .line 98
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/bo;->f:Lcom/siimkinks/sqlitemagic/b/c;

    invoke-virtual {p2, p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method public close()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bo;->e:Lcom/siimkinks/sqlitemagic/b/c;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 109
    iput-object v1, p0, Lcom/siimkinks/sqlitemagic/bo;->e:Lcom/siimkinks/sqlitemagic/b/c;

    .line 110
    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/bo;->a(Lcom/siimkinks/sqlitemagic/b/c;)V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bo;->f:Lcom/siimkinks/sqlitemagic/b/c;

    if-eqz v0, :cond_1

    .line 114
    iput-object v1, p0, Lcom/siimkinks/sqlitemagic/bo;->f:Lcom/siimkinks/sqlitemagic/b/c;

    .line 115
    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/bo;->a(Lcom/siimkinks/sqlitemagic/b/c;)V

    :cond_1
    return-void
.end method
