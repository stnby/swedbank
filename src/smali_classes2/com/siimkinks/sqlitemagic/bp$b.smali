.class final Lcom/siimkinks/sqlitemagic/bp$b;
.super Lio/reactivex/f/a;
.source "OperatorCountZeroOrNot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/bp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/f/a<",
        "Lcom/siimkinks/sqlitemagic/ca<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z


# direct methods
.method constructor <init>(Lio/reactivex/u;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Ljava/lang/Boolean;",
            ">;Z)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Lio/reactivex/f/a;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bp$b;->a:Lio/reactivex/u;

    .line 44
    iput-boolean p2, p0, Lcom/siimkinks/sqlitemagic/bp$b;->c:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/siimkinks/sqlitemagic/ca;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 56
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/ca;->a(Z)Landroid/database/Cursor;

    move-result-object v1

    .line 57
    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/ca;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    .line 58
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bp$b;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 59
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/bp$b;->a:Lio/reactivex/u;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long p1, v2, v4

    if-lez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-boolean p1, p0, Lcom/siimkinks/sqlitemagic/bp$b;->c:Z

    xor-int/2addr p1, v0

    if-eqz p1, :cond_1

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1

    :cond_1
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_1
    invoke-interface {v1, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 62
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 63
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/bp$b;->onError(Ljava/lang/Throwable;)V

    :cond_2
    :goto_2
    return-void
.end method

.method protected c()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bp$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    return-void
.end method

.method public onComplete()V
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bp$b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bp$b;->a:Lio/reactivex/u;

    invoke-interface {v0}, Lio/reactivex/u;->onComplete()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bp$b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bp$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 37
    check-cast p1, Lcom/siimkinks/sqlitemagic/ca;

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/bp$b;->a(Lcom/siimkinks/sqlitemagic/ca;)V

    return-void
.end method
