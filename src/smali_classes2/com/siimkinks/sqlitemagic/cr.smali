.class public final Lcom/siimkinks/sqlitemagic/cr;
.super Ljava/lang/Object;
.source "SqliteMagic_ActiveSession_Handler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/cr$a;,
        Lcom/siimkinks/sqlitemagic/cr$b;
    }
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/data/authentication/b/b;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/bo;)J
    .locals 5

    .line 52
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const-string v0, "INSERT\n  table: active_session\n  object: %s"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/authentication/b/b;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v0, "active_session"

    const-string v3, "INSERT%s INTO active_session (access_token, refresh_token, id_token, expires_in, created) VALUES (?, ?, ?, ?, ?)"

    .line 54
    invoke-virtual {p2, v0, v3, p1}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object p1

    .line 55
    monitor-enter p1

    .line 56
    :try_start_0
    invoke-static {p1, p0}, Lcom/swedbank/mobile/data/authentication/b/s;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/authentication/b/b;)V

    .line 57
    invoke-interface {p1}, Landroidx/k/a/f;->b()J

    move-result-wide v3

    .line 58
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    sget-boolean p1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p1, :cond_1

    const-string p1, "INSERT id: %s"

    new-array p2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p2, v1

    invoke-static {p1, p2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    const-wide/16 p1, -0x1

    cmp-long p1, v3, p1

    if-eqz p1, :cond_2

    return-wide v3

    .line 61
    :cond_2
    new-instance p1, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Failed to insert "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p0

    .line 58
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method
