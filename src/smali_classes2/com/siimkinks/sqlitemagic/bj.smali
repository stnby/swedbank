.class public Lcom/siimkinks/sqlitemagic/bj;
.super Lio/reactivex/o;
.source "ListQueryObservable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/o<",
        "Lcom/siimkinks/sqlitemagic/ca<",
        "Ljava/util/List<",
        "TR;>;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "Ljava/util/List<",
            "TR;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "Ljava/util/List<",
            "TR;>;>;>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bj;->a:Lio/reactivex/o;

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "TR;>;>;"
        }
    .end annotation

    .line 38
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bq;->a()Lcom/siimkinks/sqlitemagic/bq;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/bj;->a(Lio/reactivex/r;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lio/reactivex/u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "Ljava/util/List<",
            "TR;>;>;>;)V"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bj;->a:Lio/reactivex/o;

    invoke-virtual {v0, p1}, Lio/reactivex/o;->d(Lio/reactivex/u;)V

    return-void
.end method

.method public final b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "TR;>;>;"
        }
    .end annotation

    const-wide/16 v0, 0x1

    .line 47
    invoke-virtual {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/bj;->d(J)Lio/reactivex/o;

    move-result-object v0

    invoke-static {}, Lcom/siimkinks/sqlitemagic/bq;->a()Lcom/siimkinks/sqlitemagic/bq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/r;)Lio/reactivex/o;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
