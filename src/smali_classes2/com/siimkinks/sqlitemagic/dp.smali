.class public final Lcom/siimkinks/sqlitemagic/dp;
.super Lcom/siimkinks/sqlitemagic/x;
.source "Transaction_PaymentTypeColumn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/x<",
        "Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;",
        "Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;",
        "Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;",
        "TT;TN;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/eb$a;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    .line 17
    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dp;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/dp<",
            "TT;TN;>;"
        }
    .end annotation

    .line 33
    new-instance v6, Lcom/siimkinks/sqlitemagic/dp;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/dp;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/dp;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/dp;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v4, p0, Lcom/siimkinks/sqlitemagic/dp;->f:Z

    move-object v0, v6

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/siimkinks/sqlitemagic/dp;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v6
.end method

.method a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            ")TV;"
        }
    .end annotation

    .line 39
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/swedbank/mobile/data/overview/transaction/m;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object p1

    return-object p1
.end method

.method a(Landroidx/k/a/f;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/k/a/f;",
            ")TV;"
        }
    .end annotation

    .line 46
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Landroidx/k/a/f;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcom/swedbank/mobile/data/overview/transaction/m;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object p1

    return-object p1
.end method

.method a(Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;)Ljava/lang/String;
    .locals 1

    .line 23
    invoke-static {p1}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 25
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "SQL argument cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method bridge synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/dp;->a(Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/dp;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/dp;

    move-result-object p1

    return-object p1
.end method
