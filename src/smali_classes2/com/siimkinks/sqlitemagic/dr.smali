.class public final Lcom/siimkinks/sqlitemagic/dr;
.super Ljava/lang/Object;
.source "TransferGeneratedClassesManager.java"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 0

    const/4 p0, 0x4

    return p0
.end method

.method public static a(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method

.method public static b(Landroidx/k/a/b;)V
    .locals 3

    .line 24
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 26
    :try_start_0
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Creating tables"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v1, "CREATE TABLE IF NOT EXISTS transfer_search_keyword (local_id INTEGER PRIMARY KEY AUTOINCREMENT, keyword TEXT UNIQUE, search_count INTEGER DEFAULT 0, last_searched INTEGER DEFAULT 0)"

    .line 27
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS predefined_payment_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, customer_id TEXT DEFAULT \'\', predefined_payment_id TEXT UNIQUE, predefined_payment_name TEXT DEFAULT \'\', sender_account_id TEXT DEFAULT NULL, recipient_name TEXT DEFAULT NULL, recipient_iban TEXT DEFAULT NULL, amount TEXT DEFAULT NULL, currency TEXT DEFAULT NULL, description TEXT DEFAULT NULL, reference_number TEXT DEFAULT NULL)"

    .line 28
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS payment_execution_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, uuid TEXT UNIQUE, payment_token TEXT DEFAULT NULL, priority INTEGER DEFAULT NULL, status INTEGER DEFAULT 0, created INTEGER DEFAULT 0, raw_input_json TEXT DEFAULT \'\', result_payment_id TEXT DEFAULT NULL, result_raw_structured_errors TEXT DEFAULT NULL, result_raw_errors TEXT DEFAULT NULL)"

    .line 29
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS suggested_payment_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, suggested_payment_id TEXT DEFAULT \'\', customer_id TEXT DEFAULT \'\', suggested_payment_creditor_bank TEXT DEFAULT \'\', suggested_payment_rank REAL DEFAULT 0.0, suggested_payment_type INTEGER DEFAULT 0, sender_account_id TEXT DEFAULT NULL, recipient_name TEXT DEFAULT NULL, recipient_iban TEXT DEFAULT NULL, amount TEXT DEFAULT NULL, currency TEXT DEFAULT NULL, description TEXT DEFAULT NULL, reference_number TEXT DEFAULT NULL)"

    .line 30
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 31
    invoke-interface {p0}, Landroidx/k/a/b;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 33
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35
    :cond_1
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-void

    :goto_1
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 36
    throw v0
.end method

.method public static c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;
    .locals 6

    .line 41
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 43
    :try_start_0
    new-instance v1, Lcom/siimkinks/sqlitemagic/b/d;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/dr;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    .line 44
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "Clearing data"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v2, "DELETE FROM transfer_search_keyword"

    .line 45
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM predefined_payment_data"

    .line 46
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM payment_execution_data"

    .line 47
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM suggested_payment_data"

    .line 48
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 49
    invoke-interface {p0}, Landroidx/k/a/b;->c()V

    const-string v2, "transfer_search_keyword"

    const-string v3, "predefined_payment_data"

    const-string v4, "payment_execution_data"

    const-string v5, "suggested_payment_data"

    .line 50
    filled-new-array {v2, v3, v4, v5}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 53
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    :cond_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 57
    throw v0
.end method

.method public static d(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method
