.class public final Lcom/siimkinks/sqlitemagic/b;
.super Lcom/siimkinks/sqlitemagic/dl;
.source "AccountDataTable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/siimkinks/sqlitemagic/dl<",
        "Lcom/swedbank/mobile/data/account/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/b;


# instance fields
.field public final b:Lcom/siimkinks/sqlitemagic/dw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dw<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/siimkinks/sqlitemagic/du;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/du<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/siimkinks/sqlitemagic/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/n<",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/siimkinks/sqlitemagic/bn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/bn<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Number;",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    new-instance v0, Lcom/siimkinks/sqlitemagic/b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 8

    const-string v0, "account_data"

    const/16 v1, 0x8

    .line 37
    invoke-direct {p0, v0, p1, v1}, Lcom/siimkinks/sqlitemagic/dl;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 20
    new-instance v7, Lcom/siimkinks/sqlitemagic/dw;

    const-string v2, "local_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->c:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/dw;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/b;->b:Lcom/siimkinks/sqlitemagic/dw;

    .line 22
    new-instance v7, Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "account_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/du;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    .line 24
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "customer_id"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    .line 26
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "iban"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/b;->e:Lcom/siimkinks/sqlitemagic/x;

    .line 28
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "alias"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v5, 0x1

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/b;->f:Lcom/siimkinks/sqlitemagic/x;

    .line 30
    new-instance v6, Lcom/siimkinks/sqlitemagic/n;

    const-string v2, "is_default"

    sget-object v3, Lcom/siimkinks/sqlitemagic/eb;->d:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/siimkinks/sqlitemagic/n;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v6, p0, Lcom/siimkinks/sqlitemagic/b;->g:Lcom/siimkinks/sqlitemagic/n;

    .line 32
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "main_currency"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->a:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/b;->h:Lcom/siimkinks/sqlitemagic/x;

    .line 34
    new-instance v7, Lcom/siimkinks/sqlitemagic/bn;

    const-string v2, "ordering"

    sget-object v4, Lcom/siimkinks/sqlitemagic/eb;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    const/4 v5, 0x1

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    iput-object v7, p0, Lcom/siimkinks/sqlitemagic/b;->i:Lcom/siimkinks/sqlitemagic/bn;

    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/ca$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/ca$b<",
            "Lcom/swedbank/mobile/data/account/b;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 51
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->c()Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 60
    :cond_0
    new-instance p3, Lcom/siimkinks/sqlitemagic/b$2;

    invoke-direct {p3, p0, p1, p2}, Lcom/siimkinks/sqlitemagic/b$2;-><init>(Lcom/siimkinks/sqlitemagic/b;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)V

    return-object p3

    .line 52
    :cond_1
    :goto_0
    new-instance p1, Lcom/siimkinks/sqlitemagic/b$1;

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/b$1;-><init>(Lcom/siimkinks/sqlitemagic/b;)V

    return-object p1
.end method
