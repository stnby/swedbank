.class final Lcom/siimkinks/sqlitemagic/bq$b;
.super Lio/reactivex/f/a;
.source "OperatorRunListQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/bq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/f/a<",
        "Lcom/siimkinks/sqlitemagic/ca<",
        "Ljava/util/List<",
        "TT;>;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Ljava/util/List<",
            "TT;>;>;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Lio/reactivex/f/a;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/bq$b;->a:Lio/reactivex/u;

    return-void
.end method


# virtual methods
.method public a(Lcom/siimkinks/sqlitemagic/ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "Ljava/util/List<",
            "TT;>;>;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 50
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/ca;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 51
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bq$b;->b()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    goto :goto_2

    .line 59
    :cond_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 61
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    goto :goto_1

    .line 63
    :cond_1
    check-cast p1, Lcom/siimkinks/sqlitemagic/ca$a;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/ca$a;->i:Lcom/siimkinks/sqlitemagic/ca$b;

    if-eqz p1, :cond_3

    .line 67
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 68
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bq$b;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 69
    invoke-interface {p1, v0}, Lcom/siimkinks/sqlitemagic/ca$b;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_2
    move-object p1, v2

    .line 73
    :goto_1
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 75
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bq$b;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 76
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bq$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 65
    :cond_3
    :try_start_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v1, "Query needs a mapper -- this is a SqliteMagic bug"

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    .line 73
    :try_start_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw p1

    :cond_4
    :goto_2
    if-eqz v0, :cond_5

    .line 53
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    :cond_5
    return-void

    :catch_0
    move-exception p1

    .line 79
    invoke-static {p1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 80
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/bq$b;->onError(Ljava/lang/Throwable;)V

    :cond_6
    :goto_3
    return-void
.end method

.method protected c()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bq$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p0}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    return-void
.end method

.method public onComplete()V
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bq$b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bq$b;->a:Lio/reactivex/u;

    invoke-interface {v0}, Lio/reactivex/u;->onComplete()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/bq$b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/bq$b;->a:Lio/reactivex/u;

    invoke-interface {v0, p1}, Lio/reactivex/u;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/siimkinks/sqlitemagic/ca;

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/bq$b;->a(Lcom/siimkinks/sqlitemagic/ca;)V

    return-void
.end method
