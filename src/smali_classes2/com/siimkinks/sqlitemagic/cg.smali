.class public Lcom/siimkinks/sqlitemagic/cg;
.super Lio/reactivex/o;
.source "SingleItemQueryObservable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/o<",
        "Lcom/siimkinks/sqlitemagic/ca<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "TR;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "TR;>;>;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cg;->a:Lio/reactivex/o;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Lio/reactivex/o<",
            "TR;>;"
        }
    .end annotation

    .line 48
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/br;->a(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/br;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/cg;->a(Lio/reactivex/r;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method protected a(Lio/reactivex/u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "TR;>;>;)V"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cg;->a:Lio/reactivex/o;

    invoke-virtual {v0, p1}, Lio/reactivex/o;->d(Lio/reactivex/u;)V

    return-void
.end method

.method public final b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TR;>;"
        }
    .end annotation

    .line 35
    invoke-static {}, Lcom/siimkinks/sqlitemagic/br;->a()Lcom/siimkinks/sqlitemagic/br;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/cg;->a(Lio/reactivex/r;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    const-wide/16 v0, 0x1

    .line 72
    invoke-virtual {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/cg;->d(J)Lio/reactivex/o;

    move-result-object v0

    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/br;->a(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/br;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/o;->a(Lio/reactivex/r;)Lio/reactivex/o;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "TR;>;"
        }
    .end annotation

    const-wide/16 v0, 0x1

    .line 59
    invoke-virtual {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/cg;->d(J)Lio/reactivex/o;

    move-result-object v0

    invoke-static {}, Lcom/siimkinks/sqlitemagic/br;->a()Lcom/siimkinks/sqlitemagic/br;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/r;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->a(J)Lio/reactivex/j;

    move-result-object v0

    return-object v0
.end method
