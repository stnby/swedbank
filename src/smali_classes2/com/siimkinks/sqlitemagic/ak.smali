.class public final Lcom/siimkinks/sqlitemagic/ak;
.super Ljava/lang/Object;
.source "CoreGeneratedClassesManager.java"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 0

    const/4 p0, 0x7

    return p0
.end method

.method public static a(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method

.method public static b(Landroidx/k/a/b;)V
    .locals 3

    .line 27
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 29
    :try_start_0
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Creating tables"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v1, "CREATE TABLE IF NOT EXISTS customer_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, customer_id TEXT UNIQUE, name TEXT DEFAULT \'\', mobile_agreement_id TEXT DEFAULT NULL, type INTEGER DEFAULT 0, legal_code TEXT DEFAULT NULL, swedbank_employee INTEGER DEFAULT 0, logged_in INTEGER DEFAULT 0, selected INTEGER DEFAULT 0)"

    .line 30
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS login_info (local_id INTEGER PRIMARY KEY AUTOINCREMENT, login_method TEXT UNIQUE, extra TEXT DEFAULT \'\')"

    .line 31
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS active_scoped_session (local_id INTEGER PRIMARY KEY AUTOINCREMENT, access_token TEXT DEFAULT \'\', refresh_token TEXT DEFAULT \'\', expires_in INTEGER DEFAULT 0, created INTEGER DEFAULT 0)"

    .line 32
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS active_session (local_id INTEGER PRIMARY KEY AUTOINCREMENT, access_token TEXT DEFAULT \'\', refresh_token TEXT DEFAULT \'\', id_token TEXT DEFAULT NULL, expires_in INTEGER DEFAULT 0, created INTEGER DEFAULT 0)"

    .line 33
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS user_property_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, enabled INTEGER DEFAULT 0)"

    .line 34
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS account_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, account_id TEXT UNIQUE, customer_id TEXT DEFAULT \'\', iban TEXT DEFAULT \'\', alias TEXT DEFAULT NULL, is_default INTEGER DEFAULT 0, main_currency TEXT DEFAULT \'\', ordering INTEGER DEFAULT NULL)"

    .line 35
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS balance_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, account_id TEXT DEFAULT \'\', customer_id TEXT DEFAULT \'\', currency TEXT DEFAULT \'\', available_funds TEXT DEFAULT \'\', credit TEXT DEFAULT \'\', deposit TEXT DEFAULT \'\', reserved TEXT DEFAULT \'\', is_credit_account INTEGER DEFAULT 0)"

    .line 36
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 37
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "Creating views"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/data/account/r;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v2, "main_balance_reserved_amount_view"

    invoke-static {p0, v1, v2}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    .line 39
    sget-object v1, Lcom/swedbank/mobile/data/account/p;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v2, "account_view"

    invoke-static {p0, v1, v2}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    .line 40
    sget-object v1, Lcom/swedbank/mobile/data/account/m;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v2, "account_balance_view"

    invoke-static {p0, v1, v2}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    .line 41
    sget-object v1, Lcom/swedbank/mobile/data/account/o;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v2, "account_summary_view"

    invoke-static {p0, v1, v2}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    .line 42
    invoke-interface {p0}, Landroidx/k/a/b;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 44
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_2

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 46
    :cond_2
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-void

    :goto_1
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 47
    throw v0
.end method

.method public static c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;
    .locals 10

    .line 52
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 54
    :try_start_0
    new-instance v1, Lcom/siimkinks/sqlitemagic/b/d;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/ak;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    .line 55
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "Clearing data"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v2, "DELETE FROM customer_data"

    .line 56
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM login_info"

    .line 57
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM active_scoped_session"

    .line 58
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM active_session"

    .line 59
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM user_property_data"

    .line 60
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM account_data"

    .line 61
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM balance_data"

    .line 62
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 63
    invoke-interface {p0}, Landroidx/k/a/b;->c()V

    const-string v3, "customer_data"

    const-string v4, "login_info"

    const-string v5, "active_scoped_session"

    const-string v6, "active_session"

    const-string v7, "user_property_data"

    const-string v8, "account_data"

    const-string v9, "balance_data"

    .line 64
    filled-new-array/range {v3 .. v9}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 67
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    :cond_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 71
    throw v0
.end method

.method public static d(Landroidx/k/a/b;)V
    .locals 2

    .line 75
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Migrating views"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v0, "DROP VIEW IF EXISTS main_balance_reserved_amount_view"

    .line 76
    invoke-interface {p0, v0}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 77
    sget-object v0, Lcom/swedbank/mobile/data/account/r;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v1, "main_balance_reserved_amount_view"

    invoke-static {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    const-string v0, "DROP VIEW IF EXISTS account_view"

    .line 78
    invoke-interface {p0, v0}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 79
    sget-object v0, Lcom/swedbank/mobile/data/account/p;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v1, "account_view"

    invoke-static {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    const-string v0, "DROP VIEW IF EXISTS account_balance_view"

    .line 80
    invoke-interface {p0, v0}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/data/account/m;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v1, "account_balance_view"

    invoke-static {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    const-string v0, "DROP VIEW IF EXISTS account_summary_view"

    .line 82
    invoke-interface {p0, v0}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 83
    sget-object v0, Lcom/swedbank/mobile/data/account/o;->a:Lcom/siimkinks/sqlitemagic/ae;

    const-string v1, "account_summary_view"

    invoke-static {p0, v0, v1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;Lcom/siimkinks/sqlitemagic/ae;Ljava/lang/String;)V

    return-void
.end method
