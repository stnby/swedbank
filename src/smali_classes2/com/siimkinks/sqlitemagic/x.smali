.class public Lcom/siimkinks/sqlitemagic/x;
.super Ljava/lang/Object;
.source "Column.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        "ET:",
        "Ljava/lang/Object;",
        "P:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Lcom/siimkinks/sqlitemagic/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TP;>;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/String;

.field final c:Z

.field final d:Ljava/lang/String;

.field final e:Lcom/siimkinks/sqlitemagic/eb$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "*>;"
        }
    .end annotation
.end field

.field final f:Z

.field final g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TP;>;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "*>;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 70
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    .line 71
    iput-boolean p3, p0, Lcom/siimkinks/sqlitemagic/x;->c:Z

    .line 72
    iput-object p4, p0, Lcom/siimkinks/sqlitemagic/x;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 73
    iput-boolean p5, p0, Lcom/siimkinks/sqlitemagic/x;->f:Z

    .line 74
    iput-object p6, p0, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    .line 75
    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/dl;->y:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_0
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/x;->d:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TP;>;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "*>;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 59
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    .line 60
    iput-boolean p3, p0, Lcom/siimkinks/sqlitemagic/x;->c:Z

    .line 61
    iput-object p4, p0, Lcom/siimkinks/sqlitemagic/x;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    .line 62
    iput-boolean p5, p0, Lcom/siimkinks/sqlitemagic/x;->f:Z

    .line 63
    iput-object p6, p0, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    .line 64
    iput-object p7, p0, Lcom/siimkinks/sqlitemagic/x;->d:Ljava/lang/String;

    return-void
.end method

.method static a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Lcom/siimkinks/sqlitemagic/x;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 237
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    :cond_0
    iget-object p1, p3, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 240
    iget-object p1, p3, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "I)I"
        }
    .end annotation

    .line 154
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->d:Ljava/lang/String;

    .line 155
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/StringBuilder;)V

    .line 157
    iget-boolean p2, p0, Lcom/siimkinks/sqlitemagic/x;->c:Z

    if-eqz p2, :cond_0

    .line 158
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 159
    iget-object v0, p2, Lcom/siimkinks/sqlitemagic/dl;->y:Ljava/lang/String;

    invoke-static {p1, v0, p3, p0}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    .line 160
    iget p1, p2, Lcom/siimkinks/sqlitemagic/dl;->z:I

    add-int/2addr p3, p1

    move p2, p3

    goto :goto_0

    :cond_0
    add-int/lit8 p2, p3, 0x1

    .line 162
    invoke-static {p1, v0, p3, p0}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    :goto_0
    return p2
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;I)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;I)I"
        }
    .end annotation

    .line 183
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 184
    iget-boolean v1, p0, Lcom/siimkinks/sqlitemagic/x;->c:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 185
    iget v3, v0, Lcom/siimkinks/sqlitemagic/dl;->z:I

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 188
    :goto_0
    iget-boolean v4, v0, Lcom/siimkinks/sqlitemagic/dl;->A:Z

    if-nez v4, :cond_6

    iget-object v4, v0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {p3, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/LinkedList;

    if-nez p3, :cond_1

    goto :goto_5

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const/4 v6, 0x1

    if-eqz v1, :cond_3

    .line 204
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    goto :goto_2

    .line 208
    :cond_2
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 210
    :goto_2
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 212
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-static {p1, v1, p4, p0}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    add-int/2addr p4, v3

    goto :goto_1

    .line 218
    :cond_3
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_3
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v6, :cond_4

    const/4 v6, 0x0

    goto :goto_4

    .line 222
    :cond_4
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 224
    :goto_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 225
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, p4, 0x1

    .line 226
    invoke-static {p1, v1, p4, p0}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    move p4, v3

    goto :goto_3

    :cond_5
    return p4

    .line 189
    :cond_6
    :goto_5
    iget-object p3, p0, Lcom/siimkinks/sqlitemagic/x;->d:Ljava/lang/String;

    .line 190
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/StringBuilder;)V

    if-eqz v1, :cond_7

    .line 193
    iget-object p2, v0, Lcom/siimkinks/sqlitemagic/dl;->y:Ljava/lang/String;

    invoke-static {p1, p2, p4, p0}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    add-int/2addr p4, v3

    move p2, p4

    goto :goto_6

    :cond_7
    add-int/lit8 p2, p4, 0x1

    .line 196
    invoke-static {p1, p3, p4, p0}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    :goto_6
    return p2
.end method

.method public final a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/siimkinks/sqlitemagic/x<",
            "**+TET;**>;>(TC;)",
            "Lcom/siimkinks/sqlitemagic/bb;"
        }
    .end annotation

    .line 527
    new-instance v0, Lcom/siimkinks/sqlitemagic/bd;

    const-string v1, "="

    invoke-direct {v0, p0, v1, p1}, Lcom/siimkinks/sqlitemagic/bd;-><init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/x;)V

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "TT;>;)",
            "Lcom/siimkinks/sqlitemagic/bb;"
        }
    .end annotation

    .line 676
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 680
    new-array v1, v0, [Ljava/lang/String;

    .line 681
    new-instance v2, Ljava/lang/StringBuilder;

    shl-int/lit8 v3, v0, 0x1

    add-int/lit8 v3, v3, 0x6

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, " IN ("

    .line 682
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    if-lez v3, :cond_0

    const/16 v4, 0x2c

    .line 686
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/16 v4, 0x3f

    .line 688
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 689
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/16 p1, 0x29

    .line 691
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 692
    new-instance p1, Lcom/siimkinks/sqlitemagic/be;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p0, v0, v1}, Lcom/siimkinks/sqlitemagic/be;-><init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;[Ljava/lang/String;)V

    return-object p1

    .line 678
    :cond_2
    new-instance p1, Landroid/database/SQLException;

    const-string v0, "Empty IN clause values"

    invoke-direct {p1, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            ")TV;"
        }
    .end annotation

    .line 252
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/x;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    invoke-interface {v0, p1}, Lcom/siimkinks/sqlitemagic/eb$a;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method a(Landroidx/k/a/f;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/k/a/f;",
            ")TV;"
        }
    .end annotation

    .line 262
    :try_start_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    invoke-interface {v0, p1}, Lcom/siimkinks/sqlitemagic/eb$a;->b(Landroidx/k/a/f;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 246
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/d;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/b/d;->a(Ljava/lang/String;)Z

    return-void
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 86
    iget-boolean v1, v0, Lcom/siimkinks/sqlitemagic/dl;->A:Z

    if-nez v1, :cond_2

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/dl;->w:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/LinkedList;

    if-nez p2, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p2}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 92
    invoke-virtual {p2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x2e

    .line 93
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    .line 94
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 90
    :cond_1
    new-instance p1, Landroid/database/SQLException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Ambiguous column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/x;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; aliases="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 87
    :cond_2
    :goto_0
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/x;->d:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method a()Z
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/x;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/siimkinks/sqlitemagic/bb;"
        }
    .end annotation

    .line 514
    new-instance v0, Lcom/siimkinks/sqlitemagic/bc;

    const-string v1, "=?"

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, v1, p1}, Lcom/siimkinks/sqlitemagic/bc;-><init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "TT;>;)",
            "Lcom/siimkinks/sqlitemagic/bb;"
        }
    .end annotation

    .line 751
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 755
    new-array v1, v0, [Ljava/lang/String;

    .line 756
    new-instance v2, Ljava/lang/StringBuilder;

    shl-int/lit8 v3, v0, 0x1

    add-int/lit8 v3, v3, 0xa

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, " NOT IN ("

    .line 757
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 758
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    if-lez v3, :cond_0

    const/16 v4, 0x2c

    .line 761
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/16 v4, 0x3f

    .line 763
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 764
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/16 p1, 0x29

    .line 766
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 767
    new-instance p1, Lcom/siimkinks/sqlitemagic/be;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p0, v0, v1}, Lcom/siimkinks/sqlitemagic/be;-><init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;[Ljava/lang/String;)V

    return-object p1

    .line 753
    :cond_2
    new-instance p1, Landroid/database/SQLException;

    const-string v0, "Empty IN clause values"

    invoke-direct {p1, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/x<",
            "TT;TR;TET;TP;TN;>;"
        }
    .end annotation

    .line 315
    new-instance v7, Lcom/siimkinks/sqlitemagic/x;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/siimkinks/sqlitemagic/x;->c:Z

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/x;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v5, p0, Lcom/siimkinks/sqlitemagic/x;->f:Z

    move-object v0, v7

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/siimkinks/sqlitemagic/x;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;)V

    return-object v7
.end method

.method b()Ljava/lang/String;
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/16 v1, 0x2e

    .line 116
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    .line 118
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0

    .line 114
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Column alias == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method b(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, " AS \'"

    .line 100
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/x;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x27

    .line 102
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method b(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final c(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/siimkinks/sqlitemagic/bb;"
        }
    .end annotation

    .line 551
    new-instance v0, Lcom/siimkinks/sqlitemagic/bc;

    const-string v1, "!=?"

    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, v1, p1}, Lcom/siimkinks/sqlitemagic/bc;-><init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lcom/siimkinks/sqlitemagic/cb$f;
    .locals 3

    .line 357
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$f;

    const-string v1, " ASC"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2, v1}, Lcom/siimkinks/sqlitemagic/cb$f;-><init>(Lcom/siimkinks/sqlitemagic/x;Lcom/siimkinks/sqlitemagic/bb;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lcom/siimkinks/sqlitemagic/cb$f;
    .locals 3

    .line 370
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$f;

    const-string v1, " DESC"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2, v1}, Lcom/siimkinks/sqlitemagic/cb$f;-><init>(Lcom/siimkinks/sqlitemagic/x;Lcom/siimkinks/sqlitemagic/bb;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e()Lcom/siimkinks/sqlitemagic/bb;
    .locals 2

    .line 598
    new-instance v0, Lcom/siimkinks/sqlitemagic/bb;

    const-string v1, " IS NOT NULL"

    invoke-direct {v0, p0, v1}, Lcom/siimkinks/sqlitemagic/bb;-><init>(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 822
    :cond_1
    :try_start_0
    check-cast p1, Lcom/siimkinks/sqlitemagic/x;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 827
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v3, p1, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/dl;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 832
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 837
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/x;->a:Lcom/siimkinks/sqlitemagic/dl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
