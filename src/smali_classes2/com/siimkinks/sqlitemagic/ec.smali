.class public final Lcom/siimkinks/sqlitemagic/ec;
.super Ljava/lang/Object;
.source "VariableArgsOperationHelper.java"


# instance fields
.field final a:Z

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/StringBuilder;

.field private d:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 15
    iput v0, p0, Lcom/siimkinks/sqlitemagic/ec;->d:I

    .line 20
    sget-object v0, Lcom/siimkinks/sqlitemagic/aj;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/ec;->b:Ljava/lang/String;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 21
    :goto_0
    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/ec;->a:Z

    return-void
.end method

.method private static a(ZILjava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/ba;",
            ")",
            "Landroidx/k/a/f;"
        }
    .end annotation

    .line 61
    invoke-virtual {p5}, Lcom/siimkinks/sqlitemagic/b/c;->d()I

    move-result v0

    const/16 v1, 0x2c

    const/4 v2, 0x0

    if-nez p1, :cond_5

    if-eqz p0, :cond_0

    .line 64
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p0

    add-int/lit8 p0, p0, 0xc

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_0

    .line 66
    :cond_0
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string p0, "INSERT"

    .line 68
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " INTO "

    .line 70
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :goto_0
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x28

    .line 74
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 p0, 0x0

    :goto_1
    if-ge p0, v0, :cond_2

    if-lez p0, :cond_1

    .line 78
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    :cond_1
    invoke-virtual {p5, p0}, Lcom/siimkinks/sqlitemagic/b/c;->b(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p0, p0, 0x1

    goto :goto_1

    :cond_2
    const-string p0, ") VALUES ("

    .line 82
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p0, 0x0

    :goto_2
    if-ge p0, v0, :cond_4

    if-lez p0, :cond_3

    const-string p3, ",?"

    .line 85
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    const/16 p3, 0x3f

    .line 87
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_3
    add-int/lit8 p0, p0, 0x1

    goto :goto_2

    :cond_4
    const/16 p0, 0x29

    .line 90
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_5
    if-eqz p0, :cond_6

    .line 93
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p0

    add-int/lit8 p0, p0, 0x7

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_4

    .line 95
    :cond_6
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string p0, "UPDATE"

    .line 97
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x20

    .line 99
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    :goto_4
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " SET "

    .line 103
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p0, 0x0

    :goto_5
    if-ge p0, v0, :cond_8

    if-lez p0, :cond_7

    .line 106
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 109
    :cond_7
    invoke-virtual {p5, p0}, Lcom/siimkinks/sqlitemagic/b/c;->b(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "=?"

    .line 110
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p0, p0, 0x1

    goto :goto_5

    :cond_8
    const-string p0, " WHERE "

    .line 114
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {p2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "=?"

    .line 116
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :goto_6
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p7, p0}, Lcom/siimkinks/sqlitemagic/ba;->c(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p0

    :goto_7
    if-ge v2, v0, :cond_9

    .line 122
    invoke-virtual {p5, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(I)Ljava/lang/Object;

    move-result-object p2

    add-int/lit8 v2, v2, 0x1

    .line 123
    invoke-static {p0, v2, p2}, Lcom/siimkinks/sqlitemagic/ec;->a(Landroidx/k/a/f;ILjava/lang/Object;)V

    goto :goto_7

    :cond_9
    const/4 p2, 0x1

    if-ne p1, p2, :cond_b

    .line 126
    invoke-virtual {p5, p6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_a

    add-int/2addr v2, p2

    .line 130
    invoke-static {p0, v2, p1}, Lcom/siimkinks/sqlitemagic/ec;->a(Landroidx/k/a/f;ILjava/lang/Object;)V

    goto :goto_8

    .line 128
    :cond_a
    new-instance p0, Ljava/lang/NullPointerException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Column \""

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\" is null"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_b
    :goto_8
    return-object p0
.end method

.method private static a(Landroidx/k/a/f;ILjava/lang/Object;)V
    .locals 2

    .line 136
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 137
    check-cast p2, Ljava/lang/String;

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 138
    :cond_0
    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_3

    .line 139
    instance-of v0, p2, Ljava/lang/Float;

    if-nez v0, :cond_2

    instance-of v0, p2, Ljava/lang/Double;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 142
    :cond_1
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-interface {p0, p1, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    goto :goto_1

    .line 140
    :cond_2
    :goto_0
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-interface {p0, p1, v0, v1}, Landroidx/k/a/f;->a(ID)V

    goto :goto_1

    .line 144
    :cond_3
    instance-of v0, p2, [B

    if-eqz v0, :cond_4

    .line 145
    check-cast p2, [B

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(I[B)V

    goto :goto_1

    .line 146
    :cond_4
    instance-of v0, p2, [Ljava/lang/Byte;

    if-eqz v0, :cond_5

    .line 147
    check-cast p2, [Ljava/lang/Byte;

    invoke-static {p2}, Lcom/siimkinks/sqlitemagic/eb;->a([Ljava/lang/Byte;)[B

    move-result-object p2

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(I[B)V

    goto :goto_1

    .line 149
    :cond_5
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    :goto_1
    return-void
.end method


# virtual methods
.method a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/ba;",
            ")",
            "Landroidx/k/a/f;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ec;->c:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ec;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x7

    mul-int/lit8 v3, p3, 0x16

    add-int/2addr v2, v3

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 35
    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/ec;->c:Ljava/lang/StringBuilder;

    :cond_0
    move-object v2, v0

    .line 37
    iget v0, p0, Lcom/siimkinks/sqlitemagic/ec;->d:I

    .line 38
    iput p1, p0, Lcom/siimkinks/sqlitemagic/ec;->d:I

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 40
    :goto_0
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/ec;->b:Ljava/lang/String;

    move v1, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-static/range {v0 .. v7}, Lcom/siimkinks/sqlitemagic/ec;->a(ZILjava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v0

    return-object v0
.end method
