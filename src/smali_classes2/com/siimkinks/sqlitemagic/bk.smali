.class public final Lcom/siimkinks/sqlitemagic/bk;
.super Ljava/lang/Object;
.source "LogUtil.java"


# direct methods
.method public static a(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6

    .line 29
    sget-object v0, Lcom/siimkinks/sqlitemagic/co;->b:Lcom/siimkinks/sqlitemagic/bl;

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/siimkinks/sqlitemagic/bl;->a(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public static varargs a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 48
    array-length v0, p2

    if-lez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 49
    :cond_0
    sget-object p2, Lcom/siimkinks/sqlitemagic/co;->b:Lcom/siimkinks/sqlitemagic/bl;

    invoke-interface {p2, p1, p0}, Lcom/siimkinks/sqlitemagic/bl;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 16
    array-length v0, p1

    if-lez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :cond_0
    const-string p1, "SqliteMagic"

    .line 17
    invoke-static {p1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 33
    array-length v0, p1

    if-lez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 34
    :cond_0
    sget-object p1, Lcom/siimkinks/sqlitemagic/co;->b:Lcom/siimkinks/sqlitemagic/bl;

    invoke-interface {p1, p0}, Lcom/siimkinks/sqlitemagic/bl;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 43
    array-length v0, p1

    if-lez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 44
    :cond_0
    sget-object p1, Lcom/siimkinks/sqlitemagic/co;->b:Lcom/siimkinks/sqlitemagic/bl;

    invoke-interface {p1, p0}, Lcom/siimkinks/sqlitemagic/bl;->b(Ljava/lang/String;)V

    return-void
.end method
