.class public final Lcom/siimkinks/sqlitemagic/da;
.super Ljava/lang/Object;
.source "SqliteMagic_PaymentExecutionData_Handler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/da$b;,
        Lcom/siimkinks/sqlitemagic/da$a;
    }
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/data/transfer/payment/d;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/bo;)J
    .locals 5

    .line 52
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const-string v0, "INSERT\n  table: payment_execution_data\n  object: %s"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/payment/d;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v0, "payment_execution_data"

    const-string v3, "INSERT%s INTO payment_execution_data (uuid, payment_token, priority, status, created, raw_input_json, result_payment_id, result_raw_structured_errors, result_raw_errors) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 54
    invoke-virtual {p2, v0, v3, p1}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object p1

    .line 55
    monitor-enter p1

    .line 56
    :try_start_0
    invoke-static {p1, p0}, Lcom/swedbank/mobile/data/transfer/payment/p;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/payment/d;)V

    .line 57
    invoke-interface {p1}, Landroidx/k/a/f;->b()J

    move-result-wide v3

    .line 58
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    sget-boolean p1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p1, :cond_1

    const-string p1, "INSERT id: %s"

    new-array p2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p2, v1

    invoke-static {p1, p2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    const-wide/16 p1, -0x1

    cmp-long p1, v3, p1

    if-eqz p1, :cond_2

    return-wide v3

    .line 61
    :cond_2
    new-instance p1, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Failed to insert "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p0

    .line 58
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method public static a(Lcom/swedbank/mobile/data/transfer/payment/d;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/bo;Ljava/util/ArrayList;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/transfer/payment/d;",
            "Lcom/siimkinks/sqlitemagic/ba;",
            "Lcom/siimkinks/sqlitemagic/bo;",
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;)Z"
        }
    .end annotation

    .line 69
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const-string v0, "UPDATE\n  table: payment_execution_data\n  object: %s"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/payment/d;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v0, "payment_execution_data"

    .line 70
    invoke-static {v0, p3}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 73
    iget-object p3, p3, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string p3, "local_id"

    :goto_0
    const-string v0, "payment_execution_data"

    const-string v3, "UPDATE%s payment_execution_data SET uuid=?, payment_token=?, priority=?, status=?, created=?, raw_input_json=?, result_payment_id=?, result_raw_structured_errors=?, result_raw_errors=? WHERE local_id=?"

    .line 77
    invoke-virtual {p2, v0, v3, p1}, Lcom/siimkinks/sqlitemagic/bo;->b(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object p1

    .line 78
    monitor-enter p1

    .line 79
    :try_start_0
    invoke-static {p1, p0}, Lcom/swedbank/mobile/data/transfer/payment/p;->b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/payment/d;)V

    const/16 p2, 0xa

    .line 80
    invoke-static {p1, p2, p3, p0}, Lcom/swedbank/mobile/data/transfer/payment/p;->a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/transfer/payment/d;)V

    .line 81
    invoke-interface {p1}, Landroidx/k/a/f;->a()I

    move-result p0

    .line 82
    sget-boolean p2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p2, :cond_2

    const-string p2, "UPDATE rows affected: %s"

    new-array p3, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p3, v1

    invoke-static {p2, p3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    if-lez p0, :cond_3

    const/4 v1, 0x1

    .line 83
    :cond_3
    monitor-exit p1

    return v1

    :catchall_0
    move-exception p0

    .line 84
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method
