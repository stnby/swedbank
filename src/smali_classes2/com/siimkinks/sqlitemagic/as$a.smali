.class final Lcom/siimkinks/sqlitemagic/as$a;
.super Lcom/siimkinks/sqlitemagic/b/d;
.source "DbConnectionImpl.java"

# interfaces
.implements Landroid/database/sqlite/SQLiteTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/as;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field final a:Lcom/siimkinks/sqlitemagic/as$a;

.field b:Z


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/as$a;)V
    .locals 0

    .line 215
    invoke-direct {p0}, Lcom/siimkinks/sqlitemagic/b/d;-><init>()V

    .line 216
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/as$a;->a:Lcom/siimkinks/sqlitemagic/as$a;

    return-void
.end method


# virtual methods
.method public onBegin()V
    .locals 0

    return-void
.end method

.method public onCommit()V
    .locals 1

    const/4 v0, 0x1

    .line 225
    iput-boolean v0, p0, Lcom/siimkinks/sqlitemagic/as$a;->b:Z

    return-void
.end method

.method public onRollback()V
    .locals 0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "%08x"

    const/4 v1, 0x1

    .line 234
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 235
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/as$a;->a:Lcom/siimkinks/sqlitemagic/as$a;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/as$a;->a:Lcom/siimkinks/sqlitemagic/as$a;

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as$a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
