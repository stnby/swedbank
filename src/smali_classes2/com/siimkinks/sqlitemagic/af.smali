.class final Lcom/siimkinks/sqlitemagic/af;
.super Lcom/siimkinks/sqlitemagic/ca$a;
.source "CompiledSelect1Impl.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/ae;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/af$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/ca$a<",
        "Ljava/util/List<",
        "TT;>;TT;>;",
        "Lcom/siimkinks/sqlitemagic/ae<",
        "TT;TS;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:[Ljava/lang/String;

.field final c:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "*TT;***>;"
        }
    .end annotation
.end field

.field final d:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/as;Lcom/siimkinks/sqlitemagic/x;[Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/as;",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*TT;***>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/siimkinks/sqlitemagic/af$1;

    invoke-direct {v0, p4}, Lcom/siimkinks/sqlitemagic/af$1;-><init>(Lcom/siimkinks/sqlitemagic/x;)V

    invoke-direct {p0, p3, v0}, Lcom/siimkinks/sqlitemagic/ca$a;-><init>(Lcom/siimkinks/sqlitemagic/as;Lcom/siimkinks/sqlitemagic/ca$b;)V

    .line 43
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/af;->a:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/af;->b:[Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/siimkinks/sqlitemagic/af;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 46
    iput-object p5, p0, Lcom/siimkinks/sqlitemagic/af;->d:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method a(Z)Landroid/database/Cursor;
    .locals 5

    .line 52
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/ca$a;->a(Z)Landroid/database/Cursor;

    .line 53
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/af;->h:Lcom/siimkinks/sqlitemagic/as;

    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/as;->c()Landroidx/k/a/b;

    move-result-object p1

    .line 54
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 55
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/af;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/af;->b:[Ljava/lang/String;

    invoke-interface {p1, v2, v3}, Landroidx/k/a/b;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;

    move-result-object p1

    .line 56
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    .line 57
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 58
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/af;->d:[Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/af;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/af;->b:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/siimkinks/sqlitemagic/bk;->a(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 60
    :cond_0
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/bf;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 89
    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/af;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/af;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 66
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 68
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0

    .line 70
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/af;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 71
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 72
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 77
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v2

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public b()Lcom/siimkinks/sqlitemagic/bj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/bj<",
            "TT;>;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/siimkinks/sqlitemagic/bj;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/af;->d:[Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/siimkinks/sqlitemagic/ag;->a([Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ca$a;)Lio/reactivex/o;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/bj;-><init>(Lio/reactivex/o;)V

    return-object v0
.end method

.method synthetic b(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0

    .line 22
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/af;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public c()Lcom/siimkinks/sqlitemagic/ad;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/ad<",
            "TT;TS;>;"
        }
    .end annotation

    .line 101
    new-instance v0, Lcom/siimkinks/sqlitemagic/af$a;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/af;->h:Lcom/siimkinks/sqlitemagic/as;

    invoke-direct {v0, p0, v1}, Lcom/siimkinks/sqlitemagic/af$a;-><init>(Lcom/siimkinks/sqlitemagic/af;Lcom/siimkinks/sqlitemagic/as;)V

    return-object v0
.end method

.method public d()Lcom/siimkinks/sqlitemagic/ab;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/ab<",
            "TS;>;"
        }
    .end annotation

    .line 107
    new-instance v0, Lcom/siimkinks/sqlitemagic/ag$a;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/af;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/af;->b:[Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/af;->h:Lcom/siimkinks/sqlitemagic/as;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/af;->d:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/siimkinks/sqlitemagic/ag$a;-><init>(Ljava/lang/String;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/as;[Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Select1<List>; sql="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/af;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
