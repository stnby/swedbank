.class final Lcom/siimkinks/sqlitemagic/aq;
.super Landroidx/k/a/c$a;
.source "DbCallback.java"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/siimkinks/sqlitemagic/at;


# direct methods
.method constructor <init>(Landroid/content/Context;ILcom/siimkinks/sqlitemagic/at;)V
    .locals 0

    .line 26
    invoke-direct {p0, p2}, Landroidx/k/a/c$a;-><init>(I)V

    .line 27
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/aq;->b:Landroid/content/Context;

    .line 28
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/aq;->c:Lcom/siimkinks/sqlitemagic/at;

    return-void
.end method

.method private a(Landroidx/k/a/b;Landroid/content/res/AssetManager;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x0

    .line 66
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {p2, p3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p2

    invoke-direct {v2, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 67
    :try_start_1
    sget-boolean p2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p2, :cond_0

    const-string p2, "Executing script %s"

    const/4 v0, 0x1

    .line 68
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v0, v2

    invoke-static {p2, v0}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 72
    invoke-interface {p1, p2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    nop

    goto :goto_2

    :catchall_1
    move-exception p1

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_1

    .line 78
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :cond_1
    throw p1

    :catch_1
    move-object v1, v0

    :goto_2
    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :cond_3
    return-void
.end method


# virtual methods
.method public a(Landroidx/k/a/b;)V
    .locals 0

    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/bh;->a(Landroidx/k/a/b;)V

    return-void
.end method

.method public a(Landroidx/k/a/b;II)V
    .locals 8

    const/4 v0, 0x0

    .line 40
    :try_start_0
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Executing upgrade scripts"

    .line 41
    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/aq;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 44
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cn;->a()[Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-ge p2, p3, :cond_2

    add-int/lit8 p2, p2, 0x1

    .line 47
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ".sql"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_1

    .line 49
    array-length v4, v2

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_1

    .line 51
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v2, v5

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v1, v6}, Lcom/siimkinks/sqlitemagic/aq;->a(Landroidx/k/a/b;Landroid/content/res/AssetManager;Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 54
    :cond_1
    invoke-direct {p0, p1, v1, v3}, Lcom/siimkinks/sqlitemagic/aq;->a(Landroidx/k/a/b;Landroid/content/res/AssetManager;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_2
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/cn;->c(Landroidx/k/a/b;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 58
    new-array p2, v0, [Ljava/lang/Object;

    const-string p3, "Error executing upgrade scripts"

    invoke-static {p3, p2}, Lcom/siimkinks/sqlitemagic/bk;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public b(Landroidx/k/a/b;)V
    .locals 0

    .line 33
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/b;)V

    return-void
.end method

.method public b(Landroidx/k/a/b;II)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/aq;->c:Lcom/siimkinks/sqlitemagic/at;

    invoke-interface {v0, p1, p2, p3}, Lcom/siimkinks/sqlitemagic/at;->a(Landroidx/k/a/b;II)V

    return-void
.end method
