.class public final Lcom/siimkinks/sqlitemagic/co$a;
.super Ljava/lang/Object;
.source "SqliteMagic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/co;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Landroidx/k/a/c$c;

.field c:Lcom/siimkinks/sqlitemagic/at;

.field d:Lio/reactivex/v;

.field private final e:Landroid/app/Application;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 1

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    new-instance v0, Lcom/siimkinks/sqlitemagic/au;

    invoke-direct {v0}, Lcom/siimkinks/sqlitemagic/au;-><init>()V

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/co$a;->c:Lcom/siimkinks/sqlitemagic/at;

    .line 134
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/co$a;->d:Lio/reactivex/v;

    if-eqz p1, :cond_0

    .line 140
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/co$a;->e:Landroid/app/Application;

    return-void

    .line 138
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Application context cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(Landroidx/k/a/c$c;)Lcom/siimkinks/sqlitemagic/co$a;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/co$a;->b:Landroidx/k/a/c$c;

    return-object p0
.end method

.method public a()V
    .locals 2

    .line 211
    sget-object v0, Lcom/siimkinks/sqlitemagic/co$b;->a:Lcom/siimkinks/sqlitemagic/co;

    .line 212
    iget-object v1, v0, Lcom/siimkinks/sqlitemagic/co;->c:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, v0, Lcom/siimkinks/sqlitemagic/co;->c:Lcom/siimkinks/sqlitemagic/as;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/as;->close()V

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/co$a;->e:Landroid/app/Application;

    invoke-static {v1, p0}, Lcom/siimkinks/sqlitemagic/co;->a(Landroid/app/Application;Lcom/siimkinks/sqlitemagic/co$a;)Lcom/siimkinks/sqlitemagic/as;

    move-result-object v1

    iput-object v1, v0, Lcom/siimkinks/sqlitemagic/co;->c:Lcom/siimkinks/sqlitemagic/as;

    return-void
.end method
