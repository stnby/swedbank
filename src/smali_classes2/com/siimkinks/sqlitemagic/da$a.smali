.class public final Lcom/siimkinks/sqlitemagic/da$a;
.super Ljava/lang/Object;
.source "SqliteMagic_PaymentExecutionData_Handler.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/a/c;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/da;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/siimkinks/sqlitemagic/a/c;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/transfer/payment/d;

.field private b:Lcom/siimkinks/sqlitemagic/as;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/d;)V
    .locals 1

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 165
    iput v0, p0, Lcom/siimkinks/sqlitemagic/da$a;->c:I

    .line 168
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/da$a;->a:Lcom/swedbank/mobile/data/transfer/payment/d;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lcom/siimkinks/sqlitemagic/da$a;
    .locals 1

    .line 173
    new-instance v0, Lcom/siimkinks/sqlitemagic/da$a;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/da$a;-><init>(Lcom/swedbank/mobile/data/transfer/payment/d;)V

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 6

    .line 193
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/da$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/da$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v1, "Transfer"

    const/4 v2, 0x2

    .line 194
    invoke-virtual {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v1

    .line 195
    new-instance v2, Lcom/siimkinks/sqlitemagic/bo;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/da$a;->c:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, v5, v4}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    .line 197
    :try_start_0
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/da$a;->a:Lcom/swedbank/mobile/data/transfer/payment/d;

    invoke-static {v3, v1, v2}, Lcom/siimkinks/sqlitemagic/da;->a(Lcom/swedbank/mobile/data/transfer/payment/d;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/bo;)J

    move-result-wide v3

    .line 198
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bu;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return-wide v3

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 201
    :try_start_1
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "Operation failed"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    const-wide/16 v0, -0x1

    .line 204
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return-wide v0

    :goto_1
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    .line 205
    throw v0
.end method

.method public b()Ljava/lang/Long;
    .locals 4

    .line 210
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/da$a;->a()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 211
    iget v2, p0, Lcom/siimkinks/sqlitemagic/da$a;->c:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 212
    :cond_0
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to insert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/da$a;->a:Lcom/swedbank/mobile/data/transfer/payment/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_1
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/da$a;->b()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
