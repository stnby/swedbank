.class final Lcom/siimkinks/sqlitemagic/cf;
.super Lcom/siimkinks/sqlitemagic/dl;
.source "SelectionTable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/dl<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[Ljava/lang/String;

.field private final c:[Ljava/lang/String;

.field private final d:Lcom/siimkinks/sqlitemagic/ca$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ca$b<",
            "TT;>;"
        }
    .end annotation
.end field


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Z)Lcom/siimkinks/sqlitemagic/ca$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/siimkinks/sqlitemagic/ca$b<",
            "TT;>;"
        }
    .end annotation

    .line 89
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cf;->d:Lcom/siimkinks/sqlitemagic/ca$b;

    return-object p1
.end method

.method a(Lcom/siimkinks/sqlitemagic/cc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cc<",
            "*>;)V"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cf;->b:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 58
    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cf;->b:[Ljava/lang/String;

    invoke-static {p1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    const/16 v0, 0x28

    .line 64
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 65
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x29

    .line 66
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/cf;->A:Z

    if-eqz v0, :cond_0

    const-string v0, " AS "

    .line 68
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cf;->x:Ljava/lang/String;

    .line 69
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method a(Ljava/util/ArrayList;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 75
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cf;->c:[Ljava/lang/String;

    .line 76
    array-length p3, p2

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p3, :cond_1

    .line 78
    aget-object v2, p2, v1

    .line 79
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 80
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method
