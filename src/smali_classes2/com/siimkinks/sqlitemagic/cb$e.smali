.class public final Lcom/siimkinks/sqlitemagic/cb$e;
.super Lcom/siimkinks/sqlitemagic/cd$a;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/cd$a<",
        "TT;TS;TN;>;"
    }
.end annotation


# instance fields
.field private final a:[Lcom/siimkinks/sqlitemagic/cb$f;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/cd$a;[Lcom/siimkinks/sqlitemagic/cb$f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd$a<",
            "TT;TS;TN;>;[",
            "Lcom/siimkinks/sqlitemagic/cb$f;",
            ")V"
        }
    .end annotation

    .line 1063
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/cd$a;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    .line 1064
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$e;->a:[Lcom/siimkinks/sqlitemagic/cb$f;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/siimkinks/sqlitemagic/cb$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/siimkinks/sqlitemagic/cb$d<",
            "TT;TS;TN;>;"
        }
    .end annotation

    .line 1100
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$d;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$d;-><init>(Lcom/siimkinks/sqlitemagic/cd$a;Ljava/lang/String;)V

    return-object v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 4

    const-string v0, "ORDER BY "

    .line 1069
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1070
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$e;->a:[Lcom/siimkinks/sqlitemagic/cb$f;

    .line 1071
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    if-lez v2, :cond_0

    const/16 v3, 0x2c

    .line 1073
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1075
    :cond_0
    aget-object v3, v0, v2

    invoke-virtual {v3, p1}, Lcom/siimkinks/sqlitemagic/cb$f;->a(Ljava/lang/StringBuilder;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "ORDER BY "

    .line 1081
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1082
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$e;->a:[Lcom/siimkinks/sqlitemagic/cb$f;

    .line 1083
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    if-lez v2, :cond_0

    const/16 v3, 0x2c

    .line 1085
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1087
    :cond_0
    aget-object v3, v0, v2

    invoke-virtual {v3, p1, p2}, Lcom/siimkinks/sqlitemagic/cb$f;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
