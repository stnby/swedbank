.class public final Lcom/siimkinks/sqlitemagic/cs$a;
.super Ljava/lang/Object;
.source "SqliteMagic_BalanceData_Handler.java"

# interfaces
.implements Lio/reactivex/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/account/i;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/siimkinks/sqlitemagic/as;

.field private c:I


# direct methods
.method private constructor <init>(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/account/i;",
            ">;)V"
        }
    .end annotation

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 208
    iput v0, p0, Lcom/siimkinks/sqlitemagic/cs$a;->c:I

    .line 211
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cs$a;->a:Ljava/lang/Iterable;

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cs$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/swedbank/mobile/data/account/i;",
            ">;)",
            "Lcom/siimkinks/sqlitemagic/cs$a;"
        }
    .end annotation

    .line 216
    new-instance v0, Lcom/siimkinks/sqlitemagic/cs$a;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/cs$a;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method


# virtual methods
.method public a(Lio/reactivex/c;)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 276
    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/cs$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/siimkinks/sqlitemagic/cs$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v3, "Core"

    const/4 v4, 0x6

    .line 277
    invoke-virtual {v0, v3, v4}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v3

    .line 278
    new-instance v4, Lcom/siimkinks/sqlitemagic/bo;

    iget v5, v1, Lcom/siimkinks/sqlitemagic/cs$a;->c:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v4, v5, v7, v6}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    .line 279
    invoke-static {}, Lio/reactivex/b/d;->a()Lio/reactivex/b/c;

    move-result-object v5

    .line 280
    invoke-interface {v2, v5}, Lio/reactivex/c;->a(Lio/reactivex/b/c;)V

    const-string v6, "balance_data"

    const-string v8, "INSERT%s INTO balance_data (account_id, customer_id, currency, available_funds, credit, deposit, reserved, is_credit_account) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

    .line 281
    invoke-virtual {v4, v6, v8, v3}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v3

    .line 283
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v6

    .line 286
    :try_start_0
    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 287
    :try_start_1
    iget-object v8, v1, Lcom/siimkinks/sqlitemagic/cs$a;->a:Ljava/lang/Iterable;

    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    const/4 v9, 0x0

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/swedbank/mobile/data/account/i;

    .line 288
    sget-boolean v11, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v12, 0x1

    if-eqz v11, :cond_1

    const-string v11, "INSERT\n  table: balance_data\n  object: %s"

    new-array v13, v12, [Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/swedbank/mobile/data/account/i;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v7

    invoke-static {v11, v13}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    :cond_1
    invoke-interface {v5}, Lio/reactivex/b/c;->b()Z

    move-result v11

    if-nez v11, :cond_5

    .line 292
    invoke-static {v3, v10}, Lcom/swedbank/mobile/data/account/q;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/i;)V

    .line 293
    invoke-interface {v3}, Landroidx/k/a/f;->b()J

    move-result-wide v13

    .line 294
    sget-boolean v11, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v11, :cond_2

    const-string v11, "INSERT id: %s"

    new-array v15, v12, [Ljava/lang/Object;

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v15, v7

    invoke-static {v11, v15}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    const-wide/16 v15, -0x1

    cmp-long v11, v13, v15

    if-nez v11, :cond_4

    .line 296
    iget-boolean v11, v4, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v11, :cond_3

    goto :goto_1

    .line 297
    :cond_3
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to insert "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v9, 0x1

    goto :goto_1

    .line 290
    :cond_5
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    const-string v7, "Subscriber unsubscribed unexpectedly"

    invoke-direct {v0, v7}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_6
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 304
    :try_start_2
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 311
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v9, :cond_8

    .line 313
    sget-object v3, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/j;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    .line 303
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 307
    :try_start_5
    invoke-interface {v5}, Lio/reactivex/b/c;->b()Z

    move-result v3

    if-nez v3, :cond_7

    .line 308
    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 311
    :cond_7
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 315
    :cond_8
    :goto_2
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 317
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return-void

    .line 311
    :goto_3
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 315
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 316
    throw v0
.end method

.method public a()Z
    .locals 14

    .line 236
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cs$a;->b:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cs$a;->b:Lcom/siimkinks/sqlitemagic/as;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    :goto_0
    const-string v1, "Core"

    const/4 v2, 0x6

    .line 237
    invoke-virtual {v0, v1, v2}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;I)Lcom/siimkinks/sqlitemagic/ba;

    move-result-object v1

    .line 238
    new-instance v2, Lcom/siimkinks/sqlitemagic/bo;

    iget v3, p0, Lcom/siimkinks/sqlitemagic/cs$a;->c:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, v5, v4}, Lcom/siimkinks/sqlitemagic/bo;-><init>(IILjava/util/ArrayList;)V

    const-string v3, "balance_data"

    const-string v4, "INSERT%s INTO balance_data (account_id, customer_id, currency, available_funds, credit, deposit, reserved, is_credit_account) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

    .line 239
    invoke-virtual {v2, v3, v4, v1}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v1

    .line 241
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v3

    .line 244
    :try_start_0
    monitor-enter v1
    :try_end_0
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 245
    :try_start_1
    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/cs$a;->a:Ljava/lang/Iterable;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v6, 0x0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/swedbank/mobile/data/account/i;

    .line 246
    sget-boolean v8, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v9, 0x1

    if-eqz v8, :cond_1

    const-string v8, "INSERT\n  table: balance_data\n  object: %s"

    new-array v10, v9, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/account/i;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v5

    invoke-static {v8, v10}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    :cond_1
    invoke-static {v1, v7}, Lcom/swedbank/mobile/data/account/q;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/i;)V

    .line 248
    invoke-interface {v1}, Landroidx/k/a/f;->b()J

    move-result-wide v10

    .line 249
    sget-boolean v8, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v8, :cond_2

    const-string v8, "INSERT id: %s"

    new-array v12, v9, [Ljava/lang/Object;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v12, v5

    invoke-static {v8, v12}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    const-wide/16 v12, -0x1

    cmp-long v8, v10, v12

    if-nez v8, :cond_4

    .line 251
    iget-boolean v8, v2, Lcom/siimkinks/sqlitemagic/bo;->a:Z

    if-eqz v8, :cond_3

    goto :goto_1

    .line 252
    :cond_3
    new-instance v0, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to insert "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v6, 0x1

    goto :goto_1

    .line 258
    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 259
    :try_start_2
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_2
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 266
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    if-eqz v6, :cond_6

    .line 268
    sget-object v1, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/j;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    .line 270
    :cond_6
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v6

    :catchall_0
    move-exception v0

    .line 258
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Lcom/siimkinks/sqlitemagic/exception/OperationFailedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 263
    :try_start_5
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_7

    const-string v1, "Operation failed"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 266
    :cond_7
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 270
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    return v5

    .line 266
    :goto_2
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 270
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bo;->close()V

    .line 271
    throw v0
.end method
