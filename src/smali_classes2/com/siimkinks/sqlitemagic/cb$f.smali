.class public final Lcom/siimkinks/sqlitemagic/cb$f;
.super Lcom/siimkinks/sqlitemagic/ck;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# instance fields
.field private final a:Lcom/siimkinks/sqlitemagic/x;

.field private final b:Lcom/siimkinks/sqlitemagic/bb;

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/x;Lcom/siimkinks/sqlitemagic/bb;Ljava/lang/String;)V
    .locals 0

    .line 1015
    invoke-direct {p0}, Lcom/siimkinks/sqlitemagic/ck;-><init>()V

    .line 1016
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$f;->a:Lcom/siimkinks/sqlitemagic/x;

    .line 1017
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$f;->b:Lcom/siimkinks/sqlitemagic/bb;

    .line 1018
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/cb$f;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 1023
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->a:Lcom/siimkinks/sqlitemagic/x;

    if-eqz v0, :cond_0

    .line 1024
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;)V

    goto :goto_0

    .line 1025
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->b:Lcom/siimkinks/sqlitemagic/bb;

    if-eqz v0, :cond_2

    .line 1026
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->b:Lcom/siimkinks/sqlitemagic/bb;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/lang/StringBuilder;)V

    .line 1030
    :goto_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1031
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    return-void

    .line 1028
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Ordering term must have either column or expr"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 1037
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->a:Lcom/siimkinks/sqlitemagic/x;

    if-eqz v0, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->a:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0, p1, p2}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    goto :goto_0

    .line 1039
    :cond_0
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->b:Lcom/siimkinks/sqlitemagic/bb;

    if-eqz v0, :cond_2

    .line 1040
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$f;->b:Lcom/siimkinks/sqlitemagic/bb;

    invoke-virtual {v0, p1, p2}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    .line 1044
    :goto_0
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$f;->c:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 1045
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$f;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    return-void

    .line 1042
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ordering term must have either column or expr"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
