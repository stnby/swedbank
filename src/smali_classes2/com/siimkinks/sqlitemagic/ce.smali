.class final Lcom/siimkinks/sqlitemagic/ce;
.super Lcom/siimkinks/sqlitemagic/bn;
.source "SelectionColumn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        "ET:",
        "Ljava/lang/Object;",
        "P:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/bn<",
        "TT;TR;TET;TP;TN;>;"
    }
.end annotation


# instance fields
.field private final h:Lcom/siimkinks/sqlitemagic/cc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/cc<",
            "*>;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z


# direct methods
.method private constructor <init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/cc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TP;>;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/siimkinks/sqlitemagic/eb$a<",
            "*>;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/cc<",
            "*>;)V"
        }
    .end annotation

    .line 35
    invoke-direct/range {p0 .. p7}, Lcom/siimkinks/sqlitemagic/bn;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 29
    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/ce;->j:Z

    .line 36
    iput-object p8, p0, Lcom/siimkinks/sqlitemagic/ce;->h:Lcom/siimkinks/sqlitemagic/cc;

    return-void
.end method

.method static a(Lcom/siimkinks/sqlitemagic/cc;Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/ce;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "N:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/siimkinks/sqlitemagic/cc<",
            "*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/ce<",
            "TT;TT;TT;*TN;>;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cc;->e:Lcom/siimkinks/sqlitemagic/cb$g;

    .line 44
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/cc;->d:Lcom/siimkinks/sqlitemagic/cb$a;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 47
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/cc;->c:Lcom/siimkinks/sqlitemagic/cb$c;

    iget-object v3, v1, Lcom/siimkinks/sqlitemagic/cb$c;->a:Lcom/siimkinks/sqlitemagic/dl;

    .line 48
    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cb$g;->a:Lcom/siimkinks/sqlitemagic/x;

    .line 49
    new-instance v1, Lcom/siimkinks/sqlitemagic/ce;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/siimkinks/sqlitemagic/x;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v7, v0, Lcom/siimkinks/sqlitemagic/x;->f:Z

    move-object v2, v1

    move-object v4, p1

    move-object v8, p1

    move-object v9, p1

    move-object v10, p0

    invoke-direct/range {v2 .. v10}, Lcom/siimkinks/sqlitemagic/ce;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/cc;)V

    return-object v1

    .line 45
    :cond_0
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Only a single result allowed for a SELECT that is part of a column"

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;I)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "I)I"
        }
    .end annotation

    .line 96
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/ce;->a(Ljava/lang/StringBuilder;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/ce;->b(Ljava/lang/StringBuilder;)V

    .line 98
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/ce;->b:Ljava/lang/String;

    invoke-static {p1, p2, p3, p0}, Lcom/siimkinks/sqlitemagic/ce;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    const/4 p1, 0x1

    .line 99
    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/ce;->j:Z

    add-int/2addr p3, p1

    return p3
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;I)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;I)I"
        }
    .end annotation

    .line 108
    invoke-virtual {p0, p2, p3}, Lcom/siimkinks/sqlitemagic/ce;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    .line 109
    invoke-virtual {p0, p2}, Lcom/siimkinks/sqlitemagic/ce;->b(Ljava/lang/StringBuilder;)V

    .line 110
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/ce;->b:Ljava/lang/String;

    invoke-static {p1, p2, p4, p0}, Lcom/siimkinks/sqlitemagic/ce;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;ILcom/siimkinks/sqlitemagic/x;)V

    const/4 p1, 0x1

    .line 111
    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/ce;->j:Z

    add-int/2addr p4, p1

    return p4
.end method

.method a(Lcom/siimkinks/sqlitemagic/b/d;)V
    .locals 0

    return-void
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 2

    .line 54
    iget-boolean v0, p0, Lcom/siimkinks/sqlitemagic/ce;->j:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/ce;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :cond_0
    const/16 v0, 0x28

    .line 58
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 59
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ce;->h:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ce;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v1}, Lcom/siimkinks/sqlitemagic/cc;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    const/16 v0, 0x29

    .line 60
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 65
    iget-boolean p2, p0, Lcom/siimkinks/sqlitemagic/ce;->j:Z

    if-eqz p2, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/ce;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :cond_0
    const/16 p2, 0x28

    .line 69
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 70
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/ce;->h:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ce;->i:Ljava/util/ArrayList;

    invoke-virtual {p2, p1, v0}, Lcom/siimkinks/sqlitemagic/cc;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    const/16 p2, 0x29

    .line 71
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ce;->h:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/ce;->c(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bn;

    move-result-object p1

    return-object p1
.end method

.method b(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 89
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/ce;->i:Ljava/util/ArrayList;

    return-void
.end method

.method public c(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bn;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/siimkinks/sqlitemagic/bn<",
            "TT;TR;TET;TP;TN;>;"
        }
    .end annotation

    .line 118
    new-instance v9, Lcom/siimkinks/sqlitemagic/ce;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ce;->a:Lcom/siimkinks/sqlitemagic/dl;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ce;->b:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/siimkinks/sqlitemagic/ce;->c:Z

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/ce;->e:Lcom/siimkinks/sqlitemagic/eb$a;

    iget-boolean v5, p0, Lcom/siimkinks/sqlitemagic/ce;->f:Z

    iget-object v7, p0, Lcom/siimkinks/sqlitemagic/ce;->d:Ljava/lang/String;

    iget-object v8, p0, Lcom/siimkinks/sqlitemagic/ce;->h:Lcom/siimkinks/sqlitemagic/cc;

    move-object v0, v9

    move-object v6, p1

    invoke-direct/range {v0 .. v8}, Lcom/siimkinks/sqlitemagic/ce;-><init>(Lcom/siimkinks/sqlitemagic/dl;Ljava/lang/String;ZLcom/siimkinks/sqlitemagic/eb$a;ZLjava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/cc;)V

    return-object v9
.end method
