.class public final Lcom/siimkinks/sqlitemagic/cb$a;
.super Lcom/siimkinks/sqlitemagic/cd;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/siimkinks/sqlitemagic/cd<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final a:[Lcom/siimkinks/sqlitemagic/x;

.field b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/cd;[Lcom/siimkinks/sqlitemagic/x;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd<",
            "Ljava/lang/Object;",
            ">;[",
            "Lcom/siimkinks/sqlitemagic/x<",
            "*****>;)V"
        }
    .end annotation

    .line 264
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/cd;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    .line 265
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$a;->a:[Lcom/siimkinks/sqlitemagic/x;

    .line 266
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    iput-object p0, p1, Lcom/siimkinks/sqlitemagic/cc;->d:Lcom/siimkinks/sqlitemagic/cb$a;

    .line 267
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    .line 268
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    .line 269
    array-length v1, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 270
    aget-object v3, p2, v2

    .line 271
    invoke-virtual {v3, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/util/ArrayList;)V

    .line 272
    invoke-virtual {v3, v0}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method a(Lcom/siimkinks/sqlitemagic/b/c;)Lcom/siimkinks/sqlitemagic/b/c;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$a;->a:[Lcom/siimkinks/sqlitemagic/x;

    .line 301
    array-length v1, v0

    if-nez v1, :cond_0

    const-string p1, "*"

    .line 303
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$a;->b:Ljava/lang/String;

    .line 304
    new-instance p1, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {p1}, Lcom/siimkinks/sqlitemagic/b/c;-><init>()V

    return-object p1

    .line 306
    :cond_0
    new-instance v2, Lcom/siimkinks/sqlitemagic/b/c;

    invoke-direct {v2, v1}, Lcom/siimkinks/sqlitemagic/b/c;-><init>(I)V

    .line 307
    new-instance v3, Ljava/lang/StringBuilder;

    mul-int/lit8 v4, v1, 0xc

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    :goto_0
    if-ge v4, v1, :cond_3

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    const/16 v8, 0x2c

    .line 314
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    if-eqz p1, :cond_2

    .line 317
    aget-object v8, v0, v4

    invoke-virtual {v8, v2, v3, p1, v7}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;I)I

    move-result v7

    goto :goto_2

    .line 319
    :cond_2
    aget-object v8, v0, v4

    invoke-virtual {v8, v2, v3, v7}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;I)I

    move-result v7

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 322
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$a;->b:Ljava/lang/String;

    return-object v2
.end method

.method a()Lcom/siimkinks/sqlitemagic/b/d;
    .locals 5

    .line 286
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$a;->a:[Lcom/siimkinks/sqlitemagic/x;

    .line 287
    array-length v1, v0

    if-lez v1, :cond_1

    .line 289
    new-instance v2, Lcom/siimkinks/sqlitemagic/b/d;

    invoke-direct {v2, v1}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    .line 291
    aget-object v4, v0, v3

    invoke-virtual {v4, v2}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/b/d;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v2

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;)",
            "Lcom/siimkinks/sqlitemagic/cb$c<",
            "TT;TT;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 346
    new-instance v0, Lcom/siimkinks/sqlitemagic/cb$c;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;-><init>(Lcom/siimkinks/sqlitemagic/cd;Lcom/siimkinks/sqlitemagic/dl;)V

    return-object v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 333
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$a;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method
