.class public final Lcom/siimkinks/sqlitemagic/ah;
.super Ljava/lang/Object;
.source "CompiledUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/ah$a;
    }
.end annotation


# instance fields
.field private final a:Landroidx/k/a/f;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/siimkinks/sqlitemagic/as;


# direct methods
.method constructor <init>(Landroidx/k/a/f;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/as;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/ah;->a:Landroidx/k/a/f;

    .line 27
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ah;->b:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/ah;->c:Lcom/siimkinks/sqlitemagic/as;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .line 41
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ah;->a:Landroidx/k/a/f;

    monitor-enter v0

    .line 42
    :try_start_0
    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ah;->a:Landroidx/k/a/f;

    invoke-interface {v1}, Landroidx/k/a/f;->a()I

    move-result v1

    .line 43
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ah;->c:Lcom/siimkinks/sqlitemagic/as;

    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ah;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/as;->b(Ljava/lang/String;)V

    :cond_0
    return v1

    :catchall_0
    move-exception v1

    .line 43
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public b()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 61
    new-instance v0, Lcom/siimkinks/sqlitemagic/ah$1;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/ah$1;-><init>(Lcom/siimkinks/sqlitemagic/ah;)V

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
