.class public final Lcom/siimkinks/sqlitemagic/w;
.super Ljava/lang/Object;
.source "CardsGeneratedClassesManager.java"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 0

    const/4 p0, 0x2

    return p0
.end method

.method public static a(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method

.method public static b(Landroidx/k/a/b;)V
    .locals 3

    .line 26
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 28
    :try_start_0
    sget-boolean v1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Creating tables"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v1, "CREATE TABLE IF NOT EXISTS card_data (id INTEGER PRIMARY KEY AUTOINCREMENT, card_id TEXT UNIQUE, customer_id TEXT DEFAULT \'\', account_id TEXT DEFAULT \'\', number TEXT DEFAULT \'\', bin TEXT DEFAULT \'\', valid_until INTEGER DEFAULT 0, card_holder_name TEXT DEFAULT \'\', parent_type INTEGER DEFAULT 0, type TEXT DEFAULT \'\', type_id TEXT DEFAULT \'\', contactless INTEGER DEFAULT 0, contactless_allowed INTEGER DEFAULT 0, contactless_enabled INTEGER DEFAULT 0, state INTEGER DEFAULT 0, digitizable INTEGER DEFAULT 0, digitization_progression INTEGER DEFAULT 0, digitized_card_id TEXT DEFAULT NULL, delivery_method TEXT DEFAULT NULL, internet_shopping_enabled INTEGER DEFAULT NULL)"

    .line 29
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE IF NOT EXISTS card_limit_data (local_id INTEGER PRIMARY KEY AUTOINCREMENT, card_id TEXT DEFAULT \'\', active INTEGER DEFAULT 0, card_limit TEXT DEFAULT \'\', limit_maximum INTEGER DEFAULT 0, limit_currency TEXT DEFAULT \'\', service TEXT DEFAULT \'\', validity TEXT DEFAULT \'\')"

    .line 30
    invoke-interface {p0, v1}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 31
    invoke-interface {p0}, Landroidx/k/a/b;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 33
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35
    :cond_1
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-void

    :goto_1
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 36
    throw v0
.end method

.method public static c(Landroidx/k/a/b;)Lcom/siimkinks/sqlitemagic/b/d;
    .locals 4

    .line 41
    invoke-interface {p0}, Landroidx/k/a/b;->a()V

    const/4 v0, 0x0

    .line 43
    :try_start_0
    new-instance v1, Lcom/siimkinks/sqlitemagic/b/d;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/w;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;-><init>(I)V

    .line 44
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "Clearing data"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v2, "DELETE FROM card_data"

    .line 45
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    const-string v2, "DELETE FROM card_limit_data"

    .line 46
    invoke-interface {p0, v2}, Landroidx/k/a/b;->c(Ljava/lang/String;)V

    .line 47
    invoke-interface {p0}, Landroidx/k/a/b;->c()V

    const-string v2, "card_data"

    const-string v3, "card_limit_data"

    .line 48
    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/d;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 51
    :try_start_1
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Error while executing db transaction"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    :cond_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54
    :goto_0
    invoke-interface {p0}, Landroidx/k/a/b;->b()V

    .line 55
    throw v0
.end method

.method public static d(Landroidx/k/a/b;)V
    .locals 0

    return-void
.end method
