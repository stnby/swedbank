.class final Lcom/siimkinks/sqlitemagic/cl;
.super Ljava/lang/Object;
.source "SqlCreator.java"


# direct methods
.method static a(Lcom/siimkinks/sqlitemagic/cm;I)Ljava/lang/String;
    .locals 1

    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    mul-int/lit8 p1, p1, 0x14

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 13
    invoke-static {p0, v0}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;)V

    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Lcom/siimkinks/sqlitemagic/cm;ILcom/siimkinks/sqlitemagic/b/c;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cm;",
            "I",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    mul-int/lit8 p1, p1, 0x14

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 38
    invoke-static {p0, p2, v0}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;)V

    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Lcom/siimkinks/sqlitemagic/cm;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cm;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cm;->d:Lcom/siimkinks/sqlitemagic/cm;

    if-eqz v0, :cond_0

    .line 47
    invoke-static {v0, p1, p2}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/StringBuilder;)V

    .line 52
    invoke-static {p0, p2, p1}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    return-void

    .line 49
    :cond_0
    invoke-static {p0, p2, p1}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    return-void
.end method

.method static a(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;)V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cm;->d:Lcom/siimkinks/sqlitemagic/cm;

    if-eqz v0, :cond_0

    .line 20
    invoke-static {v0, p1}, Lcom/siimkinks/sqlitemagic/cl;->a(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;)V

    .line 25
    invoke-static {p0, p1}, Lcom/siimkinks/sqlitemagic/cl;->b(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;)V

    return-void

    .line 22
    :cond_0
    invoke-static {p0, p1}, Lcom/siimkinks/sqlitemagic/cl;->b(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method private static a(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cm;",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 57
    invoke-virtual {p0, p1, p2}, Lcom/siimkinks/sqlitemagic/cm;->a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V

    const/16 p0, 0x20

    .line 58
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static b(Lcom/siimkinks/sqlitemagic/cm;Ljava/lang/StringBuilder;)V
    .locals 0

    .line 29
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/cm;->a(Ljava/lang/StringBuilder;)V

    const/16 p0, 0x20

    .line 30
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-void
.end method
