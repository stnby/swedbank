.class public final Lcom/siimkinks/sqlitemagic/ct;
.super Ljava/lang/Object;
.source "SqliteMagic_CardData_Handler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/ct$b;,
        Lcom/siimkinks/sqlitemagic/ct$a;,
        Lcom/siimkinks/sqlitemagic/ct$c;
    }
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/data/cards/a;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/ec;Ljava/util/ArrayList;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/cards/a;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/ba;",
            "Lcom/siimkinks/sqlitemagic/ec;",
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;)J"
        }
    .end annotation

    .line 89
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const-string v0, "PERSIST\n  table: card_data\n  object: %s"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :cond_0
    invoke-static {p0, p1}, Lcom/swedbank/mobile/data/cards/p;->a(Lcom/swedbank/mobile/data/cards/a;Lcom/siimkinks/sqlitemagic/b/c;)V

    const-string v0, "card_data"

    .line 92
    invoke-static {v0, p4}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object p4

    if-eqz p4, :cond_1

    .line 95
    iget-object p4, p4, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    :goto_0
    move-object v8, p4

    goto :goto_1

    :cond_1
    const-string p4, "id"

    goto :goto_0

    :goto_1
    const/4 v4, 0x1

    const-string v5, "card_data"

    const/16 v6, 0x14

    move-object v3, p3

    move-object v7, p1

    move-object v9, p2

    .line 99
    invoke-virtual/range {v3 .. v9}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object p4

    invoke-interface {p4}, Landroidx/k/a/f;->a()I

    move-result p4

    if-gtz p4, :cond_5

    .line 101
    sget-boolean p4, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p4, :cond_2

    const-string p4, "PERSIST update failed; trying insertion"

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p4, v0}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    const-string p4, "id"

    .line 102
    invoke-virtual {p1, p4}, Lcom/siimkinks/sqlitemagic/b/c;->d(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "card_data"

    const/16 v6, 0x14

    const-string v8, "id"

    move-object v3, p3

    move-object v7, p1

    move-object v9, p2

    .line 104
    invoke-virtual/range {v3 .. v9}, Lcom/siimkinks/sqlitemagic/ec;->a(ILjava/lang/String;ILcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object p1

    invoke-interface {p1}, Landroidx/k/a/f;->b()J

    move-result-wide p1

    .line 105
    sget-boolean p3, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p3, :cond_3

    const-string p3, "PERSIST insert id: %s"

    new-array p4, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p4, v2

    invoke-static {p3, p4}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    const-wide/16 p3, -0x1

    cmp-long p1, p1, p3

    if-eqz p1, :cond_4

    goto :goto_2

    .line 107
    :cond_4
    new-instance p1, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Failed to persist "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 110
    :cond_5
    :goto_2
    invoke-static {p0}, Lcom/swedbank/mobile/data/cards/p;->a(Lcom/swedbank/mobile/data/cards/a;)Ljava/lang/Long;

    move-result-object p0

    if-eqz p0, :cond_6

    .line 112
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    return-wide p0

    :cond_6
    const-wide/16 p0, -0x2

    return-wide p0
.end method

.method public static a(Lcom/swedbank/mobile/data/cards/a;Lcom/siimkinks/sqlitemagic/ba;Lcom/siimkinks/sqlitemagic/bo;Ljava/util/ArrayList;)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/cards/a;",
            "Lcom/siimkinks/sqlitemagic/ba;",
            "Lcom/siimkinks/sqlitemagic/bo;",
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/x;",
            ">;)J"
        }
    .end annotation

    .line 119
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const-string v0, "PERSIST\n  table: card_data\n  object: %s"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v0, "card_data"

    .line 121
    invoke-static {v0, p3}, Lcom/siimkinks/sqlitemagic/cn;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 124
    iget-object p3, p3, Lcom/siimkinks/sqlitemagic/x;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string p3, "id"

    .line 128
    :goto_0
    invoke-static {p3, p0}, Lcom/swedbank/mobile/data/cards/p;->a(Ljava/lang/String;Lcom/swedbank/mobile/data/cards/a;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "card_data"

    const-string v3, "UPDATE%s card_data SET card_id=?, customer_id=?, account_id=?, number=?, bin=?, valid_until=?, card_holder_name=?, parent_type=?, type=?, type_id=?, contactless=?, contactless_allowed=?, contactless_enabled=?, state=?, digitizable=?, digitization_progression=?, digitized_card_id=?, delivery_method=?, internet_shopping_enabled=? WHERE id=?"

    .line 129
    invoke-virtual {p2, v0, v3, p1}, Lcom/siimkinks/sqlitemagic/bo;->b(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object v0

    .line 130
    monitor-enter v0

    .line 131
    :try_start_0
    invoke-static {v0, p0}, Lcom/swedbank/mobile/data/cards/p;->b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/cards/a;)V

    const/16 v3, 0x14

    .line 132
    invoke-static {v0, v3, p3, p0}, Lcom/swedbank/mobile/data/cards/p;->a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/cards/a;)V

    .line 133
    invoke-interface {v0}, Landroidx/k/a/f;->a()I

    move-result p3

    .line 134
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_2
    const/4 p3, 0x0

    :goto_1
    if-gtz p3, :cond_6

    .line 137
    sget-boolean p3, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p3, :cond_3

    const-string p3, "PERSIST update failed; trying insertion"

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p3, v0}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    const-string p3, "card_data"

    const-string v0, "INSERT%s INTO card_data (card_id, customer_id, account_id, number, bin, valid_until, card_holder_name, parent_type, type, type_id, contactless, contactless_allowed, contactless_enabled, state, digitizable, digitization_progression, digitized_card_id, delivery_method, internet_shopping_enabled) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 139
    invoke-virtual {p2, p3, v0, p1}, Lcom/siimkinks/sqlitemagic/bo;->a(Ljava/lang/String;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ba;)Landroidx/k/a/f;

    move-result-object p1

    .line 140
    monitor-enter p1

    .line 141
    :try_start_1
    invoke-static {p1, p0}, Lcom/swedbank/mobile/data/cards/p;->a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/cards/a;)V

    .line 142
    invoke-interface {p1}, Landroidx/k/a/f;->b()J

    move-result-wide p2

    .line 143
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 144
    sget-boolean p1, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz p1, :cond_4

    const-string p1, "INSERT id: %s"

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/siimkinks/sqlitemagic/bk;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    const-wide/16 v0, -0x1

    cmp-long p1, p2, v0

    if-eqz p1, :cond_5

    return-wide p2

    .line 146
    :cond_5
    new-instance p1, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Failed to insert "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/siimkinks/sqlitemagic/exception/OperationFailedException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_1
    move-exception p0

    .line 143
    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p0

    .line 150
    :cond_6
    invoke-static {p0}, Lcom/swedbank/mobile/data/cards/p;->a(Lcom/swedbank/mobile/data/cards/a;)Ljava/lang/Long;

    move-result-object p0

    if-eqz p0, :cond_7

    .line 152
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    return-wide p0

    :cond_7
    const-wide/16 p0, -0x2

    return-wide p0
.end method
