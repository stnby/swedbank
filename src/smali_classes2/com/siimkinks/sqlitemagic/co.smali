.class public final Lcom/siimkinks/sqlitemagic/co;
.super Ljava/lang/Object;
.source "SqliteMagic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/siimkinks/sqlitemagic/co$a;,
        Lcom/siimkinks/sqlitemagic/co$b;
    }
.end annotation


# static fields
.field static a:Z = false

.field static b:Lcom/siimkinks/sqlitemagic/bl;


# instance fields
.field c:Lcom/siimkinks/sqlitemagic/as;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/app/Application;Lcom/siimkinks/sqlitemagic/co$a;)Lcom/siimkinks/sqlitemagic/as;
    .locals 5

    .line 83
    iget-object v0, p1, Lcom/siimkinks/sqlitemagic/co$a;->b:Landroidx/k/a/c$c;

    if-eqz v0, :cond_2

    .line 88
    :try_start_0
    iget-object v1, p1, Lcom/siimkinks/sqlitemagic/co$a;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cn;->c()Ljava/lang/String;

    move-result-object v1

    .line 92
    :cond_1
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cn;->b()I

    move-result v2

    .line 93
    new-instance v3, Lcom/siimkinks/sqlitemagic/aq;

    iget-object v4, p1, Lcom/siimkinks/sqlitemagic/co$a;->c:Lcom/siimkinks/sqlitemagic/at;

    invoke-direct {v3, p0, v2, v4}, Lcom/siimkinks/sqlitemagic/aq;-><init>(Landroid/content/Context;ILcom/siimkinks/sqlitemagic/at;)V

    .line 95
    invoke-static {p0}, Landroidx/k/a/c$b;->a(Landroid/content/Context;)Landroidx/k/a/c$b$a;

    move-result-object p0

    .line 96
    invoke-virtual {p0, v1}, Landroidx/k/a/c$b$a;->a(Ljava/lang/String;)Landroidx/k/a/c$b$a;

    move-result-object p0

    .line 97
    invoke-virtual {p0, v3}, Landroidx/k/a/c$b$a;->a(Landroidx/k/a/c$a;)Landroidx/k/a/c$b$a;

    move-result-object p0

    .line 98
    invoke-virtual {p0}, Landroidx/k/a/c$b$a;->a()Landroidx/k/a/c$b;

    move-result-object p0

    .line 99
    invoke-interface {v0, p0}, Landroidx/k/a/c$c;->a(Landroidx/k/a/c$b;)Landroidx/k/a/c;

    move-result-object p0

    const-string v0, "Initializing database with [name=%s, version=%s, logging=%s]"

    const/4 v3, 0x3

    .line 100
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    .line 101
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x2

    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v1

    .line 100
    invoke-static {v0, v3}, Lcom/siimkinks/sqlitemagic/bk;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    new-instance v0, Lcom/siimkinks/sqlitemagic/as;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/co$a;->d:Lio/reactivex/v;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/as;-><init>(Landroidx/k/a/c;Lio/reactivex/v;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    .line 104
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Error initializing database. Make sure there is at least one model annotated with @Table"

    invoke-direct {p1, v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    .line 85
    :cond_2
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "SQLite Factory cannot be null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Landroid/app/Application;)Lcom/siimkinks/sqlitemagic/co$a;
    .locals 1

    .line 78
    new-instance v0, Lcom/siimkinks/sqlitemagic/co$a;

    invoke-direct {v0, p0}, Lcom/siimkinks/sqlitemagic/co$a;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method public static a()Lcom/siimkinks/sqlitemagic/dm;
    .locals 1

    .line 39
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/as;->b()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/siimkinks/sqlitemagic/ar;
    .locals 1

    .line 48
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->c()Lcom/siimkinks/sqlitemagic/as;

    move-result-object v0

    return-object v0
.end method

.method static c()Lcom/siimkinks/sqlitemagic/as;
    .locals 2

    .line 53
    sget-object v0, Lcom/siimkinks/sqlitemagic/co$b;->a:Lcom/siimkinks/sqlitemagic/co;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/co;->c:Lcom/siimkinks/sqlitemagic/as;

    if-eqz v0, :cond_0

    return-object v0

    .line 55
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Looks like SqliteMagic is not initialized..."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
