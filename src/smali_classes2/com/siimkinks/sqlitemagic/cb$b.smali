.class public final Lcom/siimkinks/sqlitemagic/cb$b;
.super Lcom/siimkinks/sqlitemagic/cd$a;
.source "Select.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/cb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/cd$a<",
        "TT;TS;TN;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/siimkinks/sqlitemagic/cd$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/cd$a<",
            "*TS;*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/cd$a;Ljava/lang/String;Lcom/siimkinks/sqlitemagic/cd$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/cd$a<",
            "TT;TS;TN;>;",
            "Ljava/lang/String;",
            "Lcom/siimkinks/sqlitemagic/cd$a<",
            "*TS;*>;)V"
        }
    .end annotation

    .line 955
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/cd$a;-><init>(Lcom/siimkinks/sqlitemagic/cd;)V

    .line 956
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$b;->a:Ljava/lang/String;

    .line 957
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/cb$b;->b:Lcom/siimkinks/sqlitemagic/cd$a;

    .line 958
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/cb$b;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    iget-object p2, p3, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object p2, p2, Lcom/siimkinks/sqlitemagic/cc;->f:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-void
.end method


# virtual methods
.method a(Ljava/lang/StringBuilder;)V
    .locals 2

    .line 963
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    .line 964
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 965
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$b;->b:Lcom/siimkinks/sqlitemagic/cd$a;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/cb$b;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v1}, Lcom/siimkinks/sqlitemagic/cc;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 970
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$b;->a:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    .line 971
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 972
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/cb$b;->b:Lcom/siimkinks/sqlitemagic/cd$a;

    iget-object p2, p2, Lcom/siimkinks/sqlitemagic/cd$a;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/cb$b;->c:Lcom/siimkinks/sqlitemagic/cc;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cc;->g:Ljava/util/ArrayList;

    invoke-virtual {p2, p1, v0}, Lcom/siimkinks/sqlitemagic/cc;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    return-void
.end method
