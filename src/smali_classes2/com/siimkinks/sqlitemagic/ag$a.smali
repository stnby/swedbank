.class final Lcom/siimkinks/sqlitemagic/ag$a;
.super Lcom/siimkinks/sqlitemagic/ca$a;
.source "CompiledSelectImpl.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/ab;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/ca$a<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        ">;",
        "Lcom/siimkinks/sqlitemagic/ab<",
        "TS;>;"
    }
.end annotation


# instance fields
.field private final a:Landroidx/k/a/f;

.field private final b:Ljava/lang/String;

.field private final c:[Ljava/lang/String;

.field private final d:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;[Ljava/lang/String;Lcom/siimkinks/sqlitemagic/as;[Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 141
    invoke-direct {p0, p3, v0}, Lcom/siimkinks/sqlitemagic/ca$a;-><init>(Lcom/siimkinks/sqlitemagic/as;Lcom/siimkinks/sqlitemagic/ca$b;)V

    .line 142
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/ag$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 143
    invoke-virtual {p3, p1}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p3

    .line 144
    invoke-static {p3, p2}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/f;[Ljava/lang/String;)V

    .line 145
    iput-object p3, p0, Lcom/siimkinks/sqlitemagic/ag$a;->a:Landroidx/k/a/f;

    .line 146
    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/ag$a;->b:Ljava/lang/String;

    .line 147
    iput-object p4, p0, Lcom/siimkinks/sqlitemagic/ag$a;->c:[Ljava/lang/String;

    .line 148
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag$a;->d:[Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "SELECT"

    .line 153
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x6

    const-string v1, "FROM"

    .line 154
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v3, 0x0

    .line 156
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " count(*) "

    .line 157
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()J
    .locals 7

    .line 171
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ag$a;->a:Landroidx/k/a/f;

    monitor-enter v0

    .line 172
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    .line 173
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/ag$a;->a:Landroidx/k/a/f;

    invoke-interface {v3}, Landroidx/k/a/f;->d()J

    move-result-wide v3

    .line 174
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v0, :cond_0

    .line 176
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v0, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 177
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ag$a;->c:[Ljava/lang/String;

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/ag$a;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/siimkinks/sqlitemagic/ag$a;->d:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v5, v6}, Lcom/siimkinks/sqlitemagic/bk;->a(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    return-wide v3

    :catchall_0
    move-exception v1

    .line 174
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method a(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 2

    .line 164
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/ag$a;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public b()Lcom/siimkinks/sqlitemagic/al;
    .locals 2

    .line 185
    new-instance v0, Lcom/siimkinks/sqlitemagic/al;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag$a;->c:[Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/siimkinks/sqlitemagic/ag;->a([Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ca$a;)Lio/reactivex/o;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/al;-><init>(Lio/reactivex/o;)V

    return-object v0
.end method

.method synthetic b(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0

    .line 127
    invoke-virtual {p0, p1}, Lcom/siimkinks/sqlitemagic/ag$a;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[COUNT; sql="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
