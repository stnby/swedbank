.class public final Lcom/siimkinks/sqlitemagic/dx$a;
.super Lcom/siimkinks/sqlitemagic/dy$a;
.source "Update.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/dx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/dy$a;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/siimkinks/sqlitemagic/bb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/dy;Lcom/siimkinks/sqlitemagic/bb;)V
    .locals 1

    .line 296
    invoke-direct {p0, p1}, Lcom/siimkinks/sqlitemagic/dy$a;-><init>(Lcom/siimkinks/sqlitemagic/dy;)V

    .line 292
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/dx$a;->a:Ljava/util/ArrayList;

    .line 293
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/dx$a;->c:Ljava/util/ArrayList;

    .line 297
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/dx$a;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/dx$a;->b:Lcom/siimkinks/sqlitemagic/ah$a;

    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/ah$a;->d:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            "ET:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/siimkinks/sqlitemagic/x<",
            "TV;TR;TET;TT;",
            "Ljava/lang/Object;",
            ">;TV;)",
            "Lcom/siimkinks/sqlitemagic/dx$a<",
            "TT;>;"
        }
    .end annotation

    .line 343
    new-instance v0, Lcom/siimkinks/sqlitemagic/dx$c;

    invoke-direct {v0, p1}, Lcom/siimkinks/sqlitemagic/dx$c;-><init>(Lcom/siimkinks/sqlitemagic/x;)V

    invoke-virtual {v0, p2}, Lcom/siimkinks/sqlitemagic/dx$c;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    .line 344
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/dx$a;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/dx$a;->b:Lcom/siimkinks/sqlitemagic/ah$a;

    iget-object p2, p2, Lcom/siimkinks/sqlitemagic/ah$a;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/util/ArrayList;)V

    return-object p0
.end method

.method public a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;
    .locals 1

    .line 460
    new-instance v0, Lcom/siimkinks/sqlitemagic/dx$d;

    invoke-direct {v0, p0, p1}, Lcom/siimkinks/sqlitemagic/dx$d;-><init>(Lcom/siimkinks/sqlitemagic/dy;Lcom/siimkinks/sqlitemagic/bb;)V

    return-object v0
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 5

    const-string v0, "SET "

    .line 309
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dx$a;->a:Ljava/util/ArrayList;

    .line 311
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0x2c

    if-ge v3, v1, :cond_1

    if-eqz v3, :cond_0

    .line 314
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 316
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/siimkinks/sqlitemagic/bb;

    invoke-virtual {v4, p1}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/lang/StringBuilder;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/dx$a;->c:Ljava/util/ArrayList;

    .line 319
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v2, v1, :cond_3

    if-eqz v2, :cond_2

    .line 322
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 324
    :cond_2
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "=?"

    .line 325
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method public b(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            "ET:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/siimkinks/sqlitemagic/x<",
            "TV;TR;TET;TT;",
            "Ljava/lang/Object;",
            ">;TV;)",
            "Lcom/siimkinks/sqlitemagic/dx$a<",
            "TT;>;"
        }
    .end annotation

    .line 363
    new-instance v0, Lcom/siimkinks/sqlitemagic/dx$c;

    invoke-direct {v0, p1}, Lcom/siimkinks/sqlitemagic/dx$c;-><init>(Lcom/siimkinks/sqlitemagic/x;)V

    invoke-virtual {v0, p2}, Lcom/siimkinks/sqlitemagic/dx$c;->e(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    .line 364
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/dx$a;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    iget-object p2, p0, Lcom/siimkinks/sqlitemagic/dx$a;->b:Lcom/siimkinks/sqlitemagic/ah$a;

    iget-object p2, p2, Lcom/siimkinks/sqlitemagic/ah$a;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/bb;->a(Ljava/util/ArrayList;)V

    return-object p0
.end method
