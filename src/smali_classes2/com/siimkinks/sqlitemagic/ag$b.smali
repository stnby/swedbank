.class final Lcom/siimkinks/sqlitemagic/ag$b;
.super Lcom/siimkinks/sqlitemagic/ca$a;
.source "CompiledSelectImpl.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/ad;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/ca$a<",
        "TT;TT;>;",
        "Lcom/siimkinks/sqlitemagic/ad<",
        "TT;TS;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:[Ljava/lang/String;

.field final c:Lcom/siimkinks/sqlitemagic/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;"
        }
    .end annotation
.end field

.field final d:[Ljava/lang/String;

.field final e:Lcom/siimkinks/sqlitemagic/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/siimkinks/sqlitemagic/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final g:Z


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/ag;Lcom/siimkinks/sqlitemagic/as;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/ag<",
            "TT;TS;>;",
            "Lcom/siimkinks/sqlitemagic/as;",
            ")V"
        }
    .end annotation

    .line 278
    iget-object v0, p1, Lcom/siimkinks/sqlitemagic/ag;->i:Lcom/siimkinks/sqlitemagic/ca$b;

    invoke-direct {p0, p2, v0}, Lcom/siimkinks/sqlitemagic/ca$a;-><init>(Lcom/siimkinks/sqlitemagic/as;Lcom/siimkinks/sqlitemagic/ca$b;)V

    .line 279
    iget-object p2, p1, Lcom/siimkinks/sqlitemagic/ag;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/siimkinks/sqlitemagic/ag$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->a:Ljava/lang/String;

    .line 280
    iget-object p2, p1, Lcom/siimkinks/sqlitemagic/ag;->b:[Ljava/lang/String;

    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->b:[Ljava/lang/String;

    .line 281
    iget-object p2, p1, Lcom/siimkinks/sqlitemagic/ag;->c:Lcom/siimkinks/sqlitemagic/dl;

    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->c:Lcom/siimkinks/sqlitemagic/dl;

    .line 282
    iget-object p2, p1, Lcom/siimkinks/sqlitemagic/ag;->d:[Ljava/lang/String;

    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->d:[Ljava/lang/String;

    .line 283
    iget-object p2, p1, Lcom/siimkinks/sqlitemagic/ag;->e:Lcom/siimkinks/sqlitemagic/b/c;

    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->e:Lcom/siimkinks/sqlitemagic/b/c;

    .line 284
    iget-object p2, p1, Lcom/siimkinks/sqlitemagic/ag;->f:Lcom/siimkinks/sqlitemagic/b/c;

    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->f:Lcom/siimkinks/sqlitemagic/b/c;

    .line 285
    iget-boolean p1, p1, Lcom/siimkinks/sqlitemagic/ag;->g:Z

    iput-boolean p1, p0, Lcom/siimkinks/sqlitemagic/ag$b;->g:Z

    return-void
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "LIMIT"

    .line 290
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const-string v1, " 1 "

    add-int/lit8 v2, v0, 0x5

    add-int/lit8 v0, v0, 0x8

    .line 292
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    .line 295
    :cond_0
    new-instance p0, Landroid/database/SQLException;

    const-string v0, "Tried to query first row but limit clause is already defined with limitClause != 1"

    invoke-direct {p0, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 298
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "LIMIT 1 "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method a(Z)Landroid/database/Cursor;
    .locals 5

    .line 304
    invoke-super {p0, p1}, Lcom/siimkinks/sqlitemagic/ca$a;->a(Z)Landroid/database/Cursor;

    .line 305
    iget-object p1, p0, Lcom/siimkinks/sqlitemagic/ag$b;->h:Lcom/siimkinks/sqlitemagic/as;

    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/as;->c()Landroidx/k/a/b;

    move-result-object p1

    .line 306
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 307
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/ag$b;->b:[Ljava/lang/String;

    invoke-interface {p1, v2, v3}, Landroidx/k/a/b;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;

    move-result-object p1

    .line 308
    sget-boolean v2, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v2, :cond_0

    .line 309
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 310
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/ag$b;->d:[Ljava/lang/String;

    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/ag$b;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/ag$b;->b:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/siimkinks/sqlitemagic/bk;->a(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 312
    :cond_0
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/bf;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 331
    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/ag$b;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/siimkinks/sqlitemagic/ag$b;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/siimkinks/sqlitemagic/cg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/cg<",
            "TT;>;"
        }
    .end annotation

    .line 337
    new-instance v0, Lcom/siimkinks/sqlitemagic/cg;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag$b;->d:[Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/siimkinks/sqlitemagic/ag;->a([Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ca$a;)Lio/reactivex/o;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/cg;-><init>(Lio/reactivex/o;)V

    return-object v0
.end method

.method b(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation

    .line 318
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/ag$b;->i:Lcom/siimkinks/sqlitemagic/ca$b;

    invoke-interface {v0, p1}, Lcom/siimkinks/sqlitemagic/ca$b;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[TAKE FIRST; deepQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/siimkinks/sqlitemagic/ag$b;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ";sql="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/ag$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
