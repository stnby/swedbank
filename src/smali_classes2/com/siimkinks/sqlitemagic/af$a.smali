.class final Lcom/siimkinks/sqlitemagic/af$a;
.super Lcom/siimkinks/sqlitemagic/ca$a;
.source "CompiledSelect1Impl.java"

# interfaces
.implements Lcom/siimkinks/sqlitemagic/ad;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/siimkinks/sqlitemagic/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/siimkinks/sqlitemagic/ca$a<",
        "TT;TT;>;",
        "Lcom/siimkinks/sqlitemagic/ad<",
        "TT;TS;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:[Ljava/lang/String;

.field final c:Lcom/siimkinks/sqlitemagic/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/x<",
            "*TT;***>;"
        }
    .end annotation
.end field

.field final d:[Ljava/lang/String;

.field private final e:Landroidx/k/a/f;


# direct methods
.method constructor <init>(Lcom/siimkinks/sqlitemagic/af;Lcom/siimkinks/sqlitemagic/as;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/af<",
            "TT;TS;>;",
            "Lcom/siimkinks/sqlitemagic/as;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 130
    invoke-direct {p0, p2, v0}, Lcom/siimkinks/sqlitemagic/ca$a;-><init>(Lcom/siimkinks/sqlitemagic/as;Lcom/siimkinks/sqlitemagic/ca$b;)V

    .line 131
    iget-object v0, p1, Lcom/siimkinks/sqlitemagic/af;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/ag$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    iget-object v1, p1, Lcom/siimkinks/sqlitemagic/af;->b:[Ljava/lang/String;

    .line 133
    invoke-virtual {p2, v0}, Lcom/siimkinks/sqlitemagic/as;->a(Ljava/lang/String;)Landroidx/k/a/f;

    move-result-object p2

    .line 134
    invoke-static {p2, v1}, Lcom/siimkinks/sqlitemagic/cn;->a(Landroidx/k/a/f;[Ljava/lang/String;)V

    .line 135
    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/af$a;->e:Landroidx/k/a/f;

    .line 136
    iput-object v0, p0, Lcom/siimkinks/sqlitemagic/af$a;->a:Ljava/lang/String;

    .line 137
    iput-object v1, p0, Lcom/siimkinks/sqlitemagic/af$a;->b:[Ljava/lang/String;

    .line 138
    iget-object p2, p1, Lcom/siimkinks/sqlitemagic/af;->c:Lcom/siimkinks/sqlitemagic/x;

    iput-object p2, p0, Lcom/siimkinks/sqlitemagic/af$a;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 139
    iget-object p1, p1, Lcom/siimkinks/sqlitemagic/af;->d:[Ljava/lang/String;

    iput-object p1, p0, Lcom/siimkinks/sqlitemagic/af$a;->d:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/siimkinks/sqlitemagic/af$a;->e:Landroidx/k/a/f;

    monitor-enter v0

    .line 153
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    .line 154
    iget-object v3, p0, Lcom/siimkinks/sqlitemagic/af$a;->c:Lcom/siimkinks/sqlitemagic/x;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/af$a;->e:Landroidx/k/a/f;

    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->a(Landroidx/k/a/f;)Ljava/lang/Object;

    move-result-object v3

    .line 155
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    sget-boolean v0, Lcom/siimkinks/sqlitemagic/co;->a:Z

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v1

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 158
    iget-object v2, p0, Lcom/siimkinks/sqlitemagic/af$a;->d:[Ljava/lang/String;

    iget-object v4, p0, Lcom/siimkinks/sqlitemagic/af$a;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/siimkinks/sqlitemagic/af$a;->b:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/siimkinks/sqlitemagic/bk;->a(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    return-object v3

    :catchall_0
    move-exception v1

    .line 155
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public b()Lcom/siimkinks/sqlitemagic/cg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/siimkinks/sqlitemagic/cg<",
            "TT;>;"
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/siimkinks/sqlitemagic/cg;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/af$a;->d:[Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/siimkinks/sqlitemagic/ag;->a([Ljava/lang/String;Lcom/siimkinks/sqlitemagic/ca$a;)Lio/reactivex/o;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/siimkinks/sqlitemagic/cg;-><init>(Lio/reactivex/o;)V

    return-object v0
.end method

.method b(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation

    .line 144
    invoke-virtual {p0}, Lcom/siimkinks/sqlitemagic/af$a;->a()Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[TAKE FIRST 1;sql="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/siimkinks/sqlitemagic/af$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
