.class public final Lcom/swedbank/mobile/core/ui/b$a;
.super Ljava/lang/Object;
.source "Animations.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/al;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/b;->a(Landroidx/l/n;Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/animation/Animator;


# direct methods
.method constructor <init>(Landroid/animation/Animator;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/b$a;->a:Landroid/animation/Animator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroidx/l/n;)V
    .locals 1
    .param p1    # Landroidx/l/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transition"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/al$a;->d(Lcom/swedbank/mobile/core/ui/al;Landroidx/l/n;)V

    return-void
.end method

.method public b(Landroidx/l/n;)V
    .locals 1
    .param p1    # Landroidx/l/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transition"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/al$a;->a(Lcom/swedbank/mobile/core/ui/al;Landroidx/l/n;)V

    return-void
.end method

.method public c(Landroidx/l/n;)V
    .locals 1
    .param p1    # Landroidx/l/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transition"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/al$a;->c(Lcom/swedbank/mobile/core/ui/al;Landroidx/l/n;)V

    .line 55
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/b$a;->a:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    return-void
.end method

.method public d(Landroidx/l/n;)V
    .locals 1
    .param p1    # Landroidx/l/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transition"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/al$a;->b(Lcom/swedbank/mobile/core/ui/al;Landroidx/l/n;)V

    return-void
.end method

.method public e(Landroidx/l/n;)V
    .locals 1
    .param p1    # Landroidx/l/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transition"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/al$a;->e(Lcom/swedbank/mobile/core/ui/al;Landroidx/l/n;)V

    return-void
.end method
