.class public abstract Lcom/swedbank/mobile/core/ui/af;
.super Ljava/lang/Object;
.source "ShimmerRenderer.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final a:I

.field private final b:I

.field private c:Landroid/graphics/Bitmap;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:[I

.field private g:Z

.field private final h:Landroid/animation/ValueAnimator;

.field private final i:Landroid/view/View;

.field private final j:I


# direct methods
.method public constructor <init>(Landroid/view/View;IIIZ)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->i:Landroid/view/View;

    iput p2, p0, Lcom/swedbank/mobile/core/ui/af;->j:I

    .line 28
    invoke-static {p3}, Lcom/swedbank/mobile/core/ui/ag;->a(I)I

    move-result p1

    iput p1, p0, Lcom/swedbank/mobile/core/ui/af;->a:I

    .line 29
    invoke-static {p4}, Lcom/swedbank/mobile/core/ui/ag;->a(I)I

    move-result p1

    iput p1, p0, Lcom/swedbank/mobile/core/ui/af;->b:I

    .line 31
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->d:Landroid/graphics/Paint;

    .line 32
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    const/4 p2, 0x1

    .line 33
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 32
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->e:Landroid/graphics/Paint;

    const/4 p1, 0x3

    .line 35
    new-array p1, p1, [I

    iget p3, p0, Lcom/swedbank/mobile/core/ui/af;->b:I

    const/4 p4, 0x0

    aput p3, p1, p4

    iget p3, p0, Lcom/swedbank/mobile/core/ui/af;->a:I

    aput p3, p1, p2

    iget p2, p0, Lcom/swedbank/mobile/core/ui/af;->b:I

    const/4 p3, 0x2

    aput p2, p1, p3

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->f:[I

    .line 36
    iput-boolean p5, p0, Lcom/swedbank/mobile/core/ui/af;->g:Z

    .line 38
    new-array p1, p3, [F

    fill-array-data p1, :array_0

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    const-wide/16 p2, 0x5dc

    .line 39
    invoke-virtual {p1, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 40
    new-instance p2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {p2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    check-cast p2, Landroid/animation/TimeInterpolator;

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 p2, -0x1

    .line 41
    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 42
    move-object p2, p0

    check-cast p2, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const-string p2, "ValueAnimator.ofFloat(-1\u2026this@ShimmerRenderer)\n  }"

    .line 38
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->h:Landroid/animation/ValueAnimator;

    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x40000000    # 2.0f
    .end array-data
.end method

.method public synthetic constructor <init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V
    .locals 6

    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_0

    const/4 p2, -0x1

    const/4 v2, -0x1

    goto :goto_0

    :cond_0
    move v2, p2

    :goto_0
    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_1

    const/16 p3, 0x32

    const/16 v3, 0x32

    goto :goto_1

    :cond_1
    move v3, p3

    :goto_1
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_2

    const/16 p4, 0xc

    const/16 v4, 0xc

    goto :goto_2

    :cond_2
    move v4, p4

    :goto_2
    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_3

    const/4 p5, 0x1

    const/4 v5, 0x1

    goto :goto_3

    :cond_3
    move v5, p5

    :goto_3
    move-object v0, p0

    move-object v1, p1

    .line 26
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZ)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/af;)Landroid/graphics/Paint;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/af;->e:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/af;IILandroid/graphics/Canvas;)V
    .locals 0

    .line 21
    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/af;->a(IILandroid/graphics/Canvas;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/af;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 0

    .line 21
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/core/ui/af;->a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/af;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->c:Landroid/graphics/Bitmap;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/af;)[I
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/af;->f:[I

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/core/ui/af;)Landroid/graphics/Bitmap;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/af;->c:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/core/ui/af;)I
    .locals 0

    .line 21
    iget p0, p0, Lcom/swedbank/mobile/core/ui/af;->j:I

    return p0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    if-eqz p1, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 63
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    .line 62
    :cond_1
    iget-boolean p1, p0, Lcom/swedbank/mobile/core/ui/af;->g:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final a(II)V
    .locals 11

    int-to-float v0, p1

    const/high16 v1, -0x40800000    # -1.0f

    mul-float v3, v0, v1

    .line 139
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/af;->a(Lcom/swedbank/mobile/core/ui/af;)Landroid/graphics/Paint;

    move-result-object v1

    new-instance v10, Landroid/graphics/LinearGradient;

    add-float v5, v3, v0

    .line 140
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/af;->b(Lcom/swedbank/mobile/core/ui/af;)[I

    move-result-object v7

    const/4 v0, 0x3

    new-array v8, v0, [F

    fill-array-data v8, :array_0

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, v10

    .line 139
    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    check-cast v10, Landroid/graphics/Shader;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    if-lez p2, :cond_1

    if-lez p1, :cond_1

    .line 142
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/af;->c(Lcom/swedbank/mobile/core/ui/af;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 145
    :cond_0
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 147
    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v1, "Bitmap.createBitmap(width, height, config)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 149
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/af;->d(Lcom/swedbank/mobile/core/ui/af;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 151
    invoke-static {p0, p1, p2, v1}, Lcom/swedbank/mobile/core/ui/af;->a(Lcom/swedbank/mobile/core/ui/af;IILandroid/graphics/Canvas;)V

    .line 154
    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 156
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 157
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    const/4 v5, 0x1

    .line 158
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/16 v5, 0xff

    const/4 v6, 0x0

    .line 159
    invoke-static {v5, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 161
    invoke-static {p0, p1, p2, v3, v4}, Lcom/swedbank/mobile/core/ui/af;->a(Lcom/swedbank/mobile/core/ui/af;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    const-string p1, "item"

    .line 162
    invoke-static {v2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 164
    new-instance p2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->XOR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    check-cast p2, Landroid/graphics/Xfermode;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    const/4 p2, 0x0

    .line 165
    invoke-virtual {v1, v2, p2, p2, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/af;->a(Lcom/swedbank/mobile/core/ui/af;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 72
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->c:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    const/4 p1, 0x0

    .line 73
    check-cast p1, Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->c:Landroid/graphics/Bitmap;

    .line 74
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    :catch_0
    :goto_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected a(IILandroid/graphics/Canvas;)V
    .locals 0
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string p1, "canvas"

    invoke-static {p3, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method protected abstract a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public final a(Landroid/graphics/Canvas;Lkotlin/e/a/a;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "superOnDraw"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-boolean v0, p0, Lcom/swedbank/mobile/core/ui/af;->g:Z

    if-eqz v0, :cond_0

    .line 80
    iget v0, p0, Lcom/swedbank/mobile/core/ui/af;->b:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 82
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v5, v0

    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/af;->e:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 83
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/af;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 84
    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/af;->d:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 87
    :cond_0
    invoke-interface {p2}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 46
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/af;->g:Z

    .line 47
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->i:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/af;->a(I)V

    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 10
    .param p1    # Landroid/animation/ValueAnimator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "valueAnimator"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/af;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/af;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float v2, v0, p1

    .line 135
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/af;->a(Lcom/swedbank/mobile/core/ui/af;)Landroid/graphics/Paint;

    move-result-object p1

    new-instance v9, Landroid/graphics/LinearGradient;

    const/4 v3, 0x0

    add-float v4, v2, v0

    const/4 v5, 0x0

    .line 136
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/af;->b(Lcom/swedbank/mobile/core/ui/af;)[I

    move-result-object v6

    const/4 v0, 0x3

    new-array v7, v0, [F

    fill-array-data v7, :array_0

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v1, v9

    .line 135
    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    check-cast v9, Landroid/graphics/Shader;

    invoke-virtual {p1, v9}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 54
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->i:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 52
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 56
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/af;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    :goto_0
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method
