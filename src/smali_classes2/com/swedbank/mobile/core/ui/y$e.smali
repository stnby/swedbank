.class final Lcom/swedbank/mobile/core/ui/y$e;
.super Ljava/lang/Object;
.source "SharedLoadingRenderStream.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/y;-><init>(Lkotlin/e/a/a;JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/core/ui/y;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/core/ui/y;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/y$e;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/y$b;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/core/ui/y$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/y$b;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget-object v0, Lcom/swedbank/mobile/core/ui/z;->c:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/y$b;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 125
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 124
    :pswitch_0
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/y$e;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-static {p1}, Lcom/swedbank/mobile/core/ui/y;->c(Lcom/swedbank/mobile/core/ui/y;)J

    move-result-wide v0

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p1}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object p1

    .line 125
    sget-object v0, Lcom/swedbank/mobile/core/ui/y$e$2;->a:Lcom/swedbank/mobile/core/ui/y$e$2;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x1

    .line 122
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 120
    :pswitch_2
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/y$e;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-static {p1}, Lcom/swedbank/mobile/core/ui/y;->b(Lcom/swedbank/mobile/core/ui/y;)J

    move-result-wide v0

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p1}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object p1

    .line 121
    sget-object v0, Lcom/swedbank/mobile/core/ui/y$e$1;->a:Lcom/swedbank/mobile/core/ui/y$e$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    :pswitch_3
    const/4 p1, 0x0

    .line 118
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 117
    :pswitch_4
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Lcom/swedbank/mobile/core/ui/y$b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/y$e;->a(Lcom/swedbank/mobile/core/ui/y$b;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
