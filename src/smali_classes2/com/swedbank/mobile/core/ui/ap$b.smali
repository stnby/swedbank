.class public final Lcom/swedbank/mobile/core/ui/ap$b;
.super Landroidx/recyclerview/widget/RecyclerView$n;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/appcompat/widget/Toolbar;

.field final synthetic b:Landroidx/recyclerview/widget/RecyclerView;

.field final synthetic c:F


# direct methods
.method constructor <init>(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;F)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/ap$b;->a:Landroidx/appcompat/widget/Toolbar;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/ap$b;->b:Landroidx/recyclerview/widget/RecyclerView;

    iput p3, p0, Lcom/swedbank/mobile/core/ui/ap$b;->c:F

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$n;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string p2, "rv"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/ap$b;->b:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$i;

    move-result-object p1

    instance-of p2, p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz p1, :cond_2

    .line 83
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->o()I

    move-result p1

    if-gtz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    .line 84
    :cond_1
    iget p1, p0, Lcom/swedbank/mobile/core/ui/ap$b;->c:F

    .line 86
    :goto_0
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/ap$b;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p2}, Landroidx/appcompat/widget/Toolbar;->getTranslationZ()F

    move-result p2

    cmpg-float p2, p2, p1

    if-eqz p2, :cond_2

    .line 87
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/ap$b;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p2, p1}, Landroidx/appcompat/widget/Toolbar;->setTranslationZ(F)V

    :cond_2
    return-void
.end method
