.class final Lcom/swedbank/mobile/core/ui/y$d;
.super Ljava/lang/Object;
.source "SharedLoadingRenderStream.kt"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/y;-><init>(Lkotlin/e/a/a;JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c<",
        "TR;TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/core/ui/y;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/core/ui/y;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Lkotlin/k;

    check-cast p2, Lkotlin/k;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/y$d;->a(Lkotlin/k;Lkotlin/k;)Lkotlin/k;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lkotlin/k;Lkotlin/k;)Lkotlin/k;
    .locals 10
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/core/ui/y$b;",
            "Ljava/lang/Long;",
            ">;",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/core/ui/y$a;",
            "Ljava/lang/Long;",
            ">;)",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/core/ui/y$b;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<name for destructuring parameter 1>"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/y$b;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-virtual {p2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/core/ui/y$a;

    invoke-virtual {p2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    .line 59
    sget-object p2, Lcom/swedbank/mobile/core/ui/z;->b:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/y$b;->ordinal()I

    move-result v0

    aget p2, p2, v0

    const/4 v0, 0x0

    const/4 v5, 0x1

    packed-switch p2, :pswitch_data_0

    .line 101
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 98
    :pswitch_0
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-static {p2}, Lcom/swedbank/mobile/core/ui/y;->c(Lcom/swedbank/mobile/core/ui/y;)J

    move-result-wide v6

    sub-long v8, v3, v1

    cmp-long p2, v8, v6

    if-ltz p2, :cond_0

    const/4 v0, 0x1

    .line 102
    :cond_0
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->d:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_1

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 103
    :cond_1
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_2

    if-nez v0, :cond_2

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->e:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 104
    :cond_2
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_3

    if-eqz v0, :cond_3

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 105
    :cond_3
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->a:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_4

    if-nez v0, :cond_4

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 106
    :cond_4
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->a:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_5

    if-eqz v0, :cond_5

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->c:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 107
    :cond_5
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->b:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_6

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 108
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Impossible state"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 87
    :pswitch_1
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-static {p2}, Lcom/swedbank/mobile/core/ui/y;->c(Lcom/swedbank/mobile/core/ui/y;)J

    move-result-wide v6

    sub-long v8, v3, v1

    cmp-long p2, v8, v6

    if-ltz p2, :cond_7

    const/4 v0, 0x1

    .line 91
    :cond_7
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->d:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_8

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 92
    :cond_8
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_9

    if-nez v0, :cond_9

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->e:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 93
    :cond_9
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_a

    if-eqz v0, :cond_a

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 94
    :cond_a
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 66
    :pswitch_2
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-static {p2}, Lcom/swedbank/mobile/core/ui/y;->b(Lcom/swedbank/mobile/core/ui/y;)J

    move-result-wide v6

    sub-long v8, v3, v1

    cmp-long p2, v8, v6

    if-ltz p2, :cond_b

    const/4 p2, 0x1

    goto :goto_0

    :cond_b
    const/4 p2, 0x0

    .line 69
    :goto_0
    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/y$d;->a:Lcom/swedbank/mobile/core/ui/y;

    invoke-static {v6}, Lcom/swedbank/mobile/core/ui/y;->c(Lcom/swedbank/mobile/core/ui/y;)J

    move-result-wide v6

    cmp-long v6, v8, v6

    if-ltz v6, :cond_c

    const/4 v0, 0x1

    .line 73
    :cond_c
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->d:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_d

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 74
    :cond_d
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_e

    if-nez p2, :cond_e

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 75
    :cond_e
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_f

    if-eqz p2, :cond_f

    if-nez v0, :cond_f

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->e:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 76
    :cond_f
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_10

    if-eqz v0, :cond_10

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 77
    :cond_10
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->a:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_11

    if-nez p2, :cond_11

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->c:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 78
    :cond_11
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->a:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_12

    if-eqz p2, :cond_12

    if-nez v0, :cond_12

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->c:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto/16 :goto_1

    .line 79
    :cond_12
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->a:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_13

    if-eqz v0, :cond_13

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto :goto_1

    .line 80
    :cond_13
    sget-object v5, Lcom/swedbank/mobile/core/ui/y$a;->b:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v5, :cond_14

    if-nez p2, :cond_14

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto :goto_1

    .line 81
    :cond_14
    sget-object v3, Lcom/swedbank/mobile/core/ui/y$a;->b:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, v3, :cond_15

    if-eqz p2, :cond_15

    if-nez v0, :cond_15

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->c:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto :goto_1

    .line 82
    :cond_15
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$a;->b:Lcom/swedbank/mobile/core/ui/y$a;

    if-ne p1, p2, :cond_16

    if-eqz v0, :cond_16

    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto :goto_1

    .line 83
    :cond_16
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Impossible state"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 60
    :pswitch_3
    sget-object p2, Lcom/swedbank/mobile/core/ui/z;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/y$a;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_1

    .line 63
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto :goto_1

    .line 62
    :pswitch_4
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    goto :goto_1

    .line 61
    :pswitch_5
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$b;->c:Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    :goto_1
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
