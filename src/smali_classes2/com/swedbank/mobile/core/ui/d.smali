.class public final Lcom/swedbank/mobile/core/ui/d;
.super Ljava/lang/Object;
.source "BetterBounceInterpolator.kt"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:F

.field private final b:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/swedbank/mobile/core/ui/d;-><init>(IFILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/swedbank/mobile/core/ui/d;->b:I

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p2, p1

    .line 22
    iput p2, p0, Lcom/swedbank/mobile/core/ui/d;->a:F

    return-void
.end method

.method public synthetic constructor <init>(IFILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x3

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const p2, 0x3e99999a    # 0.3f

    .line 20
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/d;-><init>(IF)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/d;)F
    .locals 0

    .line 18
    iget p0, p0, Lcom/swedbank/mobile/core/ui/d;->a:F

    return p0
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 7

    const/high16 v0, 0x3f800000    # 1.0f

    float-to-double v1, v0

    const/high16 v3, 0x40c00000    # 6.0f

    mul-float v3, v3, p1

    .line 25
    iget v4, p0, Lcom/swedbank/mobile/core/ui/d;->b:I

    int-to-float v4, v4

    mul-float v3, v3, v4

    float-to-double v3, v3

    const-wide v5, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    neg-double v3, v3

    sub-float v5, v0, p1

    const/high16 v6, 0x40000000    # 2.0f

    mul-float v5, v5, v6

    mul-float v5, v5, p1

    .line 29
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/d;->a(Lcom/swedbank/mobile/core/ui/d;)F

    move-result v6

    mul-float v5, v5, v6

    mul-float p1, p1, p1

    add-float/2addr v5, p1

    neg-float p1, v5

    add-float/2addr p1, v0

    float-to-double v5, p1

    mul-double v3, v3, v5

    add-double/2addr v1, v3

    double-to-float p1, v1

    return p1
.end method
