.class final Lcom/swedbank/mobile/core/ui/am$c;
.super Lkotlin/e/b/k;
.source "ViewBindings.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/am;->a([ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "TT;",
        "Lkotlin/h/g<",
        "*>;",
        "Ljava/util/List<",
        "+TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:[I

.field final synthetic b:Lkotlin/e/a/m;


# direct methods
.method constructor <init>([ILkotlin/e/a/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/am$c;->a:[I

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/am$c;->b:Lkotlin/e/a/m;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/h/g;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/am$c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/util/List;
    .locals 7
    .param p2    # Lkotlin/h/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lkotlin/h/g<",
            "*>;)",
            "Ljava/util/List<",
            "TV;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "desc"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/am$c;->a:[I

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 119
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget v4, v0, v3

    .line 97
    iget-object v5, p0, Lcom/swedbank/mobile/core/ui/am$c;->b:Lkotlin/e/a/m;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, p1, v6}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    if-eqz v5, :cond_0

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v4, p2}, Lcom/swedbank/mobile/core/ui/am;->a(ILkotlin/h/g;)Ljava/lang/Void;

    const/4 p1, 0x0

    throw p1

    .line 121
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
