.class final enum Lcom/swedbank/mobile/core/ui/y$a;
.super Ljava/lang/Enum;
.source "SharedLoadingRenderStream.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/core/ui/y$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/core/ui/y$a;

.field public static final enum b:Lcom/swedbank/mobile/core/ui/y$a;

.field public static final enum c:Lcom/swedbank/mobile/core/ui/y$a;

.field public static final enum d:Lcom/swedbank/mobile/core/ui/y$a;

.field private static final synthetic e:[Lcom/swedbank/mobile/core/ui/y$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/core/ui/y$a;

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$a;

    const-string v2, "SHOW"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$a;->a:Lcom/swedbank/mobile/core/ui/y$a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$a;

    const-string v2, "FORCE_SHOW"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$a;->b:Lcom/swedbank/mobile/core/ui/y$a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$a;

    const-string v2, "DISMISS"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$a;

    const-string v2, "FORCE_DISMISS"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$a;->d:Lcom/swedbank/mobile/core/ui/y$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/core/ui/y$a;->e:[Lcom/swedbank/mobile/core/ui/y$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/core/ui/y$a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/core/ui/y$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/core/ui/y$a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/core/ui/y$a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/core/ui/y$a;->e:[Lcom/swedbank/mobile/core/ui/y$a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/core/ui/y$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/core/ui/y$a;

    return-object v0
.end method
