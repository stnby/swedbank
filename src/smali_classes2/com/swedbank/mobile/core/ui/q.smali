.class public final Lcom/swedbank/mobile/core/ui/q;
.super Ljava/lang/Object;
.source "NoSpacesInputFilter.kt"

# interfaces
.implements Landroid/text/InputFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/text/Spanned;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string p4, "source"

    invoke-static {p1, p4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance p4, Ljava/lang/StringBuilder;

    sub-int p5, p3, p2

    invoke-direct {p4, p5}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 p5, 0x1

    move p5, p2

    const/4 p6, 0x1

    :goto_0
    if-ge p5, p3, :cond_1

    .line 23
    invoke-interface {p1, p5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 25
    invoke-static {v0}, Lkotlin/j/a;->a(C)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "sb.append(c)"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const/4 p6, 0x0

    :goto_1
    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_1
    if-eqz p6, :cond_2

    const/4 p1, 0x0

    goto :goto_2

    .line 31
    :cond_2
    instance-of p3, p1, Landroid/text/Spanned;

    if-eqz p3, :cond_3

    .line 32
    :try_start_0
    new-instance p3, Landroid/text/SpannableString;

    move-object p5, p4

    check-cast p5, Ljava/lang/CharSequence;

    invoke-direct {p3, p5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 33
    move-object v0, p1

    check-cast v0, Landroid/text/Spanned;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v3, 0x0

    move-object v4, p3

    check-cast v4, Landroid/text/Spannable;

    const/4 v5, 0x0

    move v1, p2

    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 32
    check-cast p3, Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, p3

    goto :goto_2

    .line 36
    :catch_0
    check-cast p4, Ljava/lang/CharSequence;

    move-object p1, p4

    goto :goto_2

    .line 38
    :cond_3
    move-object p1, p4

    check-cast p1, Ljava/lang/CharSequence;

    :goto_2
    return-object p1
.end method
