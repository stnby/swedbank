.class final Lcom/swedbank/mobile/core/ui/k$b;
.super Ljava/lang/Object;
.source "Diffs.kt"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c<",
        "TR;TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/e/a/m;

.field final synthetic b:Lkotlin/e/a/m;


# direct methods
.method constructor <init>(Lkotlin/e/a/m;Lkotlin/e/a/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/k$b;->a:Lkotlin/e/a/m;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/k$b;->b:Lkotlin/e/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/j;Ljava/util/List;)Lcom/swedbank/mobile/core/ui/j;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/core/ui/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/j<",
            "+TT;>;TR;)",
            "Lcom/swedbank/mobile/core/ui/j<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "current"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->c()Ljava/util/List;

    move-result-object p1

    .line 20
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    new-instance p1, Lcom/swedbank/mobile/core/ui/j;

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p1, p2, v1, v0, v1}, Lcom/swedbank/mobile/core/ui/j;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILkotlin/e/b/g;)V

    goto :goto_0

    .line 23
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/core/ui/j;

    new-instance v1, Lcom/swedbank/mobile/core/ui/p;

    .line 26
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/k$b;->a:Lkotlin/e/a/m;

    .line 27
    iget-object v3, p0, Lcom/swedbank/mobile/core/ui/k$b;->b:Lkotlin/e/a/m;

    .line 23
    invoke-direct {v1, p1, p2, v2, v3}, Lcom/swedbank/mobile/core/ui/p;-><init>(Ljava/util/List;Ljava/util/List;Lkotlin/e/a/m;Lkotlin/e/a/m;)V

    check-cast v1, Landroidx/recyclerview/widget/f$a;

    invoke-static {v1}, Landroidx/recyclerview/widget/f;->a(Landroidx/recyclerview/widget/f$a;)Landroidx/recyclerview/widget/f$b;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Lcom/swedbank/mobile/core/ui/j;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/core/ui/j;

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/k$b;->a(Lcom/swedbank/mobile/core/ui/j;Ljava/util/List;)Lcom/swedbank/mobile/core/ui/j;

    move-result-object p1

    return-object p1
.end method
