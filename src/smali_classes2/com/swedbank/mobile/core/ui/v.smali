.class public final Lcom/swedbank/mobile/core/ui/v;
.super Ljava/lang/Object;
.source "RxView.kt"


# static fields
.field public static a:Z = true

.field public static final b:Ljava/lang/Runnable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    sget-object v0, Lcom/swedbank/mobile/core/ui/v$a;->a:Lcom/swedbank/mobile/core/ui/v$a;

    check-cast v0, Ljava/lang/Runnable;

    sput-object v0, Lcom/swedbank/mobile/core/ui/v;->b:Ljava/lang/Runnable;

    return-void
.end method

.method public static final a(Lio/reactivex/b/c;Landroid/view/View;)V
    .locals 1
    .param p0    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$untilViewDetachedFromWindow"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-interface {p0}, Lio/reactivex/b/c;->a()V

    return-void

    .line 54
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/core/ui/v$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/core/ui/v$b;-><init>(Lio/reactivex/b/c;)V

    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method
