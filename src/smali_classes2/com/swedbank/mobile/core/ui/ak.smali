.class public final Lcom/swedbank/mobile/core/ui/ak;
.super Ljava/lang/Object;
.source "TransitionAwareRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/aj;


# instance fields
.field private a:Lcom/swedbank/mobile/core/ui/ao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/core/ui/ao<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/lang/Object;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/ak;)Ljava/lang/Object;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/ak;->c:Ljava/lang/Object;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/ak;Ljava/lang/Object;)V
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/ak;->c:Ljava/lang/Object;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/ak;Z)V
    .locals 0

    .line 13
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/ak;->d:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/architect/a/b/a;Z)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            ":",
            "Lcom/swedbank/mobile/core/ui/ao<",
            "*>;>(TT;Z)V"
        }
    .end annotation

    const-string v0, "contentView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/core/ui/ao;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/ak;->a:Lcom/swedbank/mobile/core/ui/ao;

    .line 25
    iput-boolean p2, p0, Lcom/swedbank/mobile/core/ui/ak;->b:Z

    .line 26
    new-instance p2, Lcom/swedbank/mobile/core/ui/ak$a;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/core/ui/ak$a;-><init>(Lcom/swedbank/mobile/core/ui/ak;)V

    check-cast p2, Lkotlin/e/a/a;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/a/b/a;->a(Lkotlin/e/a/a;)V

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-boolean v0, p0, Lcom/swedbank/mobile/core/ui/ak;->d:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/swedbank/mobile/core/ui/ak;->b:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 41
    :cond_0
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/ak;->c:Ljava/lang/Object;

    goto :goto_1

    .line 36
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/swedbank/mobile/core/ui/ak;->b:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 37
    iput-boolean v0, p0, Lcom/swedbank/mobile/core/ui/ak;->b:Z

    .line 39
    :cond_2
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/ak;->a:Lcom/swedbank/mobile/core/ui/ao;

    if-nez v0, :cond_3

    const-string v1, "renderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_3
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/core/ui/ao;->b(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method
