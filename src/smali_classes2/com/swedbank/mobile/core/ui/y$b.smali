.class final enum Lcom/swedbank/mobile/core/ui/y$b;
.super Ljava/lang/Enum;
.source "SharedLoadingRenderStream.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/core/ui/y$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/core/ui/y$b;

.field public static final enum b:Lcom/swedbank/mobile/core/ui/y$b;

.field public static final enum c:Lcom/swedbank/mobile/core/ui/y$b;

.field public static final enum d:Lcom/swedbank/mobile/core/ui/y$b;

.field public static final enum e:Lcom/swedbank/mobile/core/ui/y$b;

.field private static final synthetic f:[Lcom/swedbank/mobile/core/ui/y$b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/swedbank/mobile/core/ui/y$b;

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$b;

    const-string v2, "UNDETERMINED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$b;->a:Lcom/swedbank/mobile/core/ui/y$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$b;

    const-string v2, "NOT_LOADING"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$b;->b:Lcom/swedbank/mobile/core/ui/y$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$b;

    const-string v2, "INITIAL_LOADING"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$b;->c:Lcom/swedbank/mobile/core/ui/y$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$b;

    const-string v2, "LOADING"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$b;->d:Lcom/swedbank/mobile/core/ui/y$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/y$b;

    const-string v2, "DISMISS_AFTER_TIMEOUT"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/y$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/y$b;->e:Lcom/swedbank/mobile/core/ui/y$b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/core/ui/y$b;->f:[Lcom/swedbank/mobile/core/ui/y$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/core/ui/y$b;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/core/ui/y$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/core/ui/y$b;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/core/ui/y$b;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/core/ui/y$b;->f:[Lcom/swedbank/mobile/core/ui/y$b;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/core/ui/y$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/core/ui/y$b;

    return-object v0
.end method
