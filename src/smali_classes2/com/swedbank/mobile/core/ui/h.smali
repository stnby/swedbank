.class public abstract Lcom/swedbank/mobile/core/ui/h;
.super Ljava/lang/Object;
.source "DebouncingOnClickListener.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)V
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget-boolean v0, Lcom/swedbank/mobile/core/ui/v;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 13
    sput-boolean v0, Lcom/swedbank/mobile/core/ui/v;->a:Z

    .line 14
    sget-object v0, Lcom/swedbank/mobile/core/ui/v;->b:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 23
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/swedbank/mobile/core/a/e;->a(Landroid/content/Context;)Lcom/swedbank/mobile/core/a/c;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/core/a/c;->a(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    :catch_0
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/h;->a(Landroid/view/View;)V

    :cond_0
    return-void
.end method
