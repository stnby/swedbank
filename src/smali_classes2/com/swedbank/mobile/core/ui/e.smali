.class public final Lcom/swedbank/mobile/core/ui/e;
.super Ljava/lang/Object;
.source "Bitmaps.kt"


# direct methods
.method public static final a(Landroid/content/res/Resources;IIILcom/swedbank/mobile/core/ui/w;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/core/ui/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scalingLogic"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 35
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 36
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    .line 37
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 38
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 80
    sget-object v4, Lcom/swedbank/mobile/core/ui/f;->a:[I

    invoke-virtual {p4}, Lcom/swedbank/mobile/core/ui/w;->ordinal()I

    move-result p4

    aget p4, v4, p4

    if-eq p4, v1, :cond_1

    int-to-float p4, v2

    int-to-float v1, v3

    div-float/2addr p4, v1

    int-to-float v1, p2

    int-to-float v4, p3

    div-float/2addr v1, v4

    cmpl-float p4, p4, v1

    if-lez p4, :cond_0

    .line 95
    div-int/2addr v3, p3

    goto :goto_0

    .line 96
    :cond_0
    div-int v3, v2, p2

    goto :goto_0

    :cond_1
    int-to-float p4, v2

    int-to-float v1, v3

    div-float/2addr p4, v1

    int-to-float v1, p2

    int-to-float v4, p3

    div-float/2addr v1, v4

    cmpl-float p4, p4, v1

    if-lez p4, :cond_2

    .line 86
    div-int v3, v2, p2

    goto :goto_0

    .line 87
    :cond_2
    div-int/2addr v3, p3

    .line 99
    :goto_0
    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 40
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic a(Landroid/content/res/Resources;IIILcom/swedbank/mobile/core/ui/w;ILjava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    and-int/lit8 p5, p5, 0x10

    if-eqz p5, :cond_0

    .line 32
    sget-object p4, Lcom/swedbank/mobile/core/ui/w;->b:Lcom/swedbank/mobile/core/ui/w;

    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/core/ui/e;->a(Landroid/content/res/Resources;IIILcom/swedbank/mobile/core/ui/w;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method
