.class final Lcom/swedbank/mobile/core/ui/m$4;
.super Lkotlin/e/b/k;
.source "ShimmeringViews.kt"

# interfaces
.implements Lkotlin/e/a/r;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/m;-><init>(Landroid/content/res/Resources;FLandroid/graphics/Paint;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/r<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Landroid/graphics/Canvas;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/graphics/RectF;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:Landroid/graphics/Paint;


# direct methods
.method constructor <init>(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/m$4;->a:Landroid/graphics/RectF;

    iput p2, p0, Lcom/swedbank/mobile/core/ui/m$4;->b:F

    iput p3, p0, Lcom/swedbank/mobile/core/ui/m$4;->c:F

    iput-object p4, p0, Lcom/swedbank/mobile/core/ui/m$4;->d:Landroid/graphics/Paint;

    const/4 p1, 0x4

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    check-cast p4, Landroid/graphics/Canvas;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/core/ui/m$4;->a(IIILandroid/graphics/Canvas;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(IIILandroid/graphics/Canvas;)V
    .locals 2
    .param p4    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/m$4;->a:Landroid/graphics/RectF;

    int-to-float p1, p1

    iget v1, p0, Lcom/swedbank/mobile/core/ui/m$4;->b:F

    add-float/2addr p1, v1

    int-to-float p2, p2

    int-to-float p3, p3

    iget v1, p0, Lcom/swedbank/mobile/core/ui/m$4;->b:F

    sub-float/2addr p3, v1

    iget v1, p0, Lcom/swedbank/mobile/core/ui/m$4;->c:F

    add-float/2addr v1, p2

    invoke-virtual {v0, p1, p2, p3, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 65
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/m$4;->a:Landroid/graphics/RectF;

    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/m$4;->d:Landroid/graphics/Paint;

    invoke-virtual {p4, p1, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-void
.end method
