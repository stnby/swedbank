.class public final Lcom/swedbank/mobile/core/ui/k;
.super Ljava/lang/Object;
.source "Diffs.kt"


# static fields
.field private static final a:Lcom/swedbank/mobile/core/ui/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/core/ui/j<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 9
    new-instance v0, Lcom/swedbank/mobile/core/ui/j;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/core/ui/j;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/core/ui/k;->a:Lcom/swedbank/mobile/core/ui/j;

    .line 10
    sget-object v0, Lcom/swedbank/mobile/core/ui/k$a;->a:Lcom/swedbank/mobile/core/ui/k$a;

    check-cast v0, Lkotlin/e/a/m;

    sput-object v0, Lcom/swedbank/mobile/core/ui/k;->b:Lkotlin/e/a/m;

    return-void
.end method

.method public static final a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;)Lio/reactivex/o;
    .locals 2
    .param p0    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R::",
            "Ljava/util/List<",
            "+TT;>;>(",
            "Lio/reactivex/o<",
            "TR;>;",
            "Lkotlin/e/a/m<",
            "-TT;-TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/e/a/m<",
            "-TT;-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/core/ui/j<",
            "TT;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$calculateDiff"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "areContentsTheSame"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "areItemsTheSame"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/swedbank/mobile/core/ui/k;->a:Lcom/swedbank/mobile/core/ui/j;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/core/ui/k$b;

    invoke-direct {v1, p2, p1}, Lcom/swedbank/mobile/core/ui/k$b;-><init>(Lkotlin/e/a/m;Lkotlin/e/a/m;)V

    check-cast v1, Lio/reactivex/c/c;

    invoke-virtual {p0, v0, v1}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p0

    const-wide/16 p1, 0x1

    .line 30
    invoke-virtual {p0, p1, p2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object p0

    const-string p1, "scan(INITIAL_DIFF as Dif\u2026)))\n      }\n    }.skip(1)"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 19
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.swedbank.mobile.core.ui.Diff<T>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_1

    .line 17
    sget-object p1, Lcom/swedbank/mobile/core/ui/k;->b:Lkotlin/e/a/m;

    if-eqz p1, :cond_0

    const/4 p3, 0x2

    invoke-static {p1, p3}, Lkotlin/e/b/z;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/e/a/m;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type (oldItem: T, newItem: T) -> kotlin.Boolean"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    :goto_0
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method
