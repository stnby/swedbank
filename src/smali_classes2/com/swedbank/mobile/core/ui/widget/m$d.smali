.class final Lcom/swedbank/mobile/core/ui/widget/m$d;
.super Ljava/lang/Object;
.source "SlideToConfirmIndicatorView.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/m;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/animation/ValueAnimator;

.field final synthetic b:Lcom/swedbank/mobile/core/ui/widget/m;


# direct methods
.method constructor <init>(Landroid/animation/ValueAnimator;Lcom/swedbank/mobile/core/ui/widget/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$d;->a:Landroid/animation/ValueAnimator;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/m$d;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .line 197
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$d;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    mul-float p1, p1, v0

    const/high16 v0, 0x3f000000    # 0.5f

    .line 198
    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    add-float/2addr p1, v0

    .line 199
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$d;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->u(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/graphics/Paint;

    move-result-object v0

    const/16 v1, 0xff

    int-to-float v1, v1

    mul-float p1, p1, v1

    float-to-int p1, p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 201
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$d;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {p1}, Lcom/swedbank/mobile/core/ui/widget/m;->f(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result p1

    const/4 v0, 0x0

    cmpg-float p1, p1, v0

    if-eqz p1, :cond_0

    .line 202
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$d;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/m;->e(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 205
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$d;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/m;->invalidateSelf()V

    return-void

    .line 197
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
