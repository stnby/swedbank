.class public final Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;
.super Landroid/view/View;
.source "SlideToConfirmIndicatorView.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/widget/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/core/ui/widget/m;-><init>(Landroid/content/Context;)V

    .line 32
    move-object p1, p2

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 31
    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a:Lcom/swedbank/mobile/core/ui/widget/m;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 28
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 29
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->a()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/m;->a(Z)V

    return-void
.end method

.method public final a(ZLkotlin/e/a/a;)V
    .locals 1
    .param p2    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "endAction"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {v0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/m;->a(ZLkotlin/e/a/a;)V

    return-void
.end method

.method public final b()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->b()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 36
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->start()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 41
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->stop()V

    return-void
.end method
