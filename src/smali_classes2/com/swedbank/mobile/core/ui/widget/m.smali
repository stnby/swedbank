.class public final Lcom/swedbank/mobile/core/ui/widget/m;
.super Landroid/graphics/drawable/Drawable;
.source "SlideToConfirmIndicatorView.kt"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# instance fields
.field private A:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final B:Landroid/animation/ValueAnimator;

.field private final a:F

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/RectF;

.field private h:Lcom/swedbank/mobile/core/ui/widget/a;

.field private i:F

.field private j:F

.field private k:F

.field private l:Z

.field private final m:Landroid/animation/ValueAnimator;

.field private n:Z

.field private o:Z

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private final t:Landroid/view/animation/AccelerateInterpolator;

.field private final u:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field private v:Lcom/swedbank/mobile/core/ui/widget/h;

.field private w:Lcom/swedbank/mobile/core/ui/widget/h;

.field private final x:Landroid/animation/ValueAnimator;

.field private final y:Lcom/swedbank/mobile/core/ui/widget/h;

.field private final z:Lcom/swedbank/mobile/core/ui/widget/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/core/ui/t$d;->slide_to_confirm_inner_button_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->a:F

    .line 66
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->c()Landroid/graphics/Paint;

    move-result-object v0

    .line 67
    sget v1, Lcom/swedbank/mobile/core/ui/t$c;->slide_to_confirm_bg_arc:I

    .line 601
    invoke-static {p1, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->b:Landroid/graphics/Paint;

    .line 69
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->c()Landroid/graphics/Paint;

    move-result-object v0

    .line 70
    sget v1, Lcom/swedbank/mobile/core/ui/t$c;->slide_to_confirm_main_orange:I

    .line 603
    invoke-static {p1, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->c:Landroid/graphics/Paint;

    .line 72
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->c()Landroid/graphics/Paint;

    move-result-object v0

    .line 73
    sget v1, Lcom/swedbank/mobile/core/ui/t$c;->slide_to_confirm_success:I

    .line 605
    invoke-static {p1, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 72
    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->d:Landroid/graphics/Paint;

    .line 75
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->c()Landroid/graphics/Paint;

    move-result-object v0

    .line 76
    sget v1, Lcom/swedbank/mobile/core/ui/t$c;->slide_to_confirm_success:I

    .line 607
    invoke-static {p1, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->e:Landroid/graphics/Paint;

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->c()Landroid/graphics/Paint;

    move-result-object v0

    .line 79
    sget v1, Lcom/swedbank/mobile/core/ui/t$c;->slide_to_confirm_failure:I

    .line 609
    invoke-static {p1, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->f:Landroid/graphics/Paint;

    .line 82
    sget-object p1, Lcom/swedbank/mobile/core/ui/widget/a;->a:Lcom/swedbank/mobile/core/ui/widget/a;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    const/high16 p1, 0x41f00000    # 30.0f

    .line 86
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->j:F

    .line 87
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->k:F

    const/4 p1, 0x1

    .line 88
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->l:Z

    const/4 v0, 0x2

    .line 89
    new-array v1, v0, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 90
    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->a()Landroid/view/animation/Interpolator;

    move-result-object v2

    check-cast v2, Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v2, 0x578

    .line 91
    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 92
    invoke-virtual {v1, p1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    const/4 p1, -0x1

    .line 93
    invoke-virtual {v1, p1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 94
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/m$a;

    invoke-direct {p1, v1, p0}, Lcom/swedbank/mobile/core/ui/widget/m$a;-><init>(Landroid/animation/ValueAnimator;Lcom/swedbank/mobile/core/ui/widget/m;)V

    check-cast p1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, p1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 89
    iput-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->m:Landroid/animation/ValueAnimator;

    const/high16 p1, -0x40800000    # -1.0f

    .line 137
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->p:F

    .line 139
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->r:F

    .line 141
    new-instance p1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->t:Landroid/view/animation/AccelerateInterpolator;

    .line 142
    new-instance p1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->u:Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 143
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/h;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p1, v2, v2, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/h;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->v:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 144
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/h;

    invoke-direct {p1, v2, v2, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/h;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->w:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 145
    new-array p1, v0, [F

    fill-array-data p1, :array_1

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    .line 146
    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->a()Landroid/view/animation/Interpolator;

    move-result-object v3

    check-cast v3, Landroid/animation/TimeInterpolator;

    invoke-virtual {p1, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v3, 0x4b0

    .line 147
    invoke-virtual {p1, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 148
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/m$b;

    invoke-direct {v3, p1, p0}, Lcom/swedbank/mobile/core/ui/widget/m$b;-><init>(Landroid/animation/ValueAnimator;Lcom/swedbank/mobile/core/ui/widget/m;)V

    check-cast v3, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {p1, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 182
    move-object v3, p1

    check-cast v3, Landroid/animation/Animator;

    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/m$c;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/core/ui/widget/m$c;-><init>(Lcom/swedbank/mobile/core/ui/widget/m;)V

    check-cast v4, Lkotlin/e/a/a;

    .line 610
    new-instance v5, Lcom/swedbank/mobile/core/ui/c;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v5, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v3, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 145
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->x:Landroid/animation/ValueAnimator;

    .line 190
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/h;

    invoke-direct {p1, v2, v2, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/h;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->y:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 191
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/h;

    invoke-direct {p1, v2, v2, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/h;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->z:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 193
    new-array p1, v0, [F

    fill-array-data p1, :array_2

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    .line 194
    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->a()Landroid/view/animation/Interpolator;

    move-result-object v0

    check-cast v0, Landroid/animation/TimeInterpolator;

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v0, 0x32

    .line 195
    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 196
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/m$d;

    invoke-direct {v0, p1, p0}, Lcom/swedbank/mobile/core/ui/widget/m$d;-><init>(Landroid/animation/ValueAnimator;Lcom/swedbank/mobile/core/ui/widget/m;)V

    check-cast v0, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 207
    move-object v0, p1

    check-cast v0, Landroid/animation/Animator;

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/m$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/core/ui/widget/m$e;-><init>(Lcom/swedbank/mobile/core/ui/widget/m;)V

    check-cast v1, Lkotlin/e/a/a;

    .line 611
    new-instance v2, Lcom/swedbank/mobile/core/ui/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 193
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->B:Landroid/animation/ValueAnimator;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x40000000    # 2.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/m;F)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->p:F

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/m;Lcom/swedbank/mobile/core/ui/widget/a;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/m;Lkotlin/e/a/a;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->A:Lkotlin/e/a/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/m;Z)V
    .locals 0

    .line 62
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->n:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/m;)Z
    .locals 0

    .line 62
    iget-boolean p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->n:Z

    return p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/m;)F
    .locals 0

    .line 62
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->p:F

    return p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/m;F)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->i:F

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/m;Z)V
    .locals 0

    .line 62
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->l:Z

    return-void
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/core/ui/widget/m;)F
    .locals 0

    .line 62
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->i:F

    return p0
.end method

.method private final c()Landroid/graphics/Paint;
    .locals 2

    .line 463
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    .line 464
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 465
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 466
    iget v1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->a:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 467
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/core/ui/widget/m;F)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->q:F

    return-void
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/core/ui/widget/m;)F
    .locals 0

    .line 62
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->q:F

    return p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/core/ui/widget/m;F)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->j:F

    return-void
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/core/ui/widget/m;)F
    .locals 0

    .line 62
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->j:F

    return p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/core/ui/widget/m;F)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->k:F

    return-void
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/core/ui/widget/m;)F
    .locals 0

    .line 62
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->k:F

    return p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/core/ui/widget/m;F)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->r:F

    return-void
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/graphics/Paint;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->c:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/core/ui/widget/m;F)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->s:F

    return-void
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->m:Landroid/animation/ValueAnimator;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->x:Landroid/animation/ValueAnimator;

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->B:Landroid/animation/ValueAnimator;

    return-object p0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->v:Lcom/swedbank/mobile/core/ui/widget/h;

    return-object p0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->w:Lcom/swedbank/mobile/core/ui/widget/h;

    return-object p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/core/ui/widget/m;)Z
    .locals 0

    .line 62
    iget-boolean p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->o:Z

    return p0
.end method

.method public static final synthetic n(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->y:Lcom/swedbank/mobile/core/ui/widget/h;

    return-object p0
.end method

.method public static final synthetic o(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->z:Lcom/swedbank/mobile/core/ui/widget/h;

    return-object p0
.end method

.method public static final synthetic p(Lcom/swedbank/mobile/core/ui/widget/m;)Z
    .locals 0

    .line 62
    iget-boolean p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->l:Z

    return p0
.end method

.method public static final synthetic q(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/view/animation/AccelerateInterpolator;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->t:Landroid/view/animation/AccelerateInterpolator;

    return-object p0
.end method

.method public static final synthetic r(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/view/animation/AccelerateDecelerateInterpolator;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->u:Landroid/view/animation/AccelerateDecelerateInterpolator;

    return-object p0
.end method

.method public static final synthetic s(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/graphics/Paint;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->e:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic t(Lcom/swedbank/mobile/core/ui/widget/m;)F
    .locals 0

    .line 62
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->a:F

    return p0
.end method

.method public static final synthetic u(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/graphics/Paint;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->f:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic v(Lcom/swedbank/mobile/core/ui/widget/m;)Lkotlin/e/a/a;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->A:Lkotlin/e/a/a;

    return-object p0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 579
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->h(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 580
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->i(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 581
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->j(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 348
    sget-object v0, Lcom/swedbank/mobile/core/ui/widget/a;->b:Lcom/swedbank/mobile/core/ui/widget/a;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    .line 349
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->start()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 583
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->h(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 584
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->i(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 585
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->j(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    xor-int/lit8 v0, p1, 0x1

    .line 354
    iput-boolean v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->o:Z

    if-eqz p1, :cond_0

    .line 356
    sget-object p1, Lcom/swedbank/mobile/core/ui/widget/a;->c:Lcom/swedbank/mobile/core/ui/widget/a;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    .line 357
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->start()V

    goto :goto_0

    .line 359
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/core/ui/widget/a;->d:Lcom/swedbank/mobile/core/ui/widget/a;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    .line 360
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->v:Lcom/swedbank/mobile/core/ui/widget/h;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    .line 361
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->w:Lcom/swedbank/mobile/core/ui/widget/h;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    const/4 p1, 0x0

    .line 362
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->r:F

    const/high16 p1, 0x43b40000    # 360.0f

    .line 363
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->s:F

    .line 364
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->invalidateSelf()V

    :goto_0
    return-void
.end method

.method public final a(ZLkotlin/e/a/a;)V
    .locals 1
    .param p2    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "endAction"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 587
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->h(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 588
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->i(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 589
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->j(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    if-eqz p1, :cond_0

    .line 371
    sget-object p1, Lcom/swedbank/mobile/core/ui/widget/a;->e:Lcom/swedbank/mobile/core/ui/widget/a;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    .line 372
    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/m;->A:Lkotlin/e/a/a;

    .line 373
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->start()V

    goto :goto_0

    .line 375
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/core/ui/widget/a;->f:Lcom/swedbank/mobile/core/ui/widget/a;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    .line 376
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->invalidateSelf()V

    .line 377
    invoke-interface {p2}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 1

    .line 591
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->h(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 592
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->i(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 593
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->j(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 383
    sget-object v0, Lcom/swedbank/mobile/core/ui/widget/a;->a:Lcom/swedbank/mobile/core/ui/widget/a;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    .line 384
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->invalidateSelf()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->g:Landroid/graphics/RectF;

    if-eqz v0, :cond_7

    .line 242
    iget-object v7, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    const/4 v5, 0x0

    .line 244
    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/widget/m;->b:Landroid/graphics/Paint;

    move-object v1, p1

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 249
    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->b:Lcom/swedbank/mobile/core/ui/widget/a;

    if-eq v7, v1, :cond_0

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->c:Lcom/swedbank/mobile/core/ui/widget/a;

    if-eq v7, v1, :cond_0

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->e:Lcom/swedbank/mobile/core/ui/widget/a;

    if-ne v7, v1, :cond_1

    .line 250
    :cond_0
    iget v3, p0, Lcom/swedbank/mobile/core/ui/widget/m;->i:F

    iget v4, p0, Lcom/swedbank/mobile/core/ui/widget/m;->k:F

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/widget/m;->c:Landroid/graphics/Paint;

    move-object v1, p1

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 255
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->c:Lcom/swedbank/mobile/core/ui/widget/a;

    if-eq v7, v1, :cond_2

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->d:Lcom/swedbank/mobile/core/ui/widget/a;

    if-ne v7, v1, :cond_4

    .line 256
    :cond_2
    iget v3, p0, Lcom/swedbank/mobile/core/ui/widget/m;->r:F

    iget v4, p0, Lcom/swedbank/mobile/core/ui/widget/m;->s:F

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/widget/m;->d:Landroid/graphics/Paint;

    move-object v1, p1

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 257
    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->v:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 258
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/m;->w:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 259
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 261
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v3

    iget v9, v3, Landroid/graphics/PointF;->x:F

    .line 262
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v3

    iget v10, v3, Landroid/graphics/PointF;->y:F

    .line 263
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v3

    iget v11, v3, Landroid/graphics/PointF;->x:F

    .line 264
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v1

    iget v12, v1, Landroid/graphics/PointF;->y:F

    .line 265
    iget-object v13, p0, Lcom/swedbank/mobile/core/ui/widget/m;->e:Landroid/graphics/Paint;

    move-object v8, p1

    .line 260
    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 267
    :cond_3
    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/widget/h;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 269
    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v1

    iget v9, v1, Landroid/graphics/PointF;->x:F

    .line 270
    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v1

    iget v10, v1, Landroid/graphics/PointF;->y:F

    .line 271
    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v1

    iget v11, v1, Landroid/graphics/PointF;->x:F

    .line 272
    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v1

    iget v12, v1, Landroid/graphics/PointF;->y:F

    .line 273
    iget-object v13, p0, Lcom/swedbank/mobile/core/ui/widget/m;->e:Landroid/graphics/Paint;

    move-object v8, p1

    .line 268
    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 279
    :cond_4
    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->e:Lcom/swedbank/mobile/core/ui/widget/a;

    if-eq v7, v1, :cond_5

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->f:Lcom/swedbank/mobile/core/ui/widget/a;

    if-ne v7, v1, :cond_6

    :cond_5
    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    const/4 v5, 0x0

    .line 280
    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/widget/m;->f:Landroid/graphics/Paint;

    move-object v1, p1

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 281
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->y:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 282
    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->z:Lcom/swedbank/mobile/core/ui/widget/h;

    .line 284
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v2

    iget v4, v2, Landroid/graphics/PointF;->x:F

    .line 285
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v2

    iget v5, v2, Landroid/graphics/PointF;->y:F

    .line 286
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v2

    iget v6, v2, Landroid/graphics/PointF;->x:F

    .line 287
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v0

    iget v7, v0, Landroid/graphics/PointF;->y:F

    .line 288
    iget-object v8, p0, Lcom/swedbank/mobile/core/ui/widget/m;->f:Landroid/graphics/Paint;

    move-object v3, p1

    .line 283
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 290
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v0

    iget v3, v0, Landroid/graphics/PointF;->x:F

    .line 291
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v0

    iget v4, v0, Landroid/graphics/PointF;->y:F

    .line 292
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v0

    iget v5, v0, Landroid/graphics/PointF;->x:F

    .line 293
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a()Landroid/graphics/PointF;

    move-result-object v0

    iget v6, v0, Landroid/graphics/PointF;->y:F

    .line 294
    iget-object v7, p0, Lcom/swedbank/mobile/core/ui/widget/m;->f:Landroid/graphics/Paint;

    move-object v2, p1

    .line 289
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_6
    return-void

    :cond_7
    return-void
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isRunning()Z
    .locals 2

    .line 330
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->a:Lcom/swedbank/mobile/core/ui/widget/a;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 9
    .param p1    # Landroid/graphics/Rect;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "bounds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 301
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 303
    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/swedbank/mobile/core/ui/widget/m;->a:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 304
    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v4, p0, Lcom/swedbank/mobile/core/ui/widget/m;->a:F

    div-float/2addr v4, v3

    sub-float/2addr v1, v4

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 305
    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget v4, p0, Lcom/swedbank/mobile/core/ui/widget/m;->a:F

    div-float/2addr v4, v3

    add-float/2addr v1, v4

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 306
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float p1, p1

    iget v1, p0, Lcom/swedbank/mobile/core/ui/widget/m;->a:F

    div-float/2addr v1, v3

    sub-float/2addr p1, v1

    sub-float/2addr p1, v2

    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 527
    iget p1, v0, Landroid/graphics/RectF;->left:F

    .line 528
    iget v1, v0, Landroid/graphics/RectF;->top:F

    .line 529
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 530
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v4

    const v5, 0x3ea8f5c3    # 0.33f

    mul-float v5, v5, v3

    add-float/2addr v5, p1

    mul-float v2, v2, v4

    add-float/2addr v2, v1

    const v6, 0x3ef5c28f    # 0.48f

    mul-float v6, v6, v3

    add-float/2addr v6, p1

    const v7, 0x3f266666    # 0.65f

    mul-float v7, v7, v4

    add-float/2addr v7, v1

    const v8, 0x3f2e147b    # 0.68f

    mul-float v3, v3, v8

    add-float/2addr p1, v3

    const v3, 0x3ecccccd    # 0.4f

    mul-float v4, v4, v3

    add-float/2addr v1, v4

    .line 537
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->k(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v3

    invoke-virtual {v3, v5, v2, v6, v7}, Lcom/swedbank/mobile/core/ui/widget/h;->a(FFFF)V

    .line 542
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->l(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v2

    invoke-virtual {v2, v6, v7, p1, v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a(FFFF)V

    .line 547
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->m(Lcom/swedbank/mobile/core/ui/widget/m;)Z

    move-result p1

    const/high16 v1, 0x3f800000    # 1.0f

    if-eqz p1, :cond_0

    .line 548
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->k(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    .line 549
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->l(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    .line 554
    :cond_0
    iget p1, v0, Landroid/graphics/RectF;->left:F

    .line 555
    iget v2, v0, Landroid/graphics/RectF;->top:F

    .line 556
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 557
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v4

    const v5, 0x3ebd70a4    # 0.37f

    mul-float v6, v3, v5

    add-float/2addr v6, p1

    mul-float v5, v5, v4

    add-float/2addr v5, v2

    const v7, 0x3f2b851f    # 0.67f

    mul-float v3, v3, v7

    add-float/2addr p1, v3

    mul-float v4, v4, v7

    add-float/2addr v2, v4

    .line 562
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->n(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v3

    invoke-virtual {v3, v6, v5, p1, v2}, Lcom/swedbank/mobile/core/ui/widget/h;->a(FFFF)V

    .line 567
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->n(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    .line 568
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->o(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v3

    invoke-virtual {v3, v6, v2, p1, v5}, Lcom/swedbank/mobile/core/ui/widget/h;->a(FFFF)V

    .line 573
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->o(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    .line 302
    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->g:Landroid/graphics/RectF;

    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 314
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 315
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 316
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 317
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 323
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 324
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 325
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 326
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 327
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void
.end method

.method public start()V
    .locals 2

    .line 333
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->b:Lcom/swedbank/mobile/core/ui/widget/a;

    if-ne v0, v1, :cond_0

    .line 334
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->c:Lcom/swedbank/mobile/core/ui/widget/a;

    if-ne v0, v1, :cond_1

    .line 336
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->x:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 337
    :cond_1
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->h:Lcom/swedbank/mobile/core/ui/widget/a;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->e:Lcom/swedbank/mobile/core/ui/widget/a;

    if-ne v0, v1, :cond_2

    .line 338
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_2
    :goto_0
    return-void
.end method

.method public stop()V
    .locals 1

    .line 575
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->h(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 576
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->i(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 577
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/m;->j(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    return-void
.end method
