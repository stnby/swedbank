.class public final Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;
.super Lkotlin/e/b/k;
.source "Animations.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->performClick()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/ViewPropertyAnimator;

.field final synthetic b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;


# direct methods
.method public constructor <init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;->a:Landroid/view/ViewPropertyAnimator;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;->a:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 109
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    .line 110
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v1

    const/4 v3, 0x3

    int-to-long v3, v3

    mul-long v1, v1, v3

    .line 111
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v3

    const/4 v5, 0x2

    int-to-long v6, v5

    mul-long v3, v3, v6

    .line 112
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v6

    long-to-float v6, v6

    const/high16 v7, 0x3fc00000    # 1.5f

    mul-float v6, v6, v7

    float-to-long v6, v6

    .line 113
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v8

    div-int/2addr v8, v5

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->l(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v5

    sub-int/2addr v8, v5

    int-to-float v5, v8

    neg-float v5, v5

    .line 114
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->m(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 115
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setVisibility(I)V

    .line 116
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 117
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->o(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 118
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    .line 123
    invoke-virtual {v8, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    .line 122
    invoke-virtual {v8, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const-string v8, "rootContainer.animate()\n\u2026.translationX(rootTransX)"

    invoke-static {v5, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    new-instance v8, Lcom/swedbank/mobile/core/ui/c;

    new-instance v9, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$a;

    invoke-direct {v9, v5, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$a;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;)V

    check-cast v9, Lkotlin/e/a/a;

    invoke-direct {v8, v9}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v8, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v5, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const-string v8, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {v5, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->p(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 133
    invoke-virtual {v5, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f000000    # 0.5f

    .line 132
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x43610000    # 225.0f

    .line 131
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    .line 136
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 141
    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    .line 140
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-string v5, "innerButtonView.animate(\u2026ation)\n        .alpha(0f)"

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    new-instance v5, Lcom/swedbank/mobile/core/ui/c;

    new-instance v8, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$b;

    invoke-direct {v8, v1, v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$b;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v8, Lkotlin/e/a/a;

    invoke-direct {v5, v8}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v5, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-string v5, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->m(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 151
    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 150
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-string v2, "circleBgView.animate()\n \u2026ation)\n        .alpha(0f)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    new-instance v2, Lcom/swedbank/mobile/core/ui/c;

    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$c;

    invoke-direct {v3, v1, v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$c;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v3, Lkotlin/e/a/a;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-string v2, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 161
    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 160
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-string v2, "slideToConfirmIndicatorV\u2026ation)\n        .alpha(1f)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v0

    .line 162
    new-instance v2, Lcom/swedbank/mobile/core/ui/c;

    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$d;

    invoke-direct {v3, v1, v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k$d;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;)V

    check-cast v3, Lkotlin/e/a/a;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
