.class public final Lcom/swedbank/mobile/core/ui/widget/s$a;
.super Ljava/lang/Object;
.source "Snackbar.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/widget/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 149
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/s$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Lcom/swedbank/mobile/core/ui/widget/s;
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/s$a;

    const/4 v0, 0x0

    .line 239
    move-object v1, v0

    check-cast v1, Landroid/view/ViewGroup;

    move-object v2, v1

    move-object v1, p1

    .line 241
    :cond_0
    instance-of v3, v1, Landroidx/coordinatorlayout/widget/CoordinatorLayout;

    if-eqz v3, :cond_1

    .line 243
    check-cast v1, Landroid/view/ViewGroup;

    goto :goto_1

    .line 244
    :cond_1
    instance-of v3, v1, Landroid/widget/FrameLayout;

    if-eqz v3, :cond_3

    .line 245
    move-object v2, v1

    check-cast v2, Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getId()I

    move-result v2

    const v3, 0x1020002

    if-ne v2, v3, :cond_2

    .line 248
    check-cast v1, Landroid/view/ViewGroup;

    goto :goto_1

    .line 251
    :cond_2
    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup;

    .line 257
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 258
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_4

    check-cast v1, Landroid/view/View;

    goto :goto_0

    :cond_4
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_0

    if-eqz v2, :cond_5

    move-object v1, v2

    goto :goto_1

    .line 263
    :cond_5
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    .line 153
    :goto_1
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 154
    sget v2, Lcom/swedbank/mobile/core/ui/t$i;->widget_snackbar:I

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 155
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/s$b;

    const-string v3, "content"

    invoke-static {p1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/core/ui/widget/s$b;-><init>(Landroid/view/View;)V

    .line 156
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s;

    invoke-direct {v3, v1, p1, v2, v0}, Lcom/swedbank/mobile/core/ui/widget/s;-><init>(Landroid/view/ViewGroup;Landroid/view/View;Lcom/swedbank/mobile/core/ui/widget/s$b;Lkotlin/e/b/g;)V

    const/4 p1, -0x2

    .line 157
    invoke-virtual {v3, p1}, Lcom/swedbank/mobile/core/ui/widget/s;->setDuration(I)Lcom/google/android/material/snackbar/BaseTransientBottomBar;

    return-object v3
.end method
