.class final Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$c;
.super Lkotlin/e/b/k;
.source "SlideToConfirmButton.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$c;->a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Rect;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$c;->a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$c;->a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    .line 375
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 376
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->l(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x3fa00000    # 1.25f

    mul-float v0, v0, v3

    float-to-int v0, v0

    .line 377
    invoke-virtual {v1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 379
    iget v1, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v0

    .line 380
    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v0

    .line 381
    iget v4, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v0

    .line 382
    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v0

    .line 378
    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    return-object v2
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$c;->a()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method
