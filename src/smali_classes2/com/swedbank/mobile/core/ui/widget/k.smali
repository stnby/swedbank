.class public final Lcom/swedbank/mobile/core/ui/widget/k;
.super Landroidx/recyclerview/widget/RecyclerView$a;
.source "ListViewAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "*>;>;"
    }
.end annotation


# instance fields
.field private a:Landroidx/recyclerview/widget/f$b;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/core/ui/widget/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lkotlin/e/a/b<",
            "Landroid/view/ViewGroup;",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "*>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/util/SparseArray;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/SparseArray;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/core/ui/widget/i;",
            ">;",
            "Landroid/util/SparseArray<",
            "Lkotlin/e/a/b<",
            "Landroid/view/ViewGroup;",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "*>;>;>;)V"
        }
    .end annotation

    const-string v0, "listItems"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listItemViewCreators"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/k;->b:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/k;->c:Landroid/util/SparseArray;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    .line 16
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 21
    check-cast p2, Landroidx/recyclerview/widget/f$b;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/core/ui/widget/j;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/k;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/e/a/b;

    invoke-interface {p2, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/j;I)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "*>;I)V"
        }
    .end annotation

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/k;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-virtual {p2}, Lcom/swedbank/mobile/core/ui/widget/i;->a()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/core/ui/widget/j;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/f$b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/core/ui/widget/i;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            ")V"
        }
    .end annotation

    const-string v0, "listItems"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/k;->b:Ljava/util/List;

    .line 23
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 24
    :cond_0
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/k;->b:Ljava/util/List;

    .line 25
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    if-nez p2, :cond_1

    goto :goto_0

    .line 27
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/k;->a:Landroidx/recyclerview/widget/f$b;

    if-eq p1, p2, :cond_3

    .line 28
    move-object p1, p0

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/f$b;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    goto :goto_1

    .line 26
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/k;->notifyDataSetChanged()V

    .line 30
    :cond_3
    :goto_1
    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/k;->a:Landroidx/recyclerview/widget/f$b;

    return-void
.end method

.method public getItemCount()I
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/k;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/i;->b()I

    move-result p1

    return p1
.end method

.method public synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$x;I)V
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/j;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$x;
    .locals 0

    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/core/ui/widget/j;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$x;

    return-object p1
.end method
