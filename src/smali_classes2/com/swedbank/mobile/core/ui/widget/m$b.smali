.class final Lcom/swedbank/mobile/core/ui/widget/m$b;
.super Ljava/lang/Object;
.source "SlideToConfirmIndicatorView.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/m;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/animation/ValueAnimator;

.field final synthetic b:Lcom/swedbank/mobile/core/ui/widget/m;


# direct methods
.method constructor <init>(Landroid/animation/ValueAnimator;Lcom/swedbank/mobile/core/ui/widget/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->a:Landroid/animation/ValueAnimator;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 10

    .line 149
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_a

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 150
    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 152
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    .line 518
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->a(Lcom/swedbank/mobile/core/ui/widget/m;)Z

    move-result v3

    const/high16 v4, 0x43b40000    # 360.0f

    const/4 v5, 0x0

    if-nez v3, :cond_4

    .line 519
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->b(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v3

    const/high16 v6, -0x40800000    # -1.0f

    cmpg-float v3, v3, v6

    if-nez v3, :cond_1

    .line 520
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->c(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v3

    invoke-static {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/m;->a(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 521
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->c(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v3

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->e(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v6

    cmpl-float v7, v3, v6

    if-lez v7, :cond_0

    sub-float v3, v4, v3

    add-float/2addr v3, v6

    goto :goto_0

    :cond_0
    sub-float v3, v6, v3

    .line 525
    :goto_0
    invoke-static {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/m;->c(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    :cond_1
    const/4 v3, 0x3

    int-to-float v3, v3

    mul-float v3, v3, v1

    .line 526
    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/high16 v6, 0x40600000    # 3.5f

    mul-float v6, v6, v1

    .line 527
    invoke-static {v0, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 528
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->b(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v7

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->d(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v8

    mul-float v3, v3, v8

    add-float/2addr v7, v3

    cmpg-float v3, v7, v4

    if-gez v3, :cond_2

    goto :goto_1

    :cond_2
    sub-float/2addr v7, v4

    .line 529
    :goto_1
    invoke-static {v2, v7}, Lcom/swedbank/mobile/core/ui/widget/m;->b(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 533
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->c(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v3

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->e(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v7

    cmpl-float v8, v3, v7

    if-lez v8, :cond_3

    sub-float v3, v4, v3

    add-float/2addr v3, v7

    goto :goto_2

    :cond_3
    sub-float v3, v7, v3

    :goto_2
    const/high16 v7, 0x43960000    # 300.0f

    .line 538
    invoke-static {v7, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/m;->e(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 539
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->g(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/graphics/Paint;

    move-result-object v3

    const/4 v7, 0x0

    sub-float v6, v0, v6

    const/16 v8, 0xff

    int-to-float v9, v8

    mul-float v6, v6, v9

    float-to-int v6, v6

    invoke-static {v8, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 540
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->f(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v3

    cmpg-float v3, v3, v5

    if-nez v3, :cond_4

    const/4 v3, 0x1

    .line 541
    invoke-static {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/m;->a(Lcom/swedbank/mobile/core/ui/widget/m;Z)V

    .line 154
    :cond_4
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    iget-object v3, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v3}, Lcom/swedbank/mobile/core/ui/widget/m;->e(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v3

    const/high16 v6, 0x43070000    # 135.0f

    mul-float v6, v6, v1

    add-float/2addr v3, v6

    invoke-static {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/m;->f(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 155
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    mul-float v4, v4, v1

    invoke-static {v2, v4}, Lcom/swedbank/mobile/core/ui/widget/m;->g(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    const/high16 v2, 0x3f000000    # 0.5f

    const v3, 0x3f333333    # 0.7f

    cmpl-float v4, v1, v3

    if-ltz v4, :cond_6

    .line 158
    iget-object v4, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v4}, Lcom/swedbank/mobile/core/ui/widget/m;->q(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/view/animation/AccelerateInterpolator;

    move-result-object v4

    sub-float/2addr v1, v3

    const v3, 0x3e99999a    # 0.3f

    div-float/2addr v1, v3

    invoke-virtual {v4, v1}, Landroid/view/animation/AccelerateInterpolator;->getInterpolation(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v4, v1, v3

    .line 159
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 160
    iget-object v6, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v6}, Lcom/swedbank/mobile/core/ui/widget/m;->k(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    cmpg-float v4, v4, v0

    if-nez v4, :cond_5

    sub-float/2addr v1, v2

    mul-float v1, v1, v3

    .line 162
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 163
    iget-object v3, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v3}, Lcom/swedbank/mobile/core/ui/widget/m;->l(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    goto :goto_3

    .line 165
    :cond_5
    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v1}, Lcom/swedbank/mobile/core/ui/widget/m;->l(Lcom/swedbank/mobile/core/ui/widget/m;)Lcom/swedbank/mobile/core/ui/widget/h;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/swedbank/mobile/core/ui/widget/h;->a(F)V

    :cond_6
    :goto_3
    sub-float/2addr p1, v0

    .line 169
    invoke-static {v5, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    .line 170
    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v1}, Lcom/swedbank/mobile/core/ui/widget/m;->r(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/view/animation/AccelerateDecelerateInterpolator;

    move-result-object v1

    const v3, 0x3e4ccccd    # 0.2f

    cmpg-float v4, p1, v3

    if-ltz v4, :cond_8

    const v4, 0x3f4ccccd    # 0.8f

    cmpl-float v4, p1, v4

    if-lez v4, :cond_7

    goto :goto_4

    :cond_7
    sub-float/2addr p1, v3

    const v3, 0x3f19999a    # 0.6f

    div-float v5, p1, v3

    :cond_8
    :goto_4
    invoke-virtual {v1, v5}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result p1

    .line 175
    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v1}, Lcom/swedbank/mobile/core/ui/widget/m;->s(Lcom/swedbank/mobile/core/ui/widget/m;)Landroid/graphics/Paint;

    move-result-object v1

    const/high16 v3, 0x3f400000    # 0.75f

    cmpg-float v2, p1, v2

    if-gez v2, :cond_9

    .line 176
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->t(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v0

    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->t(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v2

    mul-float v2, v2, v3

    mul-float v2, v2, p1

    add-float/2addr v0, v2

    goto :goto_5

    .line 177
    :cond_9
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/m;->t(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v2

    iget-object v4, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v4}, Lcom/swedbank/mobile/core/ui/widget/m;->t(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v4

    mul-float v4, v4, v3

    sub-float/2addr v0, p1

    mul-float v4, v4, v0

    add-float v0, v2, v4

    .line 175
    :goto_5
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 180
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$b;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/m;->invalidateSelf()V

    return-void

    .line 149
    :cond_a
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
