.class final enum Lcom/swedbank/mobile/core/ui/widget/a;
.super Ljava/lang/Enum;
.source "SlideToConfirmIndicatorView.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/core/ui/widget/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/core/ui/widget/a;

.field public static final enum b:Lcom/swedbank/mobile/core/ui/widget/a;

.field public static final enum c:Lcom/swedbank/mobile/core/ui/widget/a;

.field public static final enum d:Lcom/swedbank/mobile/core/ui/widget/a;

.field public static final enum e:Lcom/swedbank/mobile/core/ui/widget/a;

.field public static final enum f:Lcom/swedbank/mobile/core/ui/widget/a;

.field private static final synthetic g:[Lcom/swedbank/mobile/core/ui/widget/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/swedbank/mobile/core/ui/widget/a;

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/a;

    const-string v2, "STOPPED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->a:Lcom/swedbank/mobile/core/ui/widget/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/a;

    const-string v2, "LOADING"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->b:Lcom/swedbank/mobile/core/ui/widget/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/a;

    const-string v2, "LOADING_TO_SUCCESS"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->c:Lcom/swedbank/mobile/core/ui/widget/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/a;

    const-string v2, "SUCCESS"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->d:Lcom/swedbank/mobile/core/ui/widget/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/a;

    const-string v2, "LOADING_TO_FAILURE"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->e:Lcom/swedbank/mobile/core/ui/widget/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/a;

    const-string v2, "FAILURE"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/widget/a;->f:Lcom/swedbank/mobile/core/ui/widget/a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/core/ui/widget/a;->g:[Lcom/swedbank/mobile/core/ui/widget/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 471
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/core/ui/widget/a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/core/ui/widget/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/core/ui/widget/a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/core/ui/widget/a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/core/ui/widget/a;->g:[Lcom/swedbank/mobile/core/ui/widget/a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/core/ui/widget/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/core/ui/widget/a;

    return-object v0
.end method
