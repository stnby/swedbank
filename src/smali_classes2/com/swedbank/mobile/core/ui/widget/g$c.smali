.class public final Lcom/swedbank/mobile/core/ui/widget/g$c;
.super Lcom/swedbank/mobile/core/ui/widget/g;
.source "ItemDividerDecoration.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/widget/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field private final a:Landroidx/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/c/b<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/e/a/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/q<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroidx/recyclerview/widget/RecyclerView$a<",
            "*>;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([I)V
    .locals 4
    .param p1    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewTypes"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/core/ui/widget/g;-><init>(Lkotlin/e/b/g;)V

    .line 47
    new-instance v0, Landroidx/c/b;

    array-length v1, p1

    invoke-direct {v0, v1}, Landroidx/c/b;-><init>(I)V

    .line 109
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, p1, v2

    .line 48
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroidx/c/b;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 47
    :cond_0
    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/g$c;->a:Landroidx/c/b;

    .line 51
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/g$c$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/g$c$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/g$c;)V

    check-cast p1, Lkotlin/e/a/q;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/g$c;->b:Lkotlin/e/a/q;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/g$c;)Landroidx/c/b;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/g$c;->a:Landroidx/c/b;

    return-object p0
.end method


# virtual methods
.method public a()Lkotlin/e/a/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/q<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroidx/recyclerview/widget/RecyclerView$a<",
            "*>;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/g$c;->b:Lkotlin/e/a/q;

    return-object v0
.end method
