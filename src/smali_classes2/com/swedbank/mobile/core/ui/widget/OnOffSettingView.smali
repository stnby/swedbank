.class public final Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "OnOffSettingView.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/ah;


# instance fields
.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroidx/appcompat/widget/SwitchCompat;

.field private j:Z

.field private k:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final l:Lcom/swedbank/mobile/core/ui/af;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    sget p3, Lcom/swedbank/mobile/core/ui/t$i;->widget_on_off_setting:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    .line 147
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string v1, "LayoutInflater.from(this)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 146
    invoke-virtual {p1, p3, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 34
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->on_off_setting_title:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p3, "findViewById(R.id.on_off_setting_title)"

    invoke-static {p1, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->g:Landroid/widget/TextView;

    .line 35
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->on_off_setting_subtitle:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p3, "findViewById(R.id.on_off_setting_subtitle)"

    invoke-static {p1, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->h:Landroid/widget/TextView;

    .line 36
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->on_off_setting_state:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p3, "findViewById(R.id.on_off_setting_state)"

    invoke-static {p1, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/appcompat/widget/SwitchCompat;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->i:Landroidx/appcompat/widget/SwitchCompat;

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p3, Lcom/swedbank/mobile/core/ui/t$d;->setting_item_min_height:I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setMinHeight(I)V

    if-eqz p2, :cond_2

    .line 149
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object p3, Lcom/swedbank/mobile/core/ui/t$k;->OnOffSettingView:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 151
    :try_start_0
    sget p2, Lcom/swedbank/mobile/core/ui/t$k;->OnOffSettingView_android_title:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 152
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 153
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;)Landroid/widget/TextView;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    :cond_0
    sget p2, Lcom/swedbank/mobile/core/ui/t$k;->OnOffSettingView_android_subtitle:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 156
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_1

    .line 157
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->b(Ljava/lang/CharSequence;)V

    .line 159
    :cond_1
    sget p2, Lcom/swedbank/mobile/core/ui/t$k;->OnOffSettingView_android_checked:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    .line 160
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->b(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;)Landroidx/appcompat/widget/SwitchCompat;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    .line 40
    :cond_2
    :goto_0
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView$1;-><init>(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;)V

    check-cast p1, Lkotlin/e/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->l:Lcom/swedbank/mobile/core/ui/af;

    return-void

    .line 146
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 19
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 20
    sget p3, Lcom/swedbank/mobile/core/ui/t$b;->onOffSettingStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;)Landroid/widget/TextView;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->g:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Landroid/graphics/Canvas;)V
    .locals 0

    .line 17
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 74
    move-object p1, v0

    check-cast p1, Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 75
    move-object p2, v0

    check-cast p2, Ljava/lang/CharSequence;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 76
    move-object p3, v0

    check-cast p3, Ljava/lang/Boolean;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;)Landroidx/appcompat/widget/SwitchCompat;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->i:Landroidx/appcompat/widget/SwitchCompat;

    return-object p0
.end method


# virtual methods
.method public a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/m<",
            "-",
            "Landroid/content/res/Resources;",
            "-",
            "Landroid/util/DisplayMetrics;",
            "+",
            "Lcom/swedbank/mobile/core/ui/af;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/af;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "create"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->i:Landroidx/appcompat/widget/SwitchCompat;

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    .line 112
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setVisibility(I)V

    .line 113
    iget-boolean v1, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->j:Z

    if-nez v1, :cond_0

    .line 114
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/SwitchCompat;Z)V

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    goto :goto_0

    :cond_1
    const/16 p1, 0x8

    .line 119
    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/SwitchCompat;->setVisibility(I)V

    :goto_0
    const/4 p1, 0x1

    .line 122
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->j:Z

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 78
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->b(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {p0, p3}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 88
    move-object v1, p1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p2, :cond_1

    .line 89
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    check-cast v0, Ljava/lang/CharSequence;

    .line 87
    invoke-virtual {p0, p1, v0, p3}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;)V

    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->h:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    .line 104
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->i:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v0}, Landroidx/appcompat/widget/SwitchCompat;->isChecked()Z

    move-result v0

    return v0
.end method

.method public getCachedVisibilityChange()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->k:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->l:Lcom/swedbank/mobile/core/ui/af;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Landroid/graphics/Canvas;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/core/ui/af;->a(Landroid/graphics/Canvas;Lkotlin/e/a/a;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 138
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/constraintlayout/widget/ConstraintLayout;->onSizeChanged(IIII)V

    .line 139
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/swedbank/mobile/core/ui/af;->a(II)V

    return-void
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "changedView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 134
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Landroid/view/View;I)V

    return-void
.end method

.method public setCachedVisibilityChange(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 27
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->k:Ljava/lang/Integer;

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 126
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->setEnabled(Z)V

    .line 127
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 128
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->i:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/SwitchCompat;->setEnabled(Z)V

    return-void
.end method

.method public final setLoading(Z)V
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/af;->a(Z)V

    return-void
.end method
