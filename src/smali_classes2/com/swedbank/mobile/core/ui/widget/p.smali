.class public final Lcom/swedbank/mobile/core/ui/widget/p;
.super Landroidx/recyclerview/widget/RecyclerView$h;
.source "PagerIndicator.kt"


# instance fields
.field private final a:Landroidx/recyclerview/widget/n;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:F

.field private final f:F

.field private final g:F

.field private final h:F

.field private final i:Landroid/graphics/Paint;

.field private j:Landroid/view/View;

.field private final k:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView$i;Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "layoutManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$h;-><init>()V

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/p;->k:Ljava/lang/Integer;

    .line 24
    invoke-static {p2}, Landroidx/recyclerview/widget/n;->a(Landroidx/recyclerview/widget/RecyclerView$i;)Landroidx/recyclerview/widget/n;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->a:Landroidx/recyclerview/widget/n;

    .line 32
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    const/4 p3, 0x1

    .line 33
    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 34
    sget-object p3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 32
    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->i:Landroid/graphics/Paint;

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 40
    sget p2, Lcom/swedbank/mobile/core/ui/t$d;->pager_indicator_bottom_padding:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->b:I

    .line 41
    sget p2, Lcom/swedbank/mobile/core/ui/t$c;->indicator_active_color:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->c:I

    .line 42
    sget p2, Lcom/swedbank/mobile/core/ui/t$c;->indicator_inactive_color:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->d:I

    const-string p2, "resources"

    .line 43
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 p2, 0x41000000    # 8.0f

    mul-float p1, p1, p2

    .line 44
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/p;->e:F

    .line 45
    iget p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->e:F

    const/high16 p3, 0x40000000    # 2.0f

    div-float/2addr p2, p3

    iput p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->f:F

    .line 46
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/p;->g:F

    .line 47
    iget p1, p0, Lcom/swedbank/mobile/core/ui/widget/p;->e:F

    iget p2, p0, Lcom/swedbank/mobile/core/ui/widget/p;->g:F

    add-float/2addr p1, p2

    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/p;->h:F

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView$i;Ljava/lang/Integer;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 22
    check-cast p3, Ljava/lang/Integer;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/p;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView$i;Ljava/lang/Integer;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/p;)I
    .locals 0

    .line 19
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/p;->d:I

    return p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/p;Landroid/view/View;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/p;->j:Landroid/view/View;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/p;)Ljava/lang/Integer;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/p;->k:Ljava/lang/Integer;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/core/ui/widget/p;)Landroid/view/View;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/p;->j:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$u;)V
    .locals 17
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "canvas"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "parent"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    move-object/from16 v4, p3

    invoke-static {v4, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-super/range {p0 .. p3}, Landroidx/recyclerview/widget/RecyclerView$h;->b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$u;)V

    .line 55
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$a;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 56
    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemCount()I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_0

    return-void

    .line 59
    :cond_0
    iget-object v5, v0, Lcom/swedbank/mobile/core/ui/widget/p;->i:Landroid/graphics/Paint;

    .line 60
    iget v6, v0, Lcom/swedbank/mobile/core/ui/widget/p;->e:F

    .line 61
    iget v7, v0, Lcom/swedbank/mobile/core/ui/widget/p;->f:F

    .line 62
    iget v8, v0, Lcom/swedbank/mobile/core/ui/widget/p;->g:F

    int-to-float v9, v3

    mul-float v9, v9, v6

    add-int/lit8 v10, v3, -0x1

    const/4 v11, 0x0

    .line 63
    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    int-to-float v10, v10

    mul-float v10, v10, v8

    add-float/2addr v9, v10

    .line 64
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredWidth()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    .line 185
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/widget/p;->b(Lcom/swedbank/mobile/core/ui/widget/p;)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x0

    if-eqz v9, :cond_2

    .line 186
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/widget/p;->c(Lcom/swedbank/mobile/core/ui/widget/p;)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_1

    goto :goto_0

    .line 190
    :cond_1
    move-object v9, v2

    check-cast v9, Landroid/view/ViewGroup;

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/widget/p;->b(Lcom/swedbank/mobile/core/ui/widget/p;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v9, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 191
    invoke-static {v0, v9}, Lcom/swedbank/mobile/core/ui/widget/p;->a(Lcom/swedbank/mobile/core/ui/widget/p;Landroid/view/View;)V

    goto :goto_0

    :cond_2
    move-object v9, v10

    :goto_0
    if-eqz v9, :cond_3

    .line 68
    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v6

    iget v9, v0, Lcom/swedbank/mobile/core/ui/widget/p;->b:I

    add-int/2addr v6, v9

    goto :goto_1

    .line 70
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredHeight()I

    move-result v9

    iget v12, v0, Lcom/swedbank/mobile/core/ui/widget/p;->b:I

    sub-int/2addr v9, v12

    float-to-int v6, v6

    sub-int v6, v9, v6

    :goto_1
    int-to-float v6, v6

    .line 73
    iget v9, v0, Lcom/swedbank/mobile/core/ui/widget/p;->h:F

    .line 196
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/widget/p;->a(Lcom/swedbank/mobile/core/ui/widget/p;)I

    move-result v12

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setColor(I)V

    add-float/2addr v8, v7

    move v13, v8

    const/4 v12, 0x0

    :goto_2
    if-ge v12, v3, :cond_4

    .line 199
    invoke-virtual {v1, v13, v6, v7, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-float/2addr v13, v9

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 84
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$i;

    move-result-object v3

    instance-of v12, v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-nez v12, :cond_5

    move-object v3, v10

    :cond_5
    check-cast v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v3, :cond_a

    .line 86
    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->n()I

    move-result v10

    const/4 v12, -0x1

    if-ne v10, v12, :cond_6

    return-void

    :cond_6
    add-int/lit8 v12, v10, 0x1

    .line 90
    invoke-virtual {v3, v10}, Landroidx/recyclerview/widget/LinearLayoutManager;->c(I)Landroid/view/View;

    move-result-object v13

    .line 91
    iget-object v14, v0, Lcom/swedbank/mobile/core/ui/widget/p;->a:Landroidx/recyclerview/widget/n;

    .line 92
    invoke-virtual {v14, v13}, Landroidx/recyclerview/widget/n;->e(Landroid/view/View;)I

    move-result v15

    if-nez v10, :cond_7

    .line 204
    new-instance v4, Lkotlin/g/d;

    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredWidth()I

    move-result v16

    sub-int v16, v16, v15

    sub-int v15, v15, v16

    invoke-direct {v4, v11, v15}, Lkotlin/g/d;-><init>(II)V

    goto :goto_3

    .line 206
    :cond_7
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v4, v15

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v15, v4

    .line 208
    new-instance v11, Lkotlin/g/d;

    invoke-direct {v11, v4, v15}, Lkotlin/g/d;-><init>(II)V

    move-object v4, v11

    .line 95
    :goto_3
    invoke-virtual {v14, v13}, Landroidx/recyclerview/widget/n;->a(Landroid/view/View;)I

    move-result v11

    .line 98
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v13

    invoke-virtual {v4}, Lkotlin/g/d;->i()Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    if-gt v13, v15, :cond_8

    move v12, v10

    goto :goto_6

    .line 103
    :cond_8
    invoke-virtual {v3, v12}, Landroidx/recyclerview/widget/LinearLayoutManager;->c(I)Landroid/view/View;

    move-result-object v3

    .line 104
    invoke-virtual {v14, v3}, Landroidx/recyclerview/widget/n;->e(Landroid/view/View;)I

    move-result v4

    if-nez v12, :cond_9

    .line 211
    new-instance v10, Lkotlin/g/d;

    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v2, v4

    sub-int/2addr v4, v2

    const/4 v2, 0x0

    invoke-direct {v10, v2, v4}, Lkotlin/g/d;-><init>(II)V

    :goto_4
    move-object v4, v10

    goto :goto_5

    .line 213
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v4, v2

    .line 215
    new-instance v10, Lkotlin/g/d;

    invoke-direct {v10, v2, v4}, Lkotlin/g/d;-><init>(II)V

    goto :goto_4

    .line 105
    :goto_5
    invoke-virtual {v14, v3}, Landroidx/recyclerview/widget/n;->a(Landroid/view/View;)I

    move-result v11

    .line 108
    :goto_6
    invoke-virtual {v4}, Lkotlin/g/d;->h()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int/2addr v11, v2

    int-to-float v2, v11

    invoke-virtual {v4}, Lkotlin/g/d;->i()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v4}, Lkotlin/g/d;->h()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-int/lit8 v3, v12, 0x1

    .line 112
    iget v4, v0, Lcom/swedbank/mobile/core/ui/widget/p;->c:I

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v4, 0x1

    int-to-float v4, v4

    sub-float/2addr v4, v2

    const/16 v10, 0xff

    int-to-float v10, v10

    mul-float v4, v4, v10

    float-to-int v4, v4

    .line 114
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    int-to-float v4, v12

    mul-float v4, v4, v9

    add-float/2addr v4, v8

    .line 218
    invoke-virtual {v1, v4, v6, v7, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    mul-float v2, v2, v10

    float-to-int v2, v2

    .line 124
    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    int-to-float v2, v3

    mul-float v9, v9, v2

    add-float/2addr v8, v9

    .line 221
    invoke-virtual {v1, v8, v6, v7, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void

    .line 85
    :cond_a
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Page indicator can only be used with LinearLayoutManager"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 55
    :cond_b
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Required value was null."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
