.class final Lcom/swedbank/mobile/core/ui/widget/h;
.super Ljava/lang/Object;
.source "SlideToConfirmIndicatorView.kt"


# instance fields
.field private a:F

.field private b:F

.field private final c:Landroid/graphics/PointF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Landroid/graphics/PointF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/swedbank/mobile/core/ui/widget/h;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 1
    .param p1    # Landroid/graphics/PointF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/PointF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "start"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialEnd"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/h;->e:Landroid/graphics/PointF;

    .line 531
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->b(Lcom/swedbank/mobile/core/ui/widget/h;)Landroid/graphics/PointF;

    move-result-object p1

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object p2

    iget p2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p2

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->b(Lcom/swedbank/mobile/core/ui/widget/h;)Landroid/graphics/PointF;

    move-result-object p2

    iget p2, p2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, v0

    div-float/2addr p1, p2

    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->a:F

    .line 532
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object p1

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->a(Lcom/swedbank/mobile/core/ui/widget/h;)F

    move-result p2

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    mul-float p2, p2, v0

    sub-float/2addr p1, p2

    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->b:F

    .line 483
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    invoke-virtual {p1, p2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->c:Landroid/graphics/PointF;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    .line 476
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 477
    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/h;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/h;)F
    .locals 0

    .line 475
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->a:F

    return p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/h;F)V
    .locals 0

    .line 475
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->a:F

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/h;)Landroid/graphics/PointF;
    .locals 0

    .line 475
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->e:Landroid/graphics/PointF;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/h;F)V
    .locals 0

    .line 475
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->b:F

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/PointF;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 483
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->c:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final a(F)V
    .locals 3

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-nez v0, :cond_0

    .line 487
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->c:Landroid/graphics/PointF;

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    invoke-virtual {p1, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    mul-float v1, v1, p1

    add-float/2addr v0, v1

    .line 490
    iget p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->a:F

    mul-float p1, p1, v0

    iget v1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->b:F

    add-float/2addr p1, v1

    .line 491
    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->c:Landroid/graphics/PointF;

    invoke-virtual {v1, v0, p1}, Landroid/graphics/PointF;->set(FF)V

    :goto_0
    return-void
.end method

.method public final a(FFFF)V
    .locals 1

    .line 504
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 505
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->e:Landroid/graphics/PointF;

    invoke-virtual {p1, p3, p4}, Landroid/graphics/PointF;->set(FF)V

    .line 506
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->c:Landroid/graphics/PointF;

    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    invoke-virtual {p1, p2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 522
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->b(Lcom/swedbank/mobile/core/ui/widget/h;)Landroid/graphics/PointF;

    move-result-object p1

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object p2

    iget p2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p2

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->b(Lcom/swedbank/mobile/core/ui/widget/h;)Landroid/graphics/PointF;

    move-result-object p2

    iget p2, p2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object p3

    iget p3, p3, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, p3

    div-float/2addr p1, p2

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/h;->a(Lcom/swedbank/mobile/core/ui/widget/h;F)V

    .line 527
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object p1

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->a(Lcom/swedbank/mobile/core/ui/widget/h;)F

    move-result p2

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/h;->c()Landroid/graphics/PointF;

    move-result-object p3

    iget p3, p3, Landroid/graphics/PointF;->x:F

    mul-float p2, p2, p3

    sub-float/2addr p1, p2

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/h;->b(Lcom/swedbank/mobile/core/ui/widget/h;F)V

    return-void
.end method

.method public final b()Z
    .locals 2

    .line 496
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/h;->c:Landroid/graphics/PointF;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final c()Landroid/graphics/PointF;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 476
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/h;->d:Landroid/graphics/PointF;

    return-object v0
.end method
