.class final Lcom/swedbank/mobile/core/ui/widget/l$a;
.super Lkotlin/e/b/k;
.source "ListViewAdapter.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Landroid/view/ViewGroup;",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lkotlin/e/a/m;


# direct methods
.method constructor <init>(ILkotlin/e/a/m;)V
    .locals 0

    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/l$a;->a:I

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/l$a;->b:Lkotlin/e/a/m;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Lcom/swedbank/mobile/core/ui/widget/j;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "parent.context"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget v1, p0, Lcom/swedbank/mobile/core/ui/widget/l$a;->a:I

    .line 87
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const-string v2, "LayoutInflater.from(this)"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 86
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/l$a;->b:Lkotlin/e/a/m;

    .line 67
    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/j;

    invoke-direct {v1, p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;-><init>(Landroid/view/View;Lkotlin/e/a/m;)V

    return-object v1

    .line 86
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/l$a;->a(Landroid/view/ViewGroup;)Lcom/swedbank/mobile/core/ui/widget/j;

    move-result-object p1

    return-object p1
.end method
