.class public final Lcom/swedbank/mobile/core/ui/widget/n$a;
.super Lio/reactivex/a/a;
.source "PagedRecyclerViewSelectedItemObservable.kt"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/widget/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/a/a;",
        "Landroid/view/View$OnLayoutChangeListener;"
    }
.end annotation


# instance fields
.field public final a:Landroidx/recyclerview/widget/RecyclerView$n;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public final b:Landroidx/recyclerview/widget/RecyclerView$c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private final e:Landroidx/recyclerview/widget/RecyclerView;

.field private final f:Lio/reactivex/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/u<",
            "-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lio/reactivex/u;)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Lio/reactivex/u<",
            "-TT;>;)V"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lio/reactivex/a/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->e:Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->f:Lio/reactivex/u;

    const/4 p1, -0x1

    .line 43
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->c:I

    .line 47
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/n$a$b;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/n$a$b;-><init>(Lcom/swedbank/mobile/core/ui/widget/n$a;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$n;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->a:Landroidx/recyclerview/widget/RecyclerView$n;

    .line 57
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/n$a$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/n$a$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/n$a;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$c;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->b:Landroidx/recyclerview/widget/RecyclerView$c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/n$a;)Lio/reactivex/u;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->f:Lio/reactivex/u;

    return-object p0
.end method


# virtual methods
.method public final a(Landroidx/recyclerview/widget/RecyclerView;Lio/reactivex/u;)Z
    .locals 3
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Lio/reactivex/u<",
            "-TT;>;)Z"
        }
    .end annotation

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$i;

    move-result-object v0

    instance-of v1, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v0, :cond_4

    .line 105
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$a;

    move-result-object p1

    instance-of v1, p1, Lcom/swedbank/mobile/core/ui/widget/q;

    if-nez v1, :cond_1

    move-object p1, v2

    :cond_1
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/q;

    if-eqz p1, :cond_3

    .line 108
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->o()I

    move-result v0

    .line 109
    iget v1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->c:I

    .line 110
    iput v0, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    if-eq v1, v0, :cond_2

    .line 112
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/q;->b(I)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p2, p1}, Lio/reactivex/u;->onNext(Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1

    .line 106
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "RecyclerView must have RandomAccessAdapter in order to observe selected item"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 104
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "RecyclerView must have LinearLayoutManager in order to observe selected item"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    .line 90
    iput-boolean v0, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->d:Z

    const/4 v0, -0x1

    .line 91
    iput v0, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->c:I

    return-void
.end method

.method protected c_()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->e:Landroidx/recyclerview/widget/RecyclerView;

    move-object v1, p0

    check-cast v1, Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 120
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->e:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->a:Landroidx/recyclerview/widget/RecyclerView$n;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$n;)V

    .line 121
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->e:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->b:Landroidx/recyclerview/widget/RecyclerView$c;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->unregisterAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$c;)V

    :cond_0
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 96
    iget-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->d:Z

    if-eqz p1, :cond_0

    .line 97
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->e:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->f:Lio/reactivex/u;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/n$a;->a(Landroidx/recyclerview/widget/RecyclerView;Lio/reactivex/u;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 98
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/n$a;->d:Z

    :cond_0
    return-void
.end method
