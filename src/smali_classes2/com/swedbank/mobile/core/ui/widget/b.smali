.class public final Lcom/swedbank/mobile/core/ui/widget/b;
.super Landroid/graphics/drawable/Drawable;
.source "AvatarDrawable.kt"


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:Lkotlin/d;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Rect;

.field private g:F

.field private h:F

.field private final i:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/b;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "businessDrawable"

    const-string v4, "getBusinessDrawable()Landroid/graphics/drawable/Drawable;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/core/ui/widget/b;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initials"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->i:Landroid/content/Context;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/b;->j:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/swedbank/mobile/core/ui/widget/b;->k:Z

    .line 19
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->i:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->b:Landroid/content/res/Resources;

    .line 20
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/b$a;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/core/ui/widget/b$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/b;)V

    check-cast p2, Lkotlin/e/a/a;

    invoke-static {p1, p2}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->c:Lkotlin/d;

    .line 21
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    const/4 p2, -0x1

    .line 23
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 p2, 0x41900000    # 18.0f

    .line 24
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    const/4 p2, 0x1

    .line 25
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 26
    iget-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/b;->i:Landroid/content/Context;

    sget v0, Lcom/swedbank/mobile/core/ui/t$f;->swedbank_headline_bold:I

    invoke-static {p3, v0}, Landroidx/core/a/a/f;->a(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->d:Landroid/graphics/Paint;

    .line 29
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 31
    iget-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/b;->i:Landroid/content/Context;

    sget v0, Lcom/swedbank/mobile/core/ui/t$c;->brand_brown_text_icon:I

    .line 120
    invoke-static {p3, v0}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 30
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->e:Landroid/graphics/Paint;

    .line 34
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->f:Landroid/graphics/Rect;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;ZILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const-string p2, ""

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 17
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/b;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method private final a()Landroid/graphics/drawable/Drawable;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/b;->c:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/b;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/b;)Ljava/lang/String;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/b;->j:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/b;F)V
    .locals 0

    .line 14
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->g:F

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Paint;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/b;->d:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/b;F)V
    .locals 0

    .line 14
    iput p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->h:F

    return-void
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Rect;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/b;->f:Landroid/graphics/Rect;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/content/res/Resources;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/b;->b:Landroid/content/res/Resources;

    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "initials"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/b;->j:Ljava/lang/String;

    .line 43
    iput-boolean p2, p0, Lcom/swedbank/mobile/core/ui/widget/b;->k:Z

    .line 109
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->a(Lcom/swedbank/mobile/core/ui/widget/b;)Ljava/lang/String;

    move-result-object p1

    .line 110
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->b(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Paint;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->c(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Rect;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p2, p1, v2, v0, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 111
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->c(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result p1

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/b;->a(Lcom/swedbank/mobile/core/ui/widget/b;F)V

    .line 112
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->c(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result p1

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/b;->b(Lcom/swedbank/mobile/core/ui/widget/b;F)V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 67
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 68
    iget-object v3, p0, Lcom/swedbank/mobile/core/ui/widget/b;->d:Landroid/graphics/Paint;

    .line 70
    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v4

    int-to-float v5, v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    .line 71
    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 72
    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v7

    cmpg-float v4, v4, v7

    if-eqz v4, :cond_0

    .line 114
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->a(Lcom/swedbank/mobile/core/ui/widget/b;)Ljava/lang/String;

    move-result-object v4

    .line 115
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->b(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Paint;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->c(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v7, v4, v8, v9, v10}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 116
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->c(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    invoke-static {p0, v4}, Lcom/swedbank/mobile/core/ui/widget/b;->a(Lcom/swedbank/mobile/core/ui/widget/b;F)V

    .line 117
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->c(Lcom/swedbank/mobile/core/ui/widget/b;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v4

    invoke-static {p0, v4}, Lcom/swedbank/mobile/core/ui/widget/b;->b(Lcom/swedbank/mobile/core/ui/widget/b;F)V

    :cond_0
    int-to-float v0, v0

    div-float/2addr v0, v6

    int-to-float v1, v1

    div-float/2addr v1, v6

    .line 77
    iget-boolean v4, p0, Lcom/swedbank/mobile/core/ui/widget/b;->k:Z

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/b;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 78
    div-int/lit8 v5, v2, 0x2

    int-to-float v5, v5

    sub-float v5, v0, v5

    float-to-int v5, v5

    float-to-double v6, v1

    const-wide v8, 0x3fe23d70a3d70a3dL    # 0.57

    int-to-double v10, v2

    mul-double v10, v10, v8

    sub-double/2addr v6, v10

    double-to-int v6, v6

    add-int v7, v5, v2

    add-int/2addr v2, v6

    .line 80
    invoke-virtual {v4, v5, v6, v7, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 81
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 83
    :cond_1
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/b;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v5, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 86
    :goto_0
    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/widget/b;->j:Ljava/lang/String;

    .line 87
    iget v4, p0, Lcom/swedbank/mobile/core/ui/widget/b;->g:F

    sub-float/2addr v0, v4

    .line 88
    iget v4, p0, Lcom/swedbank/mobile/core/ui/widget/b;->h:F

    sub-float/2addr v1, v4

    .line 85
    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/b;->b:Landroid/content/res/Resources;

    sget v1, Lcom/swedbank/mobile/core/ui/t$d;->status_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/b;->b:Landroid/content/res/Resources;

    sget v1, Lcom/swedbank/mobile/core/ui/t$d;->status_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    return-void
.end method
