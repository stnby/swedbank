.class final Lcom/swedbank/mobile/core/ui/widget/g$b$a;
.super Lkotlin/e/b/k;
.source "ItemDividerDecoration.kt"

# interfaces
.implements Lkotlin/e/a/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/widget/g$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/q<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "*>;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/core/ui/widget/g$b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/g$b$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/core/ui/widget/g$b$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/core/ui/widget/g$b$a;->a:Lcom/swedbank/mobile/core/ui/widget/g$b$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/g$b$a;->a(IILandroidx/recyclerview/widget/RecyclerView$a;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final a(IILandroidx/recyclerview/widget/RecyclerView$a;)Z
    .locals 1
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroidx/recyclerview/widget/RecyclerView$a<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "adapter"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-ge p1, p2, :cond_0

    add-int/lit8 p2, p1, 0x1

    .line 27
    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemViewType(I)I

    move-result p2

    .line 28
    invoke-virtual {p3, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemViewType(I)I

    move-result p1

    if-ne p2, p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
