.class public final Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;
.super Lkotlin/e/b/k;
.source "Animations.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->onTouchEvent(Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/ViewPropertyAnimator;

.field final synthetic b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

.field final synthetic c:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;


# direct methods
.method public constructor <init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->a:Landroid/view/ViewPropertyAnimator;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->c:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->a:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 109
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v2, "mainBgView.animate()"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v2, "innerButtonView.animate()"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 111
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v2, "textView.animate()"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 114
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->c:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Z)V

    return-void
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
