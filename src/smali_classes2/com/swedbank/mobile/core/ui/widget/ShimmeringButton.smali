.class public final Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
.super Landroidx/appcompat/widget/g;
.source "ShimmeringButton.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/ah;


# instance fields
.field private a:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/core/ui/af;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    .line 22
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setClipToOutline(Z)V

    .line 23
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton$1;-><init>(Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;)V

    check-cast p1, Lkotlin/e/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->b:Lcom/swedbank/mobile/core/ui/af;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 15
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 16
    sget p3, Lcom/swedbank/mobile/core/ui/t$b;->buttonStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;Landroid/graphics/Canvas;)V
    .locals 0

    .line 13
    invoke-super {p0, p1}, Landroidx/appcompat/widget/g;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/m<",
            "-",
            "Landroid/content/res/Resources;",
            "-",
            "Landroid/util/DisplayMetrics;",
            "+",
            "Lcom/swedbank/mobile/core/ui/af;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/af;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "create"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    return-object p1
.end method

.method public getCachedVisibilityChange()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->b:Lcom/swedbank/mobile/core/ui/af;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;Landroid/graphics/Canvas;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/core/ui/af;->a(Landroid/graphics/Canvas;Lkotlin/e/a/a;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 47
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/appcompat/widget/g;->onSizeChanged(IIII)V

    .line 48
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/swedbank/mobile/core/ui/af;->a(II)V

    return-void
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "changedView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/g;->onVisibilityChanged(Landroid/view/View;I)V

    .line 43
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Landroid/view/View;I)V

    return-void
.end method

.method public setCachedVisibilityChange(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->a:Ljava/lang/Integer;

    return-void
.end method

.method public final setShimmering(Z)V
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/af;->a(Z)V

    return-void
.end method
