.class public final Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;
.super Landroid/widget/FrameLayout;
.source "SlideToConfirmButton.kt"


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:J

.field private final k:J

.field private final l:J

.field private final m:I

.field private final n:Lkotlin/d;

.field private final o:Lkotlin/d;

.field private final p:Lkotlin/d;

.field private q:Z

.field private r:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private s:Landroid/view/ViewGroup;

.field private t:Z

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0xb

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "rootContainer"

    const-string v4, "getRootContainer()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "mainBgView"

    const-string v4, "getMainBgView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "endBgView"

    const-string v4, "getEndBgView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "textView"

    const-string v4, "getTextView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "innerButtonView"

    const-string v4, "getInnerButtonView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "innerButtonImageView"

    const-string v4, "getInnerButtonImageView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "circleBgView"

    const-string v4, "getCircleBgView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "slideToConfirmIndicatorView"

    const-string v4, "getSlideToConfirmIndicatorView()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "innerButtonTouchBounds"

    const-string v4, "getInnerButtonTouchBounds()Landroid/graphics/Rect;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "maxTouchTranslationX"

    const-string v4, "getMaxTouchTranslationX()I"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "shakeAnimation"

    const-string v4, "getShakeAnimation()Landroid/view/animation/Animation;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_root:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b:Lkotlin/f/c;

    .line 27
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_main_bg:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->c:Lkotlin/f/c;

    .line 28
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_end_bg:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->d:Lkotlin/f/c;

    .line 29
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_text:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->e:Lkotlin/f/c;

    .line 30
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_inner_button:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f:Lkotlin/f/c;

    .line 31
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_confirm_button_image:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g:Lkotlin/f/c;

    .line 32
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_circle_bg:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->h:Lkotlin/f/c;

    .line 33
    sget p3, Lcom/swedbank/mobile/core/ui/t$g;->slide_to_confirm_indicator:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i:Lkotlin/f/c;

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/swedbank/mobile/core/ui/t$h;->anim_default_dur:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    int-to-long v0, p3

    iput-wide v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j:J

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/swedbank/mobile/core/ui/t$h;->anim_short_dur:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    int-to-long v0, p3

    iput-wide v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k:J

    .line 37
    iget-wide v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k:J

    const/4 p3, 0x2

    int-to-long v2, p3

    mul-long v0, v0, v2

    iput-wide v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->l:J

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/swedbank/mobile/core/ui/t$d;->slide_to_confirm_button_half_height:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    iput p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->m:I

    .line 39
    sget-object p3, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$c;-><init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p3, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n:Lkotlin/d;

    .line 40
    sget-object p3, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$d;-><init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p3, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->o:Lkotlin/d;

    .line 41
    sget-object p3, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$l;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$l;-><init>(Landroid/content/Context;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p3, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->p:Lkotlin/d;

    .line 50
    sget p3, Lcom/swedbank/mobile/core/ui/t$i;->widget_slide_to_confirm:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    .line 674
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string v1, "LayoutInflater.from(this)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 673
    invoke-virtual {p1, p3, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 676
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object p3, Lcom/swedbank/mobile/core/ui/t$k;->SlideToConfirmButton:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 678
    :try_start_0
    sget p2, Lcom/swedbank/mobile/core/ui/t$k;->SlideToConfirmButton_android_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 679
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 680
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    :cond_0
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    :cond_1
    :goto_0
    return-void

    .line 673
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 23
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 24
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getTextView()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Z)V
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J
    .locals 2

    .line 21
    iget-wide v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j:J

    return-wide v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    return p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lkotlin/e/a/a;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->r:Lkotlin/e/a/a;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I
    .locals 0

    .line 21
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->u:I

    return p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getMaxTouchTranslationX()I

    move-result p0

    return p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getMainBgView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final getCircleBgView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getEndBgView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getInnerButtonImageView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getInnerButtonTouchBounds()Landroid/graphics/Rect;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    return-object v0
.end method

.method private final getInnerButtonView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getMainBgView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getMaxTouchTranslationX()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->o:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method private final getRootContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getShakeAnimation()Landroid/view/animation/Animation;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->p:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    return-object v0
.end method

.method private final getSlideToConfirmIndicatorView()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    return-object v0
.end method

.method private final getTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J
    .locals 2

    .line 21
    iget-wide v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->l:J

    return-wide v0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getInnerButtonView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J
    .locals 2

    .line 21
    iget-wide v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k:J

    return-wide v0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getRootContainer()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I
    .locals 0

    .line 21
    iget p0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->m:I

    return p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getCircleBgView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic n(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getSlideToConfirmIndicatorView()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic o(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getEndBgView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic p(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getInnerButtonImageView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic q(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/animation/Animation;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getShakeAnimation()Landroid/view/animation/Animation;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic r(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->t:Z

    return p0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .line 69
    iget-boolean v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getRootContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-nez v0, :cond_0

    .line 376
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v0

    int-to-float v0, v0

    .line 377
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v2

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->l(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    neg-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 378
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v2

    .line 379
    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationX(F)V

    const/16 v3, 0x8

    .line 380
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 382
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->o(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 383
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->m(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v2

    .line 384
    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    .line 385
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 387
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->p(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v2

    const/high16 v4, 0x3f000000    # 0.5f

    .line 388
    invoke-virtual {v2, v4}, Landroid/view/View;->setAlpha(F)V

    const/high16 v4, 0x43610000    # 225.0f

    .line 389
    invoke-virtual {v2, v4}, Landroid/view/View;->setRotation(F)V

    .line 391
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v2

    .line 392
    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    .line 393
    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 394
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 396
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v1

    neg-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 397
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 398
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setAlpha(F)V

    const/4 v1, 0x0

    .line 399
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setVisibility(I)V

    .line 401
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 6

    .line 75
    iget-boolean v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    if-nez v0, :cond_2

    .line 406
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    cmpg-float v0, v0, v2

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->r(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 77
    iput-boolean v1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    const/4 v0, 0x0

    .line 78
    check-cast v0, Lkotlin/e/a/a;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->r:Lkotlin/e/a/a;

    .line 79
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getSlideToConfirmIndicatorView()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a(Z)V

    goto/16 :goto_1

    .line 81
    :cond_1
    iput-boolean v3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->t:Z

    .line 413
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v0

    int-to-float v0, v0

    .line 414
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->l(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    neg-float v4, v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 415
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    .line 416
    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    const/16 v4, 0x8

    .line 417
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 419
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->o(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 420
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->m(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    .line 421
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 422
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 424
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->p(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    const/high16 v5, 0x3f000000    # 0.5f

    .line 425
    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    const/high16 v5, 0x43610000    # 225.0f

    .line 426
    invoke-virtual {v1, v5}, Landroid/view/View;->setRotation(F)V

    .line 428
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    .line 429
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 430
    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 431
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 433
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v1

    neg-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 434
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 435
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setAlpha(F)V

    .line 436
    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setVisibility(I)V

    .line 438
    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a(Z)V

    goto :goto_1

    .line 85
    :cond_2
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$g;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$g;-><init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v0, Lkotlin/e/a/a;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->r:Lkotlin/e/a/a;

    :goto_1
    return-void
.end method

.method public final c()V
    .locals 3

    .line 90
    iget-boolean v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    if-nez v0, :cond_1

    .line 443
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    cmpg-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->r(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 92
    iput-boolean v2, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    const/4 v0, 0x0

    .line 93
    check-cast v0, Lkotlin/e/a/a;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->r:Lkotlin/e/a/a;

    .line 94
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getSlideToConfirmIndicatorView()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e;-><init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-virtual {v0, v2, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->a(ZLkotlin/e/a/a;)V

    goto :goto_1

    .line 105
    :cond_1
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$f;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$f;-><init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v0, Lkotlin/e/a/a;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->r:Lkotlin/e/a/a;

    :cond_2
    :goto_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 120
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_3

    .line 129
    :pswitch_0
    iget-boolean p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->t:Z

    if-eqz p1, :cond_7

    .line 130
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getMaxTouchTranslationX()I

    move-result p1

    iget v1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->u:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    int-to-float p1, p1

    .line 131
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getMainBgView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationX(F)V

    .line 132
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getInnerButtonView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationX(F)V

    .line 133
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getTextView()Landroid/widget/TextView;

    move-result-object v0

    neg-float p1, p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTranslationX(F)V

    goto/16 :goto_3

    .line 137
    :pswitch_1
    iget-boolean v1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->t:Z

    const v2, 0x3e4ccccd    # 0.2f

    if-eqz v1, :cond_4

    .line 138
    iput-boolean v4, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->t:Z

    .line 139
    iput-boolean v5, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    .line 447
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->e(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v1

    sub-int v1, v0, v1

    int-to-float v1, v1

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v1, v6

    const v6, 0x3f59999a    # 0.85f

    cmpl-float v1, v1, v6

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->performClick()Z

    goto/16 :goto_2

    .line 448
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->e(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result p1

    sub-int/2addr v0, p1

    int-to-float p1, v0

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr p1, v0

    const v0, 0x3dcccccd    # 0.1f

    cmpg-float p1, p1, v0

    if-gez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    .line 142
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$i;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$i;-><init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast p1, Lkotlin/e/a/a;

    .line 454
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v0

    int-to-float v0, v0

    mul-float v0, v0, v2

    .line 455
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 458
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 457
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 460
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 463
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 462
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 465
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 470
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    neg-float v0, v0

    .line 469
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "textView.animate()\n     \u2026     .translationX(-endX)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 471
    new-instance v1, Lcom/swedbank/mobile/core/ui/c;

    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$a;

    invoke-direct {v2, v0, p0, p1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$a;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Lkotlin/e/a/a;)V

    check-cast v2, Lkotlin/e/a/a;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const-string v0, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 477
    :cond_3
    new-instance p1, Lcom/swedbank/mobile/core/ui/d;

    const/4 v0, 0x2

    invoke-direct {p1, v0, v3}, Lcom/swedbank/mobile/core/ui/d;-><init>(IF)V

    .line 478
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    .line 479
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    sub-float/2addr v0, v2

    .line 480
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->h(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v1

    long-to-float v1, v1

    mul-float v0, v0, v1

    .line 481
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->h(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v1

    float-to-long v6, v0

    add-long/2addr v1, v6

    .line 482
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 487
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 486
    check-cast p1, Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 485
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 490
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 495
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 494
    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 493
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 498
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 505
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 504
    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 503
    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const-string v0, "textView.animate()\n     \u2026        .translationX(0f)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 506
    new-instance v0, Lcom/swedbank/mobile/core/ui/c;

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;

    invoke-direct {v1, p1, p0, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$h;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const-string v0, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    :goto_2
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->s:Landroid/view/ViewGroup;

    if-eqz p1, :cond_7

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_3

    .line 513
    :cond_4
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->c(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Z

    move-result p1

    if-nez p1, :cond_5

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result p1

    cmpg-float p1, p1, v3

    if-nez p1, :cond_5

    const/4 v4, 0x1

    :cond_5
    if-eqz v4, :cond_7

    .line 149
    iput-boolean v5, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->q:Z

    .line 150
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$j;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$j;-><init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast p1, Lkotlin/e/a/a;

    .line 519
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v0

    int-to-float v0, v0

    mul-float v0, v0, v2

    .line 520
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 523
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 522
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 525
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 528
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 527
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 530
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 535
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    neg-float v0, v0

    .line 534
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "textView.animate()\n     \u2026     .translationX(-endX)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 536
    new-instance v1, Lcom/swedbank/mobile/core/ui/c;

    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;

    invoke-direct {v2, v0, p0, p1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Lkotlin/e/a/a;)V

    check-cast v2, Lkotlin/e/a/a;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const-string v0, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 446
    :pswitch_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->c(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Z

    move-result p1

    if-nez p1, :cond_6

    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result p1

    cmpg-float p1, p1, v3

    if-nez p1, :cond_6

    const/4 v4, 0x1

    :cond_6
    if-eqz v4, :cond_7

    .line 122
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->getInnerButtonTouchBounds()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 123
    iput-boolean v5, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->t:Z

    .line 124
    iput v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->u:I

    .line 125
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->s:Landroid/view/ViewGroup;

    if-eqz p1, :cond_7

    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    :cond_7
    :goto_3
    return v5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public performClick()Z
    .locals 4

    .line 542
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v0

    int-to-float v0, v0

    .line 543
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 546
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 545
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 548
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 551
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 550
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 553
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 558
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->j(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    neg-float v0, v0

    .line 557
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "textView.animate()\n     \u2026     .translationX(-endX)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 559
    new-instance v1, Lcom/swedbank/mobile/core/ui/c;

    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;

    invoke-direct {v2, v0, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$k;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)V

    check-cast v2, Lkotlin/e/a/a;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-super {p0}, Landroid/widget/FrameLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 169
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    if-eqz p1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    const/high16 p1, 0x3f000000    # 0.5f

    .line 170
    :goto_0
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->setAlpha(F)V

    return-void
.end method

.method public final setScrollParent(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "scrollParent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->s:Landroid/view/ViewGroup;

    return-void
.end method
