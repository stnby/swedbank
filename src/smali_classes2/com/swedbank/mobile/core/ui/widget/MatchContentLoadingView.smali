.class public final Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;
.super Landroid/view/View;
.source "MatchContentLoadingView.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/ah;


# instance fields
.field private a:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/core/ui/af;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView$1;-><init>(Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;)V

    check-cast p1, Lkotlin/e/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;->a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;->b:Lcom/swedbank/mobile/core/ui/af;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 13
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 14
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;Landroid/graphics/Canvas;)V
    .locals 0

    .line 11
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/m<",
            "-",
            "Landroid/content/res/Resources;",
            "-",
            "Landroid/util/DisplayMetrics;",
            "+",
            "Lcom/swedbank/mobile/core/ui/af;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/af;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "create"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    return-object p1
.end method

.method public getCachedVisibilityChange()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;->b:Lcom/swedbank/mobile/core/ui/af;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;Landroid/graphics/Canvas;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/core/ui/af;->a(Landroid/graphics/Canvas;Lkotlin/e/a/a;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 40
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 41
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/swedbank/mobile/core/ui/af;->a(II)V

    return-void
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "changedView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 36
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Landroid/view/View;I)V

    return-void
.end method

.method public setCachedVisibilityChange(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/MatchContentLoadingView;->a:Ljava/lang/Integer;

    return-void
.end method
