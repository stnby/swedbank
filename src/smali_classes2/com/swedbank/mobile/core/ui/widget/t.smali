.class public final Lcom/swedbank/mobile/core/ui/widget/t;
.super Ljava/lang/Object;
.source "Snackbar.kt"


# instance fields
.field private a:Lcom/swedbank/mobile/core/ui/widget/s;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lio/reactivex/b/c;

.field private final d:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/t;->d:Landroid/view/View;

    .line 211
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/t;->b:Lcom/b/c/c;

    .line 212
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "Disposables.disposed()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/t;->c:Lio/reactivex/b/c;

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/t;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Lkotlin/e/a/b;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/core/ui/widget/s;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/core/ui/widget/s;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "shouldSkipShow"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "render"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/t;->a:Lcom/swedbank/mobile/core/ui/widget/s;

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    return-void

    .line 223
    :cond_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 224
    sget-object p1, Lcom/swedbank/mobile/core/ui/widget/s;->a:Lcom/swedbank/mobile/core/ui/widget/s$a;

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/t;->d:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/s$a;->a(Landroid/view/View;)Lcom/swedbank/mobile/core/ui/widget/s;

    move-result-object p1

    .line 226
    invoke-interface {p2, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/s;->show()V

    .line 228
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/s;->a()Lio/reactivex/o;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/t;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p2, v0}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p2

    const-string v0, "clicks().subscribe(actionClickStream)"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/t;->c:Lio/reactivex/b/c;

    .line 225
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/t;->a:Lcom/swedbank/mobile/core/ui/widget/s;

    return-void
.end method

.method public final b()V
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/t;->c:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 234
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/t;->a:Lcom/swedbank/mobile/core/ui/widget/s;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/s;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    .line 235
    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/s;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/t;->a:Lcom/swedbank/mobile/core/ui/widget/s;

    return-void
.end method
