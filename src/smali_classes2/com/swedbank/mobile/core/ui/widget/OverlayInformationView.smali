.class public final Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;
.super Landroid/widget/FrameLayout;
.source "OverlayInformationView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    sget p2, Lcom/swedbank/mobile/core/ui/t$i;->widget_snackbar:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    .line 69
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string v0, "LayoutInflater.from(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 68
    invoke-virtual {p1, p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 26
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_title:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.snackbar_title)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->a:Landroid/widget/TextView;

    .line 27
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_subtitle:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.snackbar_subtitle)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->b:Landroid/widget/TextView;

    .line 28
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_close_btn:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.snackbar_close_btn)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->c:Landroid/view/View;

    .line 29
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_action_btn:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.snackbar_action_btn)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->d:Landroid/widget/Button;

    return-void

    .line 68
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 16
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 17
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 34
    check-cast p2, Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    sget-object p3, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$c;->a:Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$c;

    check-cast p3, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;)V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->c:Landroid/view/View;

    .line 66
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    check-cast v1, Lio/reactivex/s;

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->d:Landroid/widget/Button;

    check-cast v0, Landroid/view/View;

    .line 67
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 32
    invoke-static {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(closeBt\u2026ks(), actionBtn.clicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 p1, 0x8

    if-nez p2, :cond_0

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    sget-object p2, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$c;->a:Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$c;

    invoke-static {p3, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    .line 42
    sget p2, Lcom/swedbank/mobile/core/ui/t$c;->brand_turqoise:I

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setBackgroundResource(I)V

    .line 43
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->c:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 44
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->d:Landroid/widget/Button;

    invoke-virtual {p2, p1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 46
    :cond_1
    sget-object p2, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$b;->a:Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$b;

    invoke-static {p3, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 47
    sget p2, Lcom/swedbank/mobile/core/ui/t$c;->label_red:I

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setBackgroundResource(I)V

    .line 48
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->c:Landroid/view/View;

    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->d:Landroid/widget/Button;

    invoke-virtual {p2, p1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 51
    :cond_2
    instance-of p2, p3, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;

    if-eqz p2, :cond_3

    .line 52
    sget p2, Lcom/swedbank/mobile/core/ui/t$c;->brand_turqoise:I

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setBackgroundResource(I)V

    .line 53
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->c:Landroid/view/View;

    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->d:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 55
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->d:Landroid/widget/Button;

    check-cast p3, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;

    invoke-virtual {p3}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;->a()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    return-void
.end method
