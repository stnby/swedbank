.class public final Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;
.super Landroid/widget/FrameLayout;
.source "EnableSettingView.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/ah;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private b:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/core/ui/af;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    sget p3, Lcom/swedbank/mobile/core/ui/t$i;->widget_enable_setting:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    .line 91
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string v1, "LayoutInflater.from(this)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 90
    invoke-virtual {p1, p3, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 29
    sget p1, Lcom/swedbank/mobile/core/ui/t$g;->enable_setting_title:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p3, "findViewById(R.id.enable_setting_title)"

    invoke-static {p1, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a:Landroid/widget/TextView;

    if-eqz p2, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object p3, Lcom/swedbank/mobile/core/ui/t$k;->EnableSettingView:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 95
    :try_start_0
    sget p2, Lcom/swedbank/mobile/core/ui/t$k;->EnableSettingView_android_title:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 96
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 97
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;)Landroid/widget/TextView;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :cond_0
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    .line 31
    :cond_1
    :goto_0
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView$1;-><init>(Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;)V

    check-cast p1, Lkotlin/e/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->c:Lcom/swedbank/mobile/core/ui/af;

    return-void

    .line 90
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 20
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 21
    sget p3, Lcom/swedbank/mobile/core/ui/t$b;->enableSettingStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;)Landroid/widget/TextView;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;Landroid/graphics/Canvas;)V
    .locals 0

    .line 18
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/m<",
            "-",
            "Landroid/content/res/Resources;",
            "-",
            "Landroid/util/DisplayMetrics;",
            "+",
            "Lcom/swedbank/mobile/core/ui/af;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/af;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "create"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 60
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->setEnabled(Z)V

    if-eqz p1, :cond_0

    .line 86
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public getCachedVisibilityChange()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->c:Lcom/swedbank/mobile/core/ui/af;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;Landroid/graphics/Canvas;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/core/ui/af;->a(Landroid/graphics/Canvas;Lkotlin/e/a/a;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 77
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 78
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/swedbank/mobile/core/ui/af;->a(II)V

    return-void
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "changedView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 73
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/ah$a;->a(Lcom/swedbank/mobile/core/ui/ah;Landroid/view/View;I)V

    return-void
.end method

.method public setCachedVisibilityChange(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->b:Ljava/lang/Integer;

    return-void
.end method

.method public final setLoading(Z)V
    .locals 1

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/af;->a(Z)V

    return-void
.end method
