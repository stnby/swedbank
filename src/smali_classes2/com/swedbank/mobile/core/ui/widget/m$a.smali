.class final Lcom/swedbank/mobile/core/ui/widget/m$a;
.super Ljava/lang/Object;
.source "SlideToConfirmIndicatorView.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/m;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/animation/ValueAnimator;

.field final synthetic b:Lcom/swedbank/mobile/core/ui/widget/m;


# direct methods
.method constructor <init>(Landroid/animation/ValueAnimator;Lcom/swedbank/mobile/core/ui/widget/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->a:Landroid/animation/ValueAnimator;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 7

    .line 95
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_8

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->p(Lcom/swedbank/mobile/core/ui/widget/m;)Z

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    if-eqz v0, :cond_0

    cmpl-float v0, p1, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/swedbank/mobile/core/ui/widget/m;->b(Lcom/swedbank/mobile/core/ui/widget/m;Z)V

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->p(Lcom/swedbank/mobile/core/ui/widget/m;)Z

    move-result v0

    if-nez v0, :cond_1

    cmpg-float v0, p1, v1

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/swedbank/mobile/core/ui/widget/m;->b(Lcom/swedbank/mobile/core/ui/widget/m;Z)V

    .line 100
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->p(Lcom/swedbank/mobile/core/ui/widget/m;)Z

    move-result v0

    const/high16 v2, 0x43870000    # 270.0f

    const/high16 v3, 0x41f00000    # 30.0f

    const/high16 v4, 0x43340000    # 180.0f

    const/4 v5, 0x2

    const/high16 v6, 0x43b40000    # 360.0f

    if-eqz v0, :cond_3

    int-to-float v0, v5

    mul-float p1, p1, v0

    .line 103
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    mul-float v4, v4, p1

    invoke-static {v0, v4}, Lcom/swedbank/mobile/core/ui/widget/m;->b(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->c(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v0

    add-float/2addr v0, v3

    mul-float p1, p1, v2

    add-float/2addr v0, p1

    .line 107
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    cmpg-float v1, v0, v6

    if-gez v1, :cond_2

    goto :goto_1

    :cond_2
    sub-float/2addr v0, v6

    :goto_1
    invoke-static {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/m;->d(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    goto :goto_4

    :cond_3
    int-to-float v0, v5

    sub-float/2addr p1, v1

    mul-float v0, v0, p1

    const/high16 p1, 0x42f00000    # 120.0f

    mul-float v2, v2, v0

    add-float/2addr v2, p1

    .line 115
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    cmpg-float v1, v2, v6

    if-gez v1, :cond_4

    goto :goto_2

    :cond_4
    sub-float/2addr v2, v6

    :goto_2
    invoke-static {p1, v2}, Lcom/swedbank/mobile/core/ui/widget/m;->d(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    const/high16 p1, 0x44070000    # 540.0f

    mul-float v0, v0, p1

    add-float/2addr v0, v4

    .line 122
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    cmpg-float v1, v0, v6

    if-gez v1, :cond_5

    goto :goto_3

    :cond_5
    const/high16 v1, 0x44340000    # 720.0f

    cmpg-float v2, v0, v1

    if-gez v2, :cond_6

    sub-float/2addr v0, v6

    goto :goto_3

    :cond_6
    sub-float/2addr v0, v1

    :goto_3
    invoke-static {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/m;->b(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 128
    :goto_4
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/m;->c(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v0

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-static {v1}, Lcom/swedbank/mobile/core/ui/widget/m;->e(Lcom/swedbank/mobile/core/ui/widget/m;)F

    move-result v1

    cmpl-float v2, v0, v1

    if-lez v2, :cond_7

    sub-float/2addr v6, v0

    add-float/2addr v6, v1

    goto :goto_5

    :cond_7
    sub-float v6, v1, v0

    :goto_5
    const/high16 v0, 0x43960000    # 300.0f

    .line 524
    invoke-static {v0, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/m;->e(Lcom/swedbank/mobile/core/ui/widget/m;F)V

    .line 130
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/m$a;->b:Lcom/swedbank/mobile/core/ui/widget/m;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/m;->invalidateSelf()V

    return-void

    .line 95
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
