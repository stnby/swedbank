.class public final Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;
.super Lkotlin/e/b/k;
.source "Animations.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/ViewPropertyAnimator;

.field final synthetic b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

.field final synthetic c:Lkotlin/e/a/a;


# direct methods
.method public constructor <init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Lkotlin/e/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;->a:Landroid/view/ViewPropertyAnimator;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;->c:Lkotlin/e/a/a;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;->a:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 109
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;->c:Lkotlin/e/a/a;

    .line 110
    new-instance v2, Lcom/swedbank/mobile/core/ui/d;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v2, v4, v3}, Lcom/swedbank/mobile/core/ui/d;-><init>(IF)V

    .line 111
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTranslationX()F

    move-result v4

    .line 112
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->f(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    const v5, 0x3e4ccccd    # 0.2f

    sub-float/2addr v4, v5

    .line 113
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->h(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v5

    long-to-float v5, v5

    mul-float v4, v4, v5

    .line 114
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->h(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v5

    float-to-long v7, v4

    add-long/2addr v5, v7

    .line 115
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 120
    invoke-virtual {v4, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 119
    check-cast v2, Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 118
    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 123
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 128
    invoke-virtual {v4, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 127
    invoke-virtual {v4, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 126
    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 131
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 138
    invoke-virtual {v4, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 137
    invoke-virtual {v4, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 136
    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-string v3, "textView.animate()\n     \u2026        .translationX(0f)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    new-instance v3, Lcom/swedbank/mobile/core/ui/c;

    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b$a;

    invoke-direct {v4, v2, v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b$a;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;Lkotlin/e/a/a;)V

    check-cast v4, Lkotlin/e/a/a;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$b;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
