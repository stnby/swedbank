.class final Lcom/swedbank/mobile/core/ui/widget/g$c$a;
.super Lkotlin/e/b/k;
.source "ItemDividerDecoration.kt"

# interfaces
.implements Lkotlin/e/a/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/q<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "*>;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/core/ui/widget/g$c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/core/ui/widget/g$c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/g$c$a;->a:Lcom/swedbank/mobile/core/ui/widget/g$c;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/g$c$a;->a(IILandroidx/recyclerview/widget/RecyclerView$a;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final a(IILandroidx/recyclerview/widget/RecyclerView$a;)Z
    .locals 0
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroidx/recyclerview/widget/RecyclerView$a<",
            "*>;)Z"
        }
    .end annotation

    const-string p2, "adapter"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p3, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemViewType(I)I

    move-result p1

    .line 53
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/g$c$a;->a:Lcom/swedbank/mobile/core/ui/widget/g$c;

    invoke-static {p2}, Lcom/swedbank/mobile/core/ui/widget/g$c;->a(Lcom/swedbank/mobile/core/ui/widget/g$c;)Landroidx/c/b;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroidx/c/b;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method
