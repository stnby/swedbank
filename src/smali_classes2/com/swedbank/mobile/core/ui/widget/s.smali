.class public final Lcom/swedbank/mobile/core/ui/widget/s;
.super Lcom/google/android/material/snackbar/BaseTransientBottomBar;
.source "Snackbar.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/core/ui/widget/s$b;,
        Lcom/swedbank/mobile/core/ui/widget/s$c;,
        Lcom/swedbank/mobile/core/ui/widget/s$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/material/snackbar/BaseTransientBottomBar<",
        "Lcom/swedbank/mobile/core/ui/widget/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/core/ui/widget/s$a;


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/s$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/s$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/core/ui/widget/s;->a:Lcom/swedbank/mobile/core/ui/widget/s$a;

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Landroid/view/View;Lcom/swedbank/mobile/core/ui/widget/s$b;)V
    .locals 0

    .line 28
    check-cast p3, Lcom/google/android/material/snackbar/ContentViewCallback;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/material/snackbar/BaseTransientBottomBar;-><init>(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/material/snackbar/ContentViewCallback;)V

    .line 35
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->view:Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;

    .line 36
    sget p2, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_title:I

    invoke-virtual {p1, p2}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "view.findViewById(R.id.snackbar_title)"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/s;->b:Landroid/widget/TextView;

    .line 37
    sget p2, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_subtitle:I

    invoke-virtual {p1, p2}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "view.findViewById(R.id.snackbar_subtitle)"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/s;->c:Landroid/widget/TextView;

    .line 38
    sget p2, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_close_btn:I

    invoke-virtual {p1, p2}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "view.findViewById(R.id.snackbar_close_btn)"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/s;->d:Landroid/view/View;

    .line 39
    sget p2, Lcom/swedbank/mobile/core/ui/t$g;->snackbar_action_btn:I

    invoke-virtual {p1, p2}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(R.id.snackbar_action_btn)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->e:Landroid/widget/Button;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/view/ViewGroup;Landroid/view/View;Lcom/swedbank/mobile/core/ui/widget/s$b;Lkotlin/e/b/g;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/s;-><init>(Landroid/view/ViewGroup;Landroid/view/View;Lcom/swedbank/mobile/core/ui/widget/s$b;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/s;)Landroid/widget/Button;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->e:Landroid/widget/Button;

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/widget/s$c;)Lcom/swedbank/mobile/core/ui/widget/s;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    instance-of v0, p1, Lcom/swedbank/mobile/core/ui/widget/s$c$c;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->view:Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;

    sget v3, Lcom/swedbank/mobile/core/ui/t$c;->brand_turqoise:I

    invoke-virtual {v0, v3}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->setBackgroundResource(I)V

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 78
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/s$c$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/s$c$c;->a()Ljava/lang/CharSequence;

    move-result-object p1

    .line 244
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/s;->a(Lcom/swedbank/mobile/core/ui/widget/s;)Landroid/widget/Button;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 246
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 247
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 249
    :cond_0
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 80
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/core/ui/widget/s$c$d;->a:Lcom/swedbank/mobile/core/ui/widget/s$c$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->view:Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;

    sget v0, Lcom/swedbank/mobile/core/ui/t$c;->brand_turqoise:I

    invoke-virtual {p1, v0}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->setBackgroundResource(I)V

    .line 82
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->d:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->e:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 85
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    if-eqz v0, :cond_4

    .line 86
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->view:Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;

    sget v3, Lcom/swedbank/mobile/core/ui/t$c;->label_red:I

    invoke-virtual {v0, v3}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->setBackgroundResource(I)V

    .line 87
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 88
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;->a()Ljava/lang/CharSequence;

    move-result-object p1

    .line 253
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/s;->a(Lcom/swedbank/mobile/core/ui/widget/s;)Landroid/widget/Button;

    move-result-object v0

    if-eqz p1, :cond_3

    .line 255
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 256
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 258
    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 90
    :cond_4
    sget-object v0, Lcom/swedbank/mobile/core/ui/widget/s$c$b;->a:Lcom/swedbank/mobile/core/ui/widget/s$c$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 91
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->view:Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;

    sget v0, Lcom/swedbank/mobile/core/ui/t$c;->label_red:I

    invoke-virtual {p1, v0}, Lcom/google/android/material/snackbar/BaseTransientBottomBar$SnackbarBaseLayout;->setBackgroundResource(I)V

    .line 92
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->d:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 93
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/s;->e:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setVisibility(I)V

    :cond_5
    :goto_0
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/swedbank/mobile/core/ui/widget/s;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public final a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->d:Landroid/view/View;

    .line 238
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    check-cast v1, Lio/reactivex/s;

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->e:Landroid/widget/Button;

    check-cast v0, Landroid/view/View;

    .line 239
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 42
    invoke-static {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(closeBt\u2026ks(), actionBtn.clicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Lcom/swedbank/mobile/core/ui/widget/s;
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    if-nez p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/s;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    :goto_0
    return-object v0
.end method
