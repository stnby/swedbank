.class public final Lcom/swedbank/mobile/core/ui/widget/l;
.super Ljava/lang/Object;
.source "ListViewAdapter.kt"


# direct methods
.method public static final varargs a([Lkotlin/k;)Landroid/util/SparseArray;
    .locals 5
    .param p0    # [Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lkotlin/k<",
            "Ljava/lang/Integer;",
            "+",
            "Lkotlin/e/a/b<",
            "-",
            "Landroid/view/ViewGroup;",
            "+",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "*>;>;>;)",
            "Landroid/util/SparseArray<",
            "Lkotlin/e/a/b<",
            "Landroid/view/ViewGroup;",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pairs"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Landroid/util/SparseArray;

    array-length v1, p0

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 86
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    invoke-virtual {v3}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-virtual {v3}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/e/a/b;

    .line 60
    invoke-virtual {v0, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static final a(Ljava/lang/Object;)Lcom/swedbank/mobile/core/ui/widget/i;
    .locals 2
    .param p0    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "item"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    return-object v0
.end method

.method public static final a(ILkotlin/e/a/m;)Lkotlin/e/a/b;
    .locals 1
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lkotlin/e/a/m<",
            "-",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "TT;>;-TT;",
            "Lkotlin/s;",
            ">;)",
            "Lkotlin/e/a/b<",
            "Landroid/view/ViewGroup;",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "TT;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "binder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/l$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/core/ui/widget/l$a;-><init>(ILkotlin/e/a/m;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public static final b(ILkotlin/e/a/m;)Landroid/util/SparseArray;
    .locals 2
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lkotlin/e/a/m<",
            "-",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "TT;>;-TT;",
            "Lkotlin/s;",
            ">;)",
            "Landroid/util/SparseArray<",
            "Lkotlin/e/a/b<",
            "Landroid/view/ViewGroup;",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "binder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 77
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {v0, p1, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-object v0
.end method
