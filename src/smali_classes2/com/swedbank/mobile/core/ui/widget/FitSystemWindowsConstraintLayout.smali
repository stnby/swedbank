.class public Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "FitSystemWindowsConstraintLayout.kt"


# instance fields
.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->isInEditMode()Z

    move-result p3

    if-nez p3, :cond_1

    const/4 p3, 0x1

    .line 22
    invoke-virtual {p0, p3}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->setFitsSystemWindows(Z)V

    if-eqz p2, :cond_0

    .line 25
    sget-object v0, Lcom/swedbank/mobile/core/ui/t$k;->FitSystemWindowsConstraintLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 27
    :try_start_0
    sget p2, Lcom/swedbank/mobile/core/ui/t$k;->FitSystemWindowsConstraintLayout_respectSystemPadding:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    .line 34
    :cond_0
    :goto_0
    iput-boolean p3, p0, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->g:Z

    :cond_1
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 15
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 16
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;)Z
    .locals 0

    .line 13
    iget-boolean p0, p0, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->g:Z

    return p0
.end method


# virtual methods
.method public onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 5
    .param p1    # Landroid/view/WindowInsets;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "insets"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->a(Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 45
    new-instance v0, Landroid/graphics/Rect;

    .line 46
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetLeft()I

    move-result v1

    .line 47
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetTop()I

    move-result v2

    .line 48
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetRight()I

    move-result v3

    .line 49
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v4

    .line 45
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 112
    iget v1, v0, Landroid/graphics/Rect;->left:I

    const/4 v2, 0x0

    if-nez v1, :cond_1

    iget v1, v0, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_1

    iget v1, v0, Landroid/graphics/Rect;->right:I

    if-nez v1, :cond_1

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 102
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v2, v1, v2, v0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->setPadding(IIII)V

    goto :goto_2

    .line 113
    :cond_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/core/ui/t$d;->status_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 114
    invoke-virtual {p0, v2, v0, v2, v2}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->setPadding(IIII)V

    :cond_3
    :goto_2
    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 39
    invoke-super {p0}, Landroidx/constraintlayout/widget/ConstraintLayout;->onAttachedToWindow()V

    .line 73
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->a(Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;

    check-cast v0, Landroid/view/View;

    .line 40
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 89
    iget v2, v0, Landroid/graphics/Rect;->left:I

    if-nez v2, :cond_1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    if-nez v2, :cond_1

    iget v2, v0, Landroid/graphics/Rect;->right:I

    if-nez v2, :cond_1

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 79
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v1, v2, v1, v0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->setPadding(IIII)V

    goto :goto_2

    .line 90
    :cond_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/swedbank/mobile/core/ui/t$d;->status_bar_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 91
    invoke-virtual {p0, v1, v0, v1, v1}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->setPadding(IIII)V

    :cond_3
    :goto_2
    return-void
.end method
