.class final Lcom/swedbank/mobile/core/ui/widget/g$a$a;
.super Lkotlin/e/b/k;
.source "ItemDividerDecoration.kt"

# interfaces
.implements Lkotlin/e/a/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/widget/g$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/q<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "*>;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/core/ui/widget/g$a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/g$a$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/core/ui/widget/g$a$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/core/ui/widget/g$a$a;->a:Lcom/swedbank/mobile/core/ui/widget/g$a$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/g$a$a;->a(IILandroidx/recyclerview/widget/RecyclerView$a;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final a(IILandroidx/recyclerview/widget/RecyclerView$a;)Z
    .locals 0
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroidx/recyclerview/widget/RecyclerView$a<",
            "*>;)Z"
        }
    .end annotation

    const-string p2, "adapter"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    instance-of p2, p3, Lcom/swedbank/mobile/core/ui/widget/d;

    if-nez p2, :cond_0

    const/4 p3, 0x0

    :cond_0
    check-cast p3, Lcom/swedbank/mobile/core/ui/widget/d;

    if-eqz p3, :cond_1

    .line 40
    invoke-interface {p3, p1}, Lcom/swedbank/mobile/core/ui/widget/d;->a(I)Z

    move-result p1

    return p1

    .line 38
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Adapter driven strategy can only be used with DividerSupportingAdapter"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
