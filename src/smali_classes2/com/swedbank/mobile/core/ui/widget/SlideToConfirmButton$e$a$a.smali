.class final Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a$a;
.super Lkotlin/e/b/k;
.source "SlideToConfirmButton.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a$a;->a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a$a;->a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a;

    iget-object v0, v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a;->b:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e;

    iget-object v0, v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e;->a:Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    .line 375
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 376
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->o(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 377
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->m(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 378
    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    const/16 v4, 0x8

    .line 379
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 381
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->n(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;

    move-result-object v1

    const/4 v5, 0x0

    .line 382
    invoke-virtual {v1, v5}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setAlpha(F)V

    .line 383
    invoke-virtual {v1, v4}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->setVisibility(I)V

    .line 384
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmIndicatorView;->b()V

    .line 386
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    .line 387
    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 388
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 390
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->p(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    .line 391
    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 392
    invoke-virtual {v1, v5}, Landroid/view/View;->setRotation(F)V

    .line 393
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 395
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->k(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 400
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 399
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-string v2, "rootContainer.animate()\n\u2026        .translationX(0f)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 401
    new-instance v2, Lcom/swedbank/mobile/core/ui/c;

    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a$a$1;

    invoke-direct {v3, v1, p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a$a$1;-><init>(Landroid/view/ViewPropertyAnimator;Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a$a;)V

    check-cast v3, Lkotlin/e/a/a;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-string v2, "setListener(AnimatorEndL\u2026er(null)\n  endAction()\n})"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 405
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->g(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 408
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 407
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 410
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->i(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 413
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 412
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 415
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 418
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b(Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 417
    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton$e$a$a;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
