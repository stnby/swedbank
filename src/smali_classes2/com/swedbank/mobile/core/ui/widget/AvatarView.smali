.class public final Lcom/swedbank/mobile/core/ui/widget/AvatarView;
.super Landroid/view/View;
.source "AvatarView.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/widget/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/b;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/core/ui/widget/b;-><init>(Landroid/content/Context;Ljava/lang/String;ZILkotlin/e/b/g;)V

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a:Lcom/swedbank/mobile/core/ui/widget/b;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 12
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 13
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/core/ui/widget/AvatarView;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 19
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a:Lcom/swedbank/mobile/core/ui/widget/b;

    .line 22
    invoke-static {p1}, Lcom/swedbank/mobile/core/ui/widget/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 21
    invoke-virtual {v0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/b;->a(Ljava/lang/String;Z)V

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->getPaddingStart()I

    move-result v0

    int-to-float v0, v0

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->getPaddingEnd()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v1

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->getPaddingBottom()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 33
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 34
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a:Lcom/swedbank/mobile/core/ui/widget/b;

    float-to-int v1, v2

    float-to-int v2, v3

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/b;->setBounds(IIII)V

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a:Lcom/swedbank/mobile/core/ui/widget/b;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/b;->draw(Landroid/graphics/Canvas;)V

    .line 39
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 44
    invoke-super {p0, p1, p1}, Landroid/view/View;->onMeasure(II)V

    return-void
.end method
