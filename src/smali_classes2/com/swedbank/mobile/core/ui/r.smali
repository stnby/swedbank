.class public final Lcom/swedbank/mobile/core/ui/r;
.super Ljava/lang/Object;
.source "Numbers.kt"


# direct methods
.method public static final a(Ljava/math/BigDecimal;ZZ)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/math/BigDecimal;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$formatMonetaryNumber"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const-string p2, "#,##0.00"

    goto :goto_0

    :cond_0
    const-string p2, "0.00"

    .line 18
    :goto_0
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0}, Ljava/text/DecimalFormatSymbols;-><init>()V

    const/16 v1, 0x20

    .line 19
    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setGroupingSeparator(C)V

    const/16 v1, 0x2e

    .line 20
    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 16
    new-instance v1, Ljava/text/DecimalFormat;

    invoke-direct {v1, p2, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 21
    invoke-virtual {v1, p0}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    if-nez p1, :cond_2

    const-string p1, ".00"

    const/4 p2, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 23
    invoke-static {p0, p1, p2, v0, v1}, Lkotlin/j/n;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    const-string p1, "."

    .line 24
    invoke-static {p0, p1, v1, v0, v1}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    :cond_2
    :goto_1
    const-string p1, "this"

    .line 23
    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p0
.end method

.method public static synthetic a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x1

    .line 15
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
