.class public final Lcom/swedbank/mobile/core/ui/an;
.super Lio/reactivex/o;
.source "RxView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/core/ui/an$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/o<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lio/reactivex/o;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/an;->a:Landroid/view/View;

    return-void
.end method


# virtual methods
.method protected a(Lio/reactivex/u;)V
    .locals 2
    .param p1    # Lio/reactivex/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u<",
            "-",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "observer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p1}, Lcom/swedbank/mobile/core/ui/ap;->a(Lio/reactivex/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Lcom/swedbank/mobile/core/ui/an$a;

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/an;->a:Landroid/view/View;

    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/core/ui/an$a;-><init>(Landroid/view/View;Lio/reactivex/u;)V

    .line 26
    move-object v1, v0

    check-cast v1, Lio/reactivex/b/c;

    invoke-interface {p1, v1}, Lio/reactivex/u;->a(Lio/reactivex/b/c;)V

    .line 27
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/an;->a:Landroid/view/View;

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
