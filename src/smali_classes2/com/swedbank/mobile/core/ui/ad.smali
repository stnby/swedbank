.class public final Lcom/swedbank/mobile/core/ui/ad;
.super Ljava/lang/Object;
.source "SharedMessageDisplayStream.kt"


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Unit>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/ad;->a:Lcom/b/c/c;

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const-wide/16 p3, 0xdac

    .line 19
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/core/ui/ad;->a(Ljava/lang/Object;Ljava/lang/Object;J)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;J)",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/ad;->a:Lcom/b/c/c;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p3, p4, v0}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object p3

    .line 42
    new-instance p4, Lcom/swedbank/mobile/core/ui/ae$a;

    invoke-direct {p4, p2}, Lcom/swedbank/mobile/core/ui/ae$a;-><init>(Ljava/lang/Object;)V

    check-cast p4, Lio/reactivex/c/h;

    invoke-virtual {p3, p4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p2

    .line 41
    invoke-virtual {p2, p1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "Observable\n    .timer(di\u2026    .startWith(startWith)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/ad;->a:Lcom/b/c/c;

    check-cast p2, Lio/reactivex/s;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->h(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "messageDisplayStream(\n  \u2026haredMessageRenderStream)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a()V
    .locals 2

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/ad;->a:Lcom/b/c/c;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method
