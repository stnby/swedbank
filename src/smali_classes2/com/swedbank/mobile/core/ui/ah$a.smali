.class public final Lcom/swedbank/mobile/core/ui/ah$a;
.super Ljava/lang/Object;
.source "ShimmeringViews.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/core/ui/ah;Lkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/af;
    .locals 3
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/ah;",
            "Lkotlin/e/a/m<",
            "-",
            "Landroid/content/res/Resources;",
            "-",
            "Landroid/util/DisplayMetrics;",
            "+",
            "Lcom/swedbank/mobile/core/ui/af;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/af;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "create"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-interface {p0}, Lcom/swedbank/mobile/core/ui/ah;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const-string v2, "resources.displayMetrics"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/core/ui/af;

    .line 169
    invoke-interface {p0}, Lcom/swedbank/mobile/core/ui/ah;->getCachedVisibilityChange()Ljava/lang/Integer;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    move-result p0

    invoke-virtual {p1, p0}, Lcom/swedbank/mobile/core/ui/af;->a(I)V

    :cond_0
    return-object p1
.end method

.method public static a(Lcom/swedbank/mobile/core/ui/ah;Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "changedView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-interface {p0}, Lcom/swedbank/mobile/core/ui/ah;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object p1

    if-nez p1, :cond_0

    .line 177
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/swedbank/mobile/core/ui/ah;->setCachedVisibilityChange(Ljava/lang/Integer;)V

    goto :goto_0

    .line 179
    :cond_0
    invoke-interface {p0}, Lcom/swedbank/mobile/core/ui/ah;->getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;

    move-result-object p0

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/core/ui/af;->a(I)V

    :goto_0
    return-void
.end method
