.class public final Lcom/swedbank/mobile/core/ui/i$a;
.super Ljava/lang/Object;
.source "DialogView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/core/ui/i;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 2
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$showWithinViewLifecycle"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 38
    invoke-interface {p0, p1}, Lcom/swedbank/mobile/core/ui/i;->a(Landroid/app/Dialog;)V

    .line 39
    invoke-interface {p0}, Lcom/swedbank/mobile/core/ui/i;->f()Lio/reactivex/b/c;

    move-result-object v0

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 40
    new-instance v0, Lcom/swedbank/mobile/core/ui/i$b;

    invoke-direct {v0, p1, p0}, Lcom/swedbank/mobile/core/ui/i$b;-><init>(Landroid/app/Dialog;Lcom/swedbank/mobile/core/ui/i;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    .line 44
    invoke-interface {p0, v0}, Lcom/swedbank/mobile/core/ui/i;->b(Lio/reactivex/b/c;)V

    const-string v1, "Disposables.fromAction {\u2026ialogView::addDisposable)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lcom/swedbank/mobile/core/ui/i;->a(Lio/reactivex/b/c;)V

    return-object p1
.end method

.method public static a(Lcom/swedbank/mobile/core/ui/i;Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;
    .locals 2
    .param p1    # Landroidx/appcompat/app/b$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$showWithinViewLifecycle"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Landroidx/appcompat/app/b$a;->c()Landroidx/appcompat/app/b;

    move-result-object p1

    .line 24
    check-cast p1, Landroid/app/Dialog;

    .line 47
    invoke-interface {p0, p1}, Lcom/swedbank/mobile/core/ui/i;->a(Landroid/app/Dialog;)V

    .line 48
    invoke-interface {p0}, Lcom/swedbank/mobile/core/ui/i;->f()Lio/reactivex/b/c;

    move-result-object v0

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 49
    new-instance v0, Lcom/swedbank/mobile/core/ui/i$c;

    invoke-direct {v0, p1, p0}, Lcom/swedbank/mobile/core/ui/i$c;-><init>(Landroid/app/Dialog;Lcom/swedbank/mobile/core/ui/i;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    .line 53
    invoke-interface {p0, v0}, Lcom/swedbank/mobile/core/ui/i;->b(Lio/reactivex/b/c;)V

    const-string v1, "Disposables.fromAction {\u2026ialogView::addDisposable)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lcom/swedbank/mobile/core/ui/i;->a(Lio/reactivex/b/c;)V

    const-string p0, "dialog.untilViewDetached()"

    .line 54
    invoke-static {p1, p0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/appcompat/app/b;

    return-object p1
.end method
