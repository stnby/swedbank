.class public final Lcom/swedbank/mobile/core/ui/b;
.super Ljava/lang/Object;
.source "Animations.kt"


# static fields
.field private static final a:Landroid/view/animation/Interpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final b:Landroid/view/animation/Interpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:Landroidx/l/r;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final d:Landroidx/l/n;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 19
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    check-cast v0, Landroid/view/animation/Interpolator;

    sput-object v0, Lcom/swedbank/mobile/core/ui/b;->a:Landroid/view/animation/Interpolator;

    .line 20
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    check-cast v0, Landroid/view/animation/Interpolator;

    sput-object v0, Lcom/swedbank/mobile/core/ui/b;->b:Landroid/view/animation/Interpolator;

    .line 22
    new-instance v0, Landroidx/l/r;

    invoke-direct {v0}, Landroidx/l/r;-><init>()V

    .line 23
    new-instance v1, Landroidx/l/d;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroidx/l/d;-><init>(I)V

    check-cast v1, Landroidx/l/n;

    invoke-virtual {v0, v1}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object v0

    .line 24
    new-instance v1, Landroidx/l/d;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroidx/l/d;-><init>(I)V

    check-cast v1, Landroidx/l/n;

    invoke-virtual {v0, v1}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object v0

    const/4 v1, 0x0

    .line 25
    invoke-virtual {v0, v1}, Landroidx/l/r;->a(I)Landroidx/l/r;

    move-result-object v0

    const-wide/16 v1, 0x78

    .line 26
    invoke-virtual {v0, v1, v2}, Landroidx/l/r;->a(J)Landroidx/l/r;

    move-result-object v0

    const-string v3, "TransitionSet()\n    .add\u2026ER)\n    .setDuration(120)"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/swedbank/mobile/core/ui/b;->c:Landroidx/l/r;

    .line 28
    new-instance v0, Landroidx/l/c;

    invoke-direct {v0}, Landroidx/l/c;-><init>()V

    .line 29
    invoke-virtual {v0, v1, v2}, Landroidx/l/c;->setDuration(J)Landroidx/l/n;

    move-result-object v0

    const-string v1, "ChangeBounds()\n    .setDuration(120)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/swedbank/mobile/core/ui/b;->d:Landroidx/l/n;

    return-void
.end method

.method public static final a()Landroid/view/animation/Interpolator;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    sget-object v0, Lcom/swedbank/mobile/core/ui/b;->a:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method public static final a(Landroidx/l/n;Landroid/animation/Animator;)V
    .locals 1
    .param p0    # Landroidx/l/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$cancelAnimatorOnTransitionPause"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "animator"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/swedbank/mobile/core/ui/b$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/core/ui/b$a;-><init>(Landroid/animation/Animator;)V

    check-cast v0, Landroidx/l/n$d;

    invoke-virtual {p0, v0}, Landroidx/l/n;->addListener(Landroidx/l/n$d;)Landroidx/l/n;

    return-void
.end method

.method public static final b()Landroid/view/animation/Interpolator;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    sget-object v0, Lcom/swedbank/mobile/core/ui/b;->b:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method public static final c()Landroidx/l/r;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    sget-object v0, Lcom/swedbank/mobile/core/ui/b;->c:Landroidx/l/r;

    return-object v0
.end method

.method public static final d()Landroidx/l/n;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    sget-object v0, Lcom/swedbank/mobile/core/ui/b;->d:Landroidx/l/n;

    return-object v0
.end method
