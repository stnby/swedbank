.class public final Lcom/swedbank/mobile/core/ui/y;
.super Ljava/lang/Object;
.source "SharedLoadingRenderStream.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/x;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/core/ui/y$a;,
        Lcom/swedbank/mobile/core/ui/y$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/core/ui/y$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:J

.field private final e:J


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/core/ui/y;-><init>(Lkotlin/e/a/a;JJILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/e/a/a;JJ)V
    .locals 1
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Ljava/lang/Long;",
            ">;JJ)V"
        }
    .end annotation

    const-string v0, "timeSupplier"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/y;->c:Lkotlin/e/a/a;

    iput-wide p2, p0, Lcom/swedbank/mobile/core/ui/y;->d:J

    iput-wide p4, p0, Lcom/swedbank/mobile/core/ui/y;->e:J

    .line 55
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<LoadingInput>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/y;->a:Lcom/b/c/c;

    .line 56
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/y;->a:Lcom/b/c/c;

    .line 57
    new-instance p2, Lcom/swedbank/mobile/core/ui/y$c;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/core/ui/y$c;-><init>(Lcom/swedbank/mobile/core/ui/y;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lcom/b/c/c;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 58
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$b;->a:Lcom/swedbank/mobile/core/ui/y$b;

    iget-object p3, p0, Lcom/swedbank/mobile/core/ui/y;->c:Lkotlin/e/a/a;

    invoke-interface {p3}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object p3

    invoke-static {p2, p3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p2

    new-instance p3, Lcom/swedbank/mobile/core/ui/y$d;

    invoke-direct {p3, p0}, Lcom/swedbank/mobile/core/ui/y$d;-><init>(Lcom/swedbank/mobile/core/ui/y;)V

    check-cast p3, Lio/reactivex/c/c;

    invoke-virtual {p1, p2, p3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    .line 113
    sget-object p2, Lcom/swedbank/mobile/core/ui/aa;->a:Lkotlin/h/i;

    check-cast p2, Lkotlin/e/a/b;

    if-eqz p2, :cond_0

    new-instance p3, Lcom/swedbank/mobile/core/ui/ac;

    invoke-direct {p3, p2}, Lcom/swedbank/mobile/core/ui/ac;-><init>(Lkotlin/e/a/b;)V

    move-object p2, p3

    :cond_0
    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 114
    invoke-virtual {p1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object p1

    .line 115
    new-instance p2, Lcom/swedbank/mobile/core/ui/y$e;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/core/ui/y$e;-><init>(Lcom/swedbank/mobile/core/ui/y;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 128
    sget-object p2, Lcom/swedbank/mobile/core/ui/y$f;->a:Lcom/swedbank/mobile/core/ui/y$f;

    check-cast p2, Lio/reactivex/c/d;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/y;->b:Lio/reactivex/o;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/a/a;JJILkotlin/e/b/g;)V
    .locals 2

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    .line 51
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$1;->a:Lcom/swedbank/mobile/core/ui/y$1;

    check-cast p1, Lkotlin/e/a/a;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    const-wide/16 p2, 0xfa

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    const-wide/16 p4, 0x4e2

    :cond_2
    move-wide p6, p4

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    .line 53
    invoke-direct/range {p2 .. p7}, Lcom/swedbank/mobile/core/ui/y;-><init>(Lkotlin/e/a/a;JJ)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/y;)Lkotlin/e/a/a;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/y;->c:Lkotlin/e/a/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/y;)J
    .locals 2

    .line 50
    iget-wide v0, p0, Lcom/swedbank/mobile/core/ui/y;->d:J

    return-wide v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/core/ui/y;)J
    .locals 2

    .line 50
    iget-wide v0, p0, Lcom/swedbank/mobile/core/ui/y;->e:J

    return-wide v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/y;->b:Lio/reactivex/o;

    const-string v1, "loadingRenderStream"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Z)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/y;->a:Lcom/b/c/c;

    if-eqz p1, :cond_0

    .line 137
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$a;->a:Lcom/swedbank/mobile/core/ui/y$a;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$a;->c:Lcom/swedbank/mobile/core/ui/y$a;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/y;->a:Lcom/b/c/c;

    if-eqz p1, :cond_0

    .line 140
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$a;->b:Lcom/swedbank/mobile/core/ui/y$a;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/swedbank/mobile/core/ui/y$a;->d:Lcom/swedbank/mobile/core/ui/y$a;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public c(Z)Lio/reactivex/o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z)",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;Z)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public d(Z)Lio/reactivex/o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z)",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/x$a;->b(Lcom/swedbank/mobile/core/ui/x;Z)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
