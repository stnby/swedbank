.class public interface abstract Lcom/swedbank/mobile/core/ui/ah;
.super Ljava/lang/Object;
.source "ShimmeringViews.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/core/ui/ah$a;
    }
.end annotation


# virtual methods
.method public abstract getCachedVisibilityChange()Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end method

.method public abstract getResources()Landroid/content/res/Resources;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract getShimmerRenderer()Lcom/swedbank/mobile/core/ui/af;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract setCachedVisibilityChange(Ljava/lang/Integer;)V
    .param p1    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
.end method
