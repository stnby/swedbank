.class public final Lcom/swedbank/mobile/core/ui/am;
.super Ljava/lang/Object;
.source "ViewBindings.kt"


# direct methods
.method private static final a(ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">(I",
            "Lkotlin/e/a/m<",
            "-TT;-",
            "Ljava/lang/Integer;",
            "+",
            "Landroid/view/View;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/o<",
            "TT;TV;>;"
        }
    .end annotation

    .line 87
    new-instance v0, Lcom/swedbank/mobile/core/ui/o;

    new-instance v1, Lcom/swedbank/mobile/core/ui/am$b;

    invoke-direct {v1, p1, p0}, Lcom/swedbank/mobile/core/ui/am$b;-><init>(Lkotlin/e/a/m;I)V

    check-cast v1, Lkotlin/e/a/m;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/o;-><init>(Lkotlin/e/a/m;)V

    return-object v0
.end method

.method private static final a([ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">([I",
            "Lkotlin/e/a/m<",
            "-TT;-",
            "Ljava/lang/Integer;",
            "+",
            "Landroid/view/View;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/o<",
            "TT;",
            "Ljava/util/List<",
            "TV;>;>;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/swedbank/mobile/core/ui/o;

    new-instance v1, Lcom/swedbank/mobile/core/ui/am$c;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/core/ui/am$c;-><init>([ILkotlin/e/a/m;)V

    check-cast v1, Lkotlin/e/a/m;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/o;-><init>(Lkotlin/e/a/m;)V

    return-object v0
.end method

.method public static final synthetic a(ILkotlin/h/g;)Ljava/lang/Void;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->b(ILkotlin/h/g;)Ljava/lang/Void;

    move-result-object p0

    return-object p0
.end method

.method private static final a(Landroid/view/View;)Lkotlin/e/a/m;
    .locals 0
    .param p0    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lkotlin/e/a/m<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 73
    sget-object p0, Lcom/swedbank/mobile/core/ui/am$e;->a:Lcom/swedbank/mobile/core/ui/am$e;

    check-cast p0, Lkotlin/e/a/m;

    return-object p0
.end method

.method private static final a(Landroidx/recyclerview/widget/RecyclerView$x;)Lkotlin/e/a/m;
    .locals 0
    .param p0    # Landroidx/recyclerview/widget/RecyclerView$x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$x;",
            ")",
            "Lkotlin/e/a/m<",
            "Landroidx/recyclerview/widget/RecyclerView$x;",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 77
    sget-object p0, Lcom/swedbank/mobile/core/ui/am$f;->a:Lcom/swedbank/mobile/core/ui/am$f;

    check-cast p0, Lkotlin/e/a/m;

    return-object p0
.end method

.method private static final a(Lcom/swedbank/mobile/architect/a/b/a;)Lkotlin/e/a/m;
    .locals 0
    .param p0    # Lcom/swedbank/mobile/architect/a/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            ")",
            "Lkotlin/e/a/m<",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 71
    sget-object p0, Lcom/swedbank/mobile/core/ui/am$d;->a:Lcom/swedbank/mobile/core/ui/am$d;

    check-cast p0, Lkotlin/e/a/m;

    return-object p0
.end method

.method public static final a(Landroid/view/View;I)Lkotlin/f/c;
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I)",
            "Lkotlin/f/c<",
            "Landroid/view/View;",
            "TV;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$bindView"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;)Lkotlin/e/a/m;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/swedbank/mobile/core/ui/am;->a(ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;

    move-result-object p0

    check-cast p0, Lkotlin/f/c;

    return-object p0
.end method

.method public static final varargs a(Landroid/view/View;[I)Lkotlin/f/c;
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "[I)",
            "Lkotlin/f/c<",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "TV;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$bindViews"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ids"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;)Lkotlin/e/a/m;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/swedbank/mobile/core/ui/am;->a([ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;

    move-result-object p0

    check-cast p0, Lkotlin/f/c;

    return-object p0
.end method

.method public static final a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;
    .locals 1
    .param p0    # Landroidx/recyclerview/widget/RecyclerView$x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Landroidx/recyclerview/widget/RecyclerView$x;",
            "I)",
            "Lkotlin/f/c<",
            "Landroidx/recyclerview/widget/RecyclerView$x;",
            "TV;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$bindView"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;)Lkotlin/e/a/m;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/swedbank/mobile/core/ui/am;->a(ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;

    move-result-object p0

    check-cast p0, Lkotlin/f/c;

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/architect/a/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            "I)",
            "Lkotlin/f/c<",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            "TV;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$bindView"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;)Lkotlin/e/a/m;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/swedbank/mobile/core/ui/am;->a(ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;

    move-result-object p0

    check-cast p0, Lkotlin/f/c;

    return-object p0
.end method

.method public static final varargs a(Lcom/swedbank/mobile/architect/a/b/a;[I)Lkotlin/f/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/architect/a/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            "[I)",
            "Lkotlin/f/c<",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            "Ljava/util/List<",
            "TV;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$bindViews"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ids"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;)Lkotlin/e/a/m;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/swedbank/mobile/core/ui/am;->a([ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;

    move-result-object p0

    check-cast p0, Lkotlin/f/c;

    return-object p0
.end method

.method private static final b(ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">(I",
            "Lkotlin/e/a/m<",
            "-TT;-",
            "Ljava/lang/Integer;",
            "+",
            "Landroid/view/View;",
            ">;)",
            "Lcom/swedbank/mobile/core/ui/o<",
            "TT;TV;>;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/swedbank/mobile/core/ui/o;

    new-instance v1, Lcom/swedbank/mobile/core/ui/am$a;

    invoke-direct {v1, p1, p0}, Lcom/swedbank/mobile/core/ui/am$a;-><init>(Lkotlin/e/a/m;I)V

    check-cast v1, Lkotlin/e/a/m;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/o;-><init>(Lkotlin/e/a/m;)V

    return-object v0
.end method

.method private static final b(ILkotlin/h/g;)Ljava/lang/Void;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/h/g<",
            "*>;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "View ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, " for \'"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lkotlin/h/g;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\' not found."

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static final b(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/architect/a/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            "I)",
            "Lkotlin/f/c<",
            "Lcom/swedbank/mobile/architect/a/b/a;",
            "TV;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$bindOptionalView"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;)Lkotlin/e/a/m;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/swedbank/mobile/core/ui/am;->b(ILkotlin/e/a/m;)Lcom/swedbank/mobile/core/ui/o;

    move-result-object p0

    check-cast p0, Lkotlin/f/c;

    return-object p0
.end method
