.class public final Lcom/swedbank/mobile/core/ui/a/c;
.super Landroidx/l/n;
.source "EnterExitTransform.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroidx/l/n;-><init>()V

    return-void
.end method


# virtual methods
.method public captureEndValues(Landroidx/l/t;)V
    .locals 2
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "values"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 116
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 120
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v0, "valuesMap"

    .line 121
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "android:enterexittransform:visible"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public captureStartValues(Landroidx/l/t;)V
    .locals 2
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "values"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 108
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v0, "valuesMap"

    .line 113
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "android:enterexittransform:visible"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 16
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "sceneRoot"

    move-object/from16 v11, p1

    invoke-static {v11, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    const v4, 0x3f2e147b    # 0.68f

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v8, 0x2

    if-eqz v1, :cond_0

    .line 44
    iget-object v9, v1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v10, "android:enterexittransform:visible"

    invoke-interface {v9, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 45
    iget-object v12, v1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "startValues.view"

    invoke-static {v12, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v12, v1}, Landroid/view/View;->setPivotY(F)V

    .line 125
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v7

    invoke-virtual {v12, v1}, Landroid/view/View;->setPivotX(F)V

    .line 128
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/view/ViewGroupOverlay;->add(Landroid/view/View;)V

    .line 133
    sget-object v1, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v2, v8, [F

    fill-array-data v2, :array_0

    invoke-static {v12, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 137
    sget-object v2, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v7, v8, [F

    fill-array-data v7, :array_1

    invoke-static {v12, v2, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 141
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v7, v8, [F

    fill-array-data v7, :array_2

    invoke-static {v12, v2, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/c;->getDuration()J

    move-result-wide v13

    .line 145
    new-instance v15, Landroid/animation/AnimatorSet;

    invoke-direct {v15}, Landroid/animation/AnimatorSet;-><init>()V

    .line 146
    new-array v6, v6, [Landroid/animation/Animator;

    long-to-float v7, v13

    mul-float v7, v7, v4

    float-to-long v10, v7

    .line 150
    invoke-virtual {v2, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-string v7, "it"

    .line 149
    invoke-static {v4, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v7

    check-cast v7, Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v4, Landroid/animation/Animator;

    aput-object v4, v6, v5

    .line 155
    invoke-virtual {v1, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-string v5, "it"

    .line 154
    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v5

    check-cast v5, Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v4, Landroid/animation/Animator;

    aput-object v4, v6, v3

    .line 160
    invoke-virtual {v9, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-string v4, "it"

    .line 159
    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v4

    check-cast v4, Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v3, Landroid/animation/Animator;

    aput-object v3, v6, v8

    .line 146
    invoke-virtual {v15, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 163
    new-instance v3, Lcom/swedbank/mobile/core/ui/a/c$a;

    move-object v4, v3

    move-object v5, v2

    move-wide v6, v13

    move-object v8, v1

    const/4 v1, 0x0

    move v10, v1

    move-object/from16 v11, p1

    invoke-direct/range {v4 .. v12}, Lcom/swedbank/mobile/core/ui/a/c$a;-><init>(Landroid/animation/ObjectAnimator;JLandroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;ZLandroid/view/ViewGroup;Landroid/view/View;)V

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v15, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 172
    check-cast v15, Landroid/animation/Animator;

    invoke-static {v0, v15}, Lcom/swedbank/mobile/core/ui/b;->a(Landroidx/l/n;Landroid/animation/Animator;)V

    goto/16 :goto_0

    :cond_0
    if-eqz v2, :cond_1

    .line 48
    iget-object v1, v2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v9, "android:enterexittransform:visible"

    invoke-interface {v1, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    iget-object v12, v2, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "endValues.view"

    invoke-static {v12, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v12, v1}, Landroid/view/View;->setPivotY(F)V

    .line 176
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v7

    invoke-virtual {v12, v1}, Landroid/view/View;->setPivotX(F)V

    .line 183
    sget-object v1, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v2, v8, [F

    fill-array-data v2, :array_3

    invoke-static {v12, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 187
    sget-object v2, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v7, v8, [F

    fill-array-data v7, :array_4

    invoke-static {v12, v2, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 191
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v7, v8, [F

    fill-array-data v7, :array_5

    invoke-static {v12, v2, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/c;->getDuration()J

    move-result-wide v13

    .line 196
    new-instance v15, Landroid/animation/AnimatorSet;

    invoke-direct {v15}, Landroid/animation/AnimatorSet;-><init>()V

    .line 197
    new-array v6, v6, [Landroid/animation/Animator;

    long-to-float v7, v13

    mul-float v7, v7, v4

    float-to-long v10, v7

    .line 201
    invoke-virtual {v2, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-string v7, "it"

    .line 200
    invoke-static {v4, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v7

    check-cast v7, Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v4, Landroid/animation/Animator;

    aput-object v4, v6, v5

    .line 206
    invoke-virtual {v1, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-string v5, "it"

    .line 205
    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v5

    check-cast v5, Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v4, Landroid/animation/Animator;

    aput-object v4, v6, v3

    .line 211
    invoke-virtual {v9, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-string v4, "it"

    .line 210
    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v4

    check-cast v4, Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v3, Landroid/animation/Animator;

    aput-object v3, v6, v8

    .line 197
    invoke-virtual {v15, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 214
    new-instance v3, Lcom/swedbank/mobile/core/ui/a/c$b;

    move-object v4, v3

    move-object v5, v2

    move-wide v6, v13

    move-object v8, v1

    const/4 v1, 0x1

    move v10, v1

    move-object/from16 v11, p1

    invoke-direct/range {v4 .. v12}, Lcom/swedbank/mobile/core/ui/a/c$b;-><init>(Landroid/animation/ObjectAnimator;JLandroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;ZLandroid/view/ViewGroup;Landroid/view/View;)V

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v15, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 223
    check-cast v15, Landroid/animation/Animator;

    invoke-static {v0, v15}, Lcom/swedbank/mobile/core/ui/b;->a(Landroidx/l/n;Landroid/animation/Animator;)V

    goto :goto_0

    :cond_1
    const/4 v15, 0x0

    :goto_0
    return-object v15

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f59999a    # 0.85f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f59999a    # 0.85f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x3f59999a    # 0.85f
        0x3f800000    # 1.0f
    .end array-data

    :array_4
    .array-data 4
        0x3f59999a    # 0.85f
        0x3f800000    # 1.0f
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public isTransitionRequired(Landroidx/l/t;Landroidx/l/t;)Z
    .locals 1
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    .line 36
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v0, "android:enterexittransform:visible"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    .line 37
    iget-object p1, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string p2, "android:enterexittransform:visible"

    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
