.class public final Lcom/swedbank/mobile/core/ui/a/k;
.super Lcom/swedbank/mobile/architect/a/b/a/a;
.source "SlideOutPopTransition.kt"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a/a;-><init>()V

    iput p1, p0, Lcom/swedbank/mobile/core/ui/a/k;->a:I

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)Landroidx/l/n;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string p2, "container"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    xor-int/lit8 p1, p4, 0x1

    if-eqz p1, :cond_0

    .line 19
    new-instance p1, Lcom/swedbank/mobile/core/ui/a/g;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/a/g;-><init>()V

    .line 20
    iget p2, p0, Lcom/swedbank/mobile/core/ui/a/k;->a:I

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/core/ui/a/g;->addTarget(I)Landroidx/l/n;

    move-result-object p1

    const-string p2, "OverlaySlideOutTransform\u2026       .addTarget(target)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Pop transition is supported only for popping views"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
