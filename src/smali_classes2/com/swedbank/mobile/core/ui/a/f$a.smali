.class public final Lcom/swedbank/mobile/core/ui/a/f$a;
.super Landroid/animation/AnimatorListenerAdapter;
.source "MoveAwayComeBackTransform.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/core/ui/a/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/animation/ObjectAnimator;

.field final synthetic b:J

.field final synthetic c:Landroid/animation/ObjectAnimator;

.field final synthetic d:Z

.field final synthetic e:Landroid/view/ViewGroup;

.field final synthetic f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/animation/ObjectAnimator;JLandroid/animation/ObjectAnimator;ZLandroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->a:Landroid/animation/ObjectAnimator;

    iput-wide p2, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->b:J

    iput-object p4, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->c:Landroid/animation/ObjectAnimator;

    iput-boolean p5, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->d:Z

    iput-object p6, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->e:Landroid/view/ViewGroup;

    iput-object p7, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->f:Landroid/view/View;

    .line 92
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 94
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 95
    iget-boolean p1, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->d:Z

    if-nez p1, :cond_0

    .line 96
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->e:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/a/f$a;->f:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroupOverlay;->remove(Landroid/view/View;)V

    :cond_0
    return-void
.end method
