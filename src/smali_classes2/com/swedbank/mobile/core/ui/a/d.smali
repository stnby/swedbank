.class public final Lcom/swedbank/mobile/core/ui/a/d;
.super Lcom/swedbank/mobile/architect/a/b/a/b;
.source "ExpandTransition.kt"


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private final b:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a/b;-><init>()V

    .line 19
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/a/d;->a:Landroid/graphics/Rect;

    .line 20
    new-instance v0, Lcom/swedbank/mobile/core/ui/a/d$a;

    const-string v1, "viewClipHeight"

    invoke-direct {v0, p0, v1}, Lcom/swedbank/mobile/core/ui/a/d$a;-><init>(Lcom/swedbank/mobile/core/ui/a/d;Ljava/lang/String;)V

    check-cast v0, Landroid/util/Property;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/a/d;->b:Landroid/util/Property;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/a/d;)Landroid/util/Property;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/a/d;->b:Landroid/util/Property;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/core/ui/a/d;)Landroid/graphics/Rect;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/a/d;->a:Landroid/graphics/Rect;

    return-object p0
.end method


# virtual methods
.method protected a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;Z)Landroid/animation/Animator;
    .locals 16
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    const-string v2, "container"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x0

    if-eqz p5, :cond_5

    .line 138
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz p3, :cond_4

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 141
    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 142
    sget v10, Lcom/swedbank/mobile/core/ui/t$j;->tag_expand_transition_target:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "resources.getString(R.st\u2026expand_transition_target)"

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v9}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 144
    invoke-static/range {p4 .. p4}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v1, v1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 145
    :goto_0
    new-array v10, v6, [I

    .line 146
    invoke-virtual {v9, v10}, Landroid/view/View;->getLocationInWindow([I)V

    .line 147
    aget v10, v10, v5

    sub-int/2addr v10, v1

    int-to-float v1, v10

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v9, :cond_2

    .line 149
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v10

    goto :goto_2

    :cond_2
    const/4 v10, 0x0

    .line 150
    :goto_2
    new-instance v11, Lkotlin/p;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct {v11, v9, v1, v10}, Lkotlin/p;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v11}, Lkotlin/p;->d()Ljava/lang/Object;

    move-result-object v1

    .line 140
    check-cast v1, Landroid/view/View;

    invoke-virtual {v11}, Lkotlin/p;->e()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Number;

    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    move-result v9

    invoke-virtual {v11}, Lkotlin/p;->f()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->intValue()I

    move-result v10

    const-wide/16 v11, 0xfa

    .line 152
    invoke-virtual {v0, v9}, Landroid/view/View;->setTranslationY(F)V

    .line 153
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 155
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/d;->b(Lcom/swedbank/mobile/core/ui/a/d;)Landroid/graphics/Rect;

    move-result-object v13

    .line 156
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getWidth()I

    move-result v14

    invoke-virtual {v13, v7, v7, v14, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 157
    sget-object v14, Lkotlin/s;->a:Lkotlin/s;

    .line 155
    invoke-virtual {v0, v13}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    .line 159
    new-instance v13, Landroid/animation/AnimatorSet;

    invoke-direct {v13}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz v1, :cond_3

    .line 161
    sget-object v14, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v15, v6, [F

    aput v4, v15, v7

    neg-float v2, v9

    aput v2, v15, v5

    invoke-static {v1, v14, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 164
    invoke-virtual {v1, v11, v12}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-string v2, "it"

    .line 163
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v2

    check-cast v2, Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    check-cast v1, Landroid/animation/Animator;

    .line 161
    invoke-virtual {v13, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 167
    :cond_3
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    .line 168
    new-array v1, v3, [Landroid/animation/Animator;

    .line 169
    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v6, [F

    aput v9, v3, v7

    aput v4, v3, v5

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 172
    invoke-virtual {v2, v11, v12}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-string v3, "it"

    .line 171
    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v3

    check-cast v3, Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    check-cast v2, Landroid/animation/Animator;

    aput-object v2, v1, v7

    .line 174
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v3, 0x4b

    .line 177
    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-string v3, "it"

    .line 176
    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v3, 0x23

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    check-cast v2, Landroid/animation/Animator;

    aput-object v2, v1, v5

    .line 184
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/d;->a(Lcom/swedbank/mobile/core/ui/a/d;)Landroid/util/Property;

    move-result-object v2

    new-array v3, v6, [I

    aput v10, v3, v7

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    aput v4, v3, v5

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 183
    invoke-virtual {v0, v11, v12}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-string v2, "it"

    .line 182
    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v2

    check-cast v2, Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    check-cast v0, Landroid/animation/Animator;

    aput-object v0, v1, v6

    .line 187
    check-cast v13, Landroid/animation/Animator;

    const/4 v0, 0x3

    aput-object v13, v1, v0

    .line 168
    invoke-virtual {v8, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 190
    :cond_4
    check-cast v8, Landroid/animation/Animator;

    goto/16 :goto_6

    .line 191
    :cond_5
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    .line 194
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 195
    sget v9, Lcom/swedbank/mobile/core/ui/t$j;->tag_expand_transition_target:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "resources.getString(R.st\u2026expand_transition_target)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v8}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 197
    invoke-static/range {p2 .. p2}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_6

    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    .line 198
    :goto_3
    new-array v9, v6, [I

    .line 199
    invoke-virtual {v8, v9}, Landroid/view/View;->getLocationInWindow([I)V

    .line 200
    aget v9, v9, v5

    sub-int/2addr v9, v0

    int-to-float v0, v9

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    :goto_4
    if-eqz v8, :cond_8

    .line 202
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v9

    goto :goto_5

    :cond_8
    const/4 v9, 0x0

    .line 203
    :goto_5
    new-instance v10, Lkotlin/p;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v10, v8, v0, v9}, Lkotlin/p;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v10}, Lkotlin/p;->d()Ljava/lang/Object;

    move-result-object v0

    .line 193
    check-cast v0, Landroid/view/View;

    invoke-virtual {v10}, Lkotlin/p;->e()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->floatValue()F

    move-result v8

    invoke-virtual {v10}, Lkotlin/p;->f()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Number;

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    const-wide/16 v10, 0x12c

    .line 205
    new-instance v12, Landroid/animation/AnimatorSet;

    invoke-direct {v12}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz v0, :cond_9

    neg-float v13, v8

    .line 208
    invoke-virtual {v0, v13}, Landroid/view/View;->setTranslationY(F)V

    const/4 v14, 0x0

    .line 209
    check-cast v14, Ljava/lang/String;

    invoke-virtual {v0, v14}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 210
    sget-object v14, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v15, v6, [F

    aput v13, v15, v7

    aput v4, v15, v5

    invoke-static {v0, v14, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 213
    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-string v13, "it"

    .line 212
    invoke-static {v0, v13}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v13

    check-cast v13, Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v13}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v13, Lkotlin/s;->a:Lkotlin/s;

    check-cast v0, Landroid/animation/Animator;

    .line 210
    invoke-virtual {v12, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 216
    :cond_9
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 217
    new-array v0, v3, [Landroid/animation/Animator;

    .line 218
    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v13, v6, [F

    aput v4, v13, v7

    aput v8, v13, v5

    invoke-static {v1, v3, v13}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 221
    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-string v4, "it"

    .line 220
    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v4

    check-cast v4, Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    check-cast v3, Landroid/animation/Animator;

    aput-object v3, v0, v7

    .line 228
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/d;->a(Lcom/swedbank/mobile/core/ui/a/d;)Landroid/util/Property;

    move-result-object v3

    new-array v4, v6, [I

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    aput v8, v4, v7

    aput v9, v4, v5

    invoke-static {v1, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 227
    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-string v4, "it"

    .line 226
    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v4

    check-cast v4, Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    check-cast v3, Landroid/animation/Animator;

    aput-object v3, v0, v5

    .line 231
    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    fill-array-data v4, :array_1

    invoke-static {v1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v3, 0x64

    .line 234
    invoke-virtual {v1, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-string v3, "it"

    .line 236
    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v3, 0x96

    invoke-virtual {v1, v3, v4}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 237
    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v3

    check-cast v3, Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 238
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 233
    check-cast v1, Landroid/animation/Animator;

    aput-object v1, v0, v6

    .line 239
    check-cast v12, Landroid/animation/Animator;

    const/4 v1, 0x3

    aput-object v12, v0, v1

    .line 217
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 242
    :cond_a
    move-object v8, v2

    check-cast v8, Landroid/animation/Animator;

    :goto_6
    return-object v8

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method protected a(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 133
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    const/4 v0, 0x0

    .line 134
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    const/4 v0, 0x0

    .line 135
    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    return-void
.end method
