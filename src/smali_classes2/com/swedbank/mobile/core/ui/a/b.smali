.class public final Lcom/swedbank/mobile/core/ui/a/b;
.super Landroidx/l/n;
.source "ChangeLocationTransform.kt"


# instance fields
.field private final a:[I

.field private final b:Z

.field private final c:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p0, v0, v0, v1, v2}, Lcom/swedbank/mobile/core/ui/a/b;-><init>(ZZILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroidx/l/n;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/core/ui/a/b;->b:Z

    iput-boolean p2, p0, Lcom/swedbank/mobile/core/ui/a/b;->c:Z

    const/4 p1, 0x2

    .line 24
    new-array p1, p1, [I

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/a/b;->a:[I

    return-void
.end method

.method public synthetic constructor <init>(ZZILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    .line 22
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/a/b;-><init>(ZZ)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/a/b;)[I
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/a/b;->a:[I

    return-object p0
.end method


# virtual methods
.method public captureEndValues(Landroidx/l/t;)V
    .locals 9
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transitionValues"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 127
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 130
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    .line 131
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/a/b;->a(Lcom/swedbank/mobile/core/ui/a/b;)[I

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 133
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float v2, v2, v3

    .line 134
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    move-result v4

    mul-float v3, v3, v4

    const/4 v4, 0x0

    .line 135
    aget v5, v1, v4

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    const/4 v6, 0x1

    .line 136
    aget v8, v1, v6

    int-to-float v8, v8

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v3

    div-float/2addr v0, v7

    add-float/2addr v8, v0

    .line 137
    new-instance v0, Landroid/graphics/RectF;

    .line 140
    aget v4, v1, v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    .line 141
    aget v1, v1, v6

    int-to-float v1, v1

    add-float/2addr v1, v3

    .line 137
    invoke-direct {v0, v5, v8, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 142
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "valuesMap"

    .line 143
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "android:changeLocation:rectInWindow"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public captureStartValues(Landroidx/l/t;)V
    .locals 9
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transitionValues"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 107
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    .line 111
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/a/b;->a(Lcom/swedbank/mobile/core/ui/a/b;)[I

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 113
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float v2, v2, v3

    .line 114
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    move-result v4

    mul-float v3, v3, v4

    const/4 v4, 0x0

    .line 115
    aget v5, v1, v4

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    const/4 v6, 0x1

    .line 116
    aget v8, v1, v6

    int-to-float v8, v8

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v3

    div-float/2addr v0, v7

    add-float/2addr v8, v0

    .line 117
    new-instance v0, Landroid/graphics/RectF;

    .line 120
    aget v4, v1, v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    .line 121
    aget v1, v1, v6

    int-to-float v1, v1

    add-float/2addr v1, v3

    .line 117
    invoke-direct {v0, v5, v8, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 122
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "valuesMap"

    .line 123
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "android:changeLocation:rectInWindow"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 18
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "sceneRoot"

    move-object/from16 v11, p1

    invoke-static {v11, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    .line 48
    iget-object v4, v1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v5, "android:changeLocation:rectInWindow"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 49
    iget-object v4, v2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v5, "android:changeLocation:rectInWindow"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    goto/16 :goto_0

    .line 53
    :cond_0
    iget-object v5, v2, Landroidx/l/t;->b:Landroid/view/View;

    .line 54
    iget-object v9, v1, Landroidx/l/t;->b:Landroid/view/View;

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/ViewGroupOverlay;->add(Landroid/view/View;)V

    const-string v4, "startView"

    .line 57
    invoke-static {v9, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/swedbank/mobile/core/ui/a/b;->a:[I

    .line 156
    invoke-virtual {v9, v4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 157
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v9}, Landroid/view/View;->getScaleX()F

    move-result v7

    mul-float v6, v6, v7

    .line 158
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v9}, Landroid/view/View;->getScaleY()F

    move-result v8

    mul-float v7, v7, v8

    const/4 v8, 0x0

    .line 159
    aget v10, v4, v8

    int-to-float v10, v10

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v12

    int-to-float v12, v12

    sub-float/2addr v12, v6

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    add-float/2addr v10, v12

    const/4 v12, 0x1

    .line 160
    aget v14, v4, v12

    int-to-float v14, v14

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v15

    int-to-float v15, v15

    sub-float/2addr v15, v7

    div-float/2addr v15, v13

    add-float/2addr v14, v15

    .line 161
    new-instance v15, Landroid/graphics/RectF;

    .line 164
    aget v13, v4, v8

    int-to-float v13, v13

    add-float/2addr v13, v6

    .line 165
    aget v4, v4, v12

    int-to-float v4, v4

    add-float/2addr v4, v7

    .line 161
    invoke-direct {v15, v10, v14, v13, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 58
    iget-object v1, v1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v4, "android:changeLocation:rectInWindow"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    check-cast v1, Landroid/graphics/RectF;

    .line 59
    iget-object v2, v2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v4, "android:changeLocation:rectInWindow"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    check-cast v2, Landroid/graphics/RectF;

    .line 61
    iget-boolean v4, v0, Lcom/swedbank/mobile/core/ui/a/b;->b:Z

    const/4 v6, 0x0

    if-eqz v4, :cond_1

    .line 62
    invoke-virtual {v9, v6}, Landroid/view/View;->setElevation(F)V

    .line 64
    :cond_1
    iget-boolean v4, v0, Lcom/swedbank/mobile/core/ui/a/b;->c:Z

    if-eqz v4, :cond_2

    const-string v4, "endView"

    .line 65
    invoke-static {v5, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    :cond_2
    const-string v3, "endView"

    .line 68
    invoke-static {v5, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v5, v3}, Landroid/view/View;->setPivotX(F)V

    .line 69
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-virtual {v5, v3}, Landroid/view/View;->setPivotY(F)V

    .line 71
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float v7, v3, v4

    .line 72
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v3, v4

    .line 166
    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v10, v15, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v10

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v10

    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v13

    sub-float/2addr v10, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v10, v13

    add-float/2addr v4, v10

    .line 167
    iget v10, v1, Landroid/graphics/RectF;->top:F

    iget v14, v15, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v14

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v14

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v15

    sub-float/2addr v14, v15

    div-float/2addr v14, v13

    add-float/2addr v10, v14

    .line 168
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-static {v4, v10}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v4

    .line 74
    invoke-virtual {v4}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->floatValue()F

    move-result v10

    invoke-virtual {v4}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v4

    .line 75
    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v13

    add-float/2addr v13, v10

    invoke-virtual {v9, v13}, Landroid/view/View;->setTranslationX(F)V

    .line 76
    invoke-virtual {v9}, Landroid/view/View;->getTranslationY()F

    move-result v10

    add-float/2addr v10, v4

    invoke-virtual {v9, v10}, Landroid/view/View;->setTranslationY(F)V

    .line 169
    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v10, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v10

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v13

    sub-float/2addr v10, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v10, v13

    add-float/2addr v4, v10

    .line 170
    iget v10, v1, Landroid/graphics/RectF;->top:F

    iget v14, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v14

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v1, v2

    div-float/2addr v1, v13

    add-float/2addr v10, v1

    .line 171
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    .line 79
    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {v1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/b;->getPathMotion()Landroidx/l/g;

    move-result-object v4

    invoke-virtual {v4, v2, v1, v6, v6}, Landroidx/l/g;->a(FFFF)Landroid/graphics/Path;

    move-result-object v10

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/b;->getPathMotion()Landroidx/l/g;

    move-result-object v4

    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v13

    invoke-virtual {v9}, Landroid/view/View;->getTranslationY()F

    move-result v14

    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v15

    sub-float/2addr v15, v2

    invoke-virtual {v9}, Landroid/view/View;->getTranslationY()F

    move-result v2

    sub-float/2addr v2, v1

    invoke-virtual {v4, v13, v14, v15, v2}, Landroidx/l/g;->a(FFFF)Landroid/graphics/Path;

    move-result-object v1

    .line 84
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    const/16 v4, 0x8

    .line 85
    new-array v4, v4, [Landroid/animation/Animator;

    .line 86
    sget-object v13, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    sget-object v14, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    invoke-static {v5, v13, v14, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v13

    check-cast v13, Landroid/animation/Animator;

    aput-object v13, v4, v8

    .line 87
    sget-object v13, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v14, 0x2

    new-array v15, v14, [F

    aput v7, v15, v8

    const/high16 v16, 0x3f800000    # 1.0f

    aput v16, v15, v12

    invoke-static {v5, v13, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    check-cast v13, Landroid/animation/Animator;

    aput-object v13, v4, v12

    .line 88
    sget-object v13, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v15, v14, [F

    aput v3, v15, v8

    aput v16, v15, v12

    invoke-static {v5, v13, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    check-cast v13, Landroid/animation/Animator;

    aput-object v13, v4, v14

    const/4 v13, 0x3

    .line 89
    sget-object v15, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v14, [F

    invoke-virtual {v5}, Landroid/view/View;->getAlpha()F

    move-result v17

    aput v17, v6, v8

    aput v16, v6, v12

    invoke-static {v5, v15, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    check-cast v6, Landroid/animation/Animator;

    aput-object v6, v4, v13

    const/4 v6, 0x4

    .line 90
    sget-object v13, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    sget-object v15, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    invoke-static {v9, v13, v15, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v13

    check-cast v13, Landroid/animation/Animator;

    aput-object v13, v4, v6

    const/4 v6, 0x5

    .line 91
    sget-object v13, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v15, v12, [F

    int-to-float v14, v12

    div-float v16, v14, v7

    aput v16, v15, v8

    invoke-static {v9, v13, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    check-cast v13, Landroid/animation/Animator;

    aput-object v13, v4, v6

    const/4 v6, 0x6

    .line 92
    sget-object v13, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v15, v12, [F

    div-float/2addr v14, v3

    aput v14, v15, v8

    invoke-static {v9, v13, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    check-cast v13, Landroid/animation/Animator;

    aput-object v13, v4, v6

    const/4 v6, 0x7

    .line 93
    sget-object v13, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v14, 0x2

    new-array v14, v14, [F

    invoke-virtual {v9}, Landroid/view/View;->getAlpha()F

    move-result v15

    aput v15, v14, v8

    const/4 v8, 0x0

    aput v8, v14, v12

    invoke-static {v9, v13, v14}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    check-cast v8, Landroid/animation/Animator;

    aput-object v8, v4, v6

    .line 85
    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 95
    new-instance v12, Lcom/swedbank/mobile/core/ui/a/b$a;

    move-object v4, v12

    move-object v6, v10

    move v8, v3

    move-object v10, v1

    move-object/from16 v11, p1

    invoke-direct/range {v4 .. v11}, Lcom/swedbank/mobile/core/ui/a/b$a;-><init>(Landroid/view/View;Landroid/graphics/Path;FFLandroid/view/View;Landroid/graphics/Path;Landroid/view/ViewGroup;)V

    check-cast v12, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v12}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 102
    check-cast v2, Landroid/animation/Animator;

    invoke-static {v0, v2}, Lcom/swedbank/mobile/core/ui/b;->a(Landroidx/l/n;Landroid/animation/Animator;)V

    return-object v2

    .line 59
    :cond_3
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.graphics.RectF"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_4
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.graphics.RectF"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    :goto_0
    return-object v3
.end method
