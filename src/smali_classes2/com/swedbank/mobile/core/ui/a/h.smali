.class public final Lcom/swedbank/mobile/core/ui/a/h;
.super Landroidx/l/n;
.source "OverlaySwitchTransition.kt"


# instance fields
.field private final a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Landroidx/l/n;-><init>()V

    const/4 v0, 0x2

    .line 28
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/a/h;->a:[I

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/a/h;)[I
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/a/h;->a:[I

    return-object p0
.end method


# virtual methods
.method public captureEndValues(Landroidx/l/t;)V
    .locals 9
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "values"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 124
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 128
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    .line 129
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/a/h;->a(Lcom/swedbank/mobile/core/ui/a/h;)[I

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 131
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float v2, v2, v3

    .line 132
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    move-result v4

    mul-float v3, v3, v4

    const/4 v4, 0x0

    .line 133
    aget v5, v1, v4

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    const/4 v6, 0x1

    .line 134
    aget v8, v1, v6

    int-to-float v8, v8

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v3

    div-float/2addr v0, v7

    add-float/2addr v8, v0

    .line 135
    new-instance v0, Landroid/graphics/RectF;

    .line 138
    aget v4, v1, v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    .line 139
    aget v1, v1, v6

    int-to-float v1, v1

    add-float/2addr v1, v3

    .line 135
    invoke-direct {v0, v5, v8, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 140
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "valuesMap"

    .line 141
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "android:overlayswitch:visible"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "android:overlayswitch:rectInWindow"

    .line 142
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public captureStartValues(Landroidx/l/t;)V
    .locals 9
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "values"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 102
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    .line 107
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/a/h;->a(Lcom/swedbank/mobile/core/ui/a/h;)[I

    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 109
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float v2, v2, v3

    .line 110
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    move-result v4

    mul-float v3, v3, v4

    const/4 v4, 0x0

    .line 111
    aget v5, v1, v4

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    const/4 v6, 0x1

    .line 112
    aget v8, v1, v6

    int-to-float v8, v8

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v3

    div-float/2addr v0, v7

    add-float/2addr v8, v0

    .line 113
    new-instance v0, Landroid/graphics/RectF;

    .line 116
    aget v4, v1, v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    .line 117
    aget v1, v1, v6

    int-to-float v1, v1

    add-float/2addr v1, v3

    .line 113
    invoke-direct {v0, v5, v8, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 118
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "valuesMap"

    .line 119
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "android:overlayswitch:visible"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "android:overlayswitch:rectInWindow"

    .line 120
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 16
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "sceneRoot"

    move-object/from16 v8, p1

    invoke-static {v8, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 61
    iget-object v3, v1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v4, "android:overlayswitch:visible"

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 62
    iget-object v3, v2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v4, "android:overlayswitch:visible"

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    goto/16 :goto_0

    .line 66
    :cond_0
    iget-object v5, v2, Landroidx/l/t;->b:Landroid/view/View;

    const-string v2, "endView"

    .line 67
    invoke-static {v5, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/view/View;->getElevation()F

    move-result v6

    .line 68
    invoke-virtual {v5}, Landroid/view/View;->getTranslationZ()F

    move-result v7

    const/4 v2, 0x0

    .line 70
    invoke-virtual {v5, v2}, Landroid/view/View;->setElevation(F)V

    .line 71
    invoke-virtual {v5, v2}, Landroid/view/View;->setTranslationZ(F)V

    .line 73
    iget-object v9, v1, Landroidx/l/t;->b:Landroid/view/View;

    .line 74
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/ViewGroupOverlay;->add(Landroid/view/View;)V

    const-string v2, "startView"

    .line 76
    invoke-static {v9, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/swedbank/mobile/core/ui/a/h;->a:[I

    .line 155
    invoke-virtual {v9, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 156
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v9}, Landroid/view/View;->getScaleX()F

    move-result v4

    mul-float v3, v3, v4

    .line 157
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v9}, Landroid/view/View;->getScaleY()F

    move-result v10

    mul-float v4, v4, v10

    const/4 v10, 0x0

    .line 158
    aget v11, v2, v10

    int-to-float v11, v11

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v12

    int-to-float v12, v12

    sub-float/2addr v12, v3

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    add-float/2addr v11, v12

    const/4 v12, 0x1

    .line 159
    aget v14, v2, v12

    int-to-float v14, v14

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v15

    int-to-float v15, v15

    sub-float/2addr v15, v4

    div-float/2addr v15, v13

    add-float/2addr v14, v15

    .line 160
    new-instance v15, Landroid/graphics/RectF;

    .line 163
    aget v13, v2, v10

    int-to-float v13, v13

    add-float/2addr v13, v3

    .line 164
    aget v2, v2, v12

    int-to-float v2, v2

    add-float/2addr v2, v4

    .line 160
    invoke-direct {v15, v11, v14, v13, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 77
    iget-object v1, v1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v2, "android:overlayswitch:rectInWindow"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Landroid/graphics/RectF;

    .line 165
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, v15, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v4

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 166
    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget v11, v15, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v11

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v11

    sub-float/2addr v1, v11

    div-float/2addr v1, v4

    add-float/2addr v3, v1

    .line 167
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {v1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 80
    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v3

    add-float/2addr v3, v2

    invoke-virtual {v9, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 81
    invoke-virtual {v9}, Landroid/view/View;->getTranslationY()F

    move-result v2

    add-float/2addr v2, v1

    invoke-virtual {v9, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 84
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 85
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    invoke-virtual {v5}, Landroid/view/View;->getAlpha()F

    move-result v4

    aput v4, v3, v10

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v12

    invoke-static {v5, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    check-cast v2, Landroid/animation/Animator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 86
    new-instance v2, Lcom/swedbank/mobile/core/ui/a/h$a;

    move-object v4, v2

    move-object/from16 v8, p1

    invoke-direct/range {v4 .. v9}, Lcom/swedbank/mobile/core/ui/a/h$a;-><init>(Landroid/view/View;FFLandroid/view/ViewGroup;Landroid/view/View;)V

    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 97
    check-cast v1, Landroid/animation/Animator;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/b;->a(Landroidx/l/n;Landroid/animation/Animator;)V

    return-object v1

    .line 77
    :cond_1
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.graphics.RectF"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public isTransitionRequired(Landroidx/l/t;Landroidx/l/t;)Z
    .locals 1
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 51
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v0, "android:overlayswitch:visible"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 52
    iget-object p1, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string p2, "android:overlayswitch:visible"

    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
