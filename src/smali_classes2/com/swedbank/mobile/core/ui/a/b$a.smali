.class public final Lcom/swedbank/mobile/core/ui/a/b$a;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ChangeLocationTransform.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/a/b;->createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/graphics/Path;

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:Landroid/view/View;

.field final synthetic f:Landroid/graphics/Path;

.field final synthetic g:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/graphics/Path;FFLandroid/view/View;Landroid/graphics/Path;Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->b:Landroid/graphics/Path;

    iput p3, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->c:F

    iput p4, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->d:F

    iput-object p5, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->e:Landroid/view/View;

    iput-object p6, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->f:Landroid/graphics/Path;

    iput-object p7, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->g:Landroid/view/ViewGroup;

    .line 95
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 97
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 98
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->g:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/a/b$a;->e:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroupOverlay;->remove(Landroid/view/View;)V

    return-void
.end method
