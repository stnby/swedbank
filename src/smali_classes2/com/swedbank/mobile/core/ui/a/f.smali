.class public final Lcom/swedbank/mobile/core/ui/a/f;
.super Landroidx/l/n;
.source "MoveAwayComeBackTransform.kt"


# instance fields
.field private final a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Landroidx/l/n;-><init>()V

    const/4 v0, 0x2

    .line 20
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/a/f;->a:[I

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/core/ui/a/f;)[I
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/core/ui/a/f;->a:[I

    return-object p0
.end method


# virtual methods
.method public captureEndValues(Landroidx/l/t;)V
    .locals 2
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "values"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 114
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 118
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v0, "valuesMap"

    .line 119
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "android:moveawaycomebacktransform:visible"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public captureStartValues(Landroidx/l/t;)V
    .locals 2
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "values"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "view"

    .line 106
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v0, "valuesMap"

    .line 111
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "android:moveawaycomebacktransform:visible"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 16
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "sceneRoot"

    move-object/from16 v10, p1

    invoke-static {v10, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const v5, 0x3e19999a    # 0.15f

    const/4 v6, 0x2

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-eqz v1, :cond_1

    .line 46
    iget-object v11, v1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v12, "android:moveawaycomebacktransform:visible"

    invoke-interface {v11, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 47
    iget-object v11, v1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "startValues.view"

    invoke-static {v11, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/view/ViewGroupOverlay;->add(Landroid/view/View;)V

    .line 125
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/f;->a(Lcom/swedbank/mobile/core/ui/a/f;)[I

    move-result-object v2

    .line 126
    invoke-virtual {v11, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 127
    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v12

    int-to-float v12, v12

    invoke-virtual {v11}, Landroid/view/View;->getScaleX()F

    move-result v13

    mul-float v12, v12, v13

    .line 128
    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual {v11}, Landroid/view/View;->getScaleY()F

    move-result v14

    mul-float v13, v13, v14

    .line 129
    aget v14, v2, v9

    int-to-float v14, v14

    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v15

    int-to-float v15, v15

    sub-float/2addr v15, v12

    div-float/2addr v15, v7

    add-float/2addr v14, v15

    .line 130
    aget v15, v2, v8

    int-to-float v15, v15

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v13

    div-float/2addr v1, v7

    add-float/2addr v15, v1

    .line 131
    new-instance v1, Landroid/graphics/RectF;

    .line 134
    aget v3, v2, v9

    int-to-float v3, v3

    add-float/2addr v3, v12

    .line 135
    aget v2, v2, v8

    int-to-float v2, v2

    add-float/2addr v2, v13

    .line 131
    invoke-direct {v1, v14, v15, v3, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 136
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v5

    .line 138
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v7

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    goto :goto_0

    :cond_0
    neg-float v2, v2

    .line 144
    :goto_0
    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v6, [F

    aput v4, v3, v9

    aput v2, v3, v8

    invoke-static {v11, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 148
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v11, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/f;->getDuration()J

    move-result-wide v2

    .line 152
    new-instance v12, Landroid/animation/AnimatorSet;

    invoke-direct {v12}, Landroid/animation/AnimatorSet;-><init>()V

    .line 153
    new-array v4, v6, [Landroid/animation/Animator;

    long-to-float v6, v2

    const v7, 0x3f2e147b    # 0.68f

    mul-float v6, v6, v7

    float-to-long v6, v6

    .line 157
    invoke-virtual {v5, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-string v7, "it"

    .line 156
    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v7

    check-cast v7, Landroid/animation/TimeInterpolator;

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v6, Landroid/animation/Animator;

    aput-object v6, v4, v9

    .line 162
    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-string v7, "it"

    .line 161
    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v7

    check-cast v7, Landroid/animation/TimeInterpolator;

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v6, Landroid/animation/Animator;

    aput-object v6, v4, v8

    .line 153
    invoke-virtual {v12, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 165
    new-instance v13, Lcom/swedbank/mobile/core/ui/a/f$a;

    move-object v4, v13

    move-wide v6, v2

    move-object v8, v1

    const/4 v1, 0x0

    move v9, v1

    move-object/from16 v10, p1

    invoke-direct/range {v4 .. v11}, Lcom/swedbank/mobile/core/ui/a/f$a;-><init>(Landroid/animation/ObjectAnimator;JLandroid/animation/ObjectAnimator;ZLandroid/view/ViewGroup;Landroid/view/View;)V

    check-cast v13, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v12, v13}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 174
    check-cast v12, Landroid/animation/Animator;

    invoke-static {v0, v12}, Lcom/swedbank/mobile/core/ui/b;->a(Landroidx/l/n;Landroid/animation/Animator;)V

    goto/16 :goto_2

    :cond_1
    if-eqz v2, :cond_3

    .line 50
    iget-object v1, v2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v3, "android:moveawaycomebacktransform:visible"

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 51
    iget-object v11, v2, Landroidx/l/t;->b:Landroid/view/View;

    const-string v1, "endValues.view"

    invoke-static {v11, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/f;->a(Lcom/swedbank/mobile/core/ui/a/f;)[I

    move-result-object v2

    .line 181
    invoke-virtual {v11, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 182
    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v11}, Landroid/view/View;->getScaleX()F

    move-result v12

    mul-float v3, v3, v12

    .line 183
    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v12

    int-to-float v12, v12

    invoke-virtual {v11}, Landroid/view/View;->getScaleY()F

    move-result v13

    mul-float v12, v12, v13

    .line 184
    aget v13, v2, v9

    int-to-float v13, v13

    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v14

    int-to-float v14, v14

    sub-float/2addr v14, v3

    div-float/2addr v14, v7

    add-float/2addr v13, v14

    .line 185
    aget v14, v2, v8

    int-to-float v14, v14

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v15

    int-to-float v15, v15

    sub-float/2addr v15, v12

    div-float/2addr v15, v7

    add-float/2addr v14, v15

    .line 186
    new-instance v15, Landroid/graphics/RectF;

    .line 189
    aget v1, v2, v9

    int-to-float v1, v1

    add-float/2addr v1, v3

    .line 190
    aget v2, v2, v8

    int-to-float v2, v2

    add-float/2addr v2, v12

    .line 186
    invoke-direct {v15, v13, v14, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 191
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v5

    .line 193
    invoke-virtual {v15}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v7

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    goto :goto_1

    :cond_2
    neg-float v1, v1

    .line 198
    :goto_1
    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v6, [F

    aput v1, v3, v9

    aput v4, v3, v8

    invoke-static {v11, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 202
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_1

    invoke-static {v11, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 206
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/core/ui/a/f;->getDuration()J

    move-result-wide v2

    .line 207
    new-instance v12, Landroid/animation/AnimatorSet;

    invoke-direct {v12}, Landroid/animation/AnimatorSet;-><init>()V

    .line 208
    new-array v4, v6, [Landroid/animation/Animator;

    long-to-float v6, v2

    const v7, 0x3f2e147b    # 0.68f

    mul-float v6, v6, v7

    float-to-long v6, v6

    .line 212
    invoke-virtual {v5, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-string v7, "it"

    .line 211
    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v7

    check-cast v7, Landroid/animation/TimeInterpolator;

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v6, Landroid/animation/Animator;

    aput-object v6, v4, v9

    .line 217
    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-string v7, "it"

    .line 216
    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object v7

    check-cast v7, Landroid/animation/TimeInterpolator;

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast v6, Landroid/animation/Animator;

    aput-object v6, v4, v8

    .line 208
    invoke-virtual {v12, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 220
    new-instance v13, Lcom/swedbank/mobile/core/ui/a/f$b;

    move-object v4, v13

    move-wide v6, v2

    move-object v8, v1

    const/4 v1, 0x1

    move v9, v1

    move-object/from16 v10, p1

    invoke-direct/range {v4 .. v11}, Lcom/swedbank/mobile/core/ui/a/f$b;-><init>(Landroid/animation/ObjectAnimator;JLandroid/animation/ObjectAnimator;ZLandroid/view/ViewGroup;Landroid/view/View;)V

    check-cast v13, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v12, v13}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 229
    check-cast v12, Landroid/animation/Animator;

    invoke-static {v0, v12}, Lcom/swedbank/mobile/core/ui/b;->a(Landroidx/l/n;Landroid/animation/Animator;)V

    goto :goto_2

    :cond_3
    const/4 v12, 0x0

    :goto_2
    return-object v12

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public isTransitionRequired(Landroidx/l/t;Landroidx/l/t;)Z
    .locals 1
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    .line 38
    iget-object p1, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v0, "android:moveawaycomebacktransform:visible"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    .line 39
    iget-object p1, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string p2, "android:moveawaycomebacktransform:visible"

    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
