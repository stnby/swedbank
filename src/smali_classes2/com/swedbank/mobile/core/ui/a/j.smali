.class public final Lcom/swedbank/mobile/core/ui/a/j;
.super Landroidx/l/n;
.source "RecolorBackgroundTransition.kt"


# instance fields
.field private final a:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property<",
            "Landroid/graphics/drawable/ColorDrawable;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 18
    invoke-direct {p0}, Landroidx/l/n;-><init>()V

    .line 19
    new-instance v0, Lcom/swedbank/mobile/core/ui/a/j$a;

    const-string v1, "colorDrawableColor"

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/a/j$a;-><init>(Ljava/lang/String;)V

    check-cast v0, Landroid/util/Property;

    iput-object v0, p0, Lcom/swedbank/mobile/core/ui/a/j;->a:Landroid/util/Property;

    return-void
.end method


# virtual methods
.method public captureEndValues(Landroidx/l/t;)V
    .locals 3
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transitionValues"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v1, "transitionValues.values"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "android:recolorBackgroundTransition:background"

    iget-object p1, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v2, "transitionValues.view"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public captureStartValues(Landroidx/l/t;)V
    .locals 3
    .param p1    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transitionValues"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p1, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v1, "transitionValues.values"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "android:recolorBackgroundTransition:background"

    iget-object p1, p1, Landroidx/l/t;->b:Landroid/view/View;

    const-string v2, "transitionValues.view"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroidx/l/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "sceneRoot"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_6

    if-eqz p3, :cond_6

    .line 39
    iget-object v1, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v2, "android:recolorBackgroundTransition:background"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 42
    :cond_0
    iget-object p2, p2, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v1, "android:recolorBackgroundTransition:background"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    instance-of v1, p2, Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    move-object p2, v0

    :cond_1
    check-cast p2, Landroid/graphics/drawable/Drawable;

    if-eqz p2, :cond_2

    goto :goto_0

    :cond_2
    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/core/ui/t$c;->window_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {p2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 43
    :goto_0
    iget-object p3, p3, Landroidx/l/t;->a:Ljava/util/Map;

    const-string v1, "android:recolorBackgroundTransition:background"

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    instance-of v1, p3, Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    move-object p3, v0

    :cond_3
    check-cast p3, Landroid/graphics/drawable/Drawable;

    if-eqz p3, :cond_4

    goto :goto_1

    :cond_4
    new-instance p3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/swedbank/mobile/core/ui/t$c;->window_background:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-direct {p3, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast p3, Landroid/graphics/drawable/Drawable;

    .line 44
    :goto_1
    instance-of p1, p2, Landroid/graphics/drawable/ColorDrawable;

    if-eqz p1, :cond_5

    .line 45
    instance-of p1, p3, Landroid/graphics/drawable/ColorDrawable;

    if-eqz p1, :cond_5

    check-cast p2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p2}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result p1

    move-object v1, p3

    check-cast v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v2

    if-eq p1, v2, :cond_5

    .line 47
    invoke-virtual {v1}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result p1

    .line 48
    invoke-virtual {p2}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/a/j;->a:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result p2

    aput p2, v1, v2

    const/4 p2, 0x1

    aput p1, v1, p2

    invoke-static {p3, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object p1

    .line 51
    new-instance p2, Landroid/animation/ArgbEvaluator;

    invoke-direct {p2}, Landroid/animation/ArgbEvaluator;-><init>()V

    check-cast p2, Landroid/animation/TypeEvaluator;

    invoke-virtual {p1, p2}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    check-cast p1, Landroid/animation/Animator;

    return-object p1

    :cond_5
    return-object v0

    :cond_6
    :goto_2
    return-object v0
.end method
