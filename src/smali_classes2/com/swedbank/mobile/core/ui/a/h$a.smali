.class public final Lcom/swedbank/mobile/core/ui/a/h$a;
.super Landroid/animation/AnimatorListenerAdapter;
.source "OverlaySwitchTransition.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/a/h;->createAnimator(Landroid/view/ViewGroup;Landroidx/l/t;Landroidx/l/t;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:Landroid/view/ViewGroup;

.field final synthetic e:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;FFLandroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->a:Landroid/view/View;

    iput p2, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->b:F

    iput p3, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->c:F

    iput-object p4, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->d:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->e:Landroid/view/View;

    .line 86
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 88
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 89
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->a:Landroid/view/View;

    .line 90
    iget v0, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->b:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setElevation(F)V

    .line 91
    iget v0, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->c:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationZ(F)V

    .line 93
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->d:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/a/h$a;->e:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroupOverlay;->remove(Landroid/view/View;)V

    return-void
.end method
