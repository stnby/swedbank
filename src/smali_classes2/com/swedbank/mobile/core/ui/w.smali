.class public final enum Lcom/swedbank/mobile/core/ui/w;
.super Ljava/lang/Enum;
.source "Bitmaps.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/core/ui/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/core/ui/w;

.field public static final enum b:Lcom/swedbank/mobile/core/ui/w;

.field private static final synthetic c:[Lcom/swedbank/mobile/core/ui/w;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/swedbank/mobile/core/ui/w;

    new-instance v1, Lcom/swedbank/mobile/core/ui/w;

    const-string v2, "CROP"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/w;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/w;->a:Lcom/swedbank/mobile/core/ui/w;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/core/ui/w;

    const-string v2, "FIT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/w;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/core/ui/w;->b:Lcom/swedbank/mobile/core/ui/w;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/core/ui/w;->c:[Lcom/swedbank/mobile/core/ui/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/core/ui/w;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/core/ui/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/core/ui/w;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/core/ui/w;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/core/ui/w;->c:[Lcom/swedbank/mobile/core/ui/w;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/core/ui/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/core/ui/w;

    return-object v0
.end method
