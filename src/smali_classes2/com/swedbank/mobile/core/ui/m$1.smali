.class final Lcom/swedbank/mobile/core/ui/m$1;
.super Lkotlin/e/b/k;
.source "ShimmeringViews.kt"

# interfaces
.implements Lkotlin/e/a/r;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/m;-><init>(Landroid/content/res/Resources;FLandroid/graphics/Paint;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/r<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Landroid/graphics/Canvas;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/core/ui/m;

.field final synthetic b:Landroid/graphics/drawable/GradientDrawable;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/core/ui/m;Landroid/graphics/drawable/GradientDrawable;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/m$1;->a:Lcom/swedbank/mobile/core/ui/m;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/m$1;->b:Landroid/graphics/drawable/GradientDrawable;

    const/4 p1, 0x4

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    check-cast p4, Landroid/graphics/Canvas;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/core/ui/m$1;->a(IIILandroid/graphics/Canvas;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(IIILandroid/graphics/Canvas;)V
    .locals 3
    .param p4    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/m$1;->b:Landroid/graphics/drawable/GradientDrawable;

    int-to-float v1, p2

    iget-object v2, p0, Lcom/swedbank/mobile/core/ui/m$1;->a:Lcom/swedbank/mobile/core/ui/m;

    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, p1, p2, p3, v1}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 48
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/m$1;->b:Landroid/graphics/drawable/GradientDrawable;

    sget-object p2, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/GradientDrawable;->setOrientation(Landroid/graphics/drawable/GradientDrawable$Orientation;)V

    .line 49
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/m$1;->b:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p1, p4}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method
