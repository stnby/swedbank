.class final Lcom/swedbank/mobile/core/ui/o;
.super Ljava/lang/Object;
.source "ViewBindings.kt"

# interfaces
.implements Lkotlin/f/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/f/c<",
        "TT;TV;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Object;

.field private final b:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "TT;",
            "Lkotlin/h/g<",
            "*>;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/e/a/m;)V
    .locals 1
    .param p1    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/m<",
            "-TT;-",
            "Lkotlin/h/g<",
            "*>;+TV;>;)V"
        }
    .end annotation

    const-string v0, "initializer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/o;->b:Lkotlin/e/a/m;

    .line 108
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/o;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;
    .locals 2
    .param p2    # Lkotlin/h/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lkotlin/h/g<",
            "*>;)TV;"
        }
    .end annotation

    const-string v0, "property"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/o;->a:Ljava/lang/Object;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/o;->b:Lkotlin/e/a/m;

    invoke-interface {v0, p1, p2}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/o;->a:Ljava/lang/Object;

    .line 115
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/core/ui/o;->a:Ljava/lang/Object;

    return-object p1
.end method
