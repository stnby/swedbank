.class public final Lcom/swedbank/mobile/core/ui/p;
.super Landroidx/recyclerview/widget/f$a;
.source "Diffs.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<OldItemsType:",
        "Ljava/lang/Object;",
        "NewItemsType:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/recyclerview/widget/f$a;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TOldItemsType;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TNewItemsType;>;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "TOldItemsType;TNewItemsType;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "TOldItemsType;TNewItemsType;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lkotlin/e/a/m;Lkotlin/e/a/m;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TOldItemsType;>;",
            "Ljava/util/List<",
            "+TNewItemsType;>;",
            "Lkotlin/e/a/m<",
            "-TOldItemsType;-TNewItemsType;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/e/a/m<",
            "-TOldItemsType;-TNewItemsType;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "oldList"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newList"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "areItemsTheSame"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "areContentsTheSame"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Landroidx/recyclerview/widget/f$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/p;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/p;->b:Ljava/util/List;

    iput-object p3, p0, Lcom/swedbank/mobile/core/ui/p;->c:Lkotlin/e/a/m;

    iput-object p4, p0, Lcom/swedbank/mobile/core/ui/p;->d:Lkotlin/e/a/m;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/p;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(II)Z
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/p;->c:Lkotlin/e/a/m;

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/p;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/p;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public b()I
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b(II)Z
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/p;->d:Lkotlin/e/a/m;

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/p;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    iget-object v1, p0, Lcom/swedbank/mobile/core/ui/p;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method
