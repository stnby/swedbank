.class public final Lcom/swedbank/mobile/core/ui/m;
.super Ljava/lang/Object;
.source "ShimmeringViews.kt"


# instance fields
.field private final a:Lkotlin/e/a/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/r<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Canvas;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/e/a/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/r<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Canvas;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:F

.field private final d:F


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;FLandroid/graphics/Paint;)V
    .locals 5
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "separatorPaint"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget v0, Lcom/swedbank/mobile/core/ui/t$d;->list_item_elevation:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 35
    :goto_0
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    if-eqz v0, :cond_1

    .line 40
    sget-object p2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 p3, 0x2

    .line 41
    new-array p3, p3, [I

    .line 42
    sget v0, Lcom/swedbank/mobile/core/ui/t$c;->bottom_shadow_start_color:I

    const/4 v4, 0x0

    .line 185
    invoke-static {p1, v0, v4}, Landroidx/core/a/a/f;->b(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v0

    aput v0, p3, v1

    .line 43
    sget v0, Lcom/swedbank/mobile/core/ui/t$c;->bottom_shadow_end_color:I

    .line 187
    invoke-static {p1, v0, v4}, Landroidx/core/a/a/f;->b(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result p1

    aput p1, p3, v2

    .line 39
    new-instance p1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p1, p2, p3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const-string p2, "metrics"

    .line 44
    invoke-static {v3, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float p2, v2

    .line 188
    invoke-static {v2, p2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p2

    iput p2, p0, Lcom/swedbank/mobile/core/ui/m;->c:F

    const/4 p2, 0x3

    int-to-float p2, p2

    .line 189
    invoke-static {v2, p2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p2

    iput p2, p0, Lcom/swedbank/mobile/core/ui/m;->d:F

    .line 46
    new-instance p2, Lcom/swedbank/mobile/core/ui/m$1;

    invoke-direct {p2, p0, p1}, Lcom/swedbank/mobile/core/ui/m$1;-><init>(Lcom/swedbank/mobile/core/ui/m;Landroid/graphics/drawable/GradientDrawable;)V

    check-cast p2, Lkotlin/e/a/r;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/m;->a:Lkotlin/e/a/r;

    .line 51
    new-instance p2, Lcom/swedbank/mobile/core/ui/m$2;

    invoke-direct {p2, p0, p1}, Lcom/swedbank/mobile/core/ui/m$2;-><init>(Lcom/swedbank/mobile/core/ui/m;Landroid/graphics/drawable/GradientDrawable;)V

    check-cast p2, Lkotlin/e/a/r;

    iput-object p2, p0, Lcom/swedbank/mobile/core/ui/m;->b:Lkotlin/e/a/r;

    goto :goto_1

    .line 58
    :cond_1
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    const-string v0, "metrics"

    .line 59
    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float v0, v2

    .line 190
    invoke-static {v2, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    const/4 v1, 0x0

    .line 60
    iput v1, p0, Lcom/swedbank/mobile/core/ui/m;->c:F

    .line 61
    iput v0, p0, Lcom/swedbank/mobile/core/ui/m;->d:F

    .line 62
    sget-object v1, Lcom/swedbank/mobile/core/ui/m$3;->a:Lcom/swedbank/mobile/core/ui/m$3;

    check-cast v1, Lkotlin/e/a/r;

    iput-object v1, p0, Lcom/swedbank/mobile/core/ui/m;->a:Lkotlin/e/a/r;

    .line 63
    new-instance v1, Lcom/swedbank/mobile/core/ui/m$4;

    invoke-direct {v1, p1, p2, v0, p3}, Lcom/swedbank/mobile/core/ui/m$4;-><init>(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    check-cast v1, Lkotlin/e/a/r;

    iput-object v1, p0, Lcom/swedbank/mobile/core/ui/m;->b:Lkotlin/e/a/r;

    :goto_1
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .line 30
    iget v0, p0, Lcom/swedbank/mobile/core/ui/m;->c:F

    return v0
.end method

.method public final a(IIILandroid/graphics/Canvas;)V
    .locals 1
    .param p4    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/m;->a:Lkotlin/e/a/r;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {v0, p1, p2, p3, p4}, Lkotlin/e/a/r;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b()F
    .locals 1

    .line 31
    iget v0, p0, Lcom/swedbank/mobile/core/ui/m;->d:F

    return v0
.end method

.method public final b(IIILandroid/graphics/Canvas;)V
    .locals 1
    .param p4    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/swedbank/mobile/core/ui/m;->b:Lkotlin/e/a/r;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {v0, p1, p2, p3, p4}, Lkotlin/e/a/r;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
