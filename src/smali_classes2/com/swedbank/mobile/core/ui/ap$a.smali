.class final Lcom/swedbank/mobile/core/ui/ap$a;
.super Ljava/lang/Object;
.source "Views.kt"

# interfaces
.implements Landroidx/core/widget/NestedScrollView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;Landroidx/core/widget/NestedScrollView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:F


# direct methods
.method constructor <init>(Landroid/view/View;F)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/core/ui/ap$a;->a:Landroid/view/View;

    iput p2, p0, Lcom/swedbank/mobile/core/ui/ap$a;->b:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroidx/core/widget/NestedScrollView;IIII)V
    .locals 0
    .param p1    # Landroidx/core/widget/NestedScrollView;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    int-to-float p1, p3

    .line 70
    iget p2, p0, Lcom/swedbank/mobile/core/ui/ap$a;->b:F

    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    .line 71
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/ap$a;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getTranslationZ()F

    move-result p2

    cmpg-float p2, p2, p1

    if-eqz p2, :cond_0

    .line 72
    iget-object p2, p0, Lcom/swedbank/mobile/core/ui/ap$a;->a:Landroid/view/View;

    invoke-virtual {p2, p1}, Landroid/view/View;->setTranslationZ(F)V

    :cond_0
    return-void
.end method
