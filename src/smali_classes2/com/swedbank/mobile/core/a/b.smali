.class public final enum Lcom/swedbank/mobile/core/a/b;
.super Ljava/lang/Enum;
.source "AnalyticsEvent.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/core/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/core/a/b;

.field public static final enum b:Lcom/swedbank/mobile/core/a/b;

.field public static final enum c:Lcom/swedbank/mobile/core/a/b;

.field public static final enum d:Lcom/swedbank/mobile/core/a/b;

.field public static final enum e:Lcom/swedbank/mobile/core/a/b;

.field public static final enum f:Lcom/swedbank/mobile/core/a/b;

.field public static final enum g:Lcom/swedbank/mobile/core/a/b;

.field public static final enum h:Lcom/swedbank/mobile/core/a/b;

.field public static final enum i:Lcom/swedbank/mobile/core/a/b;

.field public static final enum j:Lcom/swedbank/mobile/core/a/b;

.field private static final synthetic k:[Lcom/swedbank/mobile/core/a/b;


# instance fields
.field private final l:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/swedbank/mobile/core/a/b;

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "WALLET_PAY_BTN_CLICK"

    const-string v3, "e_wallet_pay_btn_click"

    const/4 v4, 0x0

    .line 13
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->a:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "WALLET_CARD_SELECTED_FOR_PAYMENT"

    const-string v3, "e_wallet_card_selected_for_payment"

    const/4 v4, 0x1

    .line 14
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->b:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "WALLET_PAY_AUTH_REQUESTED"

    const-string v3, "e_wallet_pay_auth_requested"

    const/4 v4, 0x2

    .line 15
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->c:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "WALLET_PAY_AUTH_SUCCESS"

    const-string v3, "e_wallet_pay_auth_success"

    const/4 v4, 0x3

    .line 16
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->d:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "WALLET_PAY_SUCCESS"

    const-string v3, "e_wallet_pay_success"

    const/4 v4, 0x4

    .line 17
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->e:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "WALLET_PAY_TRY_AGAIN"

    const-string v3, "e_wallet_pay_try_again"

    const/4 v4, 0x5

    .line 18
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->f:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "WALLET_PAY_FATAL_ERROR"

    const-string v3, "e_wallet_pay_fatal_error"

    const/4 v4, 0x6

    .line 19
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->g:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "APP_SHORTCUT_WALLET_PAY"

    const-string v3, "e_app_shortcut_wallet_pay"

    const/4 v4, 0x7

    .line 20
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->h:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "DEVICE_ROOT_REASON"

    const-string v3, "e_device_root_reason"

    const/16 v4, 0x8

    .line 21
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->i:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/b;

    const-string v2, "BIOMETRIC_AUTHENTICATION_TIME"

    const-string v3, "e_biometric_authentication_time"

    const/16 v4, 0x9

    .line 22
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/b;->j:Lcom/swedbank/mobile/core/a/b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/swedbank/mobile/core/a/b;->k:[Lcom/swedbank/mobile/core/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/swedbank/mobile/core/a/b;->l:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/core/a/b;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/core/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/core/a/b;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/core/a/b;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/core/a/b;->k:[Lcom/swedbank/mobile/core/a/b;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/core/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/core/a/b;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/core/a/b;->l:Ljava/lang/String;

    return-object v0
.end method
