.class public final Lcom/swedbank/mobile/core/a/e;
.super Ljava/lang/Object;
.source "AnalyticsManagerProvider.kt"


# direct methods
.method public static final a(Landroid/content/Context;)Lcom/swedbank/mobile/core/a/c;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$analyticsManager"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/swedbank/mobile/core/a/d;

    invoke-interface {p0}, Lcom/swedbank/mobile/core/a/d;->c()Lcom/swedbank/mobile/core/a/c;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.core.analytics.AnalyticsManagerProvider"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
