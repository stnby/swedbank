.class public final enum Lcom/swedbank/mobile/core/a/f;
.super Ljava/lang/Enum;
.source "AnalyticsManager.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/core/a/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/core/a/f;

.field public static final enum b:Lcom/swedbank/mobile/core/a/f;

.field private static final synthetic c:[Lcom/swedbank/mobile/core/a/f;


# instance fields
.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/swedbank/mobile/core/a/f;

    new-instance v1, Lcom/swedbank/mobile/core/a/f;

    const-string v2, "BUTTON"

    const-string v3, "button"

    const/4 v4, 0x0

    .line 15
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/f;->a:Lcom/swedbank/mobile/core/a/f;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/core/a/f;

    const-string v2, "VIEW"

    const-string v3, "view"

    const/4 v4, 0x1

    .line 16
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/core/a/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/core/a/f;->b:Lcom/swedbank/mobile/core/a/f;

    aput-object v1, v0, v4

    sput-object v0, Lcom/swedbank/mobile/core/a/f;->c:[Lcom/swedbank/mobile/core/a/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/swedbank/mobile/core/a/f;->d:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/core/a/f;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/core/a/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/core/a/f;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/core/a/f;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/core/a/f;->c:[Lcom/swedbank/mobile/core/a/f;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/core/a/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/core/a/f;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/core/a/f;->d:Ljava/lang/String;

    return-object v0
.end method
