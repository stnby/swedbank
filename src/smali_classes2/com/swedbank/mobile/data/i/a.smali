.class public final Lcom/swedbank/mobile/data/i/a;
.super Ljava/lang/Object;
.source "WalletOnboardingRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/h;


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/a/a/a/f;)V
    .locals 3
    .param p1    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "walletOnboardingCompletedPreference"

    .line 27
    sget-object v1, Lcom/swedbank/mobile/business/onboarding/i;->a:Lcom/swedbank/mobile/business/onboarding/i;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/onboarding/i;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 26
    invoke-virtual {p1, v0, v1}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/a/a/a/d;

    move-result-object v0

    const-string v1, "preferences.getInteger(\n\u2026D, NOT_COMPLETED.ordinal)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/i/a;->a:Lcom/a/a/a/d;

    const-string v0, "walletWelcomeWasShownToUserPreference"

    const/4 v1, 0x0

    .line 29
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;

    move-result-object v0

    const-string v2, "preferences\n      .getBo\u2026WAS_SHOWN_TO_USER, false)"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/i/a;->b:Lcom/a/a/a/d;

    const-string v0, "walletNotPossibleAckPreference"

    .line 31
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;

    move-result-object p1

    const-string v0, "preferences\n      .getBo\u2026_NOT_POSSIBLE_ACK, false)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/i/a;->c:Lcom/a/a/a/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/i/a;)Lcom/a/a/a/d;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/i/a;->a:Lcom/a/a/a/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/i/a;)Lcom/a/a/a/d;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/i/a;->b:Lcom/a/a/a/d;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/i/a;)Lcom/a/a/a/d;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/i/a;->c:Lcom/a/a/a/d;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/onboarding/i;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/swedbank/mobile/data/i/a$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/i/a$c;-><init>(Lcom/swedbank/mobile/data/i/a;Lcom/swedbank/mobile/business/onboarding/i;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .fromA\u2026(false)\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Z)Lio/reactivex/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 59
    new-instance v0, Lcom/swedbank/mobile/data/i/a$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/i/a$b;-><init>(Lcom/swedbank/mobile/data/i/a;Z)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .fromA\u2026edged.set(acknowledged) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/onboarding/i;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/data/i/a;->a:Lcom/a/a/a/d;

    .line 34
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 35
    sget-object v1, Lcom/swedbank/mobile/data/i/a$a;->a:Lcom/swedbank/mobile/data/i/a$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "walletOnboardingStatus\n \u2026    .map { values()[it] }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/data/i/a;->b:Lcom/a/a/a/d;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/a/a/d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public c()Z
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/data/i/a;->b:Lcom/a/a/a/d;

    invoke-interface {v0}, Lcom/a/a/a/d;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "walletWelcomeWasShownToUser.get()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public d()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/data/i/a;->c:Lcom/a/a/a/d;

    .line 56
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "walletNotPossibleAcknowl\u2026ged\n      .asObservable()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
