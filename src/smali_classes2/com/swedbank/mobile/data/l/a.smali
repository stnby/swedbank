.class public final Lcom/swedbank/mobile/data/l/a;
.super Lorg/threeten/bp/format/DateTimeTextProvider;
.source "AndroidDateTimeTextProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/l/a$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/util/Map$Entry<",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Ljava/util/Locale;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeTextProvider;-><init>()V

    .line 45
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/swedbank/mobile/data/l/a;->a:Ljava/util/Map;

    return-void
.end method

.method private final a(Landroidx/c/a;)Lcom/swedbank/mobile/data/l/a$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/c/a<",
            "Lorg/threeten/bp/format/TextStyle;",
            "Landroidx/c/d<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/swedbank/mobile/data/l/a$a;"
        }
    .end annotation

    .line 227
    sget-object v0, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v0}, Landroidx/c/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/threeten/bp/format/TextStyle;->FULL_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v0}, Landroidx/c/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    move-object v0, p1

    check-cast v0, Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->FULL_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    sget-object v2, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v2}, Landroidx/c/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    :cond_0
    sget-object v0, Lorg/threeten/bp/format/TextStyle;->SHORT:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v0}, Landroidx/c/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lorg/threeten/bp/format/TextStyle;->SHORT_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v0}, Landroidx/c/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 231
    move-object v0, p1

    check-cast v0, Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->SHORT_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    sget-object v2, Lorg/threeten/bp/format/TextStyle;->SHORT:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v2}, Landroidx/c/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :cond_1
    sget-object v0, Lorg/threeten/bp/format/TextStyle;->NARROW:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v0}, Landroidx/c/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lorg/threeten/bp/format/TextStyle;->NARROW_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v0}, Landroidx/c/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 234
    move-object v0, p1

    check-cast v0, Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->NARROW_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    sget-object v2, Lorg/threeten/bp/format/TextStyle;->NARROW:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {p1, v2}, Landroidx/c/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    :cond_2
    new-instance v0, Lcom/swedbank/mobile/data/l/a$a;

    check-cast p1, Ljava/util/Map;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/l/a$a;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private final a(Lorg/threeten/bp/temporal/TemporalField;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 2

    .line 56
    invoke-static {p1, p2}, Lcom/swedbank/mobile/data/l/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/swedbank/mobile/data/l/a;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 59
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/data/l/a;->b(Lorg/threeten/bp/temporal/TemporalField;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object p1

    .line 60
    iget-object p2, p0, Lcom/swedbank/mobile/data/l/a;->a:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/swedbank/mobile/data/l/a;->a:Ljava/util/Map;

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/data/l/a;->a:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private final b(Lorg/threeten/bp/temporal/TemporalField;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 67
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->MONTH_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v4, 0x0

    const-wide/16 v7, 0x1

    if-ne v1, v3, :cond_a

    .line 68
    invoke-static/range {p2 .. p2}, Ljava/text/DateFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DateFormatSymbols;

    move-result-object v1

    .line 69
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v9, ""

    invoke-direct {v3, v9, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 70
    new-instance v2, Landroidx/c/a;

    invoke-direct {v2}, Landroidx/c/a;-><init>()V

    const-string v9, "oldSymbols"

    .line 72
    invoke-static {v1, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v9

    .line 73
    move-object v10, v2

    check-cast v10, Ljava/util/Map;

    sget-object v11, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    const-string v12, "months"

    invoke-static {v9, v12}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    check-cast v4, Lkotlin/e/a/b;

    .line 283
    new-instance v12, Landroidx/c/d;

    array-length v13, v9

    invoke-direct {v12, v13}, Landroidx/c/d;-><init>(I)V

    const/4 v13, 0x0

    :goto_0
    const/16 v14, 0xb

    if-gt v13, v14, :cond_1

    int-to-long v14, v13

    add-long/2addr v14, v7

    .line 288
    aget-object v6, v9, v13

    if-eqz v4, :cond_0

    invoke-interface {v4, v6}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    if-eqz v16, :cond_0

    move-object/from16 v6, v16

    .line 285
    :cond_0
    invoke-virtual {v12, v14, v15, v6}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 73
    :cond_1
    invoke-interface {v10, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v6, Lorg/threeten/bp/format/TextStyle;->NARROW:Lorg/threeten/bp/format/TextStyle;

    .line 75
    sget-object v11, Lcom/swedbank/mobile/data/l/a$b;->a:Lcom/swedbank/mobile/data/l/a$b;

    check-cast v11, Lkotlin/e/a/b;

    .line 292
    new-instance v12, Landroidx/c/d;

    array-length v13, v9

    invoke-direct {v12, v13}, Landroidx/c/d;-><init>(I)V

    const/4 v13, 0x0

    :goto_1
    if-gt v13, v14, :cond_3

    int-to-long v14, v13

    add-long/2addr v14, v7

    .line 297
    aget-object v5, v9, v13

    if-eqz v11, :cond_2

    invoke-interface {v11, v5}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    if-eqz v16, :cond_2

    move-object/from16 v5, v16

    .line 294
    :cond_2
    invoke-virtual {v12, v14, v15, v5}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    add-int/lit8 v13, v13, 0x1

    const/16 v14, 0xb

    goto :goto_1

    .line 74
    :cond_3
    invoke-interface {v10, v6, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v5, Lorg/threeten/bp/format/TextStyle;->SHORT:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v1

    const-string v6, "oldSymbols.shortMonths"

    invoke-static {v1, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 303
    new-instance v6, Landroidx/c/d;

    array-length v9, v1

    invoke-direct {v6, v9}, Landroidx/c/d;-><init>(I)V

    const/4 v9, 0x0

    :goto_2
    const/16 v11, 0xb

    if-gt v9, v11, :cond_5

    int-to-long v11, v9

    add-long/2addr v11, v7

    .line 308
    aget-object v13, v1, v9

    if-eqz v4, :cond_4

    invoke-interface {v4, v13}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    if-eqz v14, :cond_4

    move-object v13, v14

    .line 305
    :cond_4
    invoke-virtual {v6, v11, v12, v13}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 76
    :cond_5
    invoke-interface {v10, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v1, Lorg/threeten/bp/format/TextStyle;->FULL_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    const-string v5, "LLLL"

    .line 313
    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 314
    new-instance v5, Landroidx/c/d;

    const/16 v6, 0xc

    invoke-direct {v5, v6}, Landroidx/c/d;-><init>(I)V

    const/4 v9, 0x0

    :goto_3
    const/16 v11, 0xb

    if-gt v9, v11, :cond_7

    .line 316
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v11, v12, v9}, Ljava/util/Calendar;->set(II)V

    .line 317
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v11

    const-string v12, "dateFormat.calendar"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    int-to-long v12, v9

    add-long/2addr v12, v7

    if-eqz v4, :cond_6

    const-string v14, "it"

    .line 322
    invoke-static {v11, v14}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v11}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    if-eqz v14, :cond_6

    goto :goto_4

    :cond_6
    move-object v14, v11

    .line 318
    :goto_4
    invoke-virtual {v5, v12, v13, v14}, Landroidx/c/d;->c(JLjava/lang/Object;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 77
    :cond_7
    invoke-interface {v10, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v1, Lorg/threeten/bp/format/TextStyle;->SHORT_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    const-string v5, "LLL"

    .line 327
    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 328
    new-instance v5, Landroidx/c/d;

    invoke-direct {v5, v6}, Landroidx/c/d;-><init>(I)V

    const/4 v6, 0x0

    const/16 v9, 0xb

    :goto_5
    if-gt v6, v9, :cond_9

    .line 330
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v11, v12, v6}, Ljava/util/Calendar;->set(II)V

    .line 331
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v11

    const-string v12, "dateFormat.calendar"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    int-to-long v12, v6

    add-long/2addr v12, v7

    if-eqz v4, :cond_8

    const-string v14, "it"

    .line 336
    invoke-static {v11, v14}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v11}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    if-eqz v14, :cond_8

    goto :goto_6

    :cond_8
    move-object v14, v11

    .line 332
    :goto_6
    invoke-virtual {v5, v12, v13, v14}, Landroidx/c/d;->c(JLjava/lang/Object;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 78
    :cond_9
    invoke-interface {v10, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-direct {v0, v2}, Lcom/swedbank/mobile/data/l/a;->a(Landroidx/c/a;)Lcom/swedbank/mobile/data/l/a$a;

    move-result-object v1

    goto/16 :goto_d

    .line 82
    :cond_a
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->DAY_OF_WEEK:Lorg/threeten/bp/temporal/ChronoField;

    if-ne v1, v3, :cond_15

    .line 83
    invoke-static/range {p2 .. p2}, Ljava/text/DateFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DateFormatSymbols;

    move-result-object v1

    .line 84
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v6, ""

    invoke-direct {v3, v6, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 85
    new-instance v2, Landroidx/c/a;

    invoke-direct {v2}, Landroidx/c/a;-><init>()V

    const-string v6, "oldSymbols"

    .line 87
    invoke-static {v1, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v6

    .line 88
    move-object v9, v2

    check-cast v9, Ljava/util/Map;

    sget-object v10, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    const-string v11, "weekdays"

    invoke-static {v6, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    check-cast v4, Lkotlin/e/a/b;

    .line 342
    new-instance v11, Landroidx/c/d;

    invoke-direct {v11}, Landroidx/c/d;-><init>()V

    const/4 v12, 0x1

    :goto_7
    const/4 v13, 0x5

    const/4 v14, 0x7

    if-gt v12, v14, :cond_c

    move-object/from16 v17, v6

    int-to-long v5, v12

    int-to-long v7, v13

    add-long/2addr v5, v7

    int-to-long v7, v14

    .line 345
    rem-long/2addr v5, v7

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    .line 348
    aget-object v7, v17, v12

    if-eqz v4, :cond_b

    invoke-interface {v4, v7}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    if-eqz v8, :cond_b

    move-object v7, v8

    .line 346
    :cond_b
    invoke-virtual {v11, v5, v6, v7}, Landroidx/c/d;->c(JLjava/lang/Object;)V

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v6, v17

    const-wide/16 v7, 0x1

    goto :goto_7

    :cond_c
    move-object/from16 v17, v6

    .line 88
    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v5, Lorg/threeten/bp/format/TextStyle;->NARROW:Lorg/threeten/bp/format/TextStyle;

    .line 90
    sget-object v6, Lcom/swedbank/mobile/data/l/a$c;->a:Lcom/swedbank/mobile/data/l/a$c;

    check-cast v6, Lkotlin/e/a/b;

    .line 352
    new-instance v7, Landroidx/c/d;

    invoke-direct {v7}, Landroidx/c/d;-><init>()V

    const/4 v8, 0x1

    :goto_8
    if-gt v8, v14, :cond_e

    int-to-long v10, v8

    move-object/from16 v19, v2

    move-object/from16 v18, v3

    int-to-long v2, v13

    add-long/2addr v10, v2

    int-to-long v2, v14

    .line 355
    rem-long/2addr v10, v2

    const-wide/16 v2, 0x1

    add-long/2addr v10, v2

    .line 358
    aget-object v2, v17, v8

    if-eqz v6, :cond_d

    invoke-interface {v6, v2}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_d

    move-object v2, v3

    .line 356
    :cond_d
    invoke-virtual {v7, v10, v11, v2}, Landroidx/c/d;->c(JLjava/lang/Object;)V

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v3, v18

    move-object/from16 v2, v19

    goto :goto_8

    :cond_e
    move-object/from16 v19, v2

    move-object/from16 v18, v3

    .line 89
    invoke-interface {v9, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v2, Lorg/threeten/bp/format/TextStyle;->SHORT:Lorg/threeten/bp/format/TextStyle;

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    move-result-object v1

    const-string v3, "oldSymbols.shortWeekdays"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    new-instance v3, Landroidx/c/d;

    invoke-direct {v3}, Landroidx/c/d;-><init>()V

    const/4 v5, 0x1

    :goto_9
    if-gt v5, v14, :cond_10

    int-to-long v6, v5

    int-to-long v10, v13

    add-long/2addr v6, v10

    int-to-long v10, v14

    .line 367
    rem-long/2addr v6, v10

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    .line 370
    aget-object v8, v1, v5

    if-eqz v4, :cond_f

    invoke-interface {v4, v8}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    if-eqz v10, :cond_f

    move-object v8, v10

    .line 368
    :cond_f
    invoke-virtual {v3, v6, v7, v8}, Landroidx/c/d;->c(JLjava/lang/Object;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 91
    :cond_10
    invoke-interface {v9, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v1, Lorg/threeten/bp/format/TextStyle;->FULL_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    const-string v2, "cccc"

    move-object/from16 v3, v18

    .line 375
    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 376
    new-instance v2, Landroidx/c/d;

    invoke-direct {v2}, Landroidx/c/d;-><init>()V

    const/4 v5, 0x1

    :goto_a
    if-gt v5, v14, :cond_12

    .line 378
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6, v14, v5}, Ljava/util/Calendar;->set(II)V

    .line 379
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    const-string v7, "dateFormat.calendar"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    int-to-long v7, v5

    int-to-long v10, v13

    add-long/2addr v7, v10

    int-to-long v10, v14

    .line 391
    rem-long/2addr v7, v10

    const-wide/16 v10, 0x1

    add-long/2addr v7, v10

    if-eqz v4, :cond_11

    const-string v10, "it"

    .line 393
    invoke-static {v6, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    if-eqz v10, :cond_11

    move-object v6, v10

    .line 380
    :cond_11
    invoke-virtual {v2, v7, v8, v6}, Landroidx/c/d;->c(JLjava/lang/Object;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 92
    :cond_12
    invoke-interface {v9, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v1, Lorg/threeten/bp/format/TextStyle;->SHORT_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    const-string v2, "ccc"

    .line 398
    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 399
    new-instance v2, Landroidx/c/d;

    invoke-direct {v2}, Landroidx/c/d;-><init>()V

    const/4 v5, 0x1

    :goto_b
    if-gt v5, v14, :cond_14

    .line 401
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6, v14, v5}, Ljava/util/Calendar;->set(II)V

    .line 402
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    const-string v7, "dateFormat.calendar"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    int-to-long v7, v5

    int-to-long v10, v13

    add-long/2addr v7, v10

    int-to-long v10, v14

    .line 414
    rem-long/2addr v7, v10

    const-wide/16 v10, 0x1

    add-long/2addr v7, v10

    if-eqz v4, :cond_13

    const-string v10, "it"

    .line 416
    invoke-static {v6, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    if-eqz v10, :cond_13

    move-object v6, v10

    .line 403
    :cond_13
    invoke-virtual {v2, v7, v8, v6}, Landroidx/c/d;->c(JLjava/lang/Object;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    .line 93
    :cond_14
    invoke-interface {v9, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, v19

    .line 95
    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/l/a;->a(Landroidx/c/a;)Lcom/swedbank/mobile/data/l/a$a;

    move-result-object v1

    goto/16 :goto_d

    .line 97
    :cond_15
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->AMPM_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x0

    if-ne v1, v3, :cond_16

    .line 98
    invoke-static/range {p2 .. p2}, Ljava/text/DateFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DateFormatSymbols;

    move-result-object v1

    .line 99
    new-instance v2, Landroidx/c/a;

    invoke-direct {v2}, Landroidx/c/a;-><init>()V

    const-string v3, "oldSymbols"

    .line 100
    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v1

    .line 101
    new-instance v3, Landroidx/c/d;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroidx/c/d;-><init>(I)V

    const/4 v6, 0x0

    .line 102
    aget-object v6, v1, v6

    invoke-virtual {v3, v4, v5, v6}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const/4 v4, 0x1

    .line 103
    aget-object v1, v1, v4

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5, v1}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    .line 104
    move-object v1, v2

    check-cast v1, Ljava/util/Map;

    sget-object v4, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v4, Lorg/threeten/bp/format/TextStyle;->SHORT:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-direct {v0, v2}, Lcom/swedbank/mobile/data/l/a;->a(Landroidx/c/a;)Lcom/swedbank/mobile/data/l/a$a;

    move-result-object v1

    goto/16 :goto_d

    .line 109
    :cond_16
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->ERA:Lorg/threeten/bp/temporal/ChronoField;

    if-ne v1, v3, :cond_1a

    .line 110
    invoke-static/range {p2 .. p2}, Ljava/text/DateFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DateFormatSymbols;

    move-result-object v1

    .line 111
    new-instance v3, Landroidx/c/a;

    invoke-direct {v3}, Landroidx/c/a;-><init>()V

    const-string v6, "oldSymbols"

    .line 112
    invoke-static {v1, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getEras()[Ljava/lang/String;

    move-result-object v1

    .line 113
    new-instance v6, Landroidx/c/d;

    const/4 v7, 0x2

    invoke-direct {v6, v7}, Landroidx/c/d;-><init>(I)V

    const/4 v7, 0x0

    .line 114
    aget-object v8, v1, v7

    invoke-virtual {v6, v4, v5, v8}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const/4 v7, 0x1

    .line 115
    aget-object v8, v1, v7

    const-wide/16 v9, 0x1

    invoke-virtual {v6, v9, v10, v8}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    .line 116
    move-object v7, v3

    check-cast v7, Ljava/util/Map;

    sget-object v8, Lorg/threeten/bp/format/TextStyle;->SHORT:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v9, "Locale.ENGLISH"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 118
    new-instance v2, Landroidx/c/d;

    const/4 v6, 0x2

    invoke-direct {v2, v6}, Landroidx/c/d;-><init>(I)V

    const-string v6, "Before Christ"

    .line 119
    invoke-virtual {v2, v4, v5, v6}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const-string v6, "Anno Domini"

    const-wide/16 v8, 0x1

    .line 120
    invoke-virtual {v2, v8, v9, v6}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    .line 121
    sget-object v6, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v7, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c

    .line 124
    :cond_17
    sget-object v2, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v7, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :goto_c
    new-instance v2, Landroidx/c/d;

    const/4 v6, 0x2

    invoke-direct {v2, v6}, Landroidx/c/d;-><init>(I)V

    const/4 v6, 0x0

    .line 127
    aget-object v8, v1, v6

    const-string v9, "array[GregorianCalendar.BC]"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v8, :cond_19

    const/4 v9, 0x1

    invoke-virtual {v8, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const-string v10, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v8, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v4, v5, v8}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    .line 128
    aget-object v1, v1, v9

    const-string v4, "array[GregorianCalendar.AD]"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_18

    invoke-virtual {v1, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5, v1}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    .line 129
    sget-object v1, Lorg/threeten/bp/format/TextStyle;->NARROW:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v7, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-direct {v0, v3}, Lcom/swedbank/mobile/data/l/a;->a(Landroidx/c/a;)Lcom/swedbank/mobile/data/l/a$a;

    move-result-object v1

    goto :goto_d

    .line 128
    :cond_18
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 127
    :cond_19
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_1a
    sget-object v2, Lorg/threeten/bp/temporal/IsoFields;->QUARTER_OF_YEAR:Lorg/threeten/bp/temporal/TemporalField;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 135
    new-instance v1, Landroidx/c/a;

    invoke-direct {v1}, Landroidx/c/a;-><init>()V

    .line 136
    new-instance v2, Landroidx/c/d;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Landroidx/c/d;-><init>(I)V

    const-string v4, "Q1"

    const-wide/16 v5, 0x1

    .line 137
    invoke-virtual {v2, v5, v6, v4}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const-string v4, "Q2"

    const-wide/16 v5, 0x2

    .line 138
    invoke-virtual {v2, v5, v6, v4}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const-string v4, "Q3"

    const-wide/16 v7, 0x3

    .line 139
    invoke-virtual {v2, v7, v8, v4}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const-string v4, "Q4"

    const-wide/16 v9, 0x4

    .line 140
    invoke-virtual {v2, v9, v10, v4}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    .line 141
    move-object v4, v1

    check-cast v4, Ljava/util/Map;

    sget-object v11, Lorg/threeten/bp/format/TextStyle;->SHORT:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v4, v11, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    new-instance v2, Landroidx/c/d;

    invoke-direct {v2, v3}, Landroidx/c/d;-><init>(I)V

    const-string v3, "1st quarter"

    const-wide/16 v11, 0x1

    .line 143
    invoke-virtual {v2, v11, v12, v3}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const-string v3, "2nd quarter"

    .line 144
    invoke-virtual {v2, v5, v6, v3}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const-string v3, "3rd quarter"

    .line 145
    invoke-virtual {v2, v7, v8, v3}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    const-string v3, "4th quarter"

    .line 146
    invoke-virtual {v2, v9, v10, v3}, Landroidx/c/d;->b(JLjava/lang/Object;)V

    .line 147
    sget-object v3, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/l/a;->a(Landroidx/c/a;)Lcom/swedbank/mobile/data/l/a$a;

    move-result-object v1

    goto :goto_d

    :cond_1b
    const-string v1, ""

    :goto_d
    return-object v1
.end method


# virtual methods
.method public getText(Lorg/threeten/bp/temporal/TemporalField;JLorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/threeten/bp/temporal/TemporalField;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lorg/threeten/bp/format/TextStyle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/Locale;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "field"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "style"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0, p1, p5}, Lcom/swedbank/mobile/data/l/a;->a(Lorg/threeten/bp/temporal/TemporalField;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object p1

    instance-of p5, p1, Lcom/swedbank/mobile/data/l/a$a;

    const/4 v0, 0x0

    if-nez p5, :cond_0

    move-object p1, v0

    :cond_0
    check-cast p1, Lcom/swedbank/mobile/data/l/a$a;

    if-eqz p1, :cond_1

    .line 49
    invoke-virtual {p1, p2, p3, p4}, Lcom/swedbank/mobile/data/l/a$a;->a(JLorg/threeten/bp/format/TextStyle;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public getTextIterator(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/util/Iterator;
    .locals 1
    .param p1    # Lorg/threeten/bp/temporal/TemporalField;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lorg/threeten/bp/format/TextStyle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Locale;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Lorg/threeten/bp/format/TextStyle;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/Iterator<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "field"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "style"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1, p3}, Lcom/swedbank/mobile/data/l/a;->a(Lorg/threeten/bp/temporal/TemporalField;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object p1

    instance-of p3, p1, Lcom/swedbank/mobile/data/l/a$a;

    const/4 v0, 0x0

    if-nez p3, :cond_0

    move-object p1, v0

    :cond_0
    check-cast p1, Lcom/swedbank/mobile/data/l/a$a;

    if-eqz p1, :cond_1

    .line 53
    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/data/l/a$a;->a(Lorg/threeten/bp/format/TextStyle;)Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    return-object v0
.end method
