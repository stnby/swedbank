.class public final Lcom/swedbank/mobile/data/l/b;
.super Ljava/lang/Object;
.source "AndroidDateTimeTextProvider.kt"


# direct methods
.method public static final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/swedbank/mobile/data/l/b;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object p0

    return-object p0
.end method

.method public static final a()V
    .locals 1

    .line 32
    :try_start_0
    new-instance v0, Lcom/swedbank/mobile/data/l/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/l/a;-><init>()V

    check-cast v0, Lorg/threeten/bp/format/DateTimeTextProvider;

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeTextProvider;->setInitializer(Lorg/threeten/bp/format/DateTimeTextProvider;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 38
    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private static final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(TA;TB;)",
            "Ljava/util/Map$Entry<",
            "TA;TB;>;"
        }
    .end annotation

    .line 272
    new-instance v0, Ljava/util/AbstractMap$SimpleImmutableEntry;

    invoke-direct {v0, p0, p1}, Ljava/util/AbstractMap$SimpleImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    check-cast v0, Ljava/util/Map$Entry;

    return-object v0
.end method
