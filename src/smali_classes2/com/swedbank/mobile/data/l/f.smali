.class public final Lcom/swedbank/mobile/data/l/f;
.super Ljava/lang/Object;
.source "Rx.kt"


# direct methods
.method public static final a(I)J
    .locals 4

    const-wide/16 v0, 0x1

    shl-long/2addr v0, p0

    const-wide/16 v2, 0x2

    .line 17
    div-long/2addr v0, v2

    const-wide/16 v2, 0x80

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final a(Lio/reactivex/b;ILkotlin/e/a/b;Lkotlin/e/a/a;)Lio/reactivex/b;
    .locals 1
    .param p0    # Lio/reactivex/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/b;",
            "I",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/e/a/a<",
            "Ljava/lang/Double;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$retryWithExponentialBackoff"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retry"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviation"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v0, Lcom/swedbank/mobile/data/l/f$d;

    invoke-direct {v0, p1, p2, p3}, Lcom/swedbank/mobile/data/l/f$d;-><init>(ILkotlin/e/a/b;Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p0, v0}, Lio/reactivex/b;->b(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p0

    const-string p1, "retryWhen { errors ->\n  \u2026rowable>(e)\n      }\n    }"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic a(Lio/reactivex/b;ILkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    .line 72
    sget-object p2, Lcom/swedbank/mobile/data/l/f$c;->a:Lcom/swedbank/mobile/data/l/f$c;

    check-cast p2, Lkotlin/e/a/b;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 73
    new-instance p3, Lcom/swedbank/mobile/data/l/g;

    invoke-direct {p3}, Lcom/swedbank/mobile/data/l/g;-><init>()V

    check-cast p3, Lkotlin/e/a/a;

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/l/f;->a(Lio/reactivex/b;ILkotlin/e/a/b;Lkotlin/e/a/a;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lio/reactivex/w;ILkotlin/e/a/b;Lkotlin/e/a/a;)Lio/reactivex/w;
    .locals 1
    .param p0    # Lio/reactivex/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/w<",
            "TT;>;I",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/e/a/a<",
            "Ljava/lang/Double;",
            ">;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$retryWithExponentialBackoff"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retry"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviation"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/swedbank/mobile/data/l/f$b;

    invoke-direct {v0, p1, p2, p3}, Lcom/swedbank/mobile/data/l/f$b;-><init>(ILkotlin/e/a/b;Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p0, v0}, Lio/reactivex/w;->h(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p0

    const-string p1, "retryWhen { errors ->\n  \u2026rowable>(e)\n      }\n    }"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic a(Lio/reactivex/w;ILkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/w;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    .line 38
    sget-object p2, Lcom/swedbank/mobile/data/l/f$a;->a:Lcom/swedbank/mobile/data/l/f$a;

    check-cast p2, Lkotlin/e/a/b;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 39
    new-instance p3, Lcom/swedbank/mobile/data/l/g;

    invoke-direct {p3}, Lcom/swedbank/mobile/data/l/g;-><init>()V

    check-cast p3, Lkotlin/e/a/a;

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/l/f;->a(Lio/reactivex/w;ILkotlin/e/a/b;Lkotlin/e/a/a;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method
