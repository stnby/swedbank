.class final Lcom/swedbank/mobile/data/l/f$b$1;
.super Ljava/lang/Object;
.source "Rx.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/l/f$b;->a(Lio/reactivex/h;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lorg/reactivestreams/Publisher<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/l/f$b;

.field final synthetic b:Lkotlin/e/b/u$b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/l/f$b;Lkotlin/e/b/u$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/l/f$b$1;->a:Lcom/swedbank/mobile/data/l/f$b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/l/f$b$1;->b:Lkotlin/e/b/u$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lio/reactivex/h;
    .locals 4
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/h<",
            "+",
            "Ljava/io/Serializable;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/data/l/f$b$1;->a:Lcom/swedbank/mobile/data/l/f$b;

    iget-object v0, v0, Lcom/swedbank/mobile/data/l/f$b;->b:Lkotlin/e/a/b;

    invoke-interface {v0, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/l/f$b$1;->b:Lkotlin/e/b/u$b;

    iget v1, v0, Lkotlin/e/b/u$b;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lkotlin/e/b/u$b;->a:I

    iget v0, v0, Lkotlin/e/b/u$b;->a:I

    iget-object v1, p0, Lcom/swedbank/mobile/data/l/f$b$1;->a:Lcom/swedbank/mobile/data/l/f$b;

    iget v1, v1, Lcom/swedbank/mobile/data/l/f$b;->a:I

    if-ge v0, v1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/swedbank/mobile/data/l/f$b$1;->b:Lkotlin/e/b/u$b;

    iget p1, p1, Lkotlin/e/b/u$b;->a:I

    invoke-static {p1}, Lcom/swedbank/mobile/data/l/f;->a(I)J

    move-result-wide v0

    long-to-double v0, v0

    iget-object p1, p0, Lcom/swedbank/mobile/data/l/f$b$1;->a:Lcom/swedbank/mobile/data/l/f$b;

    iget-object p1, p1, Lcom/swedbank/mobile/data/l/f$b;->c:Lkotlin/e/a/a;

    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    mul-double v0, v0, v2

    double-to-long v0, v0

    .line 47
    sget-object p1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Lio/reactivex/j/a;->c()Lio/reactivex/v;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lio/reactivex/h;->a(JLjava/util/concurrent/TimeUnit;Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object p1

    return-object p1

    .line 49
    :cond_0
    invoke-static {p1}, Lio/reactivex/h;->a(Ljava/lang/Throwable;)Lio/reactivex/h;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/l/f$b$1;->a(Ljava/lang/Throwable;)Lio/reactivex/h;

    move-result-object p1

    return-object p1
.end method
