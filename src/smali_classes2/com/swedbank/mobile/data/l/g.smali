.class public final Lcom/swedbank/mobile/data/l/g;
.super Ljava/lang/Object;
.source "Rx.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/a<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/swedbank/mobile/data/l/g;->a:Ljava/util/Random;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Double;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/swedbank/mobile/data/l/g;->a:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    const v2, 0x3e99999a    # 0.3f

    float-to-double v2, v2

    mul-double v0, v0, v2

    const-wide v2, 0x3feb333333333333L    # 0.85

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/l/g;->a()Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method
