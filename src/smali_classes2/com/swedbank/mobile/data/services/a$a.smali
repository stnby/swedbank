.class final Lcom/swedbank/mobile/data/services/a$a;
.super Ljava/lang/Object;
.source "ServicesRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/services/a;->a(Z)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/services/a;

.field final synthetic b:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/services/a;Z)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/a$a;->a:Lcom/swedbank/mobile/data/services/a;

    iput-boolean p2, p0, Lcom/swedbank/mobile/data/services/a$a;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/a$a;->a:Lcom/swedbank/mobile/data/services/a;

    invoke-static {v0}, Lcom/swedbank/mobile/data/services/a;->a(Lcom/swedbank/mobile/data/services/a;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    .line 30
    iget-boolean v1, p0, Lcom/swedbank/mobile/data/services/a$a;->b:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/services/a$a;->a:Lcom/swedbank/mobile/data/services/a;

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 226
    sget-object v1, Lcom/swedbank/mobile/business/services/a/a;->b:Lcom/swedbank/mobile/business/services/a/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    check-cast v0, Ljava/util/List;

    goto/16 :goto_2

    .line 31
    :cond_0
    iget-object v1, p0, Lcom/swedbank/mobile/data/services/a$a;->a:Lcom/swedbank/mobile/data/services/a;

    .line 228
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0x10

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 230
    sget-object v3, Lcom/swedbank/mobile/business/services/a/a;->b:Lcom/swedbank/mobile/business/services/a/a;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x5

    .line 233
    new-array v4, v3, [Lcom/swedbank/mobile/business/services/a/a;

    .line 234
    sget-object v5, Lcom/swedbank/mobile/business/services/a/a;->e:Lcom/swedbank/mobile/business/services/a/a;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 235
    sget-object v5, Lcom/swedbank/mobile/business/services/a/a;->f:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v5, v4, v2

    .line 236
    sget-object v5, Lcom/swedbank/mobile/business/services/a/a;->g:Lcom/swedbank/mobile/business/services/a/a;

    const/4 v7, 0x2

    aput-object v5, v4, v7

    .line 237
    sget-object v5, Lcom/swedbank/mobile/business/services/a/a;->h:Lcom/swedbank/mobile/business/services/a/a;

    const/4 v8, 0x3

    aput-object v5, v4, v8

    .line 238
    sget-object v5, Lcom/swedbank/mobile/business/services/a/a;->i:Lcom/swedbank/mobile/business/services/a/a;

    const/4 v9, 0x4

    aput-object v5, v4, v9

    .line 239
    move-object v5, v1

    check-cast v5, Ljava/util/Collection;

    invoke-static {v4}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object v4

    invoke-static {v5, v4}, Lkotlin/a/h;->a(Ljava/util/Collection;Lkotlin/i/e;)Z

    .line 241
    sget-object v4, Lcom/swedbank/mobile/data/services/b;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v10

    aget v4, v4, v10

    if-eq v4, v2, :cond_1

    .line 248
    new-array v4, v9, [Lcom/swedbank/mobile/business/services/a/a;

    .line 249
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->j:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v4, v6

    .line 250
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->k:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v4, v2

    .line 251
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->l:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v4, v7

    .line 252
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->m:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v4, v8

    .line 253
    invoke-static {v4}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object v4

    invoke-static {v5, v4}, Lkotlin/a/h;->a(Ljava/util/Collection;Lkotlin/i/e;)Z

    goto :goto_0

    .line 242
    :cond_1
    new-array v4, v8, [Lcom/swedbank/mobile/business/services/a/a;

    .line 243
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->j:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v4, v6

    .line 244
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->k:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v4, v2

    .line 245
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->m:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v4, v7

    .line 246
    invoke-static {v4}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object v4

    invoke-static {v5, v4}, Lkotlin/a/h;->a(Ljava/util/Collection;Lkotlin/i/e;)Z

    .line 258
    :goto_0
    sget-object v4, Lcom/swedbank/mobile/data/services/b;->b:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v0

    aget v0, v4, v0

    const/4 v4, 0x6

    if-eq v0, v2, :cond_2

    const/4 v0, 0x7

    .line 268
    new-array v0, v0, [Lcom/swedbank/mobile/business/services/a/a;

    .line 269
    sget-object v10, Lcom/swedbank/mobile/business/services/a/a;->n:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v10, v0, v6

    .line 270
    sget-object v6, Lcom/swedbank/mobile/business/services/a/a;->o:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v6, v0, v2

    .line 271
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->p:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v7

    .line 272
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->q:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v8

    .line 273
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->r:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v9

    .line 274
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->s:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v3

    .line 275
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->t:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v4

    .line 276
    invoke-static {v0}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object v0

    invoke-static {v5, v0}, Lkotlin/a/h;->a(Ljava/util/Collection;Lkotlin/i/e;)Z

    goto :goto_1

    .line 259
    :cond_2
    new-array v0, v4, [Lcom/swedbank/mobile/business/services/a/a;

    .line 260
    sget-object v4, Lcom/swedbank/mobile/business/services/a/a;->n:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v4, v0, v6

    .line 261
    sget-object v4, Lcom/swedbank/mobile/business/services/a/a;->o:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v4, v0, v2

    .line 262
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->q:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v7

    .line 263
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->r:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v8

    .line 264
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->s:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v9

    .line 265
    sget-object v2, Lcom/swedbank/mobile/business/services/a/a;->t:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v2, v0, v3

    .line 266
    invoke-static {v0}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object v0

    invoke-static {v5, v0}, Lkotlin/a/h;->a(Ljava/util/Collection;Lkotlin/i/e;)Z

    .line 228
    :goto_1
    move-object v0, v1

    check-cast v0, Ljava/util/List;

    :goto_2
    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/services/a$a;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
