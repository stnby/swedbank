.class final Lcom/swedbank/mobile/data/services/agreements/a$c;
.super Lkotlin/e/b/k;
.source "ServicesAgreementsRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/services/agreements/a;->e()Lkotlin/e/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Landroid/content/Intent;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/services/agreements/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/services/agreements/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/agreements/a$c;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "install"

    .line 70
    iget-object v1, p0, Lcom/swedbank/mobile/data/services/agreements/a$c;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-static {v1}, Lcom/swedbank/mobile/data/services/agreements/a;->a(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/b/c/b;

    move-result-object v1

    .line 81
    invoke-virtual {v1}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Lcom/swedbank/mobile/business/util/l;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 70
    invoke-static {v1, v3, v2, v3}, Lcom/swedbank/mobile/business/util/m;->a(Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/a;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void

    .line 81
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/services/agreements/a$c;->a(Landroid/content/Intent;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
