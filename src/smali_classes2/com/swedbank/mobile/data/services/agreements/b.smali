.class public final Lcom/swedbank/mobile/data/services/agreements/b;
.super Ljava/lang/Object;
.source "ServicesAgreementsRepositoryImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/services/agreements/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/services/agreements/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/services/agreements/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/swedbank/mobile/data/services/agreements/b;->a:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/swedbank/mobile/data/services/agreements/b;->b:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/swedbank/mobile/data/services/agreements/b;->c:Ljavax/inject/Provider;

    .line 26
    iput-object p4, p0, Lcom/swedbank/mobile/data/services/agreements/b;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/services/agreements/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/services/agreements/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;)",
            "Lcom/swedbank/mobile/data/services/agreements/b;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/swedbank/mobile/data/services/agreements/b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/data/services/agreements/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/services/agreements/a;
    .locals 5

    .line 31
    new-instance v0, Lcom/swedbank/mobile/data/services/agreements/a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/services/agreements/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/swedbank/mobile/data/services/agreements/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/services/agreements/c;

    iget-object v3, p0, Lcom/swedbank/mobile/data/services/agreements/b;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/c/a;

    iget-object v4, p0, Lcom/swedbank/mobile/data/services/agreements/b;->d:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/moshi/n;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/data/services/agreements/a;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/data/services/agreements/c;Lcom/swedbank/mobile/business/c/a;Lcom/squareup/moshi/n;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/services/agreements/b;->a()Lcom/swedbank/mobile/data/services/agreements/a;

    move-result-object v0

    return-object v0
.end method
