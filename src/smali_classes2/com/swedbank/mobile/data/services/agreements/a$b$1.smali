.class public final Lcom/swedbank/mobile/data/services/agreements/a$b$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/services/agreements/a$b;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/services/agreements/a$b;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/services/agreements/a$b;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->a:Lcom/swedbank/mobile/data/services/agreements/a$b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/services/agreements/mpos/MPosAgreementResponse;

    .line 236
    iget-object v1, p0, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->a:Lcom/swedbank/mobile/data/services/agreements/a$b;

    iget-object v1, v1, Lcom/swedbank/mobile/data/services/agreements/a$b;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-static {v1}, Lcom/swedbank/mobile/data/services/agreements/a;->a(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/b/c/b;

    move-result-object v1

    if-eqz v0, :cond_2

    .line 239
    iget-object v2, p0, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->a:Lcom/swedbank/mobile/data/services/agreements/a$b;

    iget-object v2, v2, Lcom/swedbank/mobile/data/services/agreements/a$b;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-static {v2}, Lcom/swedbank/mobile/data/services/agreements/a;->b(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->name()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).toUpperCase()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    iget-object v3, p0, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->a:Lcom/swedbank/mobile/data/services/agreements/a$b;

    iget-object v3, v3, Lcom/swedbank/mobile/data/services/agreements/a$b;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-static {v3}, Lcom/swedbank/mobile/data/services/agreements/a;->b(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "(this as java.lang.String).toLowerCase()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    new-instance v4, Lcom/swedbank/mobile/data/services/agreements/mpos/MPosAgreement;

    .line 246
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/services/agreements/mpos/MPosAgreementResponse;->a()Ljava/lang/String;

    move-result-object v5

    .line 249
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/services/agreements/mpos/MPosAgreementResponse;->b()Ljava/util/List;

    move-result-object v0

    .line 245
    invoke-direct {v4, v5, v2, v3, v0}, Lcom/swedbank/mobile/data/services/agreements/mpos/MPosAgreement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 244
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->a:Lcom/swedbank/mobile/data/services/agreements/a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/data/services/agreements/a$b;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-static {v0}, Lcom/swedbank/mobile/data/services/agreements/a;->c(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/squareup/moshi/n;

    move-result-object v0

    .line 251
    const-class v2, Lcom/swedbank/mobile/data/services/agreements/mpos/MPosAgreement;

    invoke-virtual {v0, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "jsonMapper.adapter(T::class.java).toJson(this)"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    goto :goto_0

    .line 240
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    .line 236
    :goto_0
    invoke-virtual {v1, v0}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/services/agreements/a$b$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
