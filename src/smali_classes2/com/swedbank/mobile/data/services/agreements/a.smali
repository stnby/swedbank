.class public final Lcom/swedbank/mobile/data/services/agreements/a;
.super Ljava/lang/Object;
.source "ServicesAgreementsRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/services/plugins/agreements/g;


# instance fields
.field private final a:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/app/Application;

.field private final d:Lcom/swedbank/mobile/data/services/agreements/c;

.field private final e:Lcom/swedbank/mobile/business/c/a;

.field private final f:Lcom/squareup/moshi/n;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/data/services/agreements/c;Lcom/swedbank/mobile/business/c/a;Lcom/squareup/moshi/n;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/services/agreements/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "servicesAgreementsService"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/agreements/a;->c:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/data/services/agreements/a;->d:Lcom/swedbank/mobile/data/services/agreements/c;

    iput-object p3, p0, Lcom/swedbank/mobile/data/services/agreements/a;->e:Lcom/swedbank/mobile/business/c/a;

    iput-object p4, p0, Lcom/swedbank/mobile/data/services/agreements/a;->f:Lcom/squareup/moshi/n;

    .line 36
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Opt\u2026nal<MPosAgreementJson>>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/agreements/a;->a:Lcom/b/c/b;

    .line 37
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Boolean>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/agreements/a;->b:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/b/c/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->a:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->e:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/squareup/moshi/n;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->f:Lcom/squareup/moshi/n;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/b/c/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->b:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/data/services/agreements/a;)Landroid/app/Application;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->c:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->a:Lcom/b/c/b;

    .line 40
    sget-object v1, Lcom/swedbank/mobile/data/services/agreements/a$e;->a:Lcom/swedbank/mobile/data/services/agreements/a$e;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "mPosAgreementStream\n      .map { it is Present }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->d:Lcom/swedbank/mobile/data/services/agreements/c;

    .line 45
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/services/agreements/d;->a(Lcom/swedbank/mobile/data/services/agreements/c;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 46
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 81
    new-instance v0, Lcom/swedbank/mobile/data/services/agreements/a$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/services/agreements/a$b;-><init>(Lcom/swedbank/mobile/data/services/agreements/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/agreements/a;->b:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    new-instance v0, Lcom/swedbank/mobile/data/services/agreements/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/services/agreements/a$a;-><init>(Lcom/swedbank/mobile/data/services/agreements/a;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromCallable\u2026e(MPOS_PACKAGE_NAME))\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "ee.ellore.swedmpos"

    return-object v0
.end method

.method public e()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Landroid/content/Intent;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    new-instance v0, Lcom/swedbank/mobile/data/services/agreements/a$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/services/agreements/a$c;-><init>(Lcom/swedbank/mobile/data/services/agreements/a;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public f()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Landroid/content/Intent;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 73
    new-instance v0, Lcom/swedbank/mobile/data/services/agreements/a$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/services/agreements/a$d;-><init>(Lcom/swedbank/mobile/data/services/agreements/a;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method
