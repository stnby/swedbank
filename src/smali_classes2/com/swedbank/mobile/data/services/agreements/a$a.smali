.class final Lcom/swedbank/mobile/data/services/agreements/a$a;
.super Ljava/lang/Object;
.source "ServicesAgreementsRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/services/agreements/a;->c()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/services/agreements/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/services/agreements/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/agreements/a$a;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/agreements/a$a;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-static {v0}, Lcom/swedbank/mobile/data/services/agreements/a;->d(Lcom/swedbank/mobile/data/services/agreements/a;)Lcom/b/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/data/services/agreements/a$a;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/services/agreements/a$a;->a:Lcom/swedbank/mobile/data/services/agreements/a;

    invoke-static {v1}, Lcom/swedbank/mobile/data/services/agreements/a;->e(Lcom/swedbank/mobile/data/services/agreements/a;)Landroid/app/Application;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-string v2, "ee.ellore.swedmpos"

    .line 81
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v3, 0x0

    .line 84
    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v1

    const-string v4, "packageManager\n      .getInstalledPackages(0)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 85
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Landroid/content/pm/PackageInfo;

    .line 83
    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v5, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    const/4 v3, 0x1

    :cond_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/services/agreements/a$a;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
