.class public final Lcom/swedbank/mobile/data/services/a;
.super Ljava/lang/Object;
.source "ServicesRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/services/j;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferenceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/services/a;->a:Lcom/swedbank/mobile/business/c/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/services/a;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/data/services/a;->a:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/services/a/a;Ljava/lang/String;Z)Lcom/swedbank/mobile/business/e;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/services/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/data/services/a;->a:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    const/4 v1, 0x2

    if-eqz p3, :cond_1

    .line 234
    new-instance p3, Lcom/swedbank/mobile/business/e;

    .line 235
    sget-object v0, Lcom/swedbank/mobile/data/services/b;->r:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/a/a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 240
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank business service path not implemented"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_0
    const-string v0, "business/d2d/mobilejump"

    goto :goto_0

    :pswitch_1
    const-string v0, "business/d2d/payments/exchange/rates"

    goto :goto_0

    :pswitch_2
    const-string v0, "business/d2d/payments/exchange/currencyExchange"

    .line 242
    :goto_0
    sget-object v2, Lcom/swedbank/mobile/data/services/b;->s:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/a/a;->ordinal()I

    move-result p1

    aget p1, v2, p1

    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    .line 246
    invoke-static {}, Lkotlin/a/x;->a()Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 243
    :cond_0
    new-array p1, v1, [Lkotlin/k;

    const/4 v1, 0x0

    const-string v3, "redirectToPaymentListPage"

    const-string v4, "true"

    .line 244
    invoke-static {v3, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v3

    aput-object v3, p1, v1

    const-string v1, "customer"

    .line 245
    invoke-static {v1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p2

    aput-object p2, p1, v2

    .line 243
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/b;->a([Lkotlin/k;)Ljava/util/Map;

    move-result-object p1

    .line 234
    :goto_1
    invoke-direct {p3, v0, p1}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_3

    .line 248
    :cond_1
    new-instance p3, Lcom/swedbank/mobile/business/e;

    .line 249
    sget-object p2, Lcom/swedbank/mobile/data/services/b;->q:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/a/a;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_1

    .line 339
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 336
    :pswitch_3
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->p:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_2

    .line 339
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_5
    const-string p1, "private/insurance/accident/start"

    goto/16 :goto_2

    :pswitch_6
    const-string p1, "private/insurance/reports/start"

    goto/16 :goto_2

    .line 331
    :pswitch_7
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->o:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_3

    .line 334
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_9
    const-string p1, "private/insurance/home/lppi"

    goto/16 :goto_2

    :pswitch_a
    const-string p1, "private/insurance/credit/lppi"

    goto/16 :goto_2

    .line 326
    :pswitch_b
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->n:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_4

    .line 329
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_c
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_d
    const-string p1, "private/insurance/home/creditcover"

    goto/16 :goto_2

    :pswitch_e
    const-string p1, "private/insurance/credit/creditcover"

    goto/16 :goto_2

    .line 320
    :pswitch_f
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->m:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_5

    .line 324
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_10
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_11
    const-string p1, "private/insurance/home/travelInsurance"

    goto/16 :goto_2

    :pswitch_12
    const-string p1, "private/insurance/travel/insurance"

    goto/16 :goto_2

    :pswitch_13
    const-string p1, "private/insurance/travel/travelInsurance"

    goto/16 :goto_2

    .line 315
    :pswitch_14
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->l:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_6

    .line 318
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_15
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 317
    :pswitch_16
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "TRAFFIC_INSURANCE service does not exist for LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_17
    const-string p1, "private/insurance/car/traffic"

    goto/16 :goto_2

    .line 310
    :pswitch_18
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->k:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_7

    .line 313
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_19
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_1a
    const-string p1, "private/insurance/home/casco"

    goto/16 :goto_2

    :pswitch_1b
    const-string p1, "private/insurance/car/casco"

    goto/16 :goto_2

    :pswitch_1c
    const-string p1, "private/insurance/home/ihome"

    goto/16 :goto_2

    .line 301
    :pswitch_1d
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->j:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_8

    .line 305
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_1e
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_1f
    const-string p1, "private/insurance/life/privatePortfolio"

    goto/16 :goto_2

    :pswitch_20
    const-string p1, "private/investor/lifeInsurance/privatePortfolio"

    goto/16 :goto_2

    :pswitch_21
    const-string p1, "private/investor/funds/privatePortfolio"

    goto/16 :goto_2

    .line 295
    :pswitch_22
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->i:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_9

    .line 299
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_23
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_24
    const-string p1, "private/insurance/life/guarPension"

    goto/16 :goto_2

    .line 297
    :pswitch_25
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "SAFE_PENSION_FUND service does not exist for LV"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_26
    const-string p1, "private/pensions/pillar3/vPlus"

    goto/16 :goto_2

    .line 289
    :pswitch_27
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->h:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_a

    .line 293
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_28
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_29
    const-string p1, "private/insurance/life/child"

    goto/16 :goto_2

    :pswitch_2a
    const-string p1, "private/investor/lifeInsurance/collegefund"

    goto/16 :goto_2

    :pswitch_2b
    const-string p1, "private/investor/children/collegefund"

    goto/16 :goto_2

    :pswitch_2c
    const-string p1, "private/insurance/life/life"

    goto/16 :goto_2

    .line 280
    :pswitch_2d
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->g:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_b

    .line 284
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_2e
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_2f
    const-string p1, "private/credit/leasing"

    goto/16 :goto_2

    :pswitch_30
    const-string p1, "private/credit/leasing/car/carleasing"

    goto/16 :goto_2

    :pswitch_31
    const-string p1, "private/credit/leasing/car"

    goto/16 :goto_2

    .line 274
    :pswitch_32
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->f:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_c

    .line 278
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_33
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_34
    const-string p1, "private/credit/leasing"

    goto/16 :goto_2

    :pswitch_35
    const-string p1, "private/credit/leasing/car/carloan"

    goto/16 :goto_2

    :pswitch_36
    const-string p1, "private/credit/leasing/car/car_small_loan"

    goto/16 :goto_2

    .line 268
    :pswitch_37
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->e:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_d

    .line 272
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_38
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_39
    const-string p1, "private/credit/loans/home/housing_loan"

    goto :goto_2

    :pswitch_3a
    const-string p1, "private/credit/loans/home/mortage"

    goto :goto_2

    :pswitch_3b
    const-string p1, "private/credit/loans/home"

    goto :goto_2

    .line 262
    :pswitch_3c
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->d:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_e

    .line 266
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_3d
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_3e
    const-string p1, "private/credit/loans/home_small_loan"

    goto :goto_2

    :pswitch_3f
    const-string p1, "private/credit/loans/homeSmall"

    goto :goto_2

    :pswitch_40
    const-string p1, "private/credit/loans/homeloans/homeSmall/home_small_loan/home_small_loan"

    goto :goto_2

    .line 257
    :pswitch_41
    sget-object p1, Lcom/swedbank/mobile/data/services/b;->c:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_f

    .line 260
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_42
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Ibank service paths only known for EE, LV and LT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_43
    const-string p1, "private/credit/loans/newSmall"

    goto :goto_2

    :pswitch_44
    const-string p1, "private/credit/loans/newSmall/small_loan/small_loan"

    goto :goto_2

    :pswitch_45
    const-string p1, "private/d2d/payments2/list"

    goto :goto_2

    :pswitch_46
    const-string p1, "private/d2d/payments2/ebill"

    goto :goto_2

    :pswitch_47
    const-string p1, "private/d2d/payments2/rates/currency"

    goto :goto_2

    :pswitch_48
    const-string p1, "private/d2d/payments2/rates/currencyExchange"

    :goto_2
    const/4 p2, 0x0

    .line 248
    invoke-direct {p3, p1, p2, v1, p2}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    :goto_3
    return-object p3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_41
        :pswitch_3c
        :pswitch_37
        :pswitch_32
        :pswitch_2d
        :pswitch_2c
        :pswitch_27
        :pswitch_22
        :pswitch_1d
        :pswitch_1c
        :pswitch_18
        :pswitch_14
        :pswitch_f
        :pswitch_b
        :pswitch_7
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_a
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_e
        :pswitch_e
        :pswitch_d
        :pswitch_c
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_17
        :pswitch_17
        :pswitch_16
        :pswitch_15
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x1
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
    .end packed-switch

    :pswitch_data_b
    .packed-switch 0x1
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
    .end packed-switch

    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
    .end packed-switch

    :pswitch_data_d
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
    .end packed-switch

    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
    .end packed-switch

    :pswitch_data_f
    .packed-switch 0x1
        :pswitch_44
        :pswitch_43
        :pswitch_43
        :pswitch_42
    .end packed-switch
.end method

.method public a(Z)Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    new-instance v0, Lcom/swedbank/mobile/data/services/a$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/services/a$a;-><init>(Lcom/swedbank/mobile/data/services/a;Z)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/o;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.fromCallable \u2026rvices(country)\n    }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
