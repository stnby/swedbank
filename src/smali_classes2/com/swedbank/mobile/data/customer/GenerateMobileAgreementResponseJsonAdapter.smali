.class public final Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "GenerateMobileAgreementResponseJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 3
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "id"

    const-string v1, "message"

    const-string v2, "errorCode"

    .line 13
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"i\u2026, \"message\", \"errorCode\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 16
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "mobileAgreementId"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<String?>(S\u2026t(), \"mobileAgreementId\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "id"

    .line 63
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "message"

    .line 65
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "errorCode"

    .line 67
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 69
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 60
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p2, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;
    .locals 12
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    check-cast v0, Ljava/lang/String;

    .line 27
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v1, 0x0

    move-object v3, v0

    move-object v5, v3

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 28
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 29
    iget-object v6, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {p1, v6}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v6

    const/4 v7, 0x1

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 39
    :pswitch_0
    iget-object v4, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v5, v4

    const/4 v4, 0x1

    goto :goto_0

    .line 35
    :pswitch_1
    iget-object v2, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v3, v2

    const/4 v2, 0x1

    goto :goto_0

    .line 31
    :pswitch_2
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x1

    goto :goto_0

    .line 44
    :pswitch_3
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->j()V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->q()V

    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->f()V

    .line 50
    new-instance p1, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x7

    const/4 v11, 0x0

    move-object v6, p1

    invoke-direct/range {v6 .. v11}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    .line 51
    new-instance v6, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;

    if-eqz v1, :cond_1

    goto :goto_1

    .line 52
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v2, :cond_2

    goto :goto_2

    .line 53
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;->b()Ljava/lang/String;

    move-result-object v3

    :goto_2
    if-eqz v4, :cond_3

    goto :goto_3

    .line 54
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;->c()Ljava/lang/String;

    move-result-object v5

    .line 51
    :goto_3
    invoke-direct {v6, v0, v3, v5}, Lcom/swedbank/mobile/data/customer/GenerateMobileAgreementResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(GenerateMobileAgreementResponse)"

    return-object v0
.end method
