.class public final Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;
.super Lcom/swedbank/mobile/data/customer/CustomerItem;
.source "CustomerResponses.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/customer/CustomerItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PrivatePerson"
.end annotation


# instance fields
.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/customer/PrivateCustomerType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;",
            "Lcom/swedbank/mobile/data/customer/PrivateCustomerType;",
            ")V"
        }
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/data/customer/CustomerItem;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->d:Ljava/util/List;

    iput-object p4, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p3, 0x0

    .line 43
    check-cast p3, Ljava/util/List;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;ILjava/lang/Object;)Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c()Ljava/lang/String;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b()Ljava/util/List;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;)Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/customer/a;
    .locals 13
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 49
    new-instance v12, Lcom/swedbank/mobile/data/customer/a;

    .line 50
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c()Ljava/lang/String;

    move-result-object v3

    .line 53
    sget-object v5, Lcom/swedbank/mobile/data/customer/CustomerType;->PRIVATE:Lcom/swedbank/mobile/data/customer/CustomerType;

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    sget-object v1, Lcom/swedbank/mobile/data/customer/PrivateCustomerType;->OWN_EMPLOYEE:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x0

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    const/16 v10, 0xa1

    const/4 v11, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    move-object v0, v12

    move-object v4, p2

    .line 49
    invoke-direct/range {v0 .. v11}, Lcom/swedbank/mobile/data/customer/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/customer/CustomerType;Ljava/lang/String;ZZZILkotlin/e/b/g;)V

    return-object v12
.end method

.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->d:Ljava/util/List;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;)Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/customer/PrivateCustomerType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;",
            "Lcom/swedbank/mobile/data/customer/PrivateCustomerType;",
            ")",
            "Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;)V

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/data/customer/PrivateCustomerType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    iget-object p1, p1, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PrivatePerson(customerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", accounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->e:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
