.class public final Lcom/swedbank/mobile/data/customer/c;
.super Ljava/lang/Object;
.source "CustomerData.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/data/customer/CustomerType;)I
    .locals 1
    .param p0    # Lcom/swedbank/mobile/data/customer/CustomerType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "customerType"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/CustomerType;->getId()I

    move-result p0

    return p0
.end method

.method public static final a(I)Lcom/swedbank/mobile/data/customer/CustomerType;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    invoke-static {}, Lcom/swedbank/mobile/data/customer/CustomerType;->values()[Lcom/swedbank/mobile/data/customer/CustomerType;

    move-result-object v0

    .line 64
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 62
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/customer/CustomerType;->getId()I

    move-result v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    goto :goto_3

    .line 63
    :cond_3
    sget-object v4, Lcom/swedbank/mobile/data/customer/CustomerType;->PRIVATE:Lcom/swedbank/mobile/data/customer/CustomerType;

    :goto_3
    return-object v4
.end method
