.class public final Lcom/swedbank/mobile/data/customer/CustomerItem$a;
.super Ljava/lang/Object;
.source "CustomerResponses.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/customer/CustomerItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem$a;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/data/customer/CustomerItem$a;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 102
    :cond_0
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/customer/CustomerItem$a;->a(Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Z)Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nfragment customerData on GenericCustomer {\n  __typename\n  id\n  name\n  ...on PrivatePerson {\n    type\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    const-string v1, "\n    accounts(operation:INFORMATION) {\n      ...accountData\n    }\n"

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 110
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n  }\n  ...on SelfEmployedPerson {\n    legalCode\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_1

    const-string v1, "\n    accounts(operation:INFORMATION) {\n      ...accountData\n    }\n"

    goto :goto_1

    :cond_1
    const-string v1, ""

    .line 123
    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n  }\n  ...on Company {\n    legalCode\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_2

    const-string v1, "\n    accounts(operation:INFORMATION) {\n      ...accountData\n    }\n"

    goto :goto_2

    :cond_2
    const-string v1, ""

    .line 136
    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n  }\n}\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_3

    .line 149
    sget-object p1, Lcom/swedbank/mobile/data/account/AccountResponse;->a:Ljava/lang/String;

    goto :goto_3

    :cond_3
    const-string p1, ""

    .line 148
    :goto_3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0xa

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
