.class public abstract Lcom/swedbank/mobile/data/customer/CustomerItem;
.super Ljava/lang/Object;
.source "CustomerResponses.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;,
        Lcom/swedbank/mobile/data/customer/CustomerItem$SelfEmployedPerson;,
        Lcom/swedbank/mobile/data/customer/CustomerItem$Company;,
        Lcom/swedbank/mobile/data/customer/CustomerItem$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/customer/CustomerItem$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/swedbank/mobile/data/customer/CustomerItem$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/customer/CustomerItem$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/customer/CustomerItem;->a:Lcom/swedbank/mobile/data/customer/CustomerItem$a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/data/customer/CustomerItem;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/data/customer/CustomerItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/data/customer/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    if-nez p4, :cond_2

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    .line 34
    move-object p1, v0

    check-cast p1, Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 35
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/customer/CustomerItem;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/customer/a;

    move-result-object p0

    return-object p0

    .line 0
    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: toCustomerData"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/customer/a;
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end method
