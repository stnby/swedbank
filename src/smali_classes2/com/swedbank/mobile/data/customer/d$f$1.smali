.class public final Lcom/swedbank/mobile/data/customer/d$f$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/customer/d$f;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/customer/d$f;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/customer/d$f;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/d$f$1;->a:Lcom/swedbank/mobile/data/customer/d$f;

    iput-object p2, p0, Lcom/swedbank/mobile/data/customer/d$f$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 22
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v1, p0

    .line 80
    iget-object v0, v1, Lcom/swedbank/mobile/data/customer/d$f$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/customer/CustomerListResponse;

    .line 236
    iget-object v2, v1, Lcom/swedbank/mobile/data/customer/d$f$1;->a:Lcom/swedbank/mobile/data/customer/d$f;

    iget-object v2, v2, Lcom/swedbank/mobile/data/customer/d$f;->a:Lcom/swedbank/mobile/data/customer/d;

    .line 238
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v2

    const-string v3, "SqliteMagic.newTransaction()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v3

    .line 249
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v5, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v3, v4}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v3

    .line 250
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v5, "CUSTOMER_DATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    .line 251
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v3

    const-string v4, "SELECT\n      COLUMN CUST\u2026\n      FROM CUSTOMER_DATA"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    const-string v5, "CUSTOMER_DATA.SELECTED"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 253
    invoke-virtual {v4, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v6, "this.`is`(value)"

    invoke-static {v4, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v3

    .line 248
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v3

    .line 247
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/ad;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 255
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->b()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 256
    new-instance v6, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v4, v7}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 257
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    const/4 v9, 0x0

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 258
    check-cast v8, Lcom/swedbank/mobile/data/customer/CustomerItem;

    const/4 v10, 0x2

    .line 255
    invoke-static {v8, v3, v9, v10, v9}, Lcom/swedbank/mobile/data/customer/CustomerItem;->a(Lcom/swedbank/mobile/data/customer/CustomerItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/data/customer/a;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 259
    :cond_0
    check-cast v6, Ljava/util/List;

    .line 260
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 261
    sget-object v0, Lcom/siimkinks/sqlitemagic/an;->a:Lcom/siimkinks/sqlitemagic/an;

    .line 262
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cx$b;->a()Lcom/siimkinks/sqlitemagic/cx$b;

    move-result-object v0

    const-string v3, "SqliteMagic_CustomerData\u2026leteTableBuilder.create()"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/siimkinks/sqlitemagic/a/b;

    .line 261
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/b;->b()I

    goto/16 :goto_4

    .line 263
    :cond_1
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->a()Lcom/swedbank/mobile/data/customer/UserProfileResponse;

    move-result-object v4

    invoke-virtual {v4}, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->a()Lcom/swedbank/mobile/data/customer/CustomerItem;

    move-result-object v4

    .line 265
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->a()Lcom/swedbank/mobile/data/customer/UserProfileResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->b()Lcom/swedbank/mobile/data/customer/MobileAgreement;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/MobileAgreement;->a()Ljava/lang/String;

    move-result-object v9

    .line 263
    :cond_2
    invoke-virtual {v4, v3, v9}, Lcom/swedbank/mobile/data/customer/CustomerItem;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/customer/a;

    move-result-object v0

    .line 266
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v3

    .line 270
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v8, "CUSTOMER_DATA"

    invoke-static {v4, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v3, v4}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v3

    .line 271
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v8, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v4, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    move-object v8, v6

    check-cast v8, Ljava/lang/Iterable;

    .line 272
    new-instance v9, Ljava/util/ArrayList;

    invoke-static {v8, v7}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v9, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v9, Ljava/util/Collection;

    .line 273
    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 274
    check-cast v8, Lcom/swedbank/mobile/data/customer/a;

    .line 271
    invoke-virtual {v8}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v9, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 275
    :cond_3
    check-cast v9, Ljava/util/List;

    check-cast v9, Ljava/util/Collection;

    .line 276
    invoke-virtual {v4, v9}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v7, "this.notIn(values)"

    invoke-static {v4, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v3

    .line 269
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 278
    sget-object v3, Lcom/siimkinks/sqlitemagic/an;->a:Lcom/siimkinks/sqlitemagic/an;

    .line 297
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x17f

    const/16 v21, 0x0

    move-object v10, v0

    .line 298
    invoke-static/range {v10 .. v21}, Lcom/swedbank/mobile/data/customer/a;->a(Lcom/swedbank/mobile/data/customer/a;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/customer/CustomerType;Ljava/lang/String;ZZZILjava/lang/Object;)Lcom/swedbank/mobile/data/customer/a;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    check-cast v6, Ljava/lang/Iterable;

    .line 300
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/swedbank/mobile/data/customer/a;

    .line 301
    invoke-virtual {v7}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v7, v5

    if-eqz v7, :cond_4

    move-object v7, v3

    check-cast v7, Ljava/util/Collection;

    invoke-interface {v7, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 302
    :cond_5
    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    .line 297
    check-cast v3, Ljava/util/List;

    .line 303
    check-cast v3, Ljava/lang/Iterable;

    .line 304
    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/cx$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cx$a;

    move-result-object v3

    .line 284
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v4, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/cx$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object v3

    .line 283
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/a/a;->b()Z

    .line 306
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v3

    .line 312
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v6, "CUSTOMER_DATA"

    invoke-static {v4, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v3, v4}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v3

    .line 313
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    const-string v6, "CUSTOMER_DATA.SELECTED"

    invoke-static {v4, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 314
    invoke-virtual {v4, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v6, "this.`is`(value)"

    invoke-static {v4, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v3

    .line 311
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/cb$h;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object v3

    .line 310
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/ab;->a()J

    move-result-wide v3

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-lez v3, :cond_6

    const/4 v3, 0x1

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    :goto_3
    if-nez v3, :cond_7

    .line 317
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v3

    .line 322
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v6, "CUSTOMER_DATA"

    invoke-static {v4, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v3, v4}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v3

    .line 323
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v4

    .line 324
    invoke-virtual {v4}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v4}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v3

    const-string v4, "UPDATE\n          TABLE C\u2026ER_DATA.SELECTED to true)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    sget-object v4, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v5, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 326
    invoke-virtual {v4, v0}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v0

    const-string v4, "this.`is`(value)"

    invoke-static {v0, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 327
    invoke-virtual {v3, v0}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v0

    .line 321
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 331
    :cond_7
    :goto_4
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 333
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/d$f$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
