.class final Lcom/swedbank/mobile/data/customer/d$h;
.super Ljava/lang/Object;
.source "CustomerRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/customer/d;->c(Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/d$h;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .line 193
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v1

    .line 87
    sget-object v2, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v3, "CUSTOMER_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v1

    .line 88
    sget-object v2, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    .line 196
    invoke-virtual {v2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v1

    const-string v2, "UPDATE\n                 \u2026ER_DATA.SELECTED to true)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    sget-object v2, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v4, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    iget-object v4, p0, Lcom/swedbank/mobile/data/customer/d$h;->a:Ljava/lang/String;

    .line 197
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v4, "this.`is`(value)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    move-result v1

    if-lez v1, :cond_0

    .line 91
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v1

    .line 92
    sget-object v2, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v4, "CUSTOMER_DATA"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v1

    .line 93
    sget-object v2, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    .line 199
    invoke-virtual {v2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v1

    const-string v2, "UPDATE\n                T\u2026R_DATA.SELECTED to false)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget-object v2, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    const-string v4, "CUSTOMER_DATA.SELECTED"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 200
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget-object v3, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v4, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    iget-object v4, p0, Lcom/swedbank/mobile/data/customer/d$h;->a:Ljava/lang/String;

    .line 201
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->c(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.isNot(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.and(expr)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 204
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    .line 96
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "couldn\'t find customer with id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/swedbank/mobile/data/customer/d$h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    .line 206
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/d$h;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
