.class public final Lcom/swedbank/mobile/data/customer/d$g$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/customer/d$g;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/customer/d$g;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/customer/d$g;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/d$g$1;->a:Lcom/swedbank/mobile/data/customer/d$g;

    iput-object p2, p0, Lcom/swedbank/mobile/data/customer/d$g$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 23
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v1, p0

    .line 80
    iget-object v0, v1, Lcom/swedbank/mobile/data/customer/d$g$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/customer/CustomerListResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v2

    const-string v3, "SqliteMagic.newTransaction()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    :try_start_0
    iget-object v3, v1, Lcom/swedbank/mobile/data/customer/d$g$1;->a:Lcom/swedbank/mobile/data/customer/d$g;

    iget-object v3, v3, Lcom/swedbank/mobile/data/customer/d$g;->a:Lcom/swedbank/mobile/data/customer/d;

    .line 250
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v3

    const-string v4, "SqliteMagic.newTransaction()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254
    :try_start_1
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v4

    .line 261
    sget-object v5, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v6, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v4, v5}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v4

    .line 262
    sget-object v5, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v6, "CUSTOMER_DATA"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lcom/siimkinks/sqlitemagic/dl;

    .line 263
    invoke-virtual {v4, v5}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v4

    const-string v5, "SELECT\n      COLUMN CUST\u2026\n      FROM CUSTOMER_DATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    sget-object v5, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    const-string v6, "CUSTOMER_DATA.SELECTED"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 265
    invoke-virtual {v5, v7}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v5

    const-string v7, "this.`is`(value)"

    invoke-static {v5, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v4, v5}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v4

    .line 260
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v4

    .line 259
    invoke-interface {v4}, Lcom/siimkinks/sqlitemagic/ad;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 267
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->b()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 268
    new-instance v7, Ljava/util/ArrayList;

    const/16 v8, 0xa

    invoke-static {v5, v8}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 269
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const/4 v10, 0x0

    if-eqz v9, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 270
    check-cast v9, Lcom/swedbank/mobile/data/customer/CustomerItem;

    const/4 v11, 0x2

    .line 267
    invoke-static {v9, v4, v10, v11, v10}, Lcom/swedbank/mobile/data/customer/CustomerItem;->a(Lcom/swedbank/mobile/data/customer/CustomerItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/data/customer/a;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 271
    :cond_0
    check-cast v7, Ljava/util/List;

    .line 272
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 273
    sget-object v4, Lcom/siimkinks/sqlitemagic/an;->a:Lcom/siimkinks/sqlitemagic/an;

    .line 274
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cx$b;->a()Lcom/siimkinks/sqlitemagic/cx$b;

    move-result-object v4

    const-string v5, "SqliteMagic_CustomerData\u2026leteTableBuilder.create()"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/a/b;

    .line 273
    invoke-interface {v4}, Lcom/siimkinks/sqlitemagic/a/b;->b()I

    goto/16 :goto_4

    .line 275
    :cond_1
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->a()Lcom/swedbank/mobile/data/customer/UserProfileResponse;

    move-result-object v5

    invoke-virtual {v5}, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->a()Lcom/swedbank/mobile/data/customer/CustomerItem;

    move-result-object v5

    .line 277
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->a()Lcom/swedbank/mobile/data/customer/UserProfileResponse;

    move-result-object v9

    invoke-virtual {v9}, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->b()Lcom/swedbank/mobile/data/customer/MobileAgreement;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Lcom/swedbank/mobile/data/customer/MobileAgreement;->a()Ljava/lang/String;

    move-result-object v10

    .line 275
    :cond_2
    invoke-virtual {v5, v4, v10}, Lcom/swedbank/mobile/data/customer/CustomerItem;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/customer/a;

    move-result-object v4

    .line 278
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v5

    .line 282
    sget-object v9, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v10, "CUSTOMER_DATA"

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v9, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v5, v9}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v5

    .line 283
    sget-object v9, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v9, v9, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v10, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v9, Lcom/siimkinks/sqlitemagic/x;

    move-object v10, v7

    check-cast v10, Ljava/lang/Iterable;

    .line 284
    new-instance v11, Ljava/util/ArrayList;

    invoke-static {v10, v8}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v11, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v11, Ljava/util/Collection;

    .line 285
    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 286
    check-cast v10, Lcom/swedbank/mobile/data/customer/a;

    .line 283
    invoke-virtual {v10}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v11, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 287
    :cond_3
    check-cast v11, Ljava/util/List;

    check-cast v11, Ljava/util/Collection;

    .line 288
    invoke-virtual {v9, v11}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v8

    const-string v9, "this.notIn(values)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    invoke-virtual {v5, v8}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v5

    .line 281
    invoke-virtual {v5}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 290
    sget-object v5, Lcom/siimkinks/sqlitemagic/an;->a:Lcom/siimkinks/sqlitemagic/an;

    .line 309
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v8, v6

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    const/16 v21, 0x17f

    const/16 v22, 0x0

    move-object v11, v4

    .line 310
    invoke-static/range {v11 .. v22}, Lcom/swedbank/mobile/data/customer/a;->a(Lcom/swedbank/mobile/data/customer/a;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/customer/CustomerType;Ljava/lang/String;ZZZILjava/lang/Object;)Lcom/swedbank/mobile/data/customer/a;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    check-cast v7, Ljava/lang/Iterable;

    .line 312
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/swedbank/mobile/data/customer/a;

    .line 313
    invoke-virtual {v9}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    xor-int/2addr v9, v6

    if-eqz v9, :cond_4

    move-object v9, v5

    check-cast v9, Ljava/util/Collection;

    invoke-interface {v9, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 314
    :cond_5
    move-object v7, v5

    check-cast v7, Ljava/util/Collection;

    .line 309
    check-cast v5, Ljava/util/List;

    .line 315
    check-cast v5, Ljava/lang/Iterable;

    .line 316
    invoke-static {v5}, Lcom/siimkinks/sqlitemagic/cx$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cx$a;

    move-result-object v5

    .line 296
    sget-object v7, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v7, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {v5, v7}, Lcom/siimkinks/sqlitemagic/cx$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object v5

    .line 295
    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/a/a;->b()Z

    .line 318
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v5

    .line 324
    sget-object v7, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v8, "CUSTOMER_DATA"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v5, v7}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v5

    .line 325
    sget-object v7, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    const-string v8, "CUSTOMER_DATA.SELECTED"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 326
    invoke-virtual {v7, v8}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v7

    const-string v8, "this.`is`(value)"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 327
    invoke-virtual {v5, v7}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v5

    .line 323
    invoke-virtual {v5}, Lcom/siimkinks/sqlitemagic/cb$h;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object v5

    .line 322
    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/ab;->a()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-lez v5, :cond_6

    const/4 v5, 0x1

    goto :goto_3

    :cond_6
    const/4 v5, 0x0

    :goto_3
    if-nez v5, :cond_7

    .line 329
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v5

    .line 334
    sget-object v7, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v8, "CUSTOMER_DATA"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v5, v7}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v5

    .line 335
    sget-object v7, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/am;->j:Lcom/siimkinks/sqlitemagic/n;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v7, v6}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v6

    .line 336
    invoke-virtual {v6}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v6}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v7, v6}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v5

    const-string v6, "UPDATE\n          TABLE C\u2026ER_DATA.SELECTED to true)"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 337
    sget-object v6, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v6, v6, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v7, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v4}, Lcom/swedbank/mobile/data/customer/a;->c()Ljava/lang/String;

    move-result-object v4

    .line 338
    invoke-virtual {v6, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v6, "this.`is`(value)"

    invoke-static {v4, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    invoke-virtual {v5, v4}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v4

    .line 333
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 343
    :cond_7
    :goto_4
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    :try_start_2
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 348
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 349
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/data/customer/CustomerItem;

    .line 350
    iget-object v4, v1, Lcom/swedbank/mobile/data/customer/d$g$1;->a:Lcom/swedbank/mobile/data/customer/d$g;

    iget-object v4, v4, Lcom/swedbank/mobile/data/customer/d$g;->a:Lcom/swedbank/mobile/data/customer/d;

    invoke-static {v4}, Lcom/swedbank/mobile/data/customer/d;->a(Lcom/swedbank/mobile/data/customer/d;)Lcom/swedbank/mobile/data/account/c;

    move-result-object v4

    .line 351
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/customer/CustomerItem;->a()Ljava/lang/String;

    move-result-object v5

    .line 352
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/customer/CustomerItem;->b()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 350
    invoke-virtual {v4, v5, v3}, Lcom/swedbank/mobile/data/account/c;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_5

    :cond_8
    const-string v0, "Required value was null."

    .line 352
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Throwable;

    throw v3

    .line 356
    :cond_9
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 358
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 345
    :try_start_3
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    .line 358
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/customer/d$g$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
