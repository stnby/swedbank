.class public final Lcom/swedbank/mobile/data/customer/i;
.super Ljava/lang/Object;
.source "LocalCustomerRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/customer/k;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public e()Lcom/swedbank/mobile/business/util/l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 19
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/am;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CUSTOMER_DATA.NAME"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 20
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v2, "CUSTOMER_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 49
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026       FROM CUSTOMER_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/am;->i:Lcom/siimkinks/sqlitemagic/n;

    const-string v2, "CUSTOMER_DATA.LOGGED_IN"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 50
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->a()Ljava/lang/Object;

    move-result-object v0

    .line 24
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/swedbank/mobile/business/util/l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "CUSTOMER_DATA.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 29
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v2, "CUSTOMER_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 52
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026       FROM CUSTOMER_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/am;->i:Lcom/siimkinks/sqlitemagic/n;

    const-string v2, "CUSTOMER_DATA.LOGGED_IN"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 53
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->a()Ljava/lang/Object;

    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    return-object v0
.end method

.method public g()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 37
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/am;->h:Lcom/siimkinks/sqlitemagic/n;

    const-string v2, "CUSTOMER_DATA.SWEDBANK_EMPLOYEE"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v2, "CUSTOMER_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 55
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026       FROM CUSTOMER_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/am;->i:Lcom/siimkinks/sqlitemagic/n;

    const-string v2, "CUSTOMER_DATA.LOGGED_IN"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 56
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/swedbank/mobile/data/customer/i$a;->a:Lcom/swedbank/mobile/data/customer/i$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cg;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          COLUMN\u2026oObservable()\n          }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
