.class public final Lcom/swedbank/mobile/data/customer/UserProfileResponse;
.super Ljava/lang/Object;
.source "CustomerResponses.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/customer/CustomerItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/customer/MobileAgreement;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/customer/CustomerItem;Lcom/swedbank/mobile/data/customer/MobileAgreement;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/customer/CustomerItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/customer/MobileAgreement;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "customer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->a:Lcom/swedbank/mobile/data/customer/CustomerItem;

    iput-object p2, p0, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->b:Lcom/swedbank/mobile/data/customer/MobileAgreement;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/data/customer/CustomerItem;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->a:Lcom/swedbank/mobile/data/customer/CustomerItem;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/data/customer/MobileAgreement;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/UserProfileResponse;->b:Lcom/swedbank/mobile/data/customer/MobileAgreement;

    return-object v0
.end method
