.class public final Lcom/swedbank/mobile/data/customer/CustomerListResponse;
.super Ljava/lang/Object;
.source "CustomerResponses.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/customer/UserProfileResponse;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/customer/CustomerItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/customer/UserProfileResponse;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/customer/UserProfileResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "fetchUserProfile"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "fetchAllCustomers"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/customer/UserProfileResponse;",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/data/customer/CustomerItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "userProfile"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customers"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->a:Lcom/swedbank/mobile/data/customer/UserProfileResponse;

    iput-object p2, p0, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/data/customer/UserProfileResponse;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->a:Lcom/swedbank/mobile/data/customer/UserProfileResponse;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/customer/CustomerItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerListResponse;->b:Ljava/util/List;

    return-object v0
.end method
