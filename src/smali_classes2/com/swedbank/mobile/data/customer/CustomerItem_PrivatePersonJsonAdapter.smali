.class public final Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "CustomerItem_PrivatePersonJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableListOfAccountResponseAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;

.field private final privateCustomerTypeAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lcom/swedbank/mobile/data/customer/PrivateCustomerType;",
            ">;"
        }
    .end annotation
.end field

.field private final stringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 4
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "id"

    const-string v1, "name"

    const-string v2, "accounts"

    const-string v3, "type"

    .line 18
    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"i\u2026ame\", \"accounts\", \"type\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 21
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "customerId"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String>(St\u2026emptySet(), \"customerId\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 24
    const-class v0, Ljava/util/List;

    check-cast v0, Ljava/lang/reflect/Type;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/swedbank/mobile/data/account/AccountResponse;

    check-cast v2, Ljava/lang/reflect/Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/moshi/p;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "accounts"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<List<Accou\u2026s.emptySet(), \"accounts\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->nullableListOfAccountResponseAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 27
    const-class v0, Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<PrivateCus\u2026tions.emptySet(), \"type\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->privateCustomerTypeAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 16
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 68
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "id"

    .line 69
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "name"

    .line 71
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "accounts"

    .line 73
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->nullableListOfAccountResponseAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "type"

    .line 75
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->privateCustomerTypeAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->d()Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 66
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p2, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;
    .locals 18
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "reader"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 32
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 34
    move-object v4, v2

    check-cast v4, Ljava/util/List;

    .line 36
    check-cast v2, Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    .line 37
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v5, 0x0

    move-object v10, v2

    move-object v7, v3

    move-object v8, v7

    .line 38
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 39
    iget-object v2, v0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {v1, v2}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 46
    :pswitch_0
    iget-object v2, v0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->privateCustomerTypeAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    if-eqz v2, :cond_0

    move-object v10, v2

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'type\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 43
    :pswitch_1
    iget-object v2, v0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->nullableListOfAccountResponseAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    const/4 v3, 0x1

    move-object v4, v2

    const/4 v5, 0x1

    goto :goto_0

    .line 41
    :pswitch_2
    iget-object v2, v0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v8, v2

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'name\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 40
    :pswitch_3
    iget-object v2, v0, Lcom/swedbank/mobile/data/customer/CustomerItem_PrivatePersonJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object v7, v2

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'customerId\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 49
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->j()V

    .line 50
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->q()V

    goto/16 :goto_0

    .line 54
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->f()V

    .line 55
    new-instance v2, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    if-eqz v7, :cond_7

    if-eqz v8, :cond_6

    const/4 v9, 0x0

    if-eqz v10, :cond_5

    const/4 v11, 0x4

    const/4 v12, 0x0

    move-object v6, v2

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;ILkotlin/e/b/g;)V

    const/4 v13, 0x0

    if-eqz v5, :cond_4

    :goto_1
    move-object v14, v4

    goto :goto_2

    .line 60
    :cond_4
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->b()Ljava/util/List;

    move-result-object v4

    goto :goto_1

    :goto_2
    const/4 v15, 0x0

    const/16 v16, 0xb

    const/16 v17, 0x0

    move-object v11, v2

    .line 59
    invoke-static/range {v11 .. v17}, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;->a(Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/swedbank/mobile/data/customer/PrivateCustomerType;ILjava/lang/Object;)Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    move-result-object v1

    return-object v1

    .line 58
    :cond_5
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Required property \'type\' missing at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 57
    :cond_6
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Required property \'name\' missing at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 56
    :cond_7
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Required property \'customerId\' missing at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(CustomerItem.PrivatePerson)"

    return-object v0
.end method
