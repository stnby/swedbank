.class public final Lcom/swedbank/mobile/data/k;
.super Ljava/lang/Object;
.source "CoreDataModule_ProvideAppDataClearStreamsFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lio/reactivex/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/i;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/data/k;->a:Lcom/swedbank/mobile/data/i;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/data/k;
    .locals 1

    .line 22
    new-instance v0, Lcom/swedbank/mobile/data/k;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/k;-><init>(Lcom/swedbank/mobile/data/i;)V

    return-object v0
.end method

.method public static b(Lcom/swedbank/mobile/data/i;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/i;",
            ")",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/i;->c()Ljava/util/Set;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/data/k;->a:Lcom/swedbank/mobile/data/i;

    invoke-static {v0}, Lcom/swedbank/mobile/data/k;->b(Lcom/swedbank/mobile/data/i;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/k;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
