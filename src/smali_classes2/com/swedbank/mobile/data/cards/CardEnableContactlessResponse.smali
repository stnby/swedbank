.class public final Lcom/swedbank/mobile/data/cards/CardEnableContactlessResponse;
.super Ljava/lang/Object;
.source "ContactlessChangeResponse.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/cards/k;


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "enableContactless"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/network/GeneralResponseError;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/CardEnableContactlessResponse;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/CardEnableContactlessResponse;->b:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 16
    check-cast p2, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/data/cards/CardEnableContactlessResponse;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardEnableContactlessResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardEnableContactlessResponse;->b:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-object v0
.end method
