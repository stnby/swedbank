.class public final Lcom/swedbank/mobile/data/cards/m;
.super Ljava/lang/Object;
.source "LocalCardsRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/u;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/siimkinks/sqlitemagic/ab;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ab<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 29
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/x;->e()Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID.isNotNull"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, ""

    .line 191
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->c(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.isNot(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.and(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object v0

    const-string v1, "(SELECT\n      FROM CARD_\u2026_NOT \"\")))\n      .count()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/cards/m;->a:Lcom/siimkinks/sqlitemagic/ab;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Z)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 117
    sget-object p2, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->DIGITIZATION_IN_PROGRESS:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->NOT_APPLICABLE:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    .line 118
    :goto_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 119
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 120
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->r:Lcom/siimkinks/sqlitemagic/az;

    invoke-static {v1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p2

    .line 183
    invoke-virtual {p2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object p2

    const-string v0, "UPDATE\n        TABLE CAR\u2026_PROGRESSION to newState)"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object v0, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v1, "CARD_DATA.CARD_ID"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/siimkinks/sqlitemagic/x;

    .line 184
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v0, "this.`in`(values)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dx$d;->b()Lio/reactivex/w;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    const-string p2, "(UPDATE\n        TABLE CA\u2026\n        .ignoreElement()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object v0

    .line 36
    sget-object v1, Lcom/swedbank/mobile/data/cards/m$b;->a:Lcom/swedbank/mobile/data/cards/m$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT FROM CARD_DATA)\n\u2026tNull(CardData::toCard) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 102
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 103
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 104
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, ""

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    .line 182
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/siimkinks/sqlitemagic/dx$b;->b(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/dx$a;->b()Lio/reactivex/w;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object v0

    .line 107
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "(UPDATE\n          TABLE \u2026scribeOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()J
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/m;->a:Lcom/siimkinks/sqlitemagic/ab;

    .line 137
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ab;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public c(Ljava/lang/String;Z)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 127
    sget-object p2, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->PROVISIONING_FAILED:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->NOT_APPLICABLE:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    .line 128
    :goto_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 129
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 130
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->r:Lcom/siimkinks/sqlitemagic/az;

    invoke-static {v1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p2

    .line 186
    invoke-virtual {p2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object p2

    const-string v0, "UPDATE\n        TABLE CAR\u2026_PROGRESSION to newState)"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v1, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v0, "this.`is`(value)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object p1

    .line 132
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dx$d;->b()Lio/reactivex/w;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    const-string p2, "(UPDATE\n        TABLE CA\u2026\n        .ignoreElement()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public d()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/m;->a:Lcom/siimkinks/sqlitemagic/ab;

    .line 140
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ab;->b()Lcom/siimkinks/sqlitemagic/al;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/al;->b()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "digitizedCardCount\n     \u2026serve()\n      .runQuery()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 40
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 41
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "CARD_DATA.CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 151
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 43
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cg;->b()Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM C\u2026e()\n          .runQuery()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    sget-object v0, Lcom/swedbank/mobile/data/cards/m$a;->a:Lcom/swedbank/mobile/data/cards/m$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "map {\n        checkNotNu\u2026tType}\"\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public f(Ljava/lang/String;)Lio/reactivex/w;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "CARD_DATA.CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 158
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object p1

    .line 52
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ab;->b()Lcom/siimkinks/sqlitemagic/al;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM C\u2026nt()\n          .observe()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/al;->a()Lio/reactivex/o;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM C\u2026          .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public g(Ljava/lang/String;)Lio/reactivex/j;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "CARD_DATA.CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 68
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 167
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026\n          FROM CARD_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/x;->e()Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID.isNotNull"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget-object v2, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, ""

    .line 169
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->c(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.isNot(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.and(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.and(expr)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 73
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cg;->c()Lio/reactivex/j;

    move-result-object p1

    const-string v0, "(SELECT\n          COLUMN\u2026          .runQueryOnce()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public h(Ljava/lang/String;)Lio/reactivex/o;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 78
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 79
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 173
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026\n          FROM CARD_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "CARD_DATA.CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 174
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/x;->e()Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID.isNotNull"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    sget-object v2, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, ""

    .line 175
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->c(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.isNot(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.and(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.and(expr)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 84
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 85
    sget-object v0, Lcom/swedbank/mobile/data/cards/m$d;->a:Lcom/swedbank/mobile/data/cards/m$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/cg;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          COLUMN\u2026oObservable()\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public i(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 94
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 95
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, ""

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    .line 179
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/siimkinks/sqlitemagic/dx$b;->b(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v0

    const-string v1, "UPDATE\n          TABLE C\u2026.DIGITIZED_CARD_ID to \"\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CARD_DATA.DIGITIZED_CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dx$d;->b()Lio/reactivex/w;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    .line 99
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/b;->b(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "(UPDATE\n          TABLE \u2026scribeOn(Schedulers.io())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public j(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 145
    sget-object v1, Lcom/siimkinks/sqlitemagic/r;->a:Lcom/siimkinks/sqlitemagic/r;

    const-string v2, "CARD_LIMIT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 146
    sget-object v1, Lcom/siimkinks/sqlitemagic/r;->a:Lcom/siimkinks/sqlitemagic/r;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/r;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "CARD_LIMIT_DATA.CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 148
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 149
    sget-object v0, Lcom/swedbank/mobile/data/cards/m$c;->a:Lcom/swedbank/mobile/data/cards/m$c;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/cards/n;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/cards/n;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM C\u2026LimitData>::toCardLimits)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
