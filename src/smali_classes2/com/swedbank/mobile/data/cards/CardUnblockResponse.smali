.class public final Lcom/swedbank/mobile/data/cards/CardUnblockResponse;
.super Ljava/lang/Object;
.source "StatusChangeResult.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "unblock"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/network/GeneralResponseError;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/CardUnblockResponse;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/CardUnblockResponse;->b:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardUnblockResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardUnblockResponse;->b:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-object v0
.end method
