.class public final Lcom/swedbank/mobile/data/cards/d;
.super Ljava/lang/Object;
.source "CardData.kt"


# instance fields
.field private final a:Ljava/lang/Long;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Z

.field private final d:Ljava/math/BigDecimal;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:I

.field private final f:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/business/cards/CardLimitService;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V
    .locals 1
    .param p1    # Ljava/lang/Long;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/math/BigDecimal;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/cards/CardLimitService;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/cards/CardLimitValidity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardLimit"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "limitCurrency"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "service"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "validity"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/d;->a:Ljava/lang/Long;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/d;->b:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/swedbank/mobile/data/cards/d;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/data/cards/d;->d:Ljava/math/BigDecimal;

    iput p5, p0, Lcom/swedbank/mobile/data/cards/d;->e:I

    iput-object p6, p0, Lcom/swedbank/mobile/data/cards/d;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/swedbank/mobile/data/cards/d;->g:Lcom/swedbank/mobile/business/cards/CardLimitService;

    iput-object p8, p0, Lcom/swedbank/mobile/data/cards/d;->h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Long;Ljava/lang/String;ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;ILkotlin/e/b/g;)V
    .locals 10

    and-int/lit8 v0, p9, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 143
    check-cast v0, Ljava/lang/Long;

    move-object v2, v0

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    move-object v1, p0

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/swedbank/mobile/data/cards/d;-><init>(Ljava/lang/Long;Ljava/lang/String;ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/cards/f;
    .locals 8
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 152
    new-instance v7, Lcom/swedbank/mobile/business/cards/f;

    .line 153
    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/d;->c:Z

    .line 154
    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/d;->d:Ljava/math/BigDecimal;

    .line 155
    iget v3, p0, Lcom/swedbank/mobile/data/cards/d;->e:I

    .line 156
    iget-object v4, p0, Lcom/swedbank/mobile/data/cards/d;->f:Ljava/lang/String;

    .line 157
    iget-object v5, p0, Lcom/swedbank/mobile/data/cards/d;->g:Lcom/swedbank/mobile/business/cards/CardLimitService;

    .line 158
    iget-object v6, p0, Lcom/swedbank/mobile/data/cards/d;->h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-object v0, v7

    .line 152
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/cards/f;-><init>(ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V

    return-object v7
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 145
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/d;->c:Z

    return v0
.end method

.method public final d()Ljava/math/BigDecimal;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/d;->d:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .line 147
    iget v0, p0, Lcom/swedbank/mobile/data/cards/d;->e:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lcom/swedbank/mobile/data/cards/d;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/data/cards/d;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->a:Ljava/lang/Long;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/d;->a:Ljava/lang/Long;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/d;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/d;->c:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/cards/d;->c:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->d:Ljava/math/BigDecimal;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/d;->d:Ljava/math/BigDecimal;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/swedbank/mobile/data/cards/d;->e:I

    iget v3, p1, Lcom/swedbank/mobile/data/cards/d;->e:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/d;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->g:Lcom/swedbank/mobile/business/cards/CardLimitService;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/d;->g:Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    iget-object p1, p1, Lcom/swedbank/mobile/data/cards/d;->h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/swedbank/mobile/business/cards/CardLimitService;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/d;->g:Lcom/swedbank/mobile/business/cards/CardLimitService;

    return-object v0
.end method

.method public final h()Lcom/swedbank/mobile/business/cards/CardLimitValidity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/d;->h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/d;->a:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/d;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/data/cards/d;->c:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/d;->d:Ljava/math/BigDecimal;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/swedbank/mobile/data/cards/d;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/d;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/d;->g:Lcom/swedbank/mobile/business/cards/CardLimitService;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/d;->h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardLimitData(localId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/d;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", cardLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->d:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", limitMaximum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/data/cards/d;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", limitCurrency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", service="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->g:Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", validity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/d;->h:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
