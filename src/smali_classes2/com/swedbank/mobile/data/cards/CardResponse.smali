.class public final Lcom/swedbank/mobile/data/cards/CardResponse;
.super Ljava/lang/Object;
.source "CardsResponse.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "fetchCard"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:J

.field private final e:Lcom/swedbank/mobile/data/cards/CardHolder;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/data/cards/CardAccount;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/business/cards/CardClass;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/data/cards/CardType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final k:Lcom/swedbank/mobile/business/cards/CardState;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final l:Z

.field private final m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final n:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/cards/CardServiceResponse;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/swedbank/mobile/data/cards/CardHolder;Lcom/swedbank/mobile/data/cards/CardAccount;Lcom/swedbank/mobile/business/cards/CardClass;Lcom/swedbank/mobile/data/cards/CardType;Lcom/swedbank/mobile/data/cards/ContactlessStatus;Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;Lcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/util/List;)V
    .locals 13
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/data/cards/CardHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/data/cards/CardAccount;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/cards/CardClass;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/data/cards/CardType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/data/cards/ContactlessStatus;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/business/cards/CardState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p14    # Lcom/swedbank/mobile/business/cards/DigitizationProgression;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p15    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p16    # Lcom/swedbank/mobile/data/cards/DeliveryMethod;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p17    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/swedbank/mobile/data/cards/CardHolder;",
            "Lcom/swedbank/mobile/data/cards/CardAccount;",
            "Lcom/swedbank/mobile/business/cards/CardClass;",
            "Lcom/swedbank/mobile/data/cards/CardType;",
            "Lcom/swedbank/mobile/data/cards/ContactlessStatus;",
            "Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;",
            "Lcom/swedbank/mobile/business/cards/CardState;",
            "Z",
            "Lcom/swedbank/mobile/business/cards/DigitizationProgression;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/data/cards/DeliveryMethod;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/cards/CardServiceResponse;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p12

    move-object/from16 v10, p14

    move-object/from16 v11, p17

    const-string v12, "cardId"

    invoke-static {p1, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "number"

    invoke-static {p2, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "bin"

    invoke-static {v3, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "holder"

    invoke-static {v4, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "account"

    invoke-static {v5, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "parentType"

    invoke-static {v6, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "cardType"

    invoke-static {v7, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "contactlessStatus"

    invoke-static {v8, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "state"

    invoke-static {v9, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "digitizationProgression"

    invoke-static {v10, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "services"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    iput-object v2, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    iput-object v3, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    move-wide/from16 v1, p4

    iput-wide v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    iput-object v4, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    iput-object v5, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    iput-object v6, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    iput-object v7, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    iput-object v8, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    move-object/from16 v1, p11

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    iput-object v9, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    move/from16 v1, p13

    iput-boolean v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    iput-object v10, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    move-object/from16 v1, p16

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    iput-object v11, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->p:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/swedbank/mobile/data/cards/CardHolder;Lcom/swedbank/mobile/data/cards/CardAccount;Lcom/swedbank/mobile/business/cards/CardClass;Lcom/swedbank/mobile/data/cards/CardType;Lcom/swedbank/mobile/data/cards/ContactlessStatus;Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;Lcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/util/List;ILkotlin/e/b/g;)V
    .locals 20

    move/from16 v0, p18

    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_0

    .line 34
    sget-object v1, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->NOT_ALLOWED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    move-object v12, v1

    goto :goto_0

    :cond_0
    move-object/from16 v12, p10

    :goto_0
    and-int/lit16 v1, v0, 0x200

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 35
    move-object v1, v2

    check-cast v1, Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    move-object v13, v1

    goto :goto_1

    :cond_1
    move-object/from16 v13, p11

    :goto_1
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_2

    .line 36
    sget-object v1, Lcom/swedbank/mobile/business/cards/CardState;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;

    move-object v14, v1

    goto :goto_2

    :cond_2
    move-object/from16 v14, p12

    :goto_2
    and-int/lit16 v1, v0, 0x800

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    const/4 v15, 0x0

    goto :goto_3

    :cond_3
    move/from16 v15, p13

    :goto_3
    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_4

    .line 38
    sget-object v1, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->NOT_APPLICABLE:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-object/from16 v16, v1

    goto :goto_4

    :cond_4
    move-object/from16 v16, p14

    :goto_4
    and-int/lit16 v1, v0, 0x2000

    if-eqz v1, :cond_5

    .line 39
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object/from16 v17, v1

    goto :goto_5

    :cond_5
    move-object/from16 v17, p15

    :goto_5
    and-int/lit16 v1, v0, 0x4000

    if-eqz v1, :cond_6

    .line 40
    move-object v1, v2

    check-cast v1, Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-object/from16 v18, v1

    goto :goto_6

    :cond_6
    move-object/from16 v18, p16

    :goto_6
    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_7

    .line 41
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v0

    move-object/from16 v19, v0

    goto :goto_7

    :cond_7
    move-object/from16 v19, p17

    :goto_7
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v2 .. v19}, Lcom/swedbank/mobile/data/cards/CardResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/swedbank/mobile/data/cards/CardHolder;Lcom/swedbank/mobile/data/cards/CardAccount;Lcom/swedbank/mobile/business/cards/CardClass;Lcom/swedbank/mobile/data/cards/CardType;Lcom/swedbank/mobile/data/cards/ContactlessStatus;Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;Lcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/data/cards/CardResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/swedbank/mobile/data/cards/CardHolder;Lcom/swedbank/mobile/data/cards/CardAccount;Lcom/swedbank/mobile/business/cards/CardClass;Lcom/swedbank/mobile/data/cards/CardType;Lcom/swedbank/mobile/data/cards/ContactlessStatus;Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;Lcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/util/List;ILjava/lang/Object;)Lcom/swedbank/mobile/data/cards/CardResponse;
    .locals 19
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p18

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-wide v5, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    goto :goto_3

    :cond_3
    move-wide/from16 v5, p4

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    goto :goto_6

    :cond_6
    move-object/from16 v9, p8

    :goto_6
    and-int/lit16 v10, v1, 0x80

    if-eqz v10, :cond_7

    iget-object v10, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    goto :goto_7

    :cond_7
    move-object/from16 v10, p9

    :goto_7
    and-int/lit16 v11, v1, 0x100

    if-eqz v11, :cond_8

    iget-object v11, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    goto :goto_8

    :cond_8
    move-object/from16 v11, p10

    :goto_8
    and-int/lit16 v12, v1, 0x200

    if-eqz v12, :cond_9

    iget-object v12, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    goto :goto_9

    :cond_9
    move-object/from16 v12, p11

    :goto_9
    and-int/lit16 v13, v1, 0x400

    if-eqz v13, :cond_a

    iget-object v13, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    goto :goto_a

    :cond_a
    move-object/from16 v13, p12

    :goto_a
    and-int/lit16 v14, v1, 0x800

    if-eqz v14, :cond_b

    iget-boolean v14, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    goto :goto_b

    :cond_b
    move/from16 v14, p13

    :goto_b
    and-int/lit16 v15, v1, 0x1000

    if-eqz v15, :cond_c

    iget-object v15, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    goto :goto_c

    :cond_c
    move-object/from16 v15, p14

    :goto_c
    move-object/from16 v16, v15

    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-object v15, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p15

    :goto_d
    move-object/from16 v17, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p16

    :goto_e
    const v18, 0x8000

    and-int v1, v1, v18

    if-eqz v1, :cond_f

    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->p:Ljava/util/List;

    goto :goto_f

    :cond_f
    move-object/from16 v1, p17

    :goto_f
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-wide/from16 p4, v5

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move/from16 p13, v14

    move-object/from16 p14, v16

    move-object/from16 p15, v17

    move-object/from16 p16, v15

    move-object/from16 p17, v1

    invoke-virtual/range {p0 .. p17}, Lcom/swedbank/mobile/data/cards/CardResponse;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/swedbank/mobile/data/cards/CardHolder;Lcom/swedbank/mobile/data/cards/CardAccount;Lcom/swedbank/mobile/business/cards/CardClass;Lcom/swedbank/mobile/data/cards/CardType;Lcom/swedbank/mobile/data/cards/ContactlessStatus;Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;Lcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/util/List;)Lcom/swedbank/mobile/data/cards/CardResponse;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/swedbank/mobile/data/cards/CardHolder;Lcom/swedbank/mobile/data/cards/CardAccount;Lcom/swedbank/mobile/business/cards/CardClass;Lcom/swedbank/mobile/data/cards/CardType;Lcom/swedbank/mobile/data/cards/ContactlessStatus;Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;Lcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/util/List;)Lcom/swedbank/mobile/data/cards/CardResponse;
    .locals 20
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/data/cards/CardHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/data/cards/CardAccount;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/cards/CardClass;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/data/cards/CardType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/data/cards/ContactlessStatus;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/business/cards/CardState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p14    # Lcom/swedbank/mobile/business/cards/DigitizationProgression;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p15    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p16    # Lcom/swedbank/mobile/data/cards/DeliveryMethod;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p17    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/swedbank/mobile/data/cards/CardHolder;",
            "Lcom/swedbank/mobile/data/cards/CardAccount;",
            "Lcom/swedbank/mobile/business/cards/CardClass;",
            "Lcom/swedbank/mobile/data/cards/CardType;",
            "Lcom/swedbank/mobile/data/cards/ContactlessStatus;",
            "Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;",
            "Lcom/swedbank/mobile/business/cards/CardState;",
            "Z",
            "Lcom/swedbank/mobile/business/cards/DigitizationProgression;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/data/cards/DeliveryMethod;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/cards/CardServiceResponse;",
            ">;)",
            "Lcom/swedbank/mobile/data/cards/CardResponse;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    const-string v0, "cardId"

    move-object/from16 v18, v1

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "number"

    move-object/from16 v1, p2

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bin"

    move-object/from16 v1, p3

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    move-object/from16 v1, p6

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "account"

    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentType"

    move-object/from16 v1, p8

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardType"

    move-object/from16 v1, p9

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactlessStatus"

    move-object/from16 v1, p10

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    move-object/from16 v1, p12

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "digitizationProgression"

    move-object/from16 v1, p14

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "services"

    move-object/from16 v1, p17

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v19, Lcom/swedbank/mobile/data/cards/CardResponse;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct/range {v0 .. v17}, Lcom/swedbank/mobile/data/cards/CardResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/swedbank/mobile/data/cards/CardHolder;Lcom/swedbank/mobile/data/cards/CardAccount;Lcom/swedbank/mobile/business/cards/CardClass;Lcom/swedbank/mobile/data/cards/CardType;Lcom/swedbank/mobile/data/cards/ContactlessStatus;Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;Lcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/util/List;)V

    return-object v19
.end method

.method public final a()Lcom/swedbank/mobile/data/cards/a;
    .locals 28
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    .line 44
    iget-object v3, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    .line 45
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/cards/CardHolder;->a()Ljava/lang/String;

    move-result-object v4

    .line 46
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/cards/CardAccount;->a()Ljava/lang/String;

    move-result-object v5

    .line 47
    iget-object v6, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    .line 48
    iget-object v7, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    .line 49
    iget-wide v8, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    .line 50
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/cards/CardHolder;->b()Ljava/lang/String;

    move-result-object v10

    .line 51
    iget-object v11, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    .line 52
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/cards/CardType;->b()Ljava/lang/String;

    move-result-object v12

    .line 53
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/cards/CardType;->a()Ljava/lang/String;

    move-result-object v13

    .line 54
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/cards/CardType;->c()Z

    move-result v14

    .line 55
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    sget-object v2, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->NOT_ALLOWED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    const/4 v15, 0x0

    const/16 v16, 0x1

    if-eq v1, v2, :cond_0

    const/16 v22, 0x1

    goto :goto_0

    :cond_0
    const/16 v22, 0x0

    .line 56
    :goto_0
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    sget-object v2, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->ENABLED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    if-ne v1, v2, :cond_1

    const/16 v25, 0x1

    goto :goto_1

    :cond_1
    const/16 v25, 0x0

    .line 57
    :goto_1
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    if-eqz v1, :cond_3

    sget-object v2, Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;->ACTIVE:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    if-ne v1, v2, :cond_2

    const/4 v15, 0x1

    :cond_2
    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    move-object/from16 v26, v1

    .line 58
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    move-object/from16 v17, v1

    .line 59
    iget-boolean v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    move/from16 v18, v1

    .line 60
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-object/from16 v19, v1

    .line 61
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    move-object/from16 v20, v1

    .line 62
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-object/from16 v21, v1

    const/16 v23, 0x1

    const/16 v24, 0x0

    .line 43
    new-instance v27, Lcom/swedbank/mobile/data/cards/a;

    move-object/from16 v1, v27

    const/4 v2, 0x0

    move/from16 v15, v22

    move/from16 v16, v25

    move-object/from16 v22, v26

    invoke-direct/range {v1 .. v24}, Lcom/swedbank/mobile/data/cards/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/lang/Boolean;ILkotlin/e/b/g;)V

    return-object v27
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .line 29
    iget-wide v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lcom/swedbank/mobile/data/cards/CardResponse;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/data/cards/CardResponse;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-wide v3, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    iget-wide v5, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->p:Ljava/util/List;

    iget-object p1, p1, Lcom/swedbank/mobile/data/cards/CardResponse;->p:Ljava/util/List;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public final f()Lcom/swedbank/mobile/data/cards/CardHolder;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    return-object v0
.end method

.method public final g()Lcom/swedbank/mobile/data/cards/CardAccount;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    return-object v0
.end method

.method public final h()Lcom/swedbank/mobile/business/cards/CardClass;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_b
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_c
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_d
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->p:Ljava/util/List;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_e
    add-int/2addr v0, v1

    return v0
.end method

.method public final i()Lcom/swedbank/mobile/data/cards/CardType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    return-object v0
.end method

.method public final j()Lcom/swedbank/mobile/data/cards/ContactlessStatus;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    return-object v0
.end method

.method public final k()Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    return-object v0
.end method

.method public final l()Lcom/swedbank/mobile/business/cards/CardState;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    return v0
.end method

.method public final n()Lcom/swedbank/mobile/business/cards/DigitizationProgression;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/swedbank/mobile/data/cards/DeliveryMethod;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/cards/CardServiceResponse;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->p:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardResponse(cardId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", bin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", validUntil="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", holder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->e:Lcom/swedbank/mobile/data/cards/CardHolder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->f:Lcom/swedbank/mobile/data/cards/CardAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", parentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->g:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->h:Lcom/swedbank/mobile/data/cards/CardType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contactlessStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->i:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", internetShoppingStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->j:Lcom/swedbank/mobile/data/cards/InternetShoppingStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->k:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", digitizable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", digitizationProgression="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->m:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", digitizedCardId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", deliveryMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->o:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", services="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardResponse;->p:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
