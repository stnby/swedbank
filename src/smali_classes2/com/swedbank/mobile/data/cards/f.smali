.class public final Lcom/swedbank/mobile/data/cards/f;
.super Ljava/lang/Object;
.source "CardsRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/m;
.implements Lcom/swedbank/mobile/business/cards/u;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/cards/i;

.field private final b:Lcom/swedbank/mobile/business/f/a;

.field private final c:Lcom/swedbank/mobile/business/cards/u;

.field private final d:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/cards/i;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/c/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/cards/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localCardsRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/f;->b:Lcom/swedbank/mobile/business/f/a;

    iput-object p3, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    iput-object p4, p0, Lcom/swedbank/mobile/data/cards/f;->d:Lcom/swedbank/mobile/business/c/a;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/CardClass;)Lcom/swedbank/mobile/business/e;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/cards/CardClass;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    new-instance v0, Lcom/swedbank/mobile/business/e;

    .line 252
    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/f;->d:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/business/c/c;->a:Lcom/swedbank/mobile/business/c/c;

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

    if-ne p1, v1, :cond_0

    const-string p1, "private/d2d/cards/credit"

    goto :goto_0

    :cond_0
    const-string p1, "private/d2d/cards/bankCards"

    :goto_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 251
    invoke-direct {v0, p1, v2, v1, v2}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;)Lcom/swedbank/mobile/business/e;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/CardClass;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardClass"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->f:[I

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 289
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No card limit change ibank path for card class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 288
    :pswitch_1
    new-instance p1, Lkotlin/j;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "An operation is not implemented: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Credit card limit change not implemented yet"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 283
    :pswitch_2
    new-instance p2, Lcom/swedbank/mobile/business/e;

    const-string v0, "private/d2d/mobilejump"

    const/4 v1, 0x2

    .line 285
    new-array v1, v1, [Lkotlin/k;

    const/4 v2, 0x0

    const-string v3, "redirectToDebitCardLimitsPage"

    const-string v4, "true"

    .line 286
    invoke-static {v3, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "cardId"

    .line 287
    invoke-static {v3, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    aput-object p1, v1, v2

    .line 285
    invoke-static {v1}, Lcom/swedbank/mobile/business/util/b;->a([Lkotlin/k;)Ljava/util/Map;

    move-result-object p1

    .line 283
    invoke-direct {p2, v0, p1}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object p2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/util/List;Z)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1, p2}, Lcom/swedbank/mobile/business/cards/u;->a(Ljava/util/List;Z)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    .line 40
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/cards/j;->a(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 41
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 296
    new-instance v0, Lcom/swedbank/mobile/data/cards/f$f;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/cards/f$f;-><init>()V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    .line 76
    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/f;->b:Lcom/swedbank/mobile/business/f/a;

    const-string v2, "feature_card_limits"

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v1

    .line 77
    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/f;->b:Lcom/swedbank/mobile/business/f/a;

    const-string v3, "feature_card_internet_shopping"

    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v2

    .line 73
    invoke-static {v0, p1, p2, v1, v2}, Lcom/swedbank/mobile/data/cards/j;->a(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;Ljava/lang/String;ZZ)Lio/reactivex/w;

    move-result-object p2

    .line 78
    invoke-static {p2}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p2

    .line 305
    new-instance v0, Lcom/swedbank/mobile/data/cards/f$i;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/cards/f$i;-><init>(Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p2, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentAppInstanceId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mobileAgreementId"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    .line 220
    invoke-static {v0, p1, p2, p3}, Lcom/swedbank/mobile/data/cards/j;->a(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 224
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 225
    sget-object p2, Lcom/swedbank/mobile/data/cards/f$e;->a:Lcom/swedbank/mobile/data/cards/f$e;

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "cardsService\n      .gene\u2026lier())\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Z)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    if-eqz p2, :cond_0

    .line 163
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/cards/j;->d(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    goto :goto_0

    .line 164
    :cond_0
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/cards/j;->e(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 167
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/data/cards/f$b;

    invoke-direct {v1, p2, p1}, Lcom/swedbank/mobile/data/cards/f$b;-><init>(ZLjava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "cardsService\n      .run \u2026kError)\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Lcom/swedbank/mobile/business/cards/CardClass;)Lcom/swedbank/mobile/business/e;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/cards/CardClass;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    new-instance v0, Lcom/swedbank/mobile/business/e;

    .line 260
    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/f;->d:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/data/cards/g;->e:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 276
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No order new card path for country "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 271
    :pswitch_1
    sget-object v1, Lcom/swedbank/mobile/data/cards/g;->d:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 274
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No card ordering ibank path for card class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_3
    const-string p1, "private/d2d/cards/credit_card"

    goto :goto_0

    :pswitch_4
    const-string p1, "private/d2d/cards/debit_cards_main"

    goto :goto_0

    .line 266
    :pswitch_5
    sget-object v1, Lcom/swedbank/mobile/data/cards/g;->c:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    .line 269
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No card ordering ibank path for card class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_7
    const-string p1, "private/d2d/cards/credit_card"

    goto :goto_0

    :pswitch_8
    const-string p1, "private/d2d/cards/debit_card"

    goto :goto_0

    .line 261
    :pswitch_9
    sget-object v1, Lcom/swedbank/mobile/data/cards/g;->b:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3

    .line 264
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_a
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No card ordering ibank path for card class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_b
    const-string p1, "private/d2d/cards/credit"

    goto :goto_0

    :pswitch_c
    const-string p1, "private/d2d/cards/debit"

    :goto_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 259
    invoke-direct {v0, p1, v2, v1, v2}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_5
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch
.end method

.method public b()Lio/reactivex/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->b()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "digitizedCardId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 244
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 245
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p2

    .line 320
    invoke-virtual {p2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Lcom/siimkinks/sqlitemagic/dx$b;->b(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object p2

    const-string v0, "UPDATE\n          TABLE C\u2026RD_ID to digitizedCardId)"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    sget-object v0, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v1, "CARD_DATA.CARD_ID"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/siimkinks/sqlitemagic/x;

    .line 321
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v0, "this.`is`(value)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object p1

    .line 247
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dx$d;->b()Lio/reactivex/w;

    move-result-object p1

    .line 248
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    const-string p2, "(UPDATE\n          TABLE \u2026         .ignoreElement()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    .line 119
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/cards/j;->b(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 121
    new-instance v1, Lcom/swedbank/mobile/data/cards/f$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/cards/f$a;-><init>(Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "cardsService\n      .bloc\u2026kError)\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;Z)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    if-eqz p2, :cond_0

    .line 191
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/cards/j;->f(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    goto :goto_0

    .line 192
    :cond_0
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/cards/j;->g(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 195
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 314
    new-instance v1, Lcom/swedbank/mobile/data/cards/f$c;

    invoke-direct {v1, p2, p1}, Lcom/swedbank/mobile/data/cards/f$c;-><init>(ZLjava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 189
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type io.reactivex.Single<com.swedbank.mobile.data.network.NetworkResponse<com.swedbank.mobile.data.cards.InternetShoppingChangeResponse>>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public c(Ljava/lang/String;Z)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1, p2}, Lcom/swedbank/mobile/business/cards/u;->c(Ljava/lang/String;Z)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->a:Lcom/swedbank/mobile/data/cards/i;

    .line 143
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/cards/j;->c(Lcom/swedbank/mobile/data/cards/i;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 144
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 145
    sget-object v0, Lcom/swedbank/mobile/data/cards/f$j;->a:Lcom/swedbank/mobile/data/cards/f$j;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "cardsService\n      .unbl\u2026kError)\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public d(Ljava/lang/String;)Lcom/swedbank/mobile/business/e;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    new-instance p1, Lcom/swedbank/mobile/business/e;

    const-string v0, "business/d2d/cards/list"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p1, v0, v1, v2, v1}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    return-object p1
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->d()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/u;->e(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public f(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/u;->f(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public g(Ljava/lang/String;)Lio/reactivex/j;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/u;->g(Ljava/lang/String;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public h(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/u;->h(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public i(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/u;->i(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public j(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/u;->j(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
