.class public final Lcom/swedbank/mobile/data/cards/a;
.super Ljava/lang/Object;
.source "CardData.kt"


# instance fields
.field private final a:Ljava/lang/Long;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:J

.field private final h:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/business/cards/CardClass;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final j:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final k:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Lcom/swedbank/mobile/business/cards/CardState;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final p:Z

.field private final q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final r:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final t:Ljava/lang/Boolean;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/lang/Boolean;)V
    .locals 13
    .param p1    # Ljava/lang/Long;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/business/cards/CardClass;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p16    # Lcom/swedbank/mobile/business/cards/CardState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p18    # Lcom/swedbank/mobile/business/cards/DigitizationProgression;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p19    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p20    # Lcom/swedbank/mobile/data/cards/DeliveryMethod;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p21    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    move-object v0, p0

    move-object v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    move-object/from16 v10, p16

    move-object/from16 v11, p18

    const-string v12, "cardId"

    invoke-static {p2, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "customerId"

    invoke-static {v2, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "accountId"

    invoke-static {v3, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "number"

    invoke-static {v4, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "bin"

    invoke-static {v5, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "cardHolderName"

    invoke-static {v6, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "parentType"

    invoke-static {v7, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "type"

    invoke-static {v8, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "typeId"

    invoke-static {v9, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "state"

    invoke-static {v10, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "digitizationProgression"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v12, p1

    iput-object v12, v0, Lcom/swedbank/mobile/data/cards/a;->a:Ljava/lang/Long;

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    iput-object v3, v0, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    iput-object v4, v0, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    iput-object v5, v0, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    move-wide/from16 v1, p7

    iput-wide v1, v0, Lcom/swedbank/mobile/data/cards/a;->g:J

    iput-object v6, v0, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    iput-object v7, v0, Lcom/swedbank/mobile/data/cards/a;->i:Lcom/swedbank/mobile/business/cards/CardClass;

    iput-object v8, v0, Lcom/swedbank/mobile/data/cards/a;->j:Ljava/lang/String;

    iput-object v9, v0, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    move/from16 v1, p13

    iput-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    move/from16 v1, p14

    iput-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->m:Z

    move/from16 v1, p15

    iput-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->n:Z

    iput-object v10, v0, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    move/from16 v1, p17

    iput-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->p:Z

    iput-object v11, v0, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-object/from16 v1, p19

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    move-object/from16 v1, p20

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-object/from16 v1, p21

    iput-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/lang/Boolean;ILkotlin/e/b/g;)V
    .locals 25

    move/from16 v0, p22

    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 19
    move-object v1, v2

    check-cast v1, Ljava/lang/Long;

    move-object v4, v1

    goto :goto_0

    :cond_0
    move-object/from16 v4, p1

    :goto_0
    and-int/lit16 v1, v0, 0x800

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    const/16 v16, 0x0

    goto :goto_1

    :cond_1
    move/from16 v16, p13

    :goto_1
    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_2

    const/16 v17, 0x0

    goto :goto_2

    :cond_2
    move/from16 v17, p14

    :goto_2
    and-int/lit16 v1, v0, 0x2000

    if-eqz v1, :cond_3

    const/16 v18, 0x0

    goto :goto_3

    :cond_3
    move/from16 v18, p15

    :goto_3
    and-int/lit16 v1, v0, 0x4000

    if-eqz v1, :cond_4

    .line 33
    sget-object v1, Lcom/swedbank/mobile/business/cards/CardState;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;

    move-object/from16 v19, v1

    goto :goto_4

    :cond_4
    move-object/from16 v19, p16

    :goto_4
    const v1, 0x8000

    and-int/2addr v1, v0

    if-eqz v1, :cond_5

    const/16 v20, 0x0

    goto :goto_5

    :cond_5
    move/from16 v20, p17

    :goto_5
    const/high16 v1, 0x10000

    and-int/2addr v1, v0

    if-eqz v1, :cond_6

    .line 35
    sget-object v1, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->NOT_APPLICABLE:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-object/from16 v21, v1

    goto :goto_6

    :cond_6
    move-object/from16 v21, p18

    :goto_6
    const/high16 v1, 0x20000

    and-int/2addr v1, v0

    if-eqz v1, :cond_7

    .line 36
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object/from16 v22, v1

    goto :goto_7

    :cond_7
    move-object/from16 v22, p19

    :goto_7
    const/high16 v1, 0x40000

    and-int/2addr v1, v0

    if-eqz v1, :cond_8

    .line 38
    move-object v1, v2

    check-cast v1, Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-object/from16 v23, v1

    goto :goto_8

    :cond_8
    move-object/from16 v23, p20

    :goto_8
    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_9

    .line 39
    move-object v0, v2

    check-cast v0, Ljava/lang/Boolean;

    move-object/from16 v24, v0

    goto :goto_9

    :cond_9
    move-object/from16 v24, p21

    :goto_9
    move-object/from16 v3, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    invoke-direct/range {v3 .. v24}, Lcom/swedbank/mobile/data/cards/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/cards/a;
    .locals 25
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    move-object/from16 v0, p0

    .line 41
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->i:Lcom/swedbank/mobile/business/cards/CardClass;

    sget-object v2, Lcom/swedbank/mobile/data/cards/b;->a:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v3, 0x0

    packed-switch v1, :pswitch_data_0

    const/4 v2, 0x0

    goto/16 :goto_c

    .line 62
    :pswitch_0
    iget-object v5, v0, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    .line 63
    iget-object v6, v0, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    .line 64
    iget-object v7, v0, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    .line 65
    iget-object v8, v0, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    .line 66
    iget-object v9, v0, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    .line 67
    new-instance v10, Lcom/swedbank/mobile/business/cards/c;

    iget-wide v11, v0, Lcom/swedbank/mobile/data/cards/a;->g:J

    invoke-direct {v10, v11, v12}, Lcom/swedbank/mobile/business/cards/c;-><init>(J)V

    .line 68
    iget-object v11, v0, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    .line 69
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/e;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/g;

    move-result-object v12

    .line 70
    iget-boolean v13, v0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    .line 71
    iget-object v14, v0, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    .line 72
    iget-object v15, v0, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    .line 73
    iget-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->p:Z

    .line 74
    iget-object v4, v0, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    if-eqz v4, :cond_0

    :goto_0
    move-object/from16 v17, v4

    goto :goto_1

    :cond_0
    const-string v4, ""

    goto :goto_0

    :goto_1
    const/16 v18, 0x0

    .line 75
    iget-boolean v4, v0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    if-eqz v4, :cond_1

    iget-boolean v4, v0, Lcom/swedbank/mobile/data/cards/a;->m:Z

    if-eqz v4, :cond_1

    const/16 v19, 0x1

    goto :goto_2

    :cond_1
    const/16 v19, 0x0

    .line 76
    :goto_2
    iget-boolean v4, v0, Lcom/swedbank/mobile/data/cards/a;->n:Z

    .line 77
    iget-object v2, v0, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move/from16 v21, v2

    goto :goto_3

    :cond_2
    const/16 v21, 0x0

    .line 78
    :goto_3
    iget-object v2, v0, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    if-nez v2, :cond_3

    .line 186
    sget-object v2, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    check-cast v2, Lcom/swedbank/mobile/business/cards/r;

    :goto_4
    move-object/from16 v22, v2

    goto :goto_5

    .line 187
    :cond_3
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_5

    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v16

    check-cast v16, Ljava/lang/CharSequence;

    invoke-interface/range {v16 .. v16}, Ljava/lang/CharSequence;->length()I

    move-result v16

    if-lez v16, :cond_4

    const/4 v3, 0x1

    :cond_4
    if-eqz v3, :cond_5

    new-instance v3, Lcom/swedbank/mobile/business/cards/r$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/r$a;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/swedbank/mobile/business/cards/r;

    move-object/from16 v22, v3

    goto :goto_5

    .line 188
    :cond_5
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    sget-object v2, Lcom/swedbank/mobile/business/cards/r$b;->a:Lcom/swedbank/mobile/business/cards/r$b;

    check-cast v2, Lcom/swedbank/mobile/business/cards/r;

    goto :goto_4

    .line 189
    :cond_6
    sget-object v2, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    check-cast v2, Lcom/swedbank/mobile/business/cards/r;

    goto :goto_4

    :goto_5
    const/16 v23, 0x2000

    const/16 v24, 0x0

    .line 61
    new-instance v2, Lcom/swedbank/mobile/business/cards/o;

    move v3, v4

    move-object v4, v2

    move/from16 v16, v1

    move/from16 v20, v3

    invoke-direct/range {v4 .. v24}, Lcom/swedbank/mobile/business/cards/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;ZLcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/DigitizationProgression;ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/s;ZZZLcom/swedbank/mobile/business/cards/r;ILkotlin/e/b/g;)V

    check-cast v2, Lcom/swedbank/mobile/business/cards/a;

    goto/16 :goto_c

    .line 43
    :pswitch_1
    iget-object v4, v0, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    .line 44
    iget-object v5, v0, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    .line 45
    iget-object v6, v0, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    .line 46
    iget-object v7, v0, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    .line 47
    iget-object v8, v0, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    .line 48
    new-instance v9, Lcom/swedbank/mobile/business/cards/c;

    iget-wide v1, v0, Lcom/swedbank/mobile/data/cards/a;->g:J

    invoke-direct {v9, v1, v2}, Lcom/swedbank/mobile/business/cards/c;-><init>(J)V

    .line 49
    iget-object v10, v0, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    .line 50
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/e;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/g;

    move-result-object v11

    .line 51
    iget-boolean v12, v0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    .line 52
    iget-object v13, v0, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    .line 53
    iget-object v14, v0, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    .line 54
    iget-boolean v15, v0, Lcom/swedbank/mobile/data/cards/a;->p:Z

    .line 55
    iget-object v1, v0, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    if-eqz v1, :cond_7

    :goto_6
    move-object/from16 v16, v1

    goto :goto_7

    :cond_7
    const-string v1, ""

    goto :goto_6

    :goto_7
    const/16 v17, 0x0

    .line 56
    iget-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    if-eqz v1, :cond_8

    iget-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->m:Z

    if-eqz v1, :cond_8

    const/16 v18, 0x1

    goto :goto_8

    :cond_8
    const/16 v18, 0x0

    .line 57
    :goto_8
    iget-boolean v1, v0, Lcom/swedbank/mobile/data/cards/a;->n:Z

    .line 58
    iget-object v2, v0, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move/from16 v20, v2

    goto :goto_9

    :cond_9
    const/16 v20, 0x0

    .line 59
    :goto_9
    iget-object v2, v0, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    if-nez v2, :cond_a

    .line 180
    sget-object v2, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    check-cast v2, Lcom/swedbank/mobile/business/cards/r;

    :goto_a
    move-object/from16 v21, v2

    goto :goto_b

    .line 181
    :cond_a
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_c

    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v19

    check-cast v19, Ljava/lang/CharSequence;

    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->length()I

    move-result v19

    if-lez v19, :cond_b

    const/4 v3, 0x1

    :cond_b
    if-eqz v3, :cond_c

    new-instance v3, Lcom/swedbank/mobile/business/cards/r$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/r$a;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/swedbank/mobile/business/cards/r;

    move-object/from16 v21, v3

    goto :goto_b

    .line 182
    :cond_c
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    sget-object v2, Lcom/swedbank/mobile/business/cards/r$b;->a:Lcom/swedbank/mobile/business/cards/r$b;

    check-cast v2, Lcom/swedbank/mobile/business/cards/r;

    goto :goto_a

    .line 183
    :cond_d
    sget-object v2, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    check-cast v2, Lcom/swedbank/mobile/business/cards/r;

    goto :goto_a

    :goto_b
    const/16 v22, 0x2000

    const/16 v23, 0x0

    .line 42
    new-instance v2, Lcom/swedbank/mobile/business/cards/p;

    move-object v3, v2

    move/from16 v19, v1

    invoke-direct/range {v3 .. v23}, Lcom/swedbank/mobile/business/cards/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;ZLcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/DigitizationProgression;ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/s;ZZZLcom/swedbank/mobile/business/cards/r;ILkotlin/e/b/g;)V

    check-cast v2, Lcom/swedbank/mobile/business/cards/a;

    :goto_c
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Ljava/lang/Long;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_6

    instance-of v1, p1, Lcom/swedbank/mobile/data/cards/a;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    check-cast p1, Lcom/swedbank/mobile/data/cards/a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->a:Ljava/lang/Long;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->a:Ljava/lang/Long;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-wide v3, p0, Lcom/swedbank/mobile/data/cards/a;->g:J

    iget-wide v5, p1, Lcom/swedbank/mobile/data/cards/a;->g:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->i:Lcom/swedbank/mobile/business/cards/CardClass;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->i:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->j:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/cards/a;->l:Z

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->m:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/cards/a;->m:Z

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->n:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/cards/a;->n:Z

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->p:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/cards/a;->p:Z

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_5

    :cond_5
    return v2

    :cond_6
    :goto_5
    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final h()J
    .locals 2

    .line 25
    iget-wide v0, p0, Lcom/swedbank/mobile/data/cards/a;->g:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->a:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/swedbank/mobile/data/cards/a;->g:J

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->i:Lcom/swedbank/mobile/business/cards/CardClass;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->j:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/data/cards/a;->m:Z

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :cond_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/data/cards/a;->n:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :cond_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_d
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/data/cards/a;->p:Z

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :cond_e
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_f
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_10
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_11
    const/4 v2, 0x0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_12

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_12
    add-int/2addr v0, v1

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcom/swedbank/mobile/business/cards/CardClass;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->i:Lcom/swedbank/mobile/business/cards/CardClass;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    return v0
.end method

.method public final n()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/a;->m:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/a;->n:Z

    return v0
.end method

.method public final p()Lcom/swedbank/mobile/business/cards/CardState;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/a;->p:Z

    return v0
.end method

.method public final r()Lcom/swedbank/mobile/business/cards/DigitizationProgression;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Lcom/swedbank/mobile/data/cards/DeliveryMethod;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardData(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", customerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", bin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", validUntil="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/swedbank/mobile/data/cards/a;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", cardHolderName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", parentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->i:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", typeId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", contactless="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", contactlessAllowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->m:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", contactlessEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->n:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->o:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", digitizable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/a;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", digitizationProgression="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->q:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", digitizedCardId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", deliveryMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->s:Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", internetShoppingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/Boolean;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/a;->t:Ljava/lang/Boolean;

    return-object v0
.end method
