.class public final Lcom/swedbank/mobile/data/cards/CardDisableInternetShoppingResponse;
.super Ljava/lang/Object;
.source "InternetShoppingChangeResponse.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/cards/l;


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "disableInternetShopping"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/network/GeneralResponseError;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/CardDisableInternetShoppingResponse;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/CardDisableInternetShoppingResponse;->b:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardDisableInternetShoppingResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardDisableInternetShoppingResponse;->b:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-object v0
.end method
