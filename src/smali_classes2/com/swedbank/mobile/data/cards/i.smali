.class public interface abstract Lcom/swedbank/mobile/data/cards/i;
.super Ljava/lang/Object;
.source "CardsService.kt"


# virtual methods
.method public abstract a(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardsResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract c(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardBlockResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract d(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardUnblockResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract e(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardEnableContactlessResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract f(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardDisableContactlessResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract g(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardEnableInternetShoppingResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract h(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardDisableInternetShoppingResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract i(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method
