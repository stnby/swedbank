.class final Lcom/swedbank/mobile/data/cards/f$j;
.super Ljava/lang/Object;
.source "CardsRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/cards/f;->c(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/cards/f$j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/cards/f$j;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/cards/f$j;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/cards/f$j;->a:Lcom/swedbank/mobile/data/cards/f$j;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lcom/swedbank/mobile/business/cards/details/n;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardUnblockResponse;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/details/n;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    if-eqz v0, :cond_1

    .line 148
    check-cast p1, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/cards/CardUnblockResponse;

    .line 149
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/CardUnblockResponse;->b()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object p1

    if-nez p1, :cond_0

    .line 151
    sget-object p1, Lcom/swedbank/mobile/business/cards/details/n$c;->a:Lcom/swedbank/mobile/business/cards/details/n$c;

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/n;

    goto :goto_2

    .line 153
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/n$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/n$a;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/n;

    goto :goto_2

    .line 297
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 298
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 299
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 300
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 301
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 298
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 302
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 297
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/cards/details/n$b;

    .line 156
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/n$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_1

    .line 303
    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/cards/details/n$b;

    .line 156
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/n$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 305
    :goto_1
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/n;

    :goto_2
    return-object p1

    .line 304
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/cards/f$j;->a(Lcom/swedbank/mobile/data/network/w;)Lcom/swedbank/mobile/business/cards/details/n;

    move-result-object p1

    return-object p1
.end method
