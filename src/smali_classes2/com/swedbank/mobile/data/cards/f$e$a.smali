.class public final Lcom/swedbank/mobile/data/cards/f$e$a;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/cards/f$e;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/f$e$a;->a:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Throwable;
    .locals 11
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f$e$a;->a:Lcom/swedbank/mobile/data/network/w;

    .line 34
    instance-of v1, v0, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f$e$a;->a:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$b;

    .line 236
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 237
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 238
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 239
    check-cast v2, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 236
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 240
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 236
    move-object v2, v1

    check-cast v2, Ljava/lang/Iterable;

    const-string v0, "\n"

    .line 34
    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    goto :goto_1

    .line 35
    :cond_1
    instance-of v0, v0, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f$e$a;->a:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object v1

    goto :goto_1

    .line 36
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Network response is not an error"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Ljava/lang/Throwable;

    :goto_1
    return-object v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/f$e$a;->a()Ljava/lang/Throwable;

    move-result-object v0

    return-object v0
.end method
