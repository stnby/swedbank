.class public final Lcom/swedbank/mobile/data/cards/p;
.super Ljava/lang/Object;
.source "SqliteMagic_CardData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/cards/a;
    .locals 26

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 146
    iget v2, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 147
    iget v3, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v3, v3, 0x14

    iput v3, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 148
    new-instance v1, Lcom/swedbank/mobile/data/cards/a;

    .line 149
    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    move-object v5, v4

    goto :goto_0

    :cond_0
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object v5, v3

    :goto_0
    add-int/lit8 v3, v2, 0x1

    .line 150
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v3, v2, 0x2

    .line 151
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v3, v2, 0x3

    .line 152
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v3, v2, 0x4

    .line 153
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v3, v2, 0x5

    .line 154
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v3, v2, 0x6

    .line 155
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    add-int/lit8 v3, v2, 0x7

    .line 156
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    add-int/lit8 v3, v2, 0x8

    .line 157
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/cards/c;->a(I)Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v14

    add-int/lit8 v3, v2, 0x9

    .line 158
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    add-int/lit8 v3, v2, 0xa

    .line 159
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    add-int/lit8 v3, v2, 0xb

    .line 160
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    add-int/lit8 v3, v2, 0xc

    .line 161
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    add-int/lit8 v3, v2, 0xd

    .line 162
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    add-int/lit8 v3, v2, 0xe

    .line 163
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/cards/c;->b(I)Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v20

    add-int/lit8 v3, v2, 0xf

    .line 164
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v21

    add-int/lit8 v3, v2, 0x10

    .line 165
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/cards/c;->c(I)Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v22

    add-int/lit8 v3, v2, 0x11

    .line 166
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v23

    if-eqz v23, :cond_1

    move-object/from16 v23, v4

    goto :goto_1

    :cond_1
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v23, v3

    :goto_1
    add-int/lit8 v3, v2, 0x12

    .line 167
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v24

    if-eqz v24, :cond_2

    move-object/from16 v24, v4

    goto :goto_2

    :cond_2
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/cards/c;->a(Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v3

    move-object/from16 v24, v3

    :goto_2
    add-int/lit8 v2, v2, 0x13

    .line 168
    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v25, v4

    goto :goto_3

    :cond_3
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    move-object/from16 v25, v0

    :goto_3
    move-object v4, v1

    invoke-direct/range {v4 .. v25}, Lcom/swedbank/mobile/data/cards/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/lang/Boolean;)V

    return-object v1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/a;
    .locals 55
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/cards/a;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 174
    invoke-virtual/range {p2 .. p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 176
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v3

    :cond_0
    const-string v2, "card_data"

    .line 181
    :cond_1
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_6

    .line 183
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 184
    new-instance v2, Lcom/swedbank/mobile/data/cards/a;

    .line 185
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v5, v3

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v5, v4

    :goto_0
    add-int/lit8 v4, v1, 0x1

    .line 186
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v4, v1, 0x2

    .line 187
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v4, v1, 0x3

    .line 188
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v4, v1, 0x4

    .line 189
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v4, v1, 0x5

    .line 190
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v4, v1, 0x6

    .line 191
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    add-int/lit8 v4, v1, 0x7

    .line 192
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    add-int/lit8 v4, v1, 0x8

    .line 193
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/cards/c;->a(I)Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v14

    add-int/lit8 v4, v1, 0x9

    .line 194
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    add-int/lit8 v4, v1, 0xa

    .line 195
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    add-int/lit8 v4, v1, 0xb

    .line 196
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    add-int/lit8 v4, v1, 0xc

    .line 197
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    add-int/lit8 v4, v1, 0xd

    .line 198
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    add-int/lit8 v4, v1, 0xe

    .line 199
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/cards/c;->b(I)Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v20

    add-int/lit8 v4, v1, 0xf

    .line 200
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v21

    add-int/lit8 v4, v1, 0x10

    .line 201
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/cards/c;->c(I)Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v22

    add-int/lit8 v4, v1, 0x11

    .line 202
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v23

    if-eqz v23, :cond_3

    move-object/from16 v23, v3

    goto :goto_1

    :cond_3
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v23, v4

    :goto_1
    add-int/lit8 v4, v1, 0x12

    .line 203
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v24

    if-eqz v24, :cond_4

    move-object/from16 v24, v3

    goto :goto_2

    :cond_4
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/cards/c;->a(Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v4

    move-object/from16 v24, v4

    :goto_2
    add-int/lit8 v1, v1, 0x13

    .line 204
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_5

    :goto_3
    move-object/from16 v25, v3

    goto :goto_4

    :cond_5
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_3

    :goto_4
    move-object v4, v2

    invoke-direct/range {v4 .. v25}, Lcom/swedbank/mobile/data/cards/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/lang/Boolean;)V

    return-object v2

    .line 206
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 207
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".card_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_1e

    .line 211
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".customer_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_1d

    .line 215
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".account_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_1c

    .line 219
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".number"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_1b

    .line 223
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".bin"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    if-eqz v9, :cond_1a

    .line 227
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, ".valid_until"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_19

    .line 231
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, ".card_holder_name"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    if-eqz v11, :cond_18

    .line 235
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ".parent_type"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    if-eqz v12, :cond_17

    .line 239
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, ".type"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    if-eqz v13, :cond_16

    .line 243
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, ".type_id"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    if-eqz v14, :cond_15

    .line 247
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".contactless"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_14

    .line 251
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v26, v3

    const-string v3, ".contactless_allowed"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_13

    .line 255
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v27, v3

    const-string v3, ".contactless_enabled"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_12

    .line 259
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v28, v3

    const-string v3, ".state"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_11

    .line 263
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v29, v3

    const-string v3, ".digitizable"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_10

    .line 267
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v30, v3

    const-string v3, ".digitization_progression"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_f

    .line 271
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v31, v3

    const-string v3, ".digitized_card_id"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 272
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v32, v3

    const-string v3, ".delivery_method"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 273
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".internet_shopping_enabled"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 274
    new-instance v2, Lcom/swedbank/mobile/data/cards/a;

    if-eqz v4, :cond_8

    .line 275
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-eqz v15, :cond_7

    goto :goto_5

    :cond_7
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v34, v4

    goto :goto_6

    :cond_8
    :goto_5
    const/16 v34, 0x0

    .line 276
    :goto_6
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 277
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 278
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 279
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 280
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    .line 281
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 282
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    .line 283
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/cards/c;->a(I)Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v43

    .line 284
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 285
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v45

    .line 286
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v46

    .line 287
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v47

    .line 288
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v48

    .line 289
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/cards/c;->b(I)Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v49

    .line 290
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v50

    .line 291
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/cards/c;->c(I)Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v51

    if-eqz v32, :cond_a

    .line 292
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_9

    goto :goto_7

    :cond_9
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v52, v4

    goto :goto_8

    :cond_a
    :goto_7
    const/16 v52, 0x0

    :goto_8
    if-eqz v3, :cond_c

    .line 293
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_b

    goto :goto_9

    :cond_b
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/cards/c;->a(Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v3

    move-object/from16 v53, v3

    goto :goto_a

    :cond_c
    :goto_9
    const/16 v53, 0x0

    :goto_a
    if-eqz v1, :cond_e

    .line 294
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_d

    goto :goto_b

    :cond_d
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v54, v3

    goto :goto_c

    :cond_e
    :goto_b
    const/16 v54, 0x0

    :goto_c
    move-object/from16 v33, v2

    invoke-direct/range {v33 .. v54}, Lcom/swedbank/mobile/data/cards/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/cards/CardState;ZLcom/swedbank/mobile/business/cards/DigitizationProgression;Ljava/lang/String;Lcom/swedbank/mobile/data/cards/DeliveryMethod;Ljava/lang/Boolean;)V

    return-object v2

    .line 269
    :cond_f
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"digitization_progression\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 265
    :cond_10
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"digitizable\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_11
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"state\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_12
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"contactless_enabled\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_13
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"contactless_allowed\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_14
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"contactless\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_15
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"type_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_16
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"type\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_17
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"parent_type\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_18
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"card_holder_name\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_19
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"valid_until\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_1a
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"bin\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_1b
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"number\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_1c
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"account_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_1d
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"customer_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_1e
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_data\" required column \"card_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/swedbank/mobile/data/cards/a;)Ljava/lang/Long;
    .locals 0

    .line 141
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->b()Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/cards/a;)V
    .locals 2

    .line 58
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0xd1b

    if-eq v0, v1, :cond_1

    const v1, 0x21045caa

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "card_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, -0x1

    :goto_1
    packed-switch p2, :pswitch_data_0

    goto :goto_2

    .line 66
    :pswitch_0
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/cards/a;->c()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 60
    :pswitch_1
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/cards/a;->b()Ljava/lang/Long;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 63
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/cards/a;->b()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    invoke-interface {p0, p1, p2, p3}, Landroidx/k/a/f;->a(IJ)V

    goto :goto_2

    .line 61
    :cond_3
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "id column is null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/cards/a;)V
    .locals 3

    .line 83
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 84
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 85
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->h()J

    move-result-wide v0

    const/4 v2, 0x6

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 90
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->i()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->j()Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardClass;)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x8

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 92
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->k()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 93
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->l()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 94
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xb

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 95
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xc

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 96
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xd

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 97
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->p()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardState;)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xe

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 98
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->q()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xf

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 99
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->r()Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/DigitizationProgression;)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x10

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 100
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->s()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    .line 101
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->s()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 103
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->t()Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v0, 0x12

    .line 104
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->t()Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/data/cards/DeliveryMethod;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 106
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->u()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v0, 0x13

    .line 107
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->u()Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    :cond_2
    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/cards/a;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/cards/a;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->b()V

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->b()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "id"

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "card_id"

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "customer_id"

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account_id"

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "number"

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "bin"

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "valid_until"

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->h()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "card_holder_name"

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "parent_type"

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->j()Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardClass;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "type"

    .line 37
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "type_id"

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "contactless"

    .line 39
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->m()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "contactless_allowed"

    .line 40
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->n()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "contactless_enabled"

    .line 41
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->o()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "state"

    .line 42
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->p()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardState;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "digitizable"

    .line 43
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->q()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "digitization_progression"

    .line 44
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->r()Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/DigitizationProgression;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->s()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "digitized_card_id"

    .line 46
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    :cond_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->t()Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "delivery_method"

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->t()Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/data/cards/DeliveryMethod;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->u()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "internet_shopping_enabled"

    .line 52
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/a;->u()Ljava/lang/Boolean;

    move-result-object p0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/swedbank/mobile/data/cards/a;)Z
    .locals 4

    .line 72
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0xd1b

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x21045caa

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "card_id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 78
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " is not unique"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_0
    return v3

    .line 74
    :pswitch_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->b()Ljava/lang/Long;

    move-result-object p0

    if-nez p0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/cards/a;)V
    .locals 3

    .line 112
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 113
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 114
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 115
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 117
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 118
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->h()J

    move-result-wide v0

    const/4 v2, 0x6

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 119
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->i()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 120
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->j()Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardClass;)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x8

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 121
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->k()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 122
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->l()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 123
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xb

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 124
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xc

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 125
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xd

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 126
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->p()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardState;)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xe

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 127
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->q()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0xf

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 128
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->r()Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/DigitizationProgression;)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x10

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 129
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->s()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    .line 130
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->s()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 132
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->t()Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v0, 0x12

    .line 133
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->t()Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/data/cards/DeliveryMethod;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 135
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->u()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v0, 0x13

    .line 136
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/a;->u()Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    :cond_2
    return-void
.end method
