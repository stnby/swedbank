.class public final Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "DeliveryMethodJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/cards/DeliveryMethod;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 3
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "address"

    const-string v1, "comment"

    .line 13
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"address\", \"comment\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 16
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "address"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<String?>(S\u2026ns.emptySet(), \"address\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/cards/DeliveryMethod;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/cards/DeliveryMethod;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 55
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "address"

    .line 56
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "comment"

    .line 58
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 60
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 53
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p2, Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/cards/DeliveryMethod;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/cards/DeliveryMethod;
    .locals 7
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v2, 0x0

    move-object v4, v1

    const/4 v3, 0x0

    .line 26
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 27
    iget-object v5, p0, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {p1, v5}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v5

    const/4 v6, 0x1

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 33
    :pswitch_0
    iget-object v3, p0, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v4, v3

    const/4 v3, 0x1

    goto :goto_0

    .line 29
    :pswitch_1
    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/DeliveryMethodJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v1, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    goto :goto_0

    .line 38
    :pswitch_2
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->j()V

    .line 39
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->q()V

    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->f()V

    .line 44
    new-instance p1, Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    const/4 v5, 0x3

    invoke-direct {p1, v0, v0, v5, v0}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    if-eqz v2, :cond_1

    goto :goto_1

    .line 46
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-eqz v3, :cond_2

    goto :goto_2

    .line 47
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->b()Ljava/lang/String;

    move-result-object v4

    .line 45
    :goto_2
    invoke-virtual {p1, v1, v4}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(DeliveryMethod)"

    return-object v0
.end method
