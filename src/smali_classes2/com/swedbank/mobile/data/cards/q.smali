.class public final Lcom/swedbank/mobile/data/cards/q;
.super Ljava/lang/Object;
.source "SqliteMagic_CardLimitData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/cards/d;
    .locals 11

    .line 66
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 67
    iget v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v1, v1, 0x8

    iput v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 68
    new-instance p1, Lcom/swedbank/mobile/data/cards/d;

    .line 69
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    move-object v3, v1

    goto :goto_1

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :goto_1
    add-int/lit8 v1, v0, 0x1

    .line 70
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v1, v0, 0x2

    .line 71
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    add-int/lit8 v1, v0, 0x3

    .line 72
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v6

    add-int/lit8 v1, v0, 0x4

    .line 73
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    add-int/lit8 v1, v0, 0x5

    .line 74
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v1, v0, 0x6

    .line 75
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/cards/c;->b(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v9

    add-int/lit8 v0, v0, 0x7

    .line 76
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/swedbank/mobile/data/cards/c;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v10

    move-object v2, p1

    invoke-direct/range {v2 .. v10}, Lcom/swedbank/mobile/data/cards/d;-><init>(Ljava/lang/Long;Ljava/lang/String;ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V

    return-object p1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/d;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/cards/d;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 82
    invoke-virtual/range {p2 .. p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 84
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v3

    :cond_0
    const-string v2, "card_limit_data"

    .line 89
    :cond_1
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_3

    .line 91
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 92
    new-instance v2, Lcom/swedbank/mobile/data/cards/d;

    .line 93
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_0
    move-object v5, v3

    goto :goto_1

    :cond_2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    :goto_1
    add-int/lit8 v3, v1, 0x1

    .line 94
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v3, v1, 0x2

    .line 95
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    add-int/lit8 v3, v1, 0x3

    .line 96
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v8

    add-int/lit8 v3, v1, 0x4

    .line 97
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    add-int/lit8 v3, v1, 0x5

    .line 98
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v3, v1, 0x6

    .line 99
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/cards/c;->b(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v11

    add-int/lit8 v1, v1, 0x7

    .line 100
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v12

    move-object v4, v2

    invoke-direct/range {v4 .. v12}, Lcom/swedbank/mobile/data/cards/d;-><init>(Ljava/lang/Long;Ljava/lang/String;ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V

    return-object v2

    .line 102
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".local_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 103
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".card_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_c

    .line 107
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".active"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_b

    .line 111
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".card_limit"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_a

    .line 115
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".limit_maximum"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_9

    .line 119
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".limit_currency"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    if-eqz v9, :cond_8

    .line 123
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, ".service"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_7

    .line 127
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".validity"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 131
    new-instance v2, Lcom/swedbank/mobile/data/cards/d;

    if-eqz v4, :cond_5

    .line 132
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-interface {v0, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :cond_5
    :goto_2
    move-object v12, v3

    .line 133
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 134
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    .line 135
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v15

    .line 136
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 137
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 138
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/cards/c;->b(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v18

    .line 139
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v19

    move-object v11, v2

    invoke-direct/range {v11 .. v19}, Lcom/swedbank/mobile/data/cards/d;-><init>(Ljava/lang/Long;Ljava/lang/String;ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V

    return-object v2

    .line 129
    :cond_6
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_limit_data\" required column \"validity\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_7
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_limit_data\" required column \"service\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_8
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_limit_data\" required column \"limit_currency\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_9
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_limit_data\" required column \"limit_maximum\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_a
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_limit_data\" required column \"card_limit\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_b
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_limit_data\" required column \"active\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_c
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"card_limit_data\" required column \"card_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/cards/d;)V
    .locals 3

    .line 39
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 40
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/d;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/d;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const/4 v2, 0x2

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 42
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/d;->d()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/d;->e()I

    move-result v0

    int-to-long v0, v0

    const/4 v2, 0x4

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 44
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/d;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/d;->g()Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardLimitService;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/d;->h()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/cards/c;->a(Lcom/swedbank/mobile/business/cards/CardLimitValidity;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x7

    invoke-interface {p0, v0, p1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    return-void
.end method
