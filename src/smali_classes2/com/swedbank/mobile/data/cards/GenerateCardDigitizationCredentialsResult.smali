.class public final Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;
.super Ljava/lang/Object;
.source "GenerateCardDigitizationCredentialsResult.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "generateCardDigitizationCredentials"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/network/GeneralResponseError;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->c:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;ILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 15
    move-object p1, v0

    check-cast p1, Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 16
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 17
    move-object p3, v0

    check-cast p3, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/cards/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 22
    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20
    new-instance v2, Lcom/swedbank/mobile/business/cards/b;

    invoke-direct {v2, v0, v1}, Lcom/swedbank/mobile/business/cards/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No card secret"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 21
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No card id"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/GenerateCardDigitizationCredentialsResult;->c:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-object v0
.end method
