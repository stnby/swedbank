.class public final synthetic Lcom/swedbank/mobile/data/cards/g;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I

.field public static final synthetic e:[I

.field public static final synthetic f:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardLimitService;->values()[Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/data/cards/g;->a:[I

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardLimitService;->ATM:Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardLimitService;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardLimitService;->CASH_OUT:Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardLimitService;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardLimitService;->PURCHASES:Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardLimitService;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardClass;->values()[Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/data/cards/g;->b:[I

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->b:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->b:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->b:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardClass;->values()[Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/data/cards/g;->c:[I

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->c:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->c:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->c:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardClass;->values()[Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/data/cards/g;->d:[I

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->d:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->d:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->d:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/swedbank/mobile/business/c/c;->values()[Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/data/cards/g;->e:[I

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->e:[I

    sget-object v1, Lcom/swedbank/mobile/business/c/c;->a:Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->e:[I

    sget-object v1, Lcom/swedbank/mobile/business/c/c;->b:Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->e:[I

    sget-object v1, Lcom/swedbank/mobile/business/c/c;->c:Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->e:[I

    sget-object v1, Lcom/swedbank/mobile/business/c/c;->d:Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardClass;->values()[Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/data/cards/g;->f:[I

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->f:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->f:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/cards/g;->f:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardClass;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
