.class public final Lcom/swedbank/mobile/data/cards/c;
.super Ljava/lang/Object;
.source "CardData.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/cards/CardClass;)I
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/CardClass;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardType"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/CardClass;->getId()I

    move-result p0

    return p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/cards/CardState;)I
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/CardState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "state"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result p0

    return p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/cards/DigitizationProgression;)I
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/DigitizationProgression;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "digitizationProgression"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->ordinal()I

    move-result p0

    return p0
.end method

.method public static final a(I)Lcom/swedbank/mobile/business/cards/CardClass;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 115
    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardClass;->values()[Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    .line 179
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 116
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/CardClass;->getId()I

    move-result v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    sget-object v4, Lcom/swedbank/mobile/business/cards/CardClass;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;

    :goto_3
    return-object v4
.end method

.method public static final a(Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/DeliveryMethod;
    .locals 7
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 101
    :cond_0
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    const-string p0, "\""

    filled-new-array {p0}, [Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object p0

    const/4 v1, 0x0

    .line 102
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    .line 103
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 104
    new-instance v2, Lcom/swedbank/mobile/data/cards/DeliveryMethod;

    const-string v3, "null"

    .line 105
    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v0

    :cond_1
    const-string v3, "null"

    .line 106
    invoke-static {p0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object p0, v0

    .line 104
    :cond_2
    invoke-direct {v2, v1, p0}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public static final a(Lcom/swedbank/mobile/business/cards/CardLimitService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/CardLimitService;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "service"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/CardLimitService;->name()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/cards/CardLimitValidity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/CardLimitValidity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "validity"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->name()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/data/cards/DeliveryMethod;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/data/cards/DeliveryMethod;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    if-eqz p0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/DeliveryMethod;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final b(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitService;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "dbValue"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardLimitService;->values()[Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v0

    .line 185
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 170
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/cards/CardLimitService;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    sget-object v3, Lcom/swedbank/mobile/business/cards/CardLimitService;->UNRECOGNIZED:Lcom/swedbank/mobile/business/cards/CardLimitService;

    :goto_2
    return-object v3
.end method

.method public static final b(I)Lcom/swedbank/mobile/business/cards/CardState;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 123
    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardState;->values()[Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/b;->c([Ljava/lang/Object;)I

    move-result v0

    if-gez p0, :cond_0

    goto :goto_0

    :cond_0
    if-lt v0, p0, :cond_1

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardState;->values()[Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v0

    aget-object p0, v0, p0

    goto :goto_1

    .line 124
    :cond_1
    :goto_0
    sget-object p0, Lcom/swedbank/mobile/business/cards/CardState;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;

    :goto_1
    return-object p0
.end method

.method public static final c(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitValidity;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "dbValue"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->values()[Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v0

    .line 187
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 178
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    sget-object v3, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->UNRECOGNIZED:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    :goto_2
    return-object v3
.end method

.method public static final c(I)Lcom/swedbank/mobile/business/cards/DigitizationProgression;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 131
    invoke-static {}, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->values()[Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/b;->c([Ljava/lang/Object;)I

    move-result v0

    if-gez p0, :cond_0

    goto :goto_0

    :cond_0
    if-lt v0, p0, :cond_1

    invoke-static {}, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->values()[Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v0

    aget-object p0, v0, p0

    goto :goto_1

    .line 132
    :cond_1
    :goto_0
    sget-object p0, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->NOT_APPLICABLE:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    :goto_1
    return-object p0
.end method
