.class public final Lcom/swedbank/mobile/data/cards/e;
.super Ljava/lang/Object;
.source "CardTypeParser.kt"


# direct methods
.method public static final a(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/g;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "raw"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    packed-switch p0, :pswitch_data_3

    packed-switch p0, :pswitch_data_4

    packed-switch p0, :pswitch_data_5

    packed-switch p0, :pswitch_data_6

    sparse-switch p0, :sswitch_data_0

    .line 43
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->a:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 24
    :sswitch_0
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->u:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 23
    :sswitch_1
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->r:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 34
    :sswitch_2
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->E:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 11
    :sswitch_3
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->f:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 10
    :sswitch_4
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->e:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 7
    :sswitch_5
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->b:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 25
    :pswitch_0
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->v:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 40
    :pswitch_1
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->K:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 38
    :pswitch_2
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->I:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 42
    :pswitch_3
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->t:Lcom/swedbank/mobile/business/cards/g;

    goto/16 :goto_0

    .line 35
    :pswitch_4
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->F:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 31
    :pswitch_5
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->B:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 29
    :pswitch_6
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->z:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 33
    :pswitch_7
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->D:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 13
    :pswitch_8
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->h:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 20
    :pswitch_9
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->o:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 17
    :pswitch_a
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->l:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 41
    :pswitch_b
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->s:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 39
    :pswitch_c
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->J:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 37
    :pswitch_d
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->H:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 32
    :pswitch_e
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->C:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 30
    :pswitch_f
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->A:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 28
    :pswitch_10
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->y:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 27
    :pswitch_11
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->x:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 26
    :pswitch_12
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->w:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 15
    :pswitch_13
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->j:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 19
    :pswitch_14
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->n:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 36
    :pswitch_15
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->G:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 18
    :pswitch_16
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->m:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 12
    :pswitch_17
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->g:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 16
    :pswitch_18
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->k:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 14
    :pswitch_19
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->i:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 9
    :pswitch_1a
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->d:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 8
    :pswitch_1b
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->c:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 22
    :pswitch_1c
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->q:Lcom/swedbank/mobile/business/cards/g;

    goto :goto_0

    .line 21
    :pswitch_1d
    sget-object p0, Lcom/swedbank/mobile/business/cards/g;->p:Lcom/swedbank/mobile/business/cards/g;

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x69
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x32f
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x334
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x385
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x389
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x38e
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_5
        0x3b -> :sswitch_4
        0x64 -> :sswitch_3
        0xdb -> :sswitch_2
        0x322 -> :sswitch_1
        0x326 -> :sswitch_0
    .end sparse-switch
.end method
