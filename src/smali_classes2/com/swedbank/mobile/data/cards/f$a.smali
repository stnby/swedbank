.class final Lcom/swedbank/mobile/data/cards/f$a;
.super Ljava/lang/Object;
.source "CardsRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/cards/f;->b(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/f$a;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/cards/CardBlockResponse;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    if-eqz v0, :cond_1

    .line 124
    check-cast p1, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/cards/CardBlockResponse;

    .line 125
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/cards/CardBlockResponse;->b()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object v0

    if-nez v0, :cond_0

    .line 127
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 128
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v2, "CARD_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 129
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->p:Lcom/siimkinks/sqlitemagic/v;

    sget-object v2, Lcom/swedbank/mobile/business/cards/CardState;->BLOCKED:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    .line 296
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v0

    const-string v1, "UPDATE\n                 \u2026ATE to CardState.BLOCKED)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->s:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, ""

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    .line 297
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/siimkinks/sqlitemagic/dx$a;->b(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v0

    const-string v1, "UPDATE\n                 \u2026.DIGITIZED_CARD_ID to \"\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    sget-object v1, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "CARD_DATA.CARD_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    iget-object v2, p0, Lcom/swedbank/mobile/data/cards/f$a;->a:Ljava/lang/String;

    .line 298
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/dx$d;->b()Lio/reactivex/w;

    move-result-object v0

    .line 133
    new-instance v1, Lcom/swedbank/mobile/data/cards/f$a$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/cards/f$a$1;-><init>(Lcom/swedbank/mobile/data/cards/CardBlockResponse;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "(UPDATE\n                \u2026sult.message.orEmpty()) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 135
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/cards/details/c$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/cards/details/c$a;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(CardBlockResult.Fail(error.message))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 302
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 303
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 304
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 305
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 306
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 303
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 307
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 302
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/cards/details/c$b;

    .line 138
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/c$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_1

    .line 308
    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/cards/details/c$b;

    .line 138
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/c$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 311
    :goto_1
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    .line 309
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/cards/f$a;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
