.class final Lcom/swedbank/mobile/data/cards/m$d;
.super Ljava/lang/Object;
.source "LocalCardsRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/cards/m;->h(Ljava/lang/String;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/siimkinks/sqlitemagic/ca<",
        "TR;>;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/cards/m$d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/cards/m$d;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/cards/m$d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/cards/m$d;->a:Lcom/swedbank/mobile/data/cards/m$d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/siimkinks/sqlitemagic/ca;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/siimkinks/sqlitemagic/ca;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/siimkinks/sqlitemagic/ca<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/ca;->e()Lio/reactivex/j;

    move-result-object p1

    .line 87
    sget-object v0, Lcom/swedbank/mobile/data/cards/m$d$1;->a:Lcom/swedbank/mobile/data/cards/m$d$1;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/cards/n;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/cards/n;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    .line 88
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->c(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/siimkinks/sqlitemagic/ca;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/cards/m$d;->a(Lcom/siimkinks/sqlitemagic/ca;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
