.class public final Lcom/swedbank/mobile/data/cards/f$i$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/cards/f$i;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/cards/f$i;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/cards/f$i;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/f$i$1;->a:Lcom/swedbank/mobile/data/cards/f$i;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/f$i$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f$i$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/cards/CardResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v2

    sget-object v3, Lcom/siimkinks/sqlitemagic/r;->a:Lcom/siimkinks/sqlitemagic/r;

    const-string v4, "CARD_LIMIT_DATA"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v2

    .line 243
    sget-object v3, Lcom/siimkinks/sqlitemagic/r;->a:Lcom/siimkinks/sqlitemagic/r;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/r;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "CARD_LIMIT_DATA.CARD_ID"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swedbank/mobile/data/cards/f$i$1;->a:Lcom/swedbank/mobile/data/cards/f$i;

    iget-object v4, v4, Lcom/swedbank/mobile/data/cards/f$i;->a:Ljava/lang/String;

    .line 244
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.`is`(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 242
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 246
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/cards/CardResponse;->l()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v2

    sget-object v3, Lcom/swedbank/mobile/business/cards/CardState;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;

    if-eq v2, v3, :cond_2

    .line 247
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/cards/CardResponse;->a()Lcom/swedbank/mobile/data/cards/a;

    move-result-object v2

    .line 255
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/ct$c;->a(Lcom/swedbank/mobile/data/cards/a;)Lcom/siimkinks/sqlitemagic/ct$c;

    move-result-object v2

    .line 253
    sget-object v3, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v3, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/ct$c;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/d;

    move-result-object v2

    .line 252
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/a/d;->a()Lcom/siimkinks/sqlitemagic/a/d;

    move-result-object v2

    .line 251
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/a/d;->b()J

    .line 256
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/cards/CardResponse;->q()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 259
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 268
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 267
    check-cast v3, Lcom/swedbank/mobile/data/cards/CardServiceResponse;

    .line 269
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e()Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v4

    sget-object v5, Lcom/swedbank/mobile/data/cards/g;->a:[I

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/CardLimitService;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    const/4 v3, 0x0

    goto :goto_1

    .line 270
    :pswitch_0
    iget-object v4, p0, Lcom/swedbank/mobile/data/cards/f$i$1;->a:Lcom/swedbank/mobile/data/cards/f$i;

    iget-object v4, v4, Lcom/swedbank/mobile/data/cards/f$i;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a(Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/d;

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_0

    .line 267
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 274
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 275
    move-object v0, v2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 276
    sget-object v0, Lcom/siimkinks/sqlitemagic/s;->a:Lcom/siimkinks/sqlitemagic/s;

    .line 280
    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/cu$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cu$a;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cu$a;->a()Z

    goto :goto_2

    .line 283
    :cond_2
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v0

    .line 287
    sget-object v2, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v3, "CARD_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v0

    .line 288
    sget-object v2, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v3, "CARD_DATA.CARD_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    iget-object v3, p0, Lcom/swedbank/mobile/data/cards/f$i$1;->a:Lcom/swedbank/mobile/data/cards/f$i;

    iget-object v3, v3, Lcom/swedbank/mobile/data/cards/f$i;->a:Ljava/lang/String;

    .line 289
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 293
    :cond_3
    :goto_2
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 295
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/f$i$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
