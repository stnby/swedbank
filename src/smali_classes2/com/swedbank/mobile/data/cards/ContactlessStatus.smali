.class public final enum Lcom/swedbank/mobile/data/cards/ContactlessStatus;
.super Ljava/lang/Enum;
.source "CardsResponse.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/data/cards/ContactlessStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/data/cards/ContactlessStatus;

.field public static final enum DISABLED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

.field public static final enum ENABLED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

.field public static final enum NOT_ALLOWED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    new-instance v1, Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    const-string v2, "NOT_ALLOWED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/cards/ContactlessStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->NOT_ALLOWED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    const-string v2, "ENABLED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/cards/ContactlessStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->ENABLED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    const-string v2, "DISABLED"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/cards/ContactlessStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->DISABLED:Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->$VALUES:[Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/ContactlessStatus;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/data/cards/ContactlessStatus;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/data/cards/ContactlessStatus;->$VALUES:[Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/data/cards/ContactlessStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/data/cards/ContactlessStatus;

    return-object v0
.end method
