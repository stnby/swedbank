.class public final Lcom/swedbank/mobile/data/cards/f$f$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/cards/f$f;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/cards/f$f;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/cards/f$f;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/cards/f$f$1;->a:Lcom/swedbank/mobile/data/cards/f$f;

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/f$f$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/f$f$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/cards/CardsResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    :try_start_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/cards/CardsResponse;->a()Ljava/util/List;

    move-result-object v0

    .line 241
    check-cast v0, Ljava/lang/Iterable;

    .line 248
    invoke-static {v0}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object v0

    .line 247
    sget-object v2, Lcom/swedbank/mobile/data/cards/f$g;->a:Lcom/swedbank/mobile/data/cards/f$g;

    check-cast v2, Lkotlin/e/a/b;

    invoke-static {v0, v2}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object v0

    .line 246
    sget-object v2, Lcom/swedbank/mobile/data/cards/f$h;->a:Lcom/swedbank/mobile/data/cards/f$h;

    check-cast v2, Lkotlin/e/a/b;

    invoke-static {v0, v2}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object v0

    .line 245
    invoke-static {v0}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object v0

    .line 252
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 253
    sget-object v0, Lcom/siimkinks/sqlitemagic/q;->a:Lcom/siimkinks/sqlitemagic/q;

    .line 254
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ct$b;->a()Lcom/siimkinks/sqlitemagic/ct$b;

    move-result-object v0

    const-string v2, "SqliteMagic_CardData_Han\u2026leteTableBuilder.create()"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/siimkinks/sqlitemagic/a/b;

    .line 253
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/b;->b()I

    .line 255
    sget-object v0, Lcom/siimkinks/sqlitemagic/s;->a:Lcom/siimkinks/sqlitemagic/s;

    .line 256
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cu$b;->a()Lcom/siimkinks/sqlitemagic/cu$b;

    move-result-object v0

    const-string v2, "SqliteMagic_CardLimitDat\u2026leteTableBuilder.create()"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/siimkinks/sqlitemagic/a/b;

    .line 255
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/b;->b()I

    goto/16 :goto_1

    .line 257
    :cond_0
    move-object v2, v0

    check-cast v2, Ljava/lang/Iterable;

    .line 258
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 259
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 260
    check-cast v4, Lcom/swedbank/mobile/data/cards/a;

    .line 257
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/cards/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    :cond_1
    check-cast v3, Ljava/util/List;

    .line 262
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v2

    .line 266
    sget-object v4, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    const-string v5, "CARD_DATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v4}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v2

    .line 267
    sget-object v4, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v5, "CARD_DATA.CARD_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    .line 268
    move-object v5, v3

    check-cast v5, Ljava/util/Collection;

    invoke-virtual {v4, v5}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.notIn(values)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 265
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 270
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v2

    .line 274
    sget-object v4, Lcom/siimkinks/sqlitemagic/r;->a:Lcom/siimkinks/sqlitemagic/r;

    const-string v5, "CARD_LIMIT_DATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v4}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v2

    .line 275
    sget-object v4, Lcom/siimkinks/sqlitemagic/r;->a:Lcom/siimkinks/sqlitemagic/r;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/r;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "CARD_LIMIT_DATA.CARD_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    check-cast v3, Ljava/util/Collection;

    invoke-virtual {v4, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.notIn(values)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 273
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 278
    sget-object v2, Lcom/siimkinks/sqlitemagic/q;->a:Lcom/siimkinks/sqlitemagic/q;

    .line 286
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/ct$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/ct$a;

    move-result-object v0

    .line 284
    sget-object v2, Lcom/siimkinks/sqlitemagic/p;->a:Lcom/siimkinks/sqlitemagic/p;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/p;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v2, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/ct$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object v0

    .line 283
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/a;->a()Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object v0

    .line 282
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/a;->b()Z

    .line 289
    :goto_1
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 291
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/cards/f$f$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
