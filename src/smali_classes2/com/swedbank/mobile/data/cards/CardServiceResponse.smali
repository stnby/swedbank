.class public final Lcom/swedbank/mobile/data/cards/CardServiceResponse;
.super Ljava/lang/Object;
.source "CardsResponse.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Z

.field private final b:Ljava/math/BigDecimal;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:I

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/cards/CardLimitService;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V
    .locals 1
    .param p2    # Ljava/math/BigDecimal;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/cards/CardLimitService;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/cards/CardLimitValidity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "limit"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "limitCurrency"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardService"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "period"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a:Z

    iput-object p2, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->b:Ljava/math/BigDecimal;

    iput p3, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->c:I

    iput-object p4, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e:Lcom/swedbank/mobile/business/cards/CardLimitService;

    iput-object p6, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/data/cards/d;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/swedbank/mobile/data/cards/d;

    .line 103
    iget-boolean v4, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a:Z

    .line 104
    iget-object v5, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->b:Ljava/math/BigDecimal;

    .line 105
    iget v6, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->c:I

    .line 106
    iget-object v7, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->d:Ljava/lang/String;

    .line 107
    iget-object v8, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e:Lcom/swedbank/mobile/business/cards/CardLimitService;

    .line 108
    iget-object v9, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v1, v0

    move-object v3, p1

    .line 101
    invoke-direct/range {v1 .. v11}, Lcom/swedbank/mobile/data/cards/d;-><init>(Ljava/lang/Long;Ljava/lang/String;ZLjava/math/BigDecimal;ILjava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;ILkotlin/e/b/g;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .line 94
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a:Z

    return v0
.end method

.method public final b()Ljava/math/BigDecimal;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->b:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .line 96
    iget v0, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->c:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/swedbank/mobile/business/cards/CardLimitService;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e:Lcom/swedbank/mobile/business/cards/CardLimitService;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->b:Ljava/math/BigDecimal;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->b:Ljava/math/BigDecimal;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->c:I

    iget v3, p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->c:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e:Lcom/swedbank/mobile/business/cards/CardLimitService;

    iget-object v3, p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e:Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    iget-object p1, p1, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public final f()Lcom/swedbank/mobile/business/cards/CardLimitValidity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->b:Ljava/math/BigDecimal;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e:Lcom/swedbank/mobile/business/cards/CardLimitService;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardServiceResponse(active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", limitMaximum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", limitCurrency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardService="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->e:Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/cards/CardServiceResponse;->f:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
