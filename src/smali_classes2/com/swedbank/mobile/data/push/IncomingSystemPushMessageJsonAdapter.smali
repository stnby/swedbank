.class public final Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "IncomingSystemPushMessageJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;

.field private final stringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 4
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "challenge"

    const-string v1, "message"

    const-string v2, "controlCode"

    const-string v3, "sessionId"

    .line 15
    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"c\u2026ontrolCode\", \"sessionId\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 18
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "challenge"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String>(St\u2026.emptySet(), \"challenge\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 21
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "sessionId"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<String?>(S\u2026.emptySet(), \"sessionId\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "challenge"

    .line 63
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "message"

    .line 65
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "controlCode"

    .line 67
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "sessionId"

    .line 69
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->d()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 71
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 60
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p2, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;
    .locals 12
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 26
    check-cast v0, Ljava/lang/String;

    .line 31
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v1, 0x0

    move-object v1, v0

    move-object v2, v1

    move-object v4, v2

    const/4 v3, 0x0

    .line 32
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 33
    iget-object v5, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {p1, v5}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 38
    :pswitch_0
    iget-object v3, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x1

    move-object v4, v3

    const/4 v3, 0x1

    goto :goto_0

    .line 36
    :pswitch_1
    iget-object v2, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Non-null value \'controlCode\' was null at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 35
    :pswitch_2
    iget-object v1, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v1, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Non-null value \'message\' was null at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 34
    :pswitch_3
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessageJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Non-null value \'challenge\' was null at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 43
    :pswitch_4
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->j()V

    .line 44
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->q()V

    goto/16 :goto_0

    .line 48
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->f()V

    .line 49
    new-instance p1, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf

    const/4 v11, 0x0

    move-object v5, p1

    invoke-direct/range {v5 .. v11}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    if-eqz v0, :cond_4

    goto :goto_1

    .line 51
    :cond_4
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v1, :cond_5

    goto :goto_2

    .line 52
    :cond_5
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->b()Ljava/lang/String;

    move-result-object v1

    :goto_2
    if-eqz v2, :cond_6

    goto :goto_3

    .line 53
    :cond_6
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->c()Ljava/lang/String;

    move-result-object v2

    :goto_3
    if-eqz v3, :cond_7

    goto :goto_4

    .line 54
    :cond_7
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->d()Ljava/lang/String;

    move-result-object v4

    .line 50
    :goto_4
    invoke-virtual {p1, v0, v1, v2, v4}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(IncomingSystemPushMessage)"

    return-object v0
.end method
