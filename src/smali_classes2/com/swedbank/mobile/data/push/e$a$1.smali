.class final Lcom/swedbank/mobile/data/push/e$a$1;
.super Ljava/lang/Object;
.source "PushRepositoryImpl.kt"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/push/e$a;->a(Lio/reactivex/x;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/tasks/OnCompleteListener<",
        "Lcom/google/firebase/iid/InstanceIdResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/x;


# direct methods
.method constructor <init>(Lio/reactivex/x;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/push/e$a$1;->a:Lio/reactivex/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/tasks/Task;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "Lcom/google/firebase/iid/InstanceIdResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "task"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/iid/InstanceIdResult;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/firebase/iid/InstanceIdResult;->getToken()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    .line 40
    iget-object p1, p0, Lcom/swedbank/mobile/data/push/e$a$1;->a:Lio/reactivex/x;

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No FCM token yet"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/e$a$1;->a:Lio/reactivex/x;

    invoke-interface {v0, p1}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 43
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/data/push/e$a$1;->a:Lio/reactivex/x;

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Firebase instance id task was unsuccessful"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method
