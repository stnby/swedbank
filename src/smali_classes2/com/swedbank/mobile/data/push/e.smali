.class public final Lcom/swedbank/mobile/data/push/e;
.super Ljava/lang/Object;
.source "PushRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/push/h;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/squareup/moshi/n;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;Lcom/a/a/a/f;)V
    .locals 1
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "jsonMapper"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferences"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/push/e;->c:Lcom/squareup/moshi/n;

    const-string p1, "pushServiceStatePreference"

    .line 29
    sget-object v0, Lcom/swedbank/mobile/business/push/i;->a:Lcom/swedbank/mobile/business/push/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/push/i;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/a/a/a/d;

    move-result-object p1

    const-string v0, "appPreferences.getIntege\u2026hServiceState.UNKNOWN.id)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/push/e;->a:Lcom/a/a/a/d;

    const-string p1, "hasPendingPushTokenPreference"

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;

    move-result-object p1

    const-string p2, "appPreferences.getBoolea\u2026ENDING_PUSH_TOKEN, false)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/push/e;->b:Lcom/a/a/a/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/push/e;)Lcom/a/a/a/d;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/data/push/e;->a:Lcom/a/a/a/d;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/business/push/UserPushMessage;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "rawPayload"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/e;->c:Lcom/squareup/moshi/n;

    .line 78
    const-class v1, Lcom/swedbank/mobile/data/push/IncomingUserPushMessage;

    invoke-virtual {v0, v1}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 69
    check-cast p1, Lcom/swedbank/mobile/data/push/IncomingUserPushMessage;

    .line 71
    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/data/push/IncomingUserPushMessage;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/push/UserPushMessage;

    move-result-object p1

    return-object p1

    .line 69
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Invalid user push message"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public a(Lcom/swedbank/mobile/business/push/i;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/swedbank/mobile/data/push/e$d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/push/e$d;-><init>(Lcom/swedbank/mobile/data/push/e;Lcom/swedbank/mobile/business/push/i;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026ceState.set(state.id)\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/w;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    sget-object v0, Lcom/swedbank/mobile/data/push/e$a;->a:Lcom/swedbank/mobile/data/push/e$a;

    check-cast v0, Lio/reactivex/z;

    invoke-static {v0}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object v0

    .line 47
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v2

    const-string v0, "Single\n      .create<Pus\u2026scribeOn(Schedulers.io())"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/swedbank/mobile/data/push/e$b;->a:Lcom/swedbank/mobile/data/push/e$b;

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v3, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/data/l/f;->a(Lio/reactivex/w;ILkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/e;->b:Lcom/a/a/a/d;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/a/a/a/d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/e;->b:Lcom/a/a/a/d;

    .line 51
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "hasPendingPushToken\n      .asObservable()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/push/i;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/push/e;->d()Lio/reactivex/o;

    move-result-object v0

    .line 62
    sget-object v1, Lcom/swedbank/mobile/business/push/i;->a:Lcom/swedbank/mobile/business/push/i;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "observePushServiceState(\u2026PushServiceState.UNKNOWN)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/push/i;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/data/push/e;->a:Lcom/a/a/a/d;

    .line 58
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/swedbank/mobile/data/push/e$c;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/data/push/e;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/data/push/e$c;-><init>(Lcom/swedbank/mobile/data/push/e;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/data/push/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/push/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "serviceState\n      .asOb\u2026(::parsePushServiceState)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
