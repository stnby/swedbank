.class public final Lcom/swedbank/mobile/data/transfer/request/a$b;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/request/a;->a(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "+",
            "Lcom/swedbank/mobile/data/transfer/request/PaymentRequestFetchResponse;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/util/q<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    const/16 v1, 0xa

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/transfer/request/PaymentRequestFetchResponse;

    .line 242
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/request/PaymentRequestFetchResponse;->a()Lcom/swedbank/mobile/data/transfer/request/PaymentRequestResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 245
    new-instance v0, Lcom/swedbank/mobile/business/util/q$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/request/PaymentRequestFetchResponse;->a()Lcom/swedbank/mobile/data/transfer/request/PaymentRequestResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/request/PaymentRequestResponse;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/q$b;-><init>(Ljava/lang/Object;)V

    .line 246
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 247
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/request/PaymentRequestFetchResponse;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 250
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/request/PaymentRequestFetchResponse;->b()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 251
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 252
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 253
    check-cast v1, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    .line 250
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 254
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 250
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/q$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/q$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 255
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 256
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid payment request response"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 116
    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 236
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 238
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 239
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 236
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 240
    :cond_4
    check-cast v0, Ljava/util/List;

    .line 116
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 259
    new-instance v0, Lcom/swedbank/mobile/business/util/q$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/q$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(onError(it.errorMessages().right()))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 117
    :cond_5
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 260
    new-instance v0, Lcom/swedbank/mobile/business/util/q$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/q$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(onError(it.cause.left()))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/request/a$b;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
