.class public final Lcom/swedbank/mobile/data/transfer/request/a;
.super Ljava/lang/Object;
.source "PaymentRequestRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/request/e;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/transfer/request/d;

.field private final b:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/transfer/request/d;Lcom/swedbank/mobile/business/c/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/transfer/request/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRequestService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/request/a;->a:Lcom/swedbank/mobile/data/transfer/request/d;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/request/a;->b:Lcom/swedbank/mobile/business/c/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/transfer/request/a;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/request/a;->b:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/request/a;)Lio/reactivex/w;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/transfer/request/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/request/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/r;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/request/a;->a:Lcom/swedbank/mobile/data/transfer/request/d;

    .line 52
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/request/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/request/a;->b()Ljava/lang/String;

    move-result-object v2

    .line 54
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/request/a;->c()Ljava/lang/String;

    move-result-object v3

    .line 55
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/request/a;->d()Ljava/lang/String;

    move-result-object p1

    .line 51
    invoke-static {v0, v1, v2, v3, p1}, Lcom/swedbank/mobile/data/transfer/request/e;->a(Lcom/swedbank/mobile/data/transfer/request/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 56
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 88
    new-instance v0, Lcom/swedbank/mobile/data/transfer/request/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/transfer/request/a$a;-><init>(Lcom/swedbank/mobile/data/transfer/request/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026r(it.cause.left()))\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/q<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/request/a;->a:Lcom/swedbank/mobile/data/transfer/request/d;

    .line 30
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/transfer/request/e;->a(Lcom/swedbank/mobile/data/transfer/request/d;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 31
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 81
    new-instance v0, Lcom/swedbank/mobile/data/transfer/request/a$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/request/a$b;-><init>()V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026r(it.cause.left()))\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
