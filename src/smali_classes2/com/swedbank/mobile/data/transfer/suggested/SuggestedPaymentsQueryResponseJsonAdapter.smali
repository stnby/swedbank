.class public final Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "SuggestedPaymentsQueryResponseJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final listOfSuggestedPaymentResponseAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 4
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "fetchSuggestedPayments"

    .line 15
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"fetchSuggestedPayments\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 18
    const-class v0, Ljava/util/List;

    check-cast v0, Ljava/lang/reflect/Type;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;

    check-cast v2, Ljava/lang/reflect/Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/moshi/p;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "suggestedPayments"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<List<Sugge\u2026t(), \"suggestedPayments\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;->listOfSuggestedPaymentResponseAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;)V
    .locals 1
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "fetchSuggestedPayments"

    .line 47
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;->listOfSuggestedPaymentResponseAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;->a()Ljava/util/List;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 49
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 44
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p2, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;
    .locals 3
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 23
    move-object v1, v0

    check-cast v1, Ljava/util/List;

    .line 24
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->e()V

    .line 25
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 26
    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {p1, v2}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 27
    :pswitch_0
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponseJsonAdapter;->listOfSuggestedPaymentResponseAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v1, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Non-null value \'suggestedPayments\' was null at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 30
    :pswitch_1
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->j()V

    .line 31
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->q()V

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->f()V

    .line 36
    new-instance p1, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;

    const/4 v2, 0x1

    invoke-direct {p1, v0, v2, v0}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;-><init>(Ljava/util/List;ILkotlin/e/b/g;)V

    if-eqz v1, :cond_2

    goto :goto_1

    .line 38
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;->a()Ljava/util/List;

    move-result-object v1

    .line 37
    :goto_1
    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;->copy(Ljava/util/List;)Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(SuggestedPaymentsQueryResponse)"

    return-object v0
.end method
