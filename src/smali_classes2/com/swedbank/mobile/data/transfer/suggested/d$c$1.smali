.class public final Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/suggested/d$c;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/suggested/d$c;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/transfer/suggested/d$c;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;->a:Lcom/swedbank/mobile/data/transfer/suggested/d$c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v2

    .line 244
    sget-object v3, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    const-string v4, "SUGGESTED_PAYMENT_DATA"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v2

    .line 245
    sget-object v3, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/di;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "SUGGESTED_PAYMENT_DATA.CUSTOMER_ID"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;->a:Lcom/swedbank/mobile/data/transfer/suggested/d$c;

    iget-object v4, v4, Lcom/swedbank/mobile/data/transfer/suggested/d$c;->b:Ljava/lang/String;

    .line 246
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.`is`(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 243
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 248
    sget-object v2, Lcom/siimkinks/sqlitemagic/dj;->a:Lcom/siimkinks/sqlitemagic/dj;

    .line 255
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentsQueryResponse;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 256
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 257
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 258
    check-cast v3, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;

    .line 260
    iget-object v4, p0, Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;->a:Lcom/swedbank/mobile/data/transfer/suggested/d$c;

    iget-object v4, v4, Lcom/swedbank/mobile/data/transfer/suggested/d$c;->b:Ljava/lang/String;

    .line 261
    iget-object v5, p0, Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;->a:Lcom/swedbank/mobile/data/transfer/suggested/d$c;

    iget-object v5, v5, Lcom/swedbank/mobile/data/transfer/suggested/d$c;->a:Lcom/swedbank/mobile/data/transfer/suggested/d;

    invoke-static {v5}, Lcom/swedbank/mobile/data/transfer/suggested/d;->a(Lcom/swedbank/mobile/data/transfer/suggested/d;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v5

    .line 259
    invoke-virtual {v3, v4, v5}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/d/a;)Lcom/swedbank/mobile/data/transfer/suggested/b;

    move-result-object v3

    .line 261
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 262
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 263
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/de$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/de$a;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/de$a;->a()Z

    .line 265
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 267
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/suggested/d$c$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
