.class public final Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;
.super Ljava/lang/Object;
.source "SuggestedPaymentsNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:F

.field private final c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;FLcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "suggestedPaymentCreditorBank"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "suggestedPaymentType"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentData"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a:Ljava/lang/String;

    iput p2, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b:F

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    iput-object p4, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/swedbank/mobile/business/d/a;)Lcom/swedbank/mobile/data/transfer/suggested/b;
    .locals 19
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "customerId"

    move-object/from16 v5, p1

    invoke-static {v5, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cryptoRepository"

    move-object/from16 v2, p2

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v1, v0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    .line 30
    new-instance v18, Lcom/swedbank/mobile/data/transfer/suggested/b;

    .line 31
    invoke-virtual/range {p0 .. p2}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b(Ljava/lang/String;Lcom/swedbank/mobile/business/d/a;)Ljava/lang/String;

    move-result-object v4

    .line 35
    iget-object v6, v0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a:Ljava/lang/String;

    .line 36
    iget v7, v0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b:F

    .line 37
    iget-object v8, v0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    .line 38
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->g()Ljava/lang/String;

    move-result-object v9

    .line 39
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->a()Ljava/lang/String;

    move-result-object v10

    .line 40
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->b()Ljava/lang/String;

    move-result-object v11

    .line 41
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->c()Ljava/math/BigDecimal;

    move-result-object v12

    .line 42
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->d()Ljava/lang/String;

    move-result-object v13

    .line 43
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->e()Ljava/lang/String;

    move-result-object v14

    .line 44
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->f()Ljava/lang/String;

    move-result-object v15

    const/4 v3, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v2, v18

    .line 30
    invoke-direct/range {v2 .. v17}, Lcom/swedbank/mobile/data/transfer/suggested/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    return-object v18
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()F
    .locals 1

    .line 21
    iget v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b:F

    return v0
.end method

.method public final b(Ljava/lang/String;Lcom/swedbank/mobile/business/d/a;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cryptoRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->c()Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 52
    invoke-interface {p2, p1}, Lcom/swedbank/mobile/business/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;FLcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "suggestedPaymentCreditorBank"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "suggestedPaymentType"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentData"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;-><init>(Ljava/lang/String;FLcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b:F

    iget v1, p1, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SuggestedPaymentResponse(suggestedPaymentCreditorBank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", suggestedPaymentRank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", suggestedPaymentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->c:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/suggested/SuggestedPaymentResponse;->d:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
