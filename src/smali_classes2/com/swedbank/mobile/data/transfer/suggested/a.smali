.class public final Lcom/swedbank/mobile/data/transfer/suggested/a;
.super Ljava/lang/Object;
.source "SqliteMagic_SuggestedPaymentData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/transfer/suggested/b;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 124
    iget v2, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 125
    iget v3, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v3, v3, 0xd

    iput v3, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 126
    new-instance v1, Lcom/swedbank/mobile/data/transfer/suggested/b;

    .line 127
    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    move-object v5, v4

    goto :goto_0

    :cond_0
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object v5, v3

    :goto_0
    add-int/lit8 v3, v2, 0x1

    .line 128
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v3, v2, 0x2

    .line 129
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v3, v2, 0x3

    .line 130
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v3, v2, 0x4

    .line 131
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    add-int/lit8 v3, v2, 0x5

    .line 132
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/transfer/suggested/c;->a(I)Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    move-result-object v10

    add-int/lit8 v3, v2, 0x6

    .line 133
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_1

    move-object v11, v4

    goto :goto_1

    :cond_1
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v11, v3

    :goto_1
    add-int/lit8 v3, v2, 0x7

    .line 134
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v12

    if-eqz v12, :cond_2

    move-object v12, v4

    goto :goto_2

    :cond_2
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v12, v3

    :goto_2
    add-int/lit8 v3, v2, 0x8

    .line 135
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v13

    if-eqz v13, :cond_3

    move-object v13, v4

    goto :goto_3

    :cond_3
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v13, v3

    :goto_3
    add-int/lit8 v3, v2, 0x9

    .line 136
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_4

    move-object v14, v4

    goto :goto_4

    :cond_4
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v3

    move-object v14, v3

    :goto_4
    add-int/lit8 v3, v2, 0xa

    .line 137
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-eqz v15, :cond_5

    move-object v15, v4

    goto :goto_5

    :cond_5
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v15, v3

    :goto_5
    add-int/lit8 v3, v2, 0xb

    .line 138
    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v16

    if-eqz v16, :cond_6

    move-object/from16 v16, v4

    goto :goto_6

    :cond_6
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v16, v3

    :goto_6
    add-int/lit8 v2, v2, 0xc

    .line 139
    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_7

    move-object/from16 v17, v4

    goto :goto_7

    :cond_7
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v17, v0

    :goto_7
    move-object v4, v1

    invoke-direct/range {v4 .. v17}, Lcom/swedbank/mobile/data/transfer/suggested/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/transfer/suggested/b;
    .locals 32
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/transfer/suggested/b;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 145
    invoke-virtual/range {p2 .. p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 147
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v3

    :cond_0
    const-string v2, "suggested_payment_data"

    .line 152
    :cond_1
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_a

    .line 154
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 155
    new-instance v2, Lcom/swedbank/mobile/data/transfer/suggested/b;

    .line 156
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v5, v3

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v5, v4

    :goto_0
    add-int/lit8 v4, v1, 0x1

    .line 157
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v4, v1, 0x2

    .line 158
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v4, v1, 0x3

    .line 159
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v4, v1, 0x4

    .line 160
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    add-int/lit8 v4, v1, 0x5

    .line 161
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/transfer/suggested/c;->a(I)Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    move-result-object v10

    add-int/lit8 v4, v1, 0x6

    .line 162
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_3

    move-object v11, v3

    goto :goto_1

    :cond_3
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v11, v4

    :goto_1
    add-int/lit8 v4, v1, 0x7

    .line 163
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v12

    if-eqz v12, :cond_4

    move-object v12, v3

    goto :goto_2

    :cond_4
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v12, v4

    :goto_2
    add-int/lit8 v4, v1, 0x8

    .line 164
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v13

    if-eqz v13, :cond_5

    move-object v13, v3

    goto :goto_3

    :cond_5
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v13, v4

    :goto_3
    add-int/lit8 v4, v1, 0x9

    .line 165
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_6

    move-object v14, v3

    goto :goto_4

    :cond_6
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v4

    move-object v14, v4

    :goto_4
    add-int/lit8 v4, v1, 0xa

    .line 166
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-eqz v15, :cond_7

    move-object v15, v3

    goto :goto_5

    :cond_7
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v15, v4

    :goto_5
    add-int/lit8 v4, v1, 0xb

    .line 167
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v16

    if-eqz v16, :cond_8

    move-object/from16 v16, v3

    goto :goto_6

    :cond_8
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v16, v4

    :goto_6
    add-int/lit8 v1, v1, 0xc

    .line 168
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_9

    :goto_7
    move-object/from16 v17, v3

    goto :goto_8

    :cond_9
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    :goto_8
    move-object v4, v2

    invoke-direct/range {v4 .. v17}, Lcom/swedbank/mobile/data/transfer/suggested/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 170
    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".local_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 171
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".suggested_payment_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_1f

    .line 175
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".customer_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_1e

    .line 179
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".suggested_payment_creditor_bank"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_1d

    .line 183
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".suggested_payment_rank"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_1c

    .line 187
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".suggested_payment_type"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    if-eqz v9, :cond_1b

    .line 191
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, ".sender_account_id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 192
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, ".recipient_name"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    .line 193
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ".recipient_iban"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    .line 194
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, ".amount"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 195
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, ".currency"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    .line 196
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".description"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 197
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".reference_number"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 198
    new-instance v2, Lcom/swedbank/mobile/data/transfer/suggested/b;

    if-eqz v4, :cond_c

    .line 199
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-eqz v15, :cond_b

    goto :goto_9

    :cond_b
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v19, v4

    goto :goto_a

    :cond_c
    :goto_9
    const/16 v19, 0x0

    .line 200
    :goto_a
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 201
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 202
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 203
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v23

    .line 204
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/transfer/suggested/c;->a(I)Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    move-result-object v24

    if-eqz v10, :cond_e

    .line 205
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_d

    goto :goto_b

    :cond_d
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v25, v4

    goto :goto_c

    :cond_e
    :goto_b
    const/16 v25, 0x0

    :goto_c
    if-eqz v11, :cond_10

    .line 206
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_f

    goto :goto_d

    :cond_f
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v26, v4

    goto :goto_e

    :cond_10
    :goto_d
    const/16 v26, 0x0

    :goto_e
    if-eqz v12, :cond_12

    .line 207
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_11

    goto :goto_f

    :cond_11
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v27, v4

    goto :goto_10

    :cond_12
    :goto_f
    const/16 v27, 0x0

    :goto_10
    if-eqz v13, :cond_14

    .line 208
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_13

    goto :goto_11

    :cond_13
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v4

    move-object/from16 v28, v4

    goto :goto_12

    :cond_14
    :goto_11
    const/16 v28, 0x0

    :goto_12
    if-eqz v14, :cond_16

    .line 209
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_15

    goto :goto_13

    :cond_15
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v29, v4

    goto :goto_14

    :cond_16
    :goto_13
    const/16 v29, 0x0

    :goto_14
    if-eqz v3, :cond_18

    .line 210
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_17

    goto :goto_15

    :cond_17
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v30, v3

    goto :goto_16

    :cond_18
    :goto_15
    const/16 v30, 0x0

    :goto_16
    if-eqz v1, :cond_1a

    .line 211
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_19

    goto :goto_17

    :cond_19
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v31, v3

    goto :goto_18

    :cond_1a
    :goto_17
    const/16 v31, 0x0

    :goto_18
    move-object/from16 v18, v2

    invoke-direct/range {v18 .. v31}, Lcom/swedbank/mobile/data/transfer/suggested/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 189
    :cond_1b
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"suggested_payment_data\" required column \"suggested_payment_type\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_1c
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"suggested_payment_data\" required column \"suggested_payment_rank\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_1d
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"suggested_payment_data\" required column \"suggested_payment_creditor_bank\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_1e
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"suggested_payment_data\" required column \"customer_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_1f
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"suggested_payment_data\" required column \"suggested_payment_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/suggested/b;)V
    .locals 3

    .line 58
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 59
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 61
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 62
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->e()F

    move-result v0

    float-to-double v0, v0

    const/4 v2, 0x4

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(ID)V

    .line 63
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->f()Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/transfer/suggested/c;->a(Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;)I

    move-result v0

    int-to-long v0, v0

    const/4 v2, 0x5

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 64
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    .line 65
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    .line 68
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 70
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    .line 71
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 73
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->j()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_3

    const/16 v0, 0x9

    .line 74
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->j()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 76
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const/16 v0, 0xa

    .line 77
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->k()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 79
    :cond_4
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const/16 v0, 0xb

    .line 80
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->l()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 82
    :cond_5
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const/16 v0, 0xc

    .line 83
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/suggested/b;->m()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, v0, p1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    :cond_6
    return-void
.end method
