.class public final Lcom/swedbank/mobile/data/transfer/suggested/d;
.super Ljava/lang/Object;
.source "SuggestedPaymentsRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/suggested/b;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/transfer/suggested/g;

.field private final b:Lcom/swedbank/mobile/business/d/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/transfer/suggested/g;Lcom/swedbank/mobile/business/d/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/transfer/suggested/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "suggestedPaymentsService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cryptoRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/suggested/d;->a:Lcom/swedbank/mobile/data/transfer/suggested/g;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/suggested/d;->b:Lcom/swedbank/mobile/business/d/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/transfer/suggested/d;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/suggested/d;->b:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/suggested/d;->a:Lcom/swedbank/mobile/data/transfer/suggested/g;

    .line 25
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/transfer/suggested/h;->a(Lcom/swedbank/mobile/data/transfer/suggested/g;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 26
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/swedbank/mobile/data/transfer/suggested/d$c;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/transfer/suggested/d$c;-><init>(Lcom/swedbank/mobile/data/transfer/suggested/d;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/suggested/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    const-string v2, "SUGGESTED_PAYMENT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 46
    sget-object v1, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/di;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "SUGGESTED_PAYMENT_DATA.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    const-string v0, "SELECT\n          FROM SU\u2026USTOMER_ID IS customerId)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 48
    new-array v0, v0, [Lcom/siimkinks/sqlitemagic/cb$f;

    .line 49
    sget-object v1, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/di;->f:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/bn;->d()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v1

    const-string v2, "SUGGESTED_PAYMENT_DATA.S\u2026ESTED_PAYMENT_RANK.desc()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 50
    sget-object v1, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/di;->i:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/x;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v1

    const-string v2, "SUGGESTED_PAYMENT_DATA.RECIPIENT_NAME.asc()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 75
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/siimkinks/sqlitemagic/cb$f;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/cb$h;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$e;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 53
    sget-object v0, Lcom/swedbank/mobile/data/transfer/suggested/d$b;->a:Lcom/swedbank/mobile/data/transfer/suggested/d$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM S\u2026ta::toSuggestedPayment) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/j;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/transfer/suggested/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "suggestedPaymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 57
    sget-object v1, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    const-string v2, "SUGGESTED_PAYMENT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 58
    sget-object v1, Lcom/siimkinks/sqlitemagic/di;->a:Lcom/siimkinks/sqlitemagic/di;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/di;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "SUGGESTED_PAYMENT_DATA.SUGGESTED_PAYMENT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 60
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cg;->c()Lio/reactivex/j;

    move-result-object p1

    .line 62
    sget-object v0, Lcom/swedbank/mobile/data/transfer/suggested/d$a;->a:Lcom/swedbank/mobile/data/transfer/suggested/d$a;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/transfer/suggested/e;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/transfer/suggested/e;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM S\u2026Data::toSuggestedPayment)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
