.class public final Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;
.super Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem;
.source "TransferSearchNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentItem"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "bankName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/transfer/search/f;
    .locals 13
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    .line 33
    new-instance v12, Lcom/swedbank/mobile/business/transfer/search/f;

    .line 35
    sget-object v3, Lcom/swedbank/mobile/business/transfer/search/f$a;->a:Lcom/swedbank/mobile/business/transfer/search/f$a;

    .line 36
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->a()Ljava/lang/String;

    move-result-object v4

    .line 37
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->b()Ljava/lang/String;

    move-result-object v5

    .line 38
    iget-object v6, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->a:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xe0

    const/4 v11, 0x0

    move-object v1, v12

    .line 33
    invoke-direct/range {v1 .. v11}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    return-object v12
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "bankName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;

    invoke-direct {v0, p1, p2}, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentItem(bankName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;->b:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
