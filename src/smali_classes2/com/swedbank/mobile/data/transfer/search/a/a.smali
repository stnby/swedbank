.class public final Lcom/swedbank/mobile/data/transfer/search/a/a;
.super Ljava/lang/Object;
.source "SqliteMagic_TransferSearchKeyword_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/transfer/search/a/e;
    .locals 6

    .line 82
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 83
    iget v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 84
    new-instance p1, Lcom/swedbank/mobile/data/transfer/search/a/e;

    .line 85
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    add-int/lit8 v2, v0, 0x1

    .line 86
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v0, 0x2

    .line 87
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    add-int/lit8 v0, v0, 0x3

    .line 88
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-static {p0}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object p0

    invoke-direct {p1, v1, v2, v3, p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;-><init>(Ljava/lang/Long;Ljava/lang/String;ILjava/util/Date;)V

    return-object p1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/transfer/search/a/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/transfer/search/a/e;"
        }
    .end annotation

    .line 94
    invoke-virtual {p2, p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p2, :cond_1

    .line 96
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_0

    return-object v0

    :cond_0
    const-string p2, "transfer_search_keyword"

    .line 101
    :cond_1
    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    if-eqz p3, :cond_3

    .line 103
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 104
    new-instance p2, Lcom/swedbank/mobile/data/transfer/search/a/e;

    .line 105
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result p3

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    add-int/lit8 p3, p1, 0x1

    .line 106
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    add-int/lit8 v1, p1, 0x2

    .line 107
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    add-int/lit8 p1, p1, 0x3

    .line 108
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-static {p0}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object p0

    invoke-direct {p2, v0, p3, v1, p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;-><init>(Ljava/lang/Long;Ljava/lang/String;ILjava/util/Date;)V

    return-object p2

    .line 110
    :cond_3
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".local_id"

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".keyword"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".search_count"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ".last_searched"

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_6

    .line 123
    new-instance p2, Lcom/swedbank/mobile/data/transfer/search/a/e;

    if-eqz p3, :cond_5

    .line 124
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-interface {p0, p3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 125
    :cond_5
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 126
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 127
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-static {p0}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object p0

    invoke-direct {p2, v0, p3, v1, p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;-><init>(Ljava/lang/Long;Ljava/lang/String;ILjava/util/Date;)V

    return-object p2

    .line 121
    :cond_6
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"transfer_search_keyword\" required column \"last_searched\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 117
    :cond_7
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"transfer_search_keyword\" required column \"search_count\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 113
    :cond_8
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"transfer_search_keyword\" required column \"keyword\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lcom/swedbank/mobile/data/transfer/search/a/e;)Ljava/lang/Long;
    .locals 0

    .line 77
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;->b()Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/transfer/search/a/e;)V
    .locals 2

    .line 37
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x308ae217

    if-eq v0, v1, :cond_1

    const v1, 0x714bd66f

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "local_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "keyword"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, -0x1

    :goto_1
    packed-switch p2, :pswitch_data_0

    goto :goto_2

    .line 45
    :pswitch_0
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/transfer/search/a/e;->c()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 39
    :pswitch_1
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/transfer/search/a/e;->b()Ljava/lang/Long;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 42
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/transfer/search/a/e;->b()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    invoke-interface {p0, p1, p2, p3}, Landroidx/k/a/f;->a(IJ)V

    goto :goto_2

    .line 40
    :cond_3
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "local_id column is null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/search/a/e;)V
    .locals 3

    .line 62
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 63
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/search/a/e;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/search/a/e;->d()I

    move-result v0

    int-to-long v0, v0

    const/4 v2, 0x2

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 65
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/search/a/e;->e()Ljava/util/Date;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/util/Date;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 p1, 0x3

    invoke-interface {p0, p1, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/transfer/search/a/e;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/transfer/search/a/e;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->b()V

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;->b()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "local_id"

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "keyword"

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "search_count"

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "last_searched"

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/search/a/e;->e()Ljava/util/Date;

    move-result-object p0

    invoke-static {p0}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/util/Date;)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/search/a/e;)Z
    .locals 4

    .line 51
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x308ae217

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x714bd66f

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "local_id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "keyword"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 57
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " is not unique"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_0
    return v3

    .line 53
    :pswitch_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/search/a/e;->b()Ljava/lang/Long;

    move-result-object p0

    if-nez p0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/search/a/e;)V
    .locals 3

    .line 70
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 71
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/search/a/e;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/search/a/e;->d()I

    move-result v0

    int-to-long v0, v0

    const/4 v2, 0x2

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 73
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/search/a/e;->e()Ljava/util/Date;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/util/Date;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 p1, 0x3

    invoke-interface {p0, p1, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    return-void
.end method
