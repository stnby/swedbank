.class public final Lcom/swedbank/mobile/data/transfer/search/a;
.super Ljava/lang/Object;
.source "TransferSearchRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/search/e;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/transfer/search/c;

.field private final b:Lcom/squareup/moshi/n;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/transfer/search/c;Lcom/squareup/moshi/n;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/transfer/search/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/moshi/n;
        .annotation runtime Ljavax/inject/Named;
            value = "graphql_json_mapper"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transferSearchService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "graphQLJsonMapper"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/search/a;->a:Lcom/swedbank/mobile/data/transfer/search/c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/search/a;->b:Lcom/squareup/moshi/n;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/search/a;->a:Lcom/swedbank/mobile/data/transfer/search/c;

    .line 25
    new-instance v1, Lcom/swedbank/mobile/data/transfer/search/TransferSearchRequestData;

    invoke-direct {v1, p2, p1}, Lcom/swedbank/mobile/data/transfer/search/TransferSearchRequestData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/search/a;->b:Lcom/squareup/moshi/n;

    .line 24
    invoke-static {v0, v1, p1}, Lcom/swedbank/mobile/data/transfer/search/d;->a(Lcom/swedbank/mobile/data/transfer/search/c;Lcom/swedbank/mobile/data/transfer/search/TransferSearchRequestData;Lcom/squareup/moshi/n;)Lio/reactivex/w;

    move-result-object p1

    .line 29
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 35
    new-instance p2, Lcom/swedbank/mobile/data/transfer/search/a$a;

    invoke-direct {p2}, Lcom/swedbank/mobile/data/transfer/search/a$a;-><init>()V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026r(it.cause.left()))\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
