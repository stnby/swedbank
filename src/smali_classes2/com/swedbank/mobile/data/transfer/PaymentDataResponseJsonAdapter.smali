.class public final Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "PaymentDataResponseJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 8
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v1, "creditorName"

    const-string v2, "creditorAccountIban"

    const-string v3, "amount"

    const-string v4, "currency"

    const-string v5, "description"

    const-string v6, "referenceNumber"

    const-string v7, "debtorAccountId"

    .line 15
    filled-new-array/range {v1 .. v7}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"c\u2026mber\", \"debtorAccountId\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 18
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "creditorName"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String?>(S\u2026ptySet(), \"creditorName\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 21
    const-class v0, Ljava/math/BigDecimal;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<BigDecimal\u2026ons.emptySet(), \"amount\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 95
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "creditorName"

    .line 96
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "creditorAccountIban"

    .line 98
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "amount"

    .line 100
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 101
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->c()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "currency"

    .line 102
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 103
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "description"

    .line 104
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 105
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "referenceNumber"

    .line 106
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 107
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "debtorAccountId"

    .line 108
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 109
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->g()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 93
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p2, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
    .locals 28
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "reader"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 26
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 30
    check-cast v2, Ljava/math/BigDecimal;

    .line 40
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v4, 0x0

    move-object v7, v2

    move-object v5, v3

    move-object v9, v5

    move-object v11, v9

    move-object v13, v11

    move-object v15, v13

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    .line 41
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v16

    if-eqz v16, :cond_0

    move-object/from16 v17, v3

    .line 42
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {v1, v3}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v3

    const/16 v16, 0x1

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_1

    .line 68
    :pswitch_0
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v15, v3

    move-object/from16 v3, v17

    const/4 v14, 0x1

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v13, v3

    move-object/from16 v3, v17

    const/4 v12, 0x1

    goto :goto_0

    .line 60
    :pswitch_2
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v11, v3

    move-object/from16 v3, v17

    const/4 v10, 0x1

    goto :goto_0

    .line 56
    :pswitch_3
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v9, v3

    move-object/from16 v3, v17

    const/4 v8, 0x1

    goto :goto_0

    .line 52
    :pswitch_4
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    move-object v7, v3

    move-object/from16 v3, v17

    const/4 v6, 0x1

    goto :goto_0

    .line 48
    :pswitch_5
    iget-object v2, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v5, v2

    move-object/from16 v3, v17

    const/4 v2, 0x1

    goto :goto_0

    .line 44
    :pswitch_6
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/PaymentDataResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x1

    goto :goto_0

    .line 73
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->j()V

    .line 74
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->q()V

    :goto_1
    move-object/from16 v3, v17

    goto :goto_0

    :cond_0
    move-object/from16 v17, v3

    .line 78
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->f()V

    .line 79
    new-instance v16, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x7f

    const/16 v27, 0x0

    move-object/from16 v18, v16

    invoke-direct/range {v18 .. v27}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    if-eqz v4, :cond_1

    goto :goto_2

    .line 81
    :cond_1
    invoke-virtual/range {v16 .. v16}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v17, v3

    :goto_2
    if-eqz v2, :cond_2

    :goto_3
    move-object/from16 v18, v5

    goto :goto_4

    .line 82
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->b()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :goto_4
    if-eqz v6, :cond_3

    :goto_5
    move-object/from16 v19, v7

    goto :goto_6

    .line 83
    :cond_3
    invoke-virtual/range {v16 .. v16}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->c()Ljava/math/BigDecimal;

    move-result-object v7

    goto :goto_5

    :goto_6
    if-eqz v8, :cond_4

    :goto_7
    move-object/from16 v20, v9

    goto :goto_8

    .line 84
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->d()Ljava/lang/String;

    move-result-object v9

    goto :goto_7

    :goto_8
    if-eqz v10, :cond_5

    :goto_9
    move-object/from16 v21, v11

    goto :goto_a

    .line 85
    :cond_5
    invoke-virtual/range {v16 .. v16}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->e()Ljava/lang/String;

    move-result-object v11

    goto :goto_9

    :goto_a
    if-eqz v12, :cond_6

    :goto_b
    move-object/from16 v22, v13

    goto :goto_c

    .line 86
    :cond_6
    invoke-virtual/range {v16 .. v16}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->f()Ljava/lang/String;

    move-result-object v13

    goto :goto_b

    :goto_c
    if-eqz v14, :cond_7

    :goto_d
    move-object/from16 v23, v15

    goto :goto_e

    .line 87
    :cond_7
    invoke-virtual/range {v16 .. v16}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->g()Ljava/lang/String;

    move-result-object v15

    goto :goto_d

    .line 80
    :goto_e
    invoke-virtual/range {v16 .. v23}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->a(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    move-result-object v1

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(PaymentDataResponse)"

    return-object v0
.end method
