.class public final Lcom/swedbank/mobile/data/transfer/payment/PaymentError;
.super Ljava/lang/Object;
.source "PaymentNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/transfer/payment/PaymentError$a;
    }
.end annotation


# static fields
# The value of this static final field might be set in the static constructor
.field public static final a:Ljava/lang/String; = "\nfragment paymentErrorData on PaymentError {\n  message\n  referenceField\n}"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/transfer/payment/PaymentError$a;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentError$a;

    const-string v0, "\nfragment paymentErrorData on PaymentError {\n  message\n  referenceField\n}"

    .line 168
    sput-object v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "referenceField"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/transfer/payment/execution/g;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 154
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).toUpperCase()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string v2, "AMOUNT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v2, "CURRENCY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    :goto_0
    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->c:Lcom/swedbank/mobile/business/transfer/payment/a;

    goto :goto_2

    :sswitch_2
    const-string v2, "CREDITOR_NAME"

    .line 155
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->d:Lcom/swedbank/mobile/business/transfer/payment/a;

    goto :goto_2

    :sswitch_3
    const-string v2, "DESCRIPTION"

    .line 155
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->f:Lcom/swedbank/mobile/business/transfer/payment/a;

    goto :goto_2

    :sswitch_4
    const-string v2, "DEBTOR_ACCOUNT_ID"

    .line 155
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->b:Lcom/swedbank/mobile/business/transfer/payment/a;

    goto :goto_2

    :sswitch_5
    const-string v2, "CREDITOR_ACCOUNT_IBAN"

    .line 155
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->e:Lcom/swedbank/mobile/business/transfer/payment/a;

    goto :goto_2

    :sswitch_6
    const-string v2, "REFERENCE_NUMBER"

    .line 155
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->g:Lcom/swedbank/mobile/business/transfer/payment/a;

    goto :goto_2

    .line 162
    :cond_0
    :goto_1
    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->a:Lcom/swedbank/mobile/business/transfer/payment/a;

    .line 153
    :goto_2
    new-instance v2, Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    invoke-direct {v2, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/g;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/a;)V

    return-object v2

    .line 155
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_0
    .sparse-switch
        -0x7b4bd923 -> :sswitch_6
        -0x74dd3545 -> :sswitch_5
        -0x3ffbdd4a -> :sswitch_4
        0x198917dc -> :sswitch_3
        0x2a171e4e -> :sswitch_2
        0x50f1e011 -> :sswitch_1
        0x734d4458 -> :sswitch_0
    .end sparse-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->d:Ljava/lang/String;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->d:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentError(message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", referenceField="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
