.class final Lcom/swedbank/mobile/data/transfer/payment/j$o;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->c(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/j;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$o;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$o;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lcom/swedbank/mobile/business/transfer/payment/execution/n;
    .locals 6
    .param p1    # Lcom/swedbank/mobile/data/transfer/payment/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->e()Lcom/swedbank/mobile/data/transfer/payment/g;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/data/transfer/payment/k;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/g;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 236
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state received from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 227
    :pswitch_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j$o;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    .line 229
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->i()Ljava/lang/String;

    move-result-object p1

    .line 230
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$o;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/j;->a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/squareup/moshi/n;

    move-result-object v1

    .line 425
    const-class v2, Ljava/util/List;

    check-cast v2, Ljava/lang/reflect/Type;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/reflect/Type;

    const/4 v4, 0x0

    const-class v5, Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    check-cast v5, Ljava/lang/reflect/Type;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/squareup/moshi/p;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Type;

    invoke-virtual {v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v1

    .line 426
    invoke-virtual {v1, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_0

    goto :goto_0

    .line 231
    :cond_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    .line 227
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    goto :goto_2

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j$o;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    .line 234
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->j()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-static {p1}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    :goto_1
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 232
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    goto :goto_2

    .line 224
    :pswitch_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 225
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$o;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/j;->a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/squareup/moshi/n;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/data/transfer/payment/d;->a(Lcom/squareup/moshi/n;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object p1

    .line 223
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    :goto_2
    return-object v1

    .line 224
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/d;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$o;->a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    move-result-object p1

    return-object p1
.end method
