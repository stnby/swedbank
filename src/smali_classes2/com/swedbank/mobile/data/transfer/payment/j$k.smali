.class final Lcom/swedbank/mobile/data/transfer/payment/j$k;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->c(Ljava/lang/String;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$k;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/j;
    .locals 6
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;)",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j$k;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    .line 425
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v0

    .line 434
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    const-string v2, "PAYMENT_EXECUTION_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v0

    .line 435
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bu;->g:Lcom/siimkinks/sqlitemagic/bn;

    const-string v2, "PAYMENT_EXECUTION_DATA.CREATED"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 440
    invoke-static {}, Lorg/threeten/bp/Instant;->now()Lorg/threeten/bp/Instant;

    move-result-object v2

    .line 439
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v3, Lorg/threeten/bp/temporal/TemporalUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5, v3}, Lorg/threeten/bp/Instant;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;

    move-result-object v2

    .line 438
    invoke-virtual {v2}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 441
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/bn;->d(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.lessThan(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/av$b;->b()Lio/reactivex/w;

    move-result-object v0

    .line 432
    invoke-virtual {v0}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "(DELETE\n          FROM P\u2026         .ignoreElement()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object v1

    check-cast v1, Lio/reactivex/n;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object v0

    .line 285
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/j$k$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$k$1;-><init>(Lkotlin/k;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->f(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$k;->a(Lkotlin/k;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
