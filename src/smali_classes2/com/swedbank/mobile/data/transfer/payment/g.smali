.class public final enum Lcom/swedbank/mobile/data/transfer/payment/g;
.super Ljava/lang/Enum;
.source "PaymentExecutionData.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/data/transfer/payment/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/data/transfer/payment/g;

.field public static final enum b:Lcom/swedbank/mobile/data/transfer/payment/g;

.field public static final enum c:Lcom/swedbank/mobile/data/transfer/payment/g;

.field public static final enum d:Lcom/swedbank/mobile/data/transfer/payment/g;

.field private static final synthetic e:[Lcom/swedbank/mobile/data/transfer/payment/g;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/data/transfer/payment/g;

    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/g;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    .line 44
    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/data/transfer/payment/g;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/data/transfer/payment/g;->a:Lcom/swedbank/mobile/data/transfer/payment/g;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/g;

    const-string v2, "EXECUTING"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/data/transfer/payment/g;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/data/transfer/payment/g;->b:Lcom/swedbank/mobile/data/transfer/payment/g;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/g;

    const-string v2, "SUCCESS"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/data/transfer/payment/g;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/data/transfer/payment/g;->c:Lcom/swedbank/mobile/data/transfer/payment/g;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/g;

    const-string v2, "FAIL"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/data/transfer/payment/g;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/data/transfer/payment/g;->d:Lcom/swedbank/mobile/data/transfer/payment/g;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/data/transfer/payment/g;->e:[Lcom/swedbank/mobile/data/transfer/payment/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/data/transfer/payment/g;->f:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/data/transfer/payment/g;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/transfer/payment/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/transfer/payment/g;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/data/transfer/payment/g;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/data/transfer/payment/g;->e:[Lcom/swedbank/mobile/data/transfer/payment/g;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/data/transfer/payment/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/data/transfer/payment/g;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/swedbank/mobile/data/transfer/payment/g;->f:I

    return v0
.end method
