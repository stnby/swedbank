.class final Lcom/swedbank/mobile/data/transfer/payment/j$j;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->c(Ljava/lang/String;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/n<",
        "+",
        "Lkotlin/k<",
        "+",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
        "+",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/j;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$j;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$j;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lio/reactivex/j;
    .locals 4
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$j;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    .line 278
    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$j;->b:Ljava/lang/String;

    .line 279
    sget-object v0, Lcom/swedbank/mobile/data/transfer/payment/g;->d:Lcom/swedbank/mobile/data/transfer/payment/g;

    .line 425
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v1

    .line 432
    sget-object v2, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    const-string v3, "PAYMENT_EXECUTION_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v1

    .line 433
    sget-object v2, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bu;->f:Lcom/siimkinks/sqlitemagic/bv;

    invoke-static {v2, v0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    .line 434
    invoke-virtual {v0}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v0

    const-string v1, "UPDATE\n      TABLE PAYME\u2026ON_DATA.STATUS to status)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bu;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "PAYMENT_EXECUTION_DATA.UUID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 436
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 437
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object p1

    .line 431
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dx$d;->b()Lio/reactivex/w;

    move-result-object p1

    .line 430
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    .line 280
    invoke-static {}, Lio/reactivex/j;->a()Lio/reactivex/j;

    move-result-object v0

    check-cast v0, Lio/reactivex/n;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$j;->a(Ljava/lang/Throwable;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
