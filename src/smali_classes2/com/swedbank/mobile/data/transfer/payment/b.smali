.class public final Lcom/swedbank/mobile/data/transfer/payment/b;
.super Ljava/lang/Object;
.source "PaymentDataComponent.kt"


# direct methods
.method public static final a(Landroid/content/Context;)Lcom/swedbank/mobile/data/transfer/payment/a;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "$this$providePaymentDataComponent"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/swedbank/mobile/data/transfer/payment/c;

    .line 9
    invoke-interface {p0}, Lcom/swedbank/mobile/data/transfer/payment/c;->b()Lcom/swedbank/mobile/data/transfer/payment/a;

    move-result-object p0

    return-object p0

    .line 8
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.data.transfer.payment.PaymentDataComponentProvider"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
