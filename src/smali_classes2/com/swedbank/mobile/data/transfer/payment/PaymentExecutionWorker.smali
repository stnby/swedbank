.class public final Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;
.super Landroidx/work/RxWorker;
.source "PaymentExecutionWorker.kt"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/work/WorkerParameters;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "appContext"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workerParams"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1, p2}, Landroidx/work/RxWorker;-><init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->c:Landroid/content/Context;

    .line 19
    invoke-virtual {p2}, Landroidx/work/WorkerParameters;->b()Landroidx/work/e;

    move-result-object p1

    const-string p2, "payment_execution_uuid"

    invoke-virtual {p1, p2}, Landroidx/work/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->b:Ljava/lang/String;

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.business.transfer.payment.execution.PaymentExecutionUuid /* = kotlin.String */"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)Landroid/content/Context;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->c:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Landroidx/work/ListenableWorker$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;-><init>(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.defer<Result> {\n \u2026          }\n        }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
