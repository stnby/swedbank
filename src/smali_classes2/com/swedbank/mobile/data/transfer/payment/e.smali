.class public final Lcom/swedbank/mobile/data/transfer/payment/e;
.super Ljava/lang/Object;
.source "PaymentExecutionData.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/data/transfer/payment/g;)I
    .locals 1
    .param p0    # Lcom/swedbank/mobile/data/transfer/payment/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "paymentExecutionStatus"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/payment/g;->a()I

    move-result p0

    return p0
.end method

.method public static final a(Ljava/lang/Integer;)Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
    .locals 8
    .param p0    # Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 64
    :cond_0
    invoke-static {}, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->values()[Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v1

    .line 70
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_4

    aget-object v5, v1, v4

    .line 65
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->getPriorityLevel()I

    move-result v6

    if-nez p0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_3

    move-object v0, v5

    goto :goto_3

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    :goto_3
    if-eqz v0, :cond_5

    goto :goto_4

    :cond_5
    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->NORMAL:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    :goto_4
    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/squareup/moshi/n;)Lcom/swedbank/mobile/data/transfer/payment/d;
    .locals 16
    .param p0    # Lcom/swedbank/mobile/business/transfer/payment/execution/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "$this$toPaymentExecutionData"

    move-object/from16 v2, p0

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "jsonMapper"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->b()Ljava/lang/String;

    move-result-object v4

    .line 39
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->c()Ljava/lang/String;

    move-result-object v5

    .line 40
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v6

    .line 41
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v1

    .line 67
    const-class v2, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {v0, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const-string v0, "jsonMapper.adapter(T::class.java).toJson(this)"

    invoke-static {v10, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/d;

    const/4 v3, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x3b1

    const/4 v15, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v15}, Lcom/swedbank/mobile/data/transfer/payment/d;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    return-object v0
.end method

.method public static final a(I)Lcom/swedbank/mobile/data/transfer/payment/g;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 53
    invoke-static {}, Lcom/swedbank/mobile/data/transfer/payment/g;->values()[Lcom/swedbank/mobile/data/transfer/payment/g;

    move-result-object v0

    .line 68
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 54
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/transfer/payment/g;->a()I

    move-result v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    sget-object v4, Lcom/swedbank/mobile/data/transfer/payment/g;->a:Lcom/swedbank/mobile/data/transfer/payment/g;

    :goto_3
    return-object v4
.end method

.method public static final a(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    if-eqz p0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->getPriorityLevel()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method
