.class public final Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;
.super Ljava/lang/Object;
.source "PaymentNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "signWithPin"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/network/GeneralResponseError;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;Ljava/util/List;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;Ljava/util/List;)V
    .locals 0
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/network/GeneralResponseError;",
            ">;)V"
        }
    .end annotation

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->b:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;Ljava/util/List;ILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    .line 137
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 138
    move-object p2, v0

    check-cast p2, Ljava/util/List;

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;Ljava/util/List;)Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/network/GeneralResponseError;",
            ">;)",
            "Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;

    invoke-direct {v0, p1, p2}, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;Ljava/util/List;)V

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/network/GeneralResponseError;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->b:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->b:Ljava/util/List;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->b:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->b:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SignWithPinResponse(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
