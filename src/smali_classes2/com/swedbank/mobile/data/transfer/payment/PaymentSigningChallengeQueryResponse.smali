.class public final Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;
.super Ljava/lang/Object;
.source "PaymentNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "generateSigningChallenge"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/network/GeneralResponseError;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    iput-object p4, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;ILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 121
    move-object p1, v0

    check-cast p1, Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 122
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 123
    move-object p3, v0

    check-cast p3, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 124
    move-object p4, v0

    check-cast p4, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;)Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/network/GeneralResponseError;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/data/network/GeneralResponseError;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentSigningChallengeQueryResponse(challengeCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", signingMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d:Lcom/swedbank/mobile/data/network/GeneralResponseError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
