.class final Lcom/swedbank/mobile/data/transfer/payment/j$r;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$r;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$r;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/j$r;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/transfer/payment/j$a;)Lcom/swedbank/mobile/business/transfer/payment/execution/n;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/data/transfer/payment/j$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 379
    sget-object v0, Lcom/swedbank/mobile/data/transfer/payment/j$a$c;->a:Lcom/swedbank/mobile/data/transfer/payment/j$a$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j$r;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$r;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    goto :goto_1

    .line 380
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;

    if-eqz v0, :cond_2

    .line 381
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j$r;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 382
    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 425
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 426
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 427
    check-cast v2, Ljava/lang/String;

    .line 383
    new-instance v3, Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    .line 385
    sget-object v4, Lcom/swedbank/mobile/business/transfer/payment/a;->a:Lcom/swedbank/mobile/business/transfer/payment/a;

    .line 383
    invoke-direct {v3, v2, v4}, Lcom/swedbank/mobile/business/transfer/payment/execution/g;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/a;)V

    .line 385
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 428
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 380
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    :goto_1
    return-object p1

    .line 387
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received illegal PaymentExecutionStatusCheckResult "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/j$a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$r;->a(Lcom/swedbank/mobile/data/transfer/payment/j$a;)Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    move-result-object p1

    return-object p1
.end method
