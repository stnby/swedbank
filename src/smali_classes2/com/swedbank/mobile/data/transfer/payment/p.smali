.class public final Lcom/swedbank/mobile/data/transfer/payment/p;
.super Ljava/lang/Object;
.source "SqliteMagic_PaymentExecutionData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/transfer/payment/d;
    .locals 14

    .line 129
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 130
    iget v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v1, v1, 0xa

    iput v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 131
    new-instance p1, Lcom/swedbank/mobile/data/transfer/payment/d;

    .line 132
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v3, v2

    goto :goto_0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v3, v1

    :goto_0
    add-int/lit8 v1, v0, 0x1

    .line 133
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v1, v0, 0x2

    .line 134
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v5, v2

    goto :goto_1

    :cond_1
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    :goto_1
    add-int/lit8 v1, v0, 0x3

    .line 135
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, v2

    goto :goto_2

    :cond_2
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(Ljava/lang/Integer;)Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v1

    move-object v6, v1

    :goto_2
    add-int/lit8 v1, v0, 0x4

    .line 136
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(I)Lcom/swedbank/mobile/data/transfer/payment/g;

    move-result-object v7

    add-int/lit8 v1, v0, 0x5

    .line 137
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    add-int/lit8 v1, v0, 0x6

    .line 138
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v1, v0, 0x7

    .line 139
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_3

    move-object v11, v2

    goto :goto_3

    :cond_3
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    :goto_3
    add-int/lit8 v1, v0, 0x8

    .line 140
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v12

    if-eqz v12, :cond_4

    move-object v12, v2

    goto :goto_4

    :cond_4
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v12, v1

    :goto_4
    add-int/lit8 v0, v0, 0x9

    .line 141
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object v13, v2

    goto :goto_5

    :cond_5
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p0

    move-object v13, p0

    :goto_5
    move-object v2, p1

    invoke-direct/range {v2 .. v13}, Lcom/swedbank/mobile/data/transfer/payment/d;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/transfer/payment/d;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/transfer/payment/d;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 147
    invoke-virtual/range {p2 .. p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 149
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v3

    :cond_0
    const-string v2, "payment_execution_data"

    .line 154
    :cond_1
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_8

    .line 156
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 157
    new-instance v2, Lcom/swedbank/mobile/data/transfer/payment/d;

    .line 158
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v5, v3

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v5, v4

    :goto_0
    add-int/lit8 v4, v1, 0x1

    .line 159
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v4, v1, 0x2

    .line 160
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v7, v3

    goto :goto_1

    :cond_3
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v7, v4

    :goto_1
    add-int/lit8 v4, v1, 0x3

    .line 161
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v8, v3

    goto :goto_2

    :cond_4
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(Ljava/lang/Integer;)Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v4

    move-object v8, v4

    :goto_2
    add-int/lit8 v4, v1, 0x4

    .line 162
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(I)Lcom/swedbank/mobile/data/transfer/payment/g;

    move-result-object v9

    add-int/lit8 v4, v1, 0x5

    .line 163
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    add-int/lit8 v4, v1, 0x6

    .line 164
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    add-int/lit8 v4, v1, 0x7

    .line 165
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v13

    if-eqz v13, :cond_5

    move-object v13, v3

    goto :goto_3

    :cond_5
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v13, v4

    :goto_3
    add-int/lit8 v4, v1, 0x8

    .line 166
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_6

    move-object v14, v3

    goto :goto_4

    :cond_6
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v14, v4

    :goto_4
    add-int/lit8 v1, v1, 0x9

    .line 167
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_7

    :goto_5
    move-object v15, v3

    goto :goto_6

    :cond_7
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    :goto_6
    move-object v4, v2

    invoke-direct/range {v4 .. v15}, Lcom/swedbank/mobile/data/transfer/payment/d;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 169
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".local_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 170
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".uuid"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_18

    .line 174
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".payment_token"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 175
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".priority"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 176
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".status"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_17

    .line 180
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".created"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    if-eqz v9, :cond_16

    .line 184
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, ".raw_input_json"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_15

    .line 188
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, ".result_payment_id"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    .line 189
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ".result_raw_structured_errors"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    .line 190
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".result_raw_errors"

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 191
    new-instance v2, Lcom/swedbank/mobile/data/transfer/payment/d;

    if-eqz v4, :cond_a

    .line 192
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->isNull(I)Z

    move-result v13

    if-eqz v13, :cond_9

    goto :goto_7

    :cond_9
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v14, v4

    goto :goto_8

    :cond_a
    :goto_7
    move-object v14, v3

    .line 193
    :goto_8
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    if-eqz v6, :cond_c

    .line 194
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_b

    goto :goto_9

    :cond_b
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v16, v4

    goto :goto_a

    :cond_c
    :goto_9
    move-object/from16 v16, v3

    :goto_a
    if-eqz v7, :cond_e

    .line 195
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_d

    goto :goto_b

    :cond_d
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(Ljava/lang/Integer;)Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v4

    move-object/from16 v17, v4

    goto :goto_c

    :cond_e
    :goto_b
    move-object/from16 v17, v3

    .line 196
    :goto_c
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(I)Lcom/swedbank/mobile/data/transfer/payment/g;

    move-result-object v18

    .line 197
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    .line 198
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    if-eqz v11, :cond_10

    .line 199
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_f

    goto :goto_d

    :cond_f
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v22, v4

    goto :goto_e

    :cond_10
    :goto_d
    move-object/from16 v22, v3

    :goto_e
    if-eqz v12, :cond_12

    .line 200
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_11

    goto :goto_f

    :cond_11
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v23, v4

    goto :goto_10

    :cond_12
    :goto_f
    move-object/from16 v23, v3

    :goto_10
    if-eqz v1, :cond_14

    .line 201
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_13

    goto :goto_11

    :cond_13
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_14
    :goto_11
    move-object/from16 v24, v3

    move-object v13, v2

    invoke-direct/range {v13 .. v24}, Lcom/swedbank/mobile/data/transfer/payment/d;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 186
    :cond_15
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"payment_execution_data\" required column \"raw_input_json\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_16
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"payment_execution_data\" required column \"created\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_17
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"payment_execution_data\" required column \"status\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_18
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"payment_execution_data\" required column \"uuid\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/transfer/payment/d;)V
    .locals 2

    .line 52
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x36f3bb

    if-eq v0, v1, :cond_1

    const v1, 0x714bd66f

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "local_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "uuid"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, -0x1

    :goto_1
    packed-switch p2, :pswitch_data_0

    goto :goto_2

    .line 60
    :pswitch_0
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/transfer/payment/d;->b()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 54
    :pswitch_1
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/transfer/payment/d;->a()Ljava/lang/Long;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 57
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/transfer/payment/d;->a()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    invoke-interface {p0, p1, p2, p3}, Landroidx/k/a/f;->a(IJ)V

    goto :goto_2

    .line 55
    :cond_3
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "local_id column is null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/payment/d;)V
    .locals 3

    .line 77
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 78
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 79
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 80
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 82
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    .line 83
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    :cond_1
    const/4 v0, 0x4

    .line 85
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->e()Lcom/swedbank/mobile/data/transfer/payment/g;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(Lcom/swedbank/mobile/data/transfer/payment/g;)I

    move-result v1

    int-to-long v1, v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    const/4 v0, 0x5

    .line 86
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->f()J

    move-result-wide v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    const/4 v0, 0x6

    .line 87
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x7

    .line 89
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 91
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/16 v0, 0x8

    .line 92
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 94
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const/16 v0, 0x9

    .line 95
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->j()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, v0, p1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    :cond_4
    return-void
.end method

.method public static b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/transfer/payment/d;)V
    .locals 3

    .line 101
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 102
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 103
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 104
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 106
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    .line 107
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    :cond_1
    const/4 v0, 0x4

    .line 109
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->e()Lcom/swedbank/mobile/data/transfer/payment/g;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/e;->a(Lcom/swedbank/mobile/data/transfer/payment/g;)I

    move-result v1

    int-to-long v1, v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    const/4 v0, 0x5

    .line 110
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->f()J

    move-result-wide v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    const/4 v0, 0x6

    .line 111
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x7

    .line 113
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 115
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/16 v0, 0x8

    .line 116
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 118
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const/16 v0, 0x9

    .line 119
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->j()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, v0, p1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
