.class public final Lcom/swedbank/mobile/data/transfer/payment/j;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/payment/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/transfer/payment/j$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/ordering/h;

.field private final b:Lcom/squareup/moshi/n;

.field private final c:Lcom/squareup/moshi/n;

.field private final d:Landroidx/work/o;

.field private final e:Lcom/swedbank/mobile/business/f/a;

.field private final f:Lcom/swedbank/mobile/data/transfer/payment/n;

.field private final g:Lcom/swedbank/mobile/business/general/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/ordering/h;Lcom/squareup/moshi/n;Lcom/squareup/moshi/n;Landroidx/work/o;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/data/transfer/payment/n;Lcom/swedbank/mobile/business/general/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/ordering/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/moshi/n;
        .annotation runtime Ljavax/inject/Named;
            value = "graphql_json_mapper"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroidx/work/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/data/transfer/payment/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/general/a;
        .annotation runtime Ljavax/inject/Named;
            value = "instant_payment_poll_data"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "systemPushMessageStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "graphQLJsonMapper"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentService"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instantPaymentPollData"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->a:Lcom/swedbank/mobile/data/ordering/h;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->b:Lcom/squareup/moshi/n;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->c:Lcom/squareup/moshi/n;

    iput-object p4, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->d:Landroidx/work/o;

    iput-object p5, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->e:Lcom/swedbank/mobile/business/f/a;

    iput-object p6, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    iput-object p7, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->g:Lcom/swedbank/mobile/business/general/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/squareup/moshi/n;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->b:Lcom/squareup/moshi/n;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/transfer/payment/j;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/transfer/payment/j;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method private final a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .line 549
    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->INSTANT:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 550
    invoke-static {p0}, Lcom/swedbank/mobile/data/transfer/payment/j;->c(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/swedbank/mobile/data/transfer/payment/n;

    move-result-object v0

    .line 555
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/transfer/payment/o;->d(Lcom/swedbank/mobile/data/transfer/payment/n;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 554
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 556
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/j$c;

    invoke-direct {v1}, Lcom/swedbank/mobile/data/transfer/payment/j$c;-><init>()V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "flatMap {\n  when (it) {\n\u2026r(it.cause.left()))\n  }\n}"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "checkPaymentExecutionSta\u2026)\n        .toObservable()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->g:Lcom/swedbank/mobile/business/general/a;

    .line 564
    new-instance v2, Lcom/swedbank/mobile/data/transfer/payment/j$p;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/transfer/payment/j$p;-><init>(Lcom/swedbank/mobile/business/general/a;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->l(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 565
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/y;->a()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/y;->b()Ljava/util/concurrent/TimeUnit;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/o;->c(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "repeatWhen { it.flatMap \u2026ime, pollData.delay.unit)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 375
    sget-object v1, Lcom/swedbank/mobile/data/transfer/payment/j$q;->a:Lcom/swedbank/mobile/data/transfer/payment/j$q;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 376
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 377
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/j$r;

    invoke-direct {v1, p1, p3, p2}, Lcom/swedbank/mobile/data/transfer/payment/j$r;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "checkPaymentExecutionSta\u2026t\")\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 391
    :cond_1
    new-instance p2, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    invoke-direct {p2, p1, p3}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    .line 566
    invoke-static {p2}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "Single.just(this)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/transfer/payment/j;)Landroidx/work/o;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->d:Landroidx/work/o;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/swedbank/mobile/data/transfer/payment/n;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/squareup/moshi/n;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->c:Lcom/squareup/moshi/n;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$s;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$s;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026nsert().execute()\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/e;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->a:Lcom/swedbank/mobile/data/ordering/h;

    const-string v1, "SIGNING_REQUEST"

    .line 143
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/ordering/h;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    .line 144
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/o;->d(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 146
    sget-object v1, Lcom/swedbank/mobile/data/transfer/payment/j$l;->a:Lcom/swedbank/mobile/data/transfer/payment/j$l;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 156
    sget-object v1, Lcom/swedbank/mobile/data/transfer/payment/j$m;->a:Lcom/swedbank/mobile/data/transfer/payment/j$m;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/data/transfer/payment/l;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/transfer/payment/l;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "systemPushMessageStream\n\u2026cSigningChallenge::Error)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    const/4 v1, 0x0

    .line 425
    move-object v10, v1

    check-cast v10, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 426
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    .line 427
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v3

    .line 428
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v4

    .line 429
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v5

    .line 430
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v6

    .line 431
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v7

    .line 432
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v8

    .line 433
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v9

    move-object v2, v1

    .line 426
    invoke-direct/range {v2 .. v10}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    .line 50
    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->e:Lcom/swedbank/mobile/business/f/a;

    const-string v2, "feature_transfer_instant"

    invoke-interface {p1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    .line 51
    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->c:Lcom/squareup/moshi/n;

    .line 48
    invoke-static {v0, v1, p1, v2}, Lcom/swedbank/mobile/data/transfer/payment/o;->a(Lcom/swedbank/mobile/data/transfer/payment/n;Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;ZLcom/squareup/moshi/n;)Lio/reactivex/w;

    move-result-object p1

    .line 52
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 435
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/payment/j$b;-><init>()V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)Lio/reactivex/w;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/r;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    .line 457
    new-instance v10, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    .line 458
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v2

    .line 459
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v3

    .line 460
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v4

    .line 461
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v5

    .line 462
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v6

    .line 463
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v7

    .line 464
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v8

    move-object v1, v10

    move-object v9, p2

    .line 457
    invoke-direct/range {v1 .. v9}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    .line 103
    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->c:Lcom/squareup/moshi/n;

    .line 101
    invoke-static {v0, v10, p1}, Lcom/swedbank/mobile/data/transfer/payment/o;->b(Lcom/swedbank/mobile/data/transfer/payment/n;Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;Lcom/squareup/moshi/n;)Lio/reactivex/w;

    move-result-object p1

    .line 104
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 466
    new-instance p2, Lcom/swedbank/mobile/data/transfer/payment/j$v;

    invoke-direct {p2}, Lcom/swedbank/mobile/data/transfer/payment/j$v;-><init>()V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/r;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    .line 123
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/transfer/payment/o;->b(Lcom/swedbank/mobile/data/transfer/payment/n;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 124
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 472
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$n;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/payment/j$n;-><init>()V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "password"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    .line 176
    invoke-static {v0, p1, p2}, Lcom/swedbank/mobile/data/transfer/payment/o;->a(Lcom/swedbank/mobile/data/transfer/payment/n;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 179
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 484
    new-instance p2, Lcom/swedbank/mobile/data/transfer/payment/j$u;

    invoke-direct {p2}, Lcom/swedbank/mobile/data/transfer/payment/j$u;-><init>()V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$t;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$t;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026  )\n            }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    const/4 v1, 0x0

    .line 441
    move-object v10, v1

    check-cast v10, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 442
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    .line 443
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v3

    .line 444
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v4

    .line 445
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v5

    .line 446
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v6

    .line 447
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v7

    .line 448
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v8

    .line 449
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v9

    move-object v2, v1

    .line 442
    invoke-direct/range {v2 .. v10}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    .line 79
    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->e:Lcom/swedbank/mobile/business/f/a;

    const-string v2, "feature_transfer_instant"

    invoke-interface {p1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    .line 80
    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->c:Lcom/squareup/moshi/n;

    .line 77
    invoke-static {v0, v1, p1, v2}, Lcom/swedbank/mobile/data/transfer/payment/o;->b(Lcom/swedbank/mobile/data/transfer/payment/n;Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;ZLcom/squareup/moshi/n;)Lio/reactivex/w;

    move-result-object p1

    .line 81
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 451
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$d;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/payment/j$d;-><init>()V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j;->f:Lcom/swedbank/mobile/data/transfer/payment/n;

    .line 159
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/transfer/payment/o;->c(Lcom/swedbank/mobile/data/transfer/payment/n;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 160
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 478
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/payment/j$e;-><init>()V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final c(Ljava/lang/String;)Lio/reactivex/j;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentExecutionUuid"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 496
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 504
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    const-string v2, "PAYMENT_EXECUTION_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 505
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bu;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "PAYMENT_EXECUTION_DATA.UUID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 506
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 507
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 503
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 502
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v0

    .line 501
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cg;->c()Lio/reactivex/j;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM P\u2026          .runQueryOnce()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    sget-object v1, Lcom/swedbank/mobile/data/transfer/payment/j$f;->a:Lcom/swedbank/mobile/data/transfer/payment/j$f;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/k;)Lio/reactivex/j;

    move-result-object v0

    .line 244
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/j$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/transfer/payment/j$g;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    .line 276
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/j$j;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$j;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->e(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    .line 282
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$k;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/transfer/payment/j$k;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "getPaymentExecutionData(\u2026rn { result }\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 215
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    const-string v2, "PAYMENT_EXECUTION_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 216
    sget-object v1, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bu;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "PAYMENT_EXECUTION_DATA.UUID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->b()Ljava/lang/String;

    move-result-object v2

    .line 490
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    sget-object v2, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bu;->f:Lcom/siimkinks/sqlitemagic/bv;

    const-string v3, "PAYMENT_EXECUTION_DATA.STATUS"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    sget-object v3, Lcom/swedbank/mobile/data/transfer/payment/g;->c:Lcom/swedbank/mobile/data/transfer/payment/g;

    .line 491
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    sget-object v3, Lcom/siimkinks/sqlitemagic/bu;->a:Lcom/siimkinks/sqlitemagic/bu;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/bu;->f:Lcom/siimkinks/sqlitemagic/bv;

    const-string v4, "PAYMENT_EXECUTION_DATA.STATUS"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    sget-object v4, Lcom/swedbank/mobile/data/transfer/payment/g;->d:Lcom/swedbank/mobile/data/transfer/payment/g;

    .line 492
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.`is`(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 493
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/bb;->b(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.or(expr)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 494
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.and(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 495
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 219
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cg;->b()Lio/reactivex/o;

    move-result-object v0

    .line 221
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/j$o;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$o;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 239
    invoke-virtual {p1}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM P\u2026          .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
