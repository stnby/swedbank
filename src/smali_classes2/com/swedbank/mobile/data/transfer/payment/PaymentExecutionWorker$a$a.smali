.class final Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$a;
.super Ljava/lang/Object;
.source "PaymentExecutionWorker.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a()Lio/reactivex/aa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .line 30
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Critical error while executing payment: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;

    iget-object v1, v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->a(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$a;->a(Ljava/lang/Throwable;)V

    return-void
.end method
