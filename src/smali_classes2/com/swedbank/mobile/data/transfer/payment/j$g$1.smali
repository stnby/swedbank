.class final Lcom/swedbank/mobile/data/transfer/payment/j$g$1;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j$g;->a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/j$g;

.field final synthetic b:Lcom/swedbank/mobile/data/transfer/payment/d;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/j$g;Lcom/swedbank/mobile/data/transfer/payment/d;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->a:Lcom/swedbank/mobile/data/transfer/payment/j$g;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->b:Lcom/swedbank/mobile/data/transfer/payment/d;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;)Lio/reactivex/j;
    .locals 25
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ")",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "executionResult"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 258
    iget-object v2, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->a:Lcom/swedbank/mobile/data/transfer/payment/j$g;

    iget-object v2, v2, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    .line 259
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    if-eqz v2, :cond_0

    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->b:Lcom/swedbank/mobile/data/transfer/payment/d;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 260
    sget-object v8, Lcom/swedbank/mobile/data/transfer/payment/g;->c:Lcom/swedbank/mobile/data/transfer/payment/g;

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    .line 261
    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;->a()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x36f

    const/16 v16, 0x0

    .line 259
    invoke-static/range {v3 .. v16}, Lcom/swedbank/mobile/data/transfer/payment/d;->a(Lcom/swedbank/mobile/data/transfer/payment/d;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/data/transfer/payment/d;

    move-result-object v2

    goto/16 :goto_2

    .line 262
    :cond_0
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    if-eqz v2, :cond_1

    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->b:Lcom/swedbank/mobile/data/transfer/payment/d;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 263
    sget-object v8, Lcom/swedbank/mobile/data/transfer/payment/g;->d:Lcom/swedbank/mobile/data/transfer/payment/g;

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 264
    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;->b()Ljava/util/List;

    move-result-object v2

    iget-object v13, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->a:Lcom/swedbank/mobile/data/transfer/payment/j$g;

    iget-object v13, v13, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-static {v13}, Lcom/swedbank/mobile/data/transfer/payment/j;->a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/squareup/moshi/n;

    move-result-object v13

    .line 425
    const-class v14, Ljava/util/List;

    invoke-virtual {v13, v14}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v13

    invoke-virtual {v13, v2}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    const-string v2, "jsonMapper.adapter(T::class.java).toJson(this)"

    invoke-static {v13, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v14, 0x0

    const/16 v15, 0x2ef

    const/16 v16, 0x0

    .line 262
    invoke-static/range {v3 .. v16}, Lcom/swedbank/mobile/data/transfer/payment/d;->a(Lcom/swedbank/mobile/data/transfer/payment/d;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/data/transfer/payment/d;

    move-result-object v2

    goto :goto_2

    .line 265
    :cond_1
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    if-eqz v2, :cond_6

    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->b:Lcom/swedbank/mobile/data/transfer/payment/d;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 266
    sget-object v8, Lcom/swedbank/mobile/data/transfer/payment/g;->d:Lcom/swedbank/mobile/data/transfer/payment/g;

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 267
    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v2

    .line 427
    instance-of v14, v2, Lcom/swedbank/mobile/business/util/e$b;

    const/4 v15, 0x0

    if-eqz v14, :cond_3

    check-cast v2, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 270
    move-object v14, v2

    check-cast v14, Ljava/util/Collection;

    invoke-interface {v14}, Ljava/util/Collection;->isEmpty()Z

    move-result v14

    xor-int/lit8 v14, v14, 0x1

    if-eqz v14, :cond_2

    goto :goto_0

    :cond_2
    move-object v2, v15

    :goto_0
    if-eqz v2, :cond_4

    move-object/from16 v16, v2

    check-cast v16, Ljava/lang/Iterable;

    const-string v2, "\n"

    move-object/from16 v17, v2

    check-cast v17, Ljava/lang/CharSequence;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x3e

    const/16 v24, 0x0

    invoke-static/range {v16 .. v24}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    goto :goto_1

    .line 428
    :cond_3
    instance-of v14, v2, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v14, :cond_5

    check-cast v2, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    :cond_4
    :goto_1
    move-object v14, v15

    const/16 v15, 0x1ef

    const/16 v16, 0x0

    .line 265
    invoke-static/range {v3 .. v16}, Lcom/swedbank/mobile/data/transfer/payment/d;->a(Lcom/swedbank/mobile/data/transfer/payment/d;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/data/transfer/payment/d;

    move-result-object v2

    .line 431
    :goto_2
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/da$b;->a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lcom/siimkinks/sqlitemagic/da$b;

    move-result-object v2

    .line 430
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/da$b;->c()Lio/reactivex/b;

    move-result-object v2

    const-string v3, "update().observe()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-static {v1, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object v1

    check-cast v1, Lio/reactivex/n;

    invoke-virtual {v2, v1}, Lio/reactivex/b;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object v1

    return-object v1

    .line 268
    :cond_5
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 265
    :cond_6
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
