.class public final Lcom/swedbank/mobile/data/transfer/payment/m;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/transfer/payment/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/ordering/h;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroidx/work/o;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/payment/n;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/ordering/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroidx/work/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/payment/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->a:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->b:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->c:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->d:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->e:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->f:Ljavax/inject/Provider;

    .line 40
    iput-object p7, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->g:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/transfer/payment/m;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/ordering/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroidx/work/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/payment/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;)",
            "Lcom/swedbank/mobile/data/transfer/payment/m;"
        }
    .end annotation

    .line 55
    new-instance v8, Lcom/swedbank/mobile/data/transfer/payment/m;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/data/transfer/payment/m;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/transfer/payment/j;
    .locals 9

    .line 45
    new-instance v8, Lcom/swedbank/mobile/data/transfer/payment/j;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/data/ordering/h;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/moshi/n;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/moshi/n;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroidx/work/o;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/f/a;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/data/transfer/payment/n;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/m;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/business/general/a;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/data/transfer/payment/j;-><init>(Lcom/swedbank/mobile/data/ordering/h;Lcom/squareup/moshi/n;Lcom/squareup/moshi/n;Landroidx/work/o;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/data/transfer/payment/n;Lcom/swedbank/mobile/business/general/a;)V

    return-object v8
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/payment/m;->a()Lcom/swedbank/mobile/data/transfer/payment/j;

    move-result-object v0

    return-object v0
.end method
