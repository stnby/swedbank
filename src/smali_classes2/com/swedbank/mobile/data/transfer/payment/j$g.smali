.class final Lcom/swedbank/mobile/data/transfer/payment/j$g;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->c(Ljava/lang/String;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lio/reactivex/j;
    .locals 17
    .param p1    # Lcom/swedbank/mobile/data/transfer/payment/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/transfer/payment/d;",
            ")",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v15, p1

    const-string v1, "executionData"

    invoke-static {v15, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    iget-object v1, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/j;->a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/squareup/moshi/n;

    move-result-object v1

    invoke-virtual {v15, v1}, Lcom/swedbank/mobile/data/transfer/payment/d;->a(Lcom/squareup/moshi/n;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v14

    .line 246
    iget-object v1, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    sget-object v6, Lcom/swedbank/mobile/data/transfer/payment/g;->b:Lcom/swedbank/mobile/data/transfer/payment/g;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x3ef

    const/16 v16, 0x0

    move-object/from16 v1, p1

    move-object v15, v14

    move-object/from16 v14, v16

    invoke-static/range {v1 .. v14}, Lcom/swedbank/mobile/data/transfer/payment/d;->a(Lcom/swedbank/mobile/data/transfer/payment/d;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/data/transfer/payment/g;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/data/transfer/payment/d;

    move-result-object v1

    .line 426
    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/da$b;->a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lcom/siimkinks/sqlitemagic/da$b;

    move-result-object v1

    .line 425
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/da$b;->c()Lio/reactivex/b;

    move-result-object v1

    const-string v2, "update().observe()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    .line 250
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->c()Ljava/lang/String;

    move-result-object v3

    .line 251
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v4

    .line 427
    invoke-static {v2}, Lcom/swedbank/mobile/data/transfer/payment/j;->c(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/swedbank/mobile/data/transfer/payment/n;

    move-result-object v5

    .line 432
    invoke-static {v5, v3}, Lcom/swedbank/mobile/data/transfer/payment/o;->a(Lcom/swedbank/mobile/data/transfer/payment/n;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v3

    .line 433
    sget-object v5, Lcom/swedbank/mobile/data/network/ab;->b:Lio/reactivex/c/h;

    if-eqz v5, :cond_0

    .line 431
    invoke-virtual {v3, v5}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v3

    const-string v5, "paymentService\n      .ex\u2026workLayerErrorSupplier())"

    invoke-static {v3, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 441
    new-instance v5, Lcom/swedbank/mobile/data/transfer/payment/j$h;

    invoke-direct {v5, v2, v4, v15, v4}, Lcom/swedbank/mobile/data/transfer/payment/j$h;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v3, v5}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v2

    const-string v3, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 433
    :cond_0
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type io.reactivex.functions.Function<kotlin.Throwable, out com.swedbank.mobile.data.network.NetworkResponse<T>>"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 253
    :cond_1
    iget-object v2, v0, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    .line 254
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/transfer/payment/d;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v12

    .line 448
    invoke-static {v2}, Lcom/swedbank/mobile/data/transfer/payment/j;->c(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/swedbank/mobile/data/transfer/payment/n;

    move-result-object v13

    .line 457
    new-instance v14, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    .line 458
    invoke-virtual {v15}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v4

    .line 459
    invoke-virtual {v15}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v5

    .line 460
    invoke-virtual {v15}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v6

    .line 461
    invoke-virtual {v15}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v7

    .line 462
    invoke-virtual {v15}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v8

    .line 463
    invoke-virtual {v15}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v9

    .line 464
    invoke-virtual {v15}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v10

    move-object v3, v14

    move-object v11, v12

    .line 457
    invoke-direct/range {v3 .. v11}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    .line 466
    invoke-static {v2}, Lcom/swedbank/mobile/data/transfer/payment/j;->d(Lcom/swedbank/mobile/data/transfer/payment/j;)Lcom/squareup/moshi/n;

    move-result-object v3

    .line 455
    invoke-static {v13, v14, v3}, Lcom/swedbank/mobile/data/transfer/payment/o;->a(Lcom/swedbank/mobile/data/transfer/payment/n;Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;Lcom/squareup/moshi/n;)Lio/reactivex/w;

    move-result-object v3

    .line 467
    sget-object v4, Lcom/swedbank/mobile/data/network/ab;->b:Lio/reactivex/c/h;

    if-eqz v4, :cond_2

    .line 454
    invoke-virtual {v3, v4}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v3

    const-string v4, "paymentService\n      .ex\u2026workLayerErrorSupplier())"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 471
    new-instance v4, Lcom/swedbank/mobile/data/transfer/payment/j$i;

    invoke-direct {v4, v2, v12, v15, v12}, Lcom/swedbank/mobile/data/transfer/payment/j$i;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v2

    const-string v3, "flatMap {\n  when (it) {\n\u2026Error) as Single<O>\n  }\n}"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    :goto_0
    check-cast v2, Lio/reactivex/aa;

    invoke-virtual {v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v1

    .line 257
    new-instance v2, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;

    move-object v4, v15

    move-object/from16 v3, p1

    invoke-direct {v2, v0, v3, v4}, Lcom/swedbank/mobile/data/transfer/payment/j$g$1;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j$g;Lcom/swedbank/mobile/data/transfer/payment/d;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v1

    return-object v1

    .line 467
    :cond_2
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type io.reactivex.functions.Function<kotlin.Throwable, out com.swedbank.mobile.data.network.NetworkResponse<T>>"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/d;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$g;->a(Lcom/swedbank/mobile/data/transfer/payment/d;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
