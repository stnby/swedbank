.class final Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;
.super Ljava/lang/Object;
.source "PaymentExecutionWorker.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->b()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lio/reactivex/aa<",
        "+TT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/aa;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/aa<",
            "+",
            "Landroidx/work/ListenableWorker$a;",
            ">;"
        }
    .end annotation

    .line 22
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Payment execution job started for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->a(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;

    invoke-static {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->b(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)Landroid/content/Context;

    move-result-object v0

    .line 24
    invoke-static {v0}, Lcom/swedbank/mobile/data/transfer/payment/b;->a(Landroid/content/Context;)Lcom/swedbank/mobile/data/transfer/payment/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/data/transfer/payment/a;->b()Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 28
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->a(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 29
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$a;-><init>(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$b;-><init>(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    check-cast v0, Lio/reactivex/aa;

    goto :goto_1

    .line 38
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 39
    invoke-static {}, Landroidx/work/ListenableWorker$a;->a()Landroidx/work/ListenableWorker$a;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    check-cast v0, Lio/reactivex/aa;

    :goto_1
    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a()Lio/reactivex/aa;

    move-result-object v0

    return-object v0
.end method
