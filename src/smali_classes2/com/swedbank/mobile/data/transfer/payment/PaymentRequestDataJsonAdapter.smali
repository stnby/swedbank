.class public final Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "PaymentRequestDataJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final nullablePaymentPriorityAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;"
        }
    .end annotation
.end field

.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 9
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v1, "debtorAccountId"

    const-string v2, "amount"

    const-string v3, "currency"

    const-string v4, "creditorName"

    const-string v5, "creditorAccountIban"

    const-string v6, "description"

    const-string v7, "referenceNumber"

    const-string v8, "paymentPriority"

    .line 16
    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"d\u2026mber\", \"paymentPriority\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 19
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "debtorAccountId"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String?>(S\u2026Set(), \"debtorAccountId\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 22
    const-class v0, Ljava/math/BigDecimal;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<BigDecimal\u2026ons.emptySet(), \"amount\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 25
    const-class v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "paymentPriority"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<PaymentPri\u2026Set(), \"paymentPriority\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullablePaymentPriorityAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 106
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "debtorAccountId"

    .line 107
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 108
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "amount"

    .line 109
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->b()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "currency"

    .line 111
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 112
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "creditorName"

    .line 113
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 114
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "creditorAccountIban"

    .line 115
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 116
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "description"

    .line 117
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 118
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "referenceNumber"

    .line 119
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 120
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "paymentPriority"

    .line 121
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 122
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullablePaymentPriorityAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->h()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 123
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 104
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p2, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;
    .locals 40
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "reader"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 30
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 32
    move-object v4, v2

    check-cast v4, Ljava/math/BigDecimal;

    .line 44
    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 46
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v5, 0x0

    move-object/from16 v17, v2

    move-object v7, v3

    move-object v9, v7

    move-object v11, v9

    move-object v13, v11

    move-object v15, v13

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    .line 47
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v18

    if-eqz v18, :cond_0

    move-object/from16 v19, v3

    .line 48
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {v1, v3}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v3

    const/16 v18, 0x1

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_1

    .line 78
    :pswitch_0
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullablePaymentPriorityAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-object/from16 v17, v3

    move-object/from16 v3, v19

    const/16 v16, 0x1

    goto :goto_0

    .line 74
    :pswitch_1
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v15, v3

    move-object/from16 v3, v19

    const/4 v14, 0x1

    goto :goto_0

    .line 70
    :pswitch_2
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v13, v3

    move-object/from16 v3, v19

    const/4 v12, 0x1

    goto :goto_0

    .line 66
    :pswitch_3
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v11, v3

    move-object/from16 v3, v19

    const/4 v10, 0x1

    goto :goto_0

    .line 62
    :pswitch_4
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v9, v3

    move-object/from16 v3, v19

    const/4 v8, 0x1

    goto :goto_0

    .line 58
    :pswitch_5
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v7, v3

    move-object/from16 v3, v19

    const/4 v6, 0x1

    goto :goto_0

    .line 54
    :pswitch_6
    iget-object v2, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    move-object v4, v2

    move-object/from16 v3, v19

    const/4 v2, 0x1

    goto :goto_0

    .line 50
    :pswitch_7
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestDataJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x1

    goto :goto_0

    .line 83
    :pswitch_8
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->j()V

    .line 84
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->q()V

    :goto_1
    move-object/from16 v3, v19

    goto/16 :goto_0

    :cond_0
    move-object/from16 v19, v3

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->f()V

    .line 89
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0xff

    const/16 v30, 0x0

    move-object/from16 v20, v1

    invoke-direct/range {v20 .. v30}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;ILkotlin/e/b/g;)V

    .line 90
    new-instance v3, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;

    if-eqz v5, :cond_1

    move-object/from16 v32, v19

    goto :goto_2

    .line 91
    :cond_1
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->a()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v32, v5

    :goto_2
    if-eqz v2, :cond_2

    :goto_3
    move-object/from16 v33, v4

    goto :goto_4

    .line 92
    :cond_2
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->b()Ljava/math/BigDecimal;

    move-result-object v4

    goto :goto_3

    :goto_4
    if-eqz v6, :cond_3

    :goto_5
    move-object/from16 v34, v7

    goto :goto_6

    .line 93
    :cond_3
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->c()Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :goto_6
    if-eqz v8, :cond_4

    :goto_7
    move-object/from16 v35, v9

    goto :goto_8

    .line 94
    :cond_4
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->d()Ljava/lang/String;

    move-result-object v9

    goto :goto_7

    :goto_8
    if-eqz v10, :cond_5

    :goto_9
    move-object/from16 v36, v11

    goto :goto_a

    .line 95
    :cond_5
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->e()Ljava/lang/String;

    move-result-object v11

    goto :goto_9

    :goto_a
    if-eqz v12, :cond_6

    :goto_b
    move-object/from16 v37, v13

    goto :goto_c

    .line 96
    :cond_6
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->f()Ljava/lang/String;

    move-result-object v13

    goto :goto_b

    :goto_c
    if-eqz v14, :cond_7

    :goto_d
    move-object/from16 v38, v15

    goto :goto_e

    .line 97
    :cond_7
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->g()Ljava/lang/String;

    move-result-object v15

    goto :goto_d

    :goto_e
    if-eqz v16, :cond_8

    :goto_f
    move-object/from16 v39, v17

    goto :goto_10

    .line 98
    :cond_8
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;->h()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v17

    goto :goto_f

    :goto_10
    move-object/from16 v31, v3

    .line 90
    invoke-direct/range {v31 .. v39}, Lcom/swedbank/mobile/data/transfer/payment/PaymentRequestData;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    return-object v3

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(PaymentRequestData)"

    return-object v0
.end method
