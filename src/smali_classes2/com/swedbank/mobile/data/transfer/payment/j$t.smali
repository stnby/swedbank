.class final Lcom/swedbank/mobile/data/transfer/payment/j$t;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/j;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$t;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$t;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .line 199
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j$t;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->b()Ljava/lang/String;

    move-result-object v0

    .line 200
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scheduling payment work for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 429
    new-instance v1, Landroidx/work/j$a;

    const-class v2, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;

    invoke-direct {v1, v2}, Landroidx/work/j$a;-><init>(Ljava/lang/Class;)V

    .line 202
    invoke-static {v0}, Lcom/swedbank/mobile/data/transfer/payment/h;->b(Ljava/lang/String;)Landroidx/work/e;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/work/j$a;->a(Landroidx/work/e;)Landroidx/work/p$a;

    move-result-object v1

    check-cast v1, Landroidx/work/j$a;

    .line 203
    invoke-virtual {v1}, Landroidx/work/j$a;->e()Landroidx/work/p;

    move-result-object v1

    .line 204
    check-cast v1, Landroidx/work/j;

    .line 205
    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$t;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-static {v2}, Lcom/swedbank/mobile/data/transfer/payment/j;->b(Lcom/swedbank/mobile/data/transfer/payment/j;)Landroidx/work/o;

    move-result-object v2

    .line 206
    invoke-static {v0}, Lcom/swedbank/mobile/data/transfer/payment/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    sget-object v3, Landroidx/work/f;->b:Landroidx/work/f;

    .line 205
    invoke-virtual {v2, v0, v3, v1}, Landroidx/work/o;->a(Ljava/lang/String;Landroidx/work/f;Landroidx/work/j;)Landroidx/work/k;

    return-void
.end method
