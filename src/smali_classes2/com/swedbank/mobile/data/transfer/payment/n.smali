.class public interface abstract Lcom/swedbank/mobile/data/transfer/payment/n;
.super Ljava/lang/Object;
.source "PaymentService.kt"


# virtual methods
.method public abstract a(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/NotSignedPaymentExecutionResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract c(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/SignedPaymentExecutionResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract d(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract e(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningStartResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract f(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract g(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningStatusResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract h(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/SignWithPinResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract i(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionStatusCheckResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method
