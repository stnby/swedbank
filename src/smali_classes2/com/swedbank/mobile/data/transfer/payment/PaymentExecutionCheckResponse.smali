.class public final Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;
.super Ljava/lang/Object;
.source "PaymentNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "checkPayment"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;Ljava/util/List;)V
    .locals 1
    .param p2    # Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;)V"
        }
    .end annotation

    const-string v0, "availablePaymentPriorities"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentErrors"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a:Z

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->c:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;Ljava/util/List;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    .line 62
    invoke-static {}, Lcom/swedbank/mobile/data/transfer/payment/i;->a()Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    move-result-object p2

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 63
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p3

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;-><init>(ZLcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;ZLcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;Ljava/util/List;ILjava/lang/Object;)Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->c:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a(ZLcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;Ljava/util/List;)Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(ZLcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;Ljava/util/List;)Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;
    .locals 1
    .param p2    # Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;)",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "availablePaymentPriorities"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentErrors"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;

    invoke-direct {v0, p1, p2, p3}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;-><init>(ZLcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;Ljava/util/List;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a:Z

    return v0
.end method

.method public final b()Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->c:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    iget-object v3, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->c:Ljava/util/List;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->c:Ljava/util/List;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->c:Ljava/util/List;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentExecutionCheckResponse(needsSigning="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", availablePaymentPriorities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentErrors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionCheckResponse;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
