.class public final Lcom/swedbank/mobile/data/transfer/payment/j$b;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "+",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    const/16 v1, 0xa

    if-eqz v0, :cond_17

    check-cast p1, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;

    .line 248
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a()Ljava/util/List;

    move-result-object v0

    .line 252
    check-cast v0, Ljava/lang/Iterable;

    .line 253
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 254
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 255
    invoke-virtual {v5}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "PAYMENT_PRIORITY"

    invoke-static {v5, v6, v4}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v5

    xor-int/2addr v4, v5

    if-eqz v4, :cond_0

    .line 257
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 259
    check-cast v2, Ljava/lang/Iterable;

    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 261
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 262
    check-cast v3, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 250
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 263
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 250
    check-cast v0, Ljava/util/Collection;

    .line 248
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 264
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b()Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;->a()Lcom/swedbank/mobile/data/transfer/payment/PaymentCreditorCorrection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCreditorCorrection;->b()Ljava/util/List;

    move-result-object v0

    .line 268
    check-cast v0, Ljava/lang/Iterable;

    .line 269
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 270
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 271
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PAYMENT_PRIORITY"

    invoke-static {v6, v7, v4}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    xor-int/2addr v6, v4

    if-eqz v6, :cond_3

    .line 273
    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 274
    :cond_4
    check-cast v3, Ljava/util/List;

    .line 275
    check-cast v3, Ljava/lang/Iterable;

    .line 276
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v3, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 277
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 278
    check-cast v5, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 266
    invoke-virtual {v5}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 279
    :cond_5
    check-cast v0, Ljava/util/List;

    .line 266
    check-cast v0, Ljava/util/Collection;

    .line 264
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 280
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c()Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;->c()Ljava/util/List;

    move-result-object v0

    .line 284
    check-cast v0, Ljava/lang/Iterable;

    .line 285
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 286
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 287
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PAYMENT_PRIORITY"

    invoke-static {v6, v7, v4}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    xor-int/2addr v6, v4

    if-eqz v6, :cond_6

    .line 289
    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 290
    :cond_7
    check-cast v3, Ljava/util/List;

    .line 291
    check-cast v3, Ljava/lang/Iterable;

    .line 292
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v3, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 293
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 294
    check-cast v5, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 282
    invoke-virtual {v5}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 295
    :cond_8
    check-cast v0, Ljava/util/List;

    .line 282
    check-cast v0, Ljava/util/Collection;

    .line 280
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 296
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d()Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;->b()Ljava/util/List;

    move-result-object v0

    .line 300
    check-cast v0, Ljava/lang/Iterable;

    .line 301
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 302
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_9
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 303
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PAYMENT_PRIORITY"

    invoke-static {v6, v7, v4}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    xor-int/2addr v6, v4

    if-eqz v6, :cond_9

    .line 305
    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 306
    :cond_a
    check-cast v3, Ljava/util/List;

    .line 307
    check-cast v3, Ljava/lang/Iterable;

    .line 308
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v3, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 309
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 310
    check-cast v5, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 298
    invoke-virtual {v5}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 311
    :cond_b
    check-cast v0, Ljava/util/List;

    .line 298
    check-cast v0, Ljava/util/Collection;

    .line 296
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 312
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e()Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;->b()Ljava/util/List;

    move-result-object v0

    .line 316
    check-cast v0, Ljava/lang/Iterable;

    .line 317
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 318
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_c
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 319
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PAYMENT_PRIORITY"

    invoke-static {v6, v7, v4}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    xor-int/2addr v6, v4

    if-eqz v6, :cond_c

    .line 321
    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 322
    :cond_d
    check-cast v3, Ljava/util/List;

    .line 323
    check-cast v3, Ljava/lang/Iterable;

    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v3, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 325
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 326
    check-cast v3, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 314
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 327
    :cond_e
    check-cast v0, Ljava/util/List;

    .line 314
    check-cast v0, Ljava/util/Collection;

    .line 312
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 328
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 330
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b()Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;->a()Lcom/swedbank/mobile/data/transfer/payment/PaymentCreditorCorrection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCreditorCorrection;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_11

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_f

    const/4 v5, 0x1

    goto :goto_a

    :cond_f
    const/4 v5, 0x0

    :goto_a
    if-eqz v5, :cond_10

    goto :goto_b

    :cond_10
    move-object v0, v3

    :goto_b
    move-object v6, v0

    goto :goto_c

    :cond_11
    move-object v6, v3

    .line 331
    :goto_c
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c()Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;->a()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 333
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c()Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;->a()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 342
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c()Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_12

    .line 332
    new-instance v7, Lcom/swedbank/mobile/business/transfer/payment/execution/q;

    invoke-direct {v7, v0, v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/q;-><init>(Ljava/math/BigDecimal;Ljava/lang/String;)V

    goto :goto_d

    .line 348
    :cond_12
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 339
    :cond_13
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_14
    move-object v7, v3

    .line 353
    :goto_d
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d()Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;->a()Lcom/swedbank/mobile/data/transfer/payment/PaymentBank;

    move-result-object v0

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentBank;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_15

    const/4 v1, 0x1

    :cond_15
    if-eqz v1, :cond_16

    move-object v3, v0

    :cond_16
    move-object v8, v3

    .line 354
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e()Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;->a()Ljava/util/List;

    move-result-object v9

    .line 355
    move-object v10, v2

    check-cast v10, Ljava/util/List;

    .line 329
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;

    move-object v5, p1

    invoke-direct/range {v5 .. v10}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/q;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_10

    .line 238
    :cond_17
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_19

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 239
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 241
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_e
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 242
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 239
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 243
    :cond_18
    check-cast v0, Ljava/util/List;

    .line 238
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/f$a;

    .line 358
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_f

    .line 244
    :cond_19
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_1b

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/f$a;

    .line 359
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 247
    :goto_f
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_1a

    :goto_10
    return-object p1

    .line 236
    :cond_1a
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type io.reactivex.Single<O>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 245
    :cond_1b
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$b;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
