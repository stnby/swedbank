.class public final Lcom/swedbank/mobile/data/transfer/payment/j$i;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/j;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field final synthetic d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/j;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p4, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "+TT;>;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    const/16 v1, 0xa

    if-eqz v0, :cond_5

    check-cast p1, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/f;

    .line 248
    invoke-interface {p1}, Lcom/swedbank/mobile/data/transfer/payment/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 249
    invoke-interface {p1}, Lcom/swedbank/mobile/data/transfer/payment/f;->b()Ljava/util/List;

    move-result-object p1

    if-eqz v0, :cond_0

    .line 251
    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->a:Lcom/swedbank/mobile/data/transfer/payment/j;

    .line 253
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 254
    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    .line 251
    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/data/transfer/payment/j;->a(Lcom/swedbank/mobile/data/transfer/payment/j;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;

    move-result-object p1

    goto/16 :goto_4

    :cond_0
    if-eqz p1, :cond_4

    .line 261
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 266
    check-cast p1, Ljava/lang/Iterable;

    .line 267
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 268
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 269
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PAYMENT_PRIORITY"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    xor-int/2addr v4, v6

    if-eqz v4, :cond_1

    .line 271
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 272
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 273
    check-cast v2, Ljava/lang/Iterable;

    .line 274
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 275
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 276
    check-cast v2, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;

    .line 264
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentError;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 277
    :cond_3
    check-cast p1, Ljava/util/List;

    .line 260
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    .line 278
    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    .line 279
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid response data"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 238
    :cond_5
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 239
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 241
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 242
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 239
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 243
    :cond_6
    check-cast v0, Ljava/util/List;

    .line 238
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 283
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    .line 284
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 283
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_3

    .line 244
    :cond_7
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_9

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 286
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    .line 287
    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/j$i;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 286
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/util/e;)V

    .line 247
    :goto_3
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_8

    :goto_4
    return-object p1

    .line 236
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type io.reactivex.Single<O>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 245
    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$i;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
