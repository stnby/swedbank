.class final Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$b;
.super Ljava/lang/Object;
.source "PaymentExecutionWorker.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a()Lio/reactivex/aa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$b;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroidx/work/ListenableWorker$a;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Payment execution job completed for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$b;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;

    iget-object v1, v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a;->a:Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;

    invoke-static {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;->a(Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    invoke-static {}, Landroidx/work/ListenableWorker$a;->a()Landroidx/work/ListenableWorker$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionWorker$a$b;->a()Landroidx/work/ListenableWorker$a;

    move-result-object v0

    return-object v0
.end method
