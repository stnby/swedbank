.class public final Lcom/swedbank/mobile/data/transfer/payment/j$c;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "+",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionStatusCheckResponse;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/data/transfer/payment/j$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    const/16 v1, 0xa

    if-eqz v0, :cond_4

    check-cast p1, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionStatusCheckResponse;

    .line 250
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionStatusCheckResponse;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionStatusCheckResponse;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    move-result-object p1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/swedbank/mobile/data/transfer/payment/k;->b:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 252
    :pswitch_0
    sget-object p1, Lcom/swedbank/mobile/data/transfer/payment/j$a$b;->a:Lcom/swedbank/mobile/data/transfer/payment/j$a$b;

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/j$a;

    goto :goto_2

    .line 251
    :pswitch_1
    sget-object p1, Lcom/swedbank/mobile/data/transfer/payment/j$a$c;->a:Lcom/swedbank/mobile/data/transfer/payment/j$a$c;

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/j$a;

    goto :goto_2

    .line 252
    :cond_0
    :goto_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 254
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionStatusCheckResponse;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 255
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentExecutionStatusCheckResponse;->b()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 256
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 257
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 258
    check-cast v1, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    .line 255
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 259
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 254
    new-instance p1, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/j$a;

    .line 261
    :goto_2
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 260
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid response data"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 116
    :cond_4
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_8

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 236
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 238
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 239
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 236
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 240
    :cond_5
    check-cast v0, Ljava/util/List;

    .line 116
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 264
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    goto :goto_4

    .line 267
    :cond_6
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 268
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    .line 262
    :goto_4
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;-><init>(Ljava/util/List;)V

    .line 270
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(onError(it.errorMessages().right()))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    .line 268
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 117
    :cond_8
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_b

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 273
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_9

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    goto :goto_5

    .line 276
    :cond_9
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_a

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 277
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    .line 271
    :goto_5
    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$a$a;-><init>(Ljava/util/List;)V

    .line 279
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(onError(it.cause.left()))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_6
    return-object p1

    .line 277
    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 279
    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$c;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
