.class final Lcom/swedbank/mobile/data/transfer/payment/j$l;
.super Ljava/lang/Object;
.source "PaymentRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/payment/j;->a()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/transfer/payment/j$l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/j$l;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/payment/j$l;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/transfer/payment/j$l;->a:Lcom/swedbank/mobile/data/transfer/payment/j$l;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;)Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "systemPushMessage"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->a()Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->b()Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->c()Ljava/lang/String;

    move-result-object v2

    .line 152
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->d()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 148
    new-instance v3, Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;

    invoke-direct {v3, v0, v1, v2, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    .line 430
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/j$l;->a(Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;)Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;

    move-result-object p1

    return-object p1
.end method
