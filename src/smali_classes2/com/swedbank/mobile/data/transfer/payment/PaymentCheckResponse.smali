.class public final Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;
.super Ljava/lang/Object;
.source "PaymentNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "checkPayment"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;",
            ")V"
        }
    .end annotation

    const-string v0, "paymentErrors"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentCorrection"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fee"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationBank"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availablePaymentPriorities"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    iput-object p4, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    iput-object p5, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;ILkotlin/e/b/g;)V
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    .line 69
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    :cond_0
    move-object v1, p1

    and-int/lit8 p1, p6, 0x10

    if-eqz p1, :cond_1

    .line 73
    invoke-static {}, Lcom/swedbank/mobile/data/transfer/payment/i;->a()Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    move-result-object p5

    :cond_1
    move-object v5, p5

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;-><init>(Ljava/util/List;Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;Ljava/util/List;Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;ILjava/lang/Object;)Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a:Ljava/util/List;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a(Ljava/util/List;Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;)Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;)Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;",
            ")",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentErrors"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentCorrection"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fee"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationBank"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availablePaymentPriorities"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;-><init>(Ljava/util/List;Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;)V

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/transfer/payment/PaymentError;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    return-object v0
.end method

.method public final e()Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentCheckResponse(paymentErrors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentCorrection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->b:Lcom/swedbank/mobile/data/transfer/payment/PaymentCorrection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->c:Lcom/swedbank/mobile/data/transfer/payment/PaymentFeeData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", destinationBank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->d:Lcom/swedbank/mobile/data/transfer/payment/PaymentDestination;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", availablePaymentPriorities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentCheckResponse;->e:Lcom/swedbank/mobile/data/transfer/payment/PaymentPriorities;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
