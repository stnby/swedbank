.class public final Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "PaymentSigningChallengeQueryResponseJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableGeneralResponseErrorAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lcom/swedbank/mobile/data/network/GeneralResponseError;",
            ">;"
        }
    .end annotation
.end field

.field private final nullableSigningMethodAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;",
            ">;"
        }
    .end annotation
.end field

.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 4
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "challengeCode"

    const-string v1, "message"

    const-string v2, "signingMethod"

    const-string v3, "error"

    .line 16
    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"c\u2026\"signingMethod\", \"error\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 19
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "challengeCode"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String?>(S\u2026tySet(), \"challengeCode\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 22
    const-class v0, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "signingMethod"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<SigningMet\u2026tySet(), \"signingMethod\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableSigningMethodAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 25
    const-class v0, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<GeneralRes\u2026ions.emptySet(), \"error\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableGeneralResponseErrorAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 78
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "challengeCode"

    .line 79
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "message"

    .line 81
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 82
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "signingMethod"

    .line 83
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 84
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableSigningMethodAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c()Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "error"

    .line 85
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 86
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableGeneralResponseErrorAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 76
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p2, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;
    .locals 17
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "reader"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 30
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 34
    move-object v4, v2

    check-cast v4, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    .line 36
    check-cast v2, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    .line 38
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v5, 0x0

    move-object v9, v2

    move-object v7, v4

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v4, v3

    .line 39
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 40
    iget-object v10, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {v1, v10}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v10

    const/4 v11, 0x1

    packed-switch v10, :pswitch_data_0

    goto :goto_0

    .line 54
    :pswitch_0
    iget-object v8, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableGeneralResponseErrorAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v8, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-object v9, v8

    const/4 v8, 0x1

    goto :goto_0

    .line 50
    :pswitch_1
    iget-object v6, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableSigningMethodAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v6, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    move-object v7, v6

    const/4 v6, 0x1

    goto :goto_0

    .line 46
    :pswitch_2
    iget-object v2, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v4, v2

    const/4 v2, 0x1

    goto :goto_0

    .line 42
    :pswitch_3
    iget-object v3, v0, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponseJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x1

    goto :goto_0

    .line 59
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->j()V

    .line 60
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->q()V

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->f()V

    .line 65
    new-instance v1, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xf

    const/16 v16, 0x0

    move-object v10, v1

    invoke-direct/range {v10 .. v16}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;ILkotlin/e/b/g;)V

    if-eqz v5, :cond_1

    goto :goto_1

    .line 67
    :cond_1
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a()Ljava/lang/String;

    move-result-object v3

    :goto_1
    if-eqz v2, :cond_2

    goto :goto_2

    .line 68
    :cond_2
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->b()Ljava/lang/String;

    move-result-object v4

    :goto_2
    if-eqz v6, :cond_3

    goto :goto_3

    .line 69
    :cond_3
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->c()Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    move-result-object v7

    :goto_3
    if-eqz v8, :cond_4

    goto :goto_4

    .line 70
    :cond_4
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->d()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object v9

    .line 66
    :goto_4
    invoke-virtual {v1, v3, v4, v7, v9}, Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;->a(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Lcom/swedbank/mobile/data/network/GeneralResponseError;)Lcom/swedbank/mobile/data/transfer/payment/PaymentSigningChallengeQueryResponse;

    move-result-object v1

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(PaymentSigningChallengeQueryResponse)"

    return-object v0
.end method
