.class public final Lcom/swedbank/mobile/data/transfer/a;
.super Ljava/lang/Object;
.source "TransferDataModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/data/transfer/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/swedbank/mobile/data/transfer/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/transfer/a;->a:Lcom/swedbank/mobile/data/transfer/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "json_adapters"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x1

    .line 30
    new-array v0, v0, [Lkotlin/k;

    .line 31
    const-class v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 32
    const-class v2, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 33
    sget-object v3, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->UNKNOWN:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 31
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 30
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final b()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "json_adapters"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x1

    .line 40
    new-array v0, v0, [Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    .line 41
    sget-object v1, Lcom/swedbank/mobile/data/l/e;->a:Lcom/swedbank/mobile/data/l/e;

    .line 45
    const-class v1, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem;

    const-string v2, "__typename"

    invoke-static {v1, v2}, Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    move-result-object v1

    const-string v2, "MoshiPolymorphicJsonAdap\u2026s.java, GRAPHQL_TYPENAME)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    const-class v2, Lcom/swedbank/mobile/data/transfer/search/TransferSearchItem$PaymentItem;

    .line 48
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    move-result-object v1

    const-string v2, "withSubtype(javaType, javaType.simpleName)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "run {\n  val javaType = s\u2026e, javaType.simpleName)\n}"

    .line 46
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 40
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
