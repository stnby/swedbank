.class public final Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/predefined/b$c;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/transfer/predefined/b$c;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/transfer/predefined/b$c;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;->a:Lcom/swedbank/mobile/data/transfer/predefined/b$c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 9
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentsQueryResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    :try_start_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentsQueryResponse;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v0

    .line 245
    sget-object v2, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    const-string v3, "PREDEFINED_PAYMENT_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v0

    .line 246
    sget-object v2, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bx;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "PREDEFINED_PAYMENT_DATA.CUSTOMER_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;->a:Lcom/swedbank/mobile/data/transfer/predefined/b$c;

    iget-object v3, v3, Lcom/swedbank/mobile/data/transfer/predefined/b$c;->a:Ljava/lang/String;

    .line 247
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    goto/16 :goto_2

    .line 249
    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v2

    .line 254
    sget-object v3, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    const-string v4, "PREDEFINED_PAYMENT_DATA"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v2

    .line 255
    sget-object v3, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/bx;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "PREDEFINED_PAYMENT_DATA.CUSTOMER_ID"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;->a:Lcom/swedbank/mobile/data/transfer/predefined/b$c;

    iget-object v4, v4, Lcom/swedbank/mobile/data/transfer/predefined/b$c;->a:Ljava/lang/String;

    .line 256
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.`is`(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    sget-object v4, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/bx;->d:Lcom/siimkinks/sqlitemagic/du;

    const-string v5, "PREDEFINED_PAYMENT_DATA.PREDEFINED_PAYMENT_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentsQueryResponse;->a()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 258
    new-instance v6, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v5, v7}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 259
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 260
    check-cast v8, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;

    .line 257
    invoke-virtual {v8}, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    :cond_1
    check-cast v6, Ljava/util/List;

    check-cast v6, Ljava/util/Collection;

    .line 262
    invoke-virtual {v4, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.notIn(values)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.and(expr)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 253
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 265
    sget-object v2, Lcom/siimkinks/sqlitemagic/by;->a:Lcom/siimkinks/sqlitemagic/by;

    .line 270
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentsQueryResponse;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 271
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v7}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 272
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 273
    check-cast v3, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;

    .line 270
    iget-object v4, p0, Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;->a:Lcom/swedbank/mobile/data/transfer/predefined/b$c;

    iget-object v4, v4, Lcom/swedbank/mobile/data/transfer/predefined/b$c;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a(Ljava/lang/String;)Lcom/swedbank/mobile/data/transfer/predefined/a;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 274
    :cond_2
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 275
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/db$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/db$a;

    move-result-object v0

    .line 269
    sget-object v2, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bx;->d:Lcom/siimkinks/sqlitemagic/du;

    check-cast v2, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/db$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object v0

    .line 268
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/a;->b()Z

    .line 278
    :goto_2
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 280
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/predefined/b$c$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
