.class public final Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;
.super Ljava/lang/Object;
.source "PredefinedPaymentsNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "predefinedPaymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predefinedPaymentName"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/data/transfer/predefined/a;
    .locals 17
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "customerId"

    move-object/from16 v4, p1

    invoke-static {v4, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v1, v0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    .line 23
    new-instance v16, Lcom/swedbank/mobile/data/transfer/predefined/a;

    .line 25
    iget-object v5, v0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a:Ljava/lang/String;

    .line 26
    iget-object v6, v0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->b:Ljava/lang/String;

    .line 27
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->g()Ljava/lang/String;

    move-result-object v7

    .line 28
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->a()Ljava/lang/String;

    move-result-object v8

    .line 29
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->b()Ljava/lang/String;

    move-result-object v9

    .line 30
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->c()Ljava/math/BigDecimal;

    move-result-object v10

    .line 31
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->d()Ljava/lang/String;

    move-result-object v11

    .line 32
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->e()Ljava/lang/String;

    move-result-object v12

    .line 33
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;->f()Ljava/lang/String;

    move-result-object v13

    const/4 v3, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object/from16 v2, v16

    .line 23
    invoke-direct/range {v2 .. v15}, Lcom/swedbank/mobile/data/transfer/predefined/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    return-object v16
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "predefinedPaymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predefinedPaymentName"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;

    invoke-direct {v0, p1, p2, p3}, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    iget-object p1, p1, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PredefinedPaymentResponse(predefinedPaymentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", predefinedPaymentName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/transfer/predefined/PredefinedPaymentResponse;->c:Lcom/swedbank/mobile/data/transfer/PaymentDataResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
