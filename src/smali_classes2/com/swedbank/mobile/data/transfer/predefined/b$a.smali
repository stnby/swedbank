.class final synthetic Lcom/swedbank/mobile/data/transfer/predefined/b$a;
.super Lkotlin/e/b/i;
.source "PredefinedPaymentsRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/transfer/predefined/b;->c(Ljava/lang/String;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/data/transfer/predefined/a;",
        "Lcom/swedbank/mobile/business/transfer/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/transfer/predefined/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/transfer/predefined/b$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/predefined/b$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/transfer/predefined/b$a;->a:Lcom/swedbank/mobile/data/transfer/predefined/b$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/transfer/predefined/a;)Lcom/swedbank/mobile/business/transfer/a/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/transfer/predefined/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/transfer/predefined/a;->a()Lcom/swedbank/mobile/business/transfer/a/a;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/transfer/predefined/a;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/data/transfer/predefined/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/transfer/predefined/b$a;->a(Lcom/swedbank/mobile/data/transfer/predefined/a;)Lcom/swedbank/mobile/business/transfer/a/a;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "toPredefinedPayment"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "toPredefinedPayment()Lcom/swedbank/mobile/business/transfer/predefined/PredefinedPayment;"

    return-object v0
.end method
