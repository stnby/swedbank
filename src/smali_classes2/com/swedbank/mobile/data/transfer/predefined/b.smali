.class public final Lcom/swedbank/mobile/data/transfer/predefined/b;
.super Ljava/lang/Object;
.source "PredefinedPaymentsRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/a/b;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/transfer/predefined/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/transfer/predefined/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/transfer/predefined/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "predefinedPaymentsService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/transfer/predefined/b;->a:Lcom/swedbank/mobile/data/transfer/predefined/e;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/data/transfer/predefined/b;->a:Lcom/swedbank/mobile/data/transfer/predefined/e;

    .line 24
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/transfer/predefined/f;->a(Lcom/swedbank/mobile/data/transfer/predefined/e;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 25
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/swedbank/mobile/data/transfer/predefined/b$c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/transfer/predefined/b$c;-><init>(Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    const-string v2, "PREDEFINED_PAYMENT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 51
    sget-object v1, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bx;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "PREDEFINED_PAYMENT_DATA.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    const-string v0, "SELECT\n          FROM PR\u2026USTOMER_ID IS customerId)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/bx;->e:Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0}, Lcom/siimkinks/sqlitemagic/cb;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/x;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v0

    const-string v1, "lower(PREDEFINED_PAYMENT\u2026FINED_PAYMENT_NAME).asc()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 77
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/cb$f;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/cb$h;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$e;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 55
    sget-object v0, Lcom/swedbank/mobile/data/transfer/predefined/b$b;->a:Lcom/swedbank/mobile/data/transfer/predefined/b$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM P\u2026a::toPredefinedPayment) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/j;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/transfer/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "predefinedPaymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 59
    sget-object v1, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    const-string v2, "PREDEFINED_PAYMENT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 60
    sget-object v1, Lcom/siimkinks/sqlitemagic/bx;->a:Lcom/siimkinks/sqlitemagic/bx;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bx;->d:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "PREDEFINED_PAYMENT_DATA.PREDEFINED_PAYMENT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 78
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 62
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cg;->c()Lio/reactivex/j;

    move-result-object p1

    .line 64
    sget-object v0, Lcom/swedbank/mobile/data/transfer/predefined/b$a;->a:Lcom/swedbank/mobile/data/transfer/predefined/b$a;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/transfer/predefined/c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/transfer/predefined/c;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM P\u2026ata::toPredefinedPayment)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
