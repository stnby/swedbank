.class public final Lcom/swedbank/mobile/data/transfer/b;
.super Ljava/lang/Object;
.source "TransferDataModule_ProvideJsonAdapterFactoriesFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lcom/squareup/moshi/JsonAdapter$a;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/swedbank/mobile/data/transfer/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/swedbank/mobile/data/transfer/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/transfer/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/transfer/b;->a:Lcom/swedbank/mobile/data/transfer/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()Lcom/swedbank/mobile/data/transfer/b;
    .locals 1

    .line 18
    sget-object v0, Lcom/swedbank/mobile/data/transfer/b;->a:Lcom/swedbank/mobile/data/transfer/b;

    return-object v0
.end method

.method public static c()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;"
        }
    .end annotation

    .line 22
    invoke-static {}, Lcom/swedbank/mobile/data/transfer/a;->b()Ljava/util/Set;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;"
        }
    .end annotation

    .line 14
    invoke-static {}, Lcom/swedbank/mobile/data/transfer/b;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/transfer/b;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
