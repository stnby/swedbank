.class final Lcom/swedbank/mobile/data/ordering/k$a;
.super Ljava/lang/Object;
.source "SystemPushMessageStream.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/ordering/k;->a(Lcom/swedbank/mobile/data/ordering/h;Lio/reactivex/w;Ljava/lang/String;Lkotlin/h/b;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/ordering/h;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lkotlin/h/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/ordering/h;Ljava/lang/String;Lkotlin/h/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/ordering/k$a;->a:Lcom/swedbank/mobile/data/ordering/h;

    iput-object p2, p0, Lcom/swedbank/mobile/data/ordering/k$a;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/ordering/k$a;->c:Lkotlin/h/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "TT;>;)",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "loginStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 29
    iget-object v1, p0, Lcom/swedbank/mobile/data/ordering/k$a;->a:Lcom/swedbank/mobile/data/ordering/h;

    iget-object v2, p0, Lcom/swedbank/mobile/data/ordering/k$a;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/data/ordering/h;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v1

    .line 30
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x1e

    invoke-virtual {v1, v3, v4, v2}, Lio/reactivex/o;->d(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v1

    .line 32
    new-instance v2, Lcom/swedbank/mobile/data/ordering/k$a$1;

    iget-object v3, p0, Lcom/swedbank/mobile/data/ordering/k$a;->c:Lkotlin/h/b;

    invoke-static {v3}, Lkotlin/e/a;->a(Lkotlin/h/b;)Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/data/ordering/k$a$1;-><init>(Ljava/lang/Class;)V

    check-cast v2, Lkotlin/e/a/b;

    new-instance v3, Lcom/swedbank/mobile/data/ordering/l;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/data/ordering/l;-><init>(Lkotlin/e/a/b;)V

    check-cast v3, Lio/reactivex/c/k;

    invoke-virtual {p1, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    .line 37
    invoke-static {}, Lio/reactivex/o;->f()Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->g(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 31
    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 38
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    .line 41
    invoke-static {}, Lcom/swedbank/mobile/data/push/d;->a()Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/o;->e(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "observeMessagesOfType(me\u2026MPTY_SYSTEM_PUSH_MESSAGE)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {v0, v1, p1}, Lio/reactivex/i/d;->b(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/ordering/k$a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
