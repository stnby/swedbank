.class public final Lcom/swedbank/mobile/data/ordering/d$c;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/ordering/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "+",
            "Lcom/swedbank/mobile/data/ordering/BiometricOnboardingInitiationResponse;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/util/r;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingInitiationResponse;

    .line 249
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingInitiationResponse;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252
    new-instance v0, Lcom/swedbank/mobile/business/util/r$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingInitiationResponse;->a()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/r$b;-><init>(Ljava/lang/String;)V

    .line 253
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 254
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingInitiationResponse;->b()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 257
    new-instance v0, Lcom/swedbank/mobile/business/util/r$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingInitiationResponse;->b()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/GeneralResponseError;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/r$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 258
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 259
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid response data"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 238
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 239
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 241
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 242
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 239
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 243
    :cond_3
    check-cast v0, Ljava/util/List;

    .line 238
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/r$a;

    .line 262
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/r$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_1

    .line 244
    :cond_4
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/r$a;

    .line 263
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/r$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 247
    :goto_1
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_5

    :goto_2
    return-object p1

    .line 236
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type io.reactivex.Single<O>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 245
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/ordering/d$c;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
