.class public final Lcom/swedbank/mobile/data/ordering/d$e$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/ordering/d$e;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/ordering/d$e;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/ordering/d$e;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/ordering/d$e$1;->a:Lcom/swedbank/mobile/data/ordering/d$e;

    iput-object p2, p0, Lcom/swedbank/mobile/data/ordering/d$e$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/swedbank/mobile/data/ordering/d$e$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingFinalizationResponse;

    .line 237
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingFinalizationResponse;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    check-cast v0, Lcom/swedbank/mobile/business/util/p;

    goto :goto_0

    .line 238
    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingFinalizationResponse;->b()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/ordering/BiometricOnboardingFinalizationResponse;->b()Lcom/swedbank/mobile/data/network/GeneralResponseError;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/GeneralResponseError;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object v0, v1

    check-cast v0, Lcom/swedbank/mobile/business/util/p;

    :goto_0
    return-object v0

    .line 239
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid response data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/ordering/d$e$1;->a()Lcom/swedbank/mobile/business/util/p;

    move-result-object v0

    return-object v0
.end method
