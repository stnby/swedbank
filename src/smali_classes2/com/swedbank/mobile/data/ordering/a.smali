.class public final Lcom/swedbank/mobile/data/ordering/a;
.super Ljava/lang/Object;
.source "LocalServiceOrderingRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/h/a;


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/a/a/a/f;)V
    .locals 2
    .param p1    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferences"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "mobileAppIdPreference"

    const-string v1, ""

    .line 19
    invoke-virtual {p1, v0, v1}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object p1

    const-string v0, "appPreferences.getString(PREF_MOBILE_APP_ID, \"\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/ordering/a;->a:Lcom/a/a/a/d;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/data/ordering/a;->a:Lcom/a/a/a/d;

    .line 22
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 23
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v0

    .line 25
    sget-object v1, Lcom/swedbank/mobile/data/ordering/a$a;->a:Lcom/swedbank/mobile/data/ordering/a$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/data/ordering/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/ordering/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/k;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "localMobileAppId\n      .\u2026ilter(String::isNotEmpty)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "mobileAppId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/data/ordering/a;->a:Lcom/a/a/a/d;

    invoke-interface {v0, p1}, Lcom/a/a/a/d;->a(Ljava/lang/Object;)V

    return-void
.end method
