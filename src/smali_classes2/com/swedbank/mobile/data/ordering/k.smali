.class public final Lcom/swedbank/mobile/data/ordering/k;
.super Ljava/lang/Object;
.source "SystemPushMessageStream.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/data/ordering/h;Lio/reactivex/w;Ljava/lang/String;Lkotlin/h/b;)Lio/reactivex/w;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/data/ordering/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lio/reactivex/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "C::TT;>(",
            "Lcom/swedbank/mobile/data/ordering/h;",
            "Lio/reactivex/w<",
            "TT;>;",
            "Ljava/lang/String;",
            "Lkotlin/h/b<",
            "TC;>;)",
            "Lio/reactivex/w<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;",
            "TT;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$zipSingleMessageWith"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageType"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancellingEvent"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    .line 27
    new-instance v0, Lcom/swedbank/mobile/data/ordering/k$a;

    invoke-direct {v0, p0, p2, p3}, Lcom/swedbank/mobile/data/ordering/k$a;-><init>(Lcom/swedbank/mobile/data/ordering/h;Ljava/lang/String;Lkotlin/h/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p0

    .line 44
    invoke-virtual {p0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object p0

    const-string p1, "other\n    .toObservable(\u2026    }\n    .firstOrError()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
