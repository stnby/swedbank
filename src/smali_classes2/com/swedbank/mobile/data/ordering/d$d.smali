.class final Lcom/swedbank/mobile/data/ordering/d$d;
.super Ljava/lang/Object;
.source "ServiceOrderingRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/ordering/d;->a(Ljava/lang/String;Ljava/security/Key;Ljava/security/Signature;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/ordering/d;

.field final synthetic b:Ljava/security/Signature;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/ordering/d;Ljava/security/Signature;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/ordering/d$d;->a:Lcom/swedbank/mobile/data/ordering/d;

    iput-object p2, p0, Lcom/swedbank/mobile/data/ordering/d$d;->b:Ljava/security/Signature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/w;
    .locals 5
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;",
            "+",
            "Lcom/swedbank/mobile/business/util/r;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/r;

    .line 72
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/r$b;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/data/ordering/d$d;->a:Lcom/swedbank/mobile/data/ordering/d;

    .line 73
    check-cast p1, Lcom/swedbank/mobile/business/util/r$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/r$b;->a()Ljava/lang/String;

    move-result-object p1

    .line 74
    iget-object v2, p0, Lcom/swedbank/mobile/data/ordering/d$d;->b:Ljava/security/Signature;

    .line 75
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/push/IncomingSystemPushMessage;->a()Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-static {v1}, Lcom/swedbank/mobile/data/ordering/d;->a(Lcom/swedbank/mobile/data/ordering/d;)Lcom/swedbank/mobile/data/ordering/f;

    move-result-object v3

    .line 161
    invoke-static {v1}, Lcom/swedbank/mobile/data/ordering/d;->b(Lcom/swedbank/mobile/data/ordering/d;)Lcom/squareup/moshi/n;

    move-result-object v4

    .line 164
    invoke-static {v1}, Lcom/swedbank/mobile/data/ordering/d;->c(Lcom/swedbank/mobile/data/ordering/d;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v1

    .line 167
    invoke-interface {v1, v2, v0}, Lcom/swedbank/mobile/business/d/a;->a(Ljava/security/Signature;Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 170
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    .line 168
    check-cast v0, Ljava/lang/String;

    .line 177
    :try_start_0
    invoke-virtual {v2}, Ljava/security/Signature;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    const-string v2, "algorithm"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;->valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    :catch_0
    sget-object v1, Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;->SHA512withECDSA:Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;

    .line 163
    :goto_0
    new-instance v2, Lcom/swedbank/mobile/data/ordering/PublicKeySignature;

    invoke-direct {v2, v0, v1}, Lcom/swedbank/mobile/data/ordering/PublicKeySignature;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;)V

    .line 160
    invoke-static {v3, v4, p1, v2}, Lcom/swedbank/mobile/data/ordering/g;->a(Lcom/swedbank/mobile/data/ordering/f;Lcom/squareup/moshi/n;Ljava/lang/String;Lcom/swedbank/mobile/data/ordering/PublicKeySignature;)Lio/reactivex/w;

    move-result-object p1

    .line 159
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 181
    new-instance v0, Lcom/swedbank/mobile/data/ordering/d$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/ordering/d$e;-><init>()V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 172
    :cond_0
    instance-of p1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz p1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    .line 168
    check-cast p1, Ljava/lang/Throwable;

    .line 173
    throw p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 76
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/r$a;

    if-eqz v0, :cond_3

    .line 77
    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    check-cast p1, Lcom/swedbank/mobile/business/util/r$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/r$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 190
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/ordering/d$d;->a(Lkotlin/k;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
