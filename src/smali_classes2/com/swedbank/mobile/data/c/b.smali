.class public final Lcom/swedbank/mobile/data/c/b;
.super Ljava/lang/Object;
.source "CryptoRepositoryImpl.kt"


# static fields
.field private static final a:Ljavax/crypto/spec/OAEPParameterSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 47
    new-instance v0, Ljavax/crypto/spec/OAEPParameterSpec;

    const-string v1, "SHA-256"

    const-string v2, "MGF1"

    sget-object v3, Ljava/security/spec/MGF1ParameterSpec;->SHA1:Ljava/security/spec/MGF1ParameterSpec;

    check-cast v3, Ljava/security/spec/AlgorithmParameterSpec;

    sget-object v4, Ljavax/crypto/spec/PSource$PSpecified;->DEFAULT:Ljavax/crypto/spec/PSource$PSpecified;

    check-cast v4, Ljavax/crypto/spec/PSource;

    invoke-direct {v0, v1, v2, v3, v4}, Ljavax/crypto/spec/OAEPParameterSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/spec/PSource;)V

    sput-object v0, Lcom/swedbank/mobile/data/c/b;->a:Ljavax/crypto/spec/OAEPParameterSpec;

    return-void
.end method

.method public static final synthetic a()Ljavax/crypto/spec/OAEPParameterSpec;
    .locals 1

    .line 1
    sget-object v0, Lcom/swedbank/mobile/data/c/b;->a:Ljavax/crypto/spec/OAEPParameterSpec;

    return-object v0
.end method
