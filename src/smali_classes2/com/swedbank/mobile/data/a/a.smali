.class public final Lcom/swedbank/mobile/data/a/a;
.super Ljava/lang/Object;
.source "BankInfoRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/b/c;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/b/a$b;

.field private final b:Lcom/swedbank/mobile/business/b/a$b;

.field private final c:Lcom/swedbank/mobile/business/b/a$b;

.field private final d:Lcom/swedbank/mobile/business/b/a$a;

.field private final e:Lcom/swedbank/mobile/business/b/a$a;

.field private final f:Lcom/swedbank/mobile/business/b/a$d;

.field private final g:Lcom/swedbank/mobile/business/b/a$d;

.field private final h:Lcom/swedbank/mobile/business/b/a$d;

.field private final i:Lcom/swedbank/mobile/business/b/a$e;

.field private final j:Lcom/swedbank/mobile/business/b/a$c;

.field private final k:Lcom/swedbank/mobile/business/b/a$c;

.field private final l:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/a;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferenceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->l:Lcom/swedbank/mobile/business/c/a;

    .line 17
    new-instance p1, Lcom/swedbank/mobile/business/b/a$b;

    const-string v0, "android@swedbank.ee"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$b;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->a:Lcom/swedbank/mobile/business/b/a$b;

    .line 18
    new-instance p1, Lcom/swedbank/mobile/business/b/a$b;

    const-string v0, "android@swedbank.lv"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$b;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->b:Lcom/swedbank/mobile/business/b/a$b;

    .line 19
    new-instance p1, Lcom/swedbank/mobile/business/b/a$b;

    const-string v0, "android@swedbank.lt"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$b;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->c:Lcom/swedbank/mobile/business/b/a$b;

    .line 20
    new-instance p1, Lcom/swedbank/mobile/business/b/a$a;

    .line 21
    new-instance v0, Lcom/swedbank/mobile/business/e;

    const-string v1, "private/home/more/appointments"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    .line 20
    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$a;-><init>(Lcom/swedbank/mobile/business/e;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->d:Lcom/swedbank/mobile/business/b/a$a;

    .line 22
    new-instance p1, Lcom/swedbank/mobile/business/b/a$a;

    .line 23
    new-instance v0, Lcom/swedbank/mobile/business/e;

    const-string v1, "business/useful/useful/appointments"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    .line 22
    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$a;-><init>(Lcom/swedbank/mobile/business/e;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->e:Lcom/swedbank/mobile/business/b/a$a;

    .line 24
    new-instance p1, Lcom/swedbank/mobile/business/b/a$d;

    new-instance v0, Lcom/swedbank/mobile/business/e;

    const-string v1, "about/about/branches/map"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$d;-><init>(Lcom/swedbank/mobile/business/e;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->f:Lcom/swedbank/mobile/business/b/a$d;

    .line 25
    new-instance p1, Lcom/swedbank/mobile/business/b/a$d;

    new-instance v0, Lcom/swedbank/mobile/business/e;

    const-string v1, "about/swedbank/contacts/map"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$d;-><init>(Lcom/swedbank/mobile/business/e;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->g:Lcom/swedbank/mobile/business/b/a$d;

    .line 26
    new-instance p1, Lcom/swedbank/mobile/business/b/a$d;

    new-instance v0, Lcom/swedbank/mobile/business/e;

    const-string v1, "about/swedbank/contacts/map"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$d;-><init>(Lcom/swedbank/mobile/business/e;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->h:Lcom/swedbank/mobile/business/b/a$d;

    .line 27
    new-instance p1, Lcom/swedbank/mobile/business/b/a$e;

    const-string v0, "+371"

    const-string v1, "67 444 444"

    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/b/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->i:Lcom/swedbank/mobile/business/b/a$e;

    .line 28
    new-instance p1, Lcom/swedbank/mobile/business/b/a$c;

    const-string v0, "info@swedbank.lv"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$c;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->j:Lcom/swedbank/mobile/business/b/a$c;

    .line 29
    new-instance p1, Lcom/swedbank/mobile/business/b/a$c;

    const-string v0, "info@swedbank.lt"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/b/a$c;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/a/a;->k:Lcom/swedbank/mobile/business/b/a$c;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/b/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/data/a/a;->l:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/data/a/b;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x5

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x6

    packed-switch v1, :pswitch_data_0

    .line 54
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No private contact info for country "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 47
    :pswitch_1
    new-array v0, v8, [Lcom/swedbank/mobile/business/b/a;

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->c:Lcom/swedbank/mobile/business/b/a$b;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v7

    .line 49
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->d:Lcom/swedbank/mobile/business/b/a$a;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v6

    .line 50
    new-instance v1, Lcom/swedbank/mobile/business/b/a$e;

    const-string v6, "+370"

    const-string v7, "5 268 4444"

    invoke-direct {v1, v6, v7}, Lcom/swedbank/mobile/business/b/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v5

    .line 51
    new-instance v1, Lcom/swedbank/mobile/business/b/a$f;

    const-string v5, "swedbank.lietuva"

    invoke-direct {v1, v5}, Lcom/swedbank/mobile/business/b/a$f;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v4

    .line 52
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->k:Lcom/swedbank/mobile/business/b/a$c;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v3

    .line 53
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->h:Lcom/swedbank/mobile/business/b/a$d;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v2

    .line 47
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 40
    :pswitch_2
    new-array v0, v8, [Lcom/swedbank/mobile/business/b/a;

    .line 41
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->b:Lcom/swedbank/mobile/business/b/a$b;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v7

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->d:Lcom/swedbank/mobile/business/b/a$a;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v6

    .line 43
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->i:Lcom/swedbank/mobile/business/b/a$e;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v5

    .line 44
    new-instance v1, Lcom/swedbank/mobile/business/b/a$f;

    const-string v5, "swedbank.latvija"

    invoke-direct {v1, v5}, Lcom/swedbank/mobile/business/b/a$f;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v4

    .line 45
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->j:Lcom/swedbank/mobile/business/b/a$c;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v3

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->g:Lcom/swedbank/mobile/business/b/a$d;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v2

    .line 40
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 33
    :pswitch_3
    new-array v0, v8, [Lcom/swedbank/mobile/business/b/a;

    .line 34
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->a:Lcom/swedbank/mobile/business/b/a$b;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v7

    .line 35
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->d:Lcom/swedbank/mobile/business/b/a$a;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v6

    .line 36
    new-instance v1, Lcom/swedbank/mobile/business/b/a$e;

    const-string v6, "+372"

    const-string v7, "6 310 310"

    invoke-direct {v1, v6, v7}, Lcom/swedbank/mobile/business/b/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v5

    .line 37
    new-instance v1, Lcom/swedbank/mobile/business/b/a$f;

    const-string v5, "swedbank.eestis"

    invoke-direct {v1, v5}, Lcom/swedbank/mobile/business/b/a$f;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v4

    .line 38
    new-instance v1, Lcom/swedbank/mobile/business/b/a$c;

    const-string v4, "info@swedbank.ee"

    invoke-direct {v1, v4}, Lcom/swedbank/mobile/business/b/a$c;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v3

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->f:Lcom/swedbank/mobile/business/b/a$d;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v2

    .line 33
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/b/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/data/a/a;->l:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/data/a/b;->b:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x5

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x6

    packed-switch v1, :pswitch_data_0

    .line 80
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No business contact info for country "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 73
    :pswitch_1
    new-array v0, v8, [Lcom/swedbank/mobile/business/b/a;

    .line 74
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->c:Lcom/swedbank/mobile/business/b/a$b;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v7

    .line 75
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->e:Lcom/swedbank/mobile/business/b/a$a;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v6

    .line 76
    new-instance v1, Lcom/swedbank/mobile/business/b/a$e;

    const-string v6, "+370"

    const-string v7, "5 268 4422"

    invoke-direct {v1, v6, v7}, Lcom/swedbank/mobile/business/b/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v5

    .line 77
    new-instance v1, Lcom/swedbank/mobile/business/b/a$f;

    const-string v5, "swedbank.lietuva.corporate"

    invoke-direct {v1, v5}, Lcom/swedbank/mobile/business/b/a$f;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v4

    .line 78
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->k:Lcom/swedbank/mobile/business/b/a$c;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v3

    .line 79
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->h:Lcom/swedbank/mobile/business/b/a$d;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v2

    .line 73
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 66
    :pswitch_2
    new-array v0, v8, [Lcom/swedbank/mobile/business/b/a;

    .line 67
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->b:Lcom/swedbank/mobile/business/b/a$b;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v7

    .line 68
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->e:Lcom/swedbank/mobile/business/b/a$a;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v6

    .line 69
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->i:Lcom/swedbank/mobile/business/b/a$e;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v5

    .line 70
    new-instance v1, Lcom/swedbank/mobile/business/b/a$f;

    const-string v5, "swedbank.latvija.corporate"

    invoke-direct {v1, v5}, Lcom/swedbank/mobile/business/b/a$f;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v4

    .line 71
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->j:Lcom/swedbank/mobile/business/b/a$c;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v3

    .line 72
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->g:Lcom/swedbank/mobile/business/b/a$d;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v2

    .line 66
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 59
    :pswitch_3
    new-array v0, v8, [Lcom/swedbank/mobile/business/b/a;

    .line 60
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->a:Lcom/swedbank/mobile/business/b/a$b;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v7

    .line 61
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->e:Lcom/swedbank/mobile/business/b/a$a;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v6

    .line 62
    new-instance v1, Lcom/swedbank/mobile/business/b/a$e;

    const-string v6, "+372"

    const-string v7, "6 132 222"

    invoke-direct {v1, v6, v7}, Lcom/swedbank/mobile/business/b/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v5

    .line 63
    new-instance v1, Lcom/swedbank/mobile/business/b/a$f;

    const-string v5, "swedbank.eestis.corporate"

    invoke-direct {v1, v5}, Lcom/swedbank/mobile/business/b/a$f;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v4

    .line 64
    new-instance v1, Lcom/swedbank/mobile/business/b/a$c;

    const-string v4, "ariklient@swedbank.ee"

    invoke-direct {v1, v4}, Lcom/swedbank/mobile/business/b/a$c;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v3

    .line 65
    iget-object v1, p0, Lcom/swedbank/mobile/data/a/a;->f:Lcom/swedbank/mobile/business/b/a$d;

    check-cast v1, Lcom/swedbank/mobile/business/b/a;

    aput-object v1, v0, v2

    .line 59
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c()Lcom/swedbank/mobile/business/e;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 84
    new-instance v0, Lcom/swedbank/mobile/business/e;

    const-string v1, "private/home/important/gdpr"

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    return-object v0
.end method
