.class public final enum Lcom/swedbank/mobile/data/authentication/AuthenticationState;
.super Ljava/lang/Enum;
.source "AuthenticationModels.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/data/authentication/AuthenticationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/data/authentication/AuthenticationState;

.field public static final enum AUTHENTICATED:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

.field public static final enum AUTHENTICATION_FAILED:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

.field public static final enum AUTHENTICATION_IN_PROGRESS:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

.field public static final enum NO_SESSION:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

.field public static final enum UNAUTHENTICATED:Lcom/swedbank/mobile/data/authentication/AuthenticationState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    new-instance v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    const-string v2, "AUTHENTICATED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/authentication/AuthenticationState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->AUTHENTICATED:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    const-string v2, "AUTHENTICATION_IN_PROGRESS"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/authentication/AuthenticationState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->AUTHENTICATION_IN_PROGRESS:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    const-string v2, "UNAUTHENTICATED"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/authentication/AuthenticationState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->UNAUTHENTICATED:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    const-string v2, "AUTHENTICATION_FAILED"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/authentication/AuthenticationState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->AUTHENTICATION_FAILED:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    const-string v2, "NO_SESSION"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/data/authentication/AuthenticationState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->NO_SESSION:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->$VALUES:[Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/data/authentication/AuthenticationState;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/data/authentication/AuthenticationState;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->$VALUES:[Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/data/authentication/AuthenticationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    return-object v0
.end method
