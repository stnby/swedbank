.class final Lcom/swedbank/mobile/data/authentication/c$l;
.super Ljava/lang/Object;
.source "AuthenticationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/c;->b(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/authentication/c$l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/authentication/c$l;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/authentication/c$l;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/authentication/c$l;->a:Lcom/swedbank/mobile/data/authentication/c$l;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lretrofit2/q;)Lcom/swedbank/mobile/business/authentication/session/h;
    .locals 3
    .param p1    # Lretrofit2/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/q<",
            "Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;",
            ">;)",
            "Lcom/swedbank/mobile/business/authentication/session/h;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p1}, Lretrofit2/q;->a()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0xc8

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne v0, v1, :cond_2

    .line 128
    invoke-virtual {p1}, Lretrofit2/q;->e()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 150
    check-cast p1, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;

    .line 128
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->a()Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object p1

    goto :goto_1

    .line 155
    :cond_1
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 129
    :cond_2
    sget-object p1, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lretrofit2/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/c$l;->a(Lretrofit2/q;)Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object p1

    return-object p1
.end method
