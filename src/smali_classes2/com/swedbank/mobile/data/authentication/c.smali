.class public final Lcom/swedbank/mobile/data/authentication/c;
.super Ljava/lang/Object;
.source "AuthenticationRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/l;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/authentication/g;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/swedbank/mobile/data/network/aa;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/authentication/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/network/aa;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/authentication/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "authorization_client_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "authorization_client_secret"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "authorization_scope"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "scoped_authorization_client_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "scoped_authorization_scope"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/data/network/aa;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticationService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorizationClientId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorizationClientSecret"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorizationScope"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopedAuthorizationClientId"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopedAuthorizationScope"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "responseConverter"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/c;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/authentication/c;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/data/authentication/c;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/swedbank/mobile/data/authentication/c;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/swedbank/mobile/data/authentication/c;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/swedbank/mobile/data/authentication/c;->g:Lcom/swedbank/mobile/data/network/aa;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/authentication/c;)Lcom/swedbank/mobile/data/authentication/g;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/authentication/c;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/c;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/authentication/c;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/c;->c:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/authentication/c;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/c;->e:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/data/authentication/c;)Lcom/swedbank/mobile/data/network/aa;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/c;->g:Lcom/swedbank/mobile/data/network/aa;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "session"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 111
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/o;->a()Ljava/lang/String;

    move-result-object v2

    .line 112
    iget-object v3, p0, Lcom/swedbank/mobile/data/authentication/c;->b:Ljava/lang/String;

    .line 113
    iget-object v4, p0, Lcom/swedbank/mobile/data/authentication/c;->c:Ljava/lang/String;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 110
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/data/authentication/g$a;->c(Lcom/swedbank/mobile/data/authentication/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/b;

    move-result-object p1

    .line 114
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "authenticationService\n  \u2026       .onErrorComplete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.complete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public a()Lio/reactivex/w;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/x;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/c;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/c;->d:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/authentication/g$a;->a(Lcom/swedbank/mobile/data/authentication/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    .line 34
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/swedbank/mobile/data/authentication/c$m;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/authentication/c$m;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "authenticationService\n  \u2026Data).toSingle())\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/x;)Lio/reactivex/w;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/authentication/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/x;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "metadata"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 45
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/x;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/authentication/g$a;->b(Lcom/swedbank/mobile/data/authentication/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    .line 46
    new-instance v0, Lcom/swedbank/mobile/data/authentication/c$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/authentication/c$c;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 54
    sget-object v0, Lcom/swedbank/mobile/data/authentication/c$d;->a:Lcom/swedbank/mobile/data/authentication/c$d;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/authentication/d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/authentication/d;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "authenticationService\n  \u2026TokenResponse::toSession)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "idToken"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 59
    iget-object v3, p0, Lcom/swedbank/mobile/data/authentication/c;->e:Ljava/lang/String;

    .line 60
    iget-object v4, p0, Lcom/swedbank/mobile/data/authentication/c;->f:Ljava/lang/String;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v2, p1

    .line 57
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/data/authentication/g$a;->a(Lcom/swedbank/mobile/data/authentication/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    .line 61
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 62
    new-instance v0, Lcom/swedbank/mobile/data/authentication/c$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/authentication/c$a;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 70
    sget-object v0, Lcom/swedbank/mobile/data/authentication/c$b;->a:Lcom/swedbank/mobile/data/authentication/c$b;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/authentication/d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/authentication/d;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "authenticationService\n  \u2026TokenResponse::toSession)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumber"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 73
    new-instance v1, Lcom/swedbank/mobile/data/authentication/MobileIdCredentials;

    invoke-direct {v1, p1, p2}, Lcom/swedbank/mobile/data/authentication/MobileIdCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/g;->a(Lcom/swedbank/mobile/data/authentication/MobileIdCredentials;)Lio/reactivex/w;

    move-result-object p1

    .line 74
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 75
    new-instance p2, Lcom/swedbank/mobile/data/authentication/c$e;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/data/authentication/c$e;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "authenticationService\n  \u2026uthenticationProgress() }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/w;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "session"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 123
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/o;->b()Ljava/lang/String;

    move-result-object v2

    .line 124
    iget-object v3, p0, Lcom/swedbank/mobile/data/authentication/c;->b:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 122
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/authentication/g$a;->c(Lcom/swedbank/mobile/data/authentication/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    .line 125
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 126
    sget-object v0, Lcom/swedbank/mobile/data/authentication/c$l;->a:Lcom/swedbank/mobile/data/authentication/c$l;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "authenticationService\n  \u2026          }\n            }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 93
    new-instance v1, Lcom/swedbank/mobile/data/authentication/PollCredentials;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/authentication/PollCredentials;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/g;->a(Lcom/swedbank/mobile/data/authentication/PollCredentials;)Lio/reactivex/w;

    move-result-object p1

    .line 94
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 95
    new-instance v0, Lcom/swedbank/mobile/data/authentication/c$i;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/authentication/c$i;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "authenticationService\n  \u2026uthenticationProgress() }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "personalCode"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 78
    new-instance v1, Lcom/swedbank/mobile/data/authentication/SmartIdCredentials;

    invoke-direct {v1, p1, p2}, Lcom/swedbank/mobile/data/authentication/SmartIdCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/g;->a(Lcom/swedbank/mobile/data/authentication/SmartIdCredentials;)Lio/reactivex/w;

    move-result-object p1

    .line 79
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 80
    new-instance p2, Lcom/swedbank/mobile/data/authentication/c$h;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/data/authentication/c$h;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "authenticationService\n  \u2026uthenticationProgress() }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 98
    new-instance v1, Lcom/swedbank/mobile/data/authentication/PollCredentials;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/authentication/PollCredentials;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/g;->b(Lcom/swedbank/mobile/data/authentication/PollCredentials;)Lio/reactivex/w;

    move-result-object p1

    .line 99
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 100
    new-instance v0, Lcom/swedbank/mobile/data/authentication/c$k;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/authentication/c$k;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "authenticationService\n  \u2026uthenticationProgress() }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mobileAppId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 83
    new-instance v1, Lcom/swedbank/mobile/data/authentication/SimpleIdCredentials;

    invoke-direct {v1, p1, p2}, Lcom/swedbank/mobile/data/authentication/SimpleIdCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/g;->a(Lcom/swedbank/mobile/data/authentication/SimpleIdCredentials;)Lio/reactivex/w;

    move-result-object p1

    .line 84
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 85
    new-instance p2, Lcom/swedbank/mobile/data/authentication/c$g;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/data/authentication/c$g;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "authenticationService\n  \u2026uthenticationProgress() }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public d(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 103
    new-instance v1, Lcom/swedbank/mobile/data/authentication/PollCredentials;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/authentication/PollCredentials;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/g;->c(Lcom/swedbank/mobile/data/authentication/PollCredentials;)Lio/reactivex/w;

    move-result-object p1

    .line 104
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 105
    new-instance v0, Lcom/swedbank/mobile/data/authentication/c$j;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/authentication/c$j;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "authenticationService\n  \u2026uthenticationProgress() }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "password"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c;->a:Lcom/swedbank/mobile/data/authentication/g;

    .line 88
    new-instance v1, Lcom/swedbank/mobile/data/authentication/PinCalculatorCredentials;

    invoke-direct {v1, p1, p2}, Lcom/swedbank/mobile/data/authentication/PinCalculatorCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/g;->a(Lcom/swedbank/mobile/data/authentication/PinCalculatorCredentials;)Lio/reactivex/w;

    move-result-object p1

    .line 89
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 90
    new-instance p2, Lcom/swedbank/mobile/data/authentication/c$f;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/data/authentication/c$f;-><init>(Lcom/swedbank/mobile/data/authentication/c;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "authenticationService\n  \u2026uthenticationProgress() }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
