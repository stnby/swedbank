.class public final Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;
.super Ljava/lang/Object;
.source "AuthenticationModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->a:Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/k$d;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->a:Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;

    sget-object v1, Lcom/swedbank/mobile/data/authentication/b;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 52
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/swedbank/mobile/business/authentication/k$d$a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/authentication/k$d$a;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/swedbank/mobile/business/authentication/k$d;

    goto :goto_0

    .line 51
    :pswitch_1
    new-instance v0, Lcom/swedbank/mobile/business/authentication/k$d$b;

    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/authentication/k$d$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/swedbank/mobile/business/authentication/k$d;

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->a:Lcom/swedbank/mobile/data/authentication/AuthenticationMessageType;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->b:Ljava/lang/String;

    return-object v0
.end method
