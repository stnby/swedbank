.class public final Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "AuthenticationResultJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/data/authentication/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final authenticationStateAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationState;",
            ">;"
        }
    .end annotation
.end field

.field private final listOfAuthenticationMessageAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nullableAuthenticationSessionAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationSession;",
            ">;"
        }
    .end annotation
.end field

.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;

.field private final stringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 5
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "userId"

    const-string v1, "state"

    const-string v2, "messages"

    const-string v3, "jwtResponse"

    const-string v4, "challengeCode"

    .line 17
    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"u\u2026sponse\", \"challengeCode\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 20
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "userId"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String>(St\u2026ons.emptySet(), \"userId\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 23
    const-class v0, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "state"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<Authentica\u2026ions.emptySet(), \"state\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->authenticationStateAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 26
    const-class v0, Ljava/util/List;

    check-cast v0, Ljava/lang/reflect/Type;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;

    check-cast v2, Ljava/lang/reflect/Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/moshi/p;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "messages"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<List<Authe\u2026s.emptySet(), \"messages\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->listOfAuthenticationMessageAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 29
    const-class v0, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "jwtResponse"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<Authentica\u2026mptySet(), \"jwtResponse\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->nullableAuthenticationSessionAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 32
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "challengeCode"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<String?>(S\u2026tySet(), \"challengeCode\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/authentication/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/authentication/AuthenticationResult;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/authentication/AuthenticationResult;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 82
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "userId"

    .line 83
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 84
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "state"

    .line 85
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 86
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->authenticationStateAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->c()Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "messages"

    .line 87
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->listOfAuthenticationMessageAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "jwtResponse"

    .line 89
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 90
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->nullableAuthenticationSessionAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->e()Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "challengeCode"

    .line 91
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 92
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->f()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 93
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 80
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p2, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/data/authentication/AuthenticationResult;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/authentication/AuthenticationResult;
    .locals 18
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "reader"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 37
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 38
    move-object v4, v2

    check-cast v4, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    .line 39
    move-object v5, v2

    check-cast v5, Ljava/util/List;

    .line 40
    check-cast v2, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    .line 44
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v6, 0x0

    move-object/from16 v16, v3

    const/4 v15, 0x0

    .line 45
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 46
    iget-object v7, v0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {v1, v7}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v7

    const/4 v8, 0x1

    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 55
    :pswitch_0
    iget-object v7, v0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v7, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v16, v7

    const/4 v15, 0x1

    goto :goto_0

    .line 51
    :pswitch_1
    iget-object v2, v0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->nullableAuthenticationSessionAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    const/4 v6, 0x1

    goto :goto_0

    .line 49
    :pswitch_2
    iget-object v5, v0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->listOfAuthenticationMessageAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v5, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'messages\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 48
    :pswitch_3
    iget-object v4, v0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->authenticationStateAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v4, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'state\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 47
    :pswitch_4
    iget-object v3, v0, Lcom/swedbank/mobile/data/authentication/AuthenticationResultJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_2

    goto/16 :goto_0

    :cond_2
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'userId\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 60
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->j()V

    .line 61
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->q()V

    goto/16 :goto_0

    .line 65
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->f()V

    .line 66
    new-instance v17, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;

    if-eqz v3, :cond_8

    if-eqz v4, :cond_7

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x1c

    const/4 v14, 0x0

    move-object/from16 v7, v17

    move-object v8, v3

    move-object v9, v4

    invoke-direct/range {v7 .. v14}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/data/authentication/AuthenticationState;Ljava/util/List;Lcom/swedbank/mobile/data/authentication/AuthenticationSession;Ljava/lang/String;ILkotlin/e/b/g;)V

    .line 69
    new-instance v1, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;

    if-eqz v5, :cond_4

    :goto_1
    move-object v10, v5

    goto :goto_2

    .line 72
    :cond_4
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->d()Ljava/util/List;

    move-result-object v5

    goto :goto_1

    :goto_2
    if-eqz v6, :cond_5

    :goto_3
    move-object v11, v2

    goto :goto_4

    .line 73
    :cond_5
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->e()Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    move-result-object v2

    goto :goto_3

    :goto_4
    if-eqz v15, :cond_6

    move-object/from16 v12, v16

    goto :goto_5

    .line 74
    :cond_6
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->f()Ljava/lang/String;

    move-result-object v2

    move-object v12, v2

    :goto_5
    move-object v7, v1

    move-object v8, v3

    move-object v9, v4

    .line 69
    invoke-direct/range {v7 .. v12}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/data/authentication/AuthenticationState;Ljava/util/List;Lcom/swedbank/mobile/data/authentication/AuthenticationSession;Ljava/lang/String;)V

    return-object v1

    .line 68
    :cond_7
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Required property \'state\' missing at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 67
    :cond_8
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Required property \'userId\' missing at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(AuthenticationResult)"

    return-object v0
.end method
