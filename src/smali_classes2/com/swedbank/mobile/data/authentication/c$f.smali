.class final Lcom/swedbank/mobile/data/authentication/c$f;
.super Ljava/lang/Object;
.source "AuthenticationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/c;->d(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/authentication/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/authentication/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/c$f;->a:Lcom/swedbank/mobile/data/authentication/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lretrofit2/q;)Lcom/swedbank/mobile/business/authentication/k;
    .locals 3
    .param p1    # Lretrofit2/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/q<",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationResult;",
            ">;)",
            "Lcom/swedbank/mobile/business/authentication/k;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c$f;->a:Lcom/swedbank/mobile/data/authentication/c;

    .line 150
    invoke-virtual {p1}, Lretrofit2/q;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    invoke-virtual {p1}, Lretrofit2/q;->e()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;

    .line 157
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->a()Lcom/swedbank/mobile/business/authentication/k;

    move-result-object p1

    goto :goto_0

    .line 154
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Successful response body is empty"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 159
    :cond_1
    invoke-virtual {p1}, Lretrofit2/q;->f()Lokhttp3/ad;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 165
    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/c;->e(Lcom/swedbank/mobile/data/authentication/c;)Lcom/swedbank/mobile/data/network/aa;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/aa;->b()Lretrofit2/r;

    move-result-object v1

    .line 169
    const-class v2, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;

    check-cast v2, Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/aa;->a()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lretrofit2/r;->b(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lretrofit2/f;

    move-result-object v0

    .line 168
    invoke-interface {v0, p1}, Lretrofit2/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 165
    check-cast p1, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;

    .line 178
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->a()Lcom/swedbank/mobile/business/authentication/k;

    move-result-object p1

    :goto_0
    return-object p1

    .line 175
    :cond_2
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 162
    :cond_3
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Unsuccessful response body is empty"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lretrofit2/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/c$f;->a(Lretrofit2/q;)Lcom/swedbank/mobile/business/authentication/k;

    move-result-object p1

    return-object p1
.end method
