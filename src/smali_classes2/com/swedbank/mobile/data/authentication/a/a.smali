.class public final Lcom/swedbank/mobile/data/authentication/a/a;
.super Ljava/lang/Object;
.source "LoginCredentialsRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/login/g;


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/a/a/a/f;)V
    .locals 2
    .param p1    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferences"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "lastLoggedInUserIdPreference"

    const-string v1, ""

    .line 35
    invoke-virtual {p1, v0, v1}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object v0

    const-string v1, "appPreferences.getString\u2026ST_LOGGED_IN_USER_ID, \"\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->a:Lcom/a/a/a/d;

    const-string v0, "loginUserId"

    const-string v1, ""

    .line 36
    invoke-virtual {p1, v0, v1}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object v0

    const-string v1, "appPreferences.getString(PREF_LOGIN_USER_ID, \"\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->b:Lcom/a/a/a/d;

    const-string v0, "lastUsedLoginMethod"

    const-string v1, ""

    .line 37
    invoke-virtual {p1, v0, v1}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object p1

    const-string v0, "appPreferences.getString\u2026ST_USED_LOGIN_METHOD, \"\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/a/a;->c:Lcom/a/a/a/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/authentication/a/a;)Lcom/a/a/a/d;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->a:Lcom/a/a/a/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/authentication/a/a;)Lcom/a/a/a/d;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->c:Lcom/a/a/a/d;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/authentication/a/a;)Lcom/a/a/a/d;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->b:Lcom/a/a/a/d;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/login/m;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginMethod"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/swedbank/mobile/data/authentication/a/a$d;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/data/authentication/a/a$d;-><init>(Lcom/swedbank/mobile/data/authentication/a/a;Lcom/swedbank/mobile/business/authentication/login/m;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p2

    .line 64
    new-instance v6, Lcom/swedbank/mobile/data/authentication/a/e;

    if-eqz p3, :cond_0

    :goto_0
    move-object v3, p3

    goto :goto_1

    :cond_0
    const-string p3, ""

    goto :goto_0

    :goto_1
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/data/authentication/a/e;-><init>(Ljava/lang/Long;Lcom/swedbank/mobile/business/authentication/login/m;Ljava/lang/String;ILkotlin/e/b/g;)V

    .line 133
    invoke-static {v6}, Lcom/siimkinks/sqlitemagic/cy$a;->a(Lcom/swedbank/mobile/data/authentication/a/e;)Lcom/siimkinks/sqlitemagic/cy$a;

    move-result-object p1

    .line 68
    sget-object p3, Lcom/siimkinks/sqlitemagic/bm;->a:Lcom/siimkinks/sqlitemagic/bm;

    iget-object p3, p3, Lcom/siimkinks/sqlitemagic/bm;->c:Lcom/siimkinks/sqlitemagic/dv;

    check-cast p3, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {p1, p3}, Lcom/siimkinks/sqlitemagic/cy$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/d;

    move-result-object p1

    .line 69
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/a/d;->d()Lio/reactivex/w;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    .line 64
    invoke-virtual {p2, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    const-string p2, "Completable.fromAction {\u2026)\n      .ignoreElement())"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/swedbank/mobile/data/authentication/a/a$e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/authentication/a/a$e;-><init>(Lcom/swedbank/mobile/data/authentication/a/a;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026dInUserId.set(userId)\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/o;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ")",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginMethod"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 87
    sget-object v1, Lcom/siimkinks/sqlitemagic/bm;->a:Lcom/siimkinks/sqlitemagic/bm;

    const-string v2, "LOGIN_INFO"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 88
    sget-object v1, Lcom/siimkinks/sqlitemagic/bm;->a:Lcom/siimkinks/sqlitemagic/bm;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bm;->c:Lcom/siimkinks/sqlitemagic/dv;

    const-string v2, "LOGIN_INFO.LOGIN_METHOD"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 141
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 90
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v0

    .line 91
    new-instance v7, Lcom/swedbank/mobile/data/authentication/a/e;

    const-string v4, ""

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v7

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/data/authentication/a/e;-><init>(Ljava/lang/Long;Lcom/swedbank/mobile/business/authentication/login/m;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v7}, Lcom/siimkinks/sqlitemagic/cg;->a(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 94
    new-instance v0, Lcom/swedbank/mobile/data/authentication/a/a$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/authentication/a/a$c;-><init>(Lcom/swedbank/mobile/data/authentication/a/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM L\u2026serId.get() to it.extra }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->a:Lcom/a/a/a/d;

    invoke-interface {v0}, Lcom/a/a/a/d;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "lastLoggedInUserId.get()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/authentication/a/a;->c()Lio/reactivex/o;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    const-string v1, "observeLastLoggedInUserId()\n      .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->a:Lcom/a/a/a/d;

    .line 45
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 46
    sget-object v1, Lcom/swedbank/mobile/data/authentication/a/a$a;->a:Lcom/swedbank/mobile/data/authentication/a/a$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "lastLoggedInUserId\n     \u2026 Absent\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/business/authentication/login/m;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/a/a;->c:Lcom/a/a/a/d;

    .line 73
    invoke-interface {v0}, Lcom/a/a/a/d;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "lastUsedLoginMethod\n      .get()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/String;

    .line 137
    invoke-static {}, Lcom/swedbank/mobile/business/authentication/login/m;->values()[Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v1

    .line 138
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 136
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/authentication/login/m;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    goto :goto_2

    .line 140
    :cond_2
    sget-object v4, Lcom/swedbank/mobile/business/authentication/login/m;->e:Lcom/swedbank/mobile/business/authentication/login/m;

    :goto_2
    return-object v4
.end method

.method public e()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 79
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 80
    sget-object v1, Lcom/siimkinks/sqlitemagic/bm;->a:Lcom/siimkinks/sqlitemagic/bm;

    const-string v2, "LOGIN_INFO"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object v0

    .line 82
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ab;->b()Lcom/siimkinks/sqlitemagic/al;

    move-result-object v0

    .line 83
    sget-object v1, Lcom/swedbank/mobile/data/authentication/a/a$b;->a:Lcom/swedbank/mobile/data/authentication/a/a$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/al;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM L\u2026)\n          .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
