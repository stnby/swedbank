.class final Lcom/swedbank/mobile/data/authentication/a/a$a;
.super Ljava/lang/Object;
.source "LoginCredentialsRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/a/a;->c()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/authentication/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/authentication/a/a$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/authentication/a/a$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/authentication/a/a$a;->a:Lcom/swedbank/mobile/data/authentication/a/a$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/l;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    .line 48
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    goto :goto_0

    .line 49
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/a/a$a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    return-object p1
.end method
