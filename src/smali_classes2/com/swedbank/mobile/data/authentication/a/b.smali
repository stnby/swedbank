.class public final Lcom/swedbank/mobile/data/authentication/a/b;
.super Ljava/lang/Object;
.source "LoginCredentialsRepositoryImpl.kt"


# direct methods
.method public static final a(Ljava/lang/String;)Lcom/swedbank/mobile/business/authentication/login/m;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "dbValue"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    const/16 v1, 0x35

    if-eq v0, v1, :cond_1

    const/16 v1, 0x39

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "9"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 128
    sget-object p0, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_1

    :cond_1
    const-string v0, "5"

    .line 127
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 129
    sget-object p0, Lcom/swedbank/mobile/business/authentication/login/m;->c:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_1

    :cond_2
    const-string v0, "2"

    .line 127
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 130
    sget-object p0, Lcom/swedbank/mobile/business/authentication/login/m;->d:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_1

    .line 131
    :cond_3
    :goto_0
    sget-object p0, Lcom/swedbank/mobile/business/authentication/login/m;->e:Lcom/swedbank/mobile/business/authentication/login/m;

    :goto_1
    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/authentication/login/m;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginMethod"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    sget-object v0, Lcom/swedbank/mobile/data/authentication/a/c;->a:[I

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 123
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    const-string p0, "0"

    goto :goto_0

    :pswitch_1
    const-string p0, "2"

    goto :goto_0

    :pswitch_2
    const-string p0, "5"

    goto :goto_0

    :pswitch_3
    const-string p0, "9"

    :goto_0
    return-object p0

    .line 119
    :pswitch_4
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Biometric login method cannot be saved as login method"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
