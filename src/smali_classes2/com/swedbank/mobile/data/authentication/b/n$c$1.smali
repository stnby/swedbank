.class final synthetic Lcom/swedbank/mobile/data/authentication/b/n$c$1;
.super Lkotlin/e/b/i;
.source "SessionRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/b/n$c;->a(Lcom/siimkinks/sqlitemagic/ca;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "TT;",
        "Lcom/swedbank/mobile/business/authentication/session/h;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/authentication/b/n;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/authentication/b/g;)Lcom/swedbank/mobile/business/authentication/session/h;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/data/authentication/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/swedbank/mobile/business/authentication/session/h;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/n$c$1;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/data/authentication/b/n;

    .line 133
    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->e(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/b/c/b;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    check-cast v1, Lcom/swedbank/mobile/business/util/l;

    .line 137
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 149
    :try_start_0
    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->b(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v1

    const-string v2, "session_key_alias"

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/d/a;->e(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    .line 151
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/Key;

    .line 158
    new-instance v2, Lcom/swedbank/mobile/data/authentication/b/n$a;

    invoke-direct {v2, v0, v1}, Lcom/swedbank/mobile/data/authentication/b/n$a;-><init>(Lcom/swedbank/mobile/data/authentication/b/n;Ljava/security/Key;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 163
    invoke-interface {p1, v2}, Lcom/swedbank/mobile/data/authentication/b/g;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object p1

    goto :goto_0

    .line 154
    :cond_0
    instance-of p1, v1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz p1, :cond_1

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 156
    throw p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :catch_0
    sget-object p1, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    .line 168
    :goto_0
    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->e(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/b/c/b;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    goto :goto_1

    .line 170
    :cond_2
    sget-object v1, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->a()V

    goto :goto_1

    .line 173
    :cond_3
    instance-of v0, v1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_5

    check-cast v1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/o;

    .line 135
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/data/authentication/b/g;->a(Lcom/swedbank/mobile/business/authentication/session/o;)Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object p1

    :cond_4
    :goto_1
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 134
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/swedbank/mobile/data/authentication/b/g;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/b/n$c$1;->a(Lcom/swedbank/mobile/data/authentication/b/g;)Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "toBusinessSession"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "toBusinessSession(Lcom/swedbank/mobile/data/authentication/session/PersistedSession;)Lcom/swedbank/mobile/business/authentication/session/Session;"

    return-object v0
.end method
