.class final synthetic Lcom/swedbank/mobile/data/authentication/b/j$3;
.super Lkotlin/e/b/i;
.source "ScopedSessionRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/b/j;-><init>(Lcom/swedbank/mobile/business/d/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/data/authentication/b/a;",
        "Lcom/siimkinks/sqlitemagic/cq$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/authentication/b/j$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/authentication/b/j$3;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/authentication/b/j$3;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/authentication/b/j$3;->a:Lcom/swedbank/mobile/data/authentication/b/j$3;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/authentication/b/a;)Lcom/siimkinks/sqlitemagic/cq$b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/authentication/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/cq$b;->a(Lcom/swedbank/mobile/data/authentication/b/a;)Lcom/siimkinks/sqlitemagic/cq$b;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 2

    const-class v0, Lcom/siimkinks/sqlitemagic/eg;

    const-string v1, "data_ltRelease"

    invoke-static {v0, v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/data/authentication/b/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/b/j$3;->a(Lcom/swedbank/mobile/data/authentication/b/a;)Lcom/siimkinks/sqlitemagic/cq$b;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "insert"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "insert(Lcom/swedbank/mobile/data/authentication/session/ActiveScopedSession;)Lcom/siimkinks/sqlitemagic/SqliteMagic_ActiveScopedSession_Handler$InsertBuilder;"

    return-object v0
.end method
