.class public final Lcom/swedbank/mobile/data/authentication/b/t;
.super Ljava/lang/Object;
.source "TokenAuthenticator.kt"

# interfaces
.implements Lokhttp3/b;


# instance fields
.field private final b:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "refreshSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "refreshSession"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/b/t;->b:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method


# virtual methods
.method public authenticate(Lokhttp3/ae;Lokhttp3/ac;)Lokhttp3/aa;
    .locals 6
    .param p1    # Lokhttp3/ae;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lokhttp3/ac;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string p1, "response"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p2}, Lokhttp3/ac;->k()Lokhttp3/ac;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return-object v0

    .line 29
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 30
    invoke-virtual {p2}, Lokhttp3/ac;->a()Lokhttp3/aa;

    move-result-object p1

    .line 31
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/t;->b:Lcom/swedbank/mobile/architect/business/b;

    new-instance v2, Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    const/4 v3, 0x0

    .line 33
    invoke-virtual {p2}, Lokhttp3/ac;->m()J

    move-result-wide v4

    .line 31
    invoke-direct {v2, v3, v4, v5}, Lcom/swedbank/mobile/business/authentication/session/refresh/a;-><init>(ZJ)V

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lio/reactivex/w;

    .line 34
    invoke-virtual {p2}, Lio/reactivex/w;->b()Ljava/lang/Object;

    move-result-object p2

    .line 31
    check-cast p2, Lcom/swedbank/mobile/business/authentication/session/h;

    .line 36
    instance-of v1, p2, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v1, :cond_1

    .line 37
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 38
    invoke-virtual {p1}, Lokhttp3/aa;->e()Lokhttp3/aa$a;

    move-result-object p1

    const-string v0, "Authorization"

    .line 39
    invoke-virtual {p1, v0}, Lokhttp3/aa$a;->b(Ljava/lang/String;)Lokhttp3/aa$a;

    move-result-object p1

    const-string v0, "Authorization"

    .line 40
    check-cast p2, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object p2

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bearer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/authentication/session/o;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 40
    invoke-virtual {p1, v0, p2}, Lokhttp3/aa$a;->b(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/aa$a;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lokhttp3/aa$a;->a()Lokhttp3/aa;

    move-result-object v0

    goto :goto_0

    .line 43
    :cond_1
    sget-object p1, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    invoke-static {p2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 44
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :goto_0
    return-object v0

    .line 45
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
