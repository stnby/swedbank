.class public final Lcom/swedbank/mobile/data/authentication/b/h;
.super Ljava/lang/Object;
.source "PriorToSessionRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/session/e;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 15
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    sget-object v1, Lcom/siimkinks/sqlitemagic/h;->a:Lcom/siimkinks/sqlitemagic/h;

    const-string v2, "ACTIVE_SESSION"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object v0

    .line 17
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ab;->b()Lcom/siimkinks/sqlitemagic/al;

    move-result-object v0

    const-string v1, "(SELECT FROM ACTIVE_SESS\u2026nt()\n          .observe()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/al;->a()Lio/reactivex/o;

    move-result-object v0

    const/4 v1, 0x0

    .line 19
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "(SELECT FROM ACTIVE_SESS\u2026o\n          .first(false)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
