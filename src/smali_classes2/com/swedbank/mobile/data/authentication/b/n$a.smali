.class public final Lcom/swedbank/mobile/data/authentication/b/n$a;
.super Lkotlin/e/b/k;
.source "SessionRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/authentication/b/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/authentication/b/n;

.field final synthetic b:Ljava/security/Key;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/authentication/b/n;Ljava/security/Key;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/b/n$a;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/b/n$a;->b:Ljava/security/Key;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "encryptedToken"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/n$a;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->b(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/n$a;->b:Ljava/security/Key;

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/business/d/a;->b(Ljava/lang/String;Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 134
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    .line 136
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 137
    throw p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/b/n$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
