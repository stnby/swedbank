.class public final Lcom/swedbank/mobile/data/authentication/b/n;
.super Ljava/lang/Object;
.source "SessionRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/session/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/swedbank/mobile/data/authentication/b/g;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/business/authentication/session/m;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/session/o;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/d/a;

.field private final c:Lcom/siimkinks/sqlitemagic/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/siimkinks/sqlitemagic/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "Lcom/swedbank/mobile/business/authentication/session/h$a;",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;TT;>;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "TT;",
            "Lcom/siimkinks/sqlitemagic/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/d/a;Lcom/siimkinks/sqlitemagic/dl;Ljavax/inject/Provider;Lkotlin/e/a/m;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/siimkinks/sqlitemagic/dl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/d/a;",
            "Lcom/siimkinks/sqlitemagic/dl<",
            "TT;>;",
            "Ljavax/inject/Provider<",
            "Lcom/siimkinks/sqlitemagic/a/b;",
            ">;",
            "Lkotlin/e/a/m<",
            "-",
            "Lcom/swedbank/mobile/business/authentication/session/h$a;",
            "-",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;+TT;>;",
            "Lkotlin/e/a/b<",
            "-TT;+",
            "Lcom/siimkinks/sqlitemagic/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "cryptoRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistedSessionTable"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deletePersistedSessionTableProvider"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "convertToPersistedSession"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "insertPersistedSessionBuilder"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/b/n;->b:Lcom/swedbank/mobile/business/d/a;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/b/n;->c:Lcom/siimkinks/sqlitemagic/dl;

    iput-object p3, p0, Lcom/swedbank/mobile/data/authentication/b/n;->d:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/swedbank/mobile/data/authentication/b/n;->e:Lkotlin/e/a/m;

    iput-object p5, p0, Lcom/swedbank/mobile/data/authentication/b/n;->f:Lkotlin/e/a/b;

    .line 37
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026l<SessionTokens>>(Absent)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/b/n;->a:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/authentication/b/n;)Ljavax/inject/Provider;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->d:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->b:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/authentication/b/n;)Lkotlin/e/a/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->f:Lkotlin/e/a/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/authentication/b/n;)Lkotlin/e/a/m;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->e:Lkotlin/e/a/m;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/b/c/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->a:Lcom/b/c/b;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "session"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/authentication/b/n;->e()Lio/reactivex/b;

    move-result-object p1

    goto :goto_0

    .line 70
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/data/authentication/b/n$d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/authentication/b/n$d;-><init>(Lcom/swedbank/mobile/data/authentication/b/n;Lcom/swedbank/mobile/business/authentication/session/h;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026Optional())\n      }\n    }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public a()V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->d:Ljavax/inject/Provider;

    .line 100
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/siimkinks/sqlitemagic/a/b;

    .line 101
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/b;->b()I

    .line 102
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->a:Lcom/b/c/b;

    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public b()Lcom/swedbank/mobile/business/authentication/session/h;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/n;->c:Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/authentication/b/g;

    if-eqz v0, :cond_7

    .line 44
    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/data/authentication/b/n;

    .line 133
    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->e(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/b/c/b;

    move-result-object v2

    .line 134
    invoke-virtual {v2}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    check-cast v2, Lcom/swedbank/mobile/business/util/l;

    .line 137
    sget-object v3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 149
    :try_start_0
    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->b(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v2

    const-string v3, "session_key_alias"

    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/d/a;->e(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v2

    .line 151
    instance-of v3, v2, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/security/Key;

    .line 158
    new-instance v3, Lcom/swedbank/mobile/data/authentication/b/n$a;

    invoke-direct {v3, v1, v2}, Lcom/swedbank/mobile/data/authentication/b/n$a;-><init>(Lcom/swedbank/mobile/data/authentication/b/n;Ljava/security/Key;)V

    check-cast v3, Lkotlin/e/a/b;

    .line 163
    invoke-interface {v0, v3}, Lcom/swedbank/mobile/data/authentication/b/g;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object v0

    goto :goto_0

    .line 154
    :cond_0
    instance-of v0, v2, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_1

    check-cast v2, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 156
    throw v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :catch_0
    sget-object v0, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/h;

    .line 168
    :goto_0
    instance-of v2, v0, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->e(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/b/c/b;

    move-result-object v1

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    goto :goto_1

    .line 170
    :cond_2
    sget-object v2, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->a()V

    goto :goto_1

    .line 173
    :cond_3
    instance-of v1, v2, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_5

    check-cast v2, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/authentication/session/o;

    .line 135
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/authentication/b/g;->a(Lcom/swedbank/mobile/business/authentication/session/o;)Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object v0

    :cond_4
    :goto_1
    if-eqz v0, :cond_7

    goto :goto_2

    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 134
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 44
    :cond_7
    sget-object v0, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/h;

    :goto_2
    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/authentication/session/o;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->a:Lcom/b/c/b;

    .line 176
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    .line 178
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/authentication/b/n;->b()Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object v0

    .line 50
    instance-of v1, v0, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_0
    sget-object v1, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 179
    :cond_2
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/o;

    :goto_0
    return-object v0

    .line 180
    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 176
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public d()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 57
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/n;->c:Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/swedbank/mobile/data/authentication/b/n$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/authentication/b/n$c;-><init>(Lcom/swedbank/mobile/data/authentication/b/n;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cg;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM p\u2026oObservable()\n          }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/n;->d:Ljavax/inject/Provider;

    .line 93
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/siimkinks/sqlitemagic/a/b;

    .line 94
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/a/b;->d()Lio/reactivex/w;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object v0

    .line 96
    new-instance v1, Lcom/swedbank/mobile/data/authentication/b/n$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/authentication/b/n$b;-><init>(Lcom/swedbank/mobile/data/authentication/b/n;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "deletePersistedSessionTa\u2026nsStream.accept(Absent) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
