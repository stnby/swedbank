.class final Lcom/swedbank/mobile/data/authentication/b/n$d;
.super Ljava/lang/Object;
.source "SessionRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/b/n;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/authentication/b/n;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/session/h;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/authentication/b/n;Lcom/swedbank/mobile/business/authentication/session/h;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->b:Lcom/swedbank/mobile/business/authentication/session/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 5

    .line 133
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->a(Lcom/swedbank/mobile/data/authentication/b/n;)Ljavax/inject/Provider;

    move-result-object v1

    .line 73
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/siimkinks/sqlitemagic/a/b;

    .line 74
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/a/b;->b()I

    .line 75
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->b(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v1

    const-string v2, "session_key_alias"

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/d/a;->d(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    .line 137
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/Key;

    .line 78
    new-instance v2, Lcom/swedbank/mobile/data/authentication/b/n$d$a;

    invoke-direct {v2, v1, p0}, Lcom/swedbank/mobile/data/authentication/b/n$d$a;-><init>(Ljava/security/Key;Lcom/swedbank/mobile/data/authentication/b/n$d;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 84
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->c(Lcom/swedbank/mobile/data/authentication/b/n;)Lkotlin/e/a/b;

    move-result-object v1

    .line 83
    iget-object v3, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-static {v3}, Lcom/swedbank/mobile/data/authentication/b/n;->d(Lcom/swedbank/mobile/data/authentication/b/n;)Lkotlin/e/a/m;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->b:Lcom/swedbank/mobile/business/authentication/session/h;

    .line 84
    invoke-interface {v3, v4, v2}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/siimkinks/sqlitemagic/a/c;

    .line 86
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/a/c;->a()J

    .line 87
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->a:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/b/n;->e(Lcom/swedbank/mobile/data/authentication/b/n;)Lcom/b/c/b;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/b/n$d;->b:Lcom/swedbank/mobile/business/authentication/session/h;

    check-cast v2, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 142
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    .line 139
    :cond_0
    :try_start_1
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_1

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    .line 140
    throw v1

    :cond_1
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    .line 144
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v1
.end method
