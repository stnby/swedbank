.class public final Lcom/swedbank/mobile/data/authentication/b/k;
.super Ljava/lang/Object;
.source "ScopedSessionRepositoryImpl.kt"


# direct methods
.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/a;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/swedbank/mobile/data/authentication/b/k;->b(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/a;

    move-result-object p0

    return-object p0
.end method

.method private static final b(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/a;
    .locals 12
    .param p0    # Lcom/swedbank/mobile/business/authentication/session/h$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/h$a;",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/data/authentication/b/a;"
        }
    .end annotation

    .line 53
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object v0

    .line 54
    new-instance v11, Lcom/swedbank/mobile/data/authentication/b/a;

    .line 55
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/session/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/lang/String;

    .line 56
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/session/o;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/h$a;->c()J

    move-result-wide v5

    .line 58
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/h$a;->d()J

    move-result-wide v7

    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v1, v11

    .line 54
    invoke-direct/range {v1 .. v10}, Lcom/swedbank/mobile/data/authentication/b/a;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;JJILkotlin/e/b/g;)V

    return-object v11
.end method
