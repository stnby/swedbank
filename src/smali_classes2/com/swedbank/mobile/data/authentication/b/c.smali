.class public final Lcom/swedbank/mobile/data/authentication/b/c;
.super Ljava/lang/Object;
.source "FullSessionRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/session/a;
.implements Lcom/swedbank/mobile/business/authentication/session/e;
.implements Lcom/swedbank/mobile/business/authentication/session/m;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/d/a;

.field private final b:Lcom/swedbank/mobile/business/authentication/session/e;

.field private final synthetic c:Lcom/swedbank/mobile/data/authentication/b/n;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/authentication/session/e;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/session/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cryptoRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priorToSessionRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/swedbank/mobile/data/authentication/b/n;

    .line 21
    sget-object v1, Lcom/siimkinks/sqlitemagic/h;->a:Lcom/siimkinks/sqlitemagic/h;

    const-string v2, "ACTIVE_SESSION"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v1

    check-cast v3, Lcom/siimkinks/sqlitemagic/dl;

    .line 22
    new-instance v1, Lcom/swedbank/mobile/data/authentication/b/c$1;

    sget-object v2, Lcom/siimkinks/sqlitemagic/i;->a:Lcom/siimkinks/sqlitemagic/i;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/data/authentication/b/c$1;-><init>(Lcom/siimkinks/sqlitemagic/i;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/data/authentication/b/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/authentication/b/e;-><init>(Lkotlin/e/a/a;)V

    move-object v4, v2

    check-cast v4, Ljavax/inject/Provider;

    .line 23
    sget-object v1, Lcom/swedbank/mobile/data/authentication/b/c$2;->a:Lcom/swedbank/mobile/data/authentication/b/c$2;

    move-object v5, v1

    check-cast v5, Lkotlin/e/a/m;

    .line 24
    sget-object v1, Lcom/swedbank/mobile/data/authentication/b/c$3;->a:Lcom/swedbank/mobile/data/authentication/b/c$3;

    move-object v6, v1

    check-cast v6, Lkotlin/e/a/b;

    move-object v1, v0

    move-object v2, p1

    .line 19
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/data/authentication/b/n;-><init>(Lcom/swedbank/mobile/business/d/a;Lcom/siimkinks/sqlitemagic/dl;Ljavax/inject/Provider;Lkotlin/e/a/m;Lkotlin/e/a/b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/c;->c:Lcom/swedbank/mobile/data/authentication/b/n;

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/b/c;->a:Lcom/swedbank/mobile/business/d/a;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/b/c;->b:Lcom/swedbank/mobile/business/authentication/session/e;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "session"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/c;->c:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/data/authentication/b/n;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public a()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/c;->b:Lcom/swedbank/mobile/business/authentication/session/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/e;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/business/authentication/session/h;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/c;->c:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->b()Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/authentication/session/o;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/c;->c:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->c()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/c;->c:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->d()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public e()Lio/reactivex/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/b/c;->c:Lcom/swedbank/mobile/data/authentication/b/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/b/n;->e()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
