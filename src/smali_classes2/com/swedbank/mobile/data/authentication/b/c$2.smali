.class final synthetic Lcom/swedbank/mobile/data/authentication/b/c$2;
.super Lkotlin/e/b/i;
.source "FullSessionRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/b/c;-><init>(Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/authentication/session/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/business/authentication/session/h$a;",
        "Lkotlin/e/a/b<",
        "-",
        "Ljava/lang/String;",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Lcom/swedbank/mobile/data/authentication/b/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/authentication/b/c$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/authentication/b/c$2;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/authentication/b/c$2;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/authentication/b/c$2;->a:Lcom/swedbank/mobile/data/authentication/b/c$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/h$a;",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/data/authentication/b/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p1, p2}, Lcom/swedbank/mobile/data/authentication/b/d;->a(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/b;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    check-cast p2, Lkotlin/e/a/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/authentication/b/c$2;->a(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/b;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 2

    const-class v0, Lcom/swedbank/mobile/data/authentication/b/d;

    const-string v1, "data_ltRelease"

    invoke-static {v0, v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "toPersistedSession"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "toPersistedSession(Lcom/swedbank/mobile/business/authentication/session/Session$Active;Lkotlin/jvm/functions/Function1;)Lcom/swedbank/mobile/data/authentication/session/ActiveSession;"

    return-object v0
.end method
