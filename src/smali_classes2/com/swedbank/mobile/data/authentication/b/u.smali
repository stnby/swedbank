.class public final Lcom/swedbank/mobile/data/authentication/b/u;
.super Ljava/lang/Object;
.source "TokenAuthenticator_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/authentication/b/t;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/b/u;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/data/authentication/b/u;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/swedbank/mobile/data/authentication/b/u;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/authentication/b/u;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/authentication/b/t;
    .locals 2

    .line 21
    new-instance v0, Lcom/swedbank/mobile/data/authentication/b/t;

    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/b/u;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/b;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/authentication/b/t;-><init>(Lcom/swedbank/mobile/architect/business/b;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/authentication/b/u;->a()Lcom/swedbank/mobile/data/authentication/b/t;

    move-result-object v0

    return-object v0
.end method
