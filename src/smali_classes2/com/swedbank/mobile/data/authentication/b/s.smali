.class public final Lcom/swedbank/mobile/data/authentication/b/s;
.super Ljava/lang/Object;
.source "SqliteMagic_ActiveSession_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/authentication/b/b;
    .locals 11

    .line 64
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 65
    iget v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v1, v1, 0x6

    iput v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 66
    new-instance p1, Lcom/swedbank/mobile/data/authentication/b/b;

    .line 67
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v3, v2

    goto :goto_0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v3, v1

    :goto_0
    add-int/lit8 v1, v0, 0x1

    .line 68
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v1, v0, 0x2

    .line 69
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v1, v0, 0x3

    .line 70
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v6, v2

    goto :goto_1

    :cond_1
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    :goto_1
    add-int/lit8 v1, v0, 0x4

    .line 71
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    add-int/lit8 v0, v0, 0x5

    .line 72
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    move-object v2, p1

    invoke-direct/range {v2 .. v10}, Lcom/swedbank/mobile/data/authentication/b/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-object p1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/authentication/b/b;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/authentication/b/b;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 78
    invoke-virtual/range {p2 .. p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 80
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v3

    :cond_0
    const-string v2, "active_session"

    .line 85
    :cond_1
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_4

    .line 87
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 88
    new-instance v2, Lcom/swedbank/mobile/data/authentication/b/b;

    .line 89
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v5, v3

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v5, v4

    :goto_0
    add-int/lit8 v4, v1, 0x1

    .line 90
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v4, v1, 0x2

    .line 91
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v4, v1, 0x3

    .line 92
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_3

    :goto_1
    move-object v8, v3

    goto :goto_2

    :cond_3
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :goto_2
    add-int/lit8 v3, v1, 0x4

    .line 93
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    add-int/lit8 v1, v1, 0x5

    .line 94
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    move-object v4, v2

    invoke-direct/range {v4 .. v12}, Lcom/swedbank/mobile/data/authentication/b/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-object v2

    .line 96
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".local_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 97
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".access_token"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_c

    .line 101
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".refresh_token"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_b

    .line 105
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".id_token"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 106
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".expires_in"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_a

    .line 110
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".created"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 114
    new-instance v2, Lcom/swedbank/mobile/data/authentication/b/b;

    if-eqz v4, :cond_6

    .line 115
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-eqz v9, :cond_5

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v10, v4

    goto :goto_4

    :cond_6
    :goto_3
    move-object v10, v3

    .line 116
    :goto_4
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 117
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    if-eqz v7, :cond_8

    .line 118
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_7

    goto :goto_5

    :cond_7
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_8
    :goto_5
    move-object v13, v3

    .line 119
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 120
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    move-object v9, v2

    invoke-direct/range {v9 .. v17}, Lcom/swedbank/mobile/data/authentication/b/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-object v2

    .line 112
    :cond_9
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"active_session\" required column \"created\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_a
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"active_session\" required column \"expires_in\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_b
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"active_session\" required column \"refresh_token\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_c
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"active_session\" required column \"access_token\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/authentication/b/b;)V
    .locals 3

    .line 37
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 38
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/b/b;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/b/b;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/b/b;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    .line 41
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/b/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    :cond_0
    const/4 v0, 0x4

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/b/b;->d()J

    move-result-wide v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    const/4 v0, 0x5

    .line 44
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/authentication/b/b;->e()J

    move-result-wide v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    return-void
.end method
