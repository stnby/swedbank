.class public final Lcom/swedbank/mobile/data/authentication/b/d;
.super Ljava/lang/Object;
.source "FullSessionRepositoryImpl.kt"


# direct methods
.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/b;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/swedbank/mobile/data/authentication/b/d;->b(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/b;

    move-result-object p0

    return-object p0
.end method

.method private static final b(Lcom/swedbank/mobile/business/authentication/session/h$a;Lkotlin/e/a/b;)Lcom/swedbank/mobile/data/authentication/b/b;
    .locals 13
    .param p0    # Lcom/swedbank/mobile/business/authentication/session/h$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/h$a;",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/data/authentication/b/b;"
        }
    .end annotation

    .line 56
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/session/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/lang/String;

    .line 59
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/session/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljava/lang/String;

    .line 60
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/session/o;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    :goto_0
    move-object v6, p1

    goto :goto_1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    .line 61
    :goto_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/h$a;->c()J

    move-result-wide v7

    .line 62
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/h$a;->d()J

    move-result-wide v9

    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 57
    new-instance p0, Lcom/swedbank/mobile/data/authentication/b/b;

    const/4 v3, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v12}, Lcom/swedbank/mobile/data/authentication/b/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJILkotlin/e/b/g;)V

    return-object p0
.end method
