.class public final Lcom/swedbank/mobile/data/authentication/AuthenticationSession;
.super Ljava/lang/Object;
.source "AuthenticationModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sessionID"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/x;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 64
    new-instance v0, Lcom/swedbank/mobile/business/authentication/x;

    .line 65
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;->a:Ljava/lang/String;

    .line 66
    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;->b:Ljava/lang/String;

    .line 64
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/x;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;->b:Ljava/lang/String;

    return-object v0
.end method
