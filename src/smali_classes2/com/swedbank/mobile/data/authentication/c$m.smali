.class final Lcom/swedbank/mobile/data/authentication/c$m;
.super Ljava/lang/Object;
.source "AuthenticationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/c;->a()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/authentication/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/authentication/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/c$m;->a:Lcom/swedbank/mobile/data/authentication/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lretrofit2/q;)Lio/reactivex/w;
    .locals 3
    .param p1    # Lretrofit2/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/q<",
            "Ljava/lang/Void;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/x;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c$m;->a:Lcom/swedbank/mobile/data/authentication/c;

    .line 150
    invoke-virtual {p1}, Lretrofit2/q;->c()Lokhttp3/s;

    move-result-object p1

    const-string v0, "Location"

    .line 151
    invoke-virtual {p1, v0}, Lokhttp3/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 152
    invoke-static {p1}, Lokhttp3/t;->f(Ljava/lang/String;)Lokhttp3/t;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    const-string v0, "sessionID"

    .line 37
    invoke-virtual {p1, v0}, Lokhttp3/t;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "sessionData"

    .line 38
    invoke-virtual {p1, v1}, Lokhttp3/t;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/data/authentication/c$m;->a:Lcom/swedbank/mobile/data/authentication/c;

    invoke-static {v1}, Lcom/swedbank/mobile/data/authentication/c;->a(Lcom/swedbank/mobile/data/authentication/c;)Lcom/swedbank/mobile/data/authentication/g;

    move-result-object v1

    .line 40
    new-instance v2, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/data/authentication/g;->a(Lcom/swedbank/mobile/data/authentication/AuthenticationSession;)Lio/reactivex/b;

    move-result-object v1

    .line 41
    new-instance v2, Lcom/swedbank/mobile/business/authentication/x;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/business/authentication/x;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-static {v2}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/aa;

    .line 41
    invoke-virtual {v1, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    return-object p1

    .line 165
    :cond_1
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Missing sessionData"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 160
    :cond_2
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Missing sessionID"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 155
    :cond_3
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Missing redirect URL"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lretrofit2/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/c$m;->a(Lretrofit2/q;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
