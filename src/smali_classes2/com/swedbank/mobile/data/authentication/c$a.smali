.class final Lcom/swedbank/mobile/data/authentication/c$a;
.super Ljava/lang/Object;
.source "AuthenticationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/authentication/c;->a(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/authentication/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/authentication/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/c$a;->a:Lcom/swedbank/mobile/data/authentication/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lretrofit2/q;)Lio/reactivex/w;
    .locals 8
    .param p1    # Lretrofit2/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/q<",
            "Ljava/lang/Void;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/c$a;->a:Lcom/swedbank/mobile/data/authentication/c;

    .line 150
    invoke-virtual {p1}, Lretrofit2/q;->c()Lokhttp3/s;

    move-result-object p1

    const-string v0, "Location"

    .line 151
    invoke-virtual {p1, v0}, Lokhttp3/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 152
    invoke-static {p1}, Lokhttp3/t;->f(Ljava/lang/String;)Lokhttp3/t;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    const-string v0, "code"

    .line 64
    invoke-virtual {p1, v0}, Lokhttp3/t;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 65
    iget-object p1, p0, Lcom/swedbank/mobile/data/authentication/c$a;->a:Lcom/swedbank/mobile/data/authentication/c;

    invoke-static {p1}, Lcom/swedbank/mobile/data/authentication/c;->a(Lcom/swedbank/mobile/data/authentication/c;)Lcom/swedbank/mobile/data/authentication/g;

    move-result-object v1

    .line 67
    iget-object p1, p0, Lcom/swedbank/mobile/data/authentication/c$a;->a:Lcom/swedbank/mobile/data/authentication/c;

    invoke-static {p1}, Lcom/swedbank/mobile/data/authentication/c;->d(Lcom/swedbank/mobile/data/authentication/c;)Ljava/lang/String;

    move-result-object v3

    .line 68
    iget-object p1, p0, Lcom/swedbank/mobile/data/authentication/c$a;->a:Lcom/swedbank/mobile/data/authentication/c;

    invoke-static {p1}, Lcom/swedbank/mobile/data/authentication/c;->c(Lcom/swedbank/mobile/data/authentication/c;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 65
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/data/authentication/g$a;->b(Lcom/swedbank/mobile/data/authentication/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    return-object p1

    .line 160
    :cond_1
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Missing authorization code"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 155
    :cond_2
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Missing redirect URL"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lretrofit2/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/authentication/c$a;->a(Lretrofit2/q;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
