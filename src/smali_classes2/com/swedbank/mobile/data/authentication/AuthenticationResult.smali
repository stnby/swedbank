.class public final Lcom/swedbank/mobile/data/authentication/AuthenticationResult;
.super Ljava/lang/Object;
.source "AuthenticationModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/authentication/AuthenticationState;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/data/authentication/AuthenticationSession;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/data/authentication/AuthenticationState;Ljava/util/List;Lcom/swedbank/mobile/data/authentication/AuthenticationSession;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/authentication/AuthenticationState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/authentication/AuthenticationSession;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationState;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;",
            ">;",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationSession;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messages"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->b:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    iput-object p3, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->d:Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    iput-object p5, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->e:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/data/authentication/AuthenticationState;Ljava/util/List;Lcom/swedbank/mobile/data/authentication/AuthenticationSession;Ljava/lang/String;ILkotlin/e/b/g;)V
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    .line 17
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p3

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p6, 0x8

    const/4 p7, 0x0

    if-eqz p3, :cond_1

    .line 18
    move-object p4, p7

    check-cast p4, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p6, 0x10

    if-eqz p3, :cond_2

    .line 19
    move-object p5, p7

    check-cast p5, Ljava/lang/String;

    :cond_2
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/data/authentication/AuthenticationState;Ljava/util/List;Lcom/swedbank/mobile/data/authentication/AuthenticationSession;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/k;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->b:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    sget-object v1, Lcom/swedbank/mobile/data/authentication/f;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/16 v1, 0xa

    packed-switch v0, :pswitch_data_0

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->c:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 113
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 114
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto/16 :goto_2

    .line 28
    :pswitch_0
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->e:Ljava/lang/String;

    .line 29
    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->c:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 109
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 110
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 111
    check-cast v2, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;

    .line 29
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->a()Lcom/swedbank/mobile/business/authentication/k$d;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 27
    new-instance v1, Lcom/swedbank/mobile/business/authentication/k$c;

    invoke-direct {v1, v0, v3}, Lcom/swedbank/mobile/business/authentication/k$c;-><init>(Ljava/lang/String;Ljava/util/List;)V

    check-cast v1, Lcom/swedbank/mobile/business/authentication/k;

    goto :goto_3

    .line 23
    :pswitch_1
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->d:Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    if-eqz v0, :cond_2

    .line 25
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/authentication/AuthenticationSession;->a()Lcom/swedbank/mobile/business/authentication/x;

    move-result-object v0

    .line 26
    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->c:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 105
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 106
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 107
    check-cast v2, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;

    .line 26
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->a()Lcom/swedbank/mobile/business/authentication/k$d;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    :cond_1
    check-cast v3, Ljava/util/List;

    .line 22
    new-instance v1, Lcom/swedbank/mobile/business/authentication/k$a;

    invoke-direct {v1, v0, v3}, Lcom/swedbank/mobile/business/authentication/k$a;-><init>(Lcom/swedbank/mobile/business/authentication/x;Ljava/util/List;)V

    check-cast v1, Lcom/swedbank/mobile/business/authentication/k;

    goto :goto_3

    .line 23
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Authenticated response missing JWT response"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 114
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 115
    check-cast v1, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;

    .line 31
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;->a()Lcom/swedbank/mobile/business/authentication/k$d;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 116
    :cond_3
    check-cast v2, Ljava/util/List;

    const/4 v0, 0x1

    .line 30
    new-instance v1, Lcom/swedbank/mobile/business/authentication/k$b;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2, v0, v3}, Lcom/swedbank/mobile/business/authentication/k$b;-><init>(Ljava/lang/Throwable;Ljava/util/List;ILkotlin/e/b/g;)V

    check-cast v1, Lcom/swedbank/mobile/business/authentication/k;

    :goto_3
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/data/authentication/AuthenticationState;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->b:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/authentication/AuthenticationMessage;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->c:Ljava/util/List;

    return-object v0
.end method

.method public final e()Lcom/swedbank/mobile/data/authentication/AuthenticationSession;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->d:Lcom/swedbank/mobile/data/authentication/AuthenticationSession;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AuthenticationResult;->e:Ljava/lang/String;

    return-object v0
.end method
