.class public final Lcom/swedbank/mobile/data/authentication/j;
.super Ljava/lang/Object;
.source "InternalServiceRequestInterceptor.kt"

# interfaces
.implements Lokhttp3/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/d;

.field private final c:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/data/authentication/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "userAgent"

    const-string v4, "getUserAgent()Ljava/lang/String;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/data/authentication/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/c/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferenceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/j;->c:Lcom/swedbank/mobile/business/c/a;

    .line 15
    sget-object p1, Lcom/swedbank/mobile/data/authentication/j$a;->a:Lcom/swedbank/mobile/data/authentication/j$a;

    check-cast p1, Lkotlin/e/a/a;

    invoke-static {p1}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/j;->b:Lkotlin/d;

    return-void
.end method

.method private final a()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/j;->b:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/data/authentication/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lokhttp3/u$a;)Lokhttp3/ac;
    .locals 3
    .param p1    # Lokhttp3/u$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-interface {p1}, Lokhttp3/u$a;->a()Lokhttp3/aa;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lokhttp3/aa;->e()Lokhttp3/aa$a;

    move-result-object v0

    const-string v1, "Content-Type"

    const-string v2, "application/json"

    .line 22
    invoke-virtual {v0, v1, v2}, Lokhttp3/aa$a;->b(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/aa$a;

    move-result-object v0

    const-string v1, "User-Agent"

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/data/authentication/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/aa$a;->b(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/aa$a;

    move-result-object v0

    const-string v1, "Accept-Language"

    .line 24
    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/j;->c:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/aa$a;->b(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/aa$a;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lokhttp3/aa$a;->a()Lokhttp3/aa;

    move-result-object v0

    .line 20
    invoke-interface {p1, v0}, Lokhttp3/u$a;->a(Lokhttp3/aa;)Lokhttp3/ac;

    move-result-object p1

    const-string v0, "chain\n      .proceed(cha\u2026name)\n          .build())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
