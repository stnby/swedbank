.class public final Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;
.super Ljava/lang/Object;
.source "AuthenticationModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "access_token"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refresh_token"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "token_type"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->d:Ljava/lang/String;

    iput-wide p5, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->e:J

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILkotlin/e/b/g;)V
    .locals 7

    and-int/lit8 p7, p7, 0x4

    if-eqz p7, :cond_0

    const/4 p3, 0x0

    .line 93
    check-cast p3, Ljava/lang/String;

    :cond_0
    move-object v3, p3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-wide v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/session/h;
    .locals 9
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 97
    new-instance v8, Lcom/swedbank/mobile/business/authentication/session/h$a;

    .line 98
    new-instance v1, Lcom/swedbank/mobile/business/authentication/session/o;

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->a:Ljava/lang/String;

    .line 100
    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->b:Ljava/lang/String;

    .line 101
    iget-object v3, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->c:Ljava/lang/String;

    .line 98
    invoke-direct {v1, v0, v2, v3}, Lcom/swedbank/mobile/business/authentication/session/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-wide v2, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->e:J

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v0, v8

    .line 97
    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/business/authentication/session/h$a;-><init>(Lcom/swedbank/mobile/business/authentication/session/o;JJILkotlin/e/b/g;)V

    check-cast v8, Lcom/swedbank/mobile/business/authentication/session/h;

    return-object v8
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final f()J
    .locals 2

    .line 95
    iget-wide v0, p0, Lcom/swedbank/mobile/data/authentication/AccessTokenResponse;->e:J

    return-wide v0
.end method
