.class public final Lcom/swedbank/mobile/data/authentication/a;
.super Ljava/lang/Object;
.source "AuthenticationCookieJar.kt"

# interfaces
.implements Lokhttp3/m;


# instance fields
.field private b:Landroidx/c/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/c/g<",
            "Lokhttp3/t;",
            "Ljava/util/List<",
            "Lokhttp3/l;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Landroidx/c/g;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroidx/c/g;-><init>(I)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/authentication/a;->b:Landroidx/c/g;

    return-void
.end method


# virtual methods
.method public a(Lokhttp3/t;)Ljava/util/List;
    .locals 3
    .param p1    # Lokhttp3/t;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/t;",
            ")",
            "Ljava/util/List<",
            "Lokhttp3/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object p1, p0, Lcom/swedbank/mobile/data/authentication/a;->b:Landroidx/c/g;

    invoke-virtual {p1}, Landroidx/c/g;->size()I

    move-result p1

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 19
    iget-object v2, p0, Lcom/swedbank/mobile/data/authentication/a;->b:Landroidx/c/g;

    invoke-virtual {v2, v1}, Landroidx/c/g;->c(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 21
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Lokhttp3/t;Ljava/util/List;)V
    .locals 1
    .param p1    # Lokhttp3/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/t;",
            "Ljava/util/List<",
            "Lokhttp3/l;",
            ">;)V"
        }
    .end annotation

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cookies"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/data/authentication/a;->b:Landroidx/c/g;

    invoke-virtual {v0, p1, p2}, Landroidx/c/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
