.class public final Lcom/swedbank/mobile/data/authentication/SecurityLevelAdapter;
.super Ljava/lang/Object;
.source "SecurityLevelAdapter.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromJson(I)Lcom/swedbank/mobile/business/authentication/w;
    .locals 6
    .annotation runtime Lcom/squareup/moshi/c;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 13
    invoke-static {}, Lcom/swedbank/mobile/business/authentication/w;->values()[Lcom/swedbank/mobile/business/authentication/w;

    move-result-object v0

    .line 16
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 14
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/authentication/w;->a()I

    move-result v5

    if-ne v5, p1, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    sget-object v4, Lcom/swedbank/mobile/business/authentication/w;->a:Lcom/swedbank/mobile/business/authentication/w;

    :goto_3
    return-object v4
.end method

.method public final toJson(Lcom/swedbank/mobile/business/authentication/w;)I
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/moshi/o;
    .end annotation

    const-string v0, "method"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/w;->a()I

    move-result p1

    return p1
.end method
