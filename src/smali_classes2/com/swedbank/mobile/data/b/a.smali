.class public final Lcom/swedbank/mobile/data/b/a;
.super Ljava/lang/Object;
.source "AppPreferenceRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/c/a;


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/c/b;

.field private final c:Landroid/app/Application;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/b;Landroid/app/Application;Lcom/a/a/a/f;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "app"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferences"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/b/a;->b:Lcom/swedbank/mobile/business/c/b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/b/a;->c:Landroid/app/Application;

    const-string p1, "languagePreference"

    .line 73
    invoke-static {p0}, Lcom/swedbank/mobile/data/b/a;->a(Lcom/swedbank/mobile/data/b/a;)Lcom/swedbank/mobile/business/c/b;

    move-result-object p2

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/c/b;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object p2

    .line 74
    sget-object v0, Lcom/swedbank/mobile/business/c/g;->g:Lcom/swedbank/mobile/business/c/g$a;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "Locale.getDefault()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Locale.getDefault().language"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/c/g$a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/c/g;

    move-result-object v0

    .line 76
    invoke-virtual {p2, v0}, Lcom/swedbank/mobile/business/c/c;->a(Lcom/swedbank/mobile/business/c/g;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/c/c;->a()Lcom/swedbank/mobile/business/c/g;

    move-result-object p2

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object p2

    .line 27
    :goto_0
    invoke-virtual {p3, p1, p2}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object p1

    const-string p2, "appPreferences.getString\u2026GUAGE, defaultLanguage())"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/b/a;->a:Lcom/a/a/a/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/b/a;)Lcom/swedbank/mobile/business/c/b;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/data/b/a;->b:Lcom/swedbank/mobile/business/c/b;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    new-instance v0, Lcom/swedbank/mobile/data/b/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/b/a$a;-><init>(Lcom/swedbank/mobile/data/b/a;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.fromCallable {\n  \u2026  .supportedLanguages\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/swedbank/mobile/business/c/g;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "language"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/data/b/a;->b:Lcom/swedbank/mobile/business/c/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/b;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    const-string v1, "resources"

    .line 53
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 54
    invoke-virtual {v0, p2}, Lcom/swedbank/mobile/business/c/c;->b(Lcom/swedbank/mobile/business/c/g;)Ljava/util/Locale;

    move-result-object p2

    .line 55
    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 56
    invoke-static {p2}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 57
    iput-object p2, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 58
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/c/g;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "language"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/data/b/a;->a:Lcom/a/a/a/d;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/a/a/d;->a(Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/data/b/a;->c:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lcom/swedbank/mobile/data/b/a;->a(Landroid/content/Context;Lcom/swedbank/mobile/business/c/g;)V

    return-void
.end method

.method public b()Lcom/swedbank/mobile/business/c/g;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/data/b/a;->a:Lcom/a/a/a/d;

    invoke-interface {v0}, Lcom/a/a/a/d;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/String;

    .line 37
    sget-object v1, Lcom/swedbank/mobile/business/c/g;->g:Lcom/swedbank/mobile/business/c/g$a;

    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/business/c/g$a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/c/g;

    move-result-object v0

    return-object v0

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "language preference == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/data/b/a;->a:Lcom/a/a/a/d;

    .line 41
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/swedbank/mobile/data/b/a$b;

    sget-object v2, Lcom/swedbank/mobile/business/c/g;->g:Lcom/swedbank/mobile/business/c/g$a;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/data/b/a$b;-><init>(Lcom/swedbank/mobile/business/c/g$a;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/data/b/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/b/b;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "language\n      .asObserv\u2026anguage.Companion::parse)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/business/c/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/data/b/a;->b:Lcom/swedbank/mobile/business/c/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/b;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    return-object v0
.end method
