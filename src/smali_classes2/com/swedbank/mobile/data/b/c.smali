.class public final Lcom/swedbank/mobile/data/b/c;
.super Ljava/lang/Object;
.source "AppPreferenceRepositoryImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/b/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/a/a/a/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/a/a/a/f;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/data/b/c;->a:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/swedbank/mobile/data/b/c;->b:Ljavax/inject/Provider;

    .line 21
    iput-object p3, p0, Lcom/swedbank/mobile/data/b/c;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/a/a/a/f;",
            ">;)",
            "Lcom/swedbank/mobile/data/b/c;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/swedbank/mobile/data/b/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/data/b/c;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/b/a;
    .locals 4

    .line 26
    new-instance v0, Lcom/swedbank/mobile/data/b/a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/b/c;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/c/b;

    iget-object v2, p0, Lcom/swedbank/mobile/data/b/c;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    iget-object v3, p0, Lcom/swedbank/mobile/data/b/c;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/a/a/a/f;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/data/b/a;-><init>(Lcom/swedbank/mobile/business/c/b;Landroid/app/Application;Lcom/a/a/a/f;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/b/c;->a()Lcom/swedbank/mobile/data/b/a;

    move-result-object v0

    return-object v0
.end method
