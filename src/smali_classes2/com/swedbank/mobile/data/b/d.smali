.class public final Lcom/swedbank/mobile/data/b/d;
.super Ljava/lang/Object;
.source "ProductionCountryConfiguration.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/c/d;


# static fields
.field public static final a:Lcom/swedbank/mobile/data/b/d;

.field private static final b:Lcom/swedbank/mobile/business/c/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 7
    new-instance v0, Lcom/swedbank/mobile/data/b/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/b/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/b/d;->a:Lcom/swedbank/mobile/data/b/d;

    const-string v0, "lt"

    .line 8
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toUpperCase()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/swedbank/mobile/business/c/c;->valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/data/b/d;->b:Lcom/swedbank/mobile/business/c/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public j()Lcom/swedbank/mobile/business/c/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 9
    sget-object v0, Lcom/swedbank/mobile/data/b/d;->b:Lcom/swedbank/mobile/business/c/c;

    return-object v0
.end method
