.class final Lcom/swedbank/mobile/data/device/b$k;
.super Ljava/lang/Object;
.source "DeviceRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/device/b;->f()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/device/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/device/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b$k;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b$k;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-static {v0}, Lcom/swedbank/mobile/data/device/b;->c(Lcom/swedbank/mobile/data/device/b;)Landroid/app/KeyguardManager;

    move-result-object v0

    .line 150
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isDeviceSecure()Z

    move-result v0

    return v0

    .line 151
    :cond_0
    new-instance v0, Lkotlin/j;

    const-string v1, "This call is incompatible with API level < 23"

    invoke-direct {v0, v1}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/device/b$k;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
