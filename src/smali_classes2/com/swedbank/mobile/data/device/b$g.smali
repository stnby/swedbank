.class final Lcom/swedbank/mobile/data/device/b$g;
.super Ljava/lang/Object;
.source "DeviceRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/device/b;->d()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/device/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/device/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b$g;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/e/g;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b$g;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-static {v0}, Lcom/swedbank/mobile/data/device/b;->a(Lcom/swedbank/mobile/data/device/b;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.nfc"

    .line 133
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/device/b$g;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-static {v1}, Lcom/swedbank/mobile/data/device/b;->b(Lcom/swedbank/mobile/data/device/b;)Landroid/nfc/NfcManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/nfc/NfcManager;->getDefaultAdapter()Landroid/nfc/NfcAdapter;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    const-string v2, "android.hardware.nfc.hce"

    .line 135
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 137
    new-instance v2, Lcom/swedbank/mobile/business/e/g;

    .line 138
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 137
    invoke-direct {v2, v3, v1, v0}, Lcom/swedbank/mobile/business/e/g;-><init>(IZZ)V

    return-object v2
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/device/b$g;->a()Lcom/swedbank/mobile/business/e/g;

    move-result-object v0

    return-object v0
.end method
