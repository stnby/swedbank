.class final Lcom/swedbank/mobile/data/device/k$b;
.super Ljava/lang/Object;
.source "IsDeviceRooted.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/device/k;->a()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/device/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/device/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/k$b;->a:Lcom/swedbank/mobile/data/device/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;)",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "rootReason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/k$b;->a:Lcom/swedbank/mobile/data/device/k;

    invoke-static {v0}, Lcom/swedbank/mobile/data/device/k;->a(Lcom/swedbank/mobile/data/device/k;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    .line 25
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/e/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/e/i;->a(Lcom/swedbank/mobile/business/e/l;)Lio/reactivex/b;

    move-result-object v0

    .line 26
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    check-cast p1, Lio/reactivex/n;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/device/k$b;->a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
