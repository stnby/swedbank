.class final Lcom/swedbank/mobile/data/device/b$n;
.super Ljava/lang/Object;
.source "DeviceRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/device/b;->h()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/q<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/device/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/device/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b$n;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/p;)V
    .locals 4
    .param p1    # Lio/reactivex/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    new-instance v0, Lcom/swedbank/mobile/data/device/b$n$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/device/b$n$a;-><init>(Lio/reactivex/p;)V

    .line 168
    new-instance v1, Lcom/swedbank/mobile/data/device/b$n$1;

    invoke-direct {v1, p0, v0}, Lcom/swedbank/mobile/data/device/b$n$1;-><init>(Lcom/swedbank/mobile/data/device/b$n;Lcom/swedbank/mobile/data/device/b$n$a;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    invoke-interface {p1, v1}, Lio/reactivex/p;->a(Lio/reactivex/b/c;)V

    .line 171
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/b$n;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-static {v1}, Lcom/swedbank/mobile/data/device/b;->a(Lcom/swedbank/mobile/data/device/b;)Landroid/app/Application;

    move-result-object v1

    check-cast v0, Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.nfc.action.ADAPTER_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/Application;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 172
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b$n;->a:Lcom/swedbank/mobile/data/device/b;

    .line 270
    invoke-static {v0}, Lcom/swedbank/mobile/data/device/b;->b(Lcom/swedbank/mobile/data/device/b;)Landroid/nfc/NfcManager;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/nfc/NfcManager;->getDefaultAdapter()Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 172
    invoke-interface {p1, v0}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    return-void
.end method
