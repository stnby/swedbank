.class public final Lcom/swedbank/mobile/data/device/b;
.super Ljava/lang/Object;
.source "DeviceRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/e/i;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Ljava/io/File;

.field private final c:Lkotlin/d;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lkotlin/d;

.field private final e:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Landroid/app/Application;

.field private final h:Landroid/app/KeyguardManager;

.field private final i:Lcom/a/a/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/data/device/b;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "deviceId"

    const-string v4, "getDeviceId()Ljava/lang/String;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/data/device/b;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "nfcManager"

    const-string v4, "getNfcManager()Landroid/nfc/NfcManager;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/data/device/b;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Landroid/app/KeyguardManager;Lcom/a/a/a/f;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/KeyguardManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keyguardManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferences"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/data/device/b;->h:Landroid/app/KeyguardManager;

    iput-object p3, p0, Lcom/swedbank/mobile/data/device/b;->i:Lcom/a/a/a/f;

    .line 50
    iget-object p1, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    invoke-virtual {p1}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b;->b:Ljava/io/File;

    .line 51
    new-instance p1, Lcom/swedbank/mobile/data/device/b$e;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/data/device/b$e;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast p1, Lkotlin/e/a/a;

    invoke-static {p1}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b;->c:Lkotlin/d;

    .line 52
    new-instance p1, Lcom/swedbank/mobile/data/device/b$l;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/data/device/b$l;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast p1, Lkotlin/e/a/a;

    invoke-static {p1}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b;->d:Lkotlin/d;

    .line 53
    iget-object p1, p0, Lcom/swedbank/mobile/data/device/b;->i:Lcom/a/a/a/f;

    const-string p2, "deviceEligibilityForWalletPreference"

    .line 54
    sget-object p3, Lcom/swedbank/mobile/business/e/p$d;->a:Lcom/swedbank/mobile/business/e/p$d;

    check-cast p3, Lcom/swedbank/mobile/business/e/p;

    invoke-virtual {p0, p3}, Lcom/swedbank/mobile/data/device/b;->b(Lcom/swedbank/mobile/business/e/p;)Ljava/lang/String;

    move-result-object p3

    .line 53
    invoke-virtual {p1, p2, p3}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object p1

    const-string p2, "appPreferences.getString\u2026Undetermined.serialize())"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b;->e:Lcom/a/a/a/d;

    .line 57
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026onal<RootReason>>(Absent)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b;->f:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/device/b;)Landroid/app/Application;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/device/b;)Landroid/nfc/NfcManager;
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/swedbank/mobile/data/device/b;->r()Landroid/nfc/NfcManager;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/device/b;)Landroid/app/KeyguardManager;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/b;->h:Landroid/app/KeyguardManager;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/device/b;)Lcom/b/c/b;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/b;->f:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/data/device/b;)Lcom/a/a/a/d;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/b;->e:Lcom/a/a/a/d;

    return-object p0
.end method

.method private final r()Landroid/nfc/NfcManager;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b;->d:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/data/device/b;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/nfc/NfcManager;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/business/e/p;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "raw"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eligible"

    .line 211
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/business/e/p$a;->a:Lcom/swedbank/mobile/business/e/p$a;

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    goto :goto_0

    :cond_0
    const-string v0, "not_eligible"

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 212
    invoke-static {p1, v0, v1, v2, v3}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "not_eligible"

    .line 215
    sget-object v1, Lcom/swedbank/mobile/business/e/p$c;->f:Lcom/swedbank/mobile/business/e/p$c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/e/p$c;->toString()Ljava/lang/String;

    move-result-object v1

    .line 213
    invoke-static {p1, v0, v1}, Lkotlin/j/n;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 216
    new-instance v0, Lcom/swedbank/mobile/business/e/p$b;

    invoke-static {p1}, Lcom/swedbank/mobile/business/e/p$c;->valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/e/p$c;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    goto :goto_0

    .line 218
    :cond_1
    sget-object p1, Lcom/swedbank/mobile/business/e/p$d;->a:Lcom/swedbank/mobile/business/e/p$d;

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    :goto_0
    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/e/l;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/l;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 191
    new-instance v0, Lcom/swedbank/mobile/data/device/b$p;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/device/b$p;-><init>(Lcom/swedbank/mobile/data/device/b;Lcom/swedbank/mobile/business/e/l;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026(reason.toOptional())\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "eligibility"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    new-instance v0, Lcom/swedbank/mobile/data/device/b$o;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/device/b$o;-><init>(Lcom/swedbank/mobile/data/device/b;Lcom/swedbank/mobile/business/e/p;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .fromA\u2026ligibility.serialize()) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public varargs a([Ljava/lang/String;)Lio/reactivex/b;
    .locals 1
    .param p1    # [Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "databaseName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    new-instance v0, Lcom/swedbank/mobile/data/device/b$d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/device/b$d;-><init>(Lcom/swedbank/mobile/data/device/b;[Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026.deleteDatabase(it) }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b;->c:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/data/device/b;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 3

    const/4 v0, 0x0

    .line 114
    :try_start_0
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 115
    iget-object v2, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :catch_0
    return v0
.end method

.method public final b(Lcom/swedbank/mobile/business/e/p;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/e/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$serialize"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    sget-object v0, Lcom/swedbank/mobile/business/e/p$d;->a:Lcom/swedbank/mobile/business/e/p$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    goto :goto_0

    .line 205
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/e/p$a;->a:Lcom/swedbank/mobile/business/e/p$a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "eligible"

    goto :goto_0

    .line 206
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/e/p$b;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "not_eligible"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Lcom/swedbank/mobile/business/e/p$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/p$b;->a()Lcom/swedbank/mobile/business/e/p$c;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public c()Lcom/swedbank/mobile/business/e/h;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 124
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v0, "Build.MANUFACTURER"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v0, "Build.MODEL"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 127
    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v0, "Build.VERSION.RELEASE"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    .line 318
    :try_start_0
    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 321
    invoke-virtual {v0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v5, "packageManager\n         \u20260)\n          .versionName"

    invoke-static {v0, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, ""

    :goto_0
    move-object v5, v0

    .line 123
    new-instance v6, Lcom/swedbank/mobile/business/e/h;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/e/h;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method public d()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/e/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 131
    new-instance v0, Lcom/swedbank/mobile/data/device/b$g;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/device/b$g;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    .line 141
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single\n      .fromCallab\u2026scribeOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Landroid/content/Intent;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 144
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public f()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 148
    new-instance v0, Lcom/swedbank/mobile/data/device/b$k;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/device/b$k;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.fromCallable {\n  \u2026PI level < 23\")\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 156
    new-instance v0, Lcom/swedbank/mobile/data/device/b$j;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/data/device/b;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/device/b$j;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/data/device/f;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/device/f;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single\n      .fromCallable(this::checkNfcEnabled)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public h()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 159
    new-instance v0, Lcom/swedbank/mobile/data/device/b$n;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/device/b$n;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast v0, Lio/reactivex/q;

    invoke-static {v0}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .create\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 178
    new-instance v0, Lcom/swedbank/mobile/data/device/b$i;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/device/b$i;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.fromCallable {\n  \u2026PowerSaveMode == true\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b;->f:Lcom/b/c/b;

    invoke-virtual {v0}, Lcom/b/c/b;->j()Lio/reactivex/w;

    move-result-object v0

    const-string v1, "rootReason.firstOrError()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/e/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 195
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/b;->e:Lcom/a/a/a/d;

    .line 196
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 197
    new-instance v1, Lcom/swedbank/mobile/data/device/b$m;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/data/device/b;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/data/device/b$m;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/data/device/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/device/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "deviceEligibilityForWall\u2026rializeWalletEligibility)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public l()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 223
    new-instance v0, Lcom/swedbank/mobile/data/device/b$b;

    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->b()Lcom/siimkinks/sqlitemagic/ar;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/device/b$b;-><init>(Lcom/siimkinks/sqlitemagic/ar;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/data/device/d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/device/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromAction(S\u2026tConnection()::clearData)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public m()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 238
    new-instance v0, Lcom/swedbank/mobile/data/device/b$a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/device/b;->i:Lcom/a/a/a/f;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/device/b$a;-><init>(Lcom/a/a/a/f;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/data/device/d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/device/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromAction(appPreferences::clear)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public n()Lio/reactivex/b;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ApplySharedPref"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 242
    new-instance v0, Lcom/swedbank/mobile/data/device/b$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/device/b$c;-><init>(Lcom/swedbank/mobile/data/device/b;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromAction {\u2026t()\n          }\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public o()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 229
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/device/b;->q()Lio/reactivex/w;

    move-result-object v0

    .line 230
    sget-object v1, Lcom/swedbank/mobile/data/device/b$h;->a:Lcom/swedbank/mobile/data/device/b$h;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "getDeviceDatabaseNames()\u2026ral-db\"\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public p()Z
    .locals 2

    .line 121
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/device/b;->b()I

    move-result v0

    const/16 v1, 0x5208

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public q()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 221
    new-instance v0, Lcom/swedbank/mobile/data/device/b$f;

    iget-object v1, p0, Lcom/swedbank/mobile/data/device/b;->g:Landroid/app/Application;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/device/b$f;-><init>(Landroid/app/Application;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/data/device/f;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/device/f;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.fromCallable(application::databaseList)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
