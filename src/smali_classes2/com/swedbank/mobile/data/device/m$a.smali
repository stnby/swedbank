.class final Lcom/swedbank/mobile/data/device/m$a;
.super Ljava/lang/Object;
.source "SystemNotificationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/device/m;->a(Lcom/swedbank/mobile/business/e/m;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/device/m;

.field final synthetic b:Lcom/swedbank/mobile/business/e/m;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/device/m;Lcom/swedbank/mobile/business/e/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/m$a;->a:Lcom/swedbank/mobile/data/device/m;

    iput-object p2, p0, Lcom/swedbank/mobile/data/device/m$a;->b:Lcom/swedbank/mobile/business/e/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 6

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/m$a;->b:Lcom/swedbank/mobile/business/e/m;

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/m$a;->a:Lcom/swedbank/mobile/data/device/m;

    .line 80
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-lt v2, v3, :cond_0

    .line 81
    new-instance v2, Landroid/app/NotificationChannel;

    .line 86
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/m;->a()Ljava/lang/String;

    move-result-object v3

    .line 87
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/m;->b()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x4

    .line 81
    invoke-direct {v2, v3, v4, v5}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 85
    invoke-static {v1}, Lcom/swedbank/mobile/data/device/m;->b(Lcom/swedbank/mobile/data/device/m;)Lcom/swedbank/mobile/business/e/o;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/swedbank/mobile/business/e/o;->a(Landroid/app/NotificationChannel;)Landroid/app/NotificationChannel;

    move-result-object v2

    .line 90
    invoke-static {v1}, Lcom/swedbank/mobile/data/device/m;->e(Lcom/swedbank/mobile/data/device/m;)Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 37
    :cond_0
    new-instance v1, Landroidx/core/app/i$d;

    iget-object v2, p0, Lcom/swedbank/mobile/data/device/m$a;->a:Lcom/swedbank/mobile/data/device/m;

    invoke-static {v2}, Lcom/swedbank/mobile/data/device/m;->a(Lcom/swedbank/mobile/data/device/m;)Landroid/app/Application;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/m;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroidx/core/app/i$d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 38
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/m;->c()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroidx/core/app/i$d;->a(Ljava/lang/CharSequence;)Landroidx/core/app/i$d;

    move-result-object v1

    .line 39
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/m;->d()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroidx/core/app/i$d;->b(Ljava/lang/CharSequence;)Landroidx/core/app/i$d;

    move-result-object v1

    .line 41
    new-instance v2, Landroidx/core/app/i$c;

    invoke-direct {v2}, Landroidx/core/app/i$c;-><init>()V

    .line 42
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/m;->c()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroidx/core/app/i$c;->a(Ljava/lang/CharSequence;)Landroidx/core/app/i$c;

    move-result-object v2

    .line 43
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/m;->d()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroidx/core/app/i$c;->b(Ljava/lang/CharSequence;)Landroidx/core/app/i$c;

    move-result-object v0

    check-cast v0, Landroidx/core/app/i$e;

    .line 40
    invoke-virtual {v1, v0}, Landroidx/core/app/i$d;->a(Landroidx/core/app/i$e;)Landroidx/core/app/i$d;

    move-result-object v0

    const/4 v1, 0x1

    .line 44
    invoke-virtual {v0, v1}, Landroidx/core/app/i$d;->b(I)Landroidx/core/app/i$d;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v1}, Landroidx/core/app/i$d;->a(Z)Landroidx/core/app/i$d;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/m$a;->a:Lcom/swedbank/mobile/data/device/m;

    iget-object v2, p0, Lcom/swedbank/mobile/data/device/m$a;->b:Lcom/swedbank/mobile/business/e/m;

    .line 95
    invoke-static {v1}, Lcom/swedbank/mobile/data/device/m;->f(Lcom/swedbank/mobile/data/device/m;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v3

    .line 102
    invoke-interface {v3}, Lcom/swedbank/mobile/business/e/i;->e()Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    .line 101
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v3

    .line 100
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "deviceRepository\n       \u2026.randomUUID().toString())"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-interface {v2}, Lcom/swedbank/mobile/business/e/m;->e()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v3, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 109
    :cond_1
    invoke-static {v1}, Lcom/swedbank/mobile/data/device/m;->a(Lcom/swedbank/mobile/data/device/m;)Landroid/app/Application;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 110
    invoke-static {v1}, Lcom/swedbank/mobile/data/device/m;->d(Lcom/swedbank/mobile/data/device/m;)Ljava/util/Random;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    const/4 v4, 0x0

    .line 108
    invoke-static {v2, v1, v3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "PendingIntent.getActivit\u2026ctivityIntent,\n        0)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {v0, v1}, Landroidx/core/app/i$d;->a(Landroid/app/PendingIntent;)Landroidx/core/app/i$d;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/m$a;->a:Lcom/swedbank/mobile/data/device/m;

    invoke-static {v1}, Lcom/swedbank/mobile/data/device/m;->b(Lcom/swedbank/mobile/data/device/m;)Lcom/swedbank/mobile/business/e/o;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/e/o;->a(Landroidx/core/app/i$d;)Landroidx/core/app/i$d;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/m$a;->a:Lcom/swedbank/mobile/data/device/m;

    invoke-static {v1}, Lcom/swedbank/mobile/data/device/m;->c(Lcom/swedbank/mobile/data/device/m;)Landroidx/core/app/l;

    move-result-object v1

    .line 49
    iget-object v2, p0, Lcom/swedbank/mobile/data/device/m$a;->a:Lcom/swedbank/mobile/data/device/m;

    invoke-static {v2}, Lcom/swedbank/mobile/data/device/m;->d(Lcom/swedbank/mobile/data/device/m;)Ljava/util/Random;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 50
    invoke-virtual {v0}, Landroidx/core/app/i$d;->b()Landroid/app/Notification;

    move-result-object v0

    .line 48
    invoke-virtual {v1, v2, v0}, Landroidx/core/app/l;->a(ILandroid/app/Notification;)V

    return-void
.end method
