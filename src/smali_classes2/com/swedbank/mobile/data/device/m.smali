.class public final Lcom/swedbank/mobile/data/device/m;
.super Ljava/lang/Object;
.source "SystemNotificationRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/e/n;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Landroidx/core/app/l;

.field private final b:Landroid/app/NotificationManager;

.field private final c:Ljava/util/Random;

.field private final d:Landroid/app/Application;

.field private final e:Lcom/swedbank/mobile/business/e/i;

.field private final f:Lcom/swedbank/mobile/business/e/o;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/e/o;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/e/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationStyler"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/m;->d:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/data/device/m;->e:Lcom/swedbank/mobile/business/e/i;

    iput-object p3, p0, Lcom/swedbank/mobile/data/device/m;->f:Lcom/swedbank/mobile/business/e/o;

    .line 30
    iget-object p1, p0, Lcom/swedbank/mobile/data/device/m;->d:Landroid/app/Application;

    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Landroidx/core/app/l;->a(Landroid/content/Context;)Landroidx/core/app/l;

    move-result-object p1

    const-string p2, "NotificationManagerCompat.from(application)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/m;->a:Landroidx/core/app/l;

    .line 31
    iget-object p1, p0, Lcom/swedbank/mobile/data/device/m;->d:Landroid/app/Application;

    const-string p2, "notification"

    invoke-virtual {p1, p2}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/app/NotificationManager;

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/m;->b:Landroid/app/NotificationManager;

    .line 32
    new-instance p1, Ljava/util/Random;

    invoke-direct {p1}, Ljava/util/Random;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/m;->c:Ljava/util/Random;

    return-void

    .line 31
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.app.NotificationManager"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/device/m;)Landroid/app/Application;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/m;->d:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/device/m;)Lcom/swedbank/mobile/business/e/o;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/m;->f:Lcom/swedbank/mobile/business/e/o;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/device/m;)Landroidx/core/app/l;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/m;->a:Landroidx/core/app/l;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/device/m;)Ljava/util/Random;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/m;->c:Ljava/util/Random;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/data/device/m;)Landroid/app/NotificationManager;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/m;->b:Landroid/app/NotificationManager;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/data/device/m;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/m;->e:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/e/m;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/swedbank/mobile/data/device/m$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/device/m$a;-><init>(Lcom/swedbank/mobile/data/device/m;Lcom/swedbank/mobile/business/e/m;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026uilder.build())\n    }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
