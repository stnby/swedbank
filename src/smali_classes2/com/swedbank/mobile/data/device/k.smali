.class public final Lcom/swedbank/mobile/data/device/k;
.super Ljava/lang/Object;
.source "IsDeviceRooted.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/util/l<",
        "Lcom/swedbank/mobile/business/e/l;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/ad;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/cards/wallet/ad;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/k;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/data/device/k;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/device/k;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/data/device/k;->a:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/data/device/k;->a:Lcom/swedbank/mobile/business/e/i;

    .line 19
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->j()Lio/reactivex/w;

    move-result-object v0

    .line 20
    sget-object v1, Lcom/swedbank/mobile/data/device/k$a;->a:Lcom/swedbank/mobile/data/device/k$a;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/k;)Lio/reactivex/j;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/swedbank/mobile/data/device/k;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 22
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->e()Lio/reactivex/w;

    move-result-object v1

    .line 23
    new-instance v2, Lcom/swedbank/mobile/data/device/k$b;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/data/device/k$b;-><init>(Lcom/swedbank/mobile/data/device/k;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v1

    check-cast v1, Lio/reactivex/n;

    .line 21
    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->d(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "deviceRepository\n      .\u2026)\n      .toSingle(Absent)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/device/k;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
