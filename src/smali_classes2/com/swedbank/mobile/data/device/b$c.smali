.class final Lcom/swedbank/mobile/data/device/b$c;
.super Ljava/lang/Object;
.source "DeviceRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/device/b;->n()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/device/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/device/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/device/b$c;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .line 243
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/swedbank/mobile/data/device/b$c;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-static {v2}, Lcom/swedbank/mobile/data/device/b;->a(Lcom/swedbank/mobile/data/device/b;)Landroid/app/Application;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Application;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/shared_prefs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 244
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    const-string v1, "sharedPrefsDir\n          .listFiles()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    invoke-static {v0}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object v0

    .line 248
    sget-object v1, Lcom/swedbank/mobile/data/device/c;->a:Lkotlin/h/i;

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object v0

    .line 249
    new-instance v1, Lcom/swedbank/mobile/data/device/b$c$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/device/b$c$1;-><init>(Lcom/swedbank/mobile/data/device/b$c;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object v0

    .line 270
    invoke-interface {v0}, Lkotlin/i/e;->a()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 253
    iget-object v2, p0, Lcom/swedbank/mobile/data/device/b$c;->a:Lcom/swedbank/mobile/data/device/b;

    invoke-static {v2}, Lcom/swedbank/mobile/data/device/b;->a(Lcom/swedbank/mobile/data/device/b;)Landroid/app/Application;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 254
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 255
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 256
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_0
    return-void
.end method
