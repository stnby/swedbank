.class public final Lcom/swedbank/mobile/data/k/a;
.super Ljava/lang/Object;
.source "PushInstanceIdWatcher.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/push/b;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/push/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/k/a;->a:Lcom/swedbank/mobile/business/push/h;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 12
    iget-object p1, p0, Lcom/swedbank/mobile/data/k/a;->a:Lcom/swedbank/mobile/business/push/h;

    const/4 v0, 0x1

    .line 13
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/push/h;->a(Z)V

    return-void
.end method
