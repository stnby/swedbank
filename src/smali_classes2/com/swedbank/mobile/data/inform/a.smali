.class public final Lcom/swedbank/mobile/data/inform/a;
.super Ljava/lang/Object;
.source "InformUserRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/inform/e;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/firebase/c;

.field private final b:Lcom/swedbank/mobile/business/c/a;

.field private final c:Lcom/squareup/moshi/n;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/c/a;Lcom/squareup/moshi/n;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "firebaseRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/inform/a;->a:Lcom/swedbank/mobile/business/firebase/c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/inform/a;->b:Lcom/swedbank/mobile/business/c/a;

    iput-object p3, p0, Lcom/swedbank/mobile/data/inform/a;->c:Lcom/squareup/moshi/n;

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/util/l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/inform/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/data/inform/a;->a:Lcom/swedbank/mobile/business/firebase/c;

    const-string v1, "inform_user"

    .line 23
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/firebase/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 26
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    goto :goto_1

    .line 29
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/swedbank/mobile/data/inform/a;->c:Lcom/squareup/moshi/n;

    .line 38
    const-class v2, Lcom/swedbank/mobile/data/inform/InformUserResponse;

    invoke-virtual {v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 30
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 41
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_0

    .line 42
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    .line 39
    check-cast v0, Lcom/swedbank/mobile/data/inform/InformUserResponse;

    .line 31
    iget-object v1, p0, Lcom/swedbank/mobile/data/inform/a;->b:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/data/inform/InformUserResponse;->a(Lcom/swedbank/mobile/business/c/g;)Lcom/swedbank/mobile/business/inform/d;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 43
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    goto :goto_1

    .line 31
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :catch_0
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    :goto_1
    return-object v0
.end method
