.class public final Lcom/swedbank/mobile/data/network/o;
.super Ljava/lang/Object;
.source "NetworkModule_ProvideBaseRequestsHttpClientFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lokhttp3/x;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/swedbank/mobile/data/network/o;->a:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/swedbank/mobile/data/network/o;->b:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/swedbank/mobile/data/network/o;->c:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/swedbank/mobile/data/network/o;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;)",
            "Lcom/swedbank/mobile/data/network/o;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/swedbank/mobile/data/network/o;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/data/network/o;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Landroid/app/Application;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;Lokhttp3/u;)Lokhttp3/x;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/network/m;->a(Landroid/app/Application;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;Lokhttp3/u;)Lokhttp3/x;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lokhttp3/x;

    return-object p0
.end method


# virtual methods
.method public a()Lokhttp3/x;
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/o;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/o;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/c/b;

    iget-object v2, p0, Lcom/swedbank/mobile/data/network/o;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/firebase/c;

    iget-object v3, p0, Lcom/swedbank/mobile/data/network/o;->d:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokhttp3/u;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/data/network/o;->a(Landroid/app/Application;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;Lokhttp3/u;)Lokhttp3/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/network/o;->a()Lokhttp3/x;

    move-result-object v0

    return-object v0
.end method
