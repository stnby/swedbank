.class public final Lcom/swedbank/mobile/data/network/y;
.super Lretrofit2/f$a;
.source "NetworkResponseConverterFactory.kt"


# instance fields
.field private final a:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/network/RequestErrorDetails;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/squareup/moshi/n;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 3
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "jsonMapper"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lretrofit2/f$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/network/y;->b:Lcom/squareup/moshi/n;

    .line 23
    const-class p1, Ljava/util/List;

    check-cast p1, Ljava/lang/reflect/Type;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/reflect/Type;

    const-class v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    check-cast v1, Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/moshi/p;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object p1

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/y;->b:Lcom/squareup/moshi/n;

    check-cast p1, Ljava/lang/reflect/Type;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "jsonMapper.adapter<List<\u2026questErrorDetails>>(type)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/network/y;->a:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method

.method public static final synthetic a(Ljava/lang/reflect/Type;)Ljava/lang/Class;
    .locals 0

    .line 17
    invoke-static {p0}, Lretrofit2/f$a;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;
    .locals 0

    .line 17
    invoke-static {p0, p1}, Lretrofit2/f$a;->b(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lretrofit2/r;)Lretrofit2/f;
    .locals 4
    .param p1    # Ljava/lang/reflect/Type;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # [Ljava/lang/annotation/Annotation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lretrofit2/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lretrofit2/r;",
            ")",
            "Lretrofit2/f<",
            "Lokhttp3/ad;",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "retrofit"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/y;->a(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p2

    const-class p3, Lcom/swedbank/mobile/data/network/w;

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    return-object p3

    .line 36
    :cond_0
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    const/4 p2, 0x0

    invoke-static {p2, p1}, Lcom/swedbank/mobile/data/network/y;->a(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object p1

    .line 38
    instance-of v0, p1, Ljava/lang/Class;

    if-nez v0, :cond_1

    move-object v0, p3

    goto :goto_0

    :cond_1
    move-object v0, p1

    :goto_0
    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Class;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    const-string v1, "it.annotations"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    array-length v1, v0

    :goto_1
    if-ge p2, v1, :cond_3

    aget-object v2, v0, p2

    .line 38
    instance-of v3, v2, Lcom/squareup/moshi/d;

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_3
    move-object v2, p3

    .line 143
    :goto_2
    instance-of p2, v2, Lcom/squareup/moshi/d;

    if-nez p2, :cond_4

    goto :goto_3

    :cond_4
    move-object p3, v2

    :goto_3
    check-cast p3, Lcom/squareup/moshi/d;

    .line 39
    :cond_5
    iget-object p2, p0, Lcom/swedbank/mobile/data/network/y;->b:Lcom/squareup/moshi/n;

    invoke-virtual {p2, p1}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/moshi/JsonAdapter;->d()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 40
    new-instance p2, Lcom/swedbank/mobile/data/network/x;

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/y;->a:Lcom/squareup/moshi/JsonAdapter;

    const-string v1, "dataAdapter"

    .line 42
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p2, v0, p1, p3}, Lcom/swedbank/mobile/data/network/x;-><init>(Lcom/squareup/moshi/JsonAdapter;Lcom/squareup/moshi/JsonAdapter;Lcom/squareup/moshi/d;)V

    check-cast p2, Lretrofit2/f;

    return-object p2
.end method
