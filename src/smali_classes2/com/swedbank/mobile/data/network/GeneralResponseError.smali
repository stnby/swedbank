.class public final Lcom/swedbank/mobile/data/network/GeneralResponseError;
.super Ljava/lang/Object;
.source "NetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/network/GeneralResponseError$a;
    }
.end annotation


# static fields
# The value of this static final field might be set in the static constructor
.field public static final a:Ljava/lang/String; = "\nfragment errorData on Error {\n  code\n  message\n}"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/network/GeneralResponseError$a;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/swedbank/mobile/data/network/GeneralResponseError$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/network/GeneralResponseError$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->b:Lcom/swedbank/mobile/data/network/GeneralResponseError$a;

    const-string v0, "\nfragment errorData on Error {\n  code\n  message\n}"

    .line 67
    sput-object v0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "code"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->d:Ljava/lang/String;

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/network/GeneralResponseError;

    iget-object v0, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->d:Ljava/lang/String;

    iget-object p1, p1, Lcom/swedbank/mobile/data/network/GeneralResponseError;->d:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GeneralResponseError(code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/GeneralResponseError;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
