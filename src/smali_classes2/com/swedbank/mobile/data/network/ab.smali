.class public final Lcom/swedbank/mobile/data/network/ab;
.super Ljava/lang/Object;
.source "RequestLogic.kt"


# static fields
.field public static final a:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h<",
            "Ljava/lang/Throwable;",
            "+",
            "Lcom/swedbank/mobile/data/network/w<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    sget-object v0, Lcom/swedbank/mobile/data/network/ab$b;->a:Lcom/swedbank/mobile/data/network/ab$b;

    check-cast v0, Lkotlin/e/a/b;

    sput-object v0, Lcom/swedbank/mobile/data/network/ab;->a:Lkotlin/e/a/b;

    .line 21
    sget-object v0, Lcom/swedbank/mobile/data/network/ab$a;->a:Lcom/swedbank/mobile/data/network/ab$a;

    check-cast v0, Lio/reactivex/c/h;

    sput-object v0, Lcom/swedbank/mobile/data/network/ab;->b:Lio/reactivex/c/h;

    return-void
.end method

.method public static final a(Lio/reactivex/b;)Lio/reactivex/b;
    .locals 7
    .param p0    # Lio/reactivex/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$applyRequestLogic"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    sget-object v3, Lcom/swedbank/mobile/data/network/ab;->a:Lkotlin/e/a/b;

    const/4 v2, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    .line 66
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/l/f;->a(Lio/reactivex/b;ILkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lio/reactivex/w;)Lio/reactivex/w;
    .locals 7
    .param p0    # Lio/reactivex/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/w<",
            "TT;>;)",
            "Lio/reactivex/w<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$applyRequestLogic"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    sget-object v3, Lcom/swedbank/mobile/data/network/ab;->a:Lkotlin/e/a/b;

    const/4 v2, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    .line 42
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/l/f;->a(Lio/reactivex/w;ILkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static final b(Lio/reactivex/w;)Lio/reactivex/w;
    .locals 7
    .param p0    # Lio/reactivex/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "TT;>;>;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "TT;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$applyRequestLogic"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget-object v3, Lcom/swedbank/mobile/data/network/ab;->a:Lkotlin/e/a/b;

    const/4 v2, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    .line 48
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/l/f;->a(Lio/reactivex/w;ILkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object p0

    .line 71
    sget-object v0, Lcom/swedbank/mobile/data/network/ab;->b:Lio/reactivex/c/h;

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0, v0}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p0

    const-string v0, "retryWithExponentialBack\u2026workLayerErrorSupplier())"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 71
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type io.reactivex.functions.Function<kotlin.Throwable, out com.swedbank.mobile.data.network.NetworkResponse<T>>"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
