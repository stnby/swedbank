.class public final Lcom/swedbank/mobile/data/network/x;
.super Ljava/lang/Object;
.source "NetworkResponseConverterFactory.kt"

# interfaces
.implements Lretrofit2/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/f<",
        "Lokhttp3/ad;",
        "Lcom/swedbank/mobile/data/network/w<",
        "+TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/squareup/moshi/g$a;

.field private final b:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/network/RequestErrorDetails;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/squareup/moshi/d;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/JsonAdapter;Lcom/squareup/moshi/JsonAdapter;Lcom/squareup/moshi/d;)V
    .locals 1
    .param p1    # Lcom/squareup/moshi/JsonAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/moshi/JsonAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/moshi/d;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/network/RequestErrorDetails;",
            ">;>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "TT;>;",
            "Lcom/squareup/moshi/d;",
            ")V"
        }
    .end annotation

    const-string v0, "errorsJsonAdapter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dataAdapter"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/network/x;->b:Lcom/squareup/moshi/JsonAdapter;

    iput-object p2, p0, Lcom/swedbank/mobile/data/network/x;->c:Lcom/squareup/moshi/JsonAdapter;

    iput-object p3, p0, Lcom/swedbank/mobile/data/network/x;->d:Lcom/squareup/moshi/d;

    const-string p1, "data"

    const-string p2, "errors"

    .line 54
    filled-new-array {p1, p2}, [Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/network/x;->a:Lcom/squareup/moshi/g$a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/network/x;)Lcom/squareup/moshi/g$a;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/data/network/x;->a:Lcom/squareup/moshi/g$a;

    return-object p0
.end method

.method private final a(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/network/w;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/g;",
            ")",
            "Lcom/swedbank/mobile/data/network/w<",
            "TT;>;"
        }
    .end annotation

    .line 101
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->h()Lcom/squareup/moshi/g$b;

    move-result-object v0

    sget-object v1, Lcom/squareup/moshi/g$b;->i:Lcom/squareup/moshi/g$b;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 102
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->m()Ljava/lang/Object;

    return-object v2

    .line 106
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/data/network/x$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/network/x$a;-><init>(Lcom/swedbank/mobile/data/network/x;)V

    .line 112
    iget-object v1, p0, Lcom/swedbank/mobile/data/network/x;->d:Lcom/squareup/moshi/d;

    if-eqz v1, :cond_5

    .line 114
    iget-object v1, p0, Lcom/swedbank/mobile/data/network/x;->d:Lcom/squareup/moshi/d;

    .line 176
    check-cast v2, Lcom/swedbank/mobile/data/network/w;

    const/4 v3, 0x1

    .line 177
    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1}, Lcom/squareup/moshi/d;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v1

    move-object v3, v2

    const/4 v2, 0x0

    .line 179
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v2, v2, 0x1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->e()V

    .line 182
    invoke-virtual {p1, v1}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v5

    if-eqz v5, :cond_1

    .line 184
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->i()Ljava/lang/String;

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/data/network/x$a;->a(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/network/w;

    move-result-object v3

    goto :goto_0

    :cond_2
    :goto_1
    if-ge v4, v2, :cond_3

    .line 188
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->f()V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_4

    goto :goto_2

    .line 190
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Response contained no matching data"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 116
    :cond_5
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/data/network/x$a;->a(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/network/w;

    move-result-object v3

    :goto_2
    return-object v3
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/network/x;Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/network/w;
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/data/network/x;->a(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/network/w;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/network/x;)Lcom/squareup/moshi/JsonAdapter;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/data/network/x;->b:Lcom/squareup/moshi/JsonAdapter;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/network/x;)Lcom/squareup/moshi/JsonAdapter;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/data/network/x;->c:Lcom/squareup/moshi/JsonAdapter;

    return-object p0
.end method


# virtual methods
.method public a(Lokhttp3/ad;)Lcom/swedbank/mobile/data/network/w;
    .locals 5
    .param p1    # Lokhttp3/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/ad;",
            ")",
            "Lcom/swedbank/mobile/data/network/w<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    move-object v0, p1

    check-cast v0, Ljava/io/Closeable;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Throwable;

    :try_start_0
    move-object v2, v0

    check-cast v2, Lokhttp3/ad;

    .line 58
    invoke-virtual {p1}, Lokhttp3/ad;->c()Lb/e;

    move-result-object p1

    const-wide/16 v2, 0x0

    .line 59
    invoke-static {}, Lcom/swedbank/mobile/data/network/z;->a()Lb/f;

    move-result-object v4

    invoke-interface {p1, v2, v3, v4}, Lb/e;->a(JLb/f;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    invoke-static {}, Lcom/swedbank/mobile/data/network/z;->a()Lb/f;

    move-result-object v2

    invoke-virtual {v2}, Lb/f;->h()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {p1, v2, v3}, Lb/e;->i(J)V

    .line 62
    :cond_0
    invoke-static {p1}, Lcom/squareup/moshi/g;->a(Lb/e;)Lcom/squareup/moshi/g;

    move-result-object p1

    .line 64
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->e()V

    .line 65
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "reader"

    .line 66
    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-static {p0}, Lcom/swedbank/mobile/data/network/x;->a(Lcom/swedbank/mobile/data/network/x;)Lcom/squareup/moshi/g$a;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 153
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/16 :goto_3

    .line 149
    :pswitch_0
    invoke-static {p0}, Lcom/swedbank/mobile/data/network/x;->b(Lcom/swedbank/mobile/data/network/x;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    check-cast v2, Ljava/util/List;

    .line 152
    new-instance v3, Lcom/swedbank/mobile/data/network/w$b;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/data/network/w$b;-><init>(Ljava/util/List;)V

    check-cast v3, Lcom/swedbank/mobile/data/network/w;

    goto :goto_0

    :cond_1
    const-string p1, "Error response contained no errors"

    .line 149
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 143
    :pswitch_1
    invoke-static {p0, p1}, Lcom/swedbank/mobile/data/network/x;->a(Lcom/swedbank/mobile/data/network/x;Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/network/w;

    move-result-object v3

    .line 67
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 68
    sget-object v2, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 69
    instance-of v2, v3, Lcom/swedbank/mobile/data/network/w$c;

    if-nez v2, :cond_3

    .line 159
    invoke-static {p0}, Lcom/swedbank/mobile/data/network/x;->a(Lcom/swedbank/mobile/data/network/x;)Lcom/squareup/moshi/g$a;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 170
    new-instance p1, Ljava/lang/IllegalStateException;

    goto :goto_1

    .line 166
    :pswitch_2
    invoke-static {p0}, Lcom/swedbank/mobile/data/network/x;->b(Lcom/swedbank/mobile/data/network/x;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Ljava/util/List;

    .line 169
    new-instance v3, Lcom/swedbank/mobile/data/network/w$b;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/data/network/w$b;-><init>(Ljava/util/List;)V

    check-cast v3, Lcom/swedbank/mobile/data/network/w;

    goto :goto_2

    :cond_2
    const-string p1, "Error response contained no errors"

    .line 166
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 160
    :pswitch_3
    invoke-static {p0, p1}, Lcom/swedbank/mobile/data/network/x;->a(Lcom/swedbank/mobile/data/network/x;Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/data/network/w;

    move-result-object v2

    move-object v3, v2

    goto :goto_2

    :goto_1
    const-string v2, "Network response must have at least one response structure - either data or errors"

    .line 170
    invoke-direct {p1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 73
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->i()Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->q()V

    .line 80
    :cond_4
    :goto_2
    invoke-virtual {p1}, Lcom/squareup/moshi/g;->f()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_5

    .line 81
    invoke-static {v0, v1}, Lkotlin/io/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    return-object v3

    :cond_5
    :try_start_1
    const-string p1, "Response must have at least one block of data or errors"

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    :goto_3
    const-string v2, "Network response must have at least one response structure - either data or errors"

    .line 153
    invoke-direct {p1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 78
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v2, "Network response must have at least one response structure - either data or errors"

    invoke-direct {p1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception p1

    move-object v1, p1

    .line 57
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_4
    invoke-static {v0, v1}, Lkotlin/io/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lokhttp3/ad;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/network/x;->a(Lokhttp3/ad;)Lcom/swedbank/mobile/data/network/w;

    move-result-object p1

    return-object p1
.end method
