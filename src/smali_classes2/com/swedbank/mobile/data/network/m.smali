.class public final Lcom/swedbank/mobile/data/network/m;
.super Ljava/lang/Object;
.source "NetworkModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/data/network/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 55
    new-instance v0, Lcom/swedbank/mobile/data/network/m;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/network/m;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/network/m;->a:Lcom/swedbank/mobile/data/network/m;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/squareup/moshi/n;)Lcom/squareup/moshi/n;
    .locals 1
    .param p0    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "graphql_json_mapper"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "jsonMapper"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0}, Lcom/squareup/moshi/n;->a()Lcom/squareup/moshi/n$a;

    move-result-object p0

    .line 145
    new-instance v0, Lcom/swedbank/mobile/data/network/f;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/network/f;-><init>()V

    check-cast v0, Lcom/squareup/moshi/JsonAdapter$a;

    invoke-virtual {p0, v0}, Lcom/squareup/moshi/n$a;->a(Lcom/squareup/moshi/JsonAdapter$a;)Lcom/squareup/moshi/n$a;

    move-result-object p0

    .line 146
    invoke-virtual {p0}, Lcom/squareup/moshi/n$a;->a()Lcom/squareup/moshi/n;

    move-result-object p0

    const-string v0, "jsonMapper.newBuilder()\n\u2026Factory())\n      .build()"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/moshi/n;
    .locals 2
    .param p0    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "json_adapters"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "json_adapters"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "json_adapters"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;)",
            "Lcom/squareup/moshi/n;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "jsonAdapters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeJsonAdapters"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonAdapterFactories"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    new-instance v0, Lcom/squareup/moshi/n$a;

    invoke-direct {v0}, Lcom/squareup/moshi/n$a;-><init>()V

    .line 132
    check-cast p0, Ljava/lang/Iterable;

    .line 203
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Lcom/squareup/moshi/n$a;->a(Ljava/lang/Object;)Lcom/squareup/moshi/n$a;

    goto :goto_0

    .line 133
    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    .line 205
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/k;

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/h/b;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/moshi/JsonAdapter;

    .line 133
    invoke-static {v1}, Lkotlin/e/a;->a(Lkotlin/h/b;)Ljava/lang/Class;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/moshi/n$a;->a(Ljava/lang/reflect/Type;Lcom/squareup/moshi/JsonAdapter;)Lcom/squareup/moshi/n$a;

    goto :goto_1

    .line 134
    :cond_1
    check-cast p2, Ljava/lang/Iterable;

    .line 207
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/moshi/JsonAdapter$a;

    .line 134
    invoke-virtual {v0, p1}, Lcom/squareup/moshi/n$a;->a(Lcom/squareup/moshi/JsonAdapter$a;)Lcom/squareup/moshi/n$a;

    goto :goto_2

    .line 136
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/moshi/n$a;->a()Lcom/squareup/moshi/n;

    move-result-object p0

    const-string p1, "Moshi.Builder()\n      .a\u2026 }\n      }\n      .build()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "json_adapters"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x3

    .line 93
    new-array v0, v0, [Ljava/lang/Object;

    .line 94
    new-instance v1, Lcom/swedbank/mobile/data/network/BigDecimalAdapter;

    invoke-direct {v1}, Lcom/swedbank/mobile/data/network/BigDecimalAdapter;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 95
    new-instance v1, Lcom/swedbank/mobile/data/network/DateAdapter;

    invoke-direct {v1}, Lcom/swedbank/mobile/data/network/DateAdapter;-><init>()V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 96
    new-instance v1, Lcom/swedbank/mobile/data/authentication/SecurityLevelAdapter;

    invoke-direct {v1}, Lcom/swedbank/mobile/data/authentication/SecurityLevelAdapter;-><init>()V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 93
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Landroid/app/Application;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;Lokhttp3/u;)Lokhttp3/x;
    .locals 5
    .param p0    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lokhttp3/u;
        .annotation runtime Ljavax/inject/Named;
            value = "connectivity_interceptor"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "HTTP_CLIENT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "app"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityInterceptor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance v0, Lokhttp3/x$a;

    invoke-direct {v0}, Lokhttp3/x$a;-><init>()V

    .line 66
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/x$a;->a(JLjava/util/concurrent/TimeUnit;)Lokhttp3/x$a;

    move-result-object v0

    .line 67
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5a

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/x$a;->b(JLjava/util/concurrent/TimeUnit;)Lokhttp3/x$a;

    move-result-object v0

    .line 70
    new-instance v1, Lokhttp3/c;

    .line 71
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/app/Application;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "http"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-wide/32 v3, 0x3200000

    .line 70
    invoke-direct {v1, v2, v3, v4}, Lokhttp3/c;-><init>(Ljava/io/File;J)V

    .line 68
    invoke-virtual {v0, v1}, Lokhttp3/x$a;->a(Lokhttp3/c;)Lokhttp3/x$a;

    move-result-object v0

    .line 73
    invoke-virtual {v0, p3}, Lokhttp3/x$a;->a(Lokhttp3/u;)Lokhttp3/x$a;

    move-result-object p3

    const-string v0, "OkHttpClient.Builder()\n \u2026(connectivityInterceptor)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    check-cast p0, Landroid/content/Context;

    invoke-static {p3, p0, p1, p2}, Lcom/swedbank/mobile/data/network/h;->a(Lokhttp3/x$a;Landroid/content/Context;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;)Lokhttp3/x$a;

    move-result-object p0

    .line 75
    invoke-virtual {p0}, Lokhttp3/x$a;->b()Lokhttp3/x;

    move-result-object p0

    const-string p1, "OkHttpClient.Builder()\n \u2026epository)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Lokhttp3/x;Lokhttp3/u;)Lokhttp3/x;
    .locals 2
    .param p0    # Lokhttp3/x;
        .annotation runtime Ljavax/inject/Named;
            value = "HTTP_CLIENT"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lokhttp3/u;
        .annotation runtime Ljavax/inject/Named;
            value = "swed_requests_interceptor"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "SWED_HTTP_CLIENT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "httpClient"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swedbankRequestInterceptor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lokhttp3/x;->A()Lokhttp3/x$a;

    move-result-object p0

    const-string v0, "httpClient\n      .newBuilder()"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-virtual {p0}, Lokhttp3/x$a;->a()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 87
    invoke-virtual {p0}, Lokhttp3/x$a;->b()Lokhttp3/x;

    move-result-object p0

    const-string p1, "httpClient\n      .newBui\u2026terceptor)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Lcom/squareup/moshi/n;Lokhttp3/x;Lcom/swedbank/mobile/business/c/b;)Lretrofit2/r;
    .locals 1
    .param p0    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lokhttp3/x;
        .annotation runtime Ljavax/inject/Named;
            value = "SWED_HTTP_CLIENT"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "not_authenticated"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "jsonMapper"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "httpClient"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 157
    invoke-static {p0, v0}, Lcom/swedbank/mobile/data/network/n;->a(Lcom/squareup/moshi/n;Z)Lretrofit2/r$a;

    move-result-object p0

    .line 158
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/c/b;->e()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lretrofit2/r$a;->a(Ljava/lang/String;)Lretrofit2/r$a;

    move-result-object p0

    .line 159
    invoke-virtual {p0, p1}, Lretrofit2/r$a;->a(Lokhttp3/x;)Lretrofit2/r$a;

    move-result-object p0

    .line 160
    invoke-virtual {p0}, Lretrofit2/r$a;->b()Lretrofit2/r;

    move-result-object p0

    const-string p1, "retrofitBuilder(jsonMapp\u2026lient)\n          .build()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final b()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "json_adapters"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x2

    .line 103
    new-array v0, v0, [Lkotlin/k;

    .line 104
    const-class v1, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 105
    const-class v2, Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 106
    sget-object v3, Lcom/swedbank/mobile/data/authentication/AuthenticationState;->AUTHENTICATION_FAILED:Lcom/swedbank/mobile/data/authentication/AuthenticationState;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 104
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 107
    const-class v1, Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 108
    const-class v2, Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 109
    sget-object v3, Lcom/swedbank/mobile/data/customer/PrivateCustomerType;->REGULAR:Lcom/swedbank/mobile/data/customer/PrivateCustomerType;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 107
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 103
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final c()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "json_adapters"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x1

    .line 116
    new-array v0, v0, [Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    .line 117
    sget-object v1, Lcom/swedbank/mobile/data/l/e;->a:Lcom/swedbank/mobile/data/l/e;

    .line 190
    const-class v1, Lcom/swedbank/mobile/data/customer/CustomerItem;

    const-string v2, "__typename"

    invoke-static {v1, v2}, Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    move-result-object v1

    const-string v2, "MoshiPolymorphicJsonAdap\u2026s.java, GRAPHQL_TYPENAME)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    const-class v2, Lcom/swedbank/mobile/data/customer/CustomerItem$PrivatePerson;

    .line 193
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    move-result-object v1

    const-string v2, "withSubtype(javaType, javaType.simpleName)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "run {\n  val javaType = s\u2026e, javaType.simpleName)\n}"

    .line 191
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    const-class v2, Lcom/swedbank/mobile/data/customer/CustomerItem$SelfEmployedPerson;

    .line 197
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    move-result-object v1

    const-string v2, "withSubtype(javaType, javaType.simpleName)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "run {\n  val javaType = s\u2026e, javaType.simpleName)\n}"

    .line 195
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    const-class v2, Lcom/swedbank/mobile/data/customer/CustomerItem$Company;

    .line 201
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/moshi/adapters/PolymorphicJsonAdapterFactory;

    move-result-object v1

    const-string v2, "withSubtype(javaType, javaType.simpleName)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "run {\n  val javaType = s\u2026e, javaType.simpleName)\n}"

    .line 199
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 116
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
