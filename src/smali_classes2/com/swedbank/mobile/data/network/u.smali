.class public final Lcom/swedbank/mobile/data/network/u;
.super Ljava/lang/Object;
.source "NetworkModule_ProvideSwedbankRequestsHttpClientFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lokhttp3/x;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/data/network/u;->a:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/swedbank/mobile/data/network/u;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;)",
            "Lcom/swedbank/mobile/data/network/u;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/swedbank/mobile/data/network/u;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/network/u;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lokhttp3/x;Lokhttp3/u;)Lokhttp3/x;
    .locals 0

    .line 35
    invoke-static {p0, p1}, Lcom/swedbank/mobile/data/network/m;->a(Lokhttp3/x;Lokhttp3/u;)Lokhttp3/x;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lokhttp3/x;

    return-object p0
.end method


# virtual methods
.method public a()Lokhttp3/x;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/u;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/x;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/u;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/u;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/data/network/u;->a(Lokhttp3/x;Lokhttp3/u;)Lokhttp3/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/network/u;->a()Lokhttp3/x;

    move-result-object v0

    return-object v0
.end method
