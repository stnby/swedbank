.class public final Lcom/swedbank/mobile/data/network/a;
.super Ljava/lang/Object;
.source "ConnectivityInterceptor.kt"

# interfaces
.implements Lokhttp3/u;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/network/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/network/i;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/network/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "networkManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/network/a;->a:Lcom/swedbank/mobile/data/network/i;

    return-void
.end method


# virtual methods
.method public a(Lokhttp3/u$a;)Lokhttp3/ac;
    .locals 1
    .param p1    # Lokhttp3/u$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/a;->a:Lcom/swedbank/mobile/data/network/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/data/network/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    invoke-interface {p1}, Lokhttp3/u$a;->a()Lokhttp3/aa;

    move-result-object v0

    invoke-interface {p1, v0}, Lokhttp3/u$a;->a(Lokhttp3/aa;)Lokhttp3/ac;

    move-result-object p1

    const-string v0, "chain.proceed(chain.request())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 18
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/data/network/NoConnectivityException;->a:Lcom/swedbank/mobile/data/network/NoConnectivityException;

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
