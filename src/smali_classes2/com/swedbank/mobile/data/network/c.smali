.class final enum Lcom/swedbank/mobile/data/network/c;
.super Ljava/lang/Enum;
.source "HttpClientConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/data/network/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/data/network/c;

.field public static final enum b:Lcom/swedbank/mobile/data/network/c;

.field public static final enum c:Lcom/swedbank/mobile/data/network/c;

.field public static final enum d:Lcom/swedbank/mobile/data/network/c;

.field public static final enum e:Lcom/swedbank/mobile/data/network/c;

.field public static final enum f:Lcom/swedbank/mobile/data/network/c;

.field private static final synthetic g:[Lcom/swedbank/mobile/data/network/c;


# instance fields
.field private final h:Lcom/swedbank/mobile/data/network/e;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/business/c/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final j:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/swedbank/mobile/data/network/c;

    new-instance v7, Lcom/swedbank/mobile/data/network/c;

    const-string v2, "BACKEND_EE"

    .line 67
    sget-object v4, Lcom/swedbank/mobile/data/network/e;->a:Lcom/swedbank/mobile/data/network/e;

    sget-object v5, Lcom/swedbank/mobile/business/c/c;->a:Lcom/swedbank/mobile/business/c/c;

    const-string v6, "tech_cert_backend_ee"

    const/4 v3, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/data/network/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/data/network/e;Lcom/swedbank/mobile/business/c/c;Ljava/lang/String;)V

    sput-object v7, Lcom/swedbank/mobile/data/network/c;->a:Lcom/swedbank/mobile/data/network/c;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/swedbank/mobile/data/network/c;

    const-string v9, "BACKEND_LV"

    .line 68
    sget-object v11, Lcom/swedbank/mobile/data/network/e;->a:Lcom/swedbank/mobile/data/network/e;

    sget-object v12, Lcom/swedbank/mobile/business/c/c;->b:Lcom/swedbank/mobile/business/c/c;

    const-string v13, "tech_cert_backend_lv"

    const/4 v10, 0x1

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/swedbank/mobile/data/network/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/data/network/e;Lcom/swedbank/mobile/business/c/c;Ljava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/data/network/c;->b:Lcom/swedbank/mobile/data/network/c;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/data/network/c;

    const-string v4, "BACKEND_LT"

    .line 69
    sget-object v6, Lcom/swedbank/mobile/data/network/e;->a:Lcom/swedbank/mobile/data/network/e;

    sget-object v7, Lcom/swedbank/mobile/business/c/c;->c:Lcom/swedbank/mobile/business/c/c;

    const-string v8, "tech_cert_backend_lt"

    const/4 v5, 0x2

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/data/network/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/data/network/e;Lcom/swedbank/mobile/business/c/c;Ljava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/data/network/c;->c:Lcom/swedbank/mobile/data/network/c;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/data/network/c;

    const-string v4, "LOGIN_EE"

    .line 70
    sget-object v6, Lcom/swedbank/mobile/data/network/e;->b:Lcom/swedbank/mobile/data/network/e;

    sget-object v7, Lcom/swedbank/mobile/business/c/c;->a:Lcom/swedbank/mobile/business/c/c;

    const-string v8, "tech_cert_login_ee"

    const/4 v5, 0x3

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/data/network/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/data/network/e;Lcom/swedbank/mobile/business/c/c;Ljava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/data/network/c;->d:Lcom/swedbank/mobile/data/network/c;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/data/network/c;

    const-string v4, "LOGIN_LV"

    .line 71
    sget-object v6, Lcom/swedbank/mobile/data/network/e;->b:Lcom/swedbank/mobile/data/network/e;

    sget-object v7, Lcom/swedbank/mobile/business/c/c;->b:Lcom/swedbank/mobile/business/c/c;

    const-string v8, "tech_cert_login_lv"

    const/4 v5, 0x4

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/data/network/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/data/network/e;Lcom/swedbank/mobile/business/c/c;Ljava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/data/network/c;->e:Lcom/swedbank/mobile/data/network/c;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/data/network/c;

    const-string v4, "LOGIN_LT"

    .line 72
    sget-object v6, Lcom/swedbank/mobile/data/network/e;->b:Lcom/swedbank/mobile/data/network/e;

    sget-object v7, Lcom/swedbank/mobile/business/c/c;->c:Lcom/swedbank/mobile/business/c/c;

    const-string v8, "tech_cert_login_lt"

    const/4 v5, 0x5

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/data/network/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/data/network/e;Lcom/swedbank/mobile/business/c/c;Ljava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/data/network/c;->f:Lcom/swedbank/mobile/data/network/c;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/data/network/c;->g:[Lcom/swedbank/mobile/data/network/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/swedbank/mobile/data/network/e;Lcom/swedbank/mobile/business/c/c;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/e;",
            "Lcom/swedbank/mobile/business/c/c;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/swedbank/mobile/data/network/c;->h:Lcom/swedbank/mobile/data/network/e;

    iput-object p4, p0, Lcom/swedbank/mobile/data/network/c;->i:Lcom/swedbank/mobile/business/c/c;

    iput-object p5, p0, Lcom/swedbank/mobile/data/network/c;->j:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/data/network/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/network/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/network/c;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/data/network/c;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/data/network/c;->g:[Lcom/swedbank/mobile/data/network/c;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/data/network/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/data/network/c;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/c;->h:Lcom/swedbank/mobile/data/network/e;

    sget-object v1, Lcom/swedbank/mobile/data/network/d;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/e;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 78
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/swedbank/mobile/data/b/e;->a:Lcom/swedbank/mobile/data/b/e$a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/c;->i:Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/data/b/e$a;->a(Lcom/swedbank/mobile/business/c/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :pswitch_1
    sget-object v0, Lcom/swedbank/mobile/data/b/e;->a:Lcom/swedbank/mobile/data/b/e$a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/c;->i:Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/data/b/e$a;->c(Lcom/swedbank/mobile/business/c/c;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_0
    invoke-static {v0}, Lokhttp3/t;->f(Ljava/lang/String;)Lokhttp3/t;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0}, Lokhttp3/t;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const-string v0, ""

    :goto_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Lcom/swedbank/mobile/business/c/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/c;->i:Lcom/swedbank/mobile/business/c/c;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/c;->j:Ljava/lang/String;

    return-object v0
.end method
