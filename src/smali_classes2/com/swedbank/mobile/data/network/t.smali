.class public final Lcom/swedbank/mobile/data/network/t;
.super Ljava/lang/Object;
.source "NetworkModule_ProvideNotAuthenticatedRetrofitFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lretrofit2/r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/swedbank/mobile/data/network/t;->a:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/swedbank/mobile/data/network/t;->b:Ljavax/inject/Provider;

    .line 23
    iput-object p3, p0, Lcom/swedbank/mobile/data/network/t;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/t;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;)",
            "Lcom/swedbank/mobile/data/network/t;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/swedbank/mobile/data/network/t;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/data/network/t;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/squareup/moshi/n;Lokhttp3/x;Lcom/swedbank/mobile/business/c/b;)Lretrofit2/r;
    .locals 0

    .line 39
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/data/network/m;->a(Lcom/squareup/moshi/n;Lokhttp3/x;Lcom/swedbank/mobile/business/c/b;)Lretrofit2/r;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lretrofit2/r;

    return-object p0
.end method


# virtual methods
.method public a()Lretrofit2/r;
    .locals 3

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/t;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/moshi/n;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/t;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/x;

    iget-object v2, p0, Lcom/swedbank/mobile/data/network/t;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/c/b;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/data/network/t;->a(Lcom/squareup/moshi/n;Lokhttp3/x;Lcom/swedbank/mobile/business/c/b;)Lretrofit2/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/network/t;->a()Lretrofit2/r;

    move-result-object v0

    return-object v0
.end method
