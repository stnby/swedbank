.class public final Lcom/swedbank/mobile/data/network/s;
.super Ljava/lang/Object;
.source "NetworkModule_ProvideJsonMapperFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/squareup/moshi/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;>;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/swedbank/mobile/data/network/s;->a:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/swedbank/mobile/data/network/s;->b:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/swedbank/mobile/data/network/s;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/moshi/n;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;)",
            "Lcom/squareup/moshi/n;"
        }
    .end annotation

    .line 43
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/data/network/m;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/moshi/n;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/moshi/n;

    return-object p0
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;>;)",
            "Lcom/swedbank/mobile/data/network/s;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/swedbank/mobile/data/network/s;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/data/network/s;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/moshi/n;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/s;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iget-object v1, p0, Lcom/swedbank/mobile/data/network/s;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    iget-object v2, p0, Lcom/swedbank/mobile/data/network/s;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/data/network/s;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/moshi/n;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/network/s;->a()Lcom/squareup/moshi/n;

    move-result-object v0

    return-object v0
.end method
