.class public final Lcom/swedbank/mobile/data/network/j$1;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "NetworkManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/network/j;-><init>(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/network/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/network/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 2
    .param p1    # Landroid/net/Network;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "network"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/network/j;->a(Lcom/swedbank/mobile/data/network/j;Landroid/net/Network;)Ljava/lang/String;

    move-result-object p1

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/network/j;->a(Lcom/swedbank/mobile/data/network/j;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network connection available: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    iget-object p1, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {p1}, Lcom/swedbank/mobile/data/network/j;->a(Lcom/swedbank/mobile/data/network/j;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/network/j;->b(Lcom/swedbank/mobile/data/network/j;)Lcom/b/c/b;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 71
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 2
    .param p1    # Landroid/net/Network;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "network"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/network/j;->a(Lcom/swedbank/mobile/data/network/j;Landroid/net/Network;)Ljava/lang/String;

    move-result-object p1

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/network/j;->a(Lcom/swedbank/mobile/data/network/j;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network connection lost: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    iget-object p1, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {p1}, Lcom/swedbank/mobile/data/network/j;->a(Lcom/swedbank/mobile/data/network/j;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 82
    iget-object v0, p0, Lcom/swedbank/mobile/data/network/j$1;->a:Lcom/swedbank/mobile/data/network/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/network/j;->b(Lcom/swedbank/mobile/data/network/j;)Lcom/b/c/b;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 83
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    return-void
.end method
