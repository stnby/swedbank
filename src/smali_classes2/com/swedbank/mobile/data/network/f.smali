.class public final Lcom/swedbank/mobile/data/network/f;
.super Ljava/lang/Object;
.source "GraphQLJsonAdapters.kt"

# interfaces
.implements Lcom/squareup/moshi/JsonAdapter$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/reflect/Type;Ljava/util/Set;Lcom/squareup/moshi/n;)Lcom/squareup/moshi/JsonAdapter;
    .locals 0
    .param p1    # Ljava/lang/reflect/Type;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/Set<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Lcom/squareup/moshi/n;",
            ")",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string p2, "type"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "moshi"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p1}, Lcom/squareup/moshi/p;->d(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p1

    const-string p2, "rawType"

    .line 25
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->isEnum()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Lcom/swedbank/mobile/data/network/GraphQLEnumJsonAdapter;

    invoke-direct {p2, p1, p3}, Lcom/swedbank/mobile/data/network/GraphQLEnumJsonAdapter;-><init>(Ljava/lang/Class;Lcom/squareup/moshi/n;)V

    .line 28
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/network/GraphQLEnumJsonAdapter;->d()Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method
