.class public final Lcom/swedbank/mobile/data/network/h;
.super Ljava/lang/Object;
.source "HttpClientConfiguration.kt"


# direct methods
.method public static final a(Lokhttp3/x$a;Landroid/content/Context;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;)Lokhttp3/x$a;
    .locals 1
    .param p0    # Lokhttp3/x$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$addAdditionalConfigurations"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "configuration"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "firebaseRepository"

    invoke-static {p3, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p0, p2, p3}, Lcom/swedbank/mobile/data/network/h;->a(Lokhttp3/x$a;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;)Lokhttp3/x$a;

    move-result-object p0

    return-object p0
.end method

.method private static final a(Lokhttp3/x$a;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;)Lokhttp3/x$a;
    .locals 11
    .param p0    # Lokhttp3/x$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 31
    invoke-static {}, Lcom/swedbank/mobile/data/network/c;->values()[Lcom/swedbank/mobile/data/network/c;

    move-result-object v1

    .line 32
    invoke-static {v1}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object v1

    .line 33
    new-instance v2, Lcom/swedbank/mobile/data/network/h$a;

    invoke-direct {v2, p1, p2}, Lcom/swedbank/mobile/data/network/h$a;-><init>(Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/firebase/c;)V

    check-cast v2, Lkotlin/e/a/b;

    invoke-static {v1, v2}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 87
    invoke-interface {p1}, Lkotlin/i/e;->a()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/network/c;

    .line 35
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Lcom/swedbank/mobile/business/firebase/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 36
    move-object v5, v4

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_0

    .line 37
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/c;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, ";"

    .line 38
    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 39
    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    .line 88
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 89
    check-cast v4, Ljava/lang/String;

    .line 39
    invoke-static {v1, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    :cond_2
    move-object p1, v0

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v3

    if-eqz p1, :cond_6

    new-instance p1, Lokhttp3/g$a;

    invoke-direct {p1}, Lokhttp3/g$a;-><init>()V

    .line 47
    check-cast v0, Ljava/lang/Iterable;

    .line 92
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :catch_0
    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/k;

    invoke-virtual {v0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 49
    :try_start_0
    move-object v4, v1

    check-cast v4, Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    .line 50
    new-array v4, v3, [Ljava/lang/String;

    aput-object v0, v4, v2

    invoke-virtual {p1, v1, v4}, Lokhttp3/g$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lokhttp3/g$a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 57
    :cond_5
    invoke-virtual {p1}, Lokhttp3/g$a;->a()Lokhttp3/g;

    move-result-object p1

    .line 45
    invoke-virtual {p0, p1}, Lokhttp3/x$a;->a(Lokhttp3/g;)Lokhttp3/x$a;

    move-result-object p0

    const-string p1, "certificatePinner(Certif\u2026                .build())"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_6
    return-object p0
.end method
