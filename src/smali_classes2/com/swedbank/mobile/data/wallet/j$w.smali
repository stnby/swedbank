.class final Lcom/swedbank/mobile/data/wallet/j$w;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->a(ZZZ)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;ZZZ)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-boolean p2, p0, Lcom/swedbank/mobile/data/wallet/j$w;->b:Z

    iput-boolean p3, p0, Lcom/swedbank/mobile/data/wallet/j$w;->c:Z

    iput-boolean p4, p0, Lcom/swedbank/mobile/data/wallet/j$w;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/c;)V
    .locals 16
    .param p1    # Lio/reactivex/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "emitter"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    iget-object v0, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/data/wallet/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    goto/16 :goto_17

    .line 171
    :cond_0
    iget-object v3, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    monitor-enter v3

    .line 172
    :try_start_0
    iget-object v0, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/data/wallet/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_16

    .line 176
    :cond_1
    :try_start_1
    iget-object v0, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/data/wallet/e;->b()V

    .line 177
    iget-object v0, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    iget-object v4, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v4}, Lcom/swedbank/mobile/data/wallet/j;->d(Lcom/swedbank/mobile/data/wallet/j;)Landroid/app/Application;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    iget-object v5, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v5}, Lcom/swedbank/mobile/data/wallet/j;->e(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/u;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lcom/swedbank/mobile/data/wallet/e;->a(Landroid/content/Context;Lcom/swedbank/mobile/data/wallet/u;)V

    .line 178
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_16

    :catch_0
    move-exception v0

    .line 180
    :try_start_2
    instance-of v4, v0, Lcom/meawallet/mtp/MeaCheckedException;

    if-nez v4, :cond_2

    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    move-object v4, v0

    :goto_0
    check-cast v4, Lcom/meawallet/mtp/MeaCheckedException;

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/meawallet/mtp/MeaCheckedException;->getMeaError()Lcom/meawallet/mtp/MeaError;

    move-result-object v4

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    sget-object v4, Lcom/swedbank/mobile/data/wallet/d;->a:Lcom/swedbank/mobile/data/wallet/d;

    check-cast v4, Lcom/meawallet/mtp/MeaError;

    .line 181
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failure at wallet initialization. ErrorMsg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "; errorCode="

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v4}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    sget-object v5, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 183
    iget-object v5, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v5}, Lcom/swedbank/mobile/data/wallet/j;->f(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v5

    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v6, Ljava/lang/Throwable;

    invoke-interface {v5, v6}, Lcom/swedbank/mobile/business/firebase/c;->a(Ljava/lang/Throwable;)V

    .line 185
    iget-boolean v0, v1, Lcom/swedbank/mobile/data/wallet/j$w;->b:Z

    const/16 v9, 0x6b

    const/16 v10, 0x3f0

    const/16 v11, 0x25b

    const/16 v12, 0xd0

    const/16 v13, 0x1fc

    const/16 v14, 0xcf

    const/16 v15, 0xc9

    const/16 v5, 0xca

    const/16 v6, 0x6d

    const/16 v7, 0x7d1

    const/16 v8, 0x7e1

    if-eqz v0, :cond_13

    .line 945
    invoke-interface {v4}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    if-ne v0, v6, :cond_4

    .line 946
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_8

    :cond_4
    if-ne v0, v5, :cond_5

    goto :goto_2

    :cond_5
    if-ne v0, v15, :cond_6

    goto :goto_2

    :cond_6
    if-ne v0, v14, :cond_7

    .line 947
    :goto_2
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_8

    :cond_7
    if-ne v0, v13, :cond_8

    goto :goto_3

    :cond_8
    if-ne v0, v12, :cond_9

    .line 948
    :goto_3
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->c:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_8

    :cond_9
    if-le v7, v0, :cond_a

    goto/16 :goto_6

    :cond_a
    if-lt v8, v0, :cond_b

    .line 950
    invoke-interface {v4}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 969
    new-instance v2, Ljava/lang/IllegalStateException;

    goto :goto_5

    .line 968
    :pswitch_0
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 967
    :pswitch_1
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 966
    :pswitch_2
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 965
    :pswitch_3
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 964
    :pswitch_4
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 963
    :pswitch_5
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 962
    :pswitch_6
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 961
    :pswitch_7
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 960
    :pswitch_8
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 959
    :pswitch_9
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 958
    :pswitch_a
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 957
    :pswitch_b
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 956
    :pswitch_c
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 955
    :pswitch_d
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 954
    :pswitch_e
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 953
    :pswitch_f
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 952
    :pswitch_10
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 949
    :goto_4
    new-instance v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    invoke-direct {v4, v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_8

    .line 969
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " is not a root reason -- check your code"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    :cond_b
    :goto_6
    if-ne v0, v11, :cond_c

    goto :goto_7

    :cond_c
    if-ne v0, v10, :cond_d

    goto :goto_7

    :cond_d
    if-ne v0, v9, :cond_e

    goto :goto_7

    :cond_e
    const/16 v4, 0x68

    if-ne v0, v4, :cond_f

    goto :goto_7

    :cond_f
    const/16 v4, 0x69

    if-ne v0, v4, :cond_10

    goto :goto_7

    :cond_10
    const/16 v4, 0x66

    if-ne v0, v4, :cond_11

    .line 981
    :goto_7
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_8

    :cond_11
    const/16 v4, 0x6f

    if-ne v0, v4, :cond_12

    .line 982
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_8

    .line 983
    :cond_12
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$Unknown;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$Unknown;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    .line 945
    :goto_8
    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_16

    .line 186
    :cond_13
    iget-boolean v0, v1, Lcom/swedbank/mobile/data/wallet/j$w;->c:Z

    if-eqz v0, :cond_2d

    .line 187
    iget-object v0, v1, Lcom/swedbank/mobile/data/wallet/j$w;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    iget-boolean v9, v1, Lcom/swedbank/mobile/data/wallet/j$w;->d:Z

    .line 990
    invoke-interface {v4}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v10

    const/16 v11, 0x386

    if-ne v10, v11, :cond_14

    goto :goto_9

    :cond_14
    const/16 v11, 0x387

    if-ne v10, v11, :cond_15

    goto :goto_9

    :cond_15
    const/16 v11, 0x38d

    if-ne v10, v11, :cond_16

    goto :goto_9

    :cond_16
    const/16 v11, 0x38a

    if-ne v10, v11, :cond_17

    .line 997
    :goto_9
    sget-object v10, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast v10, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_e

    :cond_17
    const/16 v11, 0x12f

    if-ne v10, v11, :cond_18

    goto :goto_a

    :cond_18
    const/16 v11, 0x6a

    if-ne v10, v11, :cond_19

    goto :goto_a

    :cond_19
    const/16 v11, 0x132

    if-ne v10, v11, :cond_1a

    goto :goto_a

    :cond_1a
    const/16 v11, 0x1f7

    if-ne v10, v11, :cond_1b

    .line 1001
    :goto_a
    sget-object v10, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast v10, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_e

    :cond_1b
    if-le v7, v10, :cond_1c

    goto/16 :goto_d

    :cond_1c
    if-lt v8, v10, :cond_1d

    packed-switch v10, :pswitch_data_1

    .line 1024
    new-instance v0, Ljava/lang/IllegalStateException;

    goto :goto_c

    .line 1023
    :pswitch_11
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1022
    :pswitch_12
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1021
    :pswitch_13
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1020
    :pswitch_14
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1019
    :pswitch_15
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1018
    :pswitch_16
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1017
    :pswitch_17
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1016
    :pswitch_18
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1015
    :pswitch_19
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1014
    :pswitch_1a
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1013
    :pswitch_1b
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1012
    :pswitch_1c
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1011
    :pswitch_1d
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1010
    :pswitch_1e
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1009
    :pswitch_1f
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1008
    :pswitch_20
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_b

    .line 1007
    :pswitch_21
    sget-object v10, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 1002
    :goto_b
    new-instance v11, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {v11, v10}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object v10, v11

    check-cast v10, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_e

    .line 1024
    :goto_c
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " is not a root reason -- check your code"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 1026
    :cond_1d
    :goto_d
    sget-object v10, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast v10, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 986
    :goto_e
    new-instance v11, Lcom/swedbank/mobile/business/cards/wallet/x;

    invoke-direct {v11, v10, v9}, Lcom/swedbank/mobile/business/cards/wallet/x;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;Z)V

    .line 187
    invoke-virtual {v0, v11}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 1029
    invoke-interface {v4}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    if-ne v0, v6, :cond_1e

    .line 1030
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_15

    :cond_1e
    if-ne v0, v5, :cond_1f

    goto :goto_f

    :cond_1f
    if-ne v0, v15, :cond_20

    goto :goto_f

    :cond_20
    if-ne v0, v14, :cond_21

    .line 1031
    :goto_f
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_15

    :cond_21
    if-ne v0, v13, :cond_22

    goto :goto_10

    :cond_22
    if-ne v0, v12, :cond_23

    .line 1032
    :goto_10
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->c:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_15

    :cond_23
    if-le v7, v0, :cond_25

    :cond_24
    const/16 v4, 0x25b

    goto/16 :goto_13

    :cond_25
    if-lt v8, v0, :cond_24

    .line 1034
    invoke-interface {v4}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 1053
    new-instance v2, Ljava/lang/IllegalStateException;

    goto :goto_12

    .line 1052
    :pswitch_22
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1051
    :pswitch_23
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1050
    :pswitch_24
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1049
    :pswitch_25
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1048
    :pswitch_26
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1047
    :pswitch_27
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1046
    :pswitch_28
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1045
    :pswitch_29
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1044
    :pswitch_2a
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1043
    :pswitch_2b
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1042
    :pswitch_2c
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1041
    :pswitch_2d
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1040
    :pswitch_2e
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1039
    :pswitch_2f
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1038
    :pswitch_30
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1037
    :pswitch_31
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_11

    .line 1036
    :pswitch_32
    sget-object v0, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 1033
    :goto_11
    new-instance v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    invoke-direct {v4, v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_15

    .line 1053
    :goto_12
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " is not a root reason -- check your code"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    :goto_13
    if-ne v0, v4, :cond_26

    goto :goto_14

    :cond_26
    const/16 v4, 0x3f0

    if-ne v0, v4, :cond_27

    goto :goto_14

    :cond_27
    const/16 v4, 0x6b

    if-ne v0, v4, :cond_28

    goto :goto_14

    :cond_28
    const/16 v4, 0x68

    if-ne v0, v4, :cond_29

    goto :goto_14

    :cond_29
    const/16 v4, 0x69

    if-ne v0, v4, :cond_2a

    goto :goto_14

    :cond_2a
    const/16 v4, 0x66

    if-ne v0, v4, :cond_2b

    .line 1065
    :goto_14
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    invoke-direct {v0, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_15

    :cond_2b
    const/16 v4, 0x6f

    if-ne v0, v4, :cond_2c

    .line 1066
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_15

    .line 1067
    :cond_2c
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$Unknown;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$Unknown;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    .line 1029
    :goto_15
    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {v2, v0}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V

    goto :goto_16

    .line 190
    :cond_2d
    invoke-interface/range {p1 .. p1}, Lio/reactivex/c;->c()V

    .line 194
    :goto_16
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 171
    monitor-exit v3

    :goto_17
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7d1
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7d1
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
    .end packed-switch
.end method
