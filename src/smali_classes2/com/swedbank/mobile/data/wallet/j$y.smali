.class final synthetic Lcom/swedbank/mobile/data/wallet/j$y;
.super Lkotlin/e/b/i;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->i()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/a<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/e;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/wallet/e;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "isCardholderAuthenticated"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "isCardholderAuthenticated()Z"

    return-object v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$y;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/data/wallet/e;

    .line 474
    invoke-interface {v0}, Lcom/swedbank/mobile/data/wallet/e;->i()Z

    move-result v0

    return v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/j$y;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
