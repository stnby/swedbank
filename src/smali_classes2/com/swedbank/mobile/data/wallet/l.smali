.class public final synthetic Lcom/swedbank/mobile/data/wallet/l;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic a:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/meawallet/mtp/MeaCardState;->values()[Lcom/meawallet/mtp/MeaCardState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->UNKNOWN:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->REQUIRE_ADDITIONAL_AUTHENTICATION:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_INITIALIZED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->AUTHENTICATION_COMPLETE:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DIGITIZATION_STARTED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DIGITIZED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->PROVISIONED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->PROVISION_FAILED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->ACTIVE:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->SUSPENDED:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    sget-object v1, Lcom/meawallet/mtp/MeaCardState;->MARKED_FOR_DELETION:Lcom/meawallet/mtp/MeaCardState;

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    return-void
.end method
