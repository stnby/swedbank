.class final Lcom/swedbank/mobile/data/wallet/j$t;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/data/wallet/e;Lcom/swedbank/mobile/business/e/j;Landroidx/work/o;Lcom/swedbank/mobile/data/wallet/u;Lcom/a/a/a/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/q<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$t;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/p;)V
    .locals 2
    .param p1    # Lio/reactivex/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$t$1;

    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$t;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v1}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/wallet/j$t$1;-><init>(Lcom/swedbank/mobile/data/wallet/e;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/data/wallet/n;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/wallet/n;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/f;

    invoke-interface {p1, v1}, Lio/reactivex/p;->a(Lio/reactivex/c/f;)V

    .line 91
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$t;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$t$2;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/wallet/j$t$2;-><init>(Lio/reactivex/p;)V

    check-cast v1, Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/wallet/e;->a(Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;)V

    return-void
.end method
