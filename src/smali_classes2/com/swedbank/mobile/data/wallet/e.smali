.class public interface abstract Lcom/swedbank/mobile/data/wallet/e;
.super Ljava/lang/Object;
.source "WalletPlatform.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/wallet/e$a;
    }
.end annotation


# virtual methods
.method public abstract a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a(ILcom/meawallet/mtp/MeaListener;)V
    .param p2    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Landroid/content/Context;Lcom/swedbank/mobile/data/wallet/u;)V
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/wallet/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaAuthenticationListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaAuthenticationListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaCardProvisionListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaCardProvisionListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaCardReplenishListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaCardReplenishListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaInitializeDigitizationListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/meawallet/mtp/MeaListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;Lcom/meawallet/mtp/MeaListener;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/meawallet/mtp/MeaCompleteDigitizationListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;",
            "Lcom/meawallet/mtp/MeaListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a()Z
.end method

.method public abstract b()V
.end method

.method public abstract b(Lcom/meawallet/mtp/MeaListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract b(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;Lcom/meawallet/mtp/MeaListener;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract c(Lcom/meawallet/mtp/MeaListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract c()Z
.end method

.method public abstract d()V
.end method

.method public abstract d(Lcom/meawallet/mtp/MeaListener;)V
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract e()V
.end method

.method public abstract f()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end method

.method public abstract g()Lcom/meawallet/mtp/MeaCard;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end method

.method public abstract h()V
.end method

.method public abstract i()Z
.end method

.method public abstract j()V
.end method

.method public abstract k()V
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method
