.class final Lcom/swedbank/mobile/data/wallet/j$u;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->l()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$u;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .line 625
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$u;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/data/wallet/e;->g()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 628
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 625
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Default card not selected"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/j$u;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
