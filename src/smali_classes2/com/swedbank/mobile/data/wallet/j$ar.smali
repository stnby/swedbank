.class final Lcom/swedbank/mobile/data/wallet/j$ar;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->c(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/z<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ar;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ar;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/x;)V
    .locals 3
    .param p1    # Lio/reactivex/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 515
    :try_start_0
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$ar;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v1}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/data/wallet/j$ar;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/data/wallet/e;->a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 516
    new-instance v2, Lcom/swedbank/mobile/data/wallet/j$ar$1;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ar$1;-><init>(Lcom/swedbank/mobile/data/wallet/j$ar;Lio/reactivex/x;)V

    check-cast v2, Lcom/meawallet/mtp/MeaCardListener;

    invoke-interface {v1, v2}, Lcom/meawallet/mtp/MeaCard;->selectForContactlessPayment(Lcom/meawallet/mtp/MeaCardListener;)V

    goto :goto_0

    .line 528
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v1}, Lio/reactivex/x;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 530
    :catch_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
