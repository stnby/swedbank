.class final Lcom/swedbank/mobile/data/wallet/j$e;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/data/wallet/e;Lcom/swedbank/mobile/business/e/j;Landroidx/work/o;Lcom/swedbank/mobile/data/wallet/u;Lcom/a/a/a/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/wallet/j$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/wallet/j$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/wallet/j$e;->a:Lcom/swedbank/mobile/data/wallet/j$e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/meawallet/mtp/MeaCard;)Lcom/swedbank/mobile/data/wallet/a$a;
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/swedbank/mobile/data/wallet/a$a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/wallet/a$a;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/meawallet/mtp/MeaCard;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$e;->a(Lcom/meawallet/mtp/MeaCard;)Lcom/swedbank/mobile/data/wallet/a$a;

    move-result-object p1

    return-object p1
.end method
