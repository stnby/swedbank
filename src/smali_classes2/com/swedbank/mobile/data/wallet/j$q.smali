.class final Lcom/swedbank/mobile/data/wallet/j$q;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->d(Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$q;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$q;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .line 538
    :try_start_0
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$q;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$q;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/wallet/e;->a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 539
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaCard;->deselectForContactlessPayment()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 543
    :catch_0
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Card deselected for payment: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 544
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$q;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->i(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/b;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method
