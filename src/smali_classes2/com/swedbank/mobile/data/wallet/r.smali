.class public final Lcom/swedbank/mobile/data/wallet/r;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/wallet/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/j;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroidx/work/o;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/u;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/a/a/a/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroidx/work/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/a/a/a/f;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/r;->a:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/r;->b:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/swedbank/mobile/data/wallet/r;->c:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/swedbank/mobile/data/wallet/r;->d:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/swedbank/mobile/data/wallet/r;->e:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/swedbank/mobile/data/wallet/r;->f:Ljavax/inject/Provider;

    .line 40
    iput-object p7, p0, Lcom/swedbank/mobile/data/wallet/r;->g:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/wallet/r;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroidx/work/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/a/a/a/f;",
            ">;)",
            "Lcom/swedbank/mobile/data/wallet/r;"
        }
    .end annotation

    .line 55
    new-instance v8, Lcom/swedbank/mobile/data/wallet/r;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/data/wallet/r;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/wallet/j;
    .locals 9

    .line 45
    new-instance v8, Lcom/swedbank/mobile/data/wallet/j;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/r;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/r;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/firebase/c;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/r;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/data/wallet/e;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/r;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/e/j;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/r;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroidx/work/o;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/r;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/data/wallet/u;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/r;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/a/a/a/f;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/data/wallet/j;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/data/wallet/e;Lcom/swedbank/mobile/business/e/j;Landroidx/work/o;Lcom/swedbank/mobile/data/wallet/u;Lcom/a/a/a/f;)V

    return-object v8
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/r;->a()Lcom/swedbank/mobile/data/wallet/j;

    move-result-object v0

    return-object v0
.end method
