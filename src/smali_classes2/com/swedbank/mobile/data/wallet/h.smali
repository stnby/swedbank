.class public final Lcom/swedbank/mobile/data/wallet/h;
.super Ljava/lang/Object;
.source "WalletPushMessageHandler.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/push/a;


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/h;->a:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string p1, "data"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    :try_start_0
    invoke-static {p2}, Lcom/meawallet/mtp/MeaTokenPlatform$Rns;->isMeaRemoteMessage(Ljava/util/Map;)Ljava/lang/Boolean;

    move-result-object p1

    const-string v0, "MeaTokenPlatform.Rns.isMeaRemoteMessage(data)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 24
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isInitialized()Z

    move-result p1

    if-nez p1, :cond_0

    .line 25
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/h;->a:Landroid/app/Application;

    new-instance v0, Lcom/swedbank/mobile/data/wallet/h$a;

    invoke-direct {v0, p0, p2}, Lcom/swedbank/mobile/data/wallet/h$a;-><init>(Lcom/swedbank/mobile/data/wallet/h;Ljava/util/Map;)V

    check-cast v0, Lcom/meawallet/mtp/MeaListener;

    invoke-static {p1, v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->initialize(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V

    goto :goto_0

    .line 50
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/data/wallet/h$b;

    invoke-direct {p1, p2}, Lcom/swedbank/mobile/data/wallet/h$b;-><init>(Ljava/util/Map;)V

    check-cast p1, Ljava/util/concurrent/Callable;

    invoke-static {p1}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object p1

    .line 54
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/b;->b(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object p1

    const-string p2, "Completable.fromCallable\u2026scribeOn(Schedulers.io())"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x3

    const/4 v0, 0x0

    invoke-static {p1, v0, v0, p2, v0}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :cond_1
    :goto_0
    return-void
.end method
