.class final Lcom/swedbank/mobile/data/wallet/j$ap;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->a(Ljava/util/List;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ap;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ap;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 7

    .line 633
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ap;->b:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 941
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 634
    sget-object v2, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Scheduling replenishment work for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 946
    new-instance v2, Landroidx/work/j$a;

    const-class v3, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;

    invoke-direct {v2, v3}, Landroidx/work/j$a;-><init>(Ljava/lang/Class;)V

    .line 636
    invoke-static {v1}, Lcom/swedbank/mobile/data/wallet/b;->b(Ljava/lang/String;)Landroidx/work/e;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroidx/work/j$a;->a(Landroidx/work/e;)Landroidx/work/p$a;

    move-result-object v2

    check-cast v2, Landroidx/work/j$a;

    .line 637
    invoke-static {}, Lcom/swedbank/mobile/data/wallet/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroidx/work/j$a;->a(Ljava/lang/String;)Landroidx/work/p$a;

    move-result-object v2

    check-cast v2, Landroidx/work/j$a;

    .line 638
    sget-object v3, Landroidx/work/a;->a:Landroidx/work/a;

    const-wide/16 v4, 0x2710

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroidx/work/j$a;->a(Landroidx/work/a;JLjava/util/concurrent/TimeUnit;)Landroidx/work/p$a;

    move-result-object v2

    check-cast v2, Landroidx/work/j$a;

    .line 639
    new-instance v3, Landroidx/work/c$a;

    invoke-direct {v3}, Landroidx/work/c$a;-><init>()V

    .line 640
    sget-object v4, Landroidx/work/i;->b:Landroidx/work/i;

    invoke-virtual {v3, v4}, Landroidx/work/c$a;->a(Landroidx/work/i;)Landroidx/work/c$a;

    move-result-object v3

    .line 641
    invoke-virtual {v3}, Landroidx/work/c$a;->a()Landroidx/work/c;

    move-result-object v3

    .line 639
    invoke-virtual {v2, v3}, Landroidx/work/j$a;->a(Landroidx/work/c;)Landroidx/work/p$a;

    move-result-object v2

    check-cast v2, Landroidx/work/j$a;

    .line 642
    invoke-virtual {v2}, Landroidx/work/j$a;->e()Landroidx/work/p;

    move-result-object v2

    .line 643
    check-cast v2, Landroidx/work/j;

    .line 644
    iget-object v3, p0, Lcom/swedbank/mobile/data/wallet/j$ap;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v3}, Lcom/swedbank/mobile/data/wallet/j;->j(Lcom/swedbank/mobile/data/wallet/j;)Landroidx/work/o;

    move-result-object v3

    .line 645
    invoke-static {v1}, Lcom/swedbank/mobile/data/wallet/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 646
    sget-object v4, Landroidx/work/f;->b:Landroidx/work/f;

    .line 644
    invoke-virtual {v3, v1, v4, v2}, Landroidx/work/o;->a(Ljava/lang/String;Landroidx/work/f;Landroidx/work/j;)Landroidx/work/k;

    goto :goto_0

    :cond_0
    return-void
.end method
