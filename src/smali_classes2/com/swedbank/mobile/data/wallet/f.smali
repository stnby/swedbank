.class public final Lcom/swedbank/mobile/data/wallet/f;
.super Ljava/lang/Object;
.source "WalletPlatform.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/wallet/e;


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/f;->a:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->getCardById(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizationId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "digitizationSecret"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardBin"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;->withCardSecret(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    move-result-object p1

    const-string p2, "MeaInitializeDigitizatio\u2026itizationSecret, cardBin)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(ILcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p2    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-static {p1, p2}, Lcom/meawallet/mtp/MeaTokenPlatform;->setDefaultTransactionLimit(ILcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public a(Landroid/content/Context;Lcom/swedbank/mobile/data/wallet/u;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/wallet/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiver"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    check-cast p2, Lcom/meawallet/mtp/MeaTransactionReceiver;

    invoke-static {p1, p2}, Lcom/meawallet/mtp/MeaTokenPlatform;->registerTransactionReceiver(Landroid/content/Context;Lcom/meawallet/mtp/MeaTransactionReceiver;)V

    return-void
.end method

.method public a(Lcom/meawallet/mtp/MeaAuthenticationListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaAuthenticationListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->setAuthenticationListener(Lcom/meawallet/mtp/MeaAuthenticationListener;)V

    return-void
.end method

.method public a(Lcom/meawallet/mtp/MeaCardProvisionListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaCardProvisionListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->setCardProvisionListener(Lcom/meawallet/mtp/MeaCardProvisionListener;)V

    return-void
.end method

.method public a(Lcom/meawallet/mtp/MeaCardReplenishListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaCardReplenishListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->setCardReplenishListener(Lcom/meawallet/mtp/MeaCardReplenishListener;)V

    return-void
.end method

.method public a(Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->setDigitizedCardStateChangeListener(Lcom/meawallet/mtp/MeaDigitizedCardStateChangeListener;)V

    return-void
.end method

.method public a(Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->getPaymentAppInstanceId(Lcom/meawallet/mtp/MeaGetPaymentAppInstanceIdListener;)V

    return-void
.end method

.method public a(Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaInitializeDigitizationListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "initializationParams"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-static {p1, p2}, Lcom/meawallet/mtp/MeaTokenPlatform;->initializeDigitization(Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V

    return-void
.end method

.method public a(Lcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/f;->a:Landroid/app/Application;

    invoke-static {v0, p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->initialize(Landroid/app/Application;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;Lcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    const-string v0, "pushId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "consumerLanguage"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    const-string v0, "(this as java.lang.String).toLowerCase()"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/MeaTokenPlatform;->register(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/meawallet/mtp/MeaCompleteDigitizationListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "eligibilityReceiptValue"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "termsAndConditionsAssetId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "securityCode"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-static/range {p1 .. p6}, Lcom/meawallet/mtp/MeaTokenPlatform;->completeDigitization(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/meawallet/mtp/MeaCompleteDigitizationListener;)V

    return-void
.end method

.method public a(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/meawallet/mtp/MeaTransactionLimit;",
            ">;",
            "Lcom/meawallet/mtp/MeaListener;",
            ")V"
        }
    .end annotation

    const-string v0, "limits"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {p1, p2}, Lcom/meawallet/mtp/MeaTokenPlatform;->addTransactionLimits(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public a()Z
    .locals 1

    .line 82
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/f;->a:Landroid/app/Application;

    invoke-static {v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->initialize(Landroid/app/Application;)V

    return-void
.end method

.method public b(Lcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->clearAllTransactionLimits(Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public b(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;Lcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    const-string v0, "pushId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "consumerLanguage"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    const-string v0, "(this as java.lang.String).toLowerCase()"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2, p3}, Lcom/meawallet/mtp/MeaTokenPlatform;->updateDeviceInfo(Ljava/lang/String;Ljava/lang/String;Lcom/meawallet/mtp/MeaListener;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c(Lcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->markAllCardsForDeletion(Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public c()Z
    .locals 1

    .line 88
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->isRegistered()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x1

    .line 110
    invoke-static {v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->setAllowPaymentsWhenLocked(Z)V

    return-void
.end method

.method public d(Lcom/meawallet/mtp/MeaListener;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-static {p1}, Lcom/meawallet/mtp/MeaTokenPlatform;->delete(Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method

.method public e()V
    .locals 0

    .line 112
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->registerDeviceUnlockReceiver()V

    return-void
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 138
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->getCards()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/meawallet/mtp/MeaCard;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 144
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->getDefaultCardForContactlessPayments()Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    .line 149
    invoke-static {v0}, Lcom/meawallet/mtp/MeaTokenPlatform;->setAuthenticationListener(Lcom/meawallet/mtp/MeaAuthenticationListener;)V

    return-void
.end method

.method public i()Z
    .locals 1

    .line 151
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform$CdCvm;->isCardholderAuthenticated()Z

    move-result v0

    return v0
.end method

.method public j()V
    .locals 0

    .line 153
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->requestCardholderAuthentication()V

    return-void
.end method

.method public k()V
    .locals 0

    .line 155
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->authenticateWithDeviceUnlock()V

    return-void
.end method

.method public l()V
    .locals 0

    .line 160
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->removeDigitizedCardStateChangeListener()V

    return-void
.end method

.method public m()V
    .locals 0

    .line 165
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->removeCardProvisionListener()V

    return-void
.end method

.method public n()V
    .locals 0

    .line 170
    invoke-static {}, Lcom/meawallet/mtp/MeaTokenPlatform;->removeCardReplenishListener()V

    return-void
.end method
